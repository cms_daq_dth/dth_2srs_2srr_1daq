library IEEE;
use IEEE.std_logic_1164.all;

package Proc is

procedure PCIe_func(
		signal usr_clock		: IN  STD_LOGIC;
				funct			: in integer;
				rp_wn			: in std_logic;
				data_b			: in std_logic_vector(63 downto 0);
				
		signal usr_func_wr		: OUT STD_LOGIC_VECTOR(16383 downto 0); 
		signal usr_wen			: OUT STD_LOGIC;
		signal user_data_wr		: OUT STD_LOGIC_VECTOR(63 downto 0); 			
		signal usr_func_rd		: OUT STD_LOGIC_VECTOR(16383 downto 0); 
		signal usr_rden			: OUT STD_LOGIC
		);
 
end Proc;

package body Proc is

procedure PCIe_func(
		signal usr_clock		: IN  STD_LOGIC;
				funct				: in integer;
				rp_wn				: in std_logic;
				data_b			: in std_logic_vector(63 downto 0);
				
		signal usr_func_wr	: OUT STD_LOGIC_VECTOR(16383 downto 0); 
		signal usr_wen			: OUT STD_LOGIC;
		signal user_data_wr	: OUT STD_LOGIC_VECTOR(63 downto 0); 				
		signal usr_func_rd	: OUT STD_LOGIC_VECTOR(16383 downto 0); 
		signal usr_rden		: OUT STD_LOGIC) is
begin
	usr_func_wr	   <= (others => '0'); 
	usr_wen			<= '0';
	user_data_wr	<= (others => '0'); 				
	usr_func_rd	   <= (others => '0'); 
	usr_rden		   <= '0';
	wait until falling_edge(usr_clock);
	if rp_wn = '1' then
		usr_func_rd(funct)<= '1'; 
		usr_rden		   	<= '1';
		wait until falling_edge(usr_clock);
		usr_func_rd(funct)<= '0'; 
		usr_rden		   	<= '0';		
	else
		usr_func_wr(funct)<= '1'; 
		usr_wen				<= '1';
		user_data_wr		<= data_b; 
		wait until falling_edge(usr_clock);
		usr_func_wr(funct)<= '0'; 
		usr_wen				<= '0';
		user_data_wr		<= (others => '0'); 
	end if;
	wait until falling_edge(usr_clock);
 
end PCIe_func;
  
 
end Proc;