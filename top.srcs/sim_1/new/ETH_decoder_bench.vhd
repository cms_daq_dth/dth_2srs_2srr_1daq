LIBRARY ieee;
 
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;
 use work.interface.all;
 
entity top_tb is
end top_tb;

architecture behavioral of top_tb is
 
component decoder is
	port
	(
		reset_in_p							: in std_logic;
		clock_in							   : in std_logic;

		RX_lbus_in						   : in rx_user_ift;

		MAC_home								: in std_logic_vector(47 downto 0);
		IP_home								: in std_logic_vector(31 downto 0);
		IP_Dest								: in std_logic_vector(31 downto 0); 	-- IP for the ARP request   SERVER IP (used for ARP request  IP Destination)
		IP_GateWay							: in std_logic_vector(31 downto 0);		-- IP for the ARP request   GATEWAY IP (used for ARP request  IP gateway)
		request_ARP							: in std_logic;								-- The request will reset the counter for QRP probe
		
		ARP_MAC_Dest      				: out std_logic_vector(47 downto 0);
		ARP_MAC_GateWay  					: out std_logic_vector(47 downto 0);
		ARP_check_MAC_src 				: out std_logic;
		ARP_check_IP_src  				: out std_logic;
		ARP_MAC_error    					: out std_logic_vector(47 downto 0);
		ARP_MAC_rcvOK 						: out std_logic;
		ARP_MAC_gwOK  						: out std_logic;
		ARP_IP								: out std_logic_vector(31 downto 0);
		ARP_MAC								: out std_logic_vector(47 downto 0);
		req_ARP_reply 						: out std_logic;
		ARP_reply     						: out std_logic;	
		valid_ARP_packet					: out std_logic;
		
		req_icmp_prb						: out std_logic;
		PING_HD_empty						: in std_logic;
		ping_last_dt						: out std_logic;
		wr_PING_data						: out std_logic;
		data_ping							: out std_logic_vector(63 downto 0);
		Reset_PING_FIFO					: out std_logic;
		PING_MAC_dst						: out std_logic_vector(47 downto 0);
		PING_IP_dst							: out std_logic_vector(31 downto 0);
		
		clock_out							: in std_logic
  
	 );
end component;

signal reset_in_p							   : std_logic;
signal clock_in							   : std_logic;
signal RX_lbus_in						      : rx_user_ift;
signal MAC_home								: std_logic_vector(47 downto 0);
signal IP_home								   : std_logic_vector(31 downto 0);
signal IP_Dest								   : std_logic_vector(31 downto 0); 	-- IP for the ARP request   SERVER IP (used for ARP request  IP Destination)
signal IP_GateWay							   : std_logic_vector(31 downto 0);		-- IP for the ARP request   GATEWAY IP (used for ARP request  IP gateway)
signal request_ARP							: std_logic;								-- The request will reset the counter for QRP probe
signal ARP_MAC_Dest      					: std_logic_vector(47 downto 0);
signal ARP_MAC_GateWay  				   : std_logic_vector(47 downto 0);
signal ARP_check_MAC_src 					: std_logic;
signal ARP_check_IP_src  					: std_logic;
signal ARP_IP									: std_logic_vector(31 downto 0);
signal ARP_MAC									: std_logic_vector(47 downto 0);
signal ARP_MAC_error    				   : std_logic_vector(47 downto 0);
signal ARP_MAC_rcvOK 					   : std_logic;
signal ARP_MAC_gwOK  					   : std_logic;
signal req_ARP_reply 						: std_logic;
signal ARP_reply     						: std_logic;	
signal valid_ARP_packet					   : std_logic;
signal req_icmp_prb						   : std_logic;
signal PING_HD_empty						   : std_logic;
signal ping_last_dt						   : std_logic;
signal wr_PING_data						   : std_logic;
signal data_ping							   : std_logic_vector(63 downto 0);
signal Reset_PING_FIFO					   : std_logic;
signal PING_MAC_dst						   : std_logic_vector(47 downto 0);
signal PING_IP_dst							: std_logic_vector(31 downto 0);
signal clock_out								: std_logic;
SIGNAL SIM_RUN	: BOOLEAN := TRUE;

begin

tb:decoder
port map (
 		reset_in_p					=> reset_in_p			      ,				
		clock_in						=> clock_in				      ,			   
		RX_lbus_in					=> RX_lbus_in			      ,			   
		MAC_home						=> MAC_home				      ,				
		IP_home						=> IP_home				      ,				
		IP_Dest						=> IP_Dest				      ,				
		IP_GateWay					=> IP_GateWay			      ,				
		request_ARP					=> request_ARP			      ,				
		ARP_MAC_Dest      		=> ARP_MAC_Dest      		,				
		ARP_MAC_GateWay  			=> ARP_MAC_GateWay  		   ,				
		ARP_check_MAC_src 		=> ARP_check_MAC_src 		,				
		ARP_check_IP_src  		=> ARP_check_IP_src  		,				
		ARP_MAC_error    			=> ARP_MAC_error    		   ,				
		ARP_MAC_rcvOK 				=> ARP_MAC_rcvOK 			   ,				
		ARP_MAC_gwOK  				=> ARP_MAC_gwOK  			   ,
		ARP_IP						=> ARP_IP						,
		ARP_MAC						=> ARP_MAC						,
		req_ARP_reply 				=> req_ARP_reply 		      ,				
		ARP_reply     				=> ARP_reply     		      ,				
		valid_ARP_packet			=> valid_ARP_packet	      ,				
		req_icmp_prb				=> req_icmp_prb		      ,				
		PING_HD_empty				=> PING_HD_empty		      ,				
		ping_last_dt				=> ping_last_dt		      ,				
		wr_PING_data				=> wr_PING_data		      ,				
		data_ping					=> data_ping			      ,				
		Reset_PING_FIFO			=> Reset_PING_FIFO	      ,				
		PING_MAC_dst				=> PING_MAC_dst		      ,				
		PING_IP_dst					=> PING_IP_dst			      ,				
		clock_out					=> clock_out							
);

clk_b_l:process
begin
	while sim_run  loop
		clock_in <= '0';
		clock_out <= '0';
		wait for 5000 ps;
		clock_in <= '1';
		clock_out <= '1';
		wait for 5000 ps;
	end loop;
	wait;
end process;

 process
 begin
 reset_in_p <= '1';
 wait for 110 ns;
 reset_in_p <= '0';
 
 
 
 wait for 10 us;
 sim_run <= false;
 wait;
END PROCESS;


process
begin

RX_lbus_in(0).data_bus 			<= x"00000000000000000000000000000000";
RX_lbus_in(0).sopckt				<= '0';
RX_lbus_in(0).data_ena	   	<= '0';
RX_lbus_in(0).eopckt	      	<= '0';
RX_lbus_in(0).err_pckt	   	<= '0';
RX_lbus_in(0).empty_pckt		<= x"0";
RX_lbus_in(1).data_bus 			<= x"00000000000000000000000000000000";
RX_lbus_in(1).sopckt				<= '0';
RX_lbus_in(1).data_ena	   	<= '0';
RX_lbus_in(1).eopckt	      	<= '0';
RX_lbus_in(1).err_pckt	   	<= '0';
RX_lbus_in(1).empty_pckt		<= x"0";
RX_lbus_in(2).data_bus 			<= x"00000000000000000000000000000000";
RX_lbus_in(2).sopckt				<= '0';
RX_lbus_in(2).data_ena	   	<= '0';
RX_lbus_in(2).eopckt	      	<= '0';
RX_lbus_in(2).err_pckt	   	<= '0';
RX_lbus_in(2).empty_pckt		<= x"0";
RX_lbus_in(3).data_bus 			<= x"00000000000000000000000000000000";
RX_lbus_in(3).sopckt				<= '0';
RX_lbus_in(3).data_ena	    	<= '0';
RX_lbus_in(3).eopckt       	<= '0';
RX_lbus_in(3).err_pckt	    	<= '0';
RX_lbus_in(3).empty_pckt		<= x"0";
 



MAC_home					<= x"080508050805";--
IP_home					<= x"C0A80112";
IP_Dest					<= x"C0A80164";--	-- IP for the ARP request   SERVER IP (used for ARP request  IP Destination)
IP_GateWay				<= x"00000000";--	-- IP for the ARP request   GATEWAY IP (used for ARP request  IP gateway)
request_ARP				<= '0';			--
	
wait for 400 ns;
-- ARP request
wait until falling_edge(clock_in);
RX_lbus_in(0).data_bus 			<= x"08050805080545788956231208060001";
RX_lbus_in(0).sopckt				<= '1';
RX_lbus_in(0).data_ena	   	<= '1';
RX_lbus_in(0).eopckt	      	<= '0';
RX_lbus_in(0).err_pckt	   	<= '0';
RX_lbus_in(0).empty_pckt		<= x"0";
RX_lbus_in(1).data_bus 			<= x"080006040001457889562312C0A80164";
RX_lbus_in(1).sopckt				<= '0';
RX_lbus_in(1).data_ena	   	<= '1';
RX_lbus_in(1).eopckt	      	<= '0';
RX_lbus_in(1).err_pckt	   	<= '0';
RX_lbus_in(1).empty_pckt		<= x"0";
RX_lbus_in(2).data_bus 			<= x"080508050805C0A80112000000000000";
RX_lbus_in(2).sopckt				<= '0';
RX_lbus_in(2).data_ena	   	<= '1';
RX_lbus_in(2).eopckt	      	<= '0';
RX_lbus_in(2).err_pckt	   	<= '0';
RX_lbus_in(2).empty_pckt		<= x"0";
RX_lbus_in(3).data_bus 			<= x"00000000000000000000000000000000";
RX_lbus_in(3).sopckt				<= '0';
RX_lbus_in(3).data_ena	    	<= '1';
RX_lbus_in(3).eopckt       	<= '1';
RX_lbus_in(3).err_pckt	    	<= '0';
RX_lbus_in(3).empty_pckt		<= x"4";
wait until falling_edge(clock_in);
RX_lbus_in(0).sopckt				<= '0';
RX_lbus_in(0).data_ena	   	<= '0';
RX_lbus_in(0).eopckt	      	<= '0';
RX_lbus_in(0).err_pckt	   	<= '0';
RX_lbus_in(0).empty_pckt		<= x"0";
RX_lbus_in(1).sopckt				<= '0';
RX_lbus_in(1).data_ena	   	<= '0';
RX_lbus_in(1).eopckt	      	<= '0';
RX_lbus_in(1).err_pckt	   	<= '0';
RX_lbus_in(1).empty_pckt		<= x"0";
RX_lbus_in(2).sopckt				<= '0';
RX_lbus_in(2).data_ena	   	<= '0';
RX_lbus_in(2).eopckt	      	<= '0';
RX_lbus_in(2).err_pckt	   	<= '0';
RX_lbus_in(2).empty_pckt		<= x"0";
RX_lbus_in(3).sopckt				<= '0';
RX_lbus_in(3).data_ena	    	<= '0';
RX_lbus_in(3).eopckt       	<= '0';
RX_lbus_in(3).err_pckt	    	<= '0';
RX_lbus_in(3).empty_pckt		<= x"0";	
	
	
wait for 400 ns;
-- ARP reply
wait until falling_edge(clock_in);
RX_lbus_in(0).data_bus 			<= x"08050805080545788956231208060001";
RX_lbus_in(0).sopckt				<= '1';
RX_lbus_in(0).data_ena	   	<= '1';
RX_lbus_in(0).eopckt	      	<= '0';
RX_lbus_in(0).err_pckt	   	<= '0';
RX_lbus_in(0).empty_pckt		<= x"0";
RX_lbus_in(1).data_bus 			<= x"080006040002457889562312C0A80164";
RX_lbus_in(1).sopckt				<= '0';
RX_lbus_in(1).data_ena	   	<= '1';
RX_lbus_in(1).eopckt	      	<= '0';
RX_lbus_in(1).err_pckt	   	<= '0';
RX_lbus_in(1).empty_pckt		<= x"0";
RX_lbus_in(2).data_bus 			<= x"080508050805C0A80112000000000000";
RX_lbus_in(2).sopckt				<= '0';
RX_lbus_in(2).data_ena	   	<= '1';
RX_lbus_in(2).eopckt	      	<= '0';
RX_lbus_in(2).err_pckt	   	<= '0';
RX_lbus_in(2).empty_pckt		<= x"0";
RX_lbus_in(3).data_bus 			<= x"00000000000000000000000000000000";
RX_lbus_in(3).sopckt				<= '0';
RX_lbus_in(3).data_ena	    	<= '1';
RX_lbus_in(3).eopckt       	<= '1';
RX_lbus_in(3).err_pckt	    	<= '0';
RX_lbus_in(3).empty_pckt		<= x"4";
wait until falling_edge(clock_in);
RX_lbus_in(0).sopckt				<= '0';
RX_lbus_in(0).data_ena	   	<= '0';
RX_lbus_in(0).eopckt	      	<= '0';
RX_lbus_in(0).err_pckt	   	<= '0';
RX_lbus_in(0).empty_pckt		<= x"0";
RX_lbus_in(1).sopckt				<= '0';
RX_lbus_in(1).data_ena	   	<= '0';
RX_lbus_in(1).eopckt	      	<= '0';
RX_lbus_in(1).err_pckt	   	<= '0';
RX_lbus_in(1).empty_pckt		<= x"0";
RX_lbus_in(2).sopckt				<= '0';
RX_lbus_in(2).data_ena	   	<= '0';
RX_lbus_in(2).eopckt	      	<= '0';
RX_lbus_in(2).err_pckt	   	<= '0';
RX_lbus_in(2).empty_pckt		<= x"0";
RX_lbus_in(3).sopckt				<= '0';
RX_lbus_in(3).data_ena	    	<= '0';
RX_lbus_in(3).eopckt       	<= '0';
RX_lbus_in(3).err_pckt	    	<= '0';
RX_lbus_in(3).empty_pckt		<= x"0";		



wait for 400 ns;
-- ARP problem (with my MAC)
wait until falling_edge(clock_in);
RX_lbus_in(0).data_bus 			<= x"08050805080508050805080508060001";
RX_lbus_in(0).sopckt				<= '1';
RX_lbus_in(0).data_ena	   	<= '1';
RX_lbus_in(0).eopckt	      	<= '0';
RX_lbus_in(0).err_pckt	   	<= '0';
RX_lbus_in(0).empty_pckt		<= x"0";
RX_lbus_in(1).data_bus 			<= x"080006040002080508050805C0A80164";
RX_lbus_in(1).sopckt				<= '0';
RX_lbus_in(1).data_ena	   	<= '1';
RX_lbus_in(1).eopckt	      	<= '0';
RX_lbus_in(1).err_pckt	   	<= '0';
RX_lbus_in(1).empty_pckt		<= x"0";
RX_lbus_in(2).data_bus 			<= x"080508050805C0A80112000000000000";
RX_lbus_in(2).sopckt				<= '0';
RX_lbus_in(2).data_ena	   	<= '1';
RX_lbus_in(2).eopckt	      	<= '0';
RX_lbus_in(2).err_pckt	   	<= '0';
RX_lbus_in(2).empty_pckt		<= x"0";
RX_lbus_in(3).data_bus 			<= x"00000000000000000000000000000000";
RX_lbus_in(3).sopckt				<= '0';
RX_lbus_in(3).data_ena	    	<= '1';
RX_lbus_in(3).eopckt       	<= '1';
RX_lbus_in(3).err_pckt	    	<= '0';
RX_lbus_in(3).empty_pckt		<= x"4";
wait until falling_edge(clock_in);
RX_lbus_in(0).sopckt				<= '0';
RX_lbus_in(0).data_ena	   	<= '0';
RX_lbus_in(0).eopckt	      	<= '0';
RX_lbus_in(0).err_pckt	   	<= '0';
RX_lbus_in(0).empty_pckt		<= x"0";
RX_lbus_in(1).sopckt				<= '0';
RX_lbus_in(1).data_ena	   	<= '0';
RX_lbus_in(1).eopckt	      	<= '0';
RX_lbus_in(1).err_pckt	   	<= '0';
RX_lbus_in(1).empty_pckt		<= x"0";
RX_lbus_in(2).sopckt				<= '0';
RX_lbus_in(2).data_ena	   	<= '0';
RX_lbus_in(2).eopckt	      	<= '0';
RX_lbus_in(2).err_pckt	   	<= '0';
RX_lbus_in(2).empty_pckt		<= x"0";
RX_lbus_in(3).sopckt				<= '0';
RX_lbus_in(3).data_ena	    	<= '0';
RX_lbus_in(3).eopckt       	<= '0';
RX_lbus_in(3).err_pckt	    	<= '0';
RX_lbus_in(3).empty_pckt		<= x"0";	


wait for 400 ns;
-- PING request
wait until falling_edge(clock_in);
RX_lbus_in(0).data_bus 			<= x"08050805080545454545454508004500";
RX_lbus_in(0).sopckt				<= '1';
RX_lbus_in(0).data_ena	   	<= '1';
RX_lbus_in(0).eopckt	      	<= '0';
RX_lbus_in(0).err_pckt	   	<= '0';
RX_lbus_in(0).empty_pckt		<= x"0";
RX_lbus_in(1).data_bus 			<= x"003c00004000ff01f7b9c0a80164c0a8";
RX_lbus_in(1).sopckt				<= '0';
RX_lbus_in(1).data_ena	   	<= '1';
RX_lbus_in(1).eopckt	      	<= '0';
RX_lbus_in(1).err_pckt	   	<= '0';
RX_lbus_in(1).empty_pckt		<= x"0";
RX_lbus_in(2).data_bus 			<= x"01120800fecd12341234111111111111";
RX_lbus_in(2).sopckt				<= '0';
RX_lbus_in(2).data_ena	   	<= '1';
RX_lbus_in(2).eopckt	      	<= '0';
RX_lbus_in(2).err_pckt	   	<= '0';
RX_lbus_in(2).empty_pckt		<= x"0";
RX_lbus_in(3).data_bus 			<= x"22222222222222222222222222222222";
RX_lbus_in(3).sopckt				<= '0';
RX_lbus_in(3).data_ena	    	<= '1';
RX_lbus_in(3).eopckt       	<= '0';
RX_lbus_in(3).err_pckt	    	<= '0';
RX_lbus_in(3).empty_pckt		<= x"0";
wait until falling_edge(clock_in);
RX_lbus_in(0).data_bus 			<= x"33333333333333333333333333333333";
RX_lbus_in(0).sopckt				<= '0';
RX_lbus_in(0).data_ena	   	<= '1';
RX_lbus_in(0).eopckt	      	<= '0';
RX_lbus_in(0).err_pckt	   	<= '0';
RX_lbus_in(0).empty_pckt		<= x"0";
RX_lbus_in(1).data_bus 			<= x"44440000000000000000000000000000";
RX_lbus_in(1).sopckt				<= '0';
RX_lbus_in(1).data_ena	   	<= '1';
RX_lbus_in(1).eopckt	      	<= '1';
RX_lbus_in(1).err_pckt	   	<= '0';
RX_lbus_in(1).empty_pckt		<= x"e";
RX_lbus_in(2).data_bus 			<= x"080508050805C0A80112000000000000";
RX_lbus_in(2).sopckt				<= '0';
RX_lbus_in(2).data_ena	   	<= '0';
RX_lbus_in(2).eopckt	      	<= '0';
RX_lbus_in(2).err_pckt	   	<= '0';
RX_lbus_in(2).empty_pckt		<= x"0";
RX_lbus_in(3).data_bus 			<= x"00000000000000000000000000000000";
RX_lbus_in(3).sopckt				<= '0';
RX_lbus_in(3).data_ena	    	<= '0';
RX_lbus_in(3).eopckt       	<= '0';
RX_lbus_in(3).err_pckt	    	<= '0';
RX_lbus_in(3).empty_pckt		<= x"0";
wait until falling_edge(clock_in);
RX_lbus_in(0).sopckt				<= '0';
RX_lbus_in(0).data_ena	   	<= '0';
RX_lbus_in(0).eopckt	      	<= '0';
RX_lbus_in(0).err_pckt	   	<= '0';
RX_lbus_in(0).empty_pckt		<= x"0";
RX_lbus_in(1).sopckt				<= '0';
RX_lbus_in(1).data_ena	   	<= '0';
RX_lbus_in(1).eopckt	      	<= '0';
RX_lbus_in(1).err_pckt	   	<= '0';
RX_lbus_in(1).empty_pckt		<= x"0";
RX_lbus_in(2).sopckt				<= '0';
RX_lbus_in(2).data_ena	   	<= '0';
RX_lbus_in(2).eopckt	      	<= '0';
RX_lbus_in(2).err_pckt	   	<= '0';
RX_lbus_in(2).empty_pckt		<= x"0";
RX_lbus_in(3).sopckt				<= '0';
RX_lbus_in(3).data_ena	    	<= '0';
RX_lbus_in(3).eopckt       	<= '0';
RX_lbus_in(3).err_pckt	    	<= '0';
RX_lbus_in(3).empty_pckt		<= x"0";	


wait;
end process;
 
 
 
 
 
 
 
 
 
end behavioral;