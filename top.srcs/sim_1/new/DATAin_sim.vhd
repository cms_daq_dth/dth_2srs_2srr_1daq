----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 27.08.2019 09:17:43
-- Design Name: 
-- Module Name: BIFI_sim - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.std_logic_unsigned.all;
use work.Proc.all;
use work.address_table.all;
use work.interface.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity DATAin_sim is
 generic (number_of_events		: integer := 10
 );
--  Port ( );
end DATAin_sim;

architecture Behavioral of DATAin_sim is

signal usr_clk								: std_logic;
signal rst_usr_clk_n						: std_logic;
signal usr_func_wr							: std_logic_vector(16383 downto 0); 
signal usr_wen								: std_logic;
signal usr_data_wr							: std_logic_vector(63 downto 0); 
signal usr_func_rd							: std_logic_vector(16383 downto 0); 
signal usr_rden								: std_logic;
signal usr_dto								: std_logic_vector(63 downto 0); 
		
COMPONENT Event_Builder is
  generic (	addr_offset_100G_eth	 	:integer := 0 );
 Port (
		usr_clk								: in std_logic;
		usr_rst_n							: in std_logic;
		usr_func_wr							: in std_logic_vector(16383 downto 0); 
		usr_wen								: in std_logic;
		usr_data_wr							: in std_logic_vector(63 downto 0); 
		
		usr_func_rd					      	: in std_logic_vector(16383 downto 0); 
		usr_rden					      	: in std_logic;
		usr_data_rd					      	: out std_logic_vector(63 downto 0);

		clock_fed							: in std_logic;
		
		clock_SR0							: in std_logic;
		SR0_DATA							: in std_logic_vector(127 downto 0);
		SR0_UCTRL							: in std_logic;
		SR0_WEN								: in std_logic;
		SR0_LFF								: out std_logic;
		 
		clock_SR1							: in std_logic;
		SR1_DATA							: in std_logic_vector(127 downto 0);
		SR1_UCTRL							: in std_logic;
		SR1_WEN								: in std_logic;
		SR1_LFF								: out std_logic;
		 		 
		ext_trigger							: in std_logic;
		ext_veto_out						: out std_logic;
		
		clock_Buffer						: in std_logic;
		UR0_read_data_for_packet			: in std_logic;
		UR0_ready_data_for_packet			: out std_logic;
		UR0_buffered_datao					: out std_logic_vector(127 downto 0);
		UR0_buffered_uctrlo					: out std_logic;
		UR1_read_data_for_packet			: in std_logic;
		UR1_ready_data_for_packet			: out std_logic;
		UR1_buffered_datao					: out std_logic_vector(127 downto 0);
		UR1_buffered_uctrlo					: out std_logic
 );
end COMPONENT; 

signal dto_slink_merger 					: std_logic_vector(63 downto 0);

signal SR0_read_data_for_packet				: std_logic;
signal SR0_ready_data_for_packet			: std_logic;
signal SR0_buffered_datao					: std_logic_vector(127 downto 0);
signal SR0_buffered_uctrlo					: std_logic;
  

component memory_blocks is
generic ( swapp : boolean := false;
		addr_offset_100G_eth	: integer := 0);
port (
	reset						: IN STD_LOGIC;
	CLOCK						: IN STD_LOGIC;
	
	-- INPUT DATA FROM SLINK 0-1
	Read_dt_from_SR				: OUT STD_LOGIC; -- used to read the intermediate FIFO
	Data_ready_from_SR			: IN STD_LOGIC;
	Data_i_from_SR				: IN STD_LOGIC_VECTOR(127 DOWNTO 0);
	Uctrl_i_from_SR				: IN STD_LOGIC;		-- active high
	
	-- OUTPUT DATA TO Memory Stream 
	Data_o_to_UR				: OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
	Size_ready_to_UR			: OUT STD_LOGIC_VECTOR(10 DOWNTO 0); -- SIZE READY TO BE TRANSFERED (IF 0x000 AND NOT Data_ready_to_UR (4kBYTES)
	Data_ready_to_UR			: OUT STD_LOGIC; -- DATA IN fifo
	Read_data_for_UR			: IN STD_LOGIC;
	
	-- PARAMETERS READABLE BY PCI SLAVE ACCESS
	TRIGGER						: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
	FED							: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
	BX							: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
	Incr_evt					: OUT STD_LOGIC;
	
	LATCH_WC_BX					: OUT STD_LOGIC;
	ENA_LATCH					: IN STD_LOGIC;
	WC_histo					: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
	BX_histo					: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
	
	status_debug				: OUT STD_LOGIC_VECTOR(31 downto 0);
	internal_LFF				: OUT STD_LOGIC
	);
end component;

signal data_for_UR0_mem						: std_logic_vector(127 downto 0);
signal size_for_UR0_mem						: std_logic_vector(10 downto 0);
signal ready_for_UR0_mem					: std_logic;		
signal read_for_UR0_mem						: std_logic;
		 
		
COMPONENT data_manager_v2 is
  generic (	addr_offset_100G_eth	 	:integer := 0 );
  Port ( 
		usr_clk								: in std_logic;
		usr_rst_n							: in std_logic;
		usr_func_wr							: in std_logic_vector(16383 downto 0); 
		usr_wen								: in std_logic;
		usr_data_wr							: in std_logic_vector(63 downto 0); 
		
		usr_func_rd					      	: in std_logic_vector(16383 downto 0); 
		usr_rden					      	: in std_logic;
		usr_data_rd					      	: out std_logic_vector(63 downto 0); 
		resetp_counter_i                     : in std_logic;   

		clock_Buffer						: in std_logic;	
		data_for_UR_mem	    				: in std_logic_vector(127 downto 0);
		size_for_UR_mem	    				: in std_logic_vector(10 downto 0);
		ready_for_UR_mem					: in std_logic;
		read_for_UR_mem	    				: out std_logic;		
		
		clock_TCP							: in std_logic;
		
		UR_MEM_req_rd_blk					: in std_logic;
		UR_MEM_Size_request					: in std_logic_vector(31 downto 0); -- number of bytes
		UR_MEM_Add_request					: in std_logic_vector(31 downto 0); -- SND_SEQ# (in bytes)
		UR_MEM_request_done					: out std_logic;
											
		Packet_ready						: out std_logic; -- SND_SEQ# (in bytes)
		eth_data_CS16						: out std_logic_vector(15 downto 0);
		eth_ack_packet						: in std_logic;	-- pulse to read CS and release the place
		eth_rd_data							: in std_logic;
		PL_TX_lbus_A	   					: out tx_usr_record;
		PL_TX_lbus_B	   					: out tx_usr_record;
		PL_TX_lbus_C	   					: out tx_usr_record;
		PL_TX_lbus_D	   					: out tx_usr_record; -- bus of data packet
		last_data							: out std_logic;
		backpressure						: in std_logic;
											
		SND_NEW_A							: out std_logic_vector(31 downto 0); 	-- value of the latest data written in BIFI	WRITE POINTER
		SND_UNA_A							: in std_logic_vector(31 downto 0); 	-- latest data acknowledge						READ POINTER
		VALID_P_A							: in std_logic;
		resetp_FED0							: in std_logic 
  );
end COMPONENT;
 
signal dto_data_manager0     				: std_logic_vector(63 downto 0);

signal TCP_Buf0_SND_NEW						: std_logic_vector(31 downto 0);
signal TCP_Buf0_SND_UNA						: std_logic_vector(31 downto 0);
signal TCP_Buf0_VALID_P						: std_logic;
 
 
signal UR0_PL_TX_lbus_A	   					: tx_usr_record;
signal UR0_PL_TX_lbus_B	   					: tx_usr_record;
signal UR0_PL_TX_lbus_C	   					: tx_usr_record;
signal UR0_PL_TX_lbus_D	   					: tx_usr_record; 
signal UR0_blk_len							: std_logic_vector(15 downto 0):= (others => '0');
signal UR0_blk_CS				   			: std_logic_vector(15 downto 0):= (others => '0');
signal UR0_blk_end							: std_logic:= '0';
signal UR0_ack_packet						: std_logic:= '0'; 
signal UR0_rd_blk				   			: std_logic;
	  				   	 
signal UR0_MEM_req_rd_blk 				: std_logic;					-- request to read UR 
signal UR0_MEM_Add_request 				: std_logic_vector(31 downto 0);-- Address to start the read
signal UR0_MEM_Size_request 			: std_logic_vector(31 downto 0);-- length to read (in bytes)
signal UR0_MEM_request_done				: std_logic;						-- The Packet is read from UR (ended)
signal UR0_blk_ready					: std_logic;-- informs that the Data for the grant buffer is ready

signal TX_FIFO_Full						: std_logic;
		 
signal clock_fed                            : std_logic;		 
signal clock_UR                             : std_logic;		 
signal tx_usr_clk                           : std_logic;		 
signal resetn_mem                           : std_logic;		 
signal resetp_FED0                           : std_logic;		 
signal resetp_counter_out                    : std_logic;		 
		 
signal run_time				: boolean := true;	
	 
type myArrayType is array (number_of_events-1 downto 0) of integer;
signal Event_size : myArrayType; 

--****************************************************************************
--*********************    CODE    START     HERE		   *******************
--****************************************************************************
begin
----                                                             ...  <<<<<<<<< 2 < 1 < 0
--Event_size <= ((368),(548),(304),(2490),(320),(320),(272),(130),(192),(376));
Event_size <= ((100000),(100000),(100000),(100000),(100000),(100000),(100000),(100000),(100000),(100000));

Input_data:Event_Builder 
 generic map(	addr_offset_100G_eth	 	=> addr_offset_100G_eth0 )
 Port map(
		usr_clk								=> usr_clk			,
		usr_rst_n							=> rst_usr_clk_n	,
		usr_func_wr							=> usr_func_wr		, 
		usr_wen								=> usr_wen			,
		usr_data_wr							=> usr_data_wr		, 
											 
		usr_func_rd					      	=> usr_func_rd		, 
		usr_rden					      	=> usr_rden			,
		usr_data_rd					      	=> dto_slink_merger	,
											 
		clock_fed							=> clock_fed		,
		----------------------------------------------------------------									 
		-- DATA coming from the SlinkRocket								 
		clock_SR0							=> '0'		, -- First SlinkRocket
		SR0_DATA							=> (others => '0')			, 
		SR0_UCTRL							=> '0'		, 
		SR0_WEN								=> '0'			, 
		-- SR0_LFF								=> SR0_LFF			, 
																  
		clock_SR1							=> '0'		, -- Second SlinkRocket
		SR1_DATA							=> (others => '0')  , 
		SR1_UCTRL							=> '0' 		, 
		SR1_WEN								=> '0' 			, 
--		SR1_LFF								=> SR1_LFF			, 
																  
		ext_trigger							=> '0',--ext_trigger		, 
		-- ext_veto_out						=> ext_veto_out		, 
		----------------------------------------------------------------			
		-- DATA Merged from up to 6 SlinkRocket's or 6 Event generators
		clock_Buffer						=> clock_UR 				,
		UR0_read_data_for_packet			=> SR0_read_data_for_packet	,-- link to UltraRam 0
		UR0_ready_data_for_packet			=> SR0_ready_data_for_packet,
		UR0_buffered_datao					=> SR0_buffered_datao		,
		UR0_buffered_uctrlo					=> SR0_buffered_uctrlo		,
		UR1_read_data_for_packet			=> '0'	 -- link to UltraRam 1
--		UR1_ready_data_for_packet			=> SR1_ready_data_for_packet,
--		UR1_buffered_datao					=> SR1_buffered_datao		,
--		UR1_buffered_uctrlo					=> SR1_buffered_uctrlo		 
 );
 
 

--======================================================================================
		--**********************************************************************************************
		--Divide the fragment in blocks + DAQ Header  (max 2)
		--//////////////////////////////////////////////////////////////////////////////////////////////
		--  ONE 
		memory_block_i0:memory_blocks 
			generic map(	addr_offset_100G_eth 	=> 	addr_offset_100G_eth0 + addr_off_con0)
			port map(
				reset								=> resetn_mem				,--: IN STD_LOGIC;
				CLOCK								=> clock_UR					,--: IN STD_LOGIC;
				-- INPUT DATA FROM SLINK
				Read_dt_from_SR						=> SR0_read_data_for_packet	,--: OUT STD_LOGIC; -- used to read the intermediate FIFO
				Data_ready_from_SR					=> SR0_ready_data_for_packet,--: IN STD_LOGIC;
				Data_i_from_SR						=> SR0_buffered_datao		,--: IN STD_LOGIC_VECTOR(127 DOWNTO 0);
				Uctrl_i_from_SR						=> SR0_buffered_uctrlo		,--: IN STD_LOGIC;		-- active high
				-- OUTPUT DATA TO UltraRam 
				Data_o_to_UR						=> data_for_UR0_mem			,--: OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
				Size_ready_to_UR					=> size_for_UR0_mem			,--: OUT STD_LOGIC_VECTOR(10 DOWNTO 0); -- SIZE READY TO BE TRANSFERED (IF 0x000 AND NOT EMPTY (4kBYTES)
				Data_ready_to_UR					=> ready_for_UR0_mem		,--: OUT STD_LOGIC; -- DATA IN fifo
				Read_data_for_UR					=> read_for_UR0_mem			,--: IN STD_LOGIC;
				-- PARAMETERS READABLE BY PCI SLAVE ACCESS
				-- TRIGGER								=> ,--: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
				-- FED									=> ,--: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
				-- BX									=> ,--: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
				-- Incr_evt							: OUT STD_LOGIC;
				-- LATCH_WC_BX							: OUT STD_LOGIC;
				ENA_LATCH							=> '0'--: IN STD_LOGIC;
				-- WC_histo							: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
				-- BX_histo							: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
				-- status_debug						: OUT STD_LOGIC_VECTOR(31 downto 0);
				-- internal_LFF						: OUT STD_LOGIC
			);

		--**********************************************************************************************
		--  UltraRam implementation	
		Data_manager_i0:data_manager_v2 
			generic map(	addr_offset_100G_eth 	=> 	addr_offset_100G_eth0 + addr_off_con0)  
			Port map( 
				usr_clk								=> usr_clk			,--: in std_logic;
				usr_rst_n							=> rst_usr_clk_n	,--: in std_logic;
				usr_func_wr							=> usr_func_wr		,--: in std_logic_vector(16383 downto 0); 
				usr_wen								=> usr_wen			,--: in std_logic;
				usr_data_wr							=> usr_data_wr		,--: in std_logic_vector(63 downto 0); 
		 
				usr_func_rd					      	=> usr_func_rd		,--: in std_logic_vector(16383 downto 0); 
				usr_rden					      	=> usr_rden			,--: in std_logic;
				usr_data_rd					      	=> dto_data_manager0,--: out std_logic_vector(63 downto 0); 
				----------------------------------------------------------------				
				-- Data From memory block (from SR)
				clock_Buffer						=> clock_UR,--: in std_logic;	
				data_for_UR_mem	    				=> data_for_UR0_mem	,--: in std_logic_vector(127 downto 0);
				size_for_UR_mem	    				=> size_for_UR0_mem	,--: in std_logic_vector(10 downto 0);
				ready_for_UR_mem					=> ready_for_UR0_mem,--: in std_logic;
				read_for_UR_mem	    				=> read_for_UR0_mem	,--: out std_logic;		
		 
				clock_TCP							=> tx_usr_clk		,--: in std_logic;
				----------------------------------------------------------------
				-- request from Ethernet block
				UR_MEM_req_rd_blk					=> UR0_MEM_req_rd_blk,--: in std_logic;
				UR_MEM_Size_request					=> UR0_MEM_Size_request,--: in std_logic_vector(31 downto 0); -- number of bytes
				UR_MEM_Add_request					=> UR0_MEM_Add_request,--: in std_logic_vector(31 downto 0); -- SND_SEQ# (in bytes)
				UR_MEM_request_done					=> UR0_MEM_request_done,--: out std_logic;
													 
				Packet_ready						=> UR0_blk_ready	, -- SND_SEQ# (in bytes)
				eth_data_CS16						=> UR0_blk_CS		, 
				eth_ack_packet						=> UR0_ack_packet	, -- pulse to read CS and release the place
				eth_rd_data							=> UR0_rd_blk		,  
				PL_TX_lbus_A	   					=> UR0_PL_TX_lbus_A	,--  tx_usr_record;
				PL_TX_lbus_B	   					=> UR0_PL_TX_lbus_B	,--  tx_usr_record;
				PL_TX_lbus_C	   					=> UR0_PL_TX_lbus_C	,--  tx_usr_record;
				PL_TX_lbus_D	   					=> UR0_PL_TX_lbus_D	,--  tx_usr_record; -- bus of data packet
				last_data							=> UR0_blk_end		, 
				backpressure						=> TX_FIFO_Full		, 
												 
				SND_NEW_A							=> TCP_Buf0_SND_NEW	,--: out std_logic_vector(31 downto 0); 	-- value of the latest data written in BIFI	WRITE POINTER
				SND_UNA_A							=> TCP_Buf0_SND_UNA	,--: in std_logic_vector(31 downto 0); 	-- latest data acknowledge						READ POINTER
				VALID_P_A							=> TCP_Buf0_VALID_P	,--: in std_logic;
				resetp_FED0							=> resetp_FED0       , --: in std_logic; 
													 
				resetp_counter_i                     => resetp_counter_out
		  ); 
		--//////////////////////////////////////////////////////////////////////////////////////////////
		--  TWO 
		  
	
--*********************************************************	
--**********  Clock definition  ***************************
	
--clock_User
process
begin
	while run_time loop
		usr_clk	<= '1';
		wait for 8 ns;
		usr_clk	<= '0';
		wait for 8 ns;
	end loop;
	wait;
end process;
			
--clock_FED
process
begin
	while run_time loop
		clock_fed	<= '1';
		wait for 2.0 ns;
		clock_fed	<= '0';
		wait for 2.0 ns;
	end loop;
	wait;
end process;
		
--clock_UR
process
begin
	while run_time loop
		clock_UR	<= '1';
		wait for 2.5 ns;
		clock_UR	<= '0';
		wait for 2.5 ns;
	end loop;
	wait;
end process;
	
--clock_read
process
begin
	while run_time loop
		tx_usr_clk	<= '1';
		wait for 3 ns;
		tx_usr_clk	<= '0';
		wait for 3 ns;
	end loop;
	wait;
end process;	



process
begin
 
wait;
end process;

--**************************************************************************************
--  reset 
process
begin
	resetn_mem         <= '0';		 
	rst_usr_clk_n      <= '0';		 
	resetp_FED0         <= '1';		 
	resetp_counter_out  <= '1';
	wait for 300 ns;
	resetn_mem         <= '1';		 
	rst_usr_clk_n      <= '1';		 
	resetp_FED0         <= '0';		 
	resetp_counter_out  <= '0';
	wait;
end process;



--******************************************************************************************
---   PCIE control access
process
    variable data_to_write          : std_logic_vector(63 downto 0);
begin
usr_func_wr				<= (others => '0');	  
usr_wen					<= '0';
usr_data_wr				<= (others => '0');	--	 vector(63 downto 0);					
usr_func_rd				<= (others => '0');	  
usr_rden					<= '0';
--					func      data
wait for 8 us;	
    PCIe_func(USR_clk,addr_offset_slink_0  +SLINKRocket_FEDx_link_setup ,'0',x"0000000000000001"   ,usr_func_wr,usr_wen,usr_data_wr,usr_func_rd,usr_rden); --Enable emulator
--generate a fragment
    PCIe_func(USR_clk,addr_offset_evt_gen_0+Event_generator_evt_num	    ,'0',x"0000000044224422"   ,usr_func_wr,usr_wen,usr_data_wr,usr_func_rd,usr_rden); --write Event number
    PCIe_func(USR_clk,addr_offset_evt_gen_0+Event_generator_BX_SOURCE	,'0',x"00000000ABCDEFAC"   ,usr_func_wr,usr_wen,usr_data_wr,usr_func_rd,usr_rden); --write BX and Source #
    data_to_write   := std_logic_vector(TO_UNSIGNED(Event_size(0),64));
    PCIe_func(USR_clk,addr_offset_evt_gen_0+Event_generator_length	    ,'0',data_to_write         ,usr_func_wr,usr_wen,usr_data_wr,usr_func_rd,usr_rden); --write event length
    PCIe_func(USR_clk,addr_offset_evt_gen_0+Event_generator_control	    ,'0',x"0000000000000002"   ,usr_func_wr,usr_wen,usr_data_wr,usr_func_rd,usr_rden); --write Trigger gen
wait for 2 us;
    data_to_write   := std_logic_vector(TO_UNSIGNED(Event_size(1),64));
    PCIe_func(USR_clk,addr_offset_evt_gen_0+Event_generator_length	    ,'0',data_to_write         ,usr_func_wr,usr_wen,usr_data_wr,usr_func_rd,usr_rden); --write event length
    PCIe_func(USR_clk,addr_offset_evt_gen_0+Event_generator_control	    ,'0',x"0000000000000002"   ,usr_func_wr,usr_wen,usr_data_wr,usr_func_rd,usr_rden); --write Trigger gen
 
wait for 2 us;
    data_to_write   := std_logic_vector(TO_UNSIGNED(Event_size(2),64));
    PCIe_func(USR_clk,addr_offset_evt_gen_0+Event_generator_length	    ,'0',data_to_write         ,usr_func_wr,usr_wen,usr_data_wr,usr_func_rd,usr_rden); --write event length
    PCIe_func(USR_clk,addr_offset_evt_gen_0+Event_generator_control	    ,'0',x"0000000000000002"   ,usr_func_wr,usr_wen,usr_data_wr,usr_func_rd,usr_rden); --write Trigger gen
 
wait for 2 us;
    data_to_write   := std_logic_vector(TO_UNSIGNED(Event_size(3),64));
    PCIe_func(USR_clk,addr_offset_evt_gen_0+Event_generator_length	    ,'0',data_to_write         ,usr_func_wr,usr_wen,usr_data_wr,usr_func_rd,usr_rden); --write event length
    PCIe_func(USR_clk,addr_offset_evt_gen_0+Event_generator_control	    ,'0',x"0000000000000002"   ,usr_func_wr,usr_wen,usr_data_wr,usr_func_rd,usr_rden); --write Trigger gen
 
wait for 2 us;
    data_to_write   := std_logic_vector(TO_UNSIGNED(Event_size(4),64));
    PCIe_func(USR_clk,addr_offset_evt_gen_0+Event_generator_length	    ,'0',data_to_write         ,usr_func_wr,usr_wen,usr_data_wr,usr_func_rd,usr_rden); --write event length
    PCIe_func(USR_clk,addr_offset_evt_gen_0+Event_generator_control	    ,'0',x"0000000000000002"   ,usr_func_wr,usr_wen,usr_data_wr,usr_func_rd,usr_rden); --write Trigger gen
 
wait for 2 us;
    data_to_write   := std_logic_vector(TO_UNSIGNED(Event_size(5),64));
    PCIe_func(USR_clk,addr_offset_evt_gen_0+Event_generator_length	    ,'0',data_to_write         ,usr_func_wr,usr_wen,usr_data_wr,usr_func_rd,usr_rden); --write event length
    PCIe_func(USR_clk,addr_offset_evt_gen_0+Event_generator_control	    ,'0',x"0000000000000002"   ,usr_func_wr,usr_wen,usr_data_wr,usr_func_rd,usr_rden); --write Trigger gen
 
wait for 2 us;
    data_to_write   := std_logic_vector(TO_UNSIGNED(Event_size(6),64));
    PCIe_func(USR_clk,addr_offset_evt_gen_0+Event_generator_length	    ,'0',data_to_write         ,usr_func_wr,usr_wen,usr_data_wr,usr_func_rd,usr_rden); --write event length
    PCIe_func(USR_clk,addr_offset_evt_gen_0+Event_generator_control	    ,'0',x"0000000000000002"   ,usr_func_wr,usr_wen,usr_data_wr,usr_func_rd,usr_rden); --write Trigger gen
 
 
wait for 6 us;
    PCIe_func(USR_clk,addr_offset_evt_gen_0+Event_generator_length	    ,'0',x"0000000000110000"   ,usr_func_wr,usr_wen,usr_data_wr,usr_func_rd,usr_rden); --write event length
    PCIe_func(USR_clk,addr_offset_evt_gen_0+Event_generator_control	    ,'0',x"0000000000000002"   ,usr_func_wr,usr_wen,usr_data_wr,usr_func_rd,usr_rden); --write Trigger gen
 

wait;
end process;




--**************************************************************************************
-- read mechanicsm

process
begin
	UR0_MEM_req_rd_blk			<= '0';
	UR0_MEM_Size_request		<= (others => '0');
	UR0_MEM_Add_request			<= (others => '0');
	UR0_ack_packet				<= '0';
	UR0_rd_blk					<= '0'; 
	 -- we can read without acknoledging the data
	wait for 100 us;                       --  read from 0x0000000  size x*4*128-bit
	 
	wait until rising_edge(tx_usr_clk);    -- request to read a block of data
	UR0_MEM_req_rd_blk			<= '1';
	UR0_MEM_Size_request		<= x"00000300"; 
	UR0_MEM_Add_request         <= x"00000000";
	
	wait until UR0_MEM_request_done = '1';
	wait until rising_edge(tx_usr_clk);
	UR0_MEM_req_rd_blk			<= '0';
	
	wait until UR0_blk_ready = '1';        -- as soon as data is ready , we read them
	wait until rising_edge(tx_usr_clk);
    UR0_rd_blk	<= '1';
	
	wait until UR0_blk_end = '1';          -- until the end
	wait until rising_edge(tx_usr_clk);
	UR0_rd_blk	<= '0';
	
	wait until rising_edge(tx_usr_clk);    -- we acknowledge the packet of data
    UR0_ack_packet  <= '1';
	wait until rising_edge(tx_usr_clk);
    UR0_ack_packet  <= '0';
    
	wait for 500 ns;                       --  read from 0x0000010  size x*4*128-bit
	 
	-- request to read a block of data
	wait until rising_edge(tx_usr_clk);
	UR0_MEM_req_rd_blk			<= '1';
	UR0_MEM_Size_request		<= x"00000300"; 
	UR0_MEM_Add_request         <= x"00000010";
	
	wait until UR0_MEM_request_done = '1';
	wait until rising_edge(tx_usr_clk);
	UR0_MEM_req_rd_blk			<= '0';
	
	wait until UR0_blk_ready = '1';
	wait until rising_edge(tx_usr_clk);
    UR0_rd_blk	<= '1';
	
	wait until UR0_blk_end = '1';
	wait until rising_edge(tx_usr_clk);
	UR0_rd_blk	<= '0';
	
	wait until rising_edge(tx_usr_clk);
    UR0_ack_packet  <= '1';
	wait until rising_edge(tx_usr_clk);
    UR0_ack_packet  <= '0';

	wait for 500 ns;                       --  read from 0x0000020  size x*4*128-bit
	 
	-- request to read a block of data
	wait until rising_edge(tx_usr_clk);
	UR0_MEM_req_rd_blk			<= '1';
	UR0_MEM_Size_request		<= x"00000300"; 
	UR0_MEM_Add_request         <= x"00000020";
	
	wait until UR0_MEM_request_done = '1';
	wait until rising_edge(tx_usr_clk);
	UR0_MEM_req_rd_blk			<= '0';
	
	wait until UR0_blk_ready = '1';
	wait until rising_edge(tx_usr_clk);
    UR0_rd_blk	<= '1';
	
	wait until UR0_blk_end = '1';
	wait until rising_edge(tx_usr_clk);
	UR0_rd_blk	<= '0';
	
	wait until rising_edge(tx_usr_clk);
    UR0_ack_packet  <= '1';
	wait until rising_edge(tx_usr_clk);
    UR0_ack_packet  <= '0';

	wait for 500 ns;                       --  read from 0x0000030  size x*4*128-bit
	 
	-- request to read a block of data
	wait until rising_edge(tx_usr_clk);
	UR0_MEM_req_rd_blk			<= '1';
	UR0_MEM_Size_request		<= x"00000300"; 
	UR0_MEM_Add_request         <= x"00000030";
	
	wait until UR0_MEM_request_done = '1';
	wait until rising_edge(tx_usr_clk);
	UR0_MEM_req_rd_blk			<= '0';
	
	wait until UR0_blk_ready = '1';
	wait until rising_edge(tx_usr_clk);
    UR0_rd_blk	<= '1';
	
	wait until UR0_blk_end = '1';
	wait until rising_edge(tx_usr_clk);
	UR0_rd_blk	<= '0';
	
	wait until rising_edge(tx_usr_clk);
    UR0_ack_packet  <= '1';
	wait until rising_edge(tx_usr_clk);
    UR0_ack_packet  <= '0';

	wait for 500 ns;                       --  read from 0x0000040  size x*4*128-bit
	 
	-- request to read a block of data
	wait until rising_edge(tx_usr_clk);
	UR0_MEM_req_rd_blk			<= '1';
	UR0_MEM_Size_request		<= x"00000300"; 
	UR0_MEM_Add_request         <= x"00000040";
	
	wait until UR0_MEM_request_done = '1';
	wait until rising_edge(tx_usr_clk);
	UR0_MEM_req_rd_blk			<= '0';
	
	wait until UR0_blk_ready = '1';
	wait until rising_edge(tx_usr_clk);
    UR0_rd_blk	<= '1';
	
	wait until UR0_blk_end = '1';
	wait until rising_edge(tx_usr_clk);
	UR0_rd_blk	<= '0';
	
	wait until rising_edge(tx_usr_clk);
    UR0_ack_packet  <= '1';
	wait until rising_edge(tx_usr_clk);
    UR0_ack_packet  <= '0';

	wait for 500 ns;                       --  read from 0x0000000  size x*4*128-bit + 1*128-bit
	 
	-- request to read a block of data
	wait until rising_edge(tx_usr_clk);
	UR0_MEM_req_rd_blk			<= '1';
	UR0_MEM_Size_request		<= x"00000090"; 
	UR0_MEM_Add_request         <= x"00000000";
	
	wait until UR0_MEM_request_done = '1';
	wait until rising_edge(tx_usr_clk);
	UR0_MEM_req_rd_blk			<= '0';
	
	wait until UR0_blk_ready = '1';
	wait until rising_edge(tx_usr_clk);
    UR0_rd_blk	<= '1';
	
	wait until UR0_blk_end = '1';
	wait until rising_edge(tx_usr_clk);
	UR0_rd_blk	<= '0';
	
	wait until rising_edge(tx_usr_clk);
    UR0_ack_packet  <= '1';
	wait until rising_edge(tx_usr_clk);
    UR0_ack_packet  <= '0';

	wait for 500 ns;                       --  read from 0x0000010  size x*4*128-bit + 1*128-bit
	 
	-- request to read a block of data
	wait until rising_edge(tx_usr_clk);
	UR0_MEM_req_rd_blk			<= '1';
	UR0_MEM_Size_request		<= x"00000090"; 
	UR0_MEM_Add_request         <= x"00000010";
	
	wait until UR0_MEM_request_done = '1';
	wait until rising_edge(tx_usr_clk);
	UR0_MEM_req_rd_blk			<= '0';
	
	wait until UR0_blk_ready = '1';
	wait until rising_edge(tx_usr_clk);
    UR0_rd_blk	<= '1';
	
	wait until UR0_blk_end = '1';
	wait until rising_edge(tx_usr_clk);
	UR0_rd_blk	<= '0';
	
	wait until rising_edge(tx_usr_clk);
    UR0_ack_packet  <= '1';
	wait until rising_edge(tx_usr_clk);
    UR0_ack_packet  <= '0';

	wait for 500 ns;                       --  read from 0x0000020  size x*4*128-bit + 1*128-bit
	 
	-- request to read a block of data
	wait until rising_edge(tx_usr_clk);
	UR0_MEM_req_rd_blk			<= '1';
	UR0_MEM_Size_request		<= x"00000090"; 
	UR0_MEM_Add_request         <= x"00000020";
	
	wait until UR0_MEM_request_done = '1';
	wait until rising_edge(tx_usr_clk);
	UR0_MEM_req_rd_blk			<= '0';
	
	wait until UR0_blk_ready = '1';
	wait until rising_edge(tx_usr_clk);
    UR0_rd_blk	<= '1';
	
	wait until UR0_blk_end = '1';
	wait until rising_edge(tx_usr_clk);
	UR0_rd_blk	<= '0';
	
	wait until rising_edge(tx_usr_clk);
    UR0_ack_packet  <= '1';
	wait until rising_edge(tx_usr_clk);
    UR0_ack_packet  <= '0';

	wait for 500 ns;                       --  read from 0x0000030  size x*4*128-bit + 1*128-bit
	 
	-- request to read a block of data
	wait until rising_edge(tx_usr_clk);
	UR0_MEM_req_rd_blk			<= '1';
	UR0_MEM_Size_request		<= x"00000090"; 
	UR0_MEM_Add_request         <= x"00000030";
	
	wait until UR0_MEM_request_done = '1';
	wait until rising_edge(tx_usr_clk);
	UR0_MEM_req_rd_blk			<= '0';
	
	wait until UR0_blk_ready = '1';
	wait until rising_edge(tx_usr_clk);
    UR0_rd_blk	<= '1';
	
	wait until UR0_blk_end = '1';
	wait until rising_edge(tx_usr_clk);
	UR0_rd_blk	<= '0';
	
	wait until rising_edge(tx_usr_clk);
    UR0_ack_packet  <= '1';
	wait until rising_edge(tx_usr_clk);
    UR0_ack_packet  <= '0';

	wait for 500 ns;                       --  read from 0x0000000  size x*4*128-bit + 2*128-bit
	 
	-- request to read a block of data
	wait until rising_edge(tx_usr_clk);
	UR0_MEM_req_rd_blk			<= '1';
	UR0_MEM_Size_request		<= x"000000A0"; 
	UR0_MEM_Add_request         <= x"00000000";
	
	wait until UR0_MEM_request_done = '1';
	wait until rising_edge(tx_usr_clk);
	UR0_MEM_req_rd_blk			<= '0';
	
	wait until UR0_blk_ready = '1';
	wait until rising_edge(tx_usr_clk);
    UR0_rd_blk	<= '1';
	
	wait until UR0_blk_end = '1';
	wait until rising_edge(tx_usr_clk);
	UR0_rd_blk	<= '0';
	
	wait until rising_edge(tx_usr_clk);
    UR0_ack_packet  <= '1';
	wait until rising_edge(tx_usr_clk);
    UR0_ack_packet  <= '0';

	wait for 500 ns;                       --  read from 0x0000010  size x*4*128-bit + 2*128-bit
	 
	-- request to read a block of data
	wait until rising_edge(tx_usr_clk);
	UR0_MEM_req_rd_blk			<= '1';
	UR0_MEM_Size_request		<= x"000000A0"; 
	UR0_MEM_Add_request         <= x"00000010";
	
	wait until UR0_MEM_request_done = '1';
	wait until rising_edge(tx_usr_clk);
	UR0_MEM_req_rd_blk			<= '0';
	
	wait until UR0_blk_ready = '1';
	wait until rising_edge(tx_usr_clk);
    UR0_rd_blk	<= '1';
	
	wait until UR0_blk_end = '1';
	wait until rising_edge(tx_usr_clk);
	UR0_rd_blk	<= '0';
	
	wait until rising_edge(tx_usr_clk);
    UR0_ack_packet  <= '1';
	wait until rising_edge(tx_usr_clk);
    UR0_ack_packet  <= '0';

	wait for 500 ns;                       --  read from 0x0000020  size x*4*128-bit + 2*128-bit
	 
	-- request to read a block of data
	wait until rising_edge(tx_usr_clk);
	UR0_MEM_req_rd_blk			<= '1';
	UR0_MEM_Size_request		<= x"000000A0"; 
	UR0_MEM_Add_request         <= x"00000020";
	
	wait until UR0_MEM_request_done = '1';
	wait until rising_edge(tx_usr_clk);
	UR0_MEM_req_rd_blk			<= '0';
	
	wait until UR0_blk_ready = '1';
	wait until rising_edge(tx_usr_clk);
    UR0_rd_blk	<= '1';
	
	wait until UR0_blk_end = '1';
	wait until rising_edge(tx_usr_clk);
	UR0_rd_blk	<= '0';
	
	wait until rising_edge(tx_usr_clk);
    UR0_ack_packet  <= '1';
	wait until rising_edge(tx_usr_clk);
    UR0_ack_packet  <= '0';

	wait for 500 ns;                       --  read from 0x0000030  size x*4*128-bit + 2*128-bit
	 
	-- request to read a block of data
	wait until rising_edge(tx_usr_clk);
	UR0_MEM_req_rd_blk			<= '1';
	UR0_MEM_Size_request		<= x"000000A0"; 
	UR0_MEM_Add_request         <= x"00000030";
	
	wait until UR0_MEM_request_done = '1';
	wait until rising_edge(tx_usr_clk);
	UR0_MEM_req_rd_blk			<= '0';
	
	wait until UR0_blk_ready = '1';
	wait until rising_edge(tx_usr_clk);
    UR0_rd_blk	<= '1';
	
	wait until UR0_blk_end = '1';
	wait until rising_edge(tx_usr_clk);
	UR0_rd_blk	<= '0';
	
	wait until rising_edge(tx_usr_clk);
    UR0_ack_packet  <= '1';
	wait until rising_edge(tx_usr_clk);
    UR0_ack_packet  <= '0';

	wait for 500 ns;                       --  read from 0x0000000  size x*4*128-bit + 3*128-bit
	 
	-- request to read a block of data
	wait until rising_edge(tx_usr_clk);
	UR0_MEM_req_rd_blk			<= '1';
	UR0_MEM_Size_request		<= x"000000B0"; 
	UR0_MEM_Add_request         <= x"00000000";
	
	wait until UR0_MEM_request_done = '1';
	wait until rising_edge(tx_usr_clk);
	UR0_MEM_req_rd_blk			<= '0';
	
	wait until UR0_blk_ready = '1';
	wait until rising_edge(tx_usr_clk);
    UR0_rd_blk	<= '1';
	
	wait until UR0_blk_end = '1';
	wait until rising_edge(tx_usr_clk);
	UR0_rd_blk	<= '0';
	
	wait until rising_edge(tx_usr_clk);
    UR0_ack_packet  <= '1';
	wait until rising_edge(tx_usr_clk);
    UR0_ack_packet  <= '0';

	wait for 500 ns;                       --  read from 0x0000010  size x*4*128-bit + 3*128-bit
	 
	-- request to read a block of data
	wait until rising_edge(tx_usr_clk);
	UR0_MEM_req_rd_blk			<= '1';
	UR0_MEM_Size_request		<= x"000000B0"; 
	UR0_MEM_Add_request         <= x"00000010";
	
	wait until UR0_MEM_request_done = '1';
	wait until rising_edge(tx_usr_clk);
	UR0_MEM_req_rd_blk			<= '0';
	
	wait until UR0_blk_ready = '1';
	wait until rising_edge(tx_usr_clk);
    UR0_rd_blk	<= '1';
	
	wait until UR0_blk_end = '1';
	wait until rising_edge(tx_usr_clk);
	UR0_rd_blk	<= '0';
	
	wait until rising_edge(tx_usr_clk);
    UR0_ack_packet  <= '1';
	wait until rising_edge(tx_usr_clk);
    UR0_ack_packet  <= '0';

	wait for 500 ns;                       --  read from 0x0000020  size x*4*128-bit + 3*128-bit
	 
	-- request to read a block of data
	wait until rising_edge(tx_usr_clk);
	UR0_MEM_req_rd_blk			<= '1';
	UR0_MEM_Size_request		<= x"000000B0"; 
	UR0_MEM_Add_request         <= x"00000020";
	
	wait until UR0_MEM_request_done = '1';
	wait until rising_edge(tx_usr_clk);
	UR0_MEM_req_rd_blk			<= '0';
	
	wait until UR0_blk_ready = '1';
	wait until rising_edge(tx_usr_clk);
    UR0_rd_blk	<= '1';
	
	wait until UR0_blk_end = '1';
	wait until rising_edge(tx_usr_clk);
	UR0_rd_blk	<= '0';
	
	wait until rising_edge(tx_usr_clk);
    UR0_ack_packet  <= '1';
	wait until rising_edge(tx_usr_clk);
    UR0_ack_packet  <= '0';

	wait for 500 ns;                       --  read from 0x0000030  size x*4*128-bit + 3*128-bit
	 
	-- request to read a block of data
	wait until rising_edge(tx_usr_clk);
	UR0_MEM_req_rd_blk			<= '1';
	UR0_MEM_Size_request		<= x"000000B0"; 
	UR0_MEM_Add_request         <= x"00000030";
	
	wait until UR0_MEM_request_done = '1';
	wait until rising_edge(tx_usr_clk);
	UR0_MEM_req_rd_blk			<= '0';
	
	wait until UR0_blk_ready = '1';
	wait until rising_edge(tx_usr_clk);
    UR0_rd_blk	<= '1';
	
	wait until UR0_blk_end = '1';
	wait until rising_edge(tx_usr_clk);
	UR0_rd_blk	<= '0';
	
	wait until rising_edge(tx_usr_clk);
    UR0_ack_packet  <= '1';
	wait until rising_edge(tx_usr_clk);
    UR0_ack_packet  <= '0';

	wait;
end process;

process
begin
    TX_FIFO_Full <= '0'; 

wait;
end process;

process
begin
	TCP_Buf0_SND_UNA	<= (others => '0');
	TCP_Buf0_VALID_P	<= '1';

    wait for 400 us;	
	TCP_Buf0_SND_UNA	<= x"00010000";
	
    wait for 12 us;	
	TCP_Buf0_SND_UNA	<= x"00020000";
	
	wait;
end process;

end Behavioral;
