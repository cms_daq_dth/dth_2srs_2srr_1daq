----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05.07.2019 10:24:56
-- Design Name: 
-- Module Name: DAQ_sim - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.interface.all;
use work.Proc.all;
use work.address_table.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity DAQ_wo_serdes_sim is
--  Port ( );
end DAQ_wo_serdes_sim;

architecture Behavioral of DAQ_wo_serdes_sim is

signal usr_clk							: std_logic;
signal usr_rst_n						: std_logic;
signal rst_usr_clk_n					: std_logic;
signal usr_func_wr						: std_logic_vector(16383 downto 0);
signal usr_wen							: std_logic;
signal usr_data_wr						: std_logic_vector(63 downto 0);
signal usr_be							: std_logic_vector(7 downto 0);                         
signal usr_func_rd						: std_logic_vector(16383 downto 0);
signal usr_rden							: std_logic;
signal usr_dto							: std_logic_vector(63 downto 0); 
signal usr_rd_val						: std_logic; 

signal clock_fed						: std_logic;
signal clock_UR							: std_logic;
 
signal QSFP_RX_P						: std_logic_vector(3 downto 0);
signal QSFP_RX_N						: std_logic_vector(3 downto 0);
signal QSFP_TX_P						: std_logic_vector(3 downto 0);
signal QSFP_TX_N						: std_logic_vector(3 downto 0);
		
signal SERDES_reset_p					: std_logic := '0'; 

signal TX_lbus							: tx_user_ift;
signal TX_pack_ready					: std_logic; 
signal rx_lbus 							: rx_user_ift;		 
		
signal run_sim							: boolean := true;

component DAQ_module_wo_serdes is
	generic (addr_offset_100G_eth		: integer := 0
			);
   Port ( 
	-- PCIe local interface
		usr_clk								: in std_logic;
		rst_usr_clk_n						: in std_logic;
		usr_func_wr							: in std_logic_vector(16383 downto 0); 
		usr_wen								: in std_logic;
		usr_data_wr							: in std_logic_vector(63 downto 0);
		usr_be								: in std_logic_vector(7 downto 0);
			
		usr_func_rd							: in std_logic_vector(16383 downto 0); 
		usr_rden							: in std_logic;
		usr_dto								: out std_logic_vector(63 downto 0);

		clock_SR0							: in std_logic;
		SR0_DATA							: in std_logic_vector(127 downto 0);
		SR0_UCTRL							: in std_logic;
		SR0_WEN								: in std_logic;
		SR0_LFF								: out std_logic;
		
		clock_SR1							: in std_logic;
		SR1_DATA							: in std_logic_vector(127 downto 0);
		SR1_UCTRL							: in std_logic;
		SR1_WEN								: in std_logic;
		SR1_LFF								: out std_logic;
		
		ext_trigger							: in std_logic;
		ext_veto_out						: out std_logic;		
		
		clock_fed							: in std_logic;
		clock_UR 							: in std_logic; 
		UR_rst_p							: in std_logic;
	-- QSFP connection
		rx_lbus								: in rx_user_ift;
		rx_usr_clk							: in std_logic;
		tx_lbus								: out tx_user_ift;
		tx_lbus_ready						: in std_logic;
		tx_usr_clk							: in std_logic;
	
	-- SERDES/MAC
		SERDES_reset_p						: in std_logic  		
	);		
end component;

COMPONENT resetp_resync is
port (
	aresetp				: in std_logic;
	clock				: in std_logic; 
	Resetp_sync			: out std_logic
	);
end COMPONENT;

signal rx_usr_clk							: std_logic;
signal tx_lbus_ready						: std_logic;
signal tx_usr_clk							: std_logic;
signal SERDES_reset_p_tc_usr_clk			: std_logic;
 
 
--*************************************************************************
--*************************************************************************
--*************************************************************************
begin
-- Clock usr_clk   62.5 MHz
process
begin
	while run_sim loop
		usr_clk	<= '0';
		wait for 8 ns;
		usr_clk	<= '1';
		wait for 8 ns;	
	end loop;
	wait;
end process;

-- Clock FED   200 MHz
process
begin
	while run_sim loop
		clock_fed	<= '0';
		wait for 0.25 ns;
		clock_fed	<= '1';
		wait for 0.25 ns;	
	end loop;
	wait;
end process;
 

-- Clock UR   333 MHz
process
begin
	while run_sim loop
		clock_UR	<= '0';
		wait for 0.25 ns;
		clock_UR	<= '1';
		wait for 0.25 ns;	
	end loop;
	wait;
end process;
 
-- Clock FED   322.265625 MHz
process
begin
	while run_sim loop
		rx_usr_clk	<= '0';
		tx_usr_clk	<= '1';
		wait for 1.551515151515 ns;
		--wait for 2.5 ns;
		rx_usr_clk	<= '1';
		tx_usr_clk	<= '0';
		wait for 1.551515151515 ns;	
		--wait for 2.5 ns;
	end loop;
	wait;
end process; 

i1:DAQ_module_wo_serdes 
	generic map(addr_offset_100G_eth	=> addr_offset_100G_eth0 )
   Port map( 
	-- PCIe local interface
		usr_clk								=> usr_clk,--: in std_logic;
		rst_usr_clk_n						=> rst_usr_clk_n	,--: in std_logic;
		usr_func_wr							=> usr_func_wr		,--: in std_logic_vector(16383 downto 0); 
		usr_wen								=> usr_wen			,--: in std_logic;
		usr_data_wr							=> usr_data_wr		,--: in std_logic_vector(63 downto 0);
		usr_be								=> usr_be			,--: in std_logic_vector(7 downto 0);
											 
		usr_func_rd							=> usr_func_rd	,--: in std_logic_vector(16383 downto 0); 
		usr_rden							=> usr_rden	,--: in std_logic;
		usr_dto								=> usr_dto		,--: out std_logic_vector(63 downto 0);
											 
		clock_SR0							=> '0',--: in std_logic;
		SR0_DATA							=> (others => '0'),--: in std_logic_vector(127 downto 0);
		SR0_UCTRL							=> '0',--: in std_logic;
		SR0_WEN								=> '0',--: in std_logic;
		-- SR0_LFF								=> ,--: out std_logic;
											 
		clock_SR1							=> '0',--: in std_logic;
		SR1_DATA							=> (others => '0'),--: in std_logic_vector(127 downto 0);
		SR1_UCTRL							=> '0',--: in std_logic;
		SR1_WEN								=> '0',--: in std_logic;
		-- SR1_LFF								=> ,--: out std_logic;
											 
		ext_trigger							=> '0',--: in std_logic;
		-- ext_veto_out						=> ,--: out std_logic;		
											 
		clock_fed							=> clock_fed,--: in std_logic;
		clock_UR 							=> clock_UR,--: in std_logic; 
		UR_rst_p							=> '0',--: in std_logic;
	-- QSFP connection                       
		rx_lbus								=> rx_lbus,
		rx_usr_clk							=> rx_usr_clk,
		tx_lbus								=> tx_lbus,
		tx_lbus_ready						=> tx_lbus_ready,
		tx_usr_clk							=> tx_usr_clk,
											 
	-- SERDES/MAC                            
		SERDES_reset_p						=>  '0' 	
	);		 

--************************************************************************************************
--**************************          Reset process           **********************************

process
begin
usr_rst_n				<= '0';
rst_usr_clk_n			<= '0';
SERDES_reset_p			<= '1';
wait for 30 ns;
usr_rst_n				<= '1';
rst_usr_clk_n			<= '1';
SERDES_reset_p			<= '1';

wait for 100 ms;
run_sim					<= false;
wait;
end process;
  
--************************************************************************************************
--***************************    Emulate the wait state frm the MAC IP core     ******************
process
begin
tx_lbus_ready	<= '1';
wait for 33 us;
wait until falling_edge(tx_usr_clk);
tx_lbus_ready 	<= '0';

wait for 150 ns;
wait until falling_edge(tx_usr_clk);
tx_lbus_ready	<= '1';

wait;
end process;
   
--************************************************************************************************
--**************************          RX  reply transceiver           **********************************
process
begin
  
RX_lbus(0).data_bus     <= x"00000000000000000000000000000000";
RX_lbus(0).sopckt       <= '0';
RX_lbus(0).data_ena     <= '0';
RX_lbus(0).eopckt       <= '0';
RX_lbus(0).err_pckt     <= '0';
RX_lbus(0).empty_pckt	<= "0000";
 
RX_lbus(1).data_bus     <= x"00000000000000000000000000000000";
RX_lbus(1).sopckt       <= '0';
RX_lbus(1).data_ena     <= '0';
RX_lbus(1).eopckt       <= '0';
RX_lbus(1).err_pckt     <= '0';
RX_lbus(1).empty_pckt	<= "0000";
 
RX_lbus(2).data_bus     <= x"00000000000000000000000000000000";
RX_lbus(2).sopckt       <= '0';
RX_lbus(2).data_ena     <= '0';
RX_lbus(2).eopckt       <= '0';
RX_lbus(2).err_pckt     <= '0';
RX_lbus(2).empty_pckt	<= "0000";

RX_lbus(3).data_bus     <= x"00000000000000000000000000000000";
RX_lbus(3).sopckt       <= '0';
RX_lbus(3).data_ena     <= '0';
RX_lbus(3).eopckt       <= '0';
RX_lbus(3).err_pckt     <= '0';
RX_lbus(3).empty_pckt	<= "0000";

--------------------------- ARP request-----------------------------------------------------
wait for 21 us; 
wait until falling_edge(rx_usr_clk);
RX_lbus(0).data_bus     <= x"ffffffffffff248a07b78e1e08060001";
RX_lbus(0).sopckt       <= '1';
RX_lbus(0).data_ena     <= '1';
RX_lbus(0).eopckt       <= '0';
RX_lbus(0).err_pckt     <= '0';
RX_lbus(0).empty_pckt	<= "0000";
 
RX_lbus(1).data_bus     <= x"080006040001248a07b78e1ec0a80164";
RX_lbus(1).sopckt       <= '0';
RX_lbus(1).data_ena     <= '1';
RX_lbus(1).eopckt       <= '0';
RX_lbus(1).err_pckt     <= '0';
RX_lbus(1).empty_pckt	<= "0000";
 
RX_lbus(2).data_bus     <= x"000000000000c0a80116000000000000";
RX_lbus(2).sopckt       <= '0';
RX_lbus(2).data_ena     <= '1';
RX_lbus(2).eopckt       <= '1';
RX_lbus(2).err_pckt     <= '0';
RX_lbus(2).empty_pckt	<= "0110";
 
RX_lbus(3).data_bus     <= x"00000000000000000000000000000000";
RX_lbus(3).sopckt       <= '0';
RX_lbus(3).data_ena     <= '0';
RX_lbus(3).eopckt       <= '0';
RX_lbus(3).err_pckt     <= '0';
RX_lbus(3).empty_pckt	<= "0000";

wait until falling_edge(rx_usr_clk);
RX_lbus(0).data_bus     <= x"00000000000000000000000000000000";
RX_lbus(0).sopckt       <= '0';
RX_lbus(0).data_ena     <= '0';
RX_lbus(0).eopckt       <= '0';
RX_lbus(0).err_pckt     <= '0';
RX_lbus(0).empty_pckt	<= "0000";

RX_lbus(1).data_bus     <= x"00000000000000000000000000000000";
RX_lbus(1).sopckt       <= '0';
RX_lbus(1).data_ena     <= '0';
RX_lbus(1).eopckt       <= '0';
RX_lbus(1).err_pckt     <= '0';
RX_lbus(1).empty_pckt	<= "0000";

RX_lbus(2).data_bus     <= x"00000000000000000000000000000000";
RX_lbus(2).sopckt       <= '0';
RX_lbus(2).data_ena     <= '0';
RX_lbus(2).eopckt       <= '0';
RX_lbus(2).err_pckt     <= '0';
RX_lbus(2).empty_pckt	<= "0000";

RX_lbus(3).data_bus     <= x"00000000000000000000000000000000";
RX_lbus(3).sopckt       <= '0';
RX_lbus(3).data_ena     <= '0';
RX_lbus(3).eopckt       <= '0';
RX_lbus(3).err_pckt     <= '0';
RX_lbus(3).empty_pckt	<= "0000";

--------------------------- ARP request-----------------------------------------------------
wait for 1 us;   
wait until falling_edge(rx_usr_clk);
RX_lbus(0).data_bus     <= x"ffffffffffff248a07b78e1e08060001";
RX_lbus(0).sopckt       <= '1';
RX_lbus(0).data_ena     <= '1';
RX_lbus(0).eopckt       <= '0';
RX_lbus(0).err_pckt     <= '0';
RX_lbus(0).empty_pckt	<= "0000";
 
RX_lbus(1).data_bus     <= x"080006040001248a07b78e1ec0a80164";
RX_lbus(1).sopckt       <= '0';
RX_lbus(1).data_ena     <= '1';
RX_lbus(1).eopckt       <= '0';
RX_lbus(1).err_pckt     <= '0';
RX_lbus(1).empty_pckt	<= "0000";
 
RX_lbus(2).data_bus     <= x"000000000000c0a80116000000000000";
RX_lbus(2).sopckt       <= '0';
RX_lbus(2).data_ena     <= '1';
RX_lbus(2).eopckt       <= '1';
RX_lbus(2).err_pckt     <= '0';
RX_lbus(2).empty_pckt	<= "0110";
 
RX_lbus(3).data_bus     <= x"00000000000000000000000000000000";
RX_lbus(3).sopckt       <= '0';
RX_lbus(3).data_ena     <= '0';
RX_lbus(3).eopckt       <= '0';
RX_lbus(3).err_pckt     <= '0';
RX_lbus(3).empty_pckt	<= "0000";

wait until falling_edge(rx_usr_clk);
RX_lbus(0).data_bus     <= x"00000000000000000000000000000000";
RX_lbus(0).sopckt       <= '0';
RX_lbus(0).data_ena     <= '0';
RX_lbus(0).eopckt       <= '0';
RX_lbus(0).err_pckt     <= '0';
RX_lbus(0).empty_pckt	<= "0000";
 
RX_lbus(1).data_bus     <= x"00000000000000000000000000000000";
RX_lbus(1).sopckt       <= '0';
RX_lbus(1).data_ena     <= '0';
RX_lbus(1).eopckt       <= '0';
RX_lbus(1).err_pckt     <= '0';
RX_lbus(1).empty_pckt	<= "0000";
 
RX_lbus(2).data_bus     <= x"00000000000000000000000000000000";
RX_lbus(2).sopckt       <= '0';
RX_lbus(2).data_ena     <= '0';
RX_lbus(2).eopckt       <= '0';
RX_lbus(2).err_pckt     <= '0';
RX_lbus(2).empty_pckt	<= "0000";
 
RX_lbus(3).data_bus     <= x"00000000000000000000000000000000";
RX_lbus(3).sopckt       <= '0';
RX_lbus(3).data_ena     <= '0';
RX_lbus(3).eopckt       <= '0';
RX_lbus(3).err_pckt     <= '0';
RX_lbus(3).empty_pckt	<= "0000";

--------------------------- TCP reply with SYNC and ACK-----------------------------------------------------
wait for 1 us;  --TCP reply with SYNC and ACK   PORT 0
wait until falling_edge(rx_usr_clk);
RX_lbus(0).data_bus     <= x"081205055233248a07b78e1e08004500";
RX_lbus(0).sopckt       <= '1';
RX_lbus(0).data_ena     <= '1';
RX_lbus(0).eopckt       <= '0';
RX_lbus(0).err_pckt     <= '0';
RX_lbus(0).empty_pckt	<= "0000";
 
RX_lbus(1).data_bus     <= x"0030000040004006b6fdc0a80164c0a8";
RX_lbus(1).sopckt       <= '0';
RX_lbus(1).data_ena     <= '1';
RX_lbus(1).eopckt       <= '0';
RX_lbus(1).err_pckt     <= '0';
RX_lbus(1).empty_pckt	<= "0000";
 
RX_lbus(2).data_bus     <= x"0116271027107e7bf1ac000000007012";
RX_lbus(2).sopckt       <= '0';
RX_lbus(2).data_ena     <= '1';
RX_lbus(2).eopckt       <= '0';
RX_lbus(2).err_pckt     <= '0';
RX_lbus(2).empty_pckt	<= "0000";
 
RX_lbus(3).data_bus     <= x"690083ed0000020423000103030B0000";
RX_lbus(3).sopckt       <= '0';
RX_lbus(3).data_ena     <= '1';
RX_lbus(3).eopckt       <= '1';
RX_lbus(3).err_pckt     <= '0';
RX_lbus(3).empty_pckt	<= "0010";


wait until falling_edge(rx_usr_clk);
RX_lbus(0).data_bus     <= x"00000000000000000000000000000000";
RX_lbus(0).sopckt       <= '0';
RX_lbus(0).data_ena     <= '0';
RX_lbus(0).eopckt       <= '0';
RX_lbus(0).err_pckt     <= '0';
RX_lbus(0).empty_pckt	<= "0000";
 
RX_lbus(1).data_bus     <= x"00000000000000000000000000000000";
RX_lbus(1).sopckt       <= '0';
RX_lbus(1).data_ena     <= '0';
RX_lbus(1).eopckt       <= '0';
RX_lbus(1).err_pckt     <= '0';
RX_lbus(1).empty_pckt	<= "0000";
 
RX_lbus(2).data_bus     <= x"00000000000000000000000000000000";
RX_lbus(2).sopckt       <= '0';
RX_lbus(2).data_ena     <= '0';
RX_lbus(2).eopckt       <= '0';
RX_lbus(2).err_pckt     <= '0';
RX_lbus(2).empty_pckt	<= "0000";
 
RX_lbus(3).data_bus     <= x"00000000000000000000000000000000";
RX_lbus(3).sopckt       <= '0';
RX_lbus(3).data_ena     <= '0';
RX_lbus(3).eopckt       <= '0';
RX_lbus(3).err_pckt     <= '0';
RX_lbus(3).empty_pckt	<= "0000";


--------------------------- TCP reply with SYNC and ACK-----------------------------------------------------
wait for 1 us;  --TCP reply with SYNC and ACK   PORT 1
wait until falling_edge(rx_usr_clk);
RX_lbus(0).data_bus     <= x"081205055233248a07b78e1e08004500";
RX_lbus(0).sopckt       <= '1';
RX_lbus(0).data_ena     <= '1';
RX_lbus(0).eopckt       <= '0';
RX_lbus(0).err_pckt     <= '0';
RX_lbus(0).empty_pckt	<= "0000";
 
RX_lbus(1).data_bus     <= x"0030000040004006b6fdc0a80164c0a8";
RX_lbus(1).sopckt       <= '0';
RX_lbus(1).data_ena     <= '1';
RX_lbus(1).eopckt       <= '0';
RX_lbus(1).err_pckt     <= '0';
RX_lbus(1).empty_pckt	<= "0000";
 
RX_lbus(2).data_bus     <= x"0116272027207e7bf1ac000000007012";
RX_lbus(2).sopckt       <= '0';
RX_lbus(2).data_ena     <= '1';
RX_lbus(2).eopckt       <= '0';
RX_lbus(2).err_pckt     <= '0';
RX_lbus(2).empty_pckt	<= "0000";
 
RX_lbus(3).data_bus     <= x"690083ed000002042300010303070000";
RX_lbus(3).sopckt       <= '0';
RX_lbus(3).data_ena     <= '1';
RX_lbus(3).eopckt       <= '1';
RX_lbus(3).err_pckt     <= '0';
RX_lbus(3).empty_pckt	<= "0010";


wait until falling_edge(rx_usr_clk);
RX_lbus(0).data_bus     <= x"00000000000000000000000000000000";
RX_lbus(0).sopckt       <= '0';
RX_lbus(0).data_ena     <= '0';
RX_lbus(0).eopckt       <= '0';
RX_lbus(0).err_pckt     <= '0';
RX_lbus(0).empty_pckt	<= "0000";
 
RX_lbus(1).data_bus     <= x"00000000000000000000000000000000";
RX_lbus(1).sopckt       <= '0';
RX_lbus(1).data_ena     <= '0';
RX_lbus(1).eopckt       <= '0';
RX_lbus(1).err_pckt     <= '0';
RX_lbus(1).empty_pckt	<= "0000";
 
RX_lbus(2).data_bus     <= x"00000000000000000000000000000000";
RX_lbus(2).sopckt       <= '0';
RX_lbus(2).data_ena     <= '0';
RX_lbus(2).eopckt       <= '0';
RX_lbus(2).err_pckt     <= '0';
RX_lbus(2).empty_pckt	<= "0000";
 
RX_lbus(3).data_bus     <= x"00000000000000000000000000000000";
RX_lbus(3).sopckt       <= '0';
RX_lbus(3).data_ena     <= '0';
RX_lbus(3).eopckt       <= '0';
RX_lbus(3).err_pckt     <= '0';
RX_lbus(3).empty_pckt	<= "0000";

--------------------------- TCP  ACK-----------------------------------------------------
wait for 170 us;  --TCP  ACK   PORT 0
wait until falling_edge(rx_usr_clk);
RX_lbus(0).data_bus     <= x"081205055233248a07b78e1e08004500";
RX_lbus(0).sopckt       <= '1';
RX_lbus(0).data_ena     <= '1';
RX_lbus(0).eopckt       <= '0';
RX_lbus(0).err_pckt     <= '0';
RX_lbus(0).empty_pckt	<= "0000";
 
RX_lbus(1).data_bus     <= x"0030000040004006b6fdc0a80164c0a8";
RX_lbus(1).sopckt       <= '0';
RX_lbus(1).data_ena     <= '1';
RX_lbus(1).eopckt       <= '0';
RX_lbus(1).err_pckt     <= '0';
RX_lbus(1).empty_pckt	<= "0000";
 
RX_lbus(2).data_bus     <= x"0116271027107e7bf1ad000d92107010";
RX_lbus(2).sopckt       <= '0';
RX_lbus(2).data_ena     <= '1';
RX_lbus(2).eopckt       <= '0';
RX_lbus(2).err_pckt     <= '0';
RX_lbus(2).empty_pckt	<= "0000";
 
RX_lbus(3).data_bus     <= x"690083ed000002042300010303070000";
RX_lbus(3).sopckt       <= '0';
RX_lbus(3).data_ena     <= '1';
RX_lbus(3).eopckt       <= '1';
RX_lbus(3).err_pckt     <= '0';
RX_lbus(3).empty_pckt	<= "0010";


wait until falling_edge(rx_usr_clk);
RX_lbus(0).data_bus     <= x"00000000000000000000000000000000";
RX_lbus(0).sopckt       <= '0';
RX_lbus(0).data_ena     <= '0';
RX_lbus(0).eopckt       <= '0';
RX_lbus(0).err_pckt     <= '0';
RX_lbus(0).empty_pckt	<= "0000";
 
RX_lbus(1).data_bus     <= x"00000000000000000000000000000000";
RX_lbus(1).sopckt       <= '0';
RX_lbus(1).data_ena     <= '0';
RX_lbus(1).eopckt       <= '0';
RX_lbus(1).err_pckt     <= '0';
RX_lbus(1).empty_pckt	<= "0000";
 
RX_lbus(2).data_bus     <= x"00000000000000000000000000000000";
RX_lbus(2).sopckt       <= '0';
RX_lbus(2).data_ena     <= '0';
RX_lbus(2).eopckt       <= '0';
RX_lbus(2).err_pckt     <= '0';
RX_lbus(2).empty_pckt	<= "0000";
 
RX_lbus(3).data_bus     <= x"00000000000000000000000000000000";
RX_lbus(3).sopckt       <= '0';
RX_lbus(3).data_ena     <= '0';
RX_lbus(3).eopckt       <= '0';
RX_lbus(3).err_pckt     <= '0';
RX_lbus(3).empty_pckt	<= "0000";
--------------------------- Ping request-----------------------------------------------------
--wait for 1 us;  --Ping request
--wait until falling_edge(rx_usr_clk); 
--RX_lbus(0).data_bus     <= x"081205055233248a07b78e1e08004500";
--RX_lbus(0).sopckt       <= '1';
--RX_lbus(0).data_ena     <= '1';
--RX_lbus(0).eopckt       <= '0';
--RX_lbus(0).err_pckt     <= '0';
--RX_lbus(0).empty_pckt	<= "0000";
 
--RX_lbus(1).data_bus     <= x"00545afd400040015be7c0a80164c0a8";
--RX_lbus(1).sopckt       <= '0';
--RX_lbus(1).data_ena     <= '1';
--RX_lbus(1).eopckt       <= '0';
--RX_lbus(1).err_pckt     <= '0';
--RX_lbus(1).empty_pckt	<= "0000";
 
--RX_lbus(2).data_bus     <= x"01100800409492600009a36dd25c0000";
--RX_lbus(2).sopckt       <= '0';
--RX_lbus(2).data_ena     <= '1';
--RX_lbus(2).eopckt       <= '0';
--RX_lbus(2).err_pckt     <= '0';
--RX_lbus(2).empty_pckt	<= "0000";
 
--RX_lbus(3).data_bus     <= x"0000ec64040000000000101112131415";
--RX_lbus(3).sopckt       <= '0';
--RX_lbus(3).data_ena     <= '1';
--RX_lbus(3).eopckt       <= '0';
--RX_lbus(3).err_pckt     <= '0';
--RX_lbus(3).empty_pckt	<= "0000";
--wait until falling_edge(rx_usr_clk);
--RX_lbus(0).data_bus     <= x"161718191a1b1c1d1e1f202122232425";
--RX_lbus(0).sopckt       <= '1';
--RX_lbus(0).data_ena     <= '1';
--RX_lbus(0).eopckt       <= '0';
--RX_lbus(0).err_pckt     <= '0';
--RX_lbus(0).empty_pckt	<= "0000";
 
--RX_lbus(1).data_bus     <= x"262728292a2b2c2d2e2f303132333435";
--RX_lbus(1).sopckt       <= '0';
--RX_lbus(1).data_ena     <= '1';
--RX_lbus(1).eopckt       <= '0';
--RX_lbus(1).err_pckt     <= '0';
--RX_lbus(1).empty_pckt	<= "0000";
 
--RX_lbus(2).data_bus     <= x"36370000000000000000000000000000";
--RX_lbus(2).sopckt       <= '0';
--RX_lbus(2).data_ena     <= '1';
--RX_lbus(2).eopckt       <= '1';
--RX_lbus(2).err_pckt     <= '0';
--RX_lbus(2).empty_pckt	<= "1110";
 
--RX_lbus(3).data_bus     <= x"00000000000000000000000000000000";
--RX_lbus(3).sopckt       <= '0';
--RX_lbus(3).data_ena     <= '0';
--RX_lbus(3).eopckt       <= '0';
--RX_lbus(3).err_pckt     <= '0';
--RX_lbus(3).empty_pckt	<= "0000";


--wait until falling_edge(rx_usr_clk);
--RX_lbus(0).data_bus     <= x"00000000000000000000000000000000";
--RX_lbus(0).sopckt       <= '0';
--RX_lbus(0).data_ena     <= '0';
--RX_lbus(0).eopckt       <= '0';
--RX_lbus(0).err_pckt     <= '0';
--RX_lbus(0).empty_pckt	<= "0000";
 
--RX_lbus(1).data_bus     <= x"00000000000000000000000000000000";
--RX_lbus(1).sopckt       <= '0';
--RX_lbus(1).data_ena     <= '0';
--RX_lbus(1).eopckt       <= '0';
--RX_lbus(1).err_pckt     <= '0';
--RX_lbus(1).empty_pckt	<= "0000";
 
--RX_lbus(2).data_bus     <= x"00000000000000000000000000000000";
--RX_lbus(2).sopckt       <= '0';
--RX_lbus(2).data_ena     <= '0';
--RX_lbus(2).eopckt       <= '0';
--RX_lbus(2).err_pckt     <= '0';
--RX_lbus(2).empty_pckt	<= "0000";
 
--RX_lbus(3).data_bus     <= x"00000000000000000000000000000000";
--RX_lbus(3).sopckt       <= '0';
--RX_lbus(3).data_ena     <= '0';
--RX_lbus(3).eopckt       <= '0';
--RX_lbus(3).err_pckt     <= '0';
--RX_lbus(3).empty_pckt	<= "0000";
--------------------------- -----------------------------------------------------
wait;
end process;  

--************************************************************************************************
--**************************           PCIe   accesss           **********************************
process
begin
usr_func_wr				<= (others => '0');	  
usr_wen					<= '0';
usr_data_wr				<= (others => '0');	--	 vector(63 downto 0);
usr_be					<= (others => '1');	 					
usr_func_rd				<= (others => '0');	  
usr_rden					<= '0';
--					func      data
wait for 200 ns;
PCIe_func(usr_clk,0,'0',x"0000000010000000",usr_func_wr,usr_wen,usr_data_wr,usr_be,usr_func_rd,usr_rden);

 
wait for 20 us;
--state	<= "Eth port          ";
PCIe_func(usr_clk,addr_off_con0+eth_100Gb_ctrl_port0_def,'0',x"0000000027102710",usr_func_wr,usr_wen,usr_data_wr,usr_be,usr_func_rd,usr_rden); -- ETH port0
wait for 15 ns;
PCIe_func(usr_clk,addr_off_con0+eth_100Gb_ctrl_port1_def,'0',x"0000000027202720",usr_func_wr,usr_wen,usr_data_wr,usr_be,usr_func_rd,usr_rden); -- ETH port1
wait for 15 ns;
PCIe_func(usr_clk,addr_off_con0+eth_100Gb_counter_reset,'0',x"0000000010000000",usr_func_wr,usr_wen,usr_data_wr,usr_be,usr_func_rd,usr_rden); -- reset counter
wait for 15 ns;
--state	<= "Eth IPsource      ";
PCIe_func(usr_clk,addr_off_con0+eth_100Gb_ctrl_IPS_def,'0',x"00000000c0a80116",usr_func_wr,usr_wen,usr_data_wr,usr_be,usr_func_rd,usr_rden); -- IP source
wait for 15 ns;
--state	<= "Eth IPdest        ";
PCIe_func(usr_clk,addr_off_con0+eth_100gb_ctrl_IPD_def,'0',x"00000000c0a80164",usr_func_wr,usr_wen,usr_data_wr,usr_be,usr_func_rd,usr_rden); -- IP destination
wait for 15 ns;
PCIe_func(usr_clk,addr_off_con0+eth_100Gb_ctrl_Init_def,'0',x"0000000000040400",usr_func_wr,usr_wen,usr_data_wr,usr_be,usr_func_rd,usr_rden); -- TCP WIN  + No_Delay

wait for 15 ns;
--state	<= "Eth ARPt          ";
PCIe_func(usr_clk,addr_off_con0+eth_100Gb_ETH_request_status,'0',x"0000000000000001",usr_func_wr,usr_wen,usr_data_wr,usr_be,usr_func_rd,usr_rden); -- ARP

wait for 2 us;
--state	<= "Eth OPEN          ";
PCIe_func(usr_clk,addr_off_con0+eth_100Gb_ETH_request_status,'0',x"0000000000000010",usr_func_wr,usr_wen,usr_data_wr,usr_be,usr_func_rd,usr_rden); -- TCP OPEN
wait for 1 us;
PCIe_func(usr_clk,addr_off_con0+eth_100Gb_ETH_request_status,'0',x"0000000000000100",usr_func_wr,usr_wen,usr_data_wr,usr_be,usr_func_rd,usr_rden); -- TCP OPEN


wait for 5 us;
PCIe_func(usr_clk,addr_offset_100G_eth0+addr_offset_slink_0+SLINKRocket_FEDx_link_setup,'0',x"0000000000000001",usr_func_wr,usr_wen,usr_data_wr,usr_be,usr_func_rd,usr_rden); --EVT0 select emulator
wait for 15 ns;
PCIe_func(usr_clk,addr_offset_100G_eth0+addr_offset_slink_1+SLINKRocket_FEDx_link_setup,'0',x"0000000000000001",usr_func_wr,usr_wen,usr_data_wr,usr_be,usr_func_rd,usr_rden); --EVT1 select emulator
wait for 15 ns;
PCIe_func(usr_clk,addr_offset_100G_eth0+addr_offset_evt_gen_0+Event_generator_threshold_ready,'0',x"0000000000000100",usr_func_wr,usr_wen,usr_data_wr,usr_be,usr_func_rd,usr_rden); -- EVT0 set threshold
wait for 15 ns;
PCIe_func(usr_clk,addr_offset_100G_eth0+addr_offset_evt_gen_0+Event_generator_threshold_busy,'0',x"0000000000000200",usr_func_wr,usr_wen,usr_data_wr,usr_be,usr_func_rd,usr_rden); -- EVT0 set threshold
wait for 15 ns;
PCIe_func(usr_clk,addr_offset_100G_eth0+addr_offset_evt_gen_1+Event_generator_threshold_ready,'0',x"0000000000000100",usr_func_wr,usr_wen,usr_data_wr,usr_be,usr_func_rd,usr_rden); -- EVT1 set threshold
wait for 15 ns;
PCIe_func(usr_clk,addr_offset_100G_eth0+addr_offset_evt_gen_1+Event_generator_threshold_busy,'0',x"0000000000000200",usr_func_wr,usr_wen,usr_data_wr,usr_be,usr_func_rd,usr_rden); -- EVT1 set threshold
wait for 15 ns;

PCIe_func(usr_clk,addr_offset_100G_eth0+addr_offset_evt_gen_0+Event_generator_length,'0',x"0000000000010000",usr_func_wr,usr_wen,usr_data_wr,usr_be,usr_func_rd,usr_rden); -- Fragment length
wait for 15 ns;
PCIe_func(usr_clk,addr_offset_100G_eth0+addr_offset_evt_gen_1+Event_generator_length,'0',x"0000000000010000",usr_func_wr,usr_wen,usr_data_wr,usr_be,usr_func_rd,usr_rden); -- Fragment length
wait for 15 ns;

PCIe_func(usr_clk,addr_offset_100G_eth0+addr_offset_evt_gen_0+Event_generator_evt_num,'0',x"0000000000000111",usr_func_wr,usr_wen,usr_data_wr,usr_be,usr_func_rd,usr_rden); -- First Event number
wait for 15 ns;
PCIe_func(usr_clk,addr_offset_100G_eth0+addr_offset_evt_gen_1+Event_generator_evt_num,'0',x"0000000000000111",usr_func_wr,usr_wen,usr_data_wr,usr_be,usr_func_rd,usr_rden); -- First Event number
wait for 15 ns;

PCIe_func(usr_clk,addr_offset_100G_eth0+addr_offset_evt_gen_0+Event_generator_BX_SOURCE,'0',x"0000567800001234",usr_func_wr,usr_wen,usr_data_wr,usr_be,usr_func_rd,usr_rden); --BX abd Source #
wait for 15 ns;
PCIe_func(usr_clk,addr_offset_100G_eth0+addr_offset_evt_gen_1+Event_generator_BX_SOURCE,'0',x"0000567800001234",usr_func_wr,usr_wen,usr_data_wr,usr_be,usr_func_rd,usr_rden); --BX abd Source #
wait for 15 ns;

PCIe_func(usr_clk,addr_offset_100G_eth0+addr_offset_evt_gen_0+Event_generator_seed,'0',x"efcbefcb12121212",usr_func_wr,usr_wen,usr_data_wr,usr_be,usr_func_rd,usr_rden); -- seed RND
wait for 15 ns;
PCIe_func(usr_clk,addr_offset_100G_eth0+addr_offset_evt_gen_1+Event_generator_seed,'0',x"efcbefcb12121212",usr_func_wr,usr_wen,usr_data_wr,usr_be,usr_func_rd,usr_rden); -- seed RND
wait for 15 ns;

PCIe_func(usr_clk,addr_offset_100G_eth0+addr_offset_evt_gen_0+Event_generator_time_btw_evt,'0',x"0000000000000002",usr_func_wr,usr_wen,usr_data_wr,usr_be,usr_func_rd,usr_rden); -- set time between event 
wait for 15 ns;
PCIe_func(usr_clk,addr_offset_100G_eth0+addr_offset_evt_gen_1+Event_generator_time_btw_evt,'0',x"0000000000000002",usr_func_wr,usr_wen,usr_data_wr,usr_be,usr_func_rd,usr_rden); -- set time between event 
wait for 15 ns;

--state	<= "Trigger Start     ";
PCIe_func(usr_clk,addr_offset_100G_eth0+addr_offset_evt_gen_0+Event_generator_control,'0',x"0000000000000001",usr_func_wr,usr_wen,usr_data_wr,usr_be,usr_func_rd,usr_rden); -- trigger (1)
wait for 500 ns;
PCIe_func(usr_clk,addr_offset_100G_eth0+addr_offset_evt_gen_1+Event_generator_control,'0',x"0000000000000001",usr_func_wr,usr_wen,usr_data_wr,usr_be,usr_func_rd,usr_rden); -- trigger (1)

wait for 1 us;
-- PCIe_func(usr_clk,addr_offset_100G_eth0+addr_offset_evt_gen_0+Event_generator_control,'0',x"0000000000000002",usr_func_wr,usr_wen,usr_data_wr,usr_be,usr_func_rd,usr_rden); -- trigger (1)
 






wait;
end process;


 
end Behavioral;
