----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 27.08.2019 09:17:43
-- Design Name: 
-- Module Name: BIFI_sim - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity BIFI_sim is
 generic (number_of_events		: integer := 10
 );
--  Port ( );
end BIFI_sim;

architecture Behavioral of BIFI_sim is

 

signal reset				: std_logic; 
signal clock_UR				: std_logic;
signal clock_w				: std_logic; 
signal SR_wr_ena			: std_logic; 
signal SR_data				: std_logic_vector(127 downto 0);
signal SR_EOF				: std_logic;
signal Almost_FULL			: std_logic;
signal clock_r				: std_logic;
signal req_read				: std_logic;
signal req_size				: std_logic_vector(31 downto 0); -- number of bytes
signal req_add_r			: std_logic_vector(31 downto 0); -- SND_SEQ# (in bytes)
signal Packet_ready			: std_logic;
signal request_done			: std_logic;
signal eth_data_CS16		: std_logic_vector(15 downto 0);
signal eth_ack_packet		: std_logic;	-- pulse to read CS and release the place
signal eth_rd_data			: std_logic;
signal eth_data_o			: std_logic_vector(511 downto 0);
signal eth_data_ena_o		: std_logic_vector(3 downto 0);
signal eth_last_dt_o		: std_logic;
signal SND_NEW_A			: std_logic_vector(31 downto 0); 	-- value of the lastest data written in BIFI	WRITE POINTER
signal SND_UNA_A			: std_logic_vector(31 downto 0); 	--lastest data acknoledge						READ POINTER
signal VALID_P_A			: std_logic;
signal reset_FED0			: std_logic;
signal BIFI_A_HF			: std_logic; 
signal BIFI_used_A			: std_logic_vector(63 downto 0) ;

 
signal run_time				: boolean := true;	
	
component BIFI_itf_2x is
 generic (Latency_read	: integer := 3  -- one latency to shift 128b word to match with the read_add requested
		); 
 port (
 
	---------------------------------------------
	-- interface BIFI  BIg FIfo
	---------------------------------------------
	-- input port
	reset				: in std_logic;
	 
	clock_UR			: in std_logic;
	--write data to BIFI
	clock_w				: in std_logic; 
	SR_wr_ena			: in std_logic; 
	SR_data				: in std_logic_vector(127 downto 0);
	SR_EOF				: in std_logic;
	Almost_FULL			: out std_logic;
	
	--read data from BIFI
	clock_r				: in std_logic;
	req_read			: in std_logic;
	req_size			: in std_logic_vector(31 downto 0); -- number of bytes
	req_add_r			: in std_logic_vector(31 downto 0); -- SND_SEQ# (in bytes)
	Packet_ready		: out std_logic;
	request_done		: out std_logic;
	
	eth_data_CS16		: out std_logic_vector(15 downto 0);
	eth_ack_packet		: in std_logic;	-- pulse to read CS and release the place
	eth_rd_data			: in std_logic;
	eth_data_o			: out std_logic_vector(511 downto 0);
	eth_data_ena_o		: out std_logic_vector(3 downto 0);
	eth_last_dt_o		: out std_logic;
	
	-- manage the empty full BIFI
		-- FED number 0
	SND_NEW_A			: out std_logic_vector(31 downto 0); 	-- value of the lastest data written in BIFI	WRITE POINTER
	SND_UNA_A			: in std_logic_vector(31 downto 0); 	--lastest data acknoledge						READ POINTER
	VALID_P_A			: in std_logic;
	reset_FED0			: in std_logic;
  
	BIFI_A_HF			: out std_logic; 
	BIFI_used_A			: out std_logic_vector(63 downto 0)  );
end Component;

type myArrayType is array (number_of_events-1 downto 0) of integer;
signal Event_size : myArrayType; 

--****************************************************************************
--*********************    CODE    START     HERE		   *******************
--****************************************************************************
begin

Event_size <= ((53),(65),(48),(89),(50),(30),(47),(25),(12),(56));

i1:BIFI_itf_2x  
 port map(

	reset				=> reset			,
	clock_UR			=> clock_UR		,
	clock_w				=> clock_UR			,
	SR_wr_ena			=> SR_wr_ena		,
	SR_data				=> SR_data			,
	SR_EOF				=> SR_EOF			,
	Almost_FULL			=> Almost_FULL		,
	clock_r				=> clock_r			,
	req_read			=> req_read		,
	req_size			=> req_size		,		-- number of bytes
	req_add_r			=> req_add_r		,		-- SND_SEQ# (in bytes)
	Packet_ready		=> Packet_ready	,
	request_done		=> request_done	,
	eth_data_CS16		=> eth_data_CS16	,
	eth_ack_packet		=> eth_ack_packet	,			-- pulse to read CS and release the place
	eth_rd_data			=> eth_rd_data		,
	eth_data_o			=> eth_data_o		,
	eth_data_ena_o		=> eth_data_ena_o	,
	eth_last_dt_o		=> eth_last_dt_o	,
	SND_NEW_A			=> SND_NEW_A		,	-- value of the lastest data written in BIFI	WRITE POINTER
	SND_UNA_A			=> SND_UNA_A		,	--lastest data acknoledge						READ POINTER
	VALID_P_A			=> VALID_P_A		,
	reset_FED0			=> reset_FED0		,
	BIFI_A_HF			=> BIFI_A_HF		,
	BIFI_used_A			=> BIFI_used_A		 );
	
	
--clock_UR
process
begin
	while run_time loop
		clock_UR	<= '1';
		wait for 5 ns;
		clock_UR	<= '0';
		wait for 5 ns;
	end loop;
	wait;
end process;
	
--clock_read
process
begin
	while run_time loop
		clock_r	<= '1';
		wait for 3 ns;
		clock_r	<= '0';
		wait for 3 ns;
	end loop;
	wait;
end process;	



process
begin
	reset <= '0';
	wait for 30 ns;
	reset <= '1';
	
	wait for 100 us;
	run_time	<= false;
	wait;
end process;

-- generate events
process
variable Data_evt	: std_logic_vector(127 downto 0);
begin
	SR_wr_ena		<= '0';
	SR_data			<= x"00000000000000000000000000000000";
	Data_evt		:= x"00000000000010010000000000000001";
	SR_EOF			<= '0';
	
	wait for 300 ns;
	
	for I in number_of_events-1 downto 0 loop
		SR_wr_ena	<= '0';
		evt_gen:for J in 1 to Event_size(I)-1 loop
			SR_wr_ena	<= '1';
			SR_data		<= Data_evt;
			Data_evt	:= Data_evt + x"0000000000000000000000000000001";
			wait until rising_edge(clock_UR);	
		end loop;
		SR_EOF		<= '1';
		SR_data		<= Data_evt;
		Data_evt	:= Data_evt + x"0000000000000000000000000000001";
			
		wait until rising_edge(clock_UR);	
		SR_EOF 		<= '0';
		SR_wr_ena	<= '0';
	wait for 1 us;
	Data_evt	:= Data_evt + x"00000000000010010000000000000000"; 
	end loop;
	
	wait;
end process;



-- read mechanicsm

process
variable  already_read : std_logic_vector(31 downto 0); -- record the pointre of the data already read
begin
	req_read			<= '0';
	req_size			<= (others => '0');
	req_add_r			<= (others => '0');
	eth_ack_packet		<= '0';
	eth_rd_data			<= '0';
	already_read		:= (others => '0');
	
	wait until SND_NEW_A - already_read /= x"00000000" ;
	wait for 3 us;
	wait until rising_edge(clock_r);
	req_read			<= '1';
	req_size			<= SND_NEW_A - already_read;
	req_add_r			<= already_read;
	
	wait until Packet_ready = '1';
	wait until rising_edge(clock_r);
	eth_rd_data	<= '1';
	
	wait until eth_last_dt_o = '1';
	wait until rising_edge(clock_r);
	eth_rd_data	<= '0';



	wait;
end process;



process
begin
	SND_UNA_A	<= (others => '0');
	VALID_P_A	<= '0';
	reset_FED0	<= '0';
	wait;
end process;

end Behavioral;
