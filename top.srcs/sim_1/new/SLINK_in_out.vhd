----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 24.10.2019 15:28:34
-- Design Name: 
-- Module Name: SLINK_in_out - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL; 
use ieee.numeric_std.all;
-- use work.interface.all;
use work.Proc.all;
use work.address_table.all;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity SLINK_in_out is
--  Port ( );
end SLINK_in_out;

architecture Behavioral of SLINK_in_out is

signal run_time 		: boolean := true;

signal Firefly_RX_p				: std_logic_vector(1 downto 0);
signal Firefly_RX_n				: std_logic_vector(1 downto 0);
signal Firefly_TX_p				: std_logic_vector(1 downto 0);
signal Firefly_TX_n				: std_logic_vector(1 downto 0);
signal resetp_counters          : std_logic;

COMPONENT SR_receiver_QUADone is
generic(SR0_offset                               : integer := 0;
        throughput								: string := "25.78125";
				--possible choices are 15.66 or 25.78125
		ref_clock								: string := "156.25";
				--possible choices are 156.25  or  322.265625 
		technology								: string := "GTY"
				-- possible choices are GTY or GTH
				);
 Port (
    usr_clk           	: IN STD_LOGIC;
    rst_usr_clk_n     	: IN STD_LOGIC;
    usr_func_wr       	: IN STD_LOGIC_VECTOR(16383 DOWNTO 0);
    usr_wen           	: IN STD_LOGIC;
    usr_data_wr       	: IN STD_LOGIC_VECTOR(63 DOWNTO 0);
    usr_func_rd       	: IN STD_LOGIC_VECTOR(16383 DOWNTO 0);
    usr_rden          	: IN STD_LOGIC;
    usr_dto_receiver   	: OUT STD_LOGIC_VECTOR(63 DOWNTO 0); 
    resetp_counters     : in std_logic;
	
	user_100MHz_clk		: in std_logic;
	
	clock_SR0			: OUT STD_LOGIC;	
	SR0_WEN  			: OUT STD_LOGIC;	
	SR0_UCTRL			: OUT STD_LOGIC;
	SR0_DATA 			: OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
	SR0_LFF  			: IN STD_LOGIC;

    SRI_ref_clkp    	: IN STD_LOGIC;	
    SRI_ref_clkn    	: IN STD_LOGIC;	
	SRI_gt_rxn_in     	: IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    SRI_gt_rxp_in     	: IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    SRI_gt_txn_out    	: OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    SRI_gt_txp_out    	: OUT STD_LOGIC_VECTOR(0 DOWNTO 0)
);	
end COMPONENT; 


signal usr_clk           	:STD_LOGIC;
signal rst_usr_clk_n     	:STD_LOGIC;
signal rst_usr_clk_n_sender     	:STD_LOGIC;
signal usr_func_wr       	:STD_LOGIC_VECTOR(16383 DOWNTO 0);
signal usr_wen           	:STD_LOGIC;
signal usr_data_wr       	:STD_LOGIC_VECTOR(63 DOWNTO 0);
signal usr_func_rd       	:STD_LOGIC_VECTOR(16383 DOWNTO 0);
signal usr_rden          	:STD_LOGIC;
signal usr_dto_receiver0  	: STD_LOGIC_VECTOR(63 DOWNTO 0);
signal usr_dto_receiver1  	: STD_LOGIC_VECTOR(63 DOWNTO 0);
 
signal user_100MHz_clk		:std_logic;
 
signal clock_SR			    : STD_LOGIC;	
signal SR_WEN  			    : STD_LOGIC;	
signal SR_UCTRL			    : STD_LOGIC;
signal SR_DATA 			    : STD_LOGIC_VECTOR(127 DOWNTO 0);
signal SR_LFF  			    :STD_LOGIC;
 
signal SRI_ref_clkp    	    :STD_LOGIC;	
signal SRI_ref_clkn    	    :STD_LOGIC;	
 

COMPONENT SR_transmit_QUADone is
generic(SR0_offset                               : integer := 0;

        throughput								: string := "25.78125";
				--possible choices are 15.66 or 25.78125
		ref_clock								: string := "156.25";
				--possible choices are 156.25  or  322.265625 
		technology								: string := "GTY"
				-- possible choices are GTY or GTH
				);
 Port ( 
	usr_clk           	: IN STD_LOGIC;
    rst_usr_clk_n     	: IN STD_LOGIC;
    usr_func_wr       	: IN STD_LOGIC_VECTOR(16383 DOWNTO 0);
    usr_wen           	: IN STD_LOGIC;
    usr_data_wr       	: IN STD_LOGIC_VECTOR(63 DOWNTO 0);
    usr_func_rd       	: IN STD_LOGIC_VECTOR(16383 DOWNTO 0);
    usr_rden          	: IN STD_LOGIC;
    usr_dto_receiver  	: OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
	
	user_100MHz_clk		: in std_logic;
	FED_clock			: in std_logic;

	SRO_ref_clkp    	: IN STD_LOGIC;	
    SRO_ref_clkn    	: IN STD_LOGIC;
	SRO_gt_rxn_in     	: IN STD_LOGIC_VECTOR(0 downto 0);
    SRO_gt_rxp_in     	: IN STD_LOGIC_VECTOR(0 downto 0);
    SRO_gt_txn_out    	: OUT STD_LOGIC_VECTOR(0 downto 0);
    SRO_gt_txp_out    	: OUT STD_LOGIC_VECTOR(0 downto 0);
    
    Rst_hrd_sim         : in std_logic		
	
 );
end COMPONENT;

signal Rst_hrd_sim          : std_logic;
signal FED_clock			:Std_logic;
							  
signal SRO_ref_clkp    	    :STD_LOGIC;	
signal SRO_ref_clkn    	    :STD_LOGIC;
 
COMPONENT DAQ_module_wo_eth is
	generic (addr_offset_100G_eth		: integer := 0);
   Port (  
	-- PCIe local interface
		usr_clk								: in std_logic;
		rst_usr_clk_n						: in std_logic;
		usr_func_wr							: in std_logic_vector(16383 downto 0); 
		usr_wen								: in std_logic;
		usr_data_wr							: in std_logic_vector(63 downto 0); 
			
		usr_func_rd							: in std_logic_vector(16383 downto 0); 
		usr_rden							: in std_logic;
		usr_dto								: out std_logic_vector(63 downto 0);

		clock_SR0							: in std_logic;
		SR0_DATA							: in std_logic_vector(127 downto 0);
		SR0_UCTRL							: in std_logic;
		SR0_WEN								: in std_logic;
		SR0_LFF								: out std_logic;
		
		SR0_read_data_for_packet			: in std_logic;
		SR0_ready_data_for_packet			: out std_logic;
		SR0_buffered_datao					: out std_logic_vector(127 downto 0);
		SR0_buffered_uctrlo					: out std_logic;
		
		ext_trigger							: in std_logic;
		ext_veto_out						: out std_logic;		
		
		clock_fed							: in std_logic;
		clock_UR 							: in std_logic; 
		UR_rst_p							: in std_logic

	);		
end COMPONENT;	

signal DAQ_DTO						        : std_logic_vector(63 downto 0);
signal SR0_read_data_for_packet			    : std_logic;
signal SR0_ready_data_for_packet			: std_logic;
signal SR0_buffered_datao					: std_logic_vector(127 downto 0);
signal SR0_buffered_uctrlo					: std_logic;
--******************************************************************
--**************    CODE   STARFT    HERE         ******************
--******************************************************************
begin
 
--*******************
--  Clock definition 
--*******************
--
process
begin
	while run_time loop
		usr_clk	<= '1';
--		wait for 8 ns;
		wait for 5 ns;
		usr_clk	<= '0';
--		wait for 8 ns;
		wait for 5 ns;
	end loop;
wait;
end process; 


process
begin
	while run_time loop
		user_100MHz_clk	<= '1';
		wait for 5 ns;
		user_100MHz_clk	<= '0';
		wait for 5 ns;
	end loop;
wait;
end process; 

process
begin
	while run_time loop
		FED_clock	<= '1';
		wait for 2 ns;
		FED_clock	<= '0';
		wait for 2 ns;
	end loop;
wait;
end process; 

process
begin
	while run_time loop
		SRI_ref_clkp	<= '1';
		SRI_ref_clkn	<= '0';
		wait for 3.2 ns;
		SRI_ref_clkp	<= '0';
		SRI_ref_clkn	<= '1';
		wait for 3.2 ns;
	end loop;
wait;
end process; 
 
process
begin
	while run_time loop
		SRO_ref_clkp	<= '1';
		SRO_ref_clkn	<= '0';
		wait for 3.2 ns;
		SRO_ref_clkp	<= '0';
		SRO_ref_clkn	<= '1';
		wait for 3.2 ns;
	end loop;
wait;
end process; 

Receiver_i1:SR_receiver_QUADone
generic map(SR0_offset           => addr_offset_slink_0)
 Port map(
    usr_clk           	=> usr_clk         	    ,
    rst_usr_clk_n     	=> rst_usr_clk_n   	    ,
    usr_func_wr       	=> usr_func_wr     	    ,
    usr_wen           	=> usr_wen         	    ,
    usr_data_wr       	=> usr_data_wr     	    ,
    usr_func_rd       	=> usr_func_rd     	    ,
    usr_rden          	=> usr_rden        	    ,
    usr_dto_receiver  	=> usr_dto_receiver0	,
    resetp_counters     => resetp_counters      ,
					 
	user_100MHz_clk		=> user_100MHz_clk	    ,
					 
	clock_SR0			=> clock_SR	            ,
	SR0_WEN  			=> SR_WEN  	            ,
	SR0_UCTRL			=> SR_UCTRL	            ,
	SR0_DATA 			=> SR_DATA 	            ,
	SR0_LFF  			=> SR_LFF  	            ,
						 
    SRI_ref_clkp    	=> SRI_ref_clkp  	    ,
    SRI_ref_clkn    	=> SRI_ref_clkn  	    ,
	SRI_gt_rxn_in(0)  	=> Firefly_RX_n(0) 	    ,
    SRI_gt_rxp_in(0)  	=> Firefly_RX_p(0) 	    ,
    SRI_gt_txn_out(0)  	=> Firefly_TX_n(0)	    ,
    SRI_gt_txp_out(0)  	=> Firefly_TX_p(0)	   
);	


Sender_i2:SR_transmit_QUADone  
generic map(SR0_offset      => addr_offset_slinkS_0)
 Port map( 
	usr_clk           	=> usr_clk           ,
    rst_usr_clk_n     	=> rst_usr_clk_n     ,
    usr_func_wr       	=> usr_func_wr       ,
    usr_wen           	=> usr_wen           ,
    usr_data_wr       	=> usr_data_wr       ,
    usr_func_rd       	=> usr_func_rd       ,
    usr_rden          	=> usr_rden          ,
    usr_dto_receiver  	=> usr_dto_receiver1 ,
						   				
	user_100MHz_clk		=> user_100MHz_clk	 ,
	FED_clock			=> FED_clock		 ,
						   				
	SRO_ref_clkp    	=> SRO_ref_clkp      ,
    SRO_ref_clkn    	=> SRO_ref_clkn      ,
	SRO_gt_rxn_in (0)   => Firefly_RX_n(1)     ,
    SRO_gt_rxp_in (0)   => Firefly_RX_p(1)     ,
    SRO_gt_txn_out(0)	=> Firefly_TX_n(1)    ,
    SRO_gt_txp_out(0)	=> Firefly_TX_p(1)    ,
    Rst_hrd_sim         => Rst_hrd_sim
	
 );

 
--****************************************************
-- SERDES  lanes

    Firefly_RX_n(0)	 	<=  Firefly_TX_n(1);
    Firefly_RX_p(0)	 	<=  Firefly_TX_p(1);	
    Firefly_RX_n(1)	 	<=  Firefly_TX_n(0);
    Firefly_RX_p(1)	 	<=  Firefly_TX_p(0);

--****************************************************
DAQ_i1:DAQ_module_wo_eth   
   Port map(  
	-- PCIe local interface                  
		usr_clk								=> usr_clk        ,--: in std_logic;
		rst_usr_clk_n						=> rst_usr_clk_n  ,--: in std_logic;
		usr_func_wr							=> usr_func_wr    ,--: in std_logic_vector(16383 downto 0); 
		usr_wen								=> usr_wen        ,--: in std_logic;
		usr_data_wr							=> usr_data_wr    ,--: in std_logic_vector(63 downto 0); 
                                                   
		usr_func_rd							=> usr_func_rd       ,--: in std_logic_vector(16383 downto 0); 
		usr_rden							=> usr_rden,--: in std_logic;
		usr_dto								=> DAQ_DTO,--: out std_logic_vector(63 downto 0);
											 
		clock_SR0							=> clock_SR	,--: in std_logic;
		SR0_DATA							=> SR_DATA   	,--: in std_logic_vector(127 downto 0);
		SR0_UCTRL							=> SR_UCTRL	,--: in std_logic;
		SR0_WEN								=> SR_WEN	,--: in std_logic;
		SR0_LFF								=> SR_LFF  	,--: out std_logic;
										 
		SR0_read_data_for_packet			=> SR0_read_data_for_packet,--: in std_logic;
		SR0_ready_data_for_packet			=> SR0_ready_data_for_packet	,--: out std_logic;
		SR0_buffered_datao					=> SR0_buffered_datao			,--: out std_logic_vector(127 downto 0);
		SR0_buffered_uctrlo					=> SR0_buffered_uctrlo			,--: out std_logic;
											 
		ext_trigger							=> '0',--: in std_logic;
		-- ext_veto_out						=> ,--: out std_logic;		
											 
		clock_fed							=> FED_clock,--: in std_logic;
		clock_UR 							=> FED_clock,--: in std_logic; 
		UR_rst_p							=> '0'--: in std_logic;

	);		
	
	
process
begin
	SR0_read_data_for_packet	<= '0';
	while run_time loop
		wait until SR0_ready_data_for_packet = '1'  ;
			SR0_read_data_for_packet	<= '1'; 
		wait until  SR0_buffered_uctrlo = '1' and SR0_buffered_datao(127 downto 120) = x"AA";
			SR0_read_data_for_packet	<= '0'; 
	end loop;
	wait;
end process;	
 
--****************************************************

process
begin
    rst_usr_clk_n			<= '0'; 
    wait for 2 us;					-- Take care of the clock output from SERDES
--    rst_usr_clk_n	<= '0';
--    wait for 30 ns;
    rst_usr_clk_n			<= '1';
	 

wait;
end process;

--reset the transceiver core
process 
begin
    Rst_hrd_sim <= '0';
	wait for 2.5 us;
	wait until falling_edge(user_100MHz_clk) ;
	Rst_hrd_sim <= '1';
    wait for 150 ns;
    wait until falling_edge(user_100MHz_clk) ;
    Rst_hrd_sim <= '0';
    wait;
 end process; 

--************************************************************************************************
--**************************           PCIe   accesss           **********************************
process
begin
usr_func_wr				<= (others => '0');	  
usr_wen					<= '0';
usr_data_wr				<= (others => '0');	--	 vector(63 downto 0);					
usr_func_rd				<= (others => '0');	  
usr_rden					<= '0';
--					func      data
--Reset on Receiver side

 wait for 2 us;
-- PCIe_func(USR_clk,addr_offset_slink_0+SLINKRocket_rcv_serdes_control,'0',x"0000000000000001",usr_func_wr,usr_wen,usr_data_wr,usr_func_rd,usr_rden); --reset serdes
-- wait for 4 us;	
-- PCIe_func(USR_clk,addr_offset_slink_0+SLINKRocket_rcv_serdes_control,'0',x"0000000000000000",usr_func_wr,usr_wen,usr_data_wr,usr_func_rd,usr_rden); --reset serdes

--wait for 12 us; -- the generic reset pin should be done and finished
-- PCIe_func(USR_clk,addr_offset_slinkS_0+FED_SR_snd_serdes_analog,'0',x"0000000080000000",usr_func_wr,usr_wen,usr_data_wr,usr_func_rd,usr_rden); --reset serdes
-- wait for 60 ns;	
-- PCIe_func(USR_clk,addr_offset_slinkS_0+FED_SR_snd_serdes_analog,'0',x"0000000000000000",usr_func_wr,usr_wen,usr_data_wr,usr_func_rd,usr_rden); --reset serdes


-- wait for 26 us;	
-- -- resync seq number of SR
-- PCIe_func(usr_clk,Slinkrocket_resync_request+addr_offset_slink_0,'0',x"0000000000000000",usr_func_wr,usr_wen,usr_data_wr,usr_func_rd,usr_rden); -- Command DAQ
-- wait for 500 ns;
-- --DAQ on											 
-- PCIe_func(usr_clk,Slinkrocket_access_command+addr_offset_slink_0,'0',x"0000000080010006",usr_func_wr,usr_wen,usr_data_wr,usr_func_rd,usr_rden); -- Command DAQ
-- wait for 10 ns;
-- PCIe_func(usr_clk,Slinkrocket_access_data+addr_offset_slink_0   ,'0',x"0000000040000000",usr_func_wr,usr_wen,usr_data_wr,usr_func_rd,usr_rden); -- DAQ on
 
-- wait for 5 us; 
-- PCIe_func(usr_clk,event_generator_length+addr_offset_slinkS_0	,'0',x"0000000000000200",usr_func_wr,usr_wen,usr_data_wr,usr_func_rd,usr_rden); -- Event Length
-- wait for 10 ns;
-- PCIe_func(usr_clk,event_generator_BX_SOURCE+addr_offset_slinkS_0,'0',x"0000024a0ABC1234",usr_func_wr,usr_wen,usr_data_wr,usr_func_rd,usr_rden); -- BX number
-- wait for 10 ns;
-- PCIe_func(usr_clk,event_generator_seed+addr_offset_slinkS_0		,'0',x"0000000087878787",usr_func_wr,usr_wen,usr_data_wr,usr_func_rd,usr_rden); -- Seed Gen
-- wait for 10 ns;
-- PCIe_func(usr_clk,event_generator_evt_num+addr_offset_slinkS_0	,'0',x"0000000000000001",usr_func_wr,usr_wen,usr_data_wr,usr_func_rd,usr_rden); -- Event Number
-- wait for 10 ns;
-- PCIe_func(usr_clk,event_generator_control+addr_offset_slinkS_0	,'0',x"0000000000000002",usr_func_wr,usr_wen,usr_data_wr,usr_func_rd,usr_rden); -- Gen fragments
-- wait for 10 ns;



wait;
end process;








end Behavioral;
