----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 20.09.2019 11:44:06
-- Design Name: 
-- Module Name: MAC_serdes_sim - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.std_logic_unsigned.all;
use work.Proc.all;

use work.interface.all;
use work.address_table.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity MAC_serdes_sim is
--  Port ( );
end MAC_serdes_sim;

architecture Behavioral of MAC_serdes_sim is

component Hundred_gb_core is
 Generic(addr_offset_100G_eth		: integer := 0);
  Port (
		init_clk                       : in std_logic;
		Reset_p	               	       : in std_logic;
  
		rx_lbus                        : out rx_user_ift;
		mac_rx_clk					   : out std_logic;
		tx_lbus                        : in tx_user_ift;
		tx_lbus_ready				   : out std_logic;
		mac_tx_clk	                   : out std_logic;
	
		usr_clk						   : in std_logic;
		usr_rst_n					   : in std_logic;
		usr_func_wr					   : in std_logic_vector(16383 downto 0); 
		usr_wen						   : in std_logic;
		usr_data_wr				  	   : in std_logic_vector(63 downto 0);
		usr_be						   : in std_logic_vector(7 downto 0);
		                           
		usr_func_rd					   : in std_logic_vector(16383 downto 0); 
		usr_rden					   : in std_logic;
		usr_data_rd					   : out std_logic_vector(63 downto 0);
		usr_rd_val					   : out std_logic := '1';
		
		QSFP_TX4_N					   : out std_logic;
		QSFP_TX4_P                 	   : out std_logic;
		QSFP_TX3_N                 	   : out std_logic;
		QSFP_TX3_P                 	   : out std_logic;
		QSFP_TX2_N                 	   : out std_logic;
		QSFP_TX2_P                 	   : out std_logic;
		QSFP_TX1_N                 	   : out std_logic;
		QSFP_TX1_P                 	   : out std_logic;
	
		QSFP_RX4_P                 	   : in std_logic;
		QSFP_RX4_N                 	   : in std_logic;
		QSFP_RX3_P                 	   : in std_logic;
		QSFP_RX3_N                 	   : in std_logic;
		QSFP_RX2_P                 	   : in std_logic;
		QSFP_RX2_N                 	   : in std_logic;
		QSFP_RX1_P                 	   : in std_logic;
		QSFP_RX1_N                 	   : in std_logic;
		
		GT_ref_clock_p         		   : in std_logic;  
		GT_ref_clock_n         		   : in std_logic;
		
		status_out						: out std_logic_vector(31 downto 0):= x"00000000" 
  
   );
end component;

signal init_clk                         : std_logic;
signal Reset_p	               	        : std_logic;
signal rx_lbus                          : rx_user_ift;
signal mac_rx_clk					    : std_logic;
signal tx_lbus                          : tx_user_ift;
signal tx_lbus_ready				    : std_logic;
signal mac_tx_clk	                    : std_logic;
signal usr_clk						    : std_logic;
signal usr_rst_n					    : std_logic;
signal usr_func_wr					    : std_logic_vector(16383 downto 0); 
signal usr_wen						    : std_logic;
signal usr_data_wr				  	    : std_logic_vector(63 downto 0);
signal usr_be						    : std_logic_vector(7 downto 0);
signal usr_func_rd					    : std_logic_vector(16383 downto 0); 
signal usr_rden					        : std_logic;
signal usr_data_rd					    : std_logic_vector(63 downto 0);
signal usr_rd_val					    : std_logic := '1';
signal QSFP_TX4_N					    : std_logic;
signal QSFP_TX4_P                 	    : std_logic;
signal QSFP_TX3_N                 	    : std_logic;
signal QSFP_TX3_P                 	    : std_logic;
signal QSFP_TX2_N                 	    : std_logic;
signal QSFP_TX2_P                 	    : std_logic;
signal QSFP_TX1_N                 	    : std_logic;
signal QSFP_TX1_P                 	    : std_logic;
signal QSFP_RX4_P                 	    : std_logic;
signal QSFP_RX4_N                 	    : std_logic;
signal QSFP_RX3_P                 	    : std_logic;
signal QSFP_RX3_N                 	    : std_logic;
signal QSFP_RX2_P                 	    : std_logic;
signal QSFP_RX2_N                 	    : std_logic;
signal QSFP_RX1_P                 	    : std_logic;
signal QSFP_RX1_N                 	    : std_logic;
signal GT_ref_clock_p         		    : std_logic;  
signal GT_ref_clock_n         		    : std_logic;
signal status_out						: std_logic_vector(31 downto 0);

signal offset							: integer := 16#A000#;

signal run_time							: boolean := true; 

--**********************************************************************************
--**************************  CODE  START HERE    **********************************
--**********************************************************************************
begin

i1:Hundred_gb_core 
  Port map(
		init_clk                       	=> init_clk        ,
		Reset_p	               	       	=> Reset_p	        ,
		rx_lbus                        	=> rx_lbus         ,
		mac_rx_clk					   	=> mac_rx_clk		,
		tx_lbus                        	=> tx_lbus         ,
		tx_lbus_ready				   	=> tx_lbus_ready	,
		mac_tx_clk	                   	=> mac_tx_clk	    ,
		usr_clk						   	=> usr_clk			,
		usr_rst_n					   	=> usr_rst_n		,
		usr_func_wr					   	=> usr_func_wr		,
		usr_wen						   	=> usr_wen			,
		usr_data_wr				  	   	=> usr_data_wr		,
		usr_be						   	=> usr_be			,
		usr_func_rd					   	=> usr_func_rd		,
		usr_rden					   	=> usr_rden		,
		usr_data_rd					   	=> usr_data_rd		,
		usr_rd_val					   	=> usr_rd_val		,
		QSFP_TX4_N					   	=> QSFP_TX4_N		,
		QSFP_TX4_P                 	   	=> QSFP_TX4_P      ,
		QSFP_TX3_N                 	   	=> QSFP_TX3_N      ,
		QSFP_TX3_P                 	   	=> QSFP_TX3_P      ,
		QSFP_TX2_N                 	   	=> QSFP_TX2_N      ,
		QSFP_TX2_P                 	   	=> QSFP_TX2_P      ,
		QSFP_TX1_N                 	   	=> QSFP_TX1_N      ,
		QSFP_TX1_P                 	   	=> QSFP_TX1_P      ,
		QSFP_RX4_P                 	   	=> QSFP_RX4_P      ,
		QSFP_RX4_N                 	   	=> QSFP_RX4_N      ,
		QSFP_RX3_P                 	   	=> QSFP_RX3_P      ,
		QSFP_RX3_N                 	   	=> QSFP_RX3_N      ,
		QSFP_RX2_P                 	   	=> QSFP_RX2_P      ,
		QSFP_RX2_N                 	   	=> QSFP_RX2_N      ,
		QSFP_RX1_P                 	   	=> QSFP_RX1_P      ,
		QSFP_RX1_N                 	   	=> QSFP_RX1_N      ,
		GT_ref_clock_p         		   	=> GT_ref_clock_p  ,
		GT_ref_clock_n         		   	=> GT_ref_clock_n  ,
		status_out						=> status_out		 
  );
  
  
QSFP_RX4_P	<= QSFP_TX4_P;
QSFP_RX4_N	<= QSFP_TX4_N;
QSFP_RX3_P	<= QSFP_TX3_P;
QSFP_RX3_N	<= QSFP_TX3_N;
QSFP_RX2_P	<= QSFP_TX2_P;
QSFP_RX2_N	<= QSFP_TX2_N;
QSFP_RX1_P	<= QSFP_TX1_P;
QSFP_RX1_N	<= QSFP_TX1_N;

-- clock definition  
process
begin
	while run_time loop
		GT_ref_clock_p	<= '0';
		GT_ref_clock_n	<= '1';
		wait for 1551.515 ps;
		GT_ref_clock_p	<= '1';
		GT_ref_clock_n	<= '0';
		wait for 1551.515 ps;
	end loop;
	wait;
end process;   

process
begin
	while run_time loop
		init_clk	<= '1';
		usr_clk	<= '1';
		wait for 5000 ps;
		init_clk	<= '0';
		usr_clk	<= '0';
		wait for 5000 ps;
	end loop;
	wait;
end process;  

-- reset global settings
process
begin
Reset_p		<= '1';
usr_rst_n	<= '0';
wait for 120 ns;
Reset_p		<= '0';
usr_rst_n	<= '1';

wait for 1000 ms;
run_time <= false;
wait;
end process;




--PCI command
process
begin

usr_func_wr			<= (others => '0');--: in std_logic_vector(16383 downto 0); 
usr_wen				<= '0';--: in std_logic;
usr_data_wr			<= x"0000000000000000";--: in std_logic_vector(63 downto 0);
usr_be				<= "00000000";--: in std_logic_vector(7 downto 0);                        
usr_func_rd			<= (others => '0');--: in std_logic_vector(16383 downto 0); 
usr_rden			<= '0';--: in std_logic;


--Enable TX
wait for 200 ns;
PCIe_func(usr_clk,MAC100G_SERDES_AXI_Add,'0',     x"000000000000000C",usr_func_wr,usr_wen,usr_data_wr,usr_be,usr_func_rd,usr_rden);
wait for 200 ns;
PCIe_func(usr_clk,MAC100G_SERDES_AXI_Dti,'0', x"0000000000000001",usr_func_wr,usr_wen,usr_data_wr,usr_be,usr_func_rd,usr_rden);
wait for 200 ns;
PCIe_func(usr_clk,MAC100G_SERDES_AXI_ctrl,'0',x"00000000000F0001",usr_func_wr,usr_wen,usr_data_wr,usr_be,usr_func_rd,usr_rden);
--Enable RX
wait for 500 ns;
PCIe_func(usr_clk,MAC100G_SERDES_AXI_Add,'0',     x"0000000000000014",usr_func_wr,usr_wen,usr_data_wr,usr_be,usr_func_rd,usr_rden);
wait for 200 ns;
PCIe_func(usr_clk,MAC100G_SERDES_AXI_Dti,'0', x"0000000000000001",usr_func_wr,usr_wen,usr_data_wr,usr_be,usr_func_rd,usr_rden);
wait for 200 ns;
PCIe_func(usr_clk,MAC100G_SERDES_AXI_ctrl,'0',x"00000000000F0001",usr_func_wr,usr_wen,usr_data_wr,usr_be,usr_func_rd,usr_rden);






wait;
end process;



  
  
  


end Behavioral;
