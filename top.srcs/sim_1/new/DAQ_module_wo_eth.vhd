----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 21.04.2017 14:23:45
-- Design Name: 
-- Module Name: top - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.std_logic_unsigned.all;
use work.address_table.all;
use work.interface.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all; 

entity DAQ_module_wo_eth is
	generic (addr_offset_100G_eth		: integer := 0);
   Port ( 
	-- PCIe local interface
		usr_clk								: in std_logic;
		rst_usr_clk_n						: in std_logic;
		usr_func_wr							: in std_logic_vector(16383 downto 0); 
		usr_wen								: in std_logic;
		usr_data_wr							: in std_logic_vector(63 downto 0); 
			
		usr_func_rd							: in std_logic_vector(16383 downto 0); 
		usr_rden							: in std_logic;
		usr_dto								: out std_logic_vector(63 downto 0);

		clock_SR0							: in std_logic;
		SR0_DATA							: in std_logic_vector(127 downto 0);
		SR0_UCTRL							: in std_logic;
		SR0_WEN								: in std_logic;
		SR0_LFF								: out std_logic;
		
		SR0_read_data_for_packet			: in std_logic;
		SR0_ready_data_for_packet			: out std_logic;
		SR0_buffered_datao					: out std_logic_vector(127 downto 0);
		SR0_buffered_uctrlo					: out std_logic;
		
		ext_trigger							: in std_logic;
		ext_veto_out						: out std_logic;		
		
		clock_fed							: in std_logic;
		clock_UR 							: in std_logic; 
		UR_rst_p							: in std_logic

	);		
end DAQ_module_wo_eth;

--*///////////////////////////////////////////////////////////////////////////////
architecture Behavioral of DAQ_module_wo_eth is
  
attribute mark_debug : string;
  
COMPONENT Event_Builder is
  generic (	addr_offset_100G_eth	 	:integer := 0 );
 Port (
		usr_clk								: in std_logic;
		usr_rst_n							: in std_logic;
		usr_func_wr							: in std_logic_vector(16383 downto 0); 
		usr_wen								: in std_logic;
		usr_data_wr							: in std_logic_vector(63 downto 0); 
		
		usr_func_rd					      	: in std_logic_vector(16383 downto 0); 
		usr_rden					      	: in std_logic;
		usr_data_rd					      	: out std_logic_vector(63 downto 0);

		clock_fed							: in std_logic;
		
		clock_SR0							: in std_logic;
		SR0_DATA							: in std_logic_vector(127 downto 0);
		SR0_UCTRL							: in std_logic;
		SR0_WEN								: in std_logic;
		SR0_LFF								: out std_logic;
		 
		clock_SR1							: in std_logic;
		SR1_DATA							: in std_logic_vector(127 downto 0);
		SR1_UCTRL							: in std_logic;
		SR1_WEN								: in std_logic;
		SR1_LFF								: out std_logic;
		 		 
		ext_trigger							: in std_logic;
		ext_veto_out						: out std_logic;
		
		clock_Buffer						: in std_logic;
		UR0_read_data_for_packet			: in std_logic;
		UR0_ready_data_for_packet			: out std_logic;
		UR0_buffered_datao					: out std_logic_vector(127 downto 0);
		UR0_buffered_uctrlo					: out std_logic;
		UR1_read_data_for_packet			: in std_logic;
		UR1_ready_data_for_packet			: out std_logic;
		UR1_buffered_datao					: out std_logic_vector(127 downto 0);
		UR1_buffered_uctrlo					: out std_logic
 );
end COMPONENT; 

-- signal SR0_read_data_for_packet				: std_logic;
-- signal SR0_ready_data_for_packet			: std_logic;
-- signal SR0_buffered_datao					: std_logic_vector(127 downto 0);
-- signal SR0_buffered_uctrlo					: std_logic;
  

-- component memory_blocks is
-- generic ( swapp : boolean := true;
		-- addr_offset_100G_eth	: integer := 0);
-- port (
	-- reset						: IN STD_LOGIC;
	-- CLOCK						: IN STD_LOGIC;
	
	----INPUT DATA FROM SLINK 0-1
	-- Read_dt_from_SR				: OUT STD_LOGIC; -- used to read the intermediate FIFO
	-- Data_ready_from_SR			: IN STD_LOGIC;
	-- Data_i_from_SR				: IN STD_LOGIC_VECTOR(127 DOWNTO 0);
	-- Uctrl_i_from_SR				: IN STD_LOGIC;		-- active high
	
	----OUTPUT DATA TO Memory Stream 
	-- Data_o_to_UR				: OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
	-- Size_ready_to_UR			: OUT STD_LOGIC_VECTOR(10 DOWNTO 0); -- SIZE READY TO BE TRANSFERED (IF 0x000 AND NOT Data_ready_to_UR (4kBYTES)
	-- Data_ready_to_UR			: OUT STD_LOGIC; -- DATA IN fifo
	-- Read_data_for_UR			: IN STD_LOGIC;
	
	----PARAMETERS READABLE BY PCI SLAVE ACCESS
	-- TRIGGER						: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
	-- FED							: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
	-- BX							: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
	-- Incr_evt					: OUT STD_LOGIC;
	
	-- LATCH_WC_BX					: OUT STD_LOGIC;
	-- ENA_LATCH					: IN STD_LOGIC;
	-- WC_histo					: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
	-- BX_histo					: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
	
	-- status_debug				: OUT STD_LOGIC_VECTOR(31 downto 0);
	-- internal_LFF				: OUT STD_LOGIC
	-- );
-- end component;

-- signal data_for_UR0_mem						: std_logic_vector(127 downto 0);
-- signal size_for_UR0_mem						: std_logic_vector(10 downto 0);
-- signal ready_for_UR0_mem					: std_logic;		
-- signal read_for_UR0_mem						: std_logic;
		
-- signal data_for_UR1_mem						: std_logic_vector(127 downto 0);
-- signal size_for_UR1_mem						: std_logic_vector(10 downto 0);
-- signal ready_for_UR1_mem					: std_logic;		
-- signal read_for_UR1_mem						: std_logic;
	
 
COMPONENT resetp_resync is
port (
	aresetp				: in std_logic;
	clock				: in std_logic; 
	Resetp_sync			: out std_logic
	);
end COMPONENT;

signal resetp_mem							: std_logic;
signal resetn_mem							: std_logic;

signal rx_lbus								: rx_user_ift;
signal rx_usr_clk							: std_logic;
signal tx_lbus								: tx_user_ift;
signal tx_lbus_ready						: std_logic;
signal tx_usr_clk							: std_logic; 

signal TX_lbus_in							: tx_user_ift;
signal TX_lbus_wr							: std_logic;
signal TX_pack_ready						: std_logic;
signal TX_pckt_ready						: std_logic;
signal TX_FIFO_Full							: std_logic;

signal dto_serdes							: std_logic_vector(63 downto 0);
signal dto_eth								: std_logic_vector(63 downto 0);
signal dto_slink_merger						: std_logic_vector(63 downto 0);
signal dto_data_manager0					: std_logic_vector(63 downto 0); 
signal dto_data_manager1					: std_logic_vector(63 downto 0); 
 
--#############################################################################
-- Code start here
--#############################################################################
begin
 
resync_reset_i2:resetp_resync 
port map(
	aresetp				=>	UR_rst_p,
	clock				=>	clock_UR,
	Resetp_sync			=>	resetp_mem 
	); 

resetn_mem	<= not(resetp_mem);

--========================================================================================
--  SLINK and event generator (max 6)
Event_builder_i1:Event_Builder  
  generic map(	addr_offset_100G_eth	 	=> 	addr_offset_100G_eth)
 Port map(
		usr_clk								=> usr_clk			,
		usr_rst_n							=> rst_usr_clk_n	,
		usr_func_wr							=> usr_func_wr		, 
		usr_wen								=> usr_wen			,
		usr_data_wr							=> usr_data_wr		, 
											 
		usr_func_rd					      	=> usr_func_rd		, 
		usr_rden					      	=> usr_rden			,
		usr_data_rd					      	=> dto_slink_merger	,
											 
		clock_fed							=> clock_fed		,
		----------------------------------------------------------------									 
		-- DATA coming from the SlinkRocket								 
		clock_SR0							=> clock_SR0		, -- First SlinkRocket
		SR0_DATA							=> SR0_DATA			, 
		SR0_UCTRL							=> SR0_UCTRL		, 
		SR0_WEN								=> SR0_WEN			, 
		SR0_LFF								=> SR0_LFF			, 
																  
		clock_SR1							=> '0'		, -- Second SlinkRocket
		 SR1_DATA							=> x"00000000000000000000000000000000"  , 
		 SR1_UCTRL							=> '0' 		, 
		 SR1_WEN							=> '0' 			, 
--		SR1_LFF								=> '0'			, 
																  
		ext_trigger							=> '0'		, 
		-- ext_veto_out						=> ext_veto_out		, 
		----------------------------------------------------------------			
		-- DATA Merged from up to 6 SlinkRocket's or 6 Event generators
		clock_Buffer						=> clock_UR 				,
		UR0_read_data_for_packet			=> SR0_read_data_for_packet	,-- link to UltraRam 0
		UR0_ready_data_for_packet			=> SR0_ready_data_for_packet,
		UR0_buffered_datao					=> SR0_buffered_datao		,
		UR0_buffered_uctrlo					=> SR0_buffered_uctrlo		,
		 UR1_read_data_for_packet			=> '0'	--link to UltraRam 1
		-- UR1_ready_data_for_packet			=> SR1_ready_data_for_packet,
		-- UR1_buffered_datao					=> SR1_buffered_datao		,
		-- UR1_buffered_uctrlo					=> SR1_buffered_uctrlo		 
 );

--======================================================================================
		--**********************************************************************************************
		--Divide the fragment in blocks + DAQ Header  (max 2)
		--//////////////////////////////////////////////////////////////////////////////////////////////
		--  ONE 
		-- memory_block_i0:memory_blocks 
			-- generic map(	addr_offset_100G_eth 	=> 	addr_offset_100G_eth + addr_off_con0)
			-- port map(
				-- reset								=> resetn_mem				,--: IN STD_LOGIC;
				-- CLOCK								=> clock_UR					,--: IN STD_LOGIC;
				-- -- INPUT DATA FROM SLINK
				-- Read_dt_from_SR						=> SR0_read_data_for_packet	,--: OUT STD_LOGIC; -- used to read the intermediate FIFO
				-- Data_ready_from_SR					=> SR0_ready_data_for_packet,--: IN STD_LOGIC;
				-- Data_i_from_SR						=> SR0_buffered_datao		,--: IN STD_LOGIC_VECTOR(127 DOWNTO 0);
				-- Uctrl_i_from_SR						=> SR0_buffered_uctrlo		,--: IN STD_LOGIC;		-- active high
				-- -- OUTPUT DATA TO UltraRam 
				-- Data_o_to_UR						=> data_for_UR0_mem			,--: OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
				-- Size_ready_to_UR					=> size_for_UR0_mem			,--: OUT STD_LOGIC_VECTOR(10 DOWNTO 0); -- SIZE READY TO BE TRANSFERED (IF 0x000 AND NOT EMPTY (4kBYTES)
				-- Data_ready_to_UR					=> ready_for_UR0_mem		,--: OUT STD_LOGIC; -- DATA IN fifo
				-- Read_data_for_UR					=> read_for_UR0_mem			,--: IN STD_LOGIC;
				-- -- PARAMETERS READABLE BY PCI SLAVE ACCESS
				-- -- TRIGGER								=> ,--: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
				-- -- FED									=> ,--: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
				-- -- BX									=> ,--: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
				-- -- Incr_evt							: OUT STD_LOGIC;
				-- -- LATCH_WC_BX							: OUT STD_LOGIC;
				-- ENA_LATCH							=> '0'--: IN STD_LOGIC;
				-- -- WC_histo							: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
				-- -- BX_histo							: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
				-- -- status_debug						: OUT STD_LOGIC_VECTOR(31 downto 0);
				-- -- internal_LFF						: OUT STD_LOGIC
			-- );

		-- --**********************************************************************************************
		-- --  UltraRam implementation	
		-- Data_manager_i0:data_manager_v2 
			-- generic map(	addr_offset_100G_eth 	=> 	addr_offset_100G_eth + addr_off_con0)  
			-- Port map( 
				-- usr_clk								=> usr_clk			,--: in std_logic;
				-- usr_rst_n							=> rst_usr_clk_n	,--: in std_logic;
				-- usr_func_wr							=> usr_func_wr		,--: in std_logic_vector(16383 downto 0); 
				-- usr_wen								=> usr_wen			,--: in std_logic;
				-- usr_data_wr							=> usr_data_wr		,--: in std_logic_vector(63 downto 0); 
		 
				-- usr_func_rd					      	=> usr_func_rd		,--: in std_logic_vector(16383 downto 0); 
				-- usr_rden					      	=> usr_rden			,--: in std_logic;
				-- usr_data_rd					      	=> dto_data_manager0,--: out std_logic_vector(63 downto 0); 
				-- ----------------------------------------------------------------				
				-- -- Data From memory block (from SR)
				-- clock_Buffer						=> clock_UR,--: in std_logic;	
				-- data_for_UR_mem	    				=> data_for_UR0_mem	,--: in std_logic_vector(127 downto 0);
				-- size_for_UR_mem	    				=> size_for_UR0_mem	,--: in std_logic_vector(10 downto 0);
				-- ready_for_UR_mem					=> ready_for_UR0_mem,--: in std_logic;
				-- read_for_UR_mem	    				=> read_for_UR0_mem	,--: out std_logic;		
		 
				-- clock_TCP							=> tx_usr_clk		,--: in std_logic;
				-- ----------------------------------------------------------------
				-- -- request from Ethernet block
				-- UR_MEM_req_rd_blk					=> UR0_MEM_req_rd_blk,--: in std_logic;
				-- UR_MEM_Size_request					=> UR0_MEM_Size_request,--: in std_logic_vector(31 downto 0); -- number of bytes
				-- UR_MEM_Add_request					=> UR0_MEM_Add_request,--: in std_logic_vector(31 downto 0); -- SND_SEQ# (in bytes)
				-- UR_MEM_request_done					=> UR0_MEM_request_done,--: out std_logic;
													 
				-- Packet_ready						=> UR0_blk_ready	, -- SND_SEQ# (in bytes)
				-- eth_data_CS16						=> UR0_blk_CS		, 
				-- eth_ack_packet						=> UR0_ack_packet	, -- pulse to read CS and release the place
				-- eth_rd_data							=> UR0_rd_blk		,  
				-- PL_TX_lbus_A	   					=> UR0_PL_TX_lbus_A	,--  tx_usr_record;
				-- PL_TX_lbus_B	   					=> UR0_PL_TX_lbus_B	,--  tx_usr_record;
				-- PL_TX_lbus_C	   					=> UR0_PL_TX_lbus_C	,--  tx_usr_record;
				-- PL_TX_lbus_D	   					=> UR0_PL_TX_lbus_D	,--  tx_usr_record; -- bus of data packet
				-- last_data							=> UR0_blk_end		, 
				-- backpressure						=> TX_FIFO_Full		, 
												 
				-- SND_NEW_A							=> TCP_Buf0_SND_NEW	,--: out std_logic_vector(31 downto 0); 	-- value of the latest data written in BIFI	WRITE POINTER
				-- SND_UNA_A							=> TCP_Buf0_SND_UNA	,--: in std_logic_vector(31 downto 0); 	-- latest data acknowledge						READ POINTER
				-- VALID_P_A							=> TCP_Buf0_VALID_P	,--: in std_logic;
				-- resetp_FED0							=> reset_FED0       , --: in std_logic; 
													 
				-- resetp_counter_i                    => resetp_counter_out
		  -- ); 
		-- --//////////////////////////////////////////////////////////////////////////////////////////////
		-- --  TWO 
		-- memory_block_i1:memory_blocks 
			-- generic map(	addr_offset_100G_eth 	=> 	addr_offset_100G_eth + addr_off_con1)
			-- port map(
				-- reset								=> resetn_mem				,--: IN STD_LOGIC;
				-- CLOCK								=> clock_UR					,--: IN STD_LOGIC;
				-- -- INPUT DATA FROM SLINK
				-- Read_dt_from_SR						=> SR1_read_data_for_packet	,--: OUT STD_LOGIC; -- used to read the intermediate FIFO
				-- Data_ready_from_SR					=> SR1_ready_data_for_packet,--: IN STD_LOGIC;
				-- Data_i_from_SR						=> SR1_buffered_datao		,--: IN STD_LOGIC_VECTOR(127 DOWNTO 0);
				-- Uctrl_i_from_SR						=> SR1_buffered_uctrlo		,--: IN STD_LOGIC;		-- active high
				-- -- OUTPUT DATA TO UltraRam 
				-- Data_o_to_UR						=> data_for_UR1_mem			,--: OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
				-- Size_ready_to_UR					=> size_for_UR1_mem			,--: OUT STD_LOGIC_VECTOR(10 DOWNTO 0); -- SIZE READY TO BE TRANSFERED (IF 0x000 AND NOT EMPTY (4kBYTES)
				-- Data_ready_to_UR					=> ready_for_UR1_mem		,--: OUT STD_LOGIC; -- DATA IN fifo
				-- Read_data_for_UR					=> read_for_UR1_mem			,--: IN STD_LOGIC;
				-- -- PARAMETERS READABLE BY PCI SLAVE ACCESS
				-- -- TRIGGER								=> ,--: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
				-- -- FED									=> ,--: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
				-- -- BX									=> ,--: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
				-- -- Incr_evt							: OUT STD_LOGIC;
				-- -- LATCH_WC_BX							: OUT STD_LOGIC;
				-- ENA_LATCH							=> '0'--: IN STD_LOGIC;
				-- -- WC_histo							: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
				-- -- BX_histo							: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
				-- -- status_debug						: OUT STD_LOGIC_VECTOR(31 downto 0);
				-- -- internal_LFF						: OUT STD_LOGIC
			-- );

		-- --**********************************************************************************************
		-- --  UltraRam implementation	
		-- Data_manager_i1:data_manager_v2 
			-- generic map(	addr_offset_100G_eth 	=> 	addr_offset_100G_eth + addr_off_con1)  
			-- Port map( 
				-- usr_clk								=> usr_clk			,--: in std_logic;
				-- usr_rst_n							=> rst_usr_clk_n	,--: in std_logic;
				-- usr_func_wr							=> usr_func_wr		,--: in std_logic_vector(16383 downto 0); 
				-- usr_wen								=> usr_wen			,--: in std_logic;
				-- usr_data_wr							=> usr_data_wr		,--: in std_logic_vector(63 downto 0); 
		 
				-- usr_func_rd					      	=> usr_func_rd		,--: in std_logic_vector(16383 downto 0); 
				-- usr_rden					      	=> usr_rden			,--: in std_logic;
				-- usr_data_rd					      	=> dto_data_manager1,--: out std_logic_vector(63 downto 0); 
				-- ----------------------------------------------------------------				
				-- -- Data From memory block (from SR)
				-- clock_Buffer						=> clock_UR,--: in std_logic;	
				-- data_for_UR_mem	    				=> data_for_UR1_mem	,--: in std_logic_vector(127 downto 0);
				-- size_for_UR_mem	    				=> size_for_UR1_mem	,--: in std_logic_vector(10 downto 0);
				-- ready_for_UR_mem					=> ready_for_UR1_mem,--: in std_logic;
				-- read_for_UR_mem	    				=> read_for_UR1_mem	,--: out std_logic;		
		 
				-- clock_TCP							=> tx_usr_clk		,--: in std_logic;
				-- ----------------------------------------------------------------
				-- -- request from Ethernet block
				-- UR_MEM_req_rd_blk					=> UR1_MEM_req_rd_blk,--: in std_logic;
				-- UR_MEM_Size_request					=> UR1_MEM_Size_request,--: in std_logic_vector(31 downto 0); -- number of bytes
				-- UR_MEM_Add_request					=> UR1_MEM_Add_request,--: in std_logic_vector(31 downto 0); -- SND_SEQ# (in bytes)
				-- UR_MEM_request_done					=> UR1_MEM_request_done,--: out std_logic;
													 
				-- Packet_ready						=> UR1_blk_ready	, -- SND_SEQ# (in bytes)
				-- eth_data_CS16						=> UR1_blk_CS		, 
				-- eth_ack_packet						=> UR1_ack_packet	, -- pulse to read CS and release the place
				-- eth_rd_data							=> UR1_rd_blk		,  
				-- PL_TX_lbus_A	   					=> UR1_PL_TX_lbus_A	,--  tx_usr_record;
				-- PL_TX_lbus_B	   					=> UR1_PL_TX_lbus_B	,--  tx_usr_record;
				-- PL_TX_lbus_C	   					=> UR1_PL_TX_lbus_C	,--  tx_usr_record;
				-- PL_TX_lbus_D	   					=> UR1_PL_TX_lbus_D	,--  tx_usr_record; -- bus of data packet
				-- last_data							=> UR1_blk_end		, 
				-- backpressure						=> TX_FIFO_Full		, 
												 
				-- SND_NEW_A							=> TCP_Buf1_SND_NEW	,--: out std_logic_vector(31 downto 0); 	-- value of the latest data written in BIFI	WRITE POINTER
				-- SND_UNA_A							=> TCP_Buf1_SND_UNA	,--: in std_logic_vector(31 downto 0); 	-- latest data acknowledge						READ POINTER
				-- VALID_P_A							=> TCP_Buf1_VALID_P	,--: in std_logic;
				-- resetp_FED0							=> reset_FED1       , --: in std_logic; 
													 
				-- resetp_counter_i                    => resetp_counter_out
		  -- ); 		 

-- ETHx_MEM_request_done		<= ETH0_MEM_request_done;
--==========================================================================
--
 
   
usr_dto	<= dto_serdes  or dto_eth or dto_slink_merger or dto_data_manager0 or dto_data_manager1 ; 

end Behavioral;  