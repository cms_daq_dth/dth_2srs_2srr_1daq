LIBRARY ieee;
 
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;
 use work.interface.all;
 
entity top_tb is
end top_tb;

architecture behavioral of top_tb is
 
component  transf_pkt is
	port
	(
	CLOCK									: IN STD_LOGIC;
	RESET_p								: IN STD_LOGIC;
	-- Ethernet parameters to send data blocks
	PORT_D								: IN STD_LOGIC_VECTOR(15 DOWNTO 0);
	PORT_S								: IN STD_LOGIC_VECTOR(15 DOWNTO 0);
	MAC_Gateway							: IN STD_LOGIC_VECTOR(47 downto 0);
	MAC_D									: IN STD_LOGIC_VECTOR(47 downto 0);
	MAC_S									: IN STD_LOGIC_VECTOR(47 downto 0);
	IP_D									: IN STD_LOGIC_VECTOR(31 downto 0);
	IP_S									: IN STD_LOGIC_VECTOR(31 downto 0);	
	IP_Server							: IN STD_LOGIC_VECTOR(31 downto 0);
	IP_GATEWAY							: IN STD_LOGIC_VECTOR(31 downto 0);	
	IP_NETWORK							: IN STD_LOGIC_VECTOR(31 downto 0);
	NETWORK								: IN STD_LOGIC_VECTOR(31 downto 0);	
	IP_dst_and_IP_nwk_eq_Net_Dst	: OUT STD_LOGIC;
	SEQ_N									: IN STD_LOGIC_VECTOR(31 downto 0);
	ACK_N									: IN STD_LOGIC_VECTOR(31 downto 0);
  --request ARP 
	ARP_req								: IN STD_LOGIC;						-- request a ARP packet
	ARP_rply								: IN STD_LOGIC;      				-- request a ARP reply if at '1' a ARP request if at '0'
	ARP_probe							: IN STD_LOGIC;						-- probe_ARP looking for a same IP addess
	ARP_Dest								: IN STD_LOGIC;
	ARP_Gateway							: IN STD_LOGIC;
	MAC_D_ARP_reply					: IN STD_LOGIC_VECTOR(47 downto 0); -- From the ARP ETH request , we should reply to 
	IP_D_ARP_reply						: IN STD_LOGIC_VECTOR(31 downto 0);

	-- request PING
	PING_req								: IN STD_LOGIC;
	ICMP_prbl_req						: IN STD_LOGIC;			-- request q ICMP problem packet (PING with payload not aligned
	PING_HD_emp							: OUT STD_LOGIC; 			-- inform that the previous PING reply is sent out
	WR_DT_PING							: IN STD_LOGIC;			-- write & last_dt do a Ping reply request
	Last_Ping_DT						: IN STD_LOGIC;
	Data_ping							: IN STD_LOGIC_VECTOR(63 downto 0);		
	Reset_PING_FIFO					: IN std_logic;
	MAC_D_PING_reply					: IN STD_LOGIC_VECTOR(47 downto 0); -- for Ping and ARP (same origin)
	IP_D_PING_reply					: IN STD_LOGIC_VECTOR(31 downto 0);	

	TX_lbus_a							: OUT tx_usr_record;
	TX_lbus_b							: OUT tx_usr_record;
	TX_lbus_c							: OUT tx_usr_record;
	TX_lbus_d							: OUT tx_usr_record;
	TX_lbus_wr							: OUT std_logic;
	TX_FIFO_Full						: IN std_logic;
	TX_pack_ready						: out std_logic
	 );
end component;

signal CLOCK									:  STD_LOGIC;
signal RESET_p								   :  STD_LOGIC;
signal PORT_D								   :  STD_LOGIC_VECTOR(15 DOWNTO 0);
signal PORT_S								   :  STD_LOGIC_VECTOR(15 DOWNTO 0);
signal MAC_Gateway							:  STD_LOGIC_VECTOR(47 downto 0);
signal MAC_D									:  STD_LOGIC_VECTOR(47 downto 0);
signal MAC_S									:  STD_LOGIC_VECTOR(47 downto 0);
signal IP_D									   :  STD_LOGIC_VECTOR(31 downto 0);
signal IP_S									   :  STD_LOGIC_VECTOR(31 downto 0);	
signal IP_Server							   :  STD_LOGIC_VECTOR(31 downto 0);
signal IP_GATEWAY							   :  STD_LOGIC_VECTOR(31 downto 0);	
signal IP_NETWORK							   :  STD_LOGIC_VECTOR(31 downto 0);
signal NETWORK								   :  STD_LOGIC_VECTOR(31 downto 0);	
signal IP_dst_and_IP_nwk_eq_Net_Dst	   :  STD_LOGIC;
signal SEQ_N									:  STD_LOGIC_VECTOR(31 downto 0);
signal ACK_N									:  STD_LOGIC_VECTOR(31 downto 0);
signal ARP_req								   :  STD_LOGIC;						-- request a ARP packet
signal ARP_rply								:  STD_LOGIC;      				-- request a ARP reply if at '1' a ARP request if at '0'
signal ARP_probe							   :  STD_LOGIC;						-- probe_ARP looking for a same IP addess
signal ARP_Dest								:  STD_LOGIC;
signal ARP_Gateway							:  STD_LOGIC;
signal MAC_D_ARP_reply					   :  STD_LOGIC_VECTOR(47 downto 0); -- From the ARP ETH request , we should reply to 
signal IP_D_ARP_reply						:  STD_LOGIC_VECTOR(31 downto 0);
signal PING_req								:  STD_LOGIC;
signal ICMP_prbl_req						   :  STD_LOGIC;			-- request q ICMP problem packet (PING with payload not aligned
signal PING_HD_emp							:  STD_LOGIC; 			-- inform that the previous PING reply is sent out
signal WR_DT_PING							   :  STD_LOGIC;			-- write & last_dt do a Ping reply request
signal Last_Ping_DT						   :  STD_LOGIC;
signal Data_ping							   :  STD_LOGIC_VECTOR(63 downto 0);		
signal Reset_PING_FIFO					   :  std_logic;
signal MAC_D_PING_reply					   :  STD_LOGIC_VECTOR(47 downto 0); -- for Ping and ARP (same origin)
signal IP_D_PING_reply					   :  STD_LOGIC_VECTOR(31 downto 0);	
signal TX_lbus_a							   :  tx_usr_record;
signal TX_lbus_b							   :  tx_usr_record;
signal TX_lbus_c							   :  tx_usr_record;
signal TX_lbus_d							   :  tx_usr_record;
signal TX_lbus_wr							   :  std_logic;
signal TX_FIFO_Full						   :  std_logic;
signal TX_pack_ready						   :  std_logic;
	
SIGNAL SIM_RUN	: BOOLEAN := TRUE;

begin

tb:transf_pkt
port map (
	CLOCK									=> CLOCK									,
	RESET_p								=> RESET_p								,
	PORT_D								=> PORT_D								,
	PORT_S								=> PORT_S								,
	MAC_Gateway							=> MAC_Gateway							,
	MAC_D									=> MAC_D									,
	MAC_S									=> MAC_S									,
	IP_D									=> IP_D									,
	IP_S									=> IP_S									,
	IP_Server							=> IP_Server							,
	IP_GATEWAY							=> IP_GATEWAY							,
	IP_NETWORK							=> IP_NETWORK							,
	NETWORK								=> NETWORK								,
	IP_dst_and_IP_nwk_eq_Net_Dst	=> IP_dst_and_IP_nwk_eq_Net_Dst	,
	SEQ_N									=> SEQ_N									,
	ACK_N									=> ACK_N									,
   ARP_req								=> ARP_req								,
	ARP_rply								=> ARP_rply								,
	ARP_probe							=> ARP_probe							,
	ARP_Dest								=> ARP_Dest								,
	ARP_Gateway							=> ARP_Gateway							,
	MAC_D_ARP_reply					=> MAC_D_ARP_reply					,
	IP_D_ARP_reply						=> IP_D_ARP_reply						,
	PING_req								=> PING_req								,
	ICMP_prbl_req						=> ICMP_prbl_req						,
	PING_HD_emp							=> PING_HD_emp							,
	WR_DT_PING							=> WR_DT_PING							,
	Last_Ping_DT						=> Last_Ping_DT						,
	Data_ping							=> Data_ping							,
	Reset_PING_FIFO					=> Reset_PING_FIFO					,
	MAC_D_PING_reply					=> MAC_D_PING_reply					,
	IP_D_PING_reply					=> IP_D_PING_reply					,
	TX_lbus_a							=> TX_lbus_a							,
	TX_lbus_b							=> TX_lbus_b							,
	TX_lbus_c							=> TX_lbus_c							,
	TX_lbus_d							=> TX_lbus_d							,
	TX_lbus_wr							=> TX_lbus_wr							,
	TX_FIFO_Full						=> TX_FIFO_Full						,
	TX_pack_ready						=> TX_pack_ready						 
);

clk_b_l:process
begin
	while sim_run  loop
		CLOCK <= '0';
		wait for 5000 ps;
		CLOCK <= '1';
		wait for 5000 ps;
	end loop;
	wait;
end process;

 process
 begin
 RESET_p <= '1';
 wait for 110 ns;
 RESET_p <= '0';
 
 
 
 wait for 10 us;
 sim_run <= false;
 wait;
END PROCESS;


process
begin

PORT_D								<= x"1000";            
PORT_S								<= x"1000";            
MAC_Gateway							<= x"454545454545";    
MAC_D									<= x"121212121212";    
MAC_S									<= x"232323232323";    
IP_D									<= x"c0a80164";        
IP_S									<= x"c0a80112";        
IP_Server							<= x"00000000";        
IP_GATEWAY							<= x"00000000";        
IP_NETWORK							<= x"00000000";        
NETWORK								<= x"00000000";        
SEQ_N									<= x"00000000";        
ACK_N									<= x"00000000";        
                                                    --request ARP                  
ARP_req								<= '0';               	-- request a ARP packet
ARP_rply								<= '0';               	-- request a ARP reply if at '1' a ARP request if at '0'
ARP_probe							<= '0';               	-- probe_ARP looking for a same IP addess
ARP_Dest								<= '0';                
ARP_Gateway							<= '0';                
MAC_D_ARP_reply					<= x"121212121212";    -- From the ARP ETH request , we should reply to 
IP_D_ARP_reply						<= x"c0a80164";        
                              
                                                            
PING_req								<= '0';                
ICMP_prbl_req						<= '0';                	-- request q ICMP problem packet (PING with payload not aligned
WR_DT_PING							<= '0';                	-- write & last_dt do a Ping reply request
Last_Ping_DT						<= '0';                
Data_ping							<= x"0000000000000000"; 		
Reset_PING_FIFO					<= '0';                
MAC_D_PING_reply					<= x"121212121212";     -- for Ping and ARP (same origin)
IP_D_PING_reply					<= x"c0a80164";        	
                                                     
TX_FIFO_Full						<= '0';                


wait for  1 us;
wait until falling_edge(CLOCK);
WR_DT_PING							<= '1';
Data_ping							<= x"1212121212121212";	
wait until falling_edge(CLOCK);
Data_ping							<= x"3434343434343434";	
wait until falling_edge(CLOCK);
Data_ping							<= x"4545454545454545";
wait until falling_edge(CLOCK);
Data_ping							<= x"5656565656565656";
wait until falling_edge(CLOCK);
Data_ping							<= x"6767676767676767";
wait until falling_edge(CLOCK);
Data_ping							<= x"7878787878787878";
wait until falling_edge(CLOCK);
Data_ping							<= x"8989898989898989";
wait until falling_edge(CLOCK);
Last_Ping_DT						<= '1'; 
Data_ping							<= x"9191919191919191";
wait until falling_edge(CLOCK);
WR_DT_PING							<= '0';

wait;
end process;
 
 
 
 
 
 
 
 
 
end behavioral;