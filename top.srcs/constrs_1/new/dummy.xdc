set_property CONFIG_MODE BPI16 [current_design]
set_property BITSTREAM.CONFIG.PERSIST No [current_design]
set_property BITSTREAM.CONFIG.CONFIGRATE 8.0 [current_design]
set_property BITSTREAM.CONFIG.BPI_PAGE_SIZE 8 [current_design]
set_property BITSTREAM.CONFIG.BPI_1ST_READ_CYCLE 2 [current_design]
set_property BITSTREAM.CONFIG.CONFIGFALLBACK ENABLE [current_design]
set_property BITSTREAM.CONFIG.TIMER_CFG 32'h019BFCC0 [current_design]
set_property BITSTREAM.GENERAL.COMPRESS TRUE [current_design]


set_operating_conditions -process maximum
set_switching_activity -default_toggle_rate 25.000

set_false_path -from [get_ports ClockGen_MAIN_SDA]
set_max_delay -from [get_ports ClockGen_1_LOS_n] 20.000




set_false_path -from [get_pins {freq_measure*/counter_measure_reg[*]/C}] -to [get_pins {freq_measure*/measure_reg[*]/D}]

set_false_path -from [get_pins -hierarchical -filter {NAME =~  SR_led*/flip_ON_OFF_reg/C}] -to [get_pins -hierarchical -filter {NAME =~  SR_led*/mem_pulse_reg/CLR}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~  SR_led*/mem_pulse_reg/C}]


