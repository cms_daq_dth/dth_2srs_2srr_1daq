#set_false_path -from [get_pins -hierarchical -filter {NAME =~ SR*/evt_gen*/select_ext_trigger*/C}]
set_false_path -to [get_pins -hierarchical -filter {NAME =~ SR*/SR*/evt_gen*/gen_run_async_reg/D}]

set_false_path -from [get_pins -hierarchical -filter {NAME =~ SR*/evt_gen*/select_desc_fifo*/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ SR*/evt_gen*/evt_size*[*]/C}] -to [get_pins -hierarchical -filter {NAME =~ SR*/evt_gen*/counter_wc*[*]/D}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ SR*/evt_gen*/evt_size*[*]/C}] -to [get_pins -hierarchical -filter {NAME =~ SR*/evt_gen*/Event_size_mem*[*]/D}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ SR*/evt_gen*/time_wait*[*]/C}] -to [get_pins -hierarchical -filter {NAME =~ SR*/evt_gen*/counter_timer_reg*[*]/D}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ SR*/evt_gen*/evt_desc*[*]/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ SR*/evt_gen*/evt_num*[*]/C}] -to [get_pins -hierarchical -filter {NAME =~ SR*/evt_gen*/evt_num_cnt*[*]/D}]
set _xlnx_shared_i0 [get_pins -hierarchical -filter {NAME =~ SR*/evt_gen*/event_data_word_reg*/D}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ SR*/evt_gen*/evt_BX*[*]/C}] -to $_xlnx_shared_i0
set_false_path -from [get_pins -hierarchical -filter {NAME =~ SR*/evt_gen*/evt_Source*[*]/C}] -to $_xlnx_shared_i0
set_false_path -from [get_pins -hierarchical -filter {NAME =~ SR*/evt_gen*/seed_val*[*]/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ SR*/evt_gen*/trigger_threshold_ready*[*]/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ SR*/evt_gen*/trigger_threshold_busy*[*]/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ SR*/evt_gen*/select_desc_fifo*/C}]

set_false_path -from [get_pins -hierarchical -filter {NAME =~ SR*/evt_gen/Evt_gen_descriptors_i1*/RDCLK}] -to [get_pins -hierarchical -filter {NAME =~ SR*/evt_gen*/counter_timer_reg*[*]/D}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ SR*/evt_gen/Evt_gen_descriptors_i1*/RDCLK}] -to [get_pins -hierarchical -filter {NAME =~ SR*/evt_gen*/counter_wc*[*]/D}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ SR*/evt_gen/Evt_gen_descriptors_i1*/RDCLK}] -to [get_pins -hierarchical -filter {NAME =~ SR*/evt_gen*/Event_size_mem*[*]/D}]

set_false_path -from [get_pins -hierarchical -filter {NAME =~ SR*/evt_gen*/event_run_cell*/C}] -to [get_pins -hierarchical -filter {NAME =~ SR*/evt_gen*/data_o_rg*[2]/D}]
set _xlnx_shared_i1 [get_pins -hierarchical -filter {NAME =~ SR*/evt_gen*/data_o_rg*[*]/D}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ SR*/evt_gen*/trigger_counter*/C}] -to $_xlnx_shared_i1
set_false_path -from [get_pins -hierarchical -filter {NAME =~ SR*/evt_gen*/counter_timer*/C}] -to $_xlnx_shared_i1
set_false_path -from [get_pins -hierarchical -filter {NAME =~ SR*/SR_sender/Sender_core_i1/FED_interface/rsyc_test_mode*[1]/C}] -to [get_pins -hierarchical -filter {NAME =~ SR*/evt_gen*/data_o_rg*[3]/D}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ SR*/SR_sender/Sender_core_i1/FED_interface/internal_FIFO/almost_full_reg*/C}] -to [get_pins -hierarchical -filter {NAME =~ SR*/evt_gen*/data_o_rg*[3]/D}]


set_false_path -from [get_pins -hierarchical -filter {NAME =~ SR*/evt_gen*/gen_run_reg/C}] -to [get_pins -hierarchical -filter {NAME =~ SR*/evt_gen*/gen_run_resync*/D}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ SR_SND_i2/SR*_sender/evt_gen*/gen_run_reg/C}] -to [get_pins -hierarchical -filter {NAME =~ SR_SND_i2/SR*_sender/evt_gen*/gen_run_resync*/D}]
#
set_false_path -to [get_pins -hierarchical -filter {NAME =~ *event_gen*/event_data_random*/C}]

#  for lite in the sender core
set_false_path -from [get_pins -hier -filter {NAME=~ */Sender_core_i1/FED_interface/internal_FIFO/almost_full_reg_re*/C}] -to [get_pins -hier -filter {NAME=~ */ctrl_usr_data_reg*[1]/D}]





