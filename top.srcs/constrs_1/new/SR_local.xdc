
set_false_path -from [get_pins -hierarchical -filter {NAME =~ */SR*_sender/SR_sender/Sender_core_i1/FED_interface/rsyc_DAQON_reg[1]/C}] -to [get_pins -hierarchical -filter {NAME =~ */SR*_sender/ctrl_usr_data_reg_reg[0]/D}]
set_false_path -to [get_pins -hierarchical -filter {NAME =~ */SR*_sender/ctrl_usr_data_reg_reg[*]/D}]
#set_false_path   -to [get_pins -hierarchical -filter {NAME =~ */SR*_sender/FED_SR_status_address[*]/D}]

set_false_path -from [get_pins -hierarchical -filter {NAME =~ SR_REC*/SR*_receiver/ena*counter_reg[*]/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ SR_REC*/SR*_receiver/DAQ_to_SR_backpressure_counter_reg[*]/C}]



