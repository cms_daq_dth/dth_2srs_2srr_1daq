
set_max_delay -datapath_only -from [get_pins -hierarchical -filter {NAME =~ monitor_itf*/resync_pulse*/reg_1st_stage*/C}] -to [get_pins -hierarchical -filter {NAME =~ monitor_itf*/resync_pulse*/reg_2nd_stage_async*/D}] 5.000

set _xlnx_shared_i0 [get_pins -hierarchical -filter {NAME =~ monitor_itf_i2c*/dto*[*]/D}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ monitor_itf_i2c*/Access_done*/C}] -to $_xlnx_shared_i0
set_false_path -from [get_pins -hierarchical -filter {NAME =~ monitor_itf_i2c*/error*/C}] -to $_xlnx_shared_i0
set_false_path -from [get_pins -hierarchical -filter {NAME =~ monitor_itf_i2c*/rd_sh*[*]/C}] -to $_xlnx_shared_i0

set_false_path -from [get_pins -hierarchical -filter {NAME =~ monitor_itf_i2c*/add_register*[*]*/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ monitor_itf_i2c*/read_request*/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ monitor_itf_i2c*/nb_reg_offset*[*]*/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ monitor_itf_i2c*/nb_data*[*]*/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ monitor_itf_i2c*/reg_offset*[*]*/C}]
#
