
set_max_delay -datapath_only -from [get_pins -hierarchical -filter {NAME =~ */Event_builder*/SR_i*/*event_gen*/gen_run_reg/C}] -to [get_pins -hierarchical -filter {NAME =~ */Event_builder*/SR_i*/*event_gen*/gen_run_async*/D}] 4.500


set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/Event_builder*/SR_i*/SEL_emu*/C}]

#Slink status
set _xlnx_shared_i0 [get_pins -hierarchical -filter {NAME =~ DAQ*/Event_builder*/SR_i*/usr_data_rd_reg*[*]/D}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/Event_builder*/SR_i*/SEL_emu*/C}] -to $_xlnx_shared_i0
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/Event_builder*/SR_i*/backpressure_emu_counter_Latched*[*]/C}] -to $_xlnx_shared_i0
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/Event_builder*/SR_i*/nb_of_cut_frag_cnt*[*]/C}] -to $_xlnx_shared_i0
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/Event_builder*/SR_i*/max_size*[*]/C}] -to $_xlnx_shared_i0
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/Event_builder*/SR_i*/wrong_fed_ID*[*]/C}] -to $_xlnx_shared_i0
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/Event_builder*/SR_i*/FEDID_rcv*[*]/C}] -to $_xlnx_shared_i0
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/Event_builder*/SR_i*/FED_ID*[*]/C}] -to $_xlnx_shared_i0
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/Event_builder*/SR_i*/FED_ID*[*]/C}] -to [get_pins -hierarchical -filter {NAME =~ DAQ*/Event_builder*/SR_i*/FEDID_err_reg*/D}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/Event_builder*/SR_i*/FED_ID*[*]/C}] -to [get_pins -hierarchical -filter {NAME =~ DAQ*/Event_builder*/SR_i*/FEDID_rcv*[*]/D}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/Event_builder*/SR_i*/FED_ID*[*]/C}] -to [get_pins -hierarchical -filter {NAME =~ DAQ*/Event_builder*/SR_i*/FEDID_rcv*[*]/CE}]

set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/Event_builder*/SR_i*/*/nb_of_cut_frag*[*]/C}] -to $_xlnx_shared_i0
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/Event_builder*/SR_i*/*/max_size*[*]/C}] -to $_xlnx_shared_i0
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/Event_builder*/SR_i*/*/Current_size_latched*[*]/C}] -to $_xlnx_shared_i0
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/Event_builder*/SR_i*/*/error_frag_size_reg*/C}] -to $_xlnx_shared_i0


set_false_path -from [get_pins -hierarchical -filter {NAME =~ *event_gen*/evt_desc*[*]/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ *event_gen*/seed_val*[*]/C}]

set_false_path -from [get_pins -hierarchical -filter {NAME =~ *event_gen*/evt_num*[*]/C}] -to [get_pins -hierarchical -filter {NAME =~ *event_gen*/evt_num_cnt*[*]/D}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ *event_gen*/time_wait*[*]/C}] -to [get_pins -hierarchical -filter {NAME =~ *event_gen*/counter_timer_reg*[*]/D}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ *event_gen*/evt_size*[*]/C}] -to [get_pins -hierarchical -filter {NAME =~ *event_gen*/counter_wc*[*]/D}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ *event_gen*/evt_size*[*]/C}] -to [get_pins -hierarchical -filter {NAME =~ *event_gen*/Event_size_mem*[*]/D}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ *event_gen*/select_desc_fifo*/C}] -to [get_pins -hierarchical -filter {NAME =~ *event_gen*/counter_wc*[*]/D}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ *event_gen*/select_desc_fifo*/C}] -to [get_pins -hierarchical -filter {NAME =~ *event_gen*/counter_wc*[*]/CE}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ *event_gen*/select_desc_fifo*/C}] -to [get_pins -hierarchical -filter {NAME =~ *event_gen*/counter_timer_reg*[*]/CE}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ *event_gen*/select_desc_fifo*/C}] -to [get_pins -hierarchical -filter {NAME =~ *event_gen*/counter_timer_reg*[*]/D}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ *event_gen*/select_desc_fifo*/C}] -to [get_pins -hierarchical -filter {NAME =~ *event_gen*/Evt_gen_descriptors_i1*/RDEN}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ *event_gen*/select_desc_fifo*/C}] -to [get_pins -hierarchical -filter {NAME =~ *event_gen*/Evt_gen_descriptors_i1*/WREN}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ *event_gen*/select_desc_fifo*/C}] -to [get_pins -hierarchical -filter {NAME =~ *event_gen*/Event_size_mem*[*]/D}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ *event_gen*/event_run_cell*/C}] -to [get_pins -hierarchical -filter {NAME =~ *event_gen*/data_o_rg*[2]/D}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/Event_builder*/SR_i*/i2_FIFO/*prog_full*/C}] -to [get_pins -hierarchical -filter {NAME =~ *event_gen*/data_o_rg*[3]/D}]
set _xlnx_shared_i1 [get_pins -hierarchical -filter {NAME =~ *event_gen*/data_o_rg*[*]/D}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ *event_gen*/trigger_counter*/C}] -to $_xlnx_shared_i1
set_false_path -from [get_pins -hierarchical -filter {NAME =~ *event_gen*/counter_timer*/C}] -to $_xlnx_shared_i1

set _xlnx_shared_i2 [get_pins -hierarchical -filter {NAME =~ *event_gen*/event_data_word_reg*/D}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ *event_gen*/evt_BX*[*]/C}] -to $_xlnx_shared_i2
set_false_path -from [get_pins -hierarchical -filter {NAME =~ *event_gen*/evt_Source*[*]/C}] -to $_xlnx_shared_i2

set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/Event_builder*/Max_frag_size*[*]/C}] -to [get_pins -hierarchical -filter {NAME =~ DAQ*/Event_builder*/*encap*/cut_fragment*[*]/D}]


# to be implemented

#set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/Event_builder*/SR_i*/backpressure_counter_Latched*[*]/C}]      -to [get_pins -hierarchical -filter {NAME =~ DAQ*/Event_builder*/SR_i*/usr_data_rd_reg*[*]/D}]
#set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/Event_builder*/SR_i*/backpressure_cDAQ_counter_Latched*[*]/C}] -to [get_pins -hierarchical -filter {NAME =~ DAQ*/Event_builder*/SR_i*/usr_data_rd_reg*[*]/D}]
#set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/Event_builder*/SR_i*/*/TRIGGER_reg*[*]/C}]                     -to [get_pins -hierarchical -filter {NAME =~ DAQ*/Event_builder*/SR_i*/usr_data_rd_reg*[*]/D}]
#set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/Event_builder*/SR_i*/*/BX_reg*[*]/C}]                          -to [get_pins -hierarchical -filter {NAME =~ DAQ*/Event_builder*/SR_i*/usr_data_rd_reg*[*]/D}]
#set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/Event_builder*/SR_i*/*/FED_reg*[*]/C}]                         -to [get_pins -hierarchical -filter {NAME =~ DAQ*/Event_builder*/SR_i*/usr_data_rd_reg*[*]/D}]

set_false_path -from [get_pins -hierarchical -filter {NAME =~ *event_gen*/event_data_random*/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ *evt_gen*/event_data_random*/C}]



set_false_path -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/Event_builder_*/SR_*/backpressure_cDAQ_counter_reg[*]/CE}]








