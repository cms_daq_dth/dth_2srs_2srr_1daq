set_false_path -from [get_pins DAQ_i1/ethernet_i0/ARP_probe_i1/mem_req_ARP_reg/C] -to [get_pins DAQ_i1/ethernet_i0/sync_sig_i1/reg_async_reg/D]

set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ_i1/ethernet_i*/FIFO_rcv*/*/RDCLK}] -to [get_pins {DAQ*/ethernet*/stats*/out_dt_reg[*]/D}]

set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/ethernet*/PCI_setup_register*/PORT*_D_reg*[*]/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/ethernet*/PCI_setup_register*/PORT*_S_reg*[*]/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/ethernet*/PCI_setup_register*/IP_S_reg*[*]/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/ethernet*/PCI_setup_register*/IP_D_reg*[*]/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/ethernet*/PCI_setup_register*/TCP_win_reg*[*]/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/ethernet*/PCI_setup_register*/Init_reg*[*]/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/ethernet*/PCI_setup_register*/TCP_max_size_reg*[*]/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/ethernet*/PCI_setup_register*/TCP_scale_reg*[*]/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/ethernet*/PCI_setup_register*/TCP_TS_reg*[*]/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/ethernet*/PCI_setup_register*/TCP_TS_rply_reg*[*]/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/ethernet*/PCI_setup_register*/IP_NETWORK_reg*[*]/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/ethernet*/PCI_setup_register*/IP_GATEWAY_reg*[*]/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/ethernet*/PCI_setup_register*/snd_cwnd_reg*[*]/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/ethernet*/PCI_setup_register*/timer_rtt_I_reg*[*]/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/ethernet*/PCI_setup_register*/timer_rtt_I_sync_reg*[*]/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/ethernet*/PCI_setup_register*/timer_persist_I_reg*[*]/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/ethernet*/PCI_setup_register*/thresold_retrans_reg*[*]/C}]

set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/ethernet*/PCI_setup_register*/TCP_Rexmt_CWND_sh_reg*[*]/C}]

set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/ethernet*/stats_i*/*inst_g1*/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/ethernet*/stats_i*/*inst_g2*/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/ethernet*/stats_i*/ack_stat_i1/*mem_B*/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/ethernet*/stats_i*/ack_stat_i1/*Accu_ACK_timer*/C}]

set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/ethernet*/*ACK_rcv_cnt_i1*/C}]

set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/ethernet*/TCP_logic*/flag_reg*/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/ethernet*/TCP_logic*/Stat_snd_byte_reg*/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/ethernet*/TCP_logic*/stat_snd_rexmit_byte_reg*/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/ethernet*/TCP_logic*/stat_dupack_max_reg*/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/ethernet*/TCP_logic*/stat_wnd_max_rg*/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/ethernet*/TCP_logic*/stat_wnd_min_rg*/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/ethernet*/TCP_logic*/measure_RTT_max_reg*/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/ethernet*/TCP_logic*/TCP_state*/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/ethernet*/TCP_logic*/local_RTT*/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/ethernet*/TCP_logic*/measure_RTT_min_reg*/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/ethernet*/TCP_logic*/Measure_RTT_accu_rg*/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/ethernet*/TCP_logic*/rtt_shifts_MAX_reg*/C}]

set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/ethernet*/eth_decoder*/*MSS_reg*/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/ethernet*/eth_decoder*/ARP_MAC_src_rg*[*]/C}]

set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/ethernet*/counter_rst*/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/ethernet*/nb_retrans_val*[*]/C}]

set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/ethernet*/ping_req_counter*[*]/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/ethernet*/ping_rec_counter*[*]/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/ethernet*/nb_retrans_val*[*]/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/ethernet*/counter_pause_frame_reg*[*]/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/ethernet*/delay_pause_frame_reg*[*]/C}]

set_false_path -from [get_pins -hierarchical -filter {NAME =~ MAC_bytes_low_reg*[*]/C}]


set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/ethernet*/ARP_probe*/ARP_gw_reg*/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/ethernet*/ARP_probe*/ARP_dst_reg*/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/ethernet*/ARP_probe*/ARP_probe_reg*/C}]



