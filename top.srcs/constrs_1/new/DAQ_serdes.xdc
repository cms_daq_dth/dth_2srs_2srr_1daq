
set_max_delay -datapath_only -from [get_pins {DAQ_i1/serdes_i1/gt_rxprbssel_async_reg[*]/C}] -to [get_pins {DAQ_i1/serdes_i1/gt_rxprbssel_sync_reg[*]/D}] 4.000
## DAQ*/serdes*

#set_false_path -from [get_pins -hierarchical -filter {NAME =~  DAQ*/serdes*/axi_inst*/axi_dt_rd*/C}]                 -to [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/data_in_rg*[*]/D}]
#set_false_path -from [get_pins -hierarchical -filter {NAME =~  DAQ*/serdes*/axi_inst*/FSM_sequential_AXI_access*/C}] -to [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/data_in_rg*[*]/D}]
#set_false_path -from [get_pins -hierarchical -filter {NAME =~  DAQ*/serdes*/axi_inst*/axi_error_wr*/C}]              -to [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/data_in_rg*[*]/D}]
#set_false_path -from [get_pins -hierarchical -filter {NAME =~  DAQ*/serdes*/axi_inst*/axi_error_rd*/C}]              -to [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/data_in_rg*[*]/D}]
#set_false_path -from [get_pins -hierarchical -filter {NAME =~  DAQ*/serdes*/gtx_ch_drp_do_reg[*]/C}]                 -to [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/data_in_rg*[*]/D}]
#set_false_path -from [get_pins -hierarchical -filter {NAME =~  DAQ*/serdes*/stat_rx_bip_err_cnt*[*]/C}]              -to [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/data_in_rg*[*]/D}]
#set_false_path -from [get_pins -hierarchical -filter {NAME =~  DAQ*/serdes*/rx_frame_l*_err_cnt*[*]/C}]              -to [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/data_in_rg*[*]/D}]
#set_false_path -from [get_pins -hierarchical -filter {NAME =~  DAQ*/serdes*/stat_tx_frame_error_cnt*[*]/C}]          -to [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/data_in_rg*[*]/D}]
#set_false_path -from [get_pins -hierarchical -filter {NAME =~  DAQ*/serdes*/stat_tx_broadcast_cnt*[*]/C}]            -to [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/data_in_rg*[*]/D}]
#set_false_path -from [get_pins -hierarchical -filter {NAME =~  DAQ*/serdes*/stat_tx_bad_fcs_cnt*[*]/C}]              -to [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/data_in_rg*[*]/D}]
#set_false_path -from [get_pins -hierarchical -filter {NAME =~  DAQ*/serdes*/stat_tx_local_fault_cnt*[*]/C}]          -to [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/data_in_rg*[*]/D}]
#set_false_path -from [get_pins -hierarchical -filter {NAME =~  DAQ*/serdes*/stat_rx_pause_counter*[*]/C}]            -to [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/data_in_rg*[*]/D}]
#set_false_path -from [get_pins -hierarchical -filter {NAME =~  DAQ*/serdes*/rx_crc_error*[*]/C}]                     -to [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/data_in_rg*[*]/D}]
##set_false_path -from [get_pins DAQ*/serdes*/gt_txbufstatus*[*]/C]                                                   -to [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/data_in_rg*[*]/D}]
#set_false_path -from [get_pins -hierarchical -filter {NAME =~  DAQ*/serdes*/tx_unfout_mem*/C}]                        -to [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/data_in_rg*[*]/D}]
##set_false_path -from [get_pins DAQ*/serdes*/tx_rdyout*[*]/C]                                                        -to [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/data_in_rg*[*]/D}]
#set_false_path -from [get_pins -hierarchical -filter {NAME =~  DAQ*/serdes*/tx_ovfout_mem*/C}]                        -to [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/data_in_rg*[*]/D}]
#set_false_path -from [get_pins -hierarchical -filter {NAME =~  DAQ*/serdes*/*/AXI_done*/C}]                           -to [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/data_in_rg*[*]/D}]
#set_false_path -from [get_pins -hierarchical -filter {NAME =~  DAQ*/serdes*/*/Axi_status*[*]}]                      -to [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/data_in_rg*[*]/D}]
#set_false_path -from [get_pins -hierarchical -filter {NAME =~  DAQ*/serdes*/*/axi_dt_rd*[*]/C}]                       -to [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/data_in_rg*[*]/D}]

#set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/caui4/inst/*MAC100GBv1*/*/RXUSRCLK2*}]    -to [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/data_in_rg*[*]/D}]
#set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/caui4/inst/*MAC100GBv1*/*/TXUSRCLK2*}]    -to [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/data_in_rg*[*]/D}]
#set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/caui4/inst/*MAC100GBv1*/*/TX_CLK*}]       -to [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/data_in_rg*[*]/D}]
#set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/caui4/inst/*MAC100GBv1*/*/RX_CLK*}]       -to [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/data_in_rg*[*]/D}]
set_false_path -to [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/data_in_rg*[*]/D}]

set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/gtx_ch_drp_en_reg[*]/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/gtx_ch_drp_addr_reg[*]/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/gtx_ch_drp_di_reg[*]/C}]

set_false_path -to [get_pins -hierarchical -filter {NAME =~ DAQ_i1/serdes_i1/*/reg_async_reg/D}]
set_false_path -to [get_pins -hierarchical -filter {NAME =~ DAQ_i1/serdes_i1/*/reg_2nd_stage_async_reg/D}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ_i1/serdes_i1/*_async_reg[*]/C}] -to [get_pins -hierarchical -filter {NAME =~ DAQ_i1/serdes_i1/*_sync_reg[*]/D}]

set_false_path -to [get_pins -hierarchical -filter {NAME =~  DAQ_*/serdes_*/*/reg_async_reg/D}]
set_false_path -to [get_pins -hierarchical -filter {NAME =~  DAQ_*/serdes_*/*_sync_reg[*]/D}]

