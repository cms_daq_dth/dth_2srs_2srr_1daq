

set_max_delay -datapath_only -from [get_pins -hierarchical -filter {NAME =~ DAQ*/Data_manager*/BIFI*/reg_1st_stage*/C}] -to [get_pins -hierarchical -filter {NAME =~ DAQ*/Data_manager*/BIFI*/reg_2nd_stage_async*/D}] 4.500

set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/ethernet*/TCP_logic*/Snd_una_OK*[*]/C}] -to [get_pins -hierarchical -filter {NAME =~ DAQ*/Data_manager*/BIFI*/sync_UNA_A_*[*]/D}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/Data_manager*/BIFI*/SND_NEW_A_reg_reg*[*]/C}] -to [get_pins -hierarchical -filter {NAME =~ DAQ*/Data_manager*/BIFI*/SND_NEW_A_reg*[*]/D}]


set_false_path -to [get_pins -hierarchical -filter {NAME =~ DAQ*/Data_manager*/BIFI_max*[*]/CE}]
set_false_path -to [get_pins -hierarchical -filter {NAME =~ DAQ*/Data_manager*/BIFI_max*[*]/D}]
set_false_path -to [get_pins -hierarchical -filter {NAME =~ DAQ*/Data_manager*/usr_data_rd_reg*[*]/D}]


set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/ethernet*/UR*_MEM_Size_request*[*]/C}] -to [get_pins -hierarchical -filter {NAME =~ DAQ*/Data_manager*/BIFI*/rd_size_cnt*[*]*/D}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/ethernet*/UR*_MEM_Size_request*[*]/C}] -to [get_pins -hierarchical -filter {NAME =~ DAQ*/Data_manager*/BIFI*/ena_size_cnt*[*]*/D}]

set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/ethernet*/UR*_MEM_Add_request*[*]/C}] -to [get_pins -hierarchical -filter {NAME =~ DAQ*/Data_manager*/BIFI*/UR_rd_add_cnt*[*]*/D}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/ethernet*/UR*_MEM_Add_request*[*]/C}] -to [get_pins -hierarchical -filter {NAME =~ DAQ*/Data_manager*/BIFI*/word_align_128b*[*]*/D}]

set_max_delay -datapath_only -from [get_pins -hierarchical -filter {NAME =~ DAQ*/ethernet*/UR0_MEM_req_rd_blk_reg*/C}] -to [get_pins -hierarchical -filter {NAME =~ DAQ*/Data_manager*/BIFI*/req_read_async*/D}] 2.500

set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/Event_builder*/SR_*/backpressure_cDAQ_counter_Latched_reg[*]/C}]

set_false_path -from [get_pins {DAQ*/Data_manager*/BIFI_size_reg_reg[*]/C}]

set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/ethernet*/Arbiter_*/UR*_MEM_req_rd_blk_reg_reg/C}] -to [get_pins -hierarchical -filter {NAME =~  DAQ_*/Data_manager_*/BIFI_*/req_read_async_reg/D}]






