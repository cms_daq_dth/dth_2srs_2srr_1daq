#create_clock -period 10.000 -name CLK_100MHZ_user [get_ports CLK_100MHZ_P]
create_clock -period 6.400 -name CLK_156_25MHZ_P [get_ports CLK_156_25MHZ_P]
create_clock -period 3.103 -name CLK_322_265625MHZ_P [get_ports CLK_322_265625MHZ_P]

#create_clock -period 3.103 -name CLK_DAQ [get_ports QSFP_Clk_ref_D_P]

create_clock -period 6.400 -name CLK_SR0 [get_ports {SlinkRocket_refclkp[0]}]
create_clock -period 6.400 -name CLK_SR1 [get_ports {SlinkRocket_refclkp[3]}]





create_clock -period 10.000 -name PCIE_clk_p [get_ports PCIE_clk_p]


#create_clock -period 10.00 -name SlinkRocket_regclkn[0] [get_ports SlinkRocket_regclkn[0]]
#create_clock -period 10.00 -name SlinkRocket_regclkn[1] [get_ports SlinkRocket_regclkn[1]]
#create_clock -period 10.00 -name SlinkRocket_regclkn[2] [get_ports SlinkRocket_regclkn[2]]
#create_clock -period 10.00 -name SlinkRocket_regclkn[3] [get_ports SlinkRocket_regclkn[3]]
#create_clock -period 10.00 -name SlinkRocket_regclkn[4] [get_ports SlinkRocket_regclkn[4]]
#create_clock -period 10.00 -name SlinkRocket_regclkn[5] [get_ports SlinkRocket_regclkn[5]]

#create_clock -period 10.00 -name HMC_A_regclkn[0] [get_ports HMC_A_regclkn[0]]
#create_clock -period 10.00 -name HMC_A_regclkn[1] [get_ports HMC_A_regclkn[1]]
#create_clock -period 10.00 -name HMC_B_regclkn[0] [get_ports HMC_B_regclkn[0]]
#create_clock -period 10.00 -name HMC_B_regclkn[1] [get_ports HMC_B_regclkn[1]]
#create_clock -period 10.00 -name HMC_C_regclkn[0] [get_ports HMC_C_regclkn[0]]
#create_clock -period 10.00 -name HMC_C_regclkn[1] [get_ports HMC_C_regclkn[1]]
#create_clock -period 10.00 -name HMC_D_regclkn[0] [get_ports HMC_D_regclkn[0]]
#create_clock -period 10.00 -name HMC_D_regclkn[1] [get_ports HMC_D_regclkn[1]]


#create_clock -period 10.00 -name TCDS_clock_p [get_ports TCDS_clock_p]

set_false_path -from [get_ports reserved]
#
set_false_path -to [get_ports GPIO_LED_0_LS]  
set_false_path -to [get_ports GPIO_LED_1_LS]  
set_false_path -to [get_ports GPIO_LED_2_LS]  
set_false_path -to [get_ports GPIO_LED_3_LS]  
set_false_path -to [get_ports GPIO_LED_4_LS]  
set_false_path -to [get_ports GPIO_LED_5_LS]  
set_false_path -to [get_ports GPIO_LED_6_LS]  
set_false_path -to [get_ports GPIO_LED_7_LS]  


set_false_path -to [get_ports QSFP_A_MODSEL] 
set_false_path -to [get_ports QSFP_A_RESETL] 
set_false_path -from [get_ports QSFP_A_MODPRSL]  
set_false_path -from [get_ports QSFP_A_INTL]  
set_false_path -to [get_ports QSFP_A_LPMODE]  

set_false_path -to [get_ports QSFP_B_MODSEL]  
set_false_path -to [get_ports QSFP_B_RESETL]  
set_false_path -from [get_ports QSFP_B_MODPRSL]  
set_false_path -from [get_ports QSFP_B_INTL]  
set_false_path -to [get_ports QSFP_B_LPMODE]  

set_false_path -to [get_ports QSFP_C_MODSEL]  
set_false_path -to [get_ports QSFP_C_RESETL]  
set_false_path -from [get_ports QSFP_C_MODPRSL]  
set_false_path -from [get_ports QSFP_C_INTL]  
set_false_path -to [get_ports QSFP_C_LPMODE]  

set_false_path -to [get_ports QSFP_D_MODSEL]  
set_false_path -to [get_ports QSFP_D_RESETL]  
set_false_path -from [get_ports QSFP_D_MODPRSL]  
set_false_path -from [get_ports QSFP_D_INTL]  
set_false_path -to [get_ports QSFP_D_LPMODE]  

set_false_path -from [get_ports FF_A_PRESENT]  
set_false_path -to [get_ports FF_A_RESET_L]  
set_false_path -to [get_ports FF_A_CS]  
set_false_path -from [get_ports FF_A_INTL_L]  




set_false_path -to [get_ports ClockGen_1_rst_n]  
set_false_path -to [get_ports ClockGen_2_rst_n]  
set_false_path -to [get_ports ClockGen_3_rst_n]  

set_false_path -from [get_ports ClockGen_1_LOS_n] 
set_false_path -from [get_ports ClockGen_2_LOS_n] 
set_false_path -from [get_ports ClockGen_3_LOS_n] 

set_false_path -from [get_ports ClockGen_1_LOL_n] 
set_false_path -from [get_ports ClockGen_2_LOL_n] 
set_false_path -from [get_ports ClockGen_3_LOL_n] 

set_false_path -from [get_ports ClockGen_1_INTR_n]  
set_false_path -from [get_ports ClockGen_2_INTR_n]  
set_false_path -from [get_ports ClockGen_3_INTR_n]  



create_clock -period 10000.000 -name POWER_MAIN_SCL [get_ports POWER_MAIN_SCL]
create_clock -period 10000.000 -name SN_MAIN_SCL [get_ports SN_MAIN_SCL]
create_clock -period 10000.000 -name ClockGen_MAIN_SCL [get_ports ClockGen_MAIN_SCL]
create_clock -period 10000.000 -name FireFly_MAIN_SCL [get_ports FireFly_MAIN_SCL]

create_clock -period 10000.000 -name POWER_MAIN_SCL_virtual
create_clock -period 10000.000 -name SN_MAIN_SCL_virtual
create_clock -period 10000.000 -name ClockGen_MAIN_SCL_virtual
create_clock -period 10000.000 -name HMC_MAIN_SCL_virtual
create_clock -period 10000.000 -name FireFly_MAIN_SCL_virtual
create_clock -period 10000.000 -name QSFP_MAIN_SCL_virtual

#


























