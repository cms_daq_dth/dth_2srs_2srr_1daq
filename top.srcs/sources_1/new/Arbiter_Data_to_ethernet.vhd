----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 12.08.2019 16:56:52
-- Design Name: 
-- Module Name: Arbiter_Data_to_ethernet - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Arbiter_Data_to_ethernet is
 Port (
 
	Rst_tx_usr_clk_n									: in std_logic;
	tx_usr_clk											: in std_logic;
 
	stop_transfert_TCP									: in std_logic;
	wait_on_eth_transfert								: in std_logic;
	
--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-- Request from TCP logic
-- STREAM 0
	TCP_P0_wen_rq_Send_pkt						    	: in std_logic;						-- request to transfer a TCP (open,Data,..) packet
	TCP_P0_wen_rq_ack_pkt 						    	: in std_logic;						-- request to transfer a TCP (ack) packet
	PORT0_D												: in std_logic_vector(15 downto 0);	-- Destination port number
	PORT0_S												: in std_logic_vector(15 downto 0);	-- Source port number
																						-- for a data or open or close
	TCP_P0_FLAG_Send_Send_Pkt					    	: in std_logic_vector(7 downto 0);	-- flags for TCP packet
	TCP_P0_SEQ_Send_Send_Pkt							: in std_logic_vector(31 downto 0);	-- Sequence Number for TCP packet
	TCP_P0_ACK_Send_Send_Pkt							: in std_logic_vector(31 downto 0);	-- Acknowledge Number for TCP packet
	TCP_P0_pckt_wo_Send_Pkt						    	: in std_logic;						-- Specify if there are (1) or no data (0) in the packet (Open, close or data packet)
	TCP_P0_option_Send_Send_Pkt					    	: in std_logic_vector(2 downto 0);	-- Specify the option in the OPEN connection
	TCP_P0_Length_Send_Send_Pkt					    	: in std_logic_vector(31 downto 0);	-- Length of data in the packet (in bytes)
																						-- for an acknowledge packet
	TCP_P0_FLAG_R_Ack_Pkt						    	: in std_logic_vector(7 downto 0);	-- flags for TCP packet
	TCP_P0_SEQ_R_Ack_Pkt								: in std_logic_vector(31 downto 0);	-- Sequence Number for TCP packet
	TCP_P0_ACK_R_Ack_Pkt								: in std_logic_vector(31 downto 0);	-- Acknowledge Number for TCP packet
	TCP_P0_Opt_R_Ack_Pkt								: in std_logic_vector(2 downto 0); 	-- Specify the option in the ack
	
    TCP_P0_request_TCP_snd_packet_n				    	: out std_logic;   					-- specify to TCP logic when the request is DONE (UR read and data transfered over Ethernet)
	TCP_P0_request_TCP_ack_packet_n				    	: out std_logic;   					-- specify to TCP logic when the request is DONE (ack transfered over Ethernet)
	TCP_P0_request_took		   					    	: out std_logic; 					-- the data packet si sent TCP logic can continu to check if there is new incoming data
	
-- STREAM 1	
	TCP_P1_wen_rq_Send_pkt						    	: in std_logic;						-- request to transfer a TCP (open,Data,..) packet
	TCP_P1_wen_rq_ack_pkt						    	: in std_logic;						-- request to transfer a TCP (ack) packet
	PORT1_D												: in std_logic_vector(15 downto 0);
	PORT1_S												: in std_logic_vector(15 downto 0);
																						-- for a data or open or close
	TCP_P1_FLAG_Send_Send_Pkt					    	: in std_logic_vector(7 downto 0);
	TCP_P1_SEQ_Send_Send_Pkt							: in std_logic_vector(31 downto 0);
	TCP_P1_ACK_Send_Send_Pkt							: in std_logic_vector(31 downto 0);
	TCP_P1_pckt_wo_Send_Pkt						    	: in std_logic;  
	TCP_P1_option_Send_Send_Pkt					    	: in std_logic_vector(2 downto 0);
	TCP_P1_Length_Send_Send_Pkt					    	: in std_logic_vector(31 downto 0);
																						-- for an acknowledge packet
	TCP_P1_FLAG_R_Ack_Pkt						    	: in std_logic_vector(7 downto 0);
	TCP_P1_SEQ_R_Ack_Pkt								: in std_logic_vector(31 downto 0);
	TCP_P1_ACK_R_Ack_Pkt								: in std_logic_vector(31 downto 0);
	TCP_P1_Opt_R_Ack_Pkt								: in std_logic_vector(2 downto 0);  
	
    TCP_P1_request_TCP_snd_packet_n				    	: out std_logic;    
	TCP_P1_request_TCP_ack_packet_n				    	: out std_logic;    
	TCP_P1_request_took		   					    	: out std_logic; 					
														  
--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
 -- request on  UR 
	
	UR0_MEM_req_rd_blk 									: out std_logic;					-- request to read UR 
	UR0_MEM_Add_request 								: out std_logic_vector(31 downto 0);-- Address to start the read
	UR0_MEM_Size_request 								: out std_logic_vector(31 downto 0);-- length to read (in bytes)
	UR0_MEM_request_done								: in std_logic;						-- The Packet is read from UR (ended)
	 
	UR1_MEM_req_rd_blk 									: out std_logic;					-- request to read UR 
	UR1_MEM_Add_request 								: out std_logic_vector(31 downto 0);-- Address to start the read
	UR1_MEM_Size_request 								: out std_logic_vector(31 downto 0);-- length to read (in bytes)
	UR1_MEM_request_done								: in std_logic;						-- The Packet is read from UR (ended)
	 
	UR0_blk_ready										: in std_logic;-- informs that the Data are ready in the BIFI buffer
	UR1_blk_ready										: in std_logic;-- informs that the Data are ready in the BIFI buffer
	
	Request_to_send_Eth_packet							: out std_logic;					-- request to send packet over Ethernet link
	UR_Part_selection_0_1								: out std_logic := '0';				-- which UR to use
	PORTx_D												: out std_logic_vector(15 downto 0);
	PORTx_S												: out std_logic_vector(15 downto 0); 
	TCP_Px_FLAG_Send_out								: out std_logic_vector(7 downto 0);
	TCP_Px_SEQ_Send_out								    : out std_logic_vector(31 downto 0);
	TCP_Px_ACK_Send_out								    : out std_logic_vector(31 downto 0);
	TCP_Px_option_Send_out							    : out std_logic_vector(2 downto 0);
	TCP_Px_Length_Send_out							    : out std_logic_vector(31 downto 0);
	TCP_Px_pckt_wo_dt									: out std_logic;
		
	TCP_Px_request_took		   					    	: in std_logic ; 					-- the request is taken in account
	TCP_Px_request_done		   					    	: in std_logic  					-- End of packet transfer on the Ethernet side
	
 );
end Arbiter_Data_to_ethernet;

architecture Behavioral of Arbiter_Data_to_ethernet is


-- arbiter to control TCP_BufBuffe 0 or 1
signal TCP0_ETH_snd_REQ								: std_logic;
signal TCP0_ETH_snd_GNT								: std_logic; 
signal TCP0_ETH_ack_REQ								: std_logic;
signal TCP0_ETH_ack_GNT								: std_logic; 
signal TCP1_ETH_snd_REQ								: std_logic;
signal TCP1_ETH_snd_GNT								: std_logic; 
signal TCP1_ETH_ack_REQ								: std_logic;
signal TCP1_ETH_ack_GNT								: std_logic; 
signal toggle_GNT									: std_logic_vector(2 downto 0) := "000";
signal UR_Part_selection_0_1_reg					: std_logic; -- select the payload source 
 
signal UR0_MEM_req_rd_blk_reg						: std_logic; 
signal UR0_MEM_Add_request_reg						: std_logic_vector(31 downto 0);
signal UR0_MEM_Size_request_reg						: std_logic_vector(31 downto 0);
	
signal UR1_MEM_req_rd_blk_reg						: std_logic; 
signal UR1_MEM_Add_request_reg						: std_logic_vector(31 downto 0);
signal UR1_MEM_Size_request_reg						: std_logic_vector(31 downto 0);

signal TCP_P0_req_snd_ongoing						: std_logic;
signal TCP_P1_req_snd_ongoing						: std_logic;
signal TCP_P0_req_ack_ongoing						: std_logic;
signal TCP_P1_req_ack_ongoing						: std_logic;
  
-- memorise send_pckt_wo_data because it change to fast
signal ETH0_send_pckt_wo_data                   	: std_logic; 
signal ETH1_send_pckt_wo_data                   	: std_logic; 
 
-- register TCP parameters for Send  0
signal  SendP0_TCP_P0_SEQ_Send_Send_Pkt              : std_logic_vector(31 downto 0);	
signal  SendP0_TCP_P0_ACK_Send_Send_Pkt              : std_logic_vector(31 downto 0);		
signal  SendP0_TCP_P0_FLAG_Send_Send_Pkt             : std_logic_vector(7 downto 0);	
signal  SendP0_TCP_P0_option_Send_Send_Pkt           : std_logic_vector(2 downto 0);	
signal  SendP0_ETH0_send_pckt_wo_data                : std_logic;		
signal  SendP0_TCP_P0_Length_Send_Send_Pkt           : std_logic_vector(31 downto 0);	
signal  SendP0_PORT0_D                               : std_logic_vector(15 downto 0);
signal  SendP0_PORT0_S                               : std_logic_vector(15 downto 0); 

-- register TCP parameters for Ack Port 0
signal  AckP0_TCP_P0_SEQ_R_Ack_Pkt                  : std_logic_vector(31 downto 0);	
signal  AckP0_TCP_P0_ACK_R_Ack_Pkt                  : std_logic_vector(31 downto 0);
signal  AckP0_TCP_P0_FLAG_R_Ack_Pkt                 : std_logic_vector(7 downto 0);
signal  AckP0_TCP_P0_Opt_R_Ack_Pkt                  : std_logic_vector(2 downto 0);
signal  AckP0_PORT0_D                               : std_logic_vector(15 downto 0);
signal  AckP0_PORT0_S                               : std_logic_vector(15 downto 0); 

-- register TCP parameters for Send Port 1
signal  SendP1_TCP_P1_SEQ_Send_Send_Pkt              : std_logic_vector(31 downto 0);	
signal  SendP1_TCP_P1_ACK_Send_Send_Pkt              : std_logic_vector(31 downto 0);		
signal  SendP1_TCP_P1_FLAG_Send_Send_Pkt             : std_logic_vector(7 downto 0);	
signal  SendP1_TCP_P1_option_Send_Send_Pkt           : std_logic_vector(2 downto 0);	
signal  SendP1_ETH1_send_pckt_wo_data                : std_logic;				
signal  SendP1_TCP_P1_Length_Send_Send_Pkt           : std_logic_vector(31 downto 0);	
signal  SendP1_PORT1_D                               : std_logic_vector(15 downto 0);
signal  SendP1_PORT1_S                               : std_logic_vector(15 downto 0); 

-- register TCP parameters for Ack Port 1
signal  AckP1_TCP_P1_SEQ_R_Ack_Pkt                  : std_logic_vector(31 downto 0);	
signal  AckP1_TCP_P1_ACK_R_Ack_Pkt                  : std_logic_vector(31 downto 0);
signal  AckP1_TCP_P1_FLAG_R_Ack_Pkt                 : std_logic_vector(7 downto 0);
signal  AckP1_TCP_P1_Opt_R_Ack_Pkt                  : std_logic_vector(2 downto 0);
signal  AckP1_PORT1_D                               : std_logic_vector(15 downto 0);
signal  AckP1_PORT1_S                               : std_logic_vector(15 downto 0); 

signal Request_to_send_Eth_packet_memory            : std_logic; 
 
attribute mark_debug : string;
attribute mark_debug of TCP0_ETH_snd_GNT	                     : signal is "true"; 
attribute mark_debug of TCP1_ETH_snd_GNT		                 : signal is "true"; 
attribute mark_debug of TCP0_ETH_ack_GNT	                     : signal is "true"; 
attribute mark_debug of TCP1_ETH_ack_GNT		                 : signal is "true"; 
attribute mark_debug of UR_Part_selection_0_1_reg	             : signal is "true"; 
attribute mark_debug of toggle_GNT		                         : signal is "true"; 
attribute mark_debug of TCP_P0_wen_rq_Send_pkt		                         : signal is "true"; 
attribute mark_debug of TCP_P1_wen_rq_Send_pkt		                         : signal is "true"; 
attribute mark_debug of TCP_P0_req_snd_ongoing	                 : signal is "true"; 
attribute mark_debug of TCP_P0_req_ack_ongoing		             : signal is "true"; 
attribute mark_debug of TCP_P1_req_snd_ongoing	                 : signal is "true"; 
attribute mark_debug of TCP_P1_req_ack_ongoing		             : signal is "true"; 
attribute mark_debug of UR0_MEM_req_rd_blk_reg		             : signal is "true"; 
attribute mark_debug of UR1_MEM_req_rd_blk_reg		             : signal is "true"; 
attribute mark_debug of TCP0_ETH_snd_REQ	                     : signal is "true"; 
attribute mark_debug of TCP0_ETH_ack_REQ		                 : signal is "true"; 
attribute mark_debug of TCP1_ETH_snd_REQ	                     : signal is "true"; 
attribute mark_debug of TCP1_ETH_ack_REQ		                 : signal is "true"; 
attribute mark_debug of UR0_MEM_Add_request_reg	                 : signal is "true"; 
attribute mark_debug of UR0_MEM_Size_request_reg		         : signal is "true"; 
attribute mark_debug of ETH0_send_pckt_wo_data		             : signal is "true"; 
attribute mark_debug of UR1_MEM_Add_request_reg	                 : signal is "true"; 
attribute mark_debug of UR1_MEM_Size_request_reg		         : signal is "true"; 
attribute mark_debug of ETH1_send_pckt_wo_data		             : signal is "true"; 
attribute mark_debug of UR0_blk_ready		             : signal is "true"; 
attribute mark_debug of UR1_blk_ready		             : signal is "true"; 

attribute mark_debug of TCP_P0_wen_rq_ack_pkt	         : signal is "true"; 
attribute mark_debug of TCP_P1_wen_rq_ack_pkt	         : signal is "true"; 
attribute mark_debug of SendP0_TCP_P0_SEQ_Send_Send_Pkt	         : signal is "true"; 
attribute mark_debug of SendP1_TCP_P1_SEQ_Send_Send_Pkt		     : signal is "true"; 
attribute mark_debug of AckP0_TCP_P0_SEQ_R_Ack_Pkt	             : signal is "true"; 
attribute mark_debug of AckP1_TCP_P1_SEQ_R_Ack_Pkt		         : signal is "true"; 
attribute mark_debug of Request_to_send_Eth_packet		         : signal is "true"; 
attribute mark_debug of TCP_Px_request_took		         : signal is "true"; 
attribute mark_debug of TCP_Px_request_done		         : signal is "true"; 



-- Code start here
--#############################################################################
begin

-- request the read from the UR memory
-- Each UR is independant
--
-- store the information (address and length ) to read the TCP stream buffer to be sent
-- not use in case of a packet without data
--
--	UR0
--
-- Latch TCP parameters for Send  package
process(tx_usr_clk)
begin
	if rising_edge(tx_usr_clk) then 
		if TCP_P0_wen_rq_Send_pkt = '1'  and TCP_P0_req_snd_ongoing = '0' then
			UR0_MEM_Add_request_reg		            <= TCP_P0_SEQ_Send_Send_Pkt;		-- address in bytes
			UR0_MEM_Size_request_reg	            <= TCP_P0_Length_Send_Send_Pkt;		-- length in bytes
			ETH0_send_pckt_wo_data                  <= TCP_P0_pckt_wo_Send_Pkt;
            
            SendP0_TCP_P0_SEQ_Send_Send_Pkt         <= TCP_P0_SEQ_Send_Send_Pkt;	
            SendP0_TCP_P0_ACK_Send_Send_Pkt         <= TCP_P0_ACK_Send_Send_Pkt;		
            SendP0_TCP_P0_FLAG_Send_Send_Pkt        <= TCP_P0_FLAG_Send_Send_Pkt;
            SendP0_TCP_P0_option_Send_Send_Pkt      <= TCP_P0_option_Send_Send_Pkt;
            SendP0_ETH0_send_pckt_wo_data           <= TCP_P0_pckt_wo_Send_Pkt;
            SendP0_TCP_P0_Length_Send_Send_Pkt      <= TCP_P0_Length_Send_Send_Pkt;	
            SendP0_PORT0_D                          <= PORT0_D;
            SendP0_PORT0_S                          <= PORT0_S;
		end if;
	end if;
end process;

process(Rst_tx_usr_clk_n,tx_usr_clk)
begin
	if Rst_tx_usr_clk_n = '0' then
		UR0_MEM_req_rd_blk_reg		<= '0';
	elsif rising_edge(tx_usr_clk) then  
		if  UR0_MEM_request_done = '1' then		-- stop when the access is done (data READ)
			UR0_MEM_req_rd_blk_reg	<= '0';
		elsif TCP_P0_wen_rq_Send_pkt = '1' and TCP_P0_pckt_wo_Send_Pkt = '0' and TCP_P0_req_snd_ongoing = '0' then		-- request the UR access
			UR0_MEM_req_rd_blk_reg	<= '1';
		end if;
	end if;
end process;

UR0_MEM_req_rd_blk       	<= UR0_MEM_req_rd_blk_reg;
UR0_MEM_Add_request      	<= UR0_MEM_Add_request_reg;
UR0_MEM_Size_request		<= UR0_MEM_Size_request_reg;
 
--
--	UR1
--
-- Latch TCP parameters for Send  package
process(tx_usr_clk)
begin
	if rising_edge(tx_usr_clk) then 
		if TCP_P1_wen_rq_Send_pkt = '1' and TCP_P1_req_snd_ongoing = '0' then
			UR1_MEM_Add_request_reg		            <= TCP_P1_SEQ_Send_Send_Pkt;		-- address in bytes
			UR1_MEM_Size_request_reg	            <= TCP_P1_Length_Send_Send_Pkt;		-- length in bytes
			ETH1_send_pckt_wo_data                  <= TCP_P1_pckt_wo_Send_Pkt;
                        
            SendP1_TCP_P1_SEQ_Send_Send_Pkt         <= TCP_P1_SEQ_Send_Send_Pkt;	
            SendP1_TCP_P1_ACK_Send_Send_Pkt         <= TCP_P1_ACK_Send_Send_Pkt;		
            SendP1_TCP_P1_FLAG_Send_Send_Pkt        <= TCP_P1_FLAG_Send_Send_Pkt;
            SendP1_TCP_P1_option_Send_Send_Pkt      <= TCP_P1_option_Send_Send_Pkt;
            SendP1_ETH1_send_pckt_wo_data           <= TCP_P1_pckt_wo_Send_Pkt;
            SendP1_TCP_P1_Length_Send_Send_Pkt      <= TCP_P1_Length_Send_Send_Pkt;	
            SendP1_PORT1_D                          <= PORT1_D;
            SendP1_PORT1_S                          <= PORT1_S;
		end if;
	end if;
end process;

process(Rst_tx_usr_clk_n,tx_usr_clk)
begin
	if Rst_tx_usr_clk_n = '0' then
		UR1_MEM_req_rd_blk_reg		<= '0';
	elsif rising_edge(tx_usr_clk) then  
		if UR1_MEM_request_done = '1' then		-- stop when the access is done (data READ)
			UR1_MEM_req_rd_blk_reg	<= '0';
		elsif TCP_P1_wen_rq_Send_pkt = '1' and TCP_P1_pckt_wo_Send_Pkt = '0' and TCP_P1_req_snd_ongoing = '0' then		-- request teh UR access		
			UR1_MEM_req_rd_blk_reg	<= '1';
		end if;
	end if;
end process;

UR1_MEM_req_rd_blk       	<= UR1_MEM_req_rd_blk_reg;
UR1_MEM_Add_request      	<= UR1_MEM_Add_request_reg;
UR1_MEM_Size_request		<= UR1_MEM_Size_request_reg;
 
--**************************************************************************
--
-- Memorise the ongoing access when sending a packet (as an envelop)
--

process(Rst_tx_usr_clk_n,tx_usr_clk)
begin
	if Rst_tx_usr_clk_n = '0' then
		TCP_P0_req_snd_ongoing		   <= '0';
		TCP_P0_req_ack_ongoing		   <= '0';
		TCP_P1_req_snd_ongoing		   <= '0';
		TCP_P1_req_ack_ongoing		   <= '0'; 
	elsif rising_edge(tx_usr_clk) then  
		-- start to transfer a data request 
		if   TCP0_ETH_snd_GNT = '1' and TCP_Px_request_done = '1' then	-- ended when packet is send over the ethernet logic
			TCP_P0_req_snd_ongoing	   <= '0';
		elsif TCP_P0_wen_rq_Send_pkt = '1' and TCP_P0_req_snd_ongoing = '0' then							-- Start of a request from tcp logic Sender
			TCP_P0_req_snd_ongoing	   <= '1'; 
		end if; 
		 
		if   TCP0_ETH_ack_GNT = '1'  and  TCP_Px_request_done = '1' then-- ended when packet is send over the ethernet logic
			TCP_P0_req_ack_ongoing	   <= '0';
		elsif TCP_P0_wen_rq_ack_pkt = '1' and TCP_P0_req_ack_ongoing = '0' then							-- Start of a request from tcp logic Received
			TCP_P0_req_ack_ongoing	   <= '1'; 
		end if; 
		 
		-- start to transfer a data request 
		if  TCP1_ETH_snd_GNT = '1' and  TCP_Px_request_done = '1' then
			TCP_P1_req_snd_ongoing	   <= '0';
		elsif TCP_P1_wen_rq_Send_pkt = '1' and TCP_P1_req_snd_ongoing = '0' then							-- Start of a request from tcp logic Sender
			TCP_P1_req_snd_ongoing	   <= '1'; 
		end if;		
		 
		if  TCP1_ETH_ack_GNT = '1' and  TCP_Px_request_done = '1' then
			TCP_P1_req_ack_ongoing	   <= '0';
		elsif TCP_P1_wen_rq_ack_pkt = '1' and TCP_P1_req_ack_ongoing = '0' then							-- Start of a request from tcp logic Received
			TCP_P1_req_ack_ongoing	   <= '1'; 
		end if;
	end if;
end process;

TCP_P0_request_TCP_snd_packet_n		<= not(TCP_P0_req_snd_ongoing);
TCP_P0_request_TCP_ack_packet_n		<= not(TCP_P0_req_ack_ongoing);
TCP_P1_request_TCP_snd_packet_n		<= not(TCP_P1_req_snd_ongoing);
TCP_P1_request_TCP_ack_packet_n		<= not(TCP_P1_req_ack_ongoing);

--***************************************************************************
-- Arbiter for TCP access 
--
-- generate the requests
--
process(Rst_tx_usr_clk_n,tx_usr_clk)
begin
	if Rst_tx_usr_clk_n = '0' then
		TCP0_ETH_snd_REQ		<= '0';
		TCP1_ETH_snd_REQ		<= '0';
		TCP0_ETH_ack_REQ		<= '0';
		TCP1_ETH_ack_REQ		<= '0';
	elsif rising_edge(tx_usr_clk) then  
		
			-- transfer a packet without data
		if 	(TCP_P0_wen_rq_Send_pkt = '1' and TCP_P0_pckt_wo_Send_Pkt = '1') or
			-- transfer a packet with data (wait for data ready)
			(TCP_P0_req_snd_ongoing = '1' and ETH0_send_pckt_wo_data = '0' and UR0_blk_ready = '1') then
				TCP0_ETH_snd_REQ	<= '1';
		elsif TCP0_ETH_snd_GNT = '1' then
				TCP0_ETH_snd_REQ	<= '0';
		end if;
		
			-- transfer an acknowledge
		if 	TCP_P0_wen_rq_ack_pkt = '1'	 and TCP_P0_req_ack_ongoing = '0'  then
				TCP0_ETH_ack_REQ	<= '1';
		elsif TCP0_ETH_ack_GNT = '1' then
				TCP0_ETH_ack_REQ	<= '0';
		end if;
		
		
			-- transfer a packet without data
		if 	(TCP_P1_wen_rq_Send_pkt = '1' and TCP_P1_pckt_wo_Send_Pkt = '1') or
			-- transfer a packet with data (wait for data ready)
			(TCP_P1_req_snd_ongoing = '1' and ETH1_send_pckt_wo_data = '0' and UR1_blk_ready = '1') then
				TCP1_ETH_snd_REQ	<= '1';
		elsif TCP1_ETH_snd_GNT = '1'  then
				TCP1_ETH_snd_REQ	<= '0';
		end if;	
		
		-- transfer an acknowledge
		if 	 TCP_P1_wen_rq_ack_pkt = '1' and TCP_P1_req_ack_ongoing = '0' then
				TCP1_ETH_ack_REQ	<= '1';
		elsif TCP1_ETH_ack_GNT = '1'  then
				TCP1_ETH_ack_REQ	<= '0';
		end if;
	end if;
end process;


-- Latch TCP parameters for Ack package
process(tx_usr_clk)
begin
	if rising_edge(tx_usr_clk) then  
		if 	TCP_P0_wen_rq_ack_pkt = '1'	 and TCP_P0_req_ack_ongoing = '0'  then
            AckP0_TCP_P0_SEQ_R_Ack_Pkt       <= TCP_P0_SEQ_R_Ack_Pkt;	
            AckP0_TCP_P0_ACK_R_Ack_Pkt       <= TCP_P0_ACK_R_Ack_Pkt;
            AckP0_TCP_P0_FLAG_R_Ack_Pkt      <= TCP_P0_FLAG_R_Ack_Pkt;
            AckP0_TCP_P0_Opt_R_Ack_Pkt       <= TCP_P0_Opt_R_Ack_Pkt;
            AckP0_PORT0_D                    <= PORT0_D;
            AckP0_PORT0_S                    <= PORT0_S;
        end if;
        
		if 	TCP_P1_wen_rq_ack_pkt = '1'	 and TCP_P1_req_ack_ongoing = '0'  then
            AckP1_TCP_P1_SEQ_R_Ack_Pkt      <= TCP_P1_SEQ_R_Ack_Pkt;	
            AckP1_TCP_P1_ACK_R_Ack_Pkt      <= TCP_P1_ACK_R_Ack_Pkt;
            AckP1_TCP_P1_FLAG_R_Ack_Pkt     <= TCP_P1_FLAG_R_Ack_Pkt;
            AckP1_TCP_P1_Opt_R_Ack_Pkt      <= TCP_P1_Opt_R_Ack_Pkt;
            AckP1_PORT1_D                   <= PORT1_D;
            AckP1_PORT1_S                   <= PORT1_S;
        end if;

    end if;
end process;
 
--
-- arbiter
--
process(Rst_tx_usr_clk_n,tx_usr_clk)
begin
	if Rst_tx_usr_clk_n = '0' then
		TCP0_ETH_snd_GNT			<= '0'; 
		TCP1_ETH_snd_GNT			<= '0'; 
		TCP0_ETH_ack_GNT			<= '0'; 
		TCP1_ETH_ack_GNT			<= '0'; 
		UR_Part_selection_0_1_reg	<= '0';
	elsif rising_edge(tx_usr_clk)  then
	 
		if 		(TCP0_ETH_snd_REQ = '1' and toggle_GNT = "000" and TCP0_ETH_snd_GNT = '0')  then
			TCP0_ETH_snd_GNT			<= '1'; 
			UR_Part_selection_0_1_reg	<= '0';
		elsif  	(TCP0_ETH_ack_REQ = '1' and toggle_GNT = "010" and TCP0_ETH_ack_GNT = '0')  then
			TCP0_ETH_ack_GNT			<= '1'; 
		elsif  	(TCP1_ETH_snd_REQ = '1' and toggle_GNT = "100" and TCP1_ETH_snd_GNT = '0')  then
			TCP1_ETH_snd_GNT			<= '1'; 
			UR_Part_selection_0_1_reg	<= '1';
		elsif  	(TCP1_ETH_ack_REQ = '1' and toggle_GNT = "110" and TCP1_ETH_ack_GNT = '0')  then
			TCP1_ETH_ack_GNT			<= '1'; 
			
		elsif TCP_Px_request_done = '1'  then-- release the GNT when the transfer to LINK is done
			TCP0_ETH_snd_GNT			<= '0';
			TCP1_ETH_snd_GNT			<= '0';
			TCP0_ETH_ack_GNT			<= '0'; 
			TCP1_ETH_ack_GNT			<= '0'; 
		end if;
		 
	end if;
end process;

--
--	Toogle to give a access in round robin
--
process(tx_usr_clk)
begin
	if rising_edge(tx_usr_clk)  then
--		if TCP_Px_request_done = '1' and (TCP0_ETH_snd_GNT = '1' or TCP1_ETH_snd_GNT = '1' or TCP0_ETH_ack_GNT = '1' or TCP1_ETH_ack_GNT = '1') then
--			toggle_GNT	<= toggle_GNT + '1';
--		elsif (TCP0_ETH_snd_GNT = '0' and TCP1_ETH_snd_GNT = '0' and TCP0_ETH_ack_GNT = '0' and TCP1_ETH_ack_GNT = '0') then
--			if 		TCP0_ETH_snd_REQ = '0' and toggle_GNT = "000" then
--				toggle_GNT	<= toggle_GNT + '1';
--			elsif	TCP0_ETH_ack_REQ = '0' and toggle_GNT = "010" then
--				toggle_GNT	<= toggle_GNT + '1';			
--			elsif	TCP1_ETH_snd_REQ = '0' and toggle_GNT = "100" then
--				toggle_GNT	<= toggle_GNT + '1';
--			elsif	TCP1_ETH_ack_REQ = '0' and toggle_GNT = "110" then
--				toggle_GNT	<= toggle_GNT + '1';
--			end if;		
        if TCP0_ETH_snd_GNT = '0' and TCP0_ETH_ack_GNT = '0' and TCP1_ETH_snd_GNT = '0' and TCP1_ETH_ack_GNT = '0' then
             toggle_GNT	<= toggle_GNT + '1';
		end if;

	end if;
end process;




--************************************************************************************	
-- mux the parameters used for TCP packet (from two state machine)	
process(tx_usr_clk)	
begin
	if rising_edge(tx_usr_clk) then
			--- parameters coming from TCP logic to build the packet in preparation
		if  	TCP0_ETH_snd_GNT = '1' then 			-- for packet with data/open
			TCP_Px_SEQ_Send_out			<= SendP0_TCP_P0_SEQ_Send_Send_Pkt;	
			TCP_Px_ACK_Send_out 		<= SendP0_TCP_P0_ACK_Send_Send_Pkt;		
			TCP_Px_FLAG_Send_out		<= SendP0_TCP_P0_FLAG_Send_Send_Pkt;
			TCP_Px_option_Send_out 		<= SendP0_TCP_P0_option_Send_Send_Pkt;	
			TCP_Px_pckt_wo_dt 			<= SendP0_ETH0_send_pckt_wo_data;
			TCP_Px_Length_Send_out		<= SendP0_TCP_P0_Length_Send_Send_Pkt;		
			PORTx_D						<= SendP0_PORT0_D;
			PORTx_S						<= SendP0_PORT0_S;
		elsif  	TCP0_ETH_ack_GNT = '1' then 			-- for packet for acknowledge 
			TCP_Px_SEQ_Send_out			<= AckP0_TCP_P0_SEQ_R_Ack_Pkt;	
			TCP_Px_ACK_Send_out 		<= AckP0_TCP_P0_ACK_R_Ack_Pkt;
			TCP_Px_FLAG_Send_out		<= AckP0_TCP_P0_FLAG_R_Ack_Pkt;
			TCP_Px_option_Send_out 		<= AckP0_TCP_P0_Opt_R_Ack_Pkt;
			TCP_Px_pckt_wo_dt   		<= '1';
			TCP_Px_Length_Send_out 		<= (others => '0');	
			PORTx_D						<= AckP0_PORT0_D;
			PORTx_S						<= AckP0_PORT0_S; 
		elsif 	TCP1_ETH_snd_GNT = '1' then 
			TCP_Px_SEQ_Send_out			<= SendP1_TCP_P1_SEQ_Send_Send_Pkt;	
			TCP_Px_ACK_Send_out 		<= SendP1_TCP_P1_ACK_Send_Send_Pkt;	
			TCP_Px_FLAG_Send_out		<= SendP1_TCP_P1_FLAG_Send_Send_Pkt;
			TCP_Px_option_Send_out 		<= SendP1_TCP_P1_option_Send_Send_Pkt;
			TCP_Px_pckt_wo_dt 			<= SendP1_ETH1_send_pckt_wo_data;
			TCP_Px_Length_Send_out		<= SendP1_TCP_P1_Length_Send_Send_Pkt;
			PORTx_D						<= SendP1_PORT1_D;
			PORTx_S						<= SendP1_PORT1_S;
		elsif  	TCP1_ETH_ack_GNT = '1'then
			TCP_Px_SEQ_Send_out			<= AckP1_TCP_P1_SEQ_R_Ack_Pkt;	
			TCP_Px_ACK_Send_out 		<= AckP1_TCP_P1_ACK_R_Ack_Pkt;
			TCP_Px_FLAG_Send_out		<= AckP1_TCP_P1_FLAG_R_Ack_Pkt;
			TCP_Px_option_Send_out 		<= AckP1_TCP_P1_Opt_R_Ack_Pkt;
			TCP_Px_pckt_wo_dt   		<= '1';
			TCP_Px_Length_Send_out 		<= (others => '0');	
			PORTx_D						<= AckP1_PORT1_D;
			PORTx_S						<= AckP1_PORT1_S; 
		end if;
		
	end if;
end process;

process(Rst_tx_usr_clk_n,tx_usr_clk)
begin
    if Rst_tx_usr_clk_n = '0' then
        Request_to_send_Eth_packet_memory   <= '0'; 
	    Request_to_send_Eth_packet          <= '0';
	      
    elsif rising_edge(tx_usr_clk) then
        -- avoid any other request before the previous is executed up to the end
        if TCP_Px_request_done = '1'  then
            Request_to_send_Eth_packet_memory   <= '0'; 
        elsif  TCP0_ETH_snd_GNT = '1' or  TCP0_ETH_ack_GNT = '1' or TCP1_ETH_snd_GNT = '1'  or TCP1_ETH_ack_GNT = '1' then
            Request_to_send_Eth_packet_memory   <= '1'; 
        end if;
         
	   -- pulse until the request is taken in account 
	    if TCP_Px_request_took = '1'  then
	      Request_to_send_Eth_packet <= '0';
		elsif ( TCP0_ETH_snd_GNT = '1' or  TCP0_ETH_ack_GNT = '1' or TCP1_ETH_snd_GNT = '1'  or TCP1_ETH_ack_GNT = '1')  and  Request_to_send_Eth_packet_memory  = '0' then
		  Request_to_send_Eth_packet <= '1';
		end if;
        
    end if;
end process;
  
--
--  request on ETHERNET trasnfer

--Request_to_send_Eth_packet	<= '1'when  TCP0_ETH_snd_GNT = '1' or  TCP0_ETH_ack_GNT = '1' or TCP1_ETH_snd_GNT = '1'  or TCP1_ETH_ack_GNT = '1' else '0';

TCP_P0_request_took			<= '1' when TCP_Px_request_done = '1' and  TCP0_ETH_snd_GNT = '1' else '0';
TCP_P1_request_took			<= '1' when TCP_Px_request_done = '1' and  TCP1_ETH_snd_GNT = '1' else '0';

UR_Part_selection_0_1		<= UR_Part_selection_0_1_reg;

end Behavioral;
