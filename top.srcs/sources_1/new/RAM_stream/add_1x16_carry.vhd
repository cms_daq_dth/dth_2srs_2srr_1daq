----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 18.10.2018 13:23:55
-- Design Name: 
-- Module Name: add_16x16 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
-- finish accum the upper bit witht the lower  and the carry
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity add_1x16_carry is
	Port ( 
		aclrp				: IN STD_LOGIC  := '0';
		clock				: IN STD_LOGIC  := '0';
		finish_Checksum		: in std_logic;		-- a pulse add 21..16  + 15..0   and the next internal pulse add the carry
		data0x				: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		result				: OUT STD_LOGIC_VECTOR (15 DOWNTO 0);
		cs_done				: out std_logic
);
end add_1x16_carry;

architecture Behavioral of add_1x16_carry is

signal data0_reg			: std_logic_vector(15 downto 0) := (others => '0');
signal data1_reg			: std_logic_vector(15 downto 0) := (others => '0');
signal data2_reg			: std_logic_vector(15 downto 0) := (others => '0'); 
signal data3_reg			: std_logic_vector(15 downto 0) := (others => '0'); 

signal result_reg			: std_logic_vector(31 downto 0);

signal internal_pusle		: std_logic;
signal cs_done_reg			: std_logic;

attribute mark_debug : string;
attribute mark_debug of cs_done_reg					: signal is "true"; 

--*********************************************************************
--******************     CODE    START     HERE        ****************
--*********************************************************************
begin

data0_reg(15 downto 0)	<= data0x(15 downto 0);
data1_reg(15 downto 0)	<= data0x(31 downto 16);


data2_reg(15 downto 0)	<= result_reg(31 downto 16);
data3_reg(15 downto 0)	<= result_reg(15 downto 0);



process(aclrp,clock)
begin
	if aclrp = '1' then
		result_reg		<= (others => '0');
	elsif rising_edge(clock) then
		if 		finish_Checksum = '1' then
			result_reg <= (x"0000" & data0_reg) + (x"0000" & data1_reg);
		elsif 	internal_pusle = '1' then
			result_reg <= (x"0000" & data2_reg) + (x"0000" & data3_reg);
		end if;
		
		cs_done_reg		<= internal_pusle;
		internal_pusle	<= finish_Checksum;
	end if;
end process;

result		<= result_reg(15 downto 0);
cs_done		<= cs_done_reg;
end Behavioral;
