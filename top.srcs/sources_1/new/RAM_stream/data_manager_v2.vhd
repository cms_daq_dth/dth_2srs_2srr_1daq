----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 19.10.2018 15:25:16
-- Design Name: 
-- Module Name: data_manager_v2 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

use work.interface.all;
use work.address_table.all;  
 

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity data_manager_v2 is 
 Generic(addr_offset_100G_eth		: integer := 0);
  Port ( 
		usr_clk								: in std_logic;
		usr_rst_n							: in std_logic;
		usr_func_wr							: in std_logic_vector(16383 downto 0); 
		usr_wen								: in std_logic;
		usr_data_wr							: in std_logic_vector(63 downto 0); 
		
		usr_func_rd					      	: in std_logic_vector(16383 downto 0); 
		usr_rden					      	: in std_logic;
		usr_data_rd					      	: out std_logic_vector(63 downto 0);  
		resetp_counter_i                     : in std_logic;  

        -- Data from SRout
		clock_Buffer						: in std_logic;	
		data_for_UR_mem	    				: in std_logic_vector(127 downto 0);
		size_for_UR_mem	    				: in std_logic_vector(10 downto 0);
		ready_for_UR_mem					: in std_logic;
		read_for_UR_mem	    				: out std_logic;		
		
        -- Request from TCP logic
		clock_TCP							: in std_logic;
		
		UR_MEM_req_rd_blk					: in std_logic;
		UR_MEM_Size_request					: in std_logic_vector(31 downto 0); -- number of bytes
		UR_MEM_Add_request					: in std_logic_vector(31 downto 0); -- SND_SEQ# (in bytes)
		UR_MEM_request_done					: out std_logic;
 
		Packet_ready						: out std_logic; -- SND_SEQ# (in bytes)
		eth_data_CS16						: out std_logic_vector(15 downto 0);
		eth_ack_packet						: in std_logic;	-- pulse to read CS and release the place
		eth_rd_data							: in std_logic;
		PL_TX_lbus_A	   					: out tx_usr_record;
		PL_TX_lbus_B	   					: out tx_usr_record;
		PL_TX_lbus_C	   					: out tx_usr_record;
		PL_TX_lbus_D	   					: out tx_usr_record; -- bus of data packet
		last_data							: out std_logic;
		backpressure						: in std_logic;
					
		SND_NEW_A							: out std_logic_vector(31 downto 0); 	-- value of the latest data written in BIFI	WRITE POINTER
		SND_UNA_A							: in std_logic_vector(31 downto 0); 	-- latest data acknowledge						READ POINTER
		VALID_P_A							: in std_logic;
		resetp_FED0							: in std_logic 
  );
end data_manager_v2;

--$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
architecture Behavioral of data_manager_v2 is

signal usr_data_rd_reg          : std_logic_vector(63 downto 0);

signal clock_Buffer_rstn		: std_logic;
 
attribute mark_debug : string;

type read_memory_block_type is (idle,
						read_size,
						write_data
						--wait_st
						);
signal WRITE_mem_block:read_memory_block_type;
 
signal block_size_cnt_down					: std_logic_vector(10 downto 0);
signal read_memory_block					: std_logic_vector(1 downto 0);
  
signal SND_NEW_A_cell                               : std_logic_vector(31 downto 0);
signal BIFI_size_reg                                : std_logic_vector(31 downto 0);
signal BIFI_HF		                                : std_logic;
signal BIFI_used	                                : std_logic_vector(63 downto 0);	
signal BIFI_in_backpressure                         : std_logic_vector(63 downto 0);	
signal BIFI_max 	                                : std_logic_vector(63 downto 0);	
signal BIFI_Write_pointer                           : std_logic_vector(63 downto 0);	

signal SR_data_ready								: std_logic;
signal SR_wr_ena_out								: std_logic; 
signal SR_data_out									: std_logic_vector(127 downto 0);
signal SR_EOF										: std_logic;
signal Almost_FULL									: std_logic;

signal PL_TX_lbus_A_reg								: tx_usr_record;
signal PL_TX_lbus_B_reg								: tx_usr_record;
signal PL_TX_lbus_C_reg								: tx_usr_record;
signal PL_TX_lbus_D_reg								: tx_usr_record;

signal eth_last_dt_o								: std_logic;
signal last_data_rg									: std_logic;
signal eth_data_ena_o								: std_logic_vector(3 downto 0);
signal data_ena_lut									: std_logic;
signal eth_data_o									: std_logic_vector(511 downto 0);
signal data_ready_rg								: std_logic;
signal size_counter		 							: std_logic_vector(15 downto 0);

signal data_tmp										: std_logic_vector(511 downto 0);
signal data_ena_tmp									: std_logic_vector(3 downto 0); 

signal dt_rstn										: std_logic;
signal count_down									: std_logic;

signal last_data_reg								: std_logic;
signal eth_rd_data_cell								: std_logic;

attribute mark_debug of BIFI_size_reg				: signal is "true"; 
 
--#########################################################
--##############    CODE    START    HERE    ##############
--#########################################################
begin

reset_resync_i1:entity work.resetn_resync  
port map(
	aresetn				=> usr_rst_n,
	clock				=> clock_Buffer,
	Resetn_sync			=> clock_Buffer_rstn 
	);

--=======================================================================
-- Create a pulse to load the size according the availability of the data
--
--
WRITE_mem_clock:process(clock_Buffer_rstn,clock_Buffer)
begin
	if clock_Buffer_rstn = '0' then
		WRITE_mem_block <= idle;
	elsif rising_edge(clock_Buffer) then
	
		Case WRITE_mem_block is
			when idle =>
				if ready_for_UR_mem = '1' then
					WRITE_mem_block <= read_size;
				end if;
				
			when read_size =>
				WRITE_mem_block <= write_data;		
				
			when write_data =>
				if block_size_cnt_down = "0000000000" then
					WRITE_mem_block <= idle;	
				end if;

			when others =>
				WRITE_mem_block <= idle;
		end case;
 
	end if;
end process;
  
--=======================================================================
-- manage the size of the block 
--
--
process(clock_Buffer_rstn,clock_Buffer)
begin
	if clock_Buffer_rstn = '0' then
		block_size_cnt_down 			<= (others => '0');
	elsif rising_edge(clock_Buffer) then
		if WRITE_mem_block = read_size then
			block_size_cnt_down		<= size_for_UR_mem;
		elsif block_size_cnt_down /= "00000000000" and Almost_FULL = '0' and WRITE_mem_block = write_data then
			block_size_cnt_down		<= block_size_cnt_down - '1';
		end if;
	end if;
end process;

--=======================================================================
-- read the FIFO from memory_block 
-- and write the Data in QDR interface 1 clock after
--
process(clock_Buffer_rstn,clock_Buffer)
begin
	if clock_Buffer_rstn = '0' then
		read_memory_block 			<= (others => '0');
	elsif rising_edge(clock_Buffer) then
		read_memory_block(1)		<= read_memory_block(0);
	
		read_memory_block(0)		<= '0';
		if block_size_cnt_down /= "00000000000" and Almost_FULL = '0' and WRITE_mem_block = write_data then
			read_memory_block(0)	<= '1';
		end if;
	end if;
end process;

read_for_UR_mem			<= read_memory_block(0);
SR_wr_ena_out			<= read_memory_block(1);

--=======================================================================
-- manage the end of the block 
--
--

SR_data_out			<= data_for_UR_mem;

process(clock_Buffer_rstn,clock_Buffer)
begin
	if clock_Buffer_rstn = '0' then
		SR_EOF 			<= '0';
	elsif rising_edge(clock_Buffer) then
		SR_EOF				<= '0';
		if block_size_cnt_down = "00000000000" and read_memory_block(0) = '1' then
			SR_EOF			<= '1';
		end if;
	end if;
end process;

 --@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  
BIFI_i1:entity work.BIFI_itf_2x  
 port map(
 
	---------------------------------------------
	-- interface BIFI  BIg FIfo
	---------------------------------------------
	-- input port
	resetn					=> usr_rst_n		,
		 
	clock_UR				=> clock_Buffer		,
	--write data to BIFI	=>					,
	clock_w					=> clock_Buffer		,-- input data to TCP stream (UltraRAM) 
	SR_wr_ena				=> SR_wr_ena_out	,
	SR_data					=> SR_data_out		,
	SR_EOF					=> SR_EOF			,
	Almost_FULL				=> Almost_FULL		,
		 
	--read data from BIFI	=>					,
	clock_r					=> clock_TCP		,
	req_read				=> UR_MEM_req_rd_blk			,
	req_size				=> UR_MEM_Size_request			,-- number of bytes
	req_add_r				=> UR_MEM_Add_request			,-- SND_SEQ# (in bytes)
	Packet_ready			=> Packet_ready		,
	request_done			=> UR_MEM_request_done		,

	eth_data_CS16			=> eth_data_CS16		, 
	eth_ack_packet			=> eth_ack_packet		,	-- pulse to read CS and release the place
	eth_rd_data				=> eth_rd_data_cell		,	
	eth_data_o				=> eth_data_o		, 
	eth_data_ena_o			=> eth_data_ena_o	,
	eth_last_dt_o			=> eth_last_dt_o	,
	                           			
	-- manage the empty full BIFI
		-- FED number 0        
	SND_NEW_A				=> SND_NEW_A_cell		, 	-- value of the lastest data written in BIFI	WRITE POINTER
	SND_UNA_A				=> SND_UNA_A		,	-- lastest data acknoledge						READ POINTER
	VALID_P_A				=> VALID_P_A		,
	resetp_FED0				=> resetp_FED0		,
	            
    BIFI_size               => BIFI_size_reg    ,  
	BIFI_A_HF				=> BIFI_HF		,
	BIFI_used_A				=> BIFI_used	  ,
	Write_pointer           => BIFI_Write_pointer
 );   
 
SND_NEW_A   <= SND_NEW_A_cell;

-- remove the last read after the last data (logic to difficult to stop it faster (timing problem) 
eth_rd_data_cell			<= '1' when eth_rd_data = '1' and last_data_reg = '0' else '0';

-- measure the time of BIFI in backpressure
process(clock_Buffer_rstn,clock_Buffer)
begin
	if clock_Buffer_rstn = '0'  then 
			BIFI_in_backpressure<= (others => '1');
	elsif rising_edge(clock_Buffer) then
		if BIFI_HF = '1' then
			BIFI_in_backpressure <= BIFI_in_backpressure + '1';
		end if;
	end if;
end process;

 --@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
--  Send out the packet


 
--  Format to 100G MAC Xilinx
--  for TCP packet the payload start to be sent on PL_TX_lbus_D (bit 79..0) 
process(clock_TCP) 
begin
	if rising_edge(clock_TCP) then
		if backpressure = '0' then
		-- Ethernet word 000..127
			PL_TX_lbus_A_reg.data_bus		<= eth_data_o(127 downto 000);
			PL_TX_lbus_A_reg.data_ena		<= eth_data_ena_o(0);
			PL_TX_lbus_A_reg.eopckt			<= '0';
			if eth_data_ena_o(1) = '0' and eth_data_ena_o(0)  = '1' then
				PL_TX_lbus_A_reg.eopckt		<= eth_last_dt_o;
			end if;
			PL_TX_lbus_A_reg.sopckt			<= '0';
			PL_TX_lbus_A_reg.err_pckt		<= '0';
		
			if 		eth_data_ena_o(1)  = '0'  then
				PL_TX_lbus_A_reg.empty_pckt	<= x"A";
			else
				PL_TX_lbus_A_reg.empty_pckt	<= x"0";
			end if;
	
		-- Ethernet word 128..255	
			PL_TX_lbus_B_reg.data_bus		<= eth_data_o(255 downto 128);
			PL_TX_lbus_B_reg.data_ena		<= eth_data_ena_o(1);
			PL_TX_lbus_B_reg.eopckt			<= '0';
			if eth_data_ena_o(2) = '0' and eth_data_ena_o(1)  = '1' then
				PL_TX_lbus_B_reg.eopckt		<= eth_last_dt_o;
			end if;
			PL_TX_lbus_B_reg.sopckt			<= '0';
			PL_TX_lbus_B_reg.err_pckt		<= '0';
		
			if 		eth_data_ena_o(2)  = '0' then
				PL_TX_lbus_B_reg.empty_pckt		<= x"A";
			else
				PL_TX_lbus_B_reg.empty_pckt		<= x"0";
			end if;
		
		-- Ethernet word 256..383		
			PL_TX_lbus_C_reg.data_bus		<= eth_data_o(383 downto 256);
			PL_TX_lbus_C_reg.data_ena		<= eth_data_ena_o(2);
			PL_TX_lbus_C_reg.eopckt			<= '0';
			if eth_data_ena_o(3) = '0' and eth_data_ena_o(2)  = '1' then
				PL_TX_lbus_C_reg.eopckt		<= eth_last_dt_o;
			end if;PL_TX_lbus_C_reg.sopckt			<= '0';
			PL_TX_lbus_C_reg.err_pckt		<= '0';
		
			if 		eth_data_ena_o(3)  = '0' then
				PL_TX_lbus_C_reg.empty_pckt		<= x"A";
			else
				PL_TX_lbus_C_reg.empty_pckt		<= x"0";
			end if;
	
			-- Ethernet word 384..511
			PL_TX_lbus_D_reg.data_bus		<= eth_data_o(511 downto 384);
			PL_TX_lbus_D_reg.data_ena		<= eth_data_ena_o(3);
			PL_TX_lbus_D_reg.eopckt			<= '0';
			if   eth_data_ena_o(3)  = '1' then
				PL_TX_lbus_D_reg.eopckt		<= eth_last_dt_o;
			end if;PL_TX_lbus_D_reg.sopckt			<= '0';
			PL_TX_lbus_D_reg.err_pckt		<= '0';
		
			if 		eth_data_ena_o(3)  = '1' and eth_last_dt_o = '1' then
				PL_TX_lbus_D_reg.empty_pckt		<= x"A";
			else
				PL_TX_lbus_D_reg.empty_pckt		<= x"0";
			end if;
			
			last_data_reg	<= eth_last_dt_o;
 		end if;
	end if;
end process;
last_data			<= last_data_reg;
PL_TX_lbus_A	   	<= PL_TX_lbus_A_reg;
PL_TX_lbus_B	   	<= PL_TX_lbus_B_reg;
PL_TX_lbus_C	   	<= PL_TX_lbus_C_reg;
PL_TX_lbus_D	   	<= PL_TX_lbus_D_reg;
 
 
 
--******************************************************************************************
--  user access read for status


process(usr_rst_n,usr_clk)
begin
	if usr_rst_n = '0'  then
			BIFI_size_reg    	<= (others => '1'); 
	elsif rising_edge(usr_clk) then
		if usr_wen = '1' then

			if 	usr_func_wr(addr_offset_100G_eth + BIFI_size) = '1' then
				BIFI_size_reg   			<= usr_data_wr(31 downto 0);
			end if;
			
		end if;
	end if;
end process;


process(usr_clk)
begin
	if rising_edge(usr_clk) then
		usr_data_rd_reg 							       	<= (others => '0');	 
			if 		usr_func_rd(addr_offset_100G_eth + BIFI_BackPressure) = '1' then
				usr_data_rd_reg          	<= BIFI_in_backpressure;
			elsif usr_func_rd(addr_offset_100G_eth + BIFI_used_off) = '1' then
				usr_data_rd_reg         	<= BIFI_used;
			elsif usr_func_rd(addr_offset_100G_eth + BIFI_Max_used) = '1' then
				usr_data_rd_reg         	<= BIFI_max;
			elsif usr_func_rd(addr_offset_100G_eth + BIFI_size) = '1' then
				usr_data_rd_reg(31 downto 0)<= BIFI_size_reg;
			elsif usr_func_rd(addr_offset_100G_eth + BIFI_wr_pointer_item) = '1' then
				usr_data_rd_reg(63 downto 0)<=  BIFI_Write_pointer; 
			elsif usr_func_rd(addr_offset_100G_eth + BIFI_new_SND_item) = '1' then
				usr_data_rd_reg(31 downto 0)<=  SND_NEW_A_cell; 
			end if;			
	end if;
end process;
	 
process(resetp_counter_i,usr_clk)
begin
    if resetp_counter_i = '1' then
        BIFI_max    <= (others => '0');
	elsif rising_edge(usr_clk) then			
		if BIFI_max < BIFI_used then
		  BIFI_max <= BIFI_used;
		end if;
	end if;
end process;
 
usr_data_rd <= usr_data_rd_reg;
 
 
end Behavioral;