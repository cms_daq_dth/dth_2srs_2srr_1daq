------------------------------------------------------
-- BIFI interface (QDRII  BIgFIfo)
--
--  Ver 1.00
--
-- Dominique Gigi August 2012
------------------------------------------------------
--  QDRII 2 data burst 
--  Add access PCI + arbiter
--
--
------------------------------------------------------
LIBRARY ieee; 

USE ieee.std_logic_1164.all;
--use ieee.std_logic_arith.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

  
entity BIFI_itf_2x is
 generic (Latency_read	: integer := 3  -- one latency to shift 128b word to match with the read_add requested
		); 
 port (
 
	---------------------------------------------
	-- interface BIFI  BIg FIfo
	---------------------------------------------
	-- input port
	resetn				: in std_logic;
	 
	clock_UR			: in std_logic;
	--write data to BIFI
	clock_w				: in std_logic; 
	SR_wr_ena			: in std_logic; 
	SR_data				: in std_logic_vector(127 downto 0);
	SR_EOF				: in std_logic;
	Almost_FULL			: out std_logic;
	
	--read data from BIFI
	clock_r				: in std_logic;
	req_read			: in std_logic;
	req_size			: in std_logic_vector(31 downto 0); -- number of bytes
	req_add_r			: in std_logic_vector(31 downto 0); -- SND_SEQ# (in bytes)
	Packet_ready		: out std_logic;
	request_done		: out std_logic;
	
	eth_data_CS16		: out std_logic_vector(15 downto 0);
	eth_ack_packet		: in std_logic;	-- pulse to read CS and release the place
	eth_rd_data			: in std_logic;
	eth_data_o			: out std_logic_vector(511 downto 0);
	eth_data_ena_o		: out std_logic_vector(3 downto 0);
	eth_last_dt_o		: out std_logic;
	
	-- manage the empty full BIFI
		-- FED number 0
	SND_NEW_A			: out std_logic_vector(31 downto 0); 	-- value of the lastest data written in BIFI	WRITE POINTER
	SND_UNA_A			: in std_logic_vector(31 downto 0); 	--lastest data acknoledge						READ POINTER
	VALID_P_A			: in std_logic;
	resetp_FED0			: in std_logic;
  
    BIFI_size           : in std_logic_vector(31 downto 0);
	BIFI_A_HF			: out std_logic; 
	BIFI_used_A			: out std_logic_vector(63 downto 0) ;
	Write_pointer       : out std_logic_vector(63 downto 0)
 		
 );

end BIFI_itf_2x;


--******************************** ARCHITECTURE **************************************************
architecture behavioral of BIFI_itf_2x is

attribute mark_debug : string; 

type READ_mem_type is (	idle, 
						ld_add,
						read_data,
						end_read
						--wait_st
						);
signal READ_mem:READ_mem_type;
 
component FIFO_DC_517x2048_norm -- 2048 x 517bit =  
	PORT
	(
    rst 			: IN STD_LOGIC;
	wr_clk 			: IN STD_LOGIC;
	rd_clk 			: IN STD_LOGIC;
	din 			: IN STD_LOGIC_VECTOR(516 DOWNTO 0);
	wr_en 			: IN STD_LOGIC;
	rd_en 			: IN STD_LOGIC;
	dout 			: OUT STD_LOGIC_VECTOR(516 DOWNTO 0);
	full 			: OUT STD_LOGIC;
	empty 			: OUT STD_LOGIC;
	wr_data_count 	: OUT STD_LOGIC_VECTOR(10 DOWNTO 0);
	wr_rst_busy 	: OUT STD_LOGIC;
	rd_rst_busy 	: OUT STD_LOGIC
	);
end component;

COMPONENT FIFO_DC_517x2048_FWFT
  PORT (
    rst 			: IN STD_LOGIC;
    wr_clk 			: IN STD_LOGIC;
    rd_clk 			: IN STD_LOGIC;
    din 			: IN STD_LOGIC_VECTOR(516 DOWNTO 0);
    wr_en 			: IN STD_LOGIC;
    rd_en 			: IN STD_LOGIC;
    dout 			: OUT STD_LOGIC_VECTOR(516 DOWNTO 0);
    full 			: OUT STD_LOGIC;
    empty 			: OUT STD_LOGIC;
    wr_data_count 	: OUT STD_LOGIC_VECTOR(10 DOWNTO 0);
    rd_data_count 	: OUT STD_LOGIC_VECTOR(10 DOWNTO 0);
    wr_rst_busy 	: OUT STD_LOGIC;
    rd_rst_busy 	: OUT STD_LOGIC
  );
END COMPONENT;

COMPONENT FIFO_dt_ready_UR
  PORT (
    rst 		: IN STD_LOGIC;
  	wr_clk 		: IN STD_LOGIC;
  	rd_clk 		: IN STD_LOGIC;
  	din 		: IN STD_LOGIC_VECTOR(0 DOWNTO 0);
  	wr_en 		: IN STD_LOGIC;
  	rd_en 		: IN STD_LOGIC;
  	dout 		: OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
  	full 		: OUT STD_LOGIC;
  	wr_data_count  : out std_logic_vector(9 downto 0);
  	empty 		: OUT STD_LOGIC;
 	wr_rst_busy : OUT STD_LOGIC;
  	rd_rst_busy : OUT STD_LOGIC
  );
END COMPONENT;
  
component stream_buffer is
  Port ( 
		resetp							: in std_logic;
  		clock							: in std_logic;
  			
  		address_wr						: in std_logic_vector(13 downto 0);
  		data_i							: in std_logic_vector(511 downto 0);
  		write_ena						: in std_logic;
  		word_enable						: in std_logic_vector(3 downto 0);--128-bit access
  
  		address_rd						: in std_logic_vector(13 downto 0);
  		data_o							: out std_logic_vector(511 downto 0); 
  		read_ena						: in std_logic
  	);
end component;

component CS_Compute is
	Port ( 
		aclrp		: IN std_logic;
		clock		: IN std_logic;
		clken		: IN std_logic  := '1';
		last_word	: in std_logic;
		data		: IN std_logic_vector (511 DOWNTO 0);
		data_ena	: in std_logic_vector(3 downto 0);
		
		result		: OUT std_logic_vector (15 DOWNTO 0);
		CS_done		: out std_logic
);
end component;

COMPONENT FIFO_eth_CS
  PORT (
    rst         	: IN STD_LOGIC;
    wr_clk      	: IN STD_LOGIC;
    rd_clk      	: IN STD_LOGIC;
    din         	: IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    wr_en       	: IN STD_LOGIC;
    rd_en       	: IN STD_LOGIC;
    dout        	: OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    full        	: OUT STD_LOGIC;
    empty       	: OUT STD_LOGIC;
    wr_rst_busy 	: OUT STD_LOGIC;
    rd_rst_busy 	: OUT STD_LOGIC
  );
END COMPONENT;

signal CS_result					: std_logic_vector(15 downto 0);
signal CS_done						: std_logic;
 
 
signal resetp 				: std_logic;
signal delay_SR_data_wr		: std_logic_vector(511 downto 0);
signal delay_SR_data_ena	: std_logic_vector(3 downto 0);
signal delay_SR_data_shift	: std_logic_vector(3 downto 0);
signal delay_SR_data_eof	: std_logic;
signal delay_SR_wen			: std_logic;
  
signal wr_ff_used			: std_logic_vector(10 downto 0);
signal wr_ff_empty			: std_logic;
signal dt_to_UltraRAM		: std_logic_vector(511 downto 0);
signal dt_ena_to_UltraRAM	: std_logic_vector(3 downto 0);
signal dt_last_to_UltraRAM	: std_logic; 
signal UR_wr_dt_reg		: std_logic_vector(511 downto 0);
signal UR_wr_BE_reg	: std_logic_vector(3 downto 0);
signal dt_last_to_UltraRAM_reg	: std_logic; 



signal block_ready_to_write	: std_logic; 
signal wr_block_ready_form_UR	: std_logic; 
 
signal memorise_wrd_ena		: std_logic_vector(3 downto 0);
signal UR_actual_wrd_ena	: std_logic_vector(3 downto 0);
 
signal wr_lst_wd			: std_logic; 
signal lst_wd_to_ETH		: std_logic_vector(10 downto 0);
signal lst_wd_to_ETH_cell	: std_logic;
signal UR_rd_to_Wr_FIFO		: std_logic_vector(10 downto 0);
signal UR_rd_to_Wr_FIFO_cell: std_logic;
signal lst_wd_to_ETH_q		: std_logic;
	
signal UR_wr_add_cnt		: std_logic_vector(13 downto 0);  
signal UR_wr_add_cell		: std_logic_vector(13 downto 0);  
signal SND_NEW_A_cnt		: std_logic_vector(31 downto 0);  
signal SND_NEW_A_reg		: std_logic_vector(31 downto 0); 
signal update_SND_NEW		: std_logic;
signal update_SND_NEW_delay : std_logic_vector(1 downto 0);
signal read_packet_ready	: std_logic;
signal ltch_snd_new			: std_logic;
signal rd_inter_FIFO_w		: std_logic;
signal UR_wr_ena			: std_logic;
signal wr2_to_BIFI      	: std_logic;
signal BIFI_full_A			: std_logic;  
	
signal BIFI_used_A_w		: std_logic_vector(13 downto 0); 
signal sync_UNA_A			: std_logic_vector(31 downto 0); 
signal sync_UNA_A_cell		: std_logic_vector(31 downto 0); 
signal sync_valid_p_A		: std_logic; 
	
signal req_read_reg			: std_logic_vector(1 downto 0);

signal req_read_async		: std_logic;
attribute ASYNC_REG						: string;
attribute ASYNC_REG of req_read_async : signal is  "TRUE";


signal UR_rd_add_cnt_reg	: std_logic_vector(13 downto 0);
signal word_align_128b		: std_logic_vector(1 downto 0); -- specify the first 128-b word 

signal rd_size_cnt			: signed(15 downto 0) := x"0000";
signal ena_size_cnt			: signed(15 downto 0) := x"0000"; --used to generate the enable of 128b words
signal read_first_word		: std_logic; 
signal UR_rd_add_cell		: std_logic_vector(13 downto 0);
signal UR_rd_dt_cell		: std_logic_vector(511 downto 0);
signal UR_rd_dt_cell_shft	: std_logic_vector(511 downto 0);
signal UR_rd_dt_cell_tmp 	: std_logic_vector(511 downto 0);
signal dt_to_Eth_FIFO 		: std_logic_vector(511 downto 0);
signal dt_to_Eth_FIFO_cell	: std_logic_vector(511 downto 0);
signal enable_first_word	: std_logic;

signal dt_ena_from_UltraRAM	: std_logic_vector(3 downto 0);
type dt_ena_from_UltraRAM_stg_type	  is array (0 to Latency_read) of std_logic_vector(3 downto 0); 
signal dt_ena_from_UltraRAM_stg		: dt_ena_from_UltraRAM_stg_type; 
signal dt_ena_from_UltraRAM_reg		: std_logic_vector(3 downto 0); 
signal dt_ena_to_Eth_FIFO_reg		: std_logic_vector(3 downto 0); 
signal dt_ena_to_Eth_FIFO			: std_logic_vector(3 downto 0); 

signal UR_rd_ena			: std_logic;
signal rd_ff_used			: std_logic_vector(10 downto 0);
signal last_dt				: std_logic; 
 

	 
signal data_r_cell			: std_logic_vector(511 downto 0); 
signal data_ena_cell		: std_logic_vector(3 downto 0);  
signal data_last_cell		: std_logic;  
	
-- signal mux_add_UR  			: std_logic_vector(23 downto 0); 
 
signal SR_wr_ok				: std_logic;  
	
signal wr_dt_to_fifo_out	: std_logic; 
signal wr_dt_to_fifo_out_1x	: std_logic; 
	
signal start_BIST_sync		: std_logic;
signal Rst_Clock_W			: std_logic;
signal Rstp_Clock_W			: std_logic;
signal Rst_Clock_R			: std_logic;
signal Rst_FED0_G			: std_logic; 
signal Rst_FED0_Clock_R		: std_logic; 
signal Rst_clock_UR			: std_logic;
signal Rstp_clock_UR		: std_logic;
signal reset_FED0_rsync		: std_logic;

signal Reset_CS 			: std_logic;
signal Reset_CS_resync_clk_UR	: std_logic;
signal FIFO_data_ready_n	: std_logic;
  
signal dummy  				: std_logic_vector(3 downto 0); 

signal request_done_reg		: std_logic;
signal nb_packet_ready		: std_logic_vector(9 downto 0);

signal nothing_to_send      : std_logic;
signal nothing_to_send_resync      : std_logic;
signal debug_delay          : std_logic_vector(2 downto 0);
 
signal rd_data_count									: std_logic_vector(10 downto 0);  
 
attribute mark_debug of BIFI_used_A_w					: signal is "true"; 
attribute mark_debug of BIFI_full_A					    : signal is "true"; 
attribute mark_debug of SND_UNA_A					    : signal is "true"; 
attribute mark_debug of VALID_P_A					    : signal is "true"; 
attribute mark_debug of Packet_ready					: signal is "true"; 
attribute mark_debug of UR_wr_ena				        : signal is "true"; 
attribute mark_debug of ltch_snd_new					: signal is "true";
attribute mark_debug of UR_wr_add_cnt		            : signal is "true"; 
attribute mark_debug of SND_NEW_A_cnt					: signal is "true"; 
attribute mark_debug of SND_NEW_A_reg					: signal is "true"; 
attribute mark_debug of SND_NEW_A				        : signal is "true";  
attribute mark_debug of sync_UNA_A					    : signal is "true";  
attribute mark_debug of nothing_to_send   			    : signal is "true";  
attribute mark_debug of update_SND_NEW   			    : signal is "true";  
attribute mark_debug of UR_wr_BE_reg     			    : signal is "true";  
attribute mark_debug of dt_last_to_UltraRAM_reg     	: signal is "true";   

attribute mark_debug of nothing_to_send_resync		    : signal is "true"; 
attribute mark_debug of delay_SR_data_ena     	    : signal is "true";   
attribute mark_debug of SR_wr_ena     			    : signal is "true";  
attribute mark_debug of delay_SR_data_shift     	: signal is "true"; 
attribute mark_debug of delay_SR_wen     	        : signal is "true";  
attribute mark_debug of wr_block_ready_form_UR     	: signal is "true";   
attribute mark_debug of SR_EOF     	                : signal is "true";  
attribute mark_debug of wr_ff_used               	: signal is "true";  
attribute mark_debug of Almost_FULL               	: signal is "true"; 
--*****************************************************************************************
--******************************** BEGIN **************************************************
--*****************************************************************************************
begin
 	
resetp 		<= not(resetn);

reset_resync_i1:entity work.resetn_resync  
port map (
	aresetn			=> resetn,
	clock			=> clock_w, 
	Resetn_sync		=> Rst_Clock_W
	);
	
Rstp_Clock_W	<= not(Rst_Clock_W);	
	
reset_resync_i2:entity work.resetn_resync  
port map (
	aresetn			=> resetn,
	clock			=> clock_r, 
	Resetn_sync		=> Rst_Clock_R
	);
	
reset_resync_i3:entity work.resetn_resync  
port map (
	aresetn			=> resetn,
	clock			=> clock_UR, 
	Resetn_sync		=> Rst_clock_UR
	);
	
Rstp_clock_UR	<= not(Rst_clock_UR);	

reset_resync_i4:entity work.resetp_resync 
port map(
	aresetp			=> resetp_FED0,
	clock			=> clock_UR,
	Resetp_sync		=> reset_FED0_rsync 
	); 	
	
resync_debug:entity work.resync_pulse  
	port map(
		aresetn				=> '1',
		clocki			    => clock_UR,	
		in_s				=> nothing_to_send,
		clocko			    => clock_w,
		out_s			    => nothing_to_send_resync
		);
	

--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
--@@@@@@@@@@@@@@@@@@@@@@@@@@         Interface for DATA IN                        @@@@@@@@@@@@@@@@@@@@@@@@@@
--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

-- Concatanate the 128-bit word from the SlinkRocket to interface them with UR @ 512bit word

process(Rst_Clock_W,clock_w)
begin
	if Rst_Clock_W = '0' then
		delay_SR_wen 				<= '0'; 
		delay_SR_data_shift			<= "0001";
		delay_SR_data_ena			<= (others => '0');
		wr_block_ready_form_UR		<= '0';
	elsif rising_edge(clock_w) then 
	
		-- used to select the 128-bit word place in the 512_bit word
		if SR_wr_ena = '1' then
			delay_SR_data_shift(3 downto 1) <= delay_SR_data_shift(2 downto 0);
			delay_SR_data_shift(0) 			<= delay_SR_data_shift(3);
		end if;
		
		if 		delay_SR_data_eof	= '1' then
			delay_SR_data_ena   	<= "0000";			-- at the end of the block we reset all enable
		elsif 	delay_SR_data_shift(0) = '1' then
			delay_SR_data_ena   	<= "0001"; 			-- the first word (128b) reinitializes the other 3 bits
		elsif 	delay_SR_data_shift(1) = '1' then
			delay_SR_data_ena(1)	<= '1'; 
		elsif 	delay_SR_data_shift(2) = '1' then
			delay_SR_data_ena(2)	<= '1'; 
		elsif 	delay_SR_data_shift(3) = '1' then
			delay_SR_data_ena(3)	<= '1'; 
		end if;
	
		-- write word when 4 are ready or at the end
		delay_SR_wen				<= '0';
		if (delay_SR_data_shift(3) = '1' or SR_EOF = '1') and SR_wr_ena = '1' then
			delay_SR_wen			<= '1';
		end if;
				 
		-- write to Counter FIFO to mention that the data block is READY in resync FIFO 				 
		wr_block_ready_form_UR		<= '0';
		if SR_EOF = '1' and SR_wr_ena = '1' then
			wr_block_ready_form_UR	<= '1';
		end if;	 
				 
	end if;
end process;

-- latch 128-bit word one after the other
process(clock_w)
begin
	if rising_edge(clock_w) then
		if 		delay_SR_data_shift(0) = '1' then
			delay_SR_data_wr(127 downto 000) 	<= SR_data; 
		elsif 	delay_SR_data_shift(1) = '1' then
			delay_SR_data_wr(255 downto 128) 	<= SR_data; 
		elsif 	delay_SR_data_shift(2) = '1' then
			delay_SR_data_wr(383 downto 256) 	<= SR_data; 
		elsif 	delay_SR_data_shift(3) = '1' then
			delay_SR_data_wr(511 downto 384) 	<= SR_data; 
		end if;
			
		delay_SR_data_eof 	 					<= SR_EOF; 
		
	end if;
end process;

Almost_FULL			<= '1' when wr_ff_used > "11111000000" else '0'; 		 -- '1' when almost Full
  
--************************************************************************* 
-- this DTI_sync_FIFO is used to resynchronize SlinkRocket DATA clock to UR clock
FIFO_packet_from_SR:FIFO_DC_517x2048_FWFT
	PORT MAP
	(
		rst					=> resetp             ,	--reset resync done in the IP
		wr_clk				=> clock_w            , 
		din(511 downto 0)	=> delay_SR_data_wr   ,
		din(515 downto 512)	=> delay_SR_data_ena  ,
		din(516)			=> delay_SR_data_eof  ,
		wr_en				=> delay_SR_wen       ,  
		wr_data_count		=> wr_ff_used         , 
		
		rd_clk				=> clock_UR           , 
		rd_en				=> rd_inter_FIFO_w    , 
		dout(511 downto 0)	=> dt_to_UltraRAM     ,
		dout(515 downto 512)=> dt_ena_to_UltraRAM ,
		dout(516)			=> dt_last_to_UltraRAM,
		empty				=> wr_ff_empty 
	);
  
  
process(clock_UR)
begin
	if rising_edge(clock_UR) then
		UR_wr_dt_reg		     <= dt_to_UltraRAM;
		UR_wr_BE_reg	         <= dt_ena_to_UltraRAM;
		dt_last_to_UltraRAM_reg	 <= dt_last_to_UltraRAM;
	end if;
end process;  

--*************************************************************************
-- this FIFO is used to mention that the full block is done
-- This fifo count the number of packet ready to write to UR (at least 2 words per packet => fifo_size = FIFO_packet_from_SR/2
FIFO_packet_ready:FIFO_dt_ready_UR
	  PORT map(
	  	rst					=> Rstp_Clock_W			,
	    wr_clk 				=> clock_w				, 
	    din(0) 				=> '1'					, 
	    wr_en 				=> wr_block_ready_form_UR, 
	    
	    rd_clk				=> clock_UR				, 
	    rd_en 				=> read_packet_ready	, 
	    dout 				=> open					,
	    --full 				=>				, 
	     wr_data_count  	=> nb_packet_ready		,
	      
	    empty 				=> block_ready_to_write	
	    --wr_rst_busy 		=>				, 
	   -- rd_rst_busy	 	=>		
		);

--should be read when the block is finished to be written in UR logic
		
rd_inter_FIFO_w <= '1' when wr_ff_empty = '0' and SR_wr_ok = '1' else '0'; -- read local FIFO	

--**********************************************************************
-- Signal to read DATA from resync_FIFO and write data to UR
process(Rst_clock_UR,clock_UR)
begin
	if Rst_clock_UR = '0' then
		UR_wr_ena 			<= '0'; 
		debug_delay         <= "000";
	elsif rising_edge(clock_UR) then 
		UR_wr_ena 			<= rd_inter_FIFO_w;	-- write to UR
		
		
		debug_delay(2 downto 1) <= debug_delay(1 downto 0);
		debug_delay(0)          <= UR_wr_ena;
	end if;
end process;
	
--update the SND_NEW value when the block is written (at the end of the block)
-- This value is used by the TCP logic to know how many data is ready  mto be sent over  ethernet
process(Rst_clock_UR,clock_UR)
begin
	if Rst_clock_UR = '0' then
		update_SND_NEW 		<= '0'; 
		update_SND_NEW_delay<= "00";  
	elsif rising_edge(clock_UR) then
		update_SND_NEW 		<= '0'; 
		if UR_wr_ena = '1' and  dt_last_to_UltraRAM_reg = '1'  then
			update_SND_NEW 		<= '1'; 
		end if;
		
		update_SND_NEW_delay(1)   <= update_SND_NEW_delay(0);
		update_SND_NEW_delay(0)   <= update_SND_NEW;
	end if;
end process;
 
--*********************************************************** 
--count the number of data written inside the UR  BIFI STREAM
process(Rst_clock_UR,reset_FED0_rsync,clock_UR)
begin
	if Rst_clock_UR = '0' or reset_FED0_rsync = '1' then
		UR_wr_add_cnt <= (others => '0');
		SND_NEW_A_cnt <= (others => '0');
	elsif rising_edge(clock_UR) then
		if UR_wr_ena = '1' then
			--address is incremented all the time excepted if on the last word is not full 
			if not(dt_last_to_UltraRAM_reg = '1' and UR_wr_BE_reg(3) = '0')  then
				UR_wr_add_cnt	<= UR_wr_add_cnt + '1';
			end if;
			
			-- increase by nomber of 128-bit words
			-- counter number fo 128-bit words
			if 		UR_wr_BE_reg = "1111" then
				SND_NEW_A_cnt 		<= SND_NEW_A_cnt + x"4";
			elsif 	UR_wr_BE_reg = "0111" then
				SND_NEW_A_cnt 		<= SND_NEW_A_cnt + x"3";
			elsif 	UR_wr_BE_reg = "1110" then
				SND_NEW_A_cnt 		<= SND_NEW_A_cnt + x"3";
			elsif 	UR_wr_BE_reg = "0011" then
				SND_NEW_A_cnt 		<= SND_NEW_A_cnt + x"2";
			elsif 	UR_wr_BE_reg = "1100" then
				SND_NEW_A_cnt 		<= SND_NEW_A_cnt + x"2";
			elsif 	UR_wr_BE_reg = "0110" then                -- can happens if 2 last words in the bit 383..128 
				SND_NEW_A_cnt 		<= SND_NEW_A_cnt + x"2";
			elsif 	UR_wr_BE_reg = "0001" then
				SND_NEW_A_cnt 		<= SND_NEW_A_cnt + x"1";
			elsif 	UR_wr_BE_reg = "0010" then                -- can happens if last word in the bit 255..128 
				SND_NEW_A_cnt 		<= SND_NEW_A_cnt + x"1";
			elsif 	UR_wr_BE_reg = "0100" then                -- can happens if last word in the bit 283..256 
				SND_NEW_A_cnt 		<= SND_NEW_A_cnt + x"1";
			elsif 	UR_wr_BE_reg = "1000" then
				SND_NEW_A_cnt 		<= SND_NEW_A_cnt + x"1";
			end if;
		end if;
	end if;
end process;

UR_wr_add_cell  <= UR_wr_add_cnt ;--and BIFI_size(13 downto 0);
 
-- latch new counter to send to TCP logic STREAM A
process(Rst_clock_UR,reset_FED0_rsync,clock_UR)
begin
	if Rst_clock_UR = '0' or reset_FED0_rsync  = '1' then
		SND_NEW_A_reg <= (others => '0');
	elsif rising_edge(clock_UR) then
		if update_SND_NEW = '1' then
			SND_NEW_A_reg <= SND_NEW_A_cnt; 
		 end if;
	end if;
end process;


--*********************************************************
--  resync SND_NEW value to send to SM TCP                *
--*********************************************************	

resync_pulse_new:entity work.resync_pulse 
PORT MAP(
	aresetn	=> Rst_clock_UR,
	clocki	=> clock_UR,
	in_s	=> update_SND_NEW,
	clocko	=> clock_r,
	out_s	=> ltch_snd_new
	);
	
process(Rst_FED0_Clock_R,clock_r)
begin
	if Rst_FED0_Clock_R = '0' then
		SND_NEW_A 						<= (others => '0');
	elsif rising_edge(clock_r) then
		if ltch_snd_new = '1' then
		 -- nb of bytes                   nb of 128-bit words (16 bytes)
			SND_NEW_A(31 downto 4) 	<= SND_NEW_A_reg(27 downto 0); 
			SND_NEW_A(3 downto 0) 	<= "0000";--each word is 128-bit (16 bytes))
		end if;
	end if;
end process;


--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ 
--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  
--@@@@@@@@@@@@@@@@@@@@@@@@@@					 UltraRAM                         @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ 
--@@@@@@@@@@@@@@@@@@@@@@@@@@					 1 MBytes                         @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ 
--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@   
--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@   
 
 
-- enable the write to the TCP Stream (Here the UltraRAM)  when the BIFI is not full and the packet is ready
process(Rst_clock_UR,clock_UR)
begin
	if Rst_clock_UR = '0' then 
		SR_wr_OK 			<= '0';
		read_packet_ready	<= '0';
	elsif rising_edge(clock_UR) then
		read_packet_ready	    <= '0';
		if 	rd_inter_FIFO_w = '1' and  dt_last_to_UltraRAM = '1' then
			SR_wr_OK 		    <= '0';
			read_packet_ready	<= '1';
		-- write if BIFI not FULL and packet ready in the FIFO 
		elsif 	block_ready_to_write  = '0'	and BIFI_full_A = '0' and read_packet_ready = '0' then  
			SR_wr_OK 			<= '1';
		end if;

	end if;
end process;

Write_pointer(5 downto 0)  <= (others => '0');
Write_pointer(19 downto 6)  <= UR_wr_add_cell;
Write_pointer(63 downto 20)  <= (others => '0');
--*&*&*&*&*&*&**&*&*&*&*&*&*&**&*&*&*&*&*&*&**&*&*&*&*&*&*&**&*&*&*&*&*&*&*&
--*&*&*&*&*&*&**&*&*&*&*&*&*&**&*&*&*&*&*&*&**&*&*&*&*&*&*&**&*&*&*&*&*&*&*&*
--					Component interface to the UltraRAM memory "BIFI"	*&*&*
																	  --*&*&*
Mem_stream_i1:stream_buffer                                           --*&*&*
  Port Map(                                                           --*&*&*
		resetp					=> Rstp_clock_UR				,     --*&*&*
  		clock					=> clock_UR						,     --*&*&*
																	  --*&*&*
  		address_wr				=> UR_wr_add_cell(13 downto 0)	,     --*&*&*
  		data_i					=> UR_wr_dt_reg			        ,     --*&*&*
  		write_ena				=> UR_wr_ena					,     --*&*&*
  		word_enable				=> UR_wr_BE_reg		            ,     --*&*&*
																	  --*&*&*
  		address_rd				=> UR_rd_add_cell(13 downto 0)	,     --*&*&*
  		data_o					=> UR_rd_dt_cell				,     --*&*&*
  		read_ena				=> UR_rd_ena			              --*&*&*
  	);                                                                --*&*&*
																	  --*&*&*
																	  --*&*&*
--*&*&*&*&*&*&**&*&*&*&*&*&*&**&*&*&*&*&*&*&**&*&*&*&*&*&*&**&*&*&*&*&*&*&*&*
--*&*&*&*&*&*&**&*&*&*&*&*&*&**&*&*&*&*&*&*&**&*&*&*&*&*&*&**&*&*&*&*&*&*&*&

--******    Control the almost full (BIFI is like a FIFO  )      *******

resync_pulse_i21:entity work.resync_pulse 
port map(
	aresetn	=> Rst_Clock_R,
	clocki	=> clock_r,
	in_s	=> VALID_P_A,
	clocko	=> clock_UR,
	out_s	=> sync_valid_p_A
	);
 
process(Rst_clock_UR,clock_UR)
begin
	if Rst_clock_UR = '0' then
		BIFI_used_A_w <= (others => '0');
	elsif rising_edge(clock_UR) then
		BIFI_used_A_w <= UR_wr_add_cell - sync_UNA_A(19 downto 6);-- number of memory cells used   (not free)  
			
		if 	sync_valid_p_A = '1' then
			sync_UNA_A(19 downto 6)	<= SND_UNA_A(19 downto 6) ;--and BIFI_size(13 downto 0);
		end if;
	end if;
end process;

--**********************************************************************	
-- BIFI management
-- compute the number of data in inside the BIFI memory and create the almost full signal

process(reset_FED0_rsync,Rst_clock_UR,clock_UR)
begin
	if Rst_clock_UR = '0' or reset_FED0_rsync = '1' then
		BIFI_full_A       <= '0';
		nothing_to_send   <= '0'; 
	elsif rising_edge(clock_UR) then
		--if 		(BIFI_used_A_w and BIFI_size(13 downto 0)) > (BIFI_size(13 downto 8) & "10000000") then  -- almost FUll
		if 		(BIFI_used_A_w ) > ("11111110000000") then  -- almost FUll
			BIFI_full_A <= '1';
		--elsif 	(BIFI_used_A_w and BIFI_size(13 downto 0)) < (BIFI_size(13 downto 8) & "00000000") then  -- no almost Full
		elsif 	(BIFI_used_A_w ) < ("11111100000000") then  -- no almost Full
			BIFI_full_A <= '0';	
		end if;
		
		
		nothing_to_send   <= '0'; 
		if (UR_wr_ena = '1' and debug_delay(1) = '1' and debug_delay(2) = '0') and  (SND_NEW_A_cnt(1 downto 0) /= "00")  then
		  nothing_to_send <= '1'; 
		end if;
	end if;
end process;
	

process(clock_w)
begin
	if rising_edge(clock_w) then
		BIFI_A_HF	<= BIFI_full_A;
	end if;
end process;

BIFI_used_A(63 downto 14)	<= (others => '0');
BIFI_used_A(13 downto 0)	<= BIFI_used_A_w ;--and BIFI_size(13 downto 0);					
 
--*********************************************************
--					 resync to send to SM TCP
--*********************************************************
 
Rst_FED0_G			<= '0' when resetn = '0' or resetp_FED0 = '1' else '1'; 

reset_resync_i5:entity work.resetn_resync  
port map (
	aresetn			=> Rst_FED0_G,
	clock			=> clock_r, 
	Resetn_sync		=> Rst_FED0_Clock_R
	);
	 
--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
--@@@@@@@@@@@@@@@@@@@@@@@@@@ 				interface for DATA OUT                @@@@@@@@@@@@@@@@@@@@@@@@@@
--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
--
-- State machine which controls the data out
READ_mem_clock:process(Rst_clock_UR,clock_UR)
begin
	if Rst_clock_UR = '0' then
		READ_mem <= idle;
	elsif rising_edge(clock_UR) then
		Case READ_mem is
			when idle =>
				if req_read_reg(1) = '0' and req_read_reg(0) = '1' then
					READ_mem <= ld_add;
				end if;
				 				
			when ld_add =>
				READ_mem <= read_data;
				
			when read_data =>
				--if rd_size_cnt < x"0001" then
				if rd_size_cnt = x"0000" or rd_size_cnt(15) = '1' then
					READ_mem <= end_read;
				end if;
			
			when end_read =>
				READ_mem <= idle;
			
			when others =>
				READ_mem <= idle;
 		 end case;
	end if;
end process;

--*********************************************************
-- resync the read request from the Ethernet interface
process(Rst_clock_UR,clock_UR)
begin
	if Rst_clock_UR = '0' then
		req_read_reg 		<= "00";
	elsif rising_edge(clock_UR) then
		req_read_reg(1) 	<= req_read_reg(0);
		req_read_reg(0) 	<= req_read_async;
		req_read_async	 	<= req_read;
	end if;
end process;

--*********************************************************
-- address used to access the UltraRAM
process(Rst_clock_UR,clock_UR)
begin
	if Rst_clock_UR = '0' then
		UR_rd_add_cnt_reg 	<= (others => '0');
		word_align_128b     <= (others => '0');  
	elsif rising_edge(clock_UR) then	
	
		if READ_mem = ld_add then	
															-- address is in 512-bit word
			UR_rd_add_cnt_reg(13 downto 0) 	<= req_add_r(19 downto 6);  -- correspond to the Sequence Number from TCP
			word_align_128b					<= req_add_r(5 downto 4);   -- used to shift the 128b word 
																	--     because the data bus is 512-bit and the minimum data unit is 128-bit
																	--  	00   all 128b (4) words used
																	--  	01   3 higher 128b words used
																	--  	10   2 higher 128b words used
																	--  	11   the highest 128b word used
																																
		elsif UR_rd_ena = '1' then
			UR_rd_add_cnt_reg				<= UR_rd_add_cnt_reg +'1';--after we read all the times 512 bit (4 x 128b words)
		end if;
		 
	end if;
end process;
 
UR_rd_add_cell                  <= UR_rd_add_cnt_reg(13 downto 0) ;--and BIFI_size(13 downto 0); 
--*********************************************************
-- counter to read DATA number from BIFI (how many data TCP request  MAX : 9 Kbytes Jumbo frame)
process(Rst_clock_UR,clock_UR)
begin
	if Rst_clock_UR = '0' then
		rd_size_cnt						<= (others => '0');
		ena_size_cnt					<= (others => '0');
		UR_rd_ena 						<= '0';
		read_first_word					<= '0';
		UR_rd_to_Wr_FIFO				<= (others => '0');
		request_done_reg				<= '0';
		UR_rd_to_Wr_FIFO_cell			<= '0';
		
	elsif rising_edge(clock_UR) then
	
		UR_rd_to_Wr_FIFO(10 downto 1)	<= UR_rd_to_Wr_FIFO(9 downto 0);
		UR_rd_to_Wr_FIFO(0) 			<= UR_rd_to_Wr_FIFO_cell;	
		
		UR_rd_ena 						<= '0';
		
		if READ_mem = ld_add then
			rd_size_cnt(15 downto 0)	<= signed(req_size(19 downto 4)); -- size is in number of 128-bit word
			ena_size_cnt(15 downto 0)	<= signed(req_size(19 downto 4)); -- size is in number of 128-bit word
			read_first_word				<= '1'; 
			
		--elsif rd_size_cnt > x"0000" and rd_ff_used(10) = '0' then
		elsif (rd_size_cnt(15) = '0' and rd_size_cnt(14 downto 0) /= "000000000000000")   and rd_ff_used(10) = '0' then
			read_first_word				<= '0';
			if read_first_word = '1' then
				if 		word_align_128b = "11" then
					rd_size_cnt 		<= rd_size_cnt - x"1"; --first word has 1 highest x 128-bit word valid
				elsif	word_align_128b = "10" then
					rd_size_cnt 		<= rd_size_cnt - x"2"; --first word has 2 x 128-bit word valid
				elsif	word_align_128b = "01" then
					rd_size_cnt 		<= rd_size_cnt - x"3"; --first word has 3 x 128-bit word valid
				else
					rd_size_cnt 		<= rd_size_cnt - x"4"; --first word has 4 x 128-bit word valid
				end if;
			else
				rd_size_cnt 			<= rd_size_cnt - x"4"; --each address is 4 x 128-bit word valid
				 
			end if;
			UR_rd_ena 					<= '1'; 
			ena_size_cnt				<= ena_size_cnt - x"4";
		end if;
		
		UR_rd_to_Wr_FIFO_cell			<= '0';
		--if ena_size_cnt > x"0000" and rd_ff_used(10) = '0' then
		if (ena_size_cnt(15) = '0' and ena_size_cnt(14 downto 0) /= "000000000000000")   and rd_ff_used(10) = '0' then
			UR_rd_to_Wr_FIFO_cell		<= '1';
		end if;
		 
		request_done_reg				<= '0';
		if READ_mem = end_read then
			request_done_reg			<= '1';
		end if;
	end if;
end process;
  
--*******************************************************
-- shift the 128b words to aligned the data on 512-bit
process(clock_UR)
begin
	if rising_edge(clock_UR) then
		
		UR_rd_dt_cell_tmp	<= UR_rd_dt_cell;
		
		if 		word_align_128b = "11" then
			UR_rd_dt_cell_shft(511 downto 128)	<= UR_rd_dt_cell(383 downto 000);
			UR_rd_dt_cell_shft(127 downto 000)	<= UR_rd_dt_cell_tmp(511 downto 384);
		elsif	word_align_128b = "10" then 
			UR_rd_dt_cell_shft(511 downto 256)	<= UR_rd_dt_cell(255 downto 000);
			UR_rd_dt_cell_shft(255 downto 000)	<= UR_rd_dt_cell_tmp(511 downto 256);
		elsif	word_align_128b = "01" then 
			UR_rd_dt_cell_shft(511 downto 384)	<= UR_rd_dt_cell(127 downto 000);
			UR_rd_dt_cell_shft(383 downto 000)	<= UR_rd_dt_cell_tmp(511 downto 128);
		else--if	word_align_128b = "11" then 
			UR_rd_dt_cell_shft					<= UR_rd_dt_cell_tmp;
		end if;
	end if;
end process;

--*******************************************************
--  delay write_ena to FIFO  and Last_word signal 
process(Rst_clock_UR,clock_UR)
begin
	if Rst_clock_UR = '0' then
		lst_wd_to_ETH 	<= (others => '0');
		
	elsif rising_edge(clock_UR) then
	
		lst_wd_to_ETH(10 downto 1) 		<= lst_wd_to_ETH(9 downto 0);
		lst_wd_to_ETH(0) 				<= '0';
		--if ena_size_cnt <= x"0004"  then
		if (ena_size_cnt(15) = '1' or  ena_size_cnt = x"0004" or ena_size_cnt(15 downto 2) = "00000000000000" ) then
			lst_wd_to_ETH(0) 			<= '1';
		end if;

	end if;
end process;

--*********************************************************
-- Manage the ENA word when we readout the Stream memory (a word is 128-bit)
-- The ENA is used to compute the CheckSum 
-- and to enable data for the IP/MAC
process(clock_UR)
begin
	if rising_edge(clock_UR) then
		
		-- should pipe the enable crated by the size_cnt (because data is pipe by UR and in the logic
		
		for I in 0 to Latency_read-1 loop
			dt_ena_from_UltraRAM_stg(I+1)	<= dt_ena_from_UltraRAM_stg(I);
		end loop; 
		dt_ena_from_UltraRAM_stg(0)	<= dt_ena_from_UltraRAM_reg;
	
		if 	  ena_size_cnt = x"0003" then
			dt_ena_from_UltraRAM_reg 	<= x"7";
		elsif ena_size_cnt = x"0002" then
			dt_ena_from_UltraRAM_reg 	<= x"3";
		elsif ena_size_cnt = x"0001" then
			dt_ena_from_UltraRAM_reg 	<= x"1";
		else
			dt_ena_from_UltraRAM_reg 	<= x"F";
		end if;
	end if;
end process;

dt_ena_from_UltraRAM		<= dt_ena_from_UltraRAM_stg(Latency_read);

--***************************************************
--  	 Checksum calcul 
--reset teh CS computer at each request TCP
Reset_CS 	<= req_read and not(req_read_reg(0));

reset_resync_i6:entity work.resetp_resync 
port map(
	aresetp			=> Reset_CS,
	clock			=> clock_UR,
	Resetp_sync		=> Reset_CS_resync_clk_UR 
	);

CS_i1:CS_Compute  
	Port Map( 
		aclrp		=> Reset_CS_resync_clk_UR	, 
		clock		=> clock_UR					, 
		clken		=> UR_rd_to_Wr_FIFO(Latency_read), 
		last_word	=> lst_wd_to_ETH(Latency_read+2)		, 
		data		=> UR_rd_dt_cell_shft		, 
		data_ena	=> dt_ena_from_UltraRAM		, 
		                                          
		result		=> CS_result				, 
		CS_done		=> CS_done					 
); 

 
-- the FIFO will be written with the CheckSum when the data block will be recorded in the FIFO_DC_517x2048_FWFT
-- The checksum is calculated here because it is need to build the IP/TCP headers
-- The data from FIFO_DC_517x2048_FWFT  will be directly send to MAC 100Gb
FIFO_CS_i1:FIFO_eth_CS
	PORT Map(
		rst         	=> resetp		, --reset resync done in the IP
		wr_clk      	=> clock_UR 	, 
		din         	=> CS_result  	, 
		wr_en       	=> CS_done 		, 
										  
		rd_clk      	=> clock_r 		, 
		rd_en       	=> eth_ack_packet	, 
		dout        	=> eth_data_CS16 	, 
		--    full        	=> 				, 
		empty       	=> FIFO_data_ready_n
   ); 

Packet_ready   <= not(FIFO_data_ready_n);

--*******************************************************************************************************************
--**********************  internal FIFO to resync BIFI clock to rd clock

--wr_dt_to_fifo_out <= '1' when  UR_rd_to_Wr_FIFO(Latency_read) = '1' else '0';

-- reorganize data for MAC IP
--
--		data to FIFO 		|  		DATA from UR
--    	127..000				( 47..  0)' & (255..176)'
--	 	255..128				(175..128)' & (383..304)'
--	 	383..256				(303..256)' & (511..432)'
--	 	511..384				(431..384)' & (127.. 48)
-- this is done is this order because when ETH/IP/TCP header are filled, is remained 80 bit on the top (127..48) and then we have to go to next word
--			()'are registered values


process(clock_UR)
begin
	if rising_edge(clock_UR) then

		wr_dt_to_fifo_out 		<= UR_rd_to_Wr_FIFO(Latency_read);
		
		-- shift by 1 clock the data (UR is 1 clock latency when read)
		if UR_rd_to_Wr_FIFO(Latency_read) = '1' then
			dt_to_Eth_FIFO(511 downto 464)	<= UR_rd_dt_cell_shft(431 downto 384);
			
			dt_to_Eth_FIFO(383 downto 336)	<= UR_rd_dt_cell_shft(303 downto 256);
			dt_to_Eth_FIFO(335 downto 256)	<= UR_rd_dt_cell_shft(511 downto 432);
			
			dt_to_Eth_FIFO(255 downto 208)	<= UR_rd_dt_cell_shft(175 downto 128);
			dt_to_Eth_FIFO(207 downto 128)	<= UR_rd_dt_cell_shft(383 downto 304);
			
			dt_to_Eth_FIFO(127 downto 080)	<= UR_rd_dt_cell_shft(047 downto 000);
			dt_to_Eth_FIFO(079 downto 000)	<= UR_rd_dt_cell_shft(255 downto 176);
		end if;	
		
		-- shift the enable by one clock  (data pipe by one clock in this process)
		if READ_mem = ld_add then
			dt_ena_to_Eth_FIFO_reg			<= (others => '0');
		elsif UR_rd_to_Wr_FIFO(Latency_read) = '1' then
			dt_ena_to_Eth_FIFO_reg			<= dt_ena_from_UltraRAM;
		end if;
		
	end if;
end process;

--enable the first 80 bit for the MAC DATA alignment 
--(see Transfer packet, the first 80 bit finish the word after TCP header)
process(clock_UR,Rst_clock_UR)
begin
	if Rst_clock_UR = '0'  then
		enable_first_word <= '0';
	elsif rising_edge(clock_UR) then
		if READ_mem = ld_add then
			enable_first_word <= '1';
		elsif wr_dt_to_fifo_out = '1' then
			enable_first_word <= '0';
		end if;
	end if;
end process;

--
-- re-organize the bits to be directly or'ed  with the TCP bus in the transfer file (take in account ETH-IP -TCP headers
-- It is why first word will used only the 80 first bit from the packet
dt_to_Eth_FIFO_cell(511 downto 464)	<= dt_to_Eth_FIFO(511 downto 464); -- higher bit will go to D_register on MAC IP
dt_to_Eth_FIFO_cell(463 downto 384)	<= UR_rd_dt_cell_shft(127 downto 48); 
dt_to_Eth_FIFO_cell(383 downto 000)	<= dt_to_Eth_FIFO(383 downto 000); -- lower bit will got to A_register on MAC IP

-- manage the enable of 128b words
dt_ena_to_Eth_FIFO(3)	<= '1' when  (dt_ena_to_Eth_FIFO_reg(3) = '1' or enable_first_word = '1') and wr_dt_to_fifo_out_1x = '1'   else '0';
dt_ena_to_Eth_FIFO(2)	<= '1' when  (dt_ena_to_Eth_FIFO_reg(2) = '1' 							) and wr_dt_to_fifo_out_1x = '1'   else '0';
dt_ena_to_Eth_FIFO(1)	<= '1' when  (dt_ena_to_Eth_FIFO_reg(1) = '1' 							) and wr_dt_to_fifo_out_1x = '1'   else '0';
dt_ena_to_Eth_FIFO(0)	<= '1' when  (dt_ena_to_Eth_FIFO_reg(0) = '1' 							) and wr_dt_to_fifo_out_1x = '1'   else '0';

-- add +2 at latency and or withwr_dt_to_FIFO_out because we realign the words (4 x 128b) with first byte in word 3 ,
-- to be easy to insert in the ethernet packet builder
-- manage the end of packet
lst_wd_to_ETH_cell		<= '1' when wr_dt_to_fifo_out = '1' and lst_wd_to_ETH(Latency_read+2) = '1'  else '0';			

-- manage the WRITE to FIFO
wr_dt_to_fifo_out_1x	<= '1' when wr_dt_to_fifo_out = '1' or UR_rd_to_Wr_FIFO(Latency_read) = '1'  else '0';
	
--***********************************************************************************************
--
-- FIFO to buffer data from the UltraRAM
--
i3:FIFO_DC_517x2048_FWFT -- 2048 x 517bit =  
	PORT MAP
	(
		rst 				=> resetp					, --reset resync done in the IP
		wr_clk 				=> clock_UR					, 
		din(511 downto 000)	=> dt_to_Eth_FIFO_cell, 
		din(515 downto 512) => dt_ena_to_Eth_FIFO 		,
		din(516) 			=> lst_wd_to_ETH_cell			,
		wr_en 				=> wr_dt_to_fifo_out_1x		, 
		--full 				=>								, 
		wr_data_count 		=> rd_ff_used				, 
		--wr_rst_busy 		=>								, 
		
		rd_clk 				=> clock_r					, 
		rd_en 				=> eth_rd_data				, 
		dout(511 downto 0)	=> data_r_cell				, 
		dout(515 downto 512)=> data_ena_cell			,
		dout(516)			=> data_last_cell			, 
		rd_data_count		=> rd_data_count			,		
		empty 				=> last_dt					
		--rd_rst_busy 		=>						   	  
	);
 
eth_data_o		<= data_r_cell;
eth_data_ena_o	<= data_ena_cell;
eth_last_dt_o 	<= data_last_cell; 


--resync request done to Clock_read(TCP clock)
req_done_resync_pulse_i1:entity work.resync_pulse  
	port map(
		aresetn			=> '1',
		clocki			=> clock_UR,
		in_s			=> request_done_reg,
		clocko			=> clock_r,
		out_s			=> request_done
		);
 
--****************************************************************

end behavioral;