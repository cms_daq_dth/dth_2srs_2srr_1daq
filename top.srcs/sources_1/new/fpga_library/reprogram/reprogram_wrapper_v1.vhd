----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 17.09.2019 11:51:52
-- Design Name: 
-- Module Name: reprogram_wrapper - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.address_table.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity reprogram_wrapper is
 Port ( 
	usr_clk					: in std_logic;
	usr_rst_n				: in std_logic;
	usr_func_wr				: in std_logic_vector(16383 downto 0); 
	usr_wen					: in std_logic;
	usr_data_wr				: in std_logic_vector(63 downto 0);
	
	usr_func_rd				: in std_logic_vector(16383 downto 0); 
	usr_rden				: in std_logic;
	usr_data_rd				: out std_logic_vector(63 downto 0); 
 
	axi_aclk				: in std_logic;
	
	Flash_data				: inout std_logic_vector(15 downto 4);
	Flash_add				: out std_logic_vector(31 downto 0); 
	Flash_oen				: out std_logic ;
	Flash_wen				: out std_logic  
 );
end reprogram_wrapper;

architecture Behavioral of reprogram_wrapper is

signal data_out_reg	        		: std_logic_vector(63 downto 0);

signal AXI_prog_rq_add        		: std_logic_vector(31 downto 0);
signal AXI_prog_rq_strb         	: std_logic_vector(3 downto 0);
signal AXI_prog_rq_dti        		: std_logic_vector(31 downto 0);
signal AXI_prog_rtn_dto       		: std_logic_vector(31 downto 0);
signal AXI_prog_rq_rd         		: std_logic;
signal AXI_prog_rq_wr         		: std_logic;
signal AXI_prog_tick          		: std_logic;
signal AXI_prog_status        		: std_logic_vector(31 downto 0) := x"00000000";
signal AXI_prog_done          		: std_logic;

COMPONENT AXI_EMC_reprog
  PORT (
    s_axi_aclk        : IN STD_LOGIC;
    s_axi_aresetn     : IN STD_LOGIC;
    rdclk             : IN STD_LOGIC;
    s_axi_mem_awid    : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    s_axi_mem_awaddr  : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    s_axi_mem_awlen   : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    s_axi_mem_awsize  : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    s_axi_mem_awburst : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    s_axi_mem_awlock  : IN STD_LOGIC;
    s_axi_mem_awcache : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    s_axi_mem_awprot  : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    s_axi_mem_awvalid : IN STD_LOGIC;
    s_axi_mem_awready : OUT STD_LOGIC;
    s_axi_mem_wdata   : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    s_axi_mem_wstrb   : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    s_axi_mem_wlast   : IN STD_LOGIC;
    s_axi_mem_wvalid  : IN STD_LOGIC;
    s_axi_mem_wready  : OUT STD_LOGIC;
    s_axi_mem_bid     : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    s_axi_mem_bresp   : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    s_axi_mem_bvalid  : OUT STD_LOGIC;
    s_axi_mem_bready  : IN STD_LOGIC;
    s_axi_mem_arid    : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    s_axi_mem_araddr  : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    s_axi_mem_arlen   : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    s_axi_mem_arsize  : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    s_axi_mem_arburst : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    s_axi_mem_arlock  : IN STD_LOGIC;
    s_axi_mem_arcache : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    s_axi_mem_arprot  : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    s_axi_mem_arvalid : IN STD_LOGIC;
    s_axi_mem_arready : OUT STD_LOGIC;
    s_axi_mem_rid     : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    s_axi_mem_rdata   : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    s_axi_mem_rresp   : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    s_axi_mem_rlast   : OUT STD_LOGIC;
    s_axi_mem_rvalid  : OUT STD_LOGIC;
    s_axi_mem_rready  : IN STD_LOGIC;
    mem_dq_i          : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    mem_dq_o          : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    mem_dq_t          : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    mem_a             : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    mem_ce            : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    mem_cen           : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    mem_oen           : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    mem_wen           : OUT STD_LOGIC;
    mem_ben           : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    mem_qwen          : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    mem_rpn           : OUT STD_LOGIC;
    mem_adv_ldn       : OUT STD_LOGIC;
    mem_lbon          : OUT STD_LOGIC;
    mem_cken          : OUT STD_LOGIC;
    mem_rnw           : OUT STD_LOGIC;
    mem_cre           : OUT STD_LOGIC;
    mem_wait          : IN STD_LOGIC_VECTOR(0 DOWNTO 0)
  );
END COMPONENT;


signal Flash_data_o									: std_logic_vector(15 downto 0);
signal Flash_data_t									: std_logic_vector(15 downto 0);
signal Flash_datai_STARTUP							: std_logic_vector(3 downto 0);
signal Flash_data_t_STARTUP							: std_logic_vector(3 downto 0);
signal Flash_data_o_STARTUP							: std_logic_vector(3 downto 0);
signal Flash_cen									: std_logic;

COMPONENT AXI_itf is
    Port ( 
			resetn 				: in STD_LOGIC; --
			Usr_clock 			: in STD_LOGIC; -- max 1 MHz
			Usr_add				: in std_logic_vector(31 downto 0);
			Usr_strb			: in std_logic_vector(3 downto 0);
			Usr_dti	 			: in STD_LOGIC_vector(31 downto 0);
			Usr_dto	 			: out STD_LOGIC_vector(31 downto 0);
			Usr_RdREQ			: in std_logic;
			Usr_WrREQ			: in std_logic;
			Usr_AXI_tick		: in std_logic;
			Axi_status			: out std_logic_vector(31 downto 0) := x"00000000";
			Axi_done			: out std_logic;

			s_axi_clock         : in std_Logic;
			s_axi_pm_tick 		: OUT STD_LOGIC;
			s_axi_awaddr  		: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
			s_axi_awvalid 		: OUT STD_LOGIC;
			s_axi_awready 		: IN STD_LOGIC;
			s_axi_wdata   		: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
			s_axi_wstrb   		: OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
			s_axi_size          : out STD_LOGIC_VECTOR(2 downto 0);
			s_axi_wvalid  		: OUT STD_LOGIC;
			s_axi_wready  		: IN STD_LOGIC;
			s_axi_bresp   		: IN STD_LOGIC_VECTOR(1 DOWNTO 0);
			s_axi_bvalid  		: IN STD_LOGIC;
			s_axi_bready  		: OUT STD_LOGIC;
			s_axi_araddr  		: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
			s_axi_arvalid 		: OUT STD_LOGIC;
			s_axi_arready 		: IN STD_LOGIC;
			s_axi_rdata   		: IN STD_LOGIC_VECTOR(31 DOWNTO 0);
			s_axi_rresp   		: IN STD_LOGIC_VECTOR(1 DOWNTO 0);
			s_axi_rvalid  		: IN STD_LOGIC;
			s_axi_rready  		: OUT STD_LOGIC
           );
end COMPONENT;
 
signal axi_mem_awaddr  							: STD_LOGIC_VECTOR(31 DOWNTO 0);  
signal axi_mem_awvalid 							: STD_LOGIC;
signal axi_mem_awready 							: STD_LOGIC;
signal axi_mem_wdata   							: STD_LOGIC_VECTOR(31 DOWNTO 0);  
signal axi_mem_wstrb                           	: STD_LOGIC_VECTOR(3 DOWNTO 0);  
signal axi_mem_size   							: STD_LOGIC_VECTOR(2 DOWNTO 0);  
signal axi_mem_wvalid  							: STD_LOGIC;
signal axi_mem_wready  							: STD_LOGIC;  
signal axi_mem_bvalid  							: STD_LOGIC;
signal axi_mem_bready  							: STD_LOGIC; 
signal axi_mem_araddr  							: STD_LOGIC_VECTOR(31 DOWNTO 0); 
signal axi_mem_arvalid 							: STD_LOGIC;
signal axi_mem_arready 							: STD_LOGIC; 
signal axi_mem_rdata   							: STD_LOGIC_VECTOR(31 DOWNTO 0); 
signal axi_mem_rvalid  							: STD_LOGIC;
signal axi_mem_rready  							: STD_LOGIC;
												  
signal Flash_EndOfSartup						: STD_LOGIC;

component resetn_resync is
port (
	aresetn				: in std_logic;
	clock				: in std_logic; 

	Resetn_sync		: out std_logic;
	Resetp_sync		: out std_logic
	);
end component;

--ICAPE signals
signal ICAPE_AVAIL  							: std_logic;
signal ICAPE_DTO    							: std_logic_VECTOR(31 DOWNTO 0);
signal ICAPE_PRDONE 							: std_logic;
signal ICAPE_PRERROR							: std_logic; 
signal ICAPE_CS_n      							: std_logic;  
signal ICAPE_CS_n_d    							: std_logic;  
signal ICAPE_DTI    							: std_logic_VECTOR(31 DOWNTO 0);
signal ICAPE_RDWRB  							: std_logic;

signal axi_resetn								: std_logic;
--**************************************************************************
--**************   CODE START HERE 						 *******************
--**************************************************************************
begin

reset_resync_i1:resetn_resync  
port map(
	aresetn			=> usr_rst_n,
	clock			=> axi_aclk,
					 
	Resetn_sync		=> axi_resetn 
	);
	
--#############################################################################
-- 
--  Decoding for PCI to AXI
--  
--  
-- Signals generated in this process are pulses which are one clock long
-- (due to the usr_wen='1' condition since usr_wen is a pulse of 1 clock)
process(usr_clk)
  begin
    if rising_edge(usr_clk) then 
		AXI_prog_rq_wr				<= '0';
		AXI_prog_rq_rd				<= '0';
		AXI_prog_tick				<= '0';   
		ICAPE_CS_n           		<= '1';
		
		if usr_wen = '1'  then

				if usr_func_wr(Prog_flash_AXI_Add ) = '1' then
					AXI_prog_rq_add				<= usr_data_wr(31 downto 0);
				end if; 

				if usr_func_wr(Prog_flash_AXI_Dti) = '1' then
					AXI_prog_rq_dti				<= usr_data_wr(31 downto 0);
				end if;      

				if usr_func_wr(Prog_flash_AXI_ctrl) = '1' then
					AXI_prog_rq_wr             <= usr_data_wr(0);
					AXI_prog_rq_rd             <= usr_data_wr(1);
					AXI_prog_tick              <= usr_data_wr(8);
					AXI_prog_rq_strb           <= usr_data_wr(19 downto 16) ;
				end if;                  
					
				if usr_func_wr(Prog_ICAPE_dti) = '1' then
					ICAPE_CS_n                 <= '0';
					ICAPE_DTI		           <= usr_data_wr(31 downto 0); 
				end if;                  
				 		 
			end if;
      end if;
end process;    
 
--		PCIe read data
                    
process(usr_clk)
begin
	if rising_edge(usr_clk) then		
	   data_out_reg	                                  	  		<= (others => '0');
			if usr_func_rd(Prog_flash_AXI_Add) = '1' then
				data_out_reg(31 downto 0)                      <= AXI_prog_rq_add;

			elsif usr_func_rd(Prog_flash_AXI_Dti) = '1' then
				data_out_reg(31 downto 0)                      <= AXI_prog_rq_dti;

			elsif usr_func_rd(Prog_flash_AXI_Dto) = '1' then
				data_out_reg(31 downto 0)                      <= AXI_prog_rtn_dto;

			elsif usr_func_rd(Prog_flash_AXI_status) = '1' then
				data_out_reg(29 downto 0)                      <= AXI_prog_status(29 downto 0);
				data_out_reg(30)                               <= AXI_prog_done;
				data_out_reg(31)                               <= Flash_EndOfSartup;
			
			elsif usr_func_rd(Prog_ICAPE_ctrl) = '1' then
				data_out_reg(29)                      		   <= ICAPE_PRERROR;
				data_out_reg(30)                               <= ICAPE_PRDONE;
				data_out_reg(31)                               <= ICAPE_AVAIL;
			
			elsif usr_func_rd(Prog_ICAPE_dti) = '1' then
				data_out_reg(31 downto 0)  					   <= ICAPE_DTI;
				
			elsif usr_func_rd(Prog_ICAPE_dto) = '1' then
				data_out_reg(31 downto 0)                      <= ICAPE_DTO; 
			end if;
	end if;
end process;

usr_data_rd	<= data_out_reg;

--*****************************************************************************
--
--  PCI to AXI
--
AXI_converter:AXI_itf
   Port map( 
	resetn 				=> usr_rst_n         ,
	Usr_clock 			=> usr_clk               ,
	Usr_add				=> AXI_prog_rq_add       ,
	Usr_strb			=> AXI_prog_rq_strb        ,
	Usr_dti	 			=> AXI_prog_rq_dti       ,
	Usr_dto	 			=> AXI_prog_rtn_dto      ,
	Usr_RdREQ			=> AXI_prog_rq_rd        ,
	Usr_WrREQ			=> AXI_prog_rq_wr        ,
	Usr_AXI_tick		=> AXI_prog_tick         ,
	Axi_status			=> AXI_prog_status       ,
	Axi_done			=> AXI_prog_done         ,
						 
	s_axi_clock         => axi_aclk               ,
	--s_axi_pm_tick 		=> ,
	s_axi_awaddr  		=> axi_mem_awaddr ,
	s_axi_awvalid 		=> axi_mem_awvalid,
	s_axi_awready 		=> axi_mem_awready,
	s_axi_wdata   		=> axi_mem_wdata  ,
	s_axi_wstrb   		=> axi_mem_wstrb   ,
    s_axi_size          => axi_mem_size,
	s_axi_wvalid  		=> axi_mem_wvalid ,
	s_axi_wready  		=> axi_mem_wready ,
	s_axi_bresp   		=> "00",
	s_axi_bvalid  		=> axi_mem_bvalid ,
	s_axi_bready  		=> axi_mem_bready ,
	s_axi_araddr  		=> axi_mem_araddr ,
	s_axi_arvalid 		=> axi_mem_arvalid,
	s_axi_arready 		=> axi_mem_arready,
	s_axi_rdata   		=> axi_mem_rdata  ,
	s_axi_rresp   		=> "00",
	s_axi_rvalid  		=> axi_mem_rvalid ,
	s_axi_rready  		=> axi_mem_rready  
 );
 
--**************************************************************************
--
-- Implement the controller for the FLASH RAM

STARTUPE3_inst : STARTUPE3 
generic map (
	PROG_USR 		=> "FALSE", 		            -- Activate program event security feature. Requires encrypted bitstreams. 
	SIM_CCLK_FREQ 	=> 0.0 				            -- Set the Configuration Clock Frequency (ns) for simulation
)
port map (
	CFGCLK    	=> open,    	                    -- 1-bit output: Configuration main clock output
	CFGMCLK   	=> open,   	                        -- 1-bit output: Configuration internal oscillator clock output
	DI        	=> Flash_datai_STARTUP,        	    -- 4-bit output: Allow receiving on the D input pin
	EOS       	=> Flash_EndOfSartup,       	    -- 1-bit output: Active-High output signal indicating the End Of Startup 
	PREQ 		=> open, 		                    -- 1-bit output: PROGRAM request to fabric output
	DO        	=> Flash_data_o_STARTUP,        	-- 4-bit input: Allows control of the D pin output
	DTS     	=> Flash_data_t_STARTUP,       	    -- 4-bit input: Allows tristate of the D pin
	FCSBO     	=> Flash_cen,     	                -- 1-bit input: Controls the FCS_B pin for flash access
	FCSBTS    	=> '0',    	                        -- 1-bit input: Tristate the FCS_B pin
	GSR       	=> '0',       	                    -- 1-bit input: Global Set/Reset input (GSR cannot be used for the port) 
	GTS 		=> '0', 		                    -- 1-bit input: Global 3-state input (GTS cannot be used for the port name) 
	KEYCLEARB 	=> '1', 	                        -- 1-bit input: Clear AES Decrypter Key input from Battery-Backed RAM (BBRAM) 
	PACK 		=> '1', 		                    -- 1-bit input: PROGRAM acknowledge input
	USRCCLKO  	=> '0',  	                        -- 1-bit input: User CCLK input
	USRCCLKTS 	=> '0', 	                        -- 1-bit input: User CCLK 3-state enable input
	USRDONEO  	=> '1',  	                        -- 1-bit input: User DONE pin output control
	USRDONETS 	=> '1'  	                        -- 1-bit input: User DONE 3-state enable output
);

Flash_i1:AXI_EMC_reprog
  PORT map(
    s_axi_aclk        				=> axi_aclk			, 
    s_axi_aresetn     				=> axi_resetn	, 
    rdclk             				=> axi_aclk			, 
    s_axi_mem_awid    				=> "0000"    		, 
    s_axi_mem_awaddr  				=> axi_mem_awaddr  	, 
    s_axi_mem_awlen   				=> "00000000"   	, 
    s_axi_mem_awsize  				=> axi_mem_size, 
    s_axi_mem_awburst 				=> "01" 	, 
    s_axi_mem_awlock  				=> '0'  	, 
    s_axi_mem_awcache 				=> "0000" 	, 
    s_axi_mem_awprot  				=> "000"  	, 
    s_axi_mem_awvalid 				=> axi_mem_awvalid 	, 
    s_axi_mem_awready 				=> axi_mem_awready 	, 
    s_axi_mem_wdata   				=> axi_mem_wdata   	, 
    s_axi_mem_wstrb   				=> axi_mem_wstrb 	, 
    s_axi_mem_wlast   				=> '1'   	, 
    s_axi_mem_wvalid  				=> axi_mem_wvalid  	, 
    s_axi_mem_wready  				=> axi_mem_wready  	, 
    s_axi_mem_bvalid  				=> axi_mem_bvalid  	, 
    s_axi_mem_bready  				=> axi_mem_bready  	, 
    s_axi_mem_arid    				=> "0000"      	, 
    s_axi_mem_araddr  				=> axi_mem_araddr  	, 
    s_axi_mem_arlen   				=> "00000000"     	, 
    s_axi_mem_arsize  				=> axi_mem_size,  	  
    s_axi_mem_arburst 				=> "01" 	, 	  
    s_axi_mem_arlock  				=> '0'   	, 
    s_axi_mem_arcache 				=> "0000"   	, 
    s_axi_mem_arprot  				=> "000"  	, 
    s_axi_mem_arvalid 				=> axi_mem_arvalid 	, 
    s_axi_mem_arready 				=> axi_mem_arready 	, 
    s_axi_mem_rdata   				=> axi_mem_rdata   	, 
    s_axi_mem_rvalid  				=> axi_mem_rvalid  	, 
    s_axi_mem_rready  				=> axi_mem_rready  	, 
    mem_dq_i(15 downto 4)			=> Flash_data(15 downto 4), 
    mem_dq_i(3 downto 0)			=> Flash_datai_STARTUP(3 downto 0), 
    mem_dq_o(15 downto 4)          	=> Flash_data_o(15 downto 4) , 
    mem_dq_o(3 downto 0)          	=> Flash_data_o_STARTUP, 
    mem_dq_t(15 downto 4)          	=> Flash_data_t(15 downto 4), 
    mem_dq_t(3 downto 0)          	=> Flash_data_t_STARTUP, 
    mem_a(31 downto 0)				=> Flash_add,  
    -- mem_ce            				=> , 
    mem_cen(0)         				=> Flash_cen, 
    mem_oen(0)         				=> Flash_oen, 
    mem_wen           				=> Flash_wen,  
    mem_wait(0)        				=> '0'   
  );
 
data_bus:for I in  4 to 15 generate 
	Flash_data(I)			<= Flash_data_o(I) when Flash_data_t(I) = '0' else 'Z'; -- to be verified
end generate; 


--**********************************************************************************************
--
-- ICAPE3 instance to be able to load a design from FLASH
--
-- ICAPE3: Internal Configuration Access Port
-- UltraScale
-- Xilinx HDL Language Template, version 2018.2

-- delay the CS signal by one clock
process(usr_clk)
begin
	if rising_edge(usr_clk) then
		ICAPE_CS_n_d	<= ICAPE_CS_n;
	end if;
end process; 
 
ICAPE_RDWRB <= '0';  -- hardcoded. We only write to the ICAPE

ICAPE3_inst : ICAPE3 
generic map (
    DEVICE_ID           => X"03628093", -- Specifies the pre-programmed Device ID value to be used for simulation -- purposes.
    ICAP_AUTO_SWITCH    => "DISABLE", -- Enable switch ICAP using sync word
    SIM_CFG_FILE_NAME   => "NONE" -- Specifies the Raw Bitstream (RBT) file to be parsed by the simulation
-- model
)
port map (
    AVAIL               => ICAPE_AVAIL  , -- 1-bit output: Availability status of ICAP
    O                   => ICAPE_DTO    , -- 32-bit output: Configuration data output bus
    PRDONE              => ICAPE_PRDONE , -- 1-bit output: Indicates completion of Partial Reconfiguration 
    PRERROR             => ICAPE_PRERROR, -- 1-bit output: Indicates Error during Partial Reconfiguration 
    CLK                 => usr_clk      , -- 1-bit input: Clock input
    CSIB                => ICAPE_CS_n_d , -- 1-bit input: Active-Low ICAP enable
    I                   => ICAPE_DTI    , -- 32-bit input: Configuration data input bus
    RDWRB               => ICAPE_RDWRB 	  -- 1-bit input: Read/Write Select input
);
end Behavioral;
