
set_max_delay -datapath_only -from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME =~ *resync_pulse*/reg_1st_stage*}]] -to [get_pins -filter {REF_PIN_NAME == D} -of [get_cells -hierarchical -filter {NAME =~ *resync_pulse*/reg_2nd_stage_async*}]] 2.000

