set pins_from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME =~ *resync_pulse*/reg_1st_stage*}]]
set pins_to [get_pins -filter {REF_PIN_NAME == D} -of [get_cells -hierarchical -filter {NAME =~ *resync_pulse*/*reg_2nd_stage_async*}]]

set_max_delay -datapath_only -from $pins_from -to $pins_to 2.000
