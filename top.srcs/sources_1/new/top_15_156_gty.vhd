----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 21.04.2017 14:23:45
-- Design Name: 
-- Module Name: top - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
library UNISIM;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.std_logic_unsigned.all;

use work.address_table.all;
use work.interface.all;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values 

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.vcomponents.all;
 

entity top is
generic(-- parameters to set the throughput of the SlinkRocket
        throughput								: string := "15.66";
				--possible choices are 15.66 or 25.78125
		ref_clock								: string := "156.25";
				--possible choices are 156.25  or  322.265625 
		technology								: string := "GTY";
				-- possible choices are GTY or GTH
        UltraRam_mem                            : boolean := TRUE 
                -- Use the Ultra RAM for the SlinkRocket memory 
		);
   Port ( 
	CLK_100MHZ_P					    : in std_logic;
	CLK_100MHZ_N					    : in std_logic;
	CLK_125MHZ_P					    : in std_logic;
	CLK_125MHZ_N					    : in std_logic;
	CLK_156_25MHZ_P					    : in std_logic;-- prog as 125 MHz
	CLK_156_25MHZ_N					    : in std_logic;
	CLK_322_265625MHZ_P					: in std_logic;
	CLK_322_265625MHZ_N					: in std_logic;
	
 	PCIE_TX0_P  					    : out std_logic;
 	PCIE_TX0_N  					    : out std_logic;
 	PCIE_RX0_P  					    : in std_logic;
 	PCIE_RX0_N  					    : in std_logic;
                      
 	PCIE_clk_p  					    : in std_logic;
 	PCIE_clk_n  					    : in std_logic;
 	PCIE_rst							: in std_logic;
   
	GPIO_LED_0_LS            	        : out std_logic;
	GPIO_LED_1_LS            	        : out std_logic;
	GPIO_LED_2_LS            	        : out std_logic;
	GPIO_LED_3_LS            	        : out std_logic;
	GPIO_LED_4_LS           	        : out std_logic;
	GPIO_LED_5_LS           	        : out std_logic;
	GPIO_LED_6_LS          	  	        : out std_logic;
	-- this is used also to OE the I2C bus to monitor the FPGA sensors (temp,voltages,.)
	GPIO_LED_7_LS          	  	        : out std_logic;
	
	--# 100 Gb/s
	
	--# Ref clock 100 Gb/s 322.265625 MHz
	QSFP_Clk_ref_A_P      			    : in std_logic;  
	QSFP_Clk_ref_A_N				    : in std_logic;                 
	QSFP_Clk_ref_B_P      			    : in std_logic;  
	QSFP_Clk_ref_B_N				    : in std_logic;  
	QSFP_Clk_ref_C_P      			    : in std_logic;  
	QSFP_Clk_ref_C_N				    : in std_logic;  
	QSFP_Clk_ref_D_P      			    : in std_logic;  
	QSFP_Clk_ref_D_N				    : in std_logic;   
	
	         
--  	QSFP_A_TX_P                         : out std_logic_vector(3 downto 0);
--	QSFP_A_TX_N                         : out std_logic_vector(3 downto 0);
--  	QSFP_A_RX_P                         : in std_logic_vector(3 downto 0);
-- 	QSFP_A_RX_N                         : in std_logic_vector(3 downto 0);
--   	QSFP_B_TX_P                         : out std_logic_vector(3 downto 0);
--	QSFP_B_TX_N                         : out std_logic_vector(3 downto 0);
--   	QSFP_B_RX_P                         : in std_logic_vector(3 downto 0);
--   	QSFP_B_RX_N                         : in std_logic_vector(3 downto 0);
--   	QSFP_C_TX_P                         : out std_logic_vector(3 downto 0);
--	QSFP_C_TX_N                         : out std_logic_vector(3 downto 0);
--   	QSFP_C_RX_P                         : in std_logic_vector(3 downto 0);
--   	QSFP_C_RX_N                         : in std_logic_vector(3 downto 0);
   	QSFP_D_TX_P                         : out std_logic_vector(3 downto 0);
	QSFP_D_TX_N                         : out std_logic_vector(3 downto 0);
   	QSFP_D_RX_P                         : in std_logic_vector(3 downto 0);
   	QSFP_D_RX_N                         : in std_logic_vector(3 downto 0);
   
	QSFP_A_MODSEL				    	: out std_logic;
	QSFP_A_RESETL				    	: out std_logic;
	QSFP_A_MODPRSL				    	: in std_logic;
	QSFP_A_INTL					    	: in std_logic;
	QSFP_A_LPMODE				    	: out std_logic;
	
	QSFP_B_MODSEL				    	: out std_logic;
	QSFP_B_RESETL				    	: out std_logic;
	QSFP_B_MODPRSL				    	: in std_logic;
	QSFP_B_INTL					    	: in std_logic;
	QSFP_B_LPMODE				    	: out std_logic;
	
	QSFP_C_MODSEL				    	: out std_logic;
	QSFP_C_RESETL				    	: out std_logic;
	QSFP_C_MODPRSL				    	: in std_logic;
	QSFP_C_INTL					    	: in std_logic;
	QSFP_C_LPMODE				    	: out std_logic;  
	
	QSFP_D_MODSEL				    	: out std_logic;
	QSFP_D_RESETL				    	: out std_logic;
	QSFP_D_MODPRSL				    	: in std_logic;
	QSFP_D_INTL					    	: in std_logic;
	QSFP_D_LPMODE				    	: out std_logic; 
			
	QSFP_MAIN_SDA				    	: inout std_logic;
	QSFP_MAIN_SCL				    	: inout std_logic;
	QSFP_MAIN_reset						: out std_logic;
	
	--# FireFly  SlinkRocket
	
	SlinkRocket_rxn						: in std_logic_vector(1 downto 0);
	SlinkRocket_rxp						: in std_logic_vector(1 downto 0);
	SlinkRocket_txn						: out std_logic_vector(1 downto 0);
	SlinkRocket_txp						: out std_logic_vector(1 downto 0);
		
	SR_Send_rxn							: in std_logic_vector(1 downto 0);
	SR_Send_rxp							: in std_logic_vector(1 downto 0);
	SR_Send_txn							: out std_logic_vector(1 downto 0);
	SR_Send_txp							: out std_logic_vector(1 downto 0);
	
--	SlinkRocket_rxn						: in std_logic_vector(23 downto 0);
--	SlinkRocket_rxp						: in std_logic_vector(23 downto 0);
--	SlinkRocket_txn						: out std_logic_vector(23 downto 0);
--	SlinkRocket_txp						: out std_logic_vector(23 downto 0);
	
	-- ref clock  (6 only)
	--Ref clock firefly 156.25 MHz
	SlinkRocket_refclkp					: in std_logic_vector(5 downto 0);
	SlinkRocket_refclkn					: in std_logic_vector(5 downto 0);
  
	
--	SR_tcds_ref_clk_p					: in std_logic;
--	SR_tcds_ref_clk_n					: in std_logic;
	
	FireFly_MAIN_SCL					: inout std_logic;
	FireFly_MAIN_SDA					: inout std_logic;
	FireFly_MAIN_reset					: out std_logic;
	
	FF_A_PRESENT						: in std_logic;
	FF_A_RESET_L						: out std_logic;
	FF_A_CS     						: out std_logic;
	FF_A_INTL_L 						: in std_logic;
							 
	FF_B_PRESENT						: in std_logic;
	FF_B_RESET_L						: out std_logic;
	FF_B_CS     						: out std_logic;
	FF_B_INTL_L 						: in std_logic;
							 
	FF_C_PRESENT						: in std_logic;
	FF_C_RESET_L						: out std_logic;
	FF_C_CS     						: out std_logic;
	FF_C_INTL_L 						: in std_logic;
							 
	FF_D_PRESENT						: in std_logic;
	FF_D_RESET_L						: out std_logic;
	FF_D_CS     						: out std_logic;
	FF_D_INTL_L 						: in std_logic;
							 
	FF_E_PRESENT						: in std_logic;
	FF_E_RESET_L						: out std_logic;
	FF_E_CS     						: out std_logic;
	FF_E_INTL_L 						: in std_logic;
		
	FF_F_PRESENT						: in std_logic;
	FF_F_RESET_L						: out std_logic;
	FF_F_CS     						: out std_logic;
	FF_F_INTL_L 						: in std_logic;

	
	 ref_ddr4_0_clock_p					: in std_logic; 								--IC20(1)
	 ref_ddr4_0_clock_n					: in std_logic;
	 
	 ref_ddr4_1_clock_p					: in std_logic; 								--IC20(2)
	 ref_ddr4_1_clock_n					: in std_logic;
	 
	-- TCDS serdes
--	TCDS_rx_n							: in std_logic;
--	TCDS_rx_p							: in std_logic;
--	TCDS_tx_n							: out std_logic;
--	TCDS_tx_p							: out std_logic;
	
--	TCDS_ref_clock_spare_p				: in std_logic;
--	TCDS_ref_clock_spare_n				: in std_logic;
	
--	TCDS_clock_p						: in std_logic;
--	TCDS_clock_n						: in std_logic; 
	
--	TCDS_clock_GC_p 					: in std_logic; 
--	TCDS_clock_GC_n 					: in std_logic; 
	
	--SR for TCDS
--	SR_TCDS_rxn							: in std_logic_vector(1 downto 0);
--	SR_TCDS_rxp							: in std_logic_vector(1 downto 0);
--	SR_TCDS_txn							: out std_logic_vector(1 downto 0);
--	SR_TCDS_txp							: out std_logic_vector(1 downto 0);
	
--	SR_TCDS_ref_clock_p					: in std_logic;
--	SR_TCDS_ref_clock_n					: in std_logic;
	--I2C busses
	
		-- Power monitoring
	POWER_MAIN_SCL					    : inout std_logic;
	POWER_MAIN_SDA					    : inout std_logic;
	I2C_sel								: out std_logic; 
	sysMon_i2c_sda						: inout std_logic;
	sysMon_i2c_sclk						: inout std_logic;
		-- Serial Number
	SN_MAIN_SCL					        : inout std_logic;
	SN_MAIN_SDA					        : inout std_logic;
		-- Clock generator
	ClockGen_MAIN_RESET			        : out std_logic;
	ClockGen_MAIN_SCL			        : inout std_logic;
	ClockGen_MAIN_SDA			        : inout std_logic;
	
	ClockGen_1_rst_n					: out std_logic := '1';
	ClockGen_2_rst_n					: out std_logic := '1';
	ClockGen_3_rst_n					: out std_logic := '1';
	
	ClockGen_1_LOS_n					: in std_logic;
	ClockGen_2_LOS_n					: in std_logic;
	ClockGen_3_LOS_n					: in std_logic;
	
	ClockGen_1_LOL_n					: in std_logic;
	ClockGen_2_LOL_n					: in std_logic;
	ClockGen_3_LOL_n					: in std_logic;
	
	ClockGen_1_INTR_n					: in std_logic;
	ClockGen_2_INTR_n					: in std_logic;
	ClockGen_3_INTR_n					: in std_logic;	
	
	--tcds clock in
--	TCDS_CLK_FINC						: OUT STD_LOGIC;
--	TCDS_CLK_FDEC						: OUT STD_LOGIC;
--	TCDS_CLK_SEL0						: OUT STD_LOGIC;
--	TCDS_CLK_SEL1						: OUT STD_LOGIC;
--	TCDS_CLK_RST						: OUT STD_LOGIC;
--	TCDS_CLK_INT						: IN STD_LOGIC;
--	TCDS_CLK_LOL						: IN STD_LOGIC;
 
--flash IO
	Flash_data							: inout std_logic_vector(15 downto 4);
	Flash_add							: out std_logic_vector(25 downto 0);
	Flash_oen							: out std_logic;
	Flash_wen							: out std_logic ;	
	
		
	--Ethernet switch SPI bus
	ETH_MOSI							: out std_logic := '0';
	ETH_MISO							: in std_logic;
	ETH_SCLK							: out std_logic := '0';
	ETH_SS								: out std_logic := '0';
	
	ETH_LOOP_DETECTED					: IN STD_LOGIC;
	ETH_ACT_LOOP_DETECT					: OUT STD_LOGIC := 'Z';

	-- Reserved pins 
	reserved							: inout std_logic_vector(31 downto 0)
	 
   );
end top;

--*///////////////////////////////////////////////////////////////////////////////
architecture Behavioral of top is
 
attribute KEEP : string; 

signal SLINK_speed          : std_logic_vector(63 downto 0);
   
signal delay_retreive_data_back		: std_logic_vector(2 downto 0);
 
signal Compilation_version 					: std_logic_vector(31 downto 0);
   

signal clock_low                            : std_logic;
 
signal 		status_out				:  std_logic_vector(31 downto 0);
signal 		clock_FED				:  std_logic;
signal 		clock_UR				:  std_logic;
signal 		UR_rst_p				:  std_logic;
 

signal SR0_ctrl_data_out				: std_logic_vector(63 downto 0);
signal SR_snd_ctrl_data_out		        : std_logic_vector(63 downto 0);

signal clock_SR0			: STD_LOGIC;	
signal SR0_WEN  			: STD_LOGIC;	
signal SR0_UCTRL			: STD_LOGIC;
signal SR0_DATA 			: STD_LOGIC_VECTOR(127 DOWNTO 0);
signal SR0_LFF  			: STD_LOGIC;

signal clock_SR1			: STD_LOGIC;	
signal SR1_WEN  			: STD_LOGIC;	
signal SR1_UCTRL			: STD_LOGIC;
signal SR1_DATA 			: STD_LOGIC_VECTOR(127 DOWNTO 0);
signal SR1_LFF  			: STD_LOGIC;
  

component i2c_master_32b_autoread is 
generic (
		boot_address       		: std_logic_vector(6 downto 0) := "1010000";
		boot_add_Bnb            : std_logic_vector(1 downto 0) := "10";
		boot_register      		: std_logic_vector(15 downto 0):= x"0002";
		boot_dt_Bnb             : std_logic_vector(2 downto 0) := "010";
		boot_start_stop_bwt		: std_logic := '1'
);
port (
	low_clk			: in std_logic; -- 1.6 MHz
	ena_low_clk		: in std_logic; -- enable the clock to have a clock of 4000 Khz double frequency of the I2C bus (400 KHz --> 800 KHz)
	reset_n		   	: in std_logic;
	clock			: in std_logic;
			
	SCL				: INOUT std_logic;
	SDAi			: IN std_logic;-- separate the IN and the OUT if we want to implement multiple 
	SDAo			: OUT std_logic;
	SDAz			: OUT std_logic;
			
	dti				: in std_logic_vector(31 downto 0);
	cs			 	: in std_logic;
	func			: in std_logic_vector(1 downto 0);
	wen				: in std_logic;
	rd 				: in std_logic;
	dto				: out std_logic_vector(31 downto 0);
	-- direct output of I2C read
	Byte_i2c_out	: out std_logic_vector(7 downto 0);
	Byte_i2c_latch	: out std_logic;
	ack_rq			: out std_logic --- use for simulation
);
end component;

signal record_MAC_bytes         : std_logic_vector(1 downto 0);
signal MAC_byte                 : std_logic_vector(7 downto 0);
signal MAC_bytes_latch          : std_logic;
signal MAC_bytes_low            : std_logic_vector(15 downto 0);

component clk_wiz_0
port
 (-- Clock in ports
  -- Clock out ports
  clk_out1          : out    std_logic;
  clk_out2          : out    std_logic;
  -- Status and control signals
  locked            : out    std_logic;
  clk_in1_p         : in     std_logic;
  clk_in1_n         : in     std_logic
 );
end component;
 
component clk_wiz_1
	port
	 (-- Clock in ports
	  -- Clock out ports
	  clk_out1          : out    std_logic;
	  clk_out2          : out    std_logic;
	  clk_out3          : out    std_logic; 
	  clk_out4          : out    std_logic; 
	  clk_in1_p         : in     std_logic;
	  clk_in1_n         : in     std_logic
	 );
end component;

component pll_clk_in_100Mhz
port
 (-- Clock in ports
  -- Clock out ports
  clk_out1          	: out    std_logic;
  -- Status and control signals
  reset             	: in     std_logic;
  locked            	: out    std_logic;
  clk_in1_p         	: in     std_logic;
  clk_in1_n         	: in     std_logic
 );
end component;

  
signal CLK_125MHZ									: std_logic;
 
signal usr_clk										: std_logic;
signal CLK_100MHz									: std_logic;
signal CLK_TX_ETH									: std_logic;
signal rst_usr_clk_n								: std_logic;

signal I2C_ena_clock_div							: std_logic_vector(31 downto 0);
 
 
signal data_axi_prog   				                 : std_logic_vector(63 DOWNTO 0);
signal dummy                                        : std_logic_vector(31 downto 0);

signal usr_func_wr								    : std_logic_vector(16383 downto 0); 
signal usr_wen					 				  	: std_logic;
signal usr_data_wr			  					    : std_logic_vector(63 downto 0);
signal usr_be					  					: std_logic_vector(7 downto 0);
                    
signal usr_func_rd								    : std_logic_vector(16383 downto 0); 
signal usr_rden									    : std_logic;
signal usr_data_rd								    : std_logic_vector(63 downto 0);
signal funct										: std_logic_vector(15 downto 0);
signal data_out									    : std_logic_vector(31 downto 0);
signal data_in										: std_logic_vector(31 downto 0);
signal data_out_cell								: std_logic_vector(31 downto 0);
signal data_in_rg									: std_logic_vector(63 downto 0);
signal wr_ena										: std_logic;
signal rd_ena										: std_logic;
signal wr_ena_cell								    : std_logic;
signal rd_ena_cell								    : std_logic;

signal led_monitor								    : std_logic_vector(31 downto 0);

signal QSFP_I2C_RESET_cell							    : std_logic;
signal FF_I2C_RESET_cell							    : std_logic;
signal Clock_I2C_RESET_cell							: std_logic;

signal QSFP_MAIN_SDA_cell							: std_logic;
signal QSFP_MAIN_SDA_tri							: std_logic;
signal cs_i2c_QSFP_master							: std_logic;
signal func_i2c_QSFP_master							: std_logic_vector(1 downto 0);
signal dto_i2c_QSFP_monitor							: std_logic_vector(63 downto 0) := (others => '0');

signal FireFly_MAIN_SDA_cell						: std_logic;
signal FireFly_MAIN_SDA_tri							: std_logic;
signal cs_i2c_FireFly_master						: std_logic;
signal func_i2c_FireFly_master						: std_logic_vector(1 downto 0);
signal dto_i2c_FireFly_monitor						: std_logic_vector(63 downto 0) := (others => '0');
				
				
signal POWER_MAIN_SDA_cell		    				: std_logic;
signal POWER_MAIN_SDA_tri							: std_logic;
signal cs_i2c_POWER_master		    				: std_logic;
signal func_i2c_POWER_master						: std_logic_vector(1 downto 0);
signal dto_i2c_POWER_monitor						: std_logic_vector(63 downto 0) := (others => '0');
				
signal SN_MAIN_SDA_cell		        				: std_logic;
signal SN_MAIN_SDA_tri			    				: std_logic;
signal cs_i2c_SN_master		        				: std_logic;
signal func_i2c_SN_master		    				: std_logic_vector(1 downto 0);
signal dto_i2c_SN_monitor		    				: std_logic_vector(63 downto 0) := (others => '0');

signal ClockGen_1_rst_cell							: std_logic;
signal ClockGen_2_rst_cell							: std_logic;
signal ClockGen_3_rst_cell							: std_logic;
				
signal ClockGen_MAIN_SDA_cell		        		: std_logic;
signal ClockGen_MAIN_SDA_tri			    		: std_logic;
signal cs_i2c_ClockGen_master		        		: std_logic;
signal func_i2c_ClockGen_master		    			: std_logic_vector(1 downto 0);
signal dto_i2c_ClockGen_monitor		    			: std_logic_vector(63 downto 0) := (others => '0');

signal dto_sysmon_module							: std_logic_vector(63 downto 0) := (others => '0');

signal ena_clock_i2c								: std_logic;

signal I2C_reset_p								    : std_logic;
signal I2C_reset_n								    : std_logic;
signal resetp_counters                              : std_logic;
 
signal QSFP_A_MODSEL_reg						    : std_logic;
signal QSFP_A_RESETL_reg						    : std_logic;
signal QSFP_A_LPMODE_reg						    : std_logic;	
signal QSFP_B_MODSEL_reg						    : std_logic;
signal QSFP_B_RESETL_reg						    : std_logic;
signal QSFP_B_LPMODE_reg						    : std_logic;	
signal QSFP_C_MODSEL_reg						    : std_logic;
signal QSFP_C_RESETL_reg						    : std_logic;
signal QSFP_C_LPMODE_reg						    : std_logic;	
signal QSFP_D_MODSEL_reg						    : std_logic;
signal QSFP_D_RESETL_reg						    : std_logic;
signal QSFP_D_LPMODE_reg						    : std_logic;		



signal FF_A_CS_cell   								: std_logic; 
signal FF_A_RESET_cell								: std_logic; 
signal FF_B_CS_cell   								: std_logic; 
signal FF_B_RESET_cell								: std_logic; 
signal FF_C_CS_cell   								: std_logic; 
signal FF_C_RESET_cell								: std_logic; 
signal FF_D_CS_cell   								: std_logic; 
signal FF_D_RESET_cell								: std_logic; 
signal FF_E_CS_cell   								: std_logic; 
signal FF_E_RESET_cell								: std_logic; 
signal FF_F_CS_cell   								: std_logic; 
signal FF_F_RESET_cell								: std_logic; 

  
signal dto_DAQ_A_module							    : std_logic_vector(63 downto 0);
signal dto_DAQ_B_module							    : std_logic_vector(63 downto 0); 
signal dto_DAQ_C_module							    : std_logic_vector(63 downto 0);
signal dto_DAQ_D_module							    : std_logic_vector(63 downto 0); 

signal DTO_25gty							    	: std_logic_vector(63 downto 0); 

signal gt_ref_clk_A									: std_logic;
signal gt_ref_clk_B									: std_logic;
signal gt_ref_clk_C									: std_logic;
signal gt_ref_clk_D									: std_logic;

signal gtyrefclk00_in							    : std_logic_vector(3 downto 0);
signal gthrefclk00_in							    : std_logic_vector(1 downto 0);
  
signal SR_TCDS_ref_clk								: std_logic;

attribute mark_debug : string;     

signal SERDES_RESET_P								: std_logic;
signal dto_DAQ_module								: std_logic_vector(63 downto 0);

signal debug0_counter                               : std_logic_vector(31 downto 0);

component freq_measure_base is
generic( freq_used: std_logic_vector(31 downto 0) := x"0001E848");-- set for 125 Mhz ref clock (base_clk)
port (
	sysclk				: in std_logic;-- clock used by the FED to send data and to measure the backpressure
	base_clk			: in std_logic;-- clock base used to measure the sysclk
	
	frequency			: out std_logic_vector(31 downto 0)-- measure of the frequency)
);
end component;

signal measure_FED_clock                         : std_logic_vector(31 downto 0);
signal measure_UR_clock                          : std_logic_vector(31 downto 0);
signal measure_SR0_clock                          : std_logic_vector(31 downto 0);
signal measure_SR1_clock                          : std_logic_vector(31 downto 0);

--#############################################################################
-- Code start here
--#############################################################################
begin


pll_inst0: clk_wiz_0
port map
 (-- Clock in ports
  -- Clock out ports
  clk_out1          => clock_FED, 
  clk_out2          => clock_UR, 
  -- Status and control signals
--  locked            => , 
  clk_in1_p         => CLK_100MHZ_P, 
  clk_in1_n         => CLK_100MHZ_n
 ); 
 
pll_inst: clk_wiz_1
	port map
	 (-- Clock in ports
	  -- Clock out ports
	  clk_out1          	=> CLK_100MHz   	,      -- 100 Mhz        
	  clk_out2          	=> CLK_125MHZ     	,      -- 125 Mhz
--	  clk_out3		    	=> clock_FED		,      -- 200 MHz  
      clk_out4		    	=> CLK_TX_ETH		,      -- 250 MHz  
	  clk_in1_p         	=> CLK_125MHZ_P  	,              
	  clk_in1_n         	=> CLK_125MHZ_N                    
	 );
  
 
monitor_interface:entity work.PCIe_interface  
 Port map( 
 	pcie_rst				=>	PCIE_rst									, 
 	pcie_clk_n				=>	PCIE_clk_n									, 
 	pcie_clk_p				=>	PCIE_clk_p									, 
 	pcie_tx_n				=>	PCIE_TX0_N									, 
 	pcie_tx_p				=>	PCIE_TX0_P									, 
 	pcie_rx_n				=>	PCIE_RX0_N									, 
 	pcie_rx_p				=>	PCIE_RX0_P									,   	  	  	
 	
	usr_clk					=>	usr_clk										, 
	usr_rst_n				=>	rst_usr_clk_n								, 
	usr_func_wr				=>	usr_func_wr									, 
	usr_wen					=>	usr_wen										,
	usr_data_wr				=>	usr_data_wr									, 
	usr_func_rd				=>	usr_func_rd									, 
	usr_rden				=>	usr_rden									,
	usr_data_rd				=>	usr_data_rd									,
	usr_rd_val				=>	delay_retreive_data_back(2)		 
	
 );
		  
 process(usr_clk)
begin
	if rising_edge(usr_clk) then
		delay_retreive_data_back(2 downto 1) <= delay_retreive_data_back(1 downto 0);
		delay_retreive_data_back(0)			 <= usr_rden;
	end if;
end process;		

--**********************************************************************
--  QSFP control pins  (WRITE)	
ver_comp_i1:entity work.version_comp
	PORT MAP
	(
		VERSION		=> Compilation_version
	);  
 		  
--**********************************************************************
--  QSFP control pins  (WRITE)		  
		  
process(rst_usr_clk_n,usr_clk)
begin
	if rst_usr_clk_n = '0' then
		led_monitor(31 downto 4)	<= (others => '0');
		I2C_reset_p			    <= '1';
		QSFP_A_RESETL_reg		<= '0'; 
		QSFP_B_RESETL_reg		<= '0'; 
		QSFP_C_RESETL_reg		<= '0'; 
		QSFP_D_RESETL_reg		<= '0'; 
		QSFP_I2C_RESET_cell		<= '0';
		FF_I2C_RESET_cell		<= '0';
		Clock_I2C_RESET_cell	<= '0';
		FF_A_RESET_cell			<= '0';
		FF_B_RESET_cell			<= '0';
		FF_C_RESET_cell			<= '0';
		FF_D_RESET_cell			<= '0';
		FF_E_RESET_cell			<= '0';
		FF_F_RESET_cell			<= '0';
		ClockGen_1_rst_cell		<= '0';	
		ClockGen_2_rst_cell		<= '0';	
		ClockGen_3_rst_cell		<= '0'; 
		SERDES_RESET_P    		<= '0';
		UR_rst_p    			<= '0';
		resetp_counters         <= '0';
	elsif rising_edge(usr_clk) then
  
		if usr_wen = '1'  then
 
			if usr_func_wr(LED_control) = '1' then
				led_monitor(31 downto 4)	<= usr_data_wr(31 downto 4);
				
			end if; 
			
			if usr_func_wr(reset_hw_comp) = '1' then
				SERDES_RESET_P          <= usr_data_wr(0);
				resetp_counters         <= usr_data_wr(16);
				UR_rst_p				<= usr_data_wr(27);
				QSFP_I2C_RESET_cell		<= usr_data_wr(28);
				FF_I2C_RESET_cell		<= usr_data_wr(29);
				Clock_I2C_RESET_cell	<= usr_data_wr(30);
				I2C_reset_p       	    <= usr_data_wr(31);
			end if;
			
			if usr_func_wr(QSFP_monitoring) = '1' then
				QSFP_A_RESETL_reg		<= usr_data_wr(1);
				QSFP_B_RESETL_reg		<= usr_data_wr(9);
				QSFP_C_RESETL_reg		<= usr_data_wr(17);
				QSFP_D_RESETL_reg		<= usr_data_wr(25);
			end if;					

			if usr_func_wr(Firefly_monitoring) = '1' then
				FF_A_RESET_cell			<= usr_data_wr(1);
				FF_B_RESET_cell			<= usr_data_wr(5);
				FF_C_RESET_cell			<= usr_data_wr(9);
				FF_D_RESET_cell			<= usr_data_wr(13);
				FF_E_RESET_cell			<= usr_data_wr(17);
				FF_F_RESET_cell			<= usr_data_wr(21);
			end if;			
			
			if usr_func_wr(CLOCK_gen_status) = '1' then
				ClockGen_1_rst_cell		<= usr_data_wr(0);
				ClockGen_2_rst_cell		<= usr_data_wr(4);
				ClockGen_3_rst_cell		<= usr_data_wr(8); 
			end if;
									
		end if;
	end if;
end process;

process(usr_clk)
begin
	if rising_edge(usr_clk) then
  
		if usr_wen = '1'   then
			if usr_func_wr(QSFP_monitoring) = '1' then
				QSFP_A_MODSEL_reg	  	<= usr_data_wr(0);
				QSFP_A_LPMODE_reg	 	<= usr_data_wr(4);
				QSFP_B_MODSEL_reg	  	<= usr_data_wr(8);
				QSFP_B_LPMODE_reg	 	<= usr_data_wr(12);
				QSFP_C_MODSEL_reg	   	<= usr_data_wr(16);
				QSFP_C_LPMODE_reg	 	<= usr_data_wr(20);
				QSFP_D_MODSEL_reg	   	<= usr_data_wr(24);
				QSFP_D_LPMODE_reg	 	<= usr_data_wr(28);
			end if;	

			if usr_func_wr(Firefly_monitoring) = '1' then
				FF_A_CS_cell			<= usr_data_wr(0);
				FF_B_CS_cell			<= usr_data_wr(4);
				FF_C_CS_cell			<= usr_data_wr(8);
				FF_D_CS_cell			<= usr_data_wr(12);
				FF_E_CS_cell			<= usr_data_wr(16);
				FF_F_CS_cell			<= usr_data_wr(20);
			end if;
		end if;
	end if;
end process;

--**********************************************************************
--  QSFP control pins  (READ)	
 
process(usr_clk)
begin
	if rising_edge(usr_clk) then
		data_in_rg 							       	<= (others => '0');
		 
			if 		usr_func_rd(LED_control) = '1' then
				data_in_rg(29 downto 0)         	<= led_monitor(29 downto 0);
				
			elsif usr_func_rd(Compilation_version_off) = '1' then
				data_in_rg(31 downto 0)         	<= Compilation_version;
				
			elsif usr_func_rd(Slink_speed_off) = '1' then
				data_in_rg	 			           	<= SLINK_speed;	 
				
			elsif usr_func_rd(reset_hw_comp) = '1' then
				data_in_rg(16) 			           	<= resetp_counters;	 	 
				data_in_rg(27) 			           	<= UR_rst_p;	 	 
				data_in_rg(28) 			           	<= QSFP_I2C_RESET_cell;	 	 
				data_in_rg(29) 			           	<= FF_I2C_RESET_cell;		 
				data_in_rg(30) 			           	<= Clock_I2C_RESET_cell;	 
				data_in_rg(31)                  	<= I2C_reset_p;		 
				
			elsif usr_func_rd(QSFP_monitoring) = '1' then
				data_in_rg(0) 			           	<= QSFP_A_MODSEL_reg; 	-- module select
				data_in_rg(1) 			           	<= QSFP_A_RESETL_reg; 	-- module reset
				data_in_rg(2) 			           	<= QSFP_A_MODPRSL; 		-- module present
				data_in_rg(3) 			           	<= QSFP_A_INTL; 		-- interruption
				data_in_rg(4) 			           	<= QSFP_A_LPMODE_reg;	-- low power mode	
				
				data_in_rg(8) 			           	<= QSFP_B_MODSEL_reg; 	-- module select
				data_in_rg(9) 			           	<= QSFP_B_RESETL_reg; 	-- module reset
				data_in_rg(10) 			           	<= QSFP_B_MODPRSL; 		-- module present
				data_in_rg(11) 			           	<= QSFP_B_INTL; 		-- interruption
				data_in_rg(12) 			           	<= QSFP_B_LPMODE_reg;	-- low power mode	
				
				data_in_rg(16) 			           	<= QSFP_C_MODSEL_reg; 	-- module select
				data_in_rg(17) 			           	<= QSFP_C_RESETL_reg; 	-- module reset
				data_in_rg(18) 			           	<= QSFP_C_MODPRSL; 		-- module present
				data_in_rg(19) 			           	<= QSFP_C_INTL; 		-- interruption
				data_in_rg(20) 			           	<= QSFP_C_LPMODE_reg;	-- low power mode	
				
				data_in_rg(24) 			           	<= QSFP_D_MODSEL_reg; 	-- module select
				data_in_rg(25) 			           	<= QSFP_D_RESETL_reg; 	-- module reset
				data_in_rg(26) 			           	<= QSFP_D_MODPRSL; 		-- module present
				data_in_rg(27) 			           	<= QSFP_D_INTL; 		-- interruption
				data_in_rg(28) 			           	<= QSFP_D_LPMODE_reg;	-- low power mode	
			
			elsif usr_func_rd(Firefly_monitoring) = '1' then

				data_in_rg(0) 			           	<= FF_A_CS_cell;		-- module select
				data_in_rg(1) 			           	<= FF_A_RESET_cell;		-- module reset
				data_in_rg(2) 			           	<= FF_A_PRESENT;     	-- module present
				data_in_rg(3) 			           	<= FF_A_INTL_L; 		-- interruption 
				
				data_in_rg(4) 			           	<= FF_B_CS_cell;		-- module select
				data_in_rg(5) 			           	<= FF_B_RESET_cell;		-- module reset
				data_in_rg(6) 			           	<= FF_B_PRESENT;     	-- module present
				data_in_rg(7) 			           	<= FF_B_INTL_L; 		-- interruption 	
				
				data_in_rg(8) 			           	<= FF_C_CS_cell;		-- module select
				data_in_rg(9) 			           	<= FF_C_RESET_cell;		-- module reset
				data_in_rg(10) 			           	<= FF_C_PRESENT;     	-- module present
				data_in_rg(11) 			           	<= FF_C_INTL_L; 		-- interruption 
				
				data_in_rg(12) 			           	<= FF_D_CS_cell;		-- module select
				data_in_rg(13) 			           	<= FF_D_RESET_cell;		-- module reset
				data_in_rg(14) 			           	<= FF_D_PRESENT;     	-- module present
				data_in_rg(15) 			           	<= FF_D_INTL_L; 		-- interruption 
				
				data_in_rg(16) 			           	<= FF_E_CS_cell;		-- module select
				data_in_rg(17) 			           	<= FF_E_RESET_cell;		-- module reset
				data_in_rg(18) 			           	<= FF_E_PRESENT;     	-- module present
				data_in_rg(19) 			           	<= FF_E_INTL_L; 		-- interruption 
				
				data_in_rg(20) 			           	<= FF_F_CS_cell;		-- module select
				data_in_rg(21) 			           	<= FF_F_RESET_cell;		-- module reset
				data_in_rg(22) 			           	<= FF_F_PRESENT;     	-- module present
				data_in_rg(23) 			           	<= FF_F_INTL_L; 		-- interruption 
				
			elsif usr_func_rd(CLOCK_gen_status) = '1' then
				data_in_rg(0) 			           	<= ClockGen_1_rst_cell;	-- gen clock reset
				data_in_rg(1) 			           	<= ClockGen_1_LOS_n;	--  
				data_in_rg(2) 			           	<= ClockGen_1_LOL_n;    -- 
				data_in_rg(3) 			           	<= ClockGen_1_INTR_n; 	-- interruption 
				
				data_in_rg(4) 			           	<= ClockGen_2_rst_cell;	-- gen clock reset
				data_in_rg(5) 			           	<= ClockGen_2_LOS_n;	--  
				data_in_rg(6) 			           	<= ClockGen_2_LOL_n;    -- 
				data_in_rg(7) 			           	<= ClockGen_2_INTR_n; 	-- interruption 
				
				data_in_rg(8) 			           	<= ClockGen_3_rst_cell;	-- gen clock reset
				data_in_rg(9) 			           	<= ClockGen_3_LOS_n;	--  
				data_in_rg(10) 			           	<= ClockGen_3_LOL_n;    -- 
				data_in_rg(11) 			           	<= ClockGen_3_INTR_n; 	-- interruption 
				
			elsif usr_func_rd(clock_FED_measure) = '1' then
				data_in_rg(31 downto 0) 	    	<= measure_FED_clock;	--  	
			elsif usr_func_rd(clock_UR_measure) = '1' then
				data_in_rg(31 downto 0) 	    	<= measure_UR_clock;	--  
				
			elsif usr_func_rd(clock_SR0_measure) = '1' then
				data_in_rg(31 downto 0) 	    	<= measure_SR0_clock;	--  	
			elsif usr_func_rd(clock_SR1_measure) = '1' then
				data_in_rg(31 downto 0) 	    	<= measure_SR1_clock;	--  
			end if;
	 
	end if;
end process;

usr_data_rd <= SR_snd_ctrl_data_out or SR0_ctrl_data_out or data_in_rg or dto_i2c_QSFP_monitor or dto_sysmon_module or dto_i2c_ClockGen_monitor or dto_i2c_FireFly_monitor or dto_i2c_POWER_monitor or dto_i2c_SN_monitor or dto_DAQ_module or data_axi_prog;


--*********************************************
-- **  this value specify if we have to disable CDR on firefly
SLINK_speed <= x"0000000000000010" when  throughput = "15.66" else  x"0000000000000019";

---*********** QSFP control ********************

   QSFP_MAIN_reset				<= '0' when QSFP_I2C_RESET_cell = '1' else 'Z';

   QSFP_A_MODSEL				<= QSFP_A_MODSEL_reg; -- should '0' to reply to the I2C bus
   QSFP_A_RESETL				<= QSFP_A_RESETL_reg; -- should be '0' to reset the module
   QSFP_A_LPMODE				<= QSFP_A_LPMODE_reg; -- should be '1' to be in low power mode

   QSFP_B_MODSEL				<= QSFP_B_MODSEL_reg; -- should '0' to reply to the I2C bus      
   QSFP_B_RESETL				<= QSFP_B_RESETL_reg; -- should be '0' to reset the module     
   QSFP_B_LPMODE				<= QSFP_B_LPMODE_reg; -- should be '1' to be in low power mode

   QSFP_C_MODSEL				<= QSFP_C_MODSEL_reg; -- should '0' to reply to the I2C bus   
   QSFP_C_RESETL				<= QSFP_C_RESETL_reg; -- should be '0' to reset the module    
   QSFP_C_LPMODE				<= QSFP_C_LPMODE_reg; -- should be '1' to be in low power mode
   
   QSFP_D_MODSEL				<= QSFP_D_MODSEL_reg; -- should '0' to reply to the I2C bus   
   QSFP_D_RESETL				<= QSFP_D_RESETL_reg; -- should be '0' to reset the module    
   QSFP_D_LPMODE				<= QSFP_D_LPMODE_reg; -- should be '1' to be in low power mode  

 
---*********** Firefly control ********************
	FireFly_MAIN_reset			<= '0' when FF_I2C_RESET_cell = '1'  else 'Z';
	
	FF_A_RESET_L				<= FF_A_RESET_cell;
	FF_A_CS     				<= FF_A_CS_cell;
	FF_B_RESET_L				<= FF_B_RESET_cell;
	FF_B_CS     				<= FF_B_CS_cell;
	FF_C_RESET_L				<= FF_C_RESET_cell;
	FF_C_CS     				<= FF_C_CS_cell;
	FF_D_RESET_L				<= FF_D_RESET_cell;
	FF_D_CS     				<= FF_D_CS_cell;
	FF_E_RESET_L				<= FF_E_RESET_cell;
	FF_E_CS     				<= FF_E_CS_cell;
	FF_F_RESET_L				<= FF_F_RESET_cell;
	FF_F_CS     				<= FF_F_CS_cell;
	
 
--*************  Clock gen status ******************************************
	ClockGen_MAIN_RESET			<= '0' when Clock_I2C_RESET_cell = '1'  else 'Z';
	
	ClockGen_1_rst_n			<= '0' when ClockGen_1_rst_cell = '1'  else 'Z';
	ClockGen_2_rst_n			<= '0' when ClockGen_2_rst_cell = '1'  else 'Z';
	ClockGen_3_rst_n			<= '0' when ClockGen_3_rst_cell = '1'  else 'Z';

--**********************************************************************
--  Implement four DAQ module	

clock_low       <= ena_clock_i2c;
 
 
DAQ_i1:entity work.DAQ_module 
	generic map(addr_offset_100G_eth => addr_offset_100G_eth0 )
   Port map( 
        clock_low                           => clock_low                    ,-- low clock ~1MHz for ARP probe
        MAC_lower_part                      =>  MAC_bytes_low               ,
	-- PCIe local interface
		usr_clk								=>	usr_clk				      	,
		rst_usr_clk_n						=>	rst_usr_clk_n				,
		usr_func_wr							=>	usr_func_wr				   	, 
		usr_wen								=>	usr_wen				       	,
		usr_data_wr							=>	usr_data_wr					, 
			                           
		usr_func_rd							=>	usr_func_rd				   	, 
		usr_rden							=>	usr_rden					,
		usr_dto								=>	dto_DAQ_module				,

		clock_SR0							=> clock_SR0			        ,
		SR0_DATA							=> SR0_DATA  			        ,
		SR0_UCTRL							=> SR0_UCTRL			        ,
		SR0_WEN								=> SR0_WEN 			            ,
		SR0_LFF							   	=> SR0_LFF                      ,
			
		clock_SR1							=> clock_SR1			        ,
		SR1_DATA							=> SR1_DATA  			        ,
		SR1_UCTRL							=> SR1_UCTRL			        ,
		SR1_WEN								=> SR1_WEN 			            ,
		SR1_LFF							   	=> SR1_LFF                      ,
										 
		ext_trigger							=> '0'							,
		--ext_veto_out						=> 								,
		
		clock_fed							=> 	clock_FED					,         
		clock_UR							=> 	clock_UR					,   
		UR_rst_p							=>  UR_rst_p					,
	-- QSFP connection        
		QSFP_TX_P 							=>	QSFP_D_TX_P				    , 
		QSFP_TX_N 							=>	QSFP_D_TX_N				    ,       
		QSFP_RX_P 							=>	QSFP_D_RX_P				    ,
		QSFP_RX_N 							=>	QSFP_D_RX_N				    ,
	-- SERDES/MAC                     
		SERDES_reset_p						=>	SERDES_RESET_P			    ,
		init_clock							=>	CLK_100MHz				    , 
		GT_ref_clock_p      				=>	QSFP_Clk_ref_D_P		    ,
		GT_ref_clock_n      				=>	QSFP_Clk_ref_D_N		    ,
		status_out							=>  status_out
		);

DAQ_transmit_led:entity work.pulse_led  
port map(
	reset			=> rst_usr_clk_n,
	fast_clock	    => usr_clk,
	ena_clock	    => ena_clock_i2c,
	activity		=> status_out(0),
	led_pulse	    => led_monitor(0) 
);

DAQ_receive_led:entity work.pulse_led  
port map(
	reset			 => rst_usr_clk_n,
	fast_clock	     => usr_clk,
	ena_clock	     => ena_clock_i2c,
	activity		 => status_out(1),
	led_pulse	     => led_monitor(1) 
);		
--**********************************************************************
--  SlinkRocket receiver

SR_REC_i1:entity work.SR_receiver_quad
generic map(SR0_offset         =>  addr_offset_slink_0,
            SR1_offset         =>  addr_offset_slink_1,
            throughput	       =>  throughput,
		    ref_clock		   =>  ref_clock,
		    technology	       =>  technology,
		    UltraRam_mem       =>  UltraRam_mem)
 Port map(
    usr_clk           		=> usr_clk			,
    rst_usr_clk_n     		=> rst_usr_clk_n	,
    usr_func_wr       		=> usr_func_wr		,
    usr_wen           		=> usr_wen			,
    usr_data_wr       		=> usr_data_wr		,
    usr_func_rd       		=> usr_func_rd		,
    usr_rden          		=> usr_rden			,
    usr_dto_receiver  		=> SR0_ctrl_data_out, 
    resetp_counters         => resetp_counters   ,
    						 
	clk_freerun_in			=> CLK_100MHz,
							 
	clock_SR0				=> clock_SR0,
	SR0_WEN  				=> SR0_WEN  ,
	SR0_UCTRL				=> SR0_UCTRL,
	SR0_DATA 				=> SR0_DATA ,
	SR0_LFF  				=> SR0_LFF  ,
								 
	clock_SR1				=> clock_SR1,
	SR1_WEN  				=> SR1_WEN  ,
	SR1_UCTRL				=> SR1_UCTRL,
	SR1_DATA 				=> SR1_DATA ,
	SR1_LFF  				=> SR1_LFF  ,
							 
    SRI_ref_clkp    		=> SlinkRocket_refclkp(0),
    SRI_ref_clkn    		=> SlinkRocket_refclkn(0),
	SRI_gt_rxn_in     		=> SlinkRocket_rxn,
    SRI_gt_rxp_in     		=> SlinkRocket_rxp,
    SRI_gt_txn_out    		=> SlinkRocket_txn,
    SRI_gt_txp_out    		=> SlinkRocket_txp
);	
		
--**********************************************************************
--  SlinkRocket sender

SR_SND_i2:entity work.SR_transmit_Quad  
generic map(SR0_offset          => addr_offset_slinkS_0,
            SR1_offset          => addr_offset_slinkS_1,
            throughput	       =>  throughput,
		    ref_clock		   =>  ref_clock,
		    technology	       =>  technology)
 Port map( 
	usr_clk           	=> usr_clk			,
    rst_usr_clk_n     	=> rst_usr_clk_n	,
    usr_func_wr       	=> usr_func_wr		,
    usr_wen           	=> usr_wen			,
    usr_data_wr       	=> usr_data_wr		,
    usr_func_rd       	=> usr_func_rd		,
    usr_rden          	=> usr_rden			,
    usr_dto_receiver  	=> SR_snd_ctrl_data_out,
						 
	clk_freerun_in		=> CLK_100MHz,
	FED_clock			=> clock_FED,
						 
	SRO_ref_clkp    	=> SlinkRocket_refclkp(3),
    SRO_ref_clkn    	=> SlinkRocket_refclkn(3),
	SRO_gt_rxn_in     	=> SR_Send_rxn,
    SRO_gt_rxp_in     	=> SR_Send_rxp,
    SRO_gt_txn_out    	=> SR_Send_txn,
    SRO_gt_txp_out    	=> SR_Send_txp,
    
    Rst_hrd_sim         => '0'
	
 );
 
SR_led0:entity work.pulse_led  
port map(
	reset			 => rst_usr_clk_n,
	fast_clock	     => usr_clk,
	ena_clock	     => ena_clock_i2c,
	activity		 => SR0_UCTRL,
	led_pulse	     => led_monitor(2) 
);		 
 
SR_led1:entity work.pulse_led  
port map(
	reset			 => rst_usr_clk_n,
	fast_clock	     => usr_clk,
	ena_clock	     => ena_clock_i2c,
	activity		 => SR1_UCTRL,
	led_pulse	     => led_monitor(3) 
);
--**********************************************************************
--  Clock monitor
freq_measure_FED:entity work.freq_measure_base  
generic map (freq_used => x"0001E848")
port map(
    resetn				=> '1',
	sysclk				=> clock_FED,-- clock used by the FED to send data and to measure the backpressure
	base_clk			=> CLK_125MHZ,-- clock base used to measure the sysclk
	
	frequency			=> measure_FED_clock-- measure of the frequency)
);

freq_measure_UR:entity work.freq_measure_base  
generic map (freq_used => x"0001E848")  
port map(
    resetn				=> '1',
	sysclk				=> clock_UR,-- clock used by the FED to send data and to measure the backpressure
	base_clk			=> CLK_125MHZ,-- clock base used to measure the sysclk
	
	frequency			=> measure_UR_clock-- measure of the frequency)
);

freq_measure_SR0:entity work.freq_measure_base  
generic map (freq_used => x"0001E848")  
port map(
    resetn				=> '1',
	sysclk				=> clock_SR0,-- clock used by the FED to send data and to measure the backpressure
	base_clk			=> CLK_125MHZ,-- clock base used to measure the sysclk
	
	frequency			=> measure_SR0_clock-- measure of the frequency)
);

freq_measure_SR1:entity work.freq_measure_base  
generic map (freq_used => x"0001E848")  
port map(
    resetn				=> '1',
	sysclk				=> clock_SR1,-- clock used by the FED to send data and to measure the backpressure
	base_clk			=> CLK_125MHZ,-- clock base used to measure the sysclk
	
	frequency			=> measure_SR1_clock-- measure of the frequency)
);


--**********************************************************************
--  Debug part
process(CLK_100MHz)
begin
    if rising_edge(CLK_100MHz) then
        debug0_counter  <= debug0_counter + '1';
    end if;
end process;
 
GPIO_LED_0_LS   <= led_monitor(0); -- DAQ transmit
GPIO_LED_1_LS   <= led_monitor(1); -- DAQ receive
GPIO_LED_2_LS   <= led_monitor(2); -- SR0 receiver;
GPIO_LED_3_LS   <= led_monitor(3); -- SR1 receiver;
GPIO_LED_4_LS   <= led_monitor(4);
GPIO_LED_5_LS   <= I2C_ena_clock_div(22);
GPIO_LED_6_LS   <= led_monitor(6);
GPIO_LED_7_LS   <= led_monitor(7);

--###########################################################################
--**************** I2C  Clock divider ************************************* 
process(usr_clk)
begin
	 if rising_edge(usr_clk) then
		I2C_ena_clock_div <= I2C_ena_clock_div + '1';
	end if;
end process;

ena_clock_i2c <= '1' when I2C_ena_clock_div(6 downto 0) = "1000000" else '0';
--###########################################################################
-- I2C  QSFP28  (4 elements) 

I2C_reset_n   <= not(I2C_reset_p);

monitor_itf_i2c_QSFP:entity work.i2c_master_32b  
port map(
	low_clk			  		=>	usr_clk				            , -- 25 MHz
	ena_low_clk		  		=>	ena_clock_i2c			            , -- enable the clock to have a clock of 4000 Khz double frequency of the I2C bus (400 KHz --> 800 KHz)
	reset_n					=>	I2C_reset_n			        	,
	clock					=>	usr_clk				                ,		
	SCL				   		=>	QSFP_MAIN_SCL		                ,
	SDAi					=>	QSFP_MAIN_SDA		                ,-- separate the IN and the OUT if we want to implement multiple 
	SDAo					=>	QSFP_MAIN_SDA_cell	                , 
	SDAz					=>	QSFP_MAIN_SDA_tri	                , 
	dti				   		=>	usr_data_wr(31 downto 0)			, 
	cs						=>	'1'						            , 
	func					=>	func_i2c_QSFP_master                , 
	wen				  		=>	usr_wen					            , 
	rd 				   		=>	usr_rden					        , 
	dto				   		=>	dto_i2c_QSFP_monitor(31 downto 0)		 
	--ack_rq			=>				 --: out std_logic --- use for simulation
	);

func_i2c_QSFP_master(0)	<= '1' when (usr_func_wr(QSFP_i2c_access_a) = '1' and usr_wen = '1' ) or (usr_func_rd(QSFP_i2c_access_a) = '1' and usr_rden = '1' ) else '0'; -- Address; function and Offset
func_i2c_QSFP_master(1)	<= '1' when (usr_func_wr(QSFP_i2c_access_b) = '1' and usr_wen = '1' ) or (usr_func_rd(QSFP_i2c_access_b) = '1' and usr_rden = '1' ) else '0'; -- Data
 
 		 
QSFP_MAIN_SDA <= QSFP_MAIN_SDA_cell when QSFP_MAIN_SDA_tri = '1' else 'Z';		



--###########################################################################
-- I2C  FireFly  (6 elements) 


monitor_itf_i2c_Firefly:entity work.i2c_master_32b  
port map(
	low_clk			  		=>	usr_clk				            , -- 25 MHz
	ena_low_clk		  		=>	ena_clock_i2c			            , -- enable the clock to have a clock of 4000 Khz double frequency of the I2C bus (400 KHz --> 800 KHz)
	reset_n					=>	I2C_reset_n			        ,
	clock					=>	usr_clk				                ,		
	SCL				   		=>	FireFly_MAIN_SCL		                ,
	SDAi					=>	FireFly_MAIN_SDA		                ,-- separate the IN and the OUT if we want to implement multiple 
	SDAo					=>	FireFly_MAIN_SDA_cell	                , 
	SDAz					=>	FireFly_MAIN_SDA_tri	                , 
	dti				   		=>	usr_data_wr(31 downto 0)			, 
	cs						=>	'1'						            , 
	func					=>	func_i2c_FireFly_master		                , 
	wen				  		=>	usr_wen					            , 
	rd 				   		=>	usr_rden					        , 
	dto				   		=>	dto_i2c_FireFly_monitor(31 downto 0)		 
	--ack_rq			=>				 --: out std_logic --- use for simulation
	);

func_i2c_FireFly_master(0)	<= '1' when (usr_func_wr(FireFly_i2c_access_a) = '1' and usr_wen = '1') or (usr_func_rd(FireFly_i2c_access_a) = '1' and usr_rden = '1') else '0'; -- Address; function and Offset
func_i2c_FireFly_master(1)	<= '1' when (usr_func_wr(FireFly_i2c_access_b) = '1' and usr_wen = '1') or (usr_func_rd(FireFly_i2c_access_b) = '1' and usr_rden = '1') else '0'; -- Data
 
 		 
FireFly_MAIN_SDA <= FireFly_MAIN_SDA_cell when FireFly_MAIN_SDA_tri = '1' else 'Z';		


--###########################################################################
-- I2C  Monitoring/Power  (x elements) 
 
monitor_itf_i2c_i3:entity work.i2c_master_32b  
port map(
	low_clk			  		=>	usr_clk				            , -- 25 MHz
	ena_low_clk		  		=>	ena_clock_i2c			            , -- enable the clock to have a clock of 4000 Khz double frequency of the I2C bus (400 KHz --> 800 KHz)
	reset_n					=>	I2C_reset_n			        ,
	clock					=>	usr_clk				                ,		
	SCL				   		=>	POWER_MAIN_SCL		                ,
	SDAi					=>	POWER_MAIN_SDA		                ,-- separate the IN and the OUT if we want to implement multiple 
	SDAo					=>	POWER_MAIN_SDA_cell	                , 
	SDAz					=>	POWER_MAIN_SDA_tri	                , 
	dti				   		=>	usr_data_wr(31 downto 0)			, 
	cs						=>	'1'		            , 
	func					=>	func_i2c_POWER_master		                , 
	wen				  		=>	usr_wen					            , 
	rd 				   		=>	usr_rden					        , 
	dto				   		=>	dto_i2c_POWER_monitor(31 downto 0)		 
	--ack_rq			=>				 --: out std_logic --- use for simulation
	);

func_i2c_POWER_master(0)	<= '1' when (usr_func_wr(Power_i2c_access_a) = '1' and usr_wen = '1' ) or (usr_func_rd(Power_i2c_access_a) = '1' and usr_rden = '1') else '0'; -- Address; function and Offset
func_i2c_POWER_master(1)	<= '1' when (usr_func_wr(Power_i2c_access_b) = '1' and usr_wen = '1' ) or (usr_func_rd(Power_i2c_access_b) = '1' and usr_rden = '1') else '0'; -- Data
 

POWER_MAIN_SDA <= POWER_MAIN_SDA_cell when POWER_MAIN_SDA_tri = '1' else 'Z';


--###########################################################################
-- I2C  SN  (1 elements) 
 
monitor_itf_SN:entity work.i2c_master_32b_autoread  
port map(
	low_clk			  		=>	usr_clk				            , -- 25 MHz
	ena_low_clk		  		=>	ena_clock_i2c			        , -- enable the clock to have a clock of 4000 Khz double frequency of the I2C bus (400 KHz --> 800 KHz)
	reset_n					=>	I2C_reset_n			        ,
	clock					=>	usr_clk				            ,		
	SCL				   		=>	SN_MAIN_SCL		                ,
	SDAi					=>	SN_MAIN_SDA		                ,-- separate the IN and the OUT if we want to implement multiple 
	SDAo					=>	SN_MAIN_SDA_cell	            , 
	SDAz					=>	SN_MAIN_SDA_tri	                , 
	dti				   		=>	usr_data_wr(31 downto 0)		,
	cs						=>	'1'	                            , 
	func					=>	func_i2c_SN_master		        , 
	wen				  		=>	usr_wen					        , 
	rd 				   		=>	usr_rden					    , 
	dto				   		=>	dto_i2c_SN_monitor(31 downto 0)	,	 
		-- direct output of I2C read
	Byte_i2c_out	        => MAC_byte                         , 
	Byte_i2c_latch	        => MAC_bytes_latch                  
	--ack_rq			=>				 --- use for simulation
	);

func_i2c_SN_master(0)	<= '1' when (usr_func_wr(SN_i2c_access_a) = '1' and usr_wen = '1' ) or (usr_func_rd(SN_i2c_access_a) = '1' and usr_rden = '1' ) else '0'; -- Address; function and Offset
func_i2c_SN_master(1)	<= '1' when (usr_func_wr(SN_i2c_access_b) = '1' and usr_wen = '1' ) or (usr_func_rd(SN_i2c_access_b) = '1' and usr_rden = '1' ) else '0'; -- Data
 

SN_MAIN_SDA <= SN_MAIN_SDA_cell when SN_MAIN_SDA_tri = '1' else 'Z';

-- memorize the MAC lower part read from EEPROM after reset
process(I2C_reset_n,usr_clk)
begin
    if I2C_reset_n = '0'  then
        record_MAC_bytes    <= "11";
    elsif rising_edge(usr_clk) then
        if MAC_bytes_latch = '1'  and record_MAC_bytes(0) = '1' then
            record_MAC_bytes(0)       <= '0';
            MAC_bytes_low(15 downto 8) <= MAC_byte;
        end if;
        if MAC_bytes_latch = '1'  and  record_MAC_bytes(0) = '0' and record_MAC_bytes(1) = '1' then
            record_MAC_bytes(1)       <= '0';
            MAC_bytes_low(7 downto 0) <= MAC_byte;
        end if;        
    end if;
end process;
--###########################################################################
-- I2C  cLOCK MANAGMENT  (1 elements) 
 
monitor_itf_i2c_Clocks:entity work.i2c_master_32b  
port map(
	low_clk			  		=>	usr_clk				            , -- 25 MHz
	ena_low_clk		  		=>	ena_clock_i2c			            , -- enable the clock to have a clock of 4000 Khz double frequency of the I2C bus (400 KHz --> 800 KHz)
	reset_n					=>	I2C_reset_n			        ,
	clock					=>	usr_clk				                ,		
	SCL				   		=>	ClockGen_MAIN_SCL		                ,
	SDAi					=>	ClockGen_MAIN_SDA		                ,-- separate the IN and the OUT if we want to implement multiple 
	SDAo					=>	ClockGen_MAIN_SDA_cell	                , 
	SDAz					=>	ClockGen_MAIN_SDA_tri	                , 
	dti				   		=>	usr_data_wr(31 downto 0)			, 
	cs						=>	'1'			            , 
	func					=>	func_i2c_ClockGen_master		                , 
	wen				  		=>	usr_wen					            , 
	rd 				   		=>	usr_rden					        , 
	dto				   		=>	dto_i2c_ClockGen_monitor(31 downto 0)		 
	--ack_rq			=>				 --: out std_logic --- use for simulation
	);


func_i2c_ClockGen_master(0)	<= '1' when (usr_func_wr(Clock_i2c_access_a) = '1' and usr_wen = '1' ) or (usr_func_rd(Clock_i2c_access_a) = '1' and usr_rden = '1') else '0'; -- Address; function and Offset
func_i2c_ClockGen_master(1)	<= '1' when (usr_func_wr(Clock_i2c_access_b) = '1' and usr_wen = '1') or (usr_func_rd(Clock_i2c_access_b) = '1' and usr_rden = '1') else '0'; -- Data

 		 
ClockGen_MAIN_SDA <= ClockGen_MAIN_SDA_cell when ClockGen_MAIN_SDA_tri = '1' else 'Z';
--#############################################################################

-- Sysmon
SYMON_i1:entity work.Sysmon 
	port map
	(
		usr_clk					=>	usr_clk			   	, 
		usr_rst_n				=>	rst_usr_clk_n	   	,
		usr_func_wr				=>	usr_func_wr		   	,
		usr_wen					=>	usr_wen			   	, 
		usr_data_wr				=>	usr_data_wr	   	,
		usr_func_rd				=>	usr_func_rd		   	,
		usr_data_rd				=>	dto_sysmon_module  	,
		i2c_sda 				=>	sysMon_i2c_sda		,
		i2c_sclk 				=>	sysMon_i2c_sclk		 
	 );

--#############################################################################
 --
 -- reprogram FLASH interface

Design_prog:entity work.reprogram_wrapper   
 Port map( 
	usr_clk						=> usr_clk               ,
	usr_rst_n					=> rst_usr_clk_n         ,
	usr_func_wr					=> usr_func_wr           ,
	usr_wen						=> usr_wen               ,
	usr_data_wr					=> usr_data_wr           ,
	 
	usr_func_rd					=> usr_func_rd           ,
	usr_rden					=> usr_rden              ,
	usr_data_rd					=> data_axi_prog         ,
	 
	axi_aclk					=> CLK_100MHz               ,
	  
	Flash_data					=> Flash_data            ,
	Flash_add(0)	            => dummy(0),
	Flash_add(26 downto 1)	    => Flash_add,
	Flash_add(31 downto 27)     => dummy(31 downto 27),
	Flash_oen					=> Flash_oen             ,
	Flash_wen					=> Flash_wen 
 ); 
 
--#############################################################################
 

end Behavioral;
