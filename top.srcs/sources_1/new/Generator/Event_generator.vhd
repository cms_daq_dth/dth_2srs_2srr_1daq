
LIBRARY ieee;

USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use work.address_table.all;
 
-- generate data in function INPUT request.
-- It will generate data and check the almost full of the stream buffer
-- 21/06/2021 change the read of fifo distribution = Generate_internal_trigger
 
--ENTITY-----------------------------------------------------------
entity Event_generator is
	generic (addr_offset :integer := 0);
	port
	(
		usr_clk								: in std_logic;
		usr_rst_n							: in std_logic;
		usr_func_wr							: in std_logic_vector(16383 downto 0); 
		usr_wen								: in std_logic;
		usr_data_wr							: in std_logic_vector(63 downto 0); 
			
		usr_func_rd					      	: in std_logic_vector(16383 downto 0); 
		usr_rden					      	: in std_logic;
		usr_data_rd					      	: out std_logic_vector(63 downto 0);
			
		dt_clock							: in std_logic;	-- max 190 Mhz for 25 Gb/s
		
		event_data_word						: out std_logic_vector(127 downto 0);
		event_ctrl							: out std_logic;		--  '1' when control word  ---- '0' when data word (Payload)
		event_data_wen						: out std_logic;		-- '1'  to validate a word (Header/ Trailer / Payload 
		backpressure						: in std_logic;			-- backpressured when '1'
		
		ext_trigger							: in std_logic;
		ext_veto_out						: out std_logic
	 );
end Event_generator;

--ARCHITECTURE------------------------------------------------------
architecture behavioral of Event_generator is


COMPONENT Evt_gen_descriptors  --FIFO First Word Fall Through
  PORT (
    clk         		: IN STD_LOGIC;
    srst        		: IN STD_LOGIC;
    din         		: IN STD_LOGIC_VECTOR(63 DOWNTO 0);
    wr_en       		: IN STD_LOGIC;
    rd_en       		: IN STD_LOGIC;
    dout        		: OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    full        		: OUT STD_LOGIC;
    empty       		: OUT STD_LOGIC;
    wr_rst_busy 		: OUT STD_LOGIC;
    rd_rst_busy 		: OUT STD_LOGIC
  );
END COMPONENT;
  
 
signal seed_val															: std_logic_vector(63 downto 0); 
signal load_seed														: std_logic; 
signal load_seed_resync													: std_logic; 
signal load_seed_resync_n												: std_logic; 
signal evt_size 													    : std_logic_vector(23 downto 0);
signal Event_size_mem													: std_logic_vector(23 downto 0);
signal evt_num	 													    : std_logic_vector(43 downto 0);
signal evt_num_cnt 													    : std_logic_vector(43 downto 0);
signal load_evt_num 												    : std_logic;
signal load_evt_num_resync											    : std_logic;
signal evt_BX	 													    : std_logic_vector(31 downto 0);
signal evt_Source 													    : std_logic_vector(31 downto 0);
signal gen_run 													        : std_logic;
signal gen_run_async											        : std_logic;
signal gen_run_resync											        : std_logic_vector(3 downto 0) := "0000";
signal time_wait 													    : std_logic_vector(31 downto 0);
signal select_desc_fifo													: std_logic;

signal desc_time_wait 													: std_logic_vector(31 downto 0);
signal desc_evt_size 													: std_logic_vector(23 downto 0);

signal counter_wc												        : std_logic_vector(23 downto 0);
signal counter_timer												    : std_logic_vector(31 downto 0);
signal Start_evt												        : std_logic;
signal Event_ongoing											        : std_logic;
signal end_evt														    : std_logic;
signal end_evt_delay												    : std_logic_vector(1 downto 0);
signal trigger_pulse												    : std_logic;

signal event_run													    : std_logic; 
signal event_run_cell												    : std_logic; 

signal single_trigger												    : std_logic;
signal single_trigger_resync										    : std_logic; 
signal Generate_internal_trigger										: std_logic; 
signal latch_ack_pointer_resync									        : std_logic;
signal event_data_random                                                : std_logic;

signal write_pointer												    : std_logic_vector(31 downto 0);
signal write_pointer_reg											    : std_logic_vector(31 downto 0);
signal resync_write_pointer												: std_logic;
signal read_pointer												        : std_logic_vector(31 downto 0);
signal read_pointer_resync										        : std_logic_vector(31 downto 0);

 
signal diff_pointer												        : std_logic_vector(27 downto 0);

 
signal data_o_rg			 										    : std_logic_vector(63 downto 0);
signal dt_rstn															: std_logic;
signal dt_rstp															: std_logic;
signal TCP_closed_state_resync											: std_logic;
  
  
signal data_gen_rnd														: std_logic_vector(127 downto 0);
signal data_gen_counter													: std_logic_vector(127 downto 0);
signal event_data_word_reg												: std_logic_vector(127 downto 0);
signal event_ctrl_reg													: std_logic;
signal event_data_wen_reg												: std_logic;
signal event_data_crc_ena												: std_logic;

signal event_data_crc_result														: std_logic_vector(15 downto 0);
signal reset_crc														: std_logic;

signal error_a															: std_logic;
signal error_b															: std_logic;
signal error_c															: std_logic;
signal error_d															: std_logic;
signal error_e															: std_logic;
signal error_f															: std_logic;
signal error_g															: std_logic;
signal error_h															: std_logic;
signal error_i															: std_logic;
signal error_j															: std_logic;

signal error_data_counter_resync										: std_logic;
signal error_data_counter												: std_logic_vector(1 downto 0);

signal evt_desc															: std_logic_vector(63 downto 0);
signal evt_desc_ld														: std_logic;
signal evt_desc_ld_resync												: std_logic;
signal wr_evt_desc_mux													: std_logic;
signal rd_evt_desc 														: std_logic;

signal evt_desc_mux														: std_logic_vector(63 downto 0);
signal evt_desc_out														: std_logic_vector(63 downto 0);

signal trigger_threshold_ready											: std_logic_vector(31 downto 0);
signal trigger_threshold_busy											: std_logic_vector(31 downto 0);
signal trigger_counter													: std_logic_vector(31 downto 0);
signal ext_trigger_resync												: std_logic;
signal select_ext_trigger												: std_logic;
signal ext_veto_out_reg													: std_logic;


signal Evt_Header														: std_logic_vector(127 downto 0);
signal Evt_Trailer														: std_logic_vector(127 downto 0);

signal crc_error_CMD_Sync												: std_logic;
signal gen_a_CRC_error													: std_logic;

attribute ASYNC_REG						: string;
attribute ASYNC_REG of gen_run_async : signal is  "TRUE";

attribute mark_debug : string;

--attribute mark_debug of event_data_word_reg			: signal is "true";	
--attribute mark_debug of event_data_wen_reg			: signal is "true";
--attribute mark_debug of event_ctrl_reg  			: signal is "true";
--attribute mark_debug of backpressure      			: signal is "true";
 	
--***************************************************************** 
--      Code start HERE
--***************************************************************** 
 begin 
   
 --- control process to set parameters
 process(usr_rst_n,usr_clk)
 begin
	if usr_rst_n = '0' then
		gen_run							<= '0';
		evt_size						<= (others => '0');
		single_trigger					<= '0';
		time_wait						<= (others => '0');
		load_seed						<= '0';
		load_evt_num					<= '0';
		error_a							<= '0';
		error_b							<= '0';
		error_c							<= '0';
		error_d							<= '0';
		error_e							<= '0';
		error_f							<= '0';
		error_g							<= '0';
		error_h							<= '0';
		error_i							<= '0';
		error_j							<= '0';
		evt_desc						<= (others => '0');
		evt_desc_ld						<= '0';
		select_desc_fifo				<= '0';
		select_ext_trigger				<= '0';
		event_data_random               <= '0';
	elsif rising_edge(usr_clk) then  
		single_trigger					<= '0';
		load_seed						<= '0';
		load_evt_num					<= '0';
		error_a							<= '0';
		error_b							<= '0';
		error_c							<= '0';
		error_d							<= '0';
		error_e							<= '0';
		error_f							<= '0';
		error_g							<= '0';
		error_h							<= '0';
		error_i							<= '0';
		error_j							<= '0';
		evt_desc_ld						<= '0';
		
		if usr_wen = '1' then
			-- start the generator
					
			if usr_func_wr(Event_generator_control + addr_offset) =  '1'   then
				gen_run 						<= usr_data_wr(0);		-- generate data in loop
				single_trigger					<= usr_data_wr(1);		-- generate 1 trigger
				
			    event_data_random               <= usr_data_wr(4);      -- if 1 data random if 0 counter + event#
			    
				error_a							<= usr_data_wr(16);	-- CRC error gen	
				error_b							<= usr_data_wr(17);	-- to be defined
				error_c							<= usr_data_wr(18);
				error_d							<= usr_data_wr(19);
				error_e							<= usr_data_wr(20);
				error_f							<= usr_data_wr(21);
				error_g							<= usr_data_wr(22);
				error_h							<= usr_data_wr(23);
				error_i							<= usr_data_wr(24);
				error_j							<= usr_data_wr(25);
				
				select_ext_trigger				<= usr_data_wr(30); 
				select_desc_fifo				<= usr_data_wr(31); 
			end if;
			
			if usr_func_wr(Event_generator_evt_num 				+ addr_offset) =  '1'    then 
				load_evt_num					<= '1';
			end if;
			
			--set the event fragment size (in bytes  aligned on 0x10 (16 bytes: event word size)
			if usr_func_wr(Event_generator_length 				+ addr_offset) =  '1'    then
				evt_size 						<= usr_data_wr(23 downto 0);			
			end if;
			
 			if usr_func_wr(Event_generator_seed 				+ addr_offset) =  '1'    then 
				load_seed						<= '1';
			end if;	
			
			-- set the time between two fragment
			if usr_func_wr( Event_Generator_Time_Btw_Frags 		+ addr_offset) =  '1'   then
				time_wait						<= usr_data_wr(31 downto 0);			
			end if;			
			
			-- load event descriptors
			if usr_func_wr(Event_generator_load_evt_descriptors + addr_offset) =  '1'   then
               --(63 downto 32) desc_time_wait 		 
               --(23 downto 00) desc_evt_size  		 
               -- 1024 values max
				evt_desc						<= usr_data_wr;
				evt_desc_ld						<= '1';
			end if;			
 
		end if;
	end if;
 end process;
 
  process(usr_rst_n,usr_clk)
 begin
	if rising_edge(usr_clk) then  
 
		if usr_wen = '1' then
 
			--set the event fragment size (in bytes  aligned on 0x10 (16 bytes: event word size)
			if usr_func_wr(Event_generator_evt_num 				+ addr_offset) =  '1'    then
				evt_num 						<= usr_data_wr(43 downto 0); 
			end if;
			
			--set the event BX and source#
			if usr_func_wr(Event_generator_BX_SOURCE 			+ addr_offset) =  '1'    then
				evt_BX							<= usr_data_wr(31 downto 0);			
				evt_Source						<= usr_data_wr(63 downto 32);			
			end if;			
						
			--set the seed
			if usr_func_wr(Event_generator_seed 				+ addr_offset) =  '1'    then
				seed_val						<= usr_data_wr;
			end if;			
	 
			-- load event descriptors
			if usr_func_wr(Event_generator_threshold_ready 		+ addr_offset) =  '1'   then
				trigger_threshold_ready			<= usr_data_wr(31 downto 0);
			end if;			
			
			-- load event descriptors
			if usr_func_wr(Event_generator_threshold_busy 		+ addr_offset) =  '1'   then
				trigger_threshold_busy			<= usr_data_wr(31 downto 0);
			end if;			

		end if;
	end if;
 end process;
 -- atstic value read back by conrtoller (PCIe)
 process(usr_clk)
 begin
	if rising_edge(usr_clk) then  
		data_o_rg								<= (others => '0');
		
		if 			usr_func_rd(Event_generator_control + addr_offset) =  '1' then
			data_o_rg(0)					<= gen_run ;	
			data_o_rg(1)					<= single_trigger;	
			data_o_rg(2)					<= event_run_cell;	
			data_o_rg(3)					<= backpressure;	
			data_o_rg(4)					<= event_data_random;
				
			data_o_rg(30)					<= select_ext_trigger;	
			data_o_rg(31)					<= select_desc_fifo;	
			
		elsif		usr_func_rd(Event_generator_length 				+ addr_offset) =  '1' then
			data_o_rg(23 downto 0)			<= evt_size;
		elsif 		usr_func_rd(Event_Generator_Time_Btw_Frags 		+ addr_offset) = '1'  then	
			data_o_rg(31 downto 0)			<= time_wait;
		elsif 		usr_func_rd(Event_generator_evt_num 			+ addr_offset) = '1'  then	
			data_o_rg(43 downto 0)			<= evt_num;
		elsif 		usr_func_rd(Event_generator_seed 				+ addr_offset) = '1'  then	
			data_o_rg 						<= seed_val;
		elsif 		usr_func_rd(Event_generator_BX_SOURCE 			+ addr_offset) = '1'  then	
			data_o_rg(31 downto 0)			<= evt_BX;
			data_o_rg(63 downto 32)			<= evt_Source;
		elsif 		usr_func_rd(Event_generator_threshold_ready 	+ addr_offset) = '1'  then	
			data_o_rg(31 downto 0)			<= trigger_threshold_ready; 
		elsif 		usr_func_rd(Event_generator_threshold_busy 		+ addr_offset) = '1'  then	
			data_o_rg(31 downto 0)			<= trigger_threshold_busy; 
		elsif 		usr_func_rd(Event_generator_trigger_counter 		+ addr_offset) = '1'  then	
			data_o_rg(31 downto 0)			<= trigger_counter; 
		elsif 		usr_func_rd(Event_generator_timer_counter 		+ addr_offset) = '1'  then
			data_o_rg(31 downto 0)			<=  counter_timer; 
		elsif 		usr_func_rd(event_test 							+ addr_offset) = '1'  then	
			data_o_rg(31 downto 0)			<= x"12345678"; 
		end if;

	end if;
 end process;
  
usr_data_rd			<= data_o_rg;
 
--***************************************************************
-- resync signals

reset_resync_i1:entity work.resetn_resync 
port map(
	aresetn				=> usr_rst_n,
	clock				=> dt_clock	,
	Resetn_sync			=> dt_rstn	 
	);

dt_rstp		<= not(dt_rstn);
	
resync_pulse_trg:entity work.resync_pulse  
		port map(
			aresetn		=> usr_rst_n, 
			clocki		=> usr_clk,	
			in_s		=> single_trigger,
			clocko		=> dt_clock,
			out_s		=> single_trigger_resync 
			);	
				
resync_pulse_ld:entity work.resync_pulse  
		port map(
			aresetn		=> usr_rst_n,
			clocki		=> usr_clk,	
			in_s		=> load_evt_num,
			clocko		=> dt_clock,
			out_s		=> load_evt_num_resync 
			);	
							
resync_pulse_ldseed:entity work.resync_pulse  
		port map(
			aresetn		=> usr_rst_n,
			clocki		=> usr_clk,	
			in_s		=> load_seed,
			clocko		=> dt_clock,
			out_s		=> load_seed_resync 
			);	
			
resync_pulse_lddesc:entity work.resync_pulse  
		port map(
			aresetn		=> usr_rst_n,
			clocki		=> usr_clk,	
			in_s		=> evt_desc_ld,
			clocko		=> dt_clock,
			out_s		=> evt_desc_ld_resync 
			);	
			
resync_pulse_CRC_error:entity work.resync_pulse  
		port map(
			aresetn		=> usr_rst_n,
			clocki		=> usr_clk,	
			in_s		=> error_a,
			clocko		=> dt_clock,
			out_s		=> crc_error_CMD_Sync 
			);	
			
			
			
resync_pulse_data_error:entity work.resync_pulse  
		port map(
			aresetn		=> usr_rst_n,
			clocki		=> usr_clk,	
			in_s		=> error_b,
			clocko		=> dt_clock,
			out_s		=> error_data_counter_resync 
			);	
			
			
			
process(dt_clock)
begin
	if rising_edge(dt_clock) then
		gen_run_resync(3 downto 1) 	<= gen_run_resync(2 downto 0);
		gen_run_resync(0)			<= gen_run_async;
		
		gen_run_async	 <= '0';
		if gen_run = '1' then
			gen_run_async <= '1';
		end if;
	end if;
end process;			

--*****************************************************************
-- Generator of errors

-- crc error on one event
process(dt_rstn,dt_clock)
begin
	if dt_rstn = '0' then
		gen_a_CRC_error	<= '0';
	elsif rising_edge(dt_clock) then
		if crc_error_CMD_Sync = '1' then
			gen_a_CRC_error <= '1';
		elsif  end_evt_delay(1)	 = '1' then
			gen_a_CRC_error <= '0';
		end if;
	end if;
end process;
	 
--*****************************************************************
-- Generator of errors

-- error_data_counter_resync
process(dt_rstn,dt_clock)
begin
	if dt_rstn = '0' then
		error_data_counter	<= "00";
	elsif rising_edge(dt_clock) then
		if error_data_counter_resync = '1' then
			error_data_counter(0) <= '1';
		elsif  end_evt_delay(1)	 = '1' then
			error_data_counter(0) <= '0';
		end if;
		
		if error_data_counter(0) = '1' and data_gen_counter(15 downto 0) = x"0007"  then
			error_data_counter(1) <= '1';
		elsif  end_evt_delay(1)	 = '1' then
			error_data_counter(1) <= '0';
		end if;		
	end if;
end process;
	 
--*****************************************************************
-- Event descriptor (32 bit event size, 32bit time between events)

-- write to FIFO when load via PCIe or when we read a descriptor we write it back.
wr_evt_desc_mux		<= '1' 		when evt_desc_ld_resync = '1' or rd_evt_desc = '1' 	else '0';
evt_desc_mux		<= evt_desc when evt_desc_ld_resync = '1' 						else evt_desc_out;

Evt_gen_descriptors_i1:Evt_gen_descriptors
  PORT MAP(
    clk         		=> dt_clock				, 
    srst        		=> dt_rstp				, 
    din         		=> evt_desc_mux			, 
    wr_en       		=> wr_evt_desc_mux		, 
    rd_en       		=> rd_evt_desc			, 
    dout        		=> evt_desc_out			  
    -- full        		=>						, 
    -- empty       		=>						, 
    -- wr_rst_busy 		=>						, 
    -- rd_rst_busy 		=>						  
  );
  
rd_evt_desc  		<= '1' when Generate_internal_trigger = '1' and select_desc_fifo = '1' else '0';
desc_time_wait 		<= evt_desc_out(63 downto 32);
desc_evt_size  		<= evt_desc_out(23 downto 00);


 --********************************************************************
 --time wait between two fragments
  process(dt_rstn, dt_clock)
 begin
	if dt_rstn = '0' then
		counter_timer		<= (others => '0');
	elsif rising_edge(dt_clock) then
		-- at the end of a fragment (load the counter_timer
		if end_evt = '1' and (select_desc_fifo  = '1' or gen_run_resync(3) = '1') then
			if select_desc_fifo = '0'  then
				counter_timer	<= time_wait;
			else
				counter_timer	<= desc_time_wait;
			end if;
		-- timer_counter count down when not NULL
		elsif	counter_timer >= x"00000001" then
			counter_timer	<= counter_timer - '1';
		end if;
		
		trigger_pulse 		<= '0';
		-- in  loop mode generate a 1st trigger with the controller command + automatic trigger after counter_timerout
		if	(counter_timer = x"00000001" and gen_run_resync(3) = '1') or (gen_run_resync(2) = '1' and gen_run_resync(3) = '0') then
			trigger_pulse 	<= '1';
		end if;
	end if;
 end process;

 
 --*****************************************************************
 -- generator of fragment
 
 resync_pulse_ext_trg:entity work.resync_pulse  
 		port map(
 			-- reset		=> usr_rst_n,
 			aresetn		=> select_ext_trigger,
			clocki		=> ext_trigger,	
 			in_s		=> '1',
 			clocko		=> dt_clock,
 			out_s		=> ext_trigger_resync 
 			);	
 
 process(dt_rstn, dt_clock)
 begin
	if dt_rstn = '0' then
		counter_wc			<= (others => '0');
		event_run_cell		<= '0';
		event_run			<= '0';
		end_evt 			<= '0';
		Start_evt			<= '0';
		Event_ongoing		<= '0';
		reset_crc			<= '0';
		evt_num_cnt			<= (others => '0');
		trigger_counter		<= (others => '0');
	elsif rising_edge(dt_clock) then
		end_evt 				<= '0';
		-- specify the end of event
		if	counter_wc <= x"000010" and backpressure = '0' and event_run_cell = '1' then
			end_evt 			<= '1';
			event_run_cell		<= '0';
		elsif  Generate_internal_trigger = '1' then
			event_run_cell		<= '1';						-- start the gragment generator
		end if;		
	
	---
		event_run				<= '0';
		reset_crc				<= '0'; -- reset the CRC computing
		if counter_wc > x"000010" and backpressure = '0' and event_run_cell = '1' then
			-- event ongoing state
			counter_wc			<= counter_wc - x"10";		-- event word is 128 bit	
			event_run			<= '1';			
			
			-- start the event
		elsif Generate_internal_trigger = '1' then
			reset_crc			<= '1';
			if select_desc_fifo = '0' then
				counter_wc		<= evt_size;			-- load the size 
				Event_size_mem	<= evt_size;			-- load the size 
			else
				counter_wc		<= desc_evt_size;		-- load the size
				Event_size_mem	<= desc_evt_size;		-- load the size
			end if;
		end if;
	---	
		-- increment the event number for each trigger
		if load_evt_num_resync = '1' then
			evt_num_cnt		<= evt_num;
		elsif end_evt = '1'  then
			evt_num_cnt		<= evt_num_cnt + '1';
		end if;
		
		--define the Event Header
		if 		Event_run = '1' then
			Start_evt		<= '1';
		elsif 	end_evt = '1' then
			Start_evt		<= '0';
		end if; 
		
		-- define a signal which stays ON during the Event "Envelop" 
		if Generate_internal_trigger = '1' then
			Event_ongoing	<= '1';
		elsif  end_evt = '1'  then
			Event_ongoing	<= '0';
		end if;
		
		-- Count and uncount the trigger  (trigger   and End of envent)
		if 		( trigger_pulse = '1' or single_trigger_resync = '1' or ext_trigger_resync = '1') and end_evt = '0'    then
			trigger_counter	<= trigger_counter + '1';
		elsif	( trigger_pulse = '0' and single_trigger_resync = '0' and ext_trigger_resync = '0') and end_evt = '1' then
			trigger_counter	<= trigger_counter - '1';
		end if;
		
	end if;
 end process;
 
 -- backpressure on external trigger
 process(dt_rstn,dt_clock)
 begin
	if dt_rstn = '0' then
		ext_veto_out_reg	<= '0';
	elsif rising_edge(dt_clock) then
		if trigger_counter > trigger_threshold_busy then
			ext_veto_out_reg	<= '1';
		elsif trigger_counter <= trigger_threshold_ready then
			ext_veto_out_reg	<= '0';
		end if;
	end if;
end process;

ext_veto_out 	<= ext_veto_out_reg;

 process(dt_clock)
 begin
 	if rising_edge(dt_clock) then 
 		Generate_internal_trigger		<= '0';
		if  trigger_counter /= x"00000000"  and Event_ongoing = '0' and Generate_internal_trigger = '0' then
			Generate_internal_trigger	<= '1';
		end if;
 	end if; 
 end process;
 
 
 --******************************************************************
 -- fragment format 

load_seed_resync_n	<= not(load_seed_resync);

gen_rnd_i1:entity work.gen_dt_rnd 
	port map(
	 	clk					=> dt_clock,
		reset				=> load_seed_resync_n,
		seed(63 downto 0)	=> seed_val,
		seed(127 downto 64)	=> seed_val, 
		ena_dt				=> event_run,
		lfsr_out			=> data_gen_rnd 
  	);
  	
  	
--not a random data
process(dt_clock)
begin
    if rising_edge(dt_clock) then
        if event_run = '1' and Start_evt = '0'  then
            data_gen_counter    <= (others => '0');
         elsif event_run = '1' then
            data_gen_counter    <= data_gen_counter + '1';
         end if;
    end if;
end process;  
  	
--*****************************************************************
--   HEADER and TRAILER

Evt_Header(127 downto 120) 		<= x"55";
Evt_Header(119 downto 116)      <= x"2";
Evt_Header(115 downto 108) 		<= x"00";
Evt_Header(107 downto 064) 		<= evt_num_cnt;
Evt_Header(063 downto 058) 		<= "000000";
Evt_Header(057) 		        <= '1';          -- Emulator Fragment
Evt_Header(056) 		        <= '0';          -- Emulator Fragment
Evt_Header(055 downto 048) 		<= x"00";        -- Physics type
Evt_Header(047 downto 032) 		<= x"0000";      -- L1A type & content
Evt_Header(031 downto 000) 		<= evt_Source;
 			 
Evt_Trailer(127 downto 120) 	<= x"AA";
Evt_Trailer(119 downto 096) 	<= x"000000";
Evt_Trailer(095 downto 076) 	<= Event_size_mem(23 downto 4);
Evt_Trailer(075 downto 064) 	<= x"000";
Evt_Trailer(063 downto 032) 	<= x"00000000";
Evt_Trailer(031 downto 016) 	<= event_data_crc_result when gen_a_CRC_error = '0' else not(event_data_crc_result);
Evt_Trailer(015 downto 000) 	<= x"0000";
   	
--*****************************************************************  	
 
 process(dt_rstn, dt_clock) 
 begin
	if dt_rstn = '0'then
		event_ctrl_reg		<= '0';
		event_data_word_reg <= (others => '0');
		event_data_wen_reg	<= '0';
		event_data_crc_ena	<= '0';		
	elsif rising_edge(dt_clock) then
		event_ctrl_reg		<= '0';
		event_data_wen_reg	<= '0';
		event_data_crc_ena	<= '0';
		if event_run = '1' and Start_evt = '0' then
			event_ctrl_reg 							<= '1';
			event_data_word_reg 				 	<= Evt_Header;
			event_data_wen_reg						<= '1';
			event_data_crc_ena						<= '1';

		elsif end_evt	 = '1' then
			event_data_word_reg(127 downto 032) 	<= Evt_Trailer(127 downto 032);
			event_data_word_reg(031 downto 016) 	<= x"0000";--event_data_crc_result;
			event_data_word_reg(015 downto 000) 	<= Evt_Trailer(015 downto 000);
			event_data_crc_ena						<= '1'; 
		
		elsif end_evt_delay(1)	 = '1' then
			event_ctrl_reg 							<= '1';
			event_data_word_reg					 	<= Evt_Trailer;
			event_data_wen_reg						<= '1'; 
			
		elsif event_run = '1' then
		    if event_data_random = '1' then
			     event_data_word_reg(127 downto 000) 	<= data_gen_rnd;
			else
			     event_data_word_reg(127 downto 096) 	<= x"C1C1C1C1";
				 -- if error_data_counter(1) = '1' then
					-- event_data_word_reg(095 downto 064) 	<= evt_num_cnt - '1';
				 -- else
					event_data_word_reg(095 downto 064) 	<= evt_num_cnt(31 downto 0);
				 -- end if;
				 event_data_word_reg(063 downto 048) 	<= evt_Source(15 downto 0);
			     event_data_word_reg(047 downto 000) 	<= data_gen_counter(47 downto 0);
			end if;
			event_data_wen_reg						<= '1';
			event_data_crc_ena						<= '1';
		end if;		
	end if;
 end process;
 
 
 process(dt_clock)
 begin
	if rising_edge(dt_clock) then
		end_evt_delay(1) 			<= end_evt_delay(0);
		end_evt_delay(0)			<= end_evt;
	end if;
 end process;
 
 
 -- CRC compute
CRC_i1:entity work.crcUSB_128b_16b  
port map(
	clear_p		=> reset_crc,
	clk		    => dt_clock,
	Data		=> event_data_word_reg,
	enable		=> event_data_crc_ena,
	crc_out		=> event_data_crc_result
	);
 

 
 --**********************************************************************
 --output
 
 event_data_word	<= event_data_word_reg;	
 event_ctrl		    <= event_ctrl_reg;		
 event_data_wen	    <= event_data_wen_reg;	
 
 
 
end behavioral;