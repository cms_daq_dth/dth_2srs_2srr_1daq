----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 25.09.2019 15:29:57
-- Design Name: 
-- Module Name: SR_receiver - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use work.address_table.all;  

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
 
library UNISIM;
use UNISIM.VComponents.all;

entity SR_receiver is
generic(SR_offset                               : integer := 0;
		Clock_source							: string := "Master";
        throughput								: string := "15.66";
				--possible choices are 15.66 or 25.78125
		ref_clock								: string := "156.25";
				--possible choices are 156.25  or  322.265625 
		technology								: string := "GTY";
				-- possible choices are GTY or GTH
        UltraRam_mem                            : boolean := TRUE  
				);
 Port (
    usr_clk           			: IN STD_LOGIC;
    rst_usr_clk_n     			: IN STD_LOGIC;
    usr_func_wr       			: IN STD_LOGIC_VECTOR(16383 DOWNTO 0);
    usr_wen           			: IN STD_LOGIC;
    usr_data_wr       			: IN STD_LOGIC_VECTOR(63 DOWNTO 0);
    usr_func_rd       			: IN STD_LOGIC_VECTOR(16383 DOWNTO 0);
    usr_rden          			: IN STD_LOGIC;
    usr_dto_receiver  			: OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    resetp_counters     		: in std_logic;
			
	clk_freerun_in				: in std_logic;
			
	clock_SR					: OUT STD_LOGIC;	
	SR_WEN  					: OUT STD_LOGIC;	
	SR_UCTRL					: OUT STD_LOGIC;
	SR_DATA 					: OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
	SR_LFF  					: IN STD_LOGIC;

    gtS_Reset_TX_clock_out		: out std_logic;                				 
    gtS_userclk_tx_active_in	: in std_logic := '0';                  				 
    gtS_userclk_tx_usrclk_in	: in std_logic := '0';                  				 
    gtS_userclk_tx_usrclk2_in	: in std_logic := '0';                  				 
    gtS_userclk_tx_usrclk4_in	: in std_logic := '0';                				  
    gtM_Reset_TX_clock_in		: in std_logic_vector(2 downto 0) := "000";--active HIGH  
    gtM_userclk_tx_active_out	: out std_logic := '0';                          
    gtM_userclk_tx_usrclk_out	: out std_logic := '0';                          
    gtM_userclk_tx_usrclk2_out	: out std_logic := '0';                          
    gtM_userclk_tx_usrclk4_out	: out std_logic := '0';    
	 
    qpll_lock_in                : IN STD_LOGIC;
    qpll_reset_out              : OUT STD_LOGIC;
    qpll_clkin                  : IN STD_LOGIC;
    qpll_ref_clkin              : IN STD_LOGIC; 
	SRI_gt_rxn_in     			: IN STD_LOGIC;
    SRI_gt_rxp_in     			: IN STD_LOGIC;
    SRI_gt_txn_out    			: OUT STD_LOGIC;
    SRI_gt_txp_out    			: OUT STD_LOGIC
);	
end SR_receiver;

architecture Behavioral of SR_receiver is
 

signal core_usr_data_out					: std_logic_vector(63 DOWNTO 0);
signal ctrl_usr_data_reg					: std_logic_vector(63 DOWNTO 0);
 
signal clock_SR_out     					: STD_LOGIC;
signal clock_SR_resetp   					: STD_LOGIC;

signal ena_cnt_pckt     					: STD_LOGIC;
signal ena_cnt_pckt_counter					: STD_LOGIC_vector(63 downto 0);
signal ena_cnt_bad_p    					: STD_LOGIC;
signal ena_cnt_bad_p_counter				: STD_LOGIC_vector(63 downto 0);
signal ena_back_pres    					: STD_LOGIC;
signal ena_back_pres_counter				: STD_LOGIC_vector(63 downto 0);
signal ena_FED_crc_err  					: STD_LOGIC;
signal ena_FED_crc_err_counter				: STD_LOGIC_vector(63 downto 0);
signal ena_SLINK_crc_err					: STD_LOGIC;
signal ena_SLINK_crc_err_counter			: STD_LOGIC_vector(63 downto 0);
signal DAQ_to_SR_backpressure_counter   	: STD_LOGIC_vector(63 downto 0);
 
signal SERDES_ready                         : std_logic;
signal Resetp_until_SERDES_ready             : std_logic; 

--****************************************************************************
--**********    CODE   START     HERE    *************************************
--****************************************************************************
begin

Resetp_until_SERDES_ready <= '1' when resetp_counters = '1' or SERDES_ready = '0' else '0'; 

reset_resync_i0:entity work.resetp_resync
port map(
	aresetp			=> Resetp_until_SERDES_ready,
	clock			=> clock_SR_out,
	Resetp_sync		=> clock_SR_resetp
	);
	 
SR_receiver:entity work.SR_Receive_GLB
 generic map (  Add_Offset_SLINKR   => SR_offset,
				Clock_source		=> Clock_source,
                throughput			=> throughput,
                technology			=> technology,
			    UltraRam_mem        => UltraRam_mem 
				)  
  PORT map(
    usr_clk                     	=> usr_clk           ,
    rst_usr_clk_n               	=> rst_usr_clk_n     ,
    usr_func_wr                 	=> usr_func_wr       ,
    usr_wen                     	=> usr_wen           ,
    usr_data_wr                 	=> usr_data_wr       ,
    
    usr_func_rd                 	=> usr_func_rd       ,
    usr_rden                    	=> usr_rden          ,
    usr_dto_receiver            	=> core_usr_data_out ,
    
    clk_freerun_in             	=> clk_freerun_in   , 
    
    clk_out                     	=> clock_SR_out,
    wen_data                    	=> SR_WEN       ,
    UCTRL_out                   	=> SR_UCTRL     ,
    data_out                    	=> SR_DATA      ,
    lff                         	=> SR_LFF       ,
    
    ena_cnt_pckt                	=> ena_cnt_pckt      ,
    ena_cnt_bad_p               	=> ena_cnt_bad_p     ,
    ena_back_pres               	=> ena_back_pres     ,
    ena_FED_crc_err             	=> ena_FED_crc_err   ,
    ena_SLINK_crc_err           	=> ena_SLINK_crc_err ,
    
    qpll_lock_in                	=> qpll_lock_in      ,
    qpll_reset_out              	=> qpll_reset_out    ,
    qpll_clkin                  	=> qpll_clkin        ,
    qpll_ref_clkin              	=> qpll_ref_clkin    ,
    
    gtS_Reset_TX_clock_out			=> gtS_Reset_TX_clock_out	,
    gtS_userclk_tx_active_in		=> gtS_userclk_tx_active_in	,		 
    gtS_userclk_tx_usrclk_in		=> gtS_userclk_tx_usrclk_in	,		 
    gtS_userclk_tx_usrclk2_in		=> gtS_userclk_tx_usrclk2_in,		 
    gtS_userclk_tx_usrclk4_in		=> gtS_userclk_tx_usrclk4_in,	

	gtM_Reset_TX_clock_in_0			=> gtM_Reset_TX_clock_in(0),
	gtM_Reset_TX_clock_in_1			=> gtM_Reset_TX_clock_in(1),
	gtM_Reset_TX_clock_in_2			=> gtM_Reset_TX_clock_in(2),
    gtM_userclk_tx_active_out		=> gtM_userclk_tx_active_out,
    gtM_userclk_tx_usrclk_out		=> gtM_userclk_tx_usrclk_out,
    gtM_userclk_tx_usrclk2_out		=> gtM_userclk_tx_usrclk2_out,
    gtM_userclk_tx_usrclk4_out		=> gtM_userclk_tx_usrclk4_out,
    
    Rcv_gt_rxn_in               	=> SRI_gt_rxn_in   ,
    Rcv_gt_rxp_in               	=> SRI_gt_rxp_in   ,
    Rcv_gt_txn_out              	=> SRI_gt_txn_out  ,
    Rcv_gt_txp_out              	=> SRI_gt_txp_out  ,
    SERDES_ready                    => SERDES_ready
 
    
  );
  
clock_SR	<= clock_SR_out;

--****************************************************************************
-- Status register for the SLINKROCKET 
process(clock_SR_resetp,clock_SR_out)
begin
	if clock_SR_resetp = '1' then
		ena_cnt_pckt_counter			<= (others => '0');
		ena_cnt_bad_p_counter			<= (others => '0');
		ena_back_pres_counter			<= (others => '0');
		ena_FED_crc_err_counter			<= (others => '0');
		ena_SLINK_crc_err_counter		<= (others => '0');
	
	elsif rising_edge(clock_SR_out) then
			if ena_cnt_pckt = '1' then
				ena_cnt_pckt_counter 		<= ena_cnt_pckt_counter + '1';
			end if;
			
			if ena_cnt_bad_p = '1' then
				ena_cnt_bad_p_counter 		<= ena_cnt_bad_p_counter + '1';
			end if; 
			
			if ena_back_pres  = '1' then
				ena_back_pres_counter 		<= ena_back_pres_counter + '1';
			end if; 
			
			if ena_FED_crc_err = '1' then
				ena_FED_crc_err_counter 	<= ena_FED_crc_err_counter + '1';
			end if; 
			
			if ena_SLINK_crc_err = '1' then
				ena_SLINK_crc_err_counter 	<= ena_SLINK_crc_err_counter + '1';
			end if;
			 		
			if SR_LFF = '1' then
				DAQ_to_SR_backpressure_counter 	<= DAQ_to_SR_backpressure_counter + '1';
			end if; 
	end if;
end process;
  
--****************************************************************************
-- Mux for monitoring
process(usr_clk)
begin
	if rising_edge(usr_clk) then
		ctrl_usr_data_reg			<= (others => '0');
		if 		usr_func_rd(SR_Rec_pckt_counter + SR_offset) = '1' then
			ctrl_usr_data_reg 	<= ena_cnt_pckt_counter;
		elsif 	usr_func_rd(SR_Rec_Bad_pckt_counter + SR_offset) = '1' then
			ctrl_usr_data_reg 	<= ena_cnt_bad_p_counter;
		elsif 	usr_func_rd(SR_Rec_BP_counter + SR_offset) = '1' then
			ctrl_usr_data_reg 	<= ena_back_pres_counter;
		elsif 	usr_func_rd(SR_Rec_FED_CRC_error_counter + SR_offset) = '1' then
			ctrl_usr_data_reg 	<= ena_FED_crc_err_counter;
		elsif 	usr_func_rd(SR_Rec_SR_CRC_error_counter + SR_offset) = '1' then
			ctrl_usr_data_reg 	<= ena_SLINK_crc_err_counter;
		elsif 	usr_func_rd(SR_DAQ_to_SR_Bp_counter + SR_offset) = '1' then
			ctrl_usr_data_reg 	<= DAQ_to_SR_backpressure_counter;
		end if; 
	end if;
end process;

usr_dto_receiver	<= ctrl_usr_data_reg or core_usr_data_out;  
  
end Behavioral;
