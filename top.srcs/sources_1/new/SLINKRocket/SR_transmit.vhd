----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 26.09.2019 14:25:39
-- Design Name: 
-- Module Name: SR_transmit - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use work.address_table.all; 
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity SR_transmit is
generic(SR_offset                               : integer := 0;
        Clock_source							: string := "Master";
				--possible choices are Slave or Master
        throughput								: string := "15.66";
				--possible choices are 15.66 or 25.78125
		ref_clock								: string := "156.25";
				--possible choices are 156.25  or  322.265625 
		technology								: string := "GTY" 
				-- possible choices are GTY or GTH 
				);
 Port ( 
	usr_clk           				: IN STD_LOGIC;
    rst_usr_clk_n     				: IN STD_LOGIC;
    usr_func_wr       				: IN STD_LOGIC_VECTOR(16383 DOWNTO 0);
    usr_wen           				: IN STD_LOGIC;
    usr_data_wr       				: IN STD_LOGIC_VECTOR(63 DOWNTO 0);
    usr_func_rd       				: IN STD_LOGIC_VECTOR(16383 DOWNTO 0);
    usr_rden          				: IN STD_LOGIC;
    usr_dto_receiver  				: OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
				
	clk_freerun_in					: in std_logic;
	FED_clock						: in std_logic;
	
	gtS_Reset_TX_clock_out			: out std_logic;         
	gtS_userclk_tx_active_in		: in std_logic	:= '0';                				
	gtS_userclk_tx_usrclk_in		: in std_logic	:= '0';                  				
	gtS_userclk_tx_usrclk2_in		: in std_logic	:= '0';                  				
	gtS_userclk_tx_usrclk4_in		: in std_logic	:= '0';                  				
	gtM_Reset_TX_clock_in			: in std_logic_vector(2 downto 0) := "000";--active HIGH 
	gtM_userclk_tx_active_out		: out std_logic := '0';                        
	gtM_userclk_tx_usrclk_out		: out std_logic := '0';                         
	gtM_userclk_tx_usrclk2_out		: out std_logic := '0';                         
	gtM_userclk_tx_usrclk4_out		: out std_logic := '0';   
	
    qpll_lock_in         			: IN STD_LOGIC;
    qpll_reset_out       			: OUT STD_LOGIC;
    qpll_clkin           			: IN STD_LOGIC;
    qpll_ref_clkin       			: IN STD_LOGIC;   
	reset_GTTXRESET_in              : in STD_LOGIC;
	reset_GTTXRESET_out             : OUT STD_LOGIC;
	
	SRO_gt_rxn_in     				: IN STD_LOGIC;
    SRO_gt_rxp_in     				: IN STD_LOGIC;
    SRO_gt_txn_out    				: OUT STD_LOGIC;
    SRO_gt_txp_out    				: OUT STD_LOGIC;
				
    Rst_hrd_sim         			: in std_logic	
	
 );
end SR_transmit;

architecture Behavioral of SR_transmit is

COMPONENT resetn_resync is
port (
	aresetn				: in std_logic;
	clock				: in std_logic; 

	Resetn_sync		: out std_logic;
	Resetp_sync		: out std_logic
	);
end COMPONENT;
  
signal txdiffctrl_in        		: STD_LOGIC_VECTOR(4 DOWNTO 0);
signal txpostcursor_in      		: STD_LOGIC_VECTOR(4 DOWNTO 0);
signal txprecursor_in       		: STD_LOGIC_VECTOR(4 DOWNTO 0);
signal Srds_loopback_in       		: STD_LOGIC; 
	
signal core_usr_data_out			: std_logic_vector(63 downto 0);
signal ctrl_usr_data_reg			: std_logic_vector(63 downto 0);
  

signal event_data_word				: std_logic_vector(127 downto 0);
signal event_ctrl					: std_logic;
signal event_data_wen				: std_logic;
signal backpressure					: std_logic;
signal Link_DOWN_n					: std_logic;
		
signal FED_clock_resetn				: std_logic;

signal INJECT_ERR                   : std_logic_vector(17 downto 0);
signal FED_SR_status_address        : std_logic_vector(15 downto 0);
signal FED_SR_status                : std_logic_vector(63 downto 0);

signal rst_usr_clk_async_p          : std_logic;
signal rst_usr_clk_async_n          : std_logic;
--****************************************************************************
--**********    CODE   START     HERE    *************************************
--****************************************************************************
begin

rst_usr_clk_async_n     <= not(rst_usr_clk_async_p);

reset_resync_i0:resetn_resync
port map(
	aresetn			=> rst_usr_clk_async_n,
	clock			=> FED_clock,
					 
	Resetn_sync		=> FED_clock_resetn
	);

--
-- Control write/read functions
--
process(rst_usr_clk_n,usr_clk)
begin
	if rst_usr_clk_n = '0' then
	    txdiffctrl_in       <= "11000";
        txpostcursor_in     <= "00000";
        txprecursor_in     	<= "00000";	 
        INJECT_ERR       	<= (others => '0');	 
        rst_usr_clk_async_p <= '0';
	elsif rising_edge(usr_clk) then
	
       INJECT_ERR     	<= (others => '0');	 
	   if  usr_wen = '1'  then
            if usr_func_wr( FED_SR_snd_serdes_analog + SR_offset) = '1' then
                txdiffctrl_in       <= usr_data_wr(04 downto 00);
                txpostcursor_in     <= usr_data_wr(12 downto 08);
                txprecursor_in     	<= usr_data_wr(20 downto 16); 
                rst_usr_clk_async_p <= usr_data_wr(31);
            end if;
            
            if usr_func_wr( FED_SR_SND_gen_error + SR_offset) = '1' then
                INJECT_ERR          <= usr_data_wr(17 downto 00); 
            end if;
       end if;
	end if;
end process;

process(usr_clk)
begin
    if rising_edge(usr_clk) then	
        if  usr_wen = '1'  then
            if usr_func_wr( FED_SR_status_address_off + SR_offset) = '1' then
                FED_SR_status_address   <= usr_data_wr(15 downto 00); 
            end if;
       end if;
	end if;
end process;

process(usr_clk)
begin
	if rising_edge(usr_clk) then
		ctrl_usr_data_reg		<= (others => '0');
		if  	usr_func_rd( FED_SR_snd_serdes_analog + SR_offset) = '1' then
			ctrl_usr_data_reg(04 downto 00)	<= txdiffctrl_in  ;
			ctrl_usr_data_reg(12 downto 08)	<= txpostcursor_in;
			ctrl_usr_data_reg(20 downto 16)	<= txprecursor_in ;			 
			ctrl_usr_data_reg(31)			<= rst_usr_clk_async_p ;
			
		elsif	usr_func_rd( FED_SR_snd_status + SR_offset) = '1' then 
			ctrl_usr_data_reg(00)			<= Link_DOWN_n  ;
			ctrl_usr_data_reg(01)			<= backpressure  ;
			
		elsif	usr_func_rd( FED_SR_status_address_off + SR_offset) = '1' then 
			ctrl_usr_data_reg(15 downto 0)	<= FED_SR_status_address  ; 
		elsif	usr_func_rd( FED_SR_status_off + SR_offset) = '1' then 
			ctrl_usr_data_reg(63 downto 00) <= FED_SR_status  ; 
		end if;
	end if;
end process;


usr_dto_receiver	<= 	ctrl_usr_data_reg or core_usr_data_out;
--
-- FED emulator
--
evt_gen:entity work.Event_generator 
	generic map(addr_offset 	=> SR_offset )
	port map 	(
		usr_clk					=> usr_clk			,
		usr_rst_n				=> rst_usr_clk_n	,
		usr_func_wr				=> usr_func_wr      ,
		usr_wen					=> usr_wen          ,
		usr_data_wr				=> usr_data_wr      ,  				    
		usr_func_rd				=> usr_func_rd 		,
		usr_rden				=> usr_rden        	,
		usr_data_rd				=> core_usr_data_out,
								 
		dt_clock				=> FED_clock		,
								 
		event_data_word			=> event_data_word	,
		event_ctrl				=> event_ctrl		,
		event_data_wen			=> event_data_wen	,
		backpressure			=> backpressure		,
								 
		ext_trigger				=> '0'
		-- ext_veto_out			=>  
	 );

 
--
--  CORE SLINKROCKET SENDER
-- 
SR_sender:entity work.SR_sender_GLB
  generic map( txpolarity_in	=> '0',
			   rxpolarity_in	=> '0',
			   Clock_source		=> Clock_source,
			   throughput		=> throughput  , 
			   technology		=> technology  
			   )
  PORT MAP( 
    aresetn              		=> FED_clock_resetn,
    txdiffctrl_in        		=> txdiffctrl_in    ,
    txpostcursor_in      		=> txpostcursor_in  ,
    txprecursor_in       		=> txprecursor_in   , 
    Core_status_CE       		=> '1',
    Core_status_addr     		=> FED_SR_status_address,
    Core_status_data_out 		=> FED_SR_status,
	INJECT_ERR                  => INJECT_ERR,
    
    srds_freerunning_clock      => clk_freerun_in,
    FED_CLOCK            		=> FED_clock,
    event_data_word      		=> event_data_word,
    event_ctrl           		=> event_ctrl,
    event_data_wen       		=> event_data_wen,
    backpressure         		=> backpressure,
    Link_DOWN_n          		=> Link_DOWN_n,
	
	gtS_Reset_TX_clock_out		=> gtS_Reset_TX_clock_out	,
	gtS_userclk_tx_active_in	=> gtS_userclk_tx_active_in	,	
	gtS_userclk_tx_usrclk_in	=> gtS_userclk_tx_usrclk_in	,		
	gtS_userclk_tx_usrclk2_in	=> gtS_userclk_tx_usrclk2_in,		
	gtS_userclk_tx_usrclk4_in	=> gtS_userclk_tx_usrclk4_in,
	
	gtM_Reset_TX_clock_in_0		=> gtM_Reset_TX_clock_in(0),
	gtM_Reset_TX_clock_in_1		=> gtM_Reset_TX_clock_in(1),
	gtM_Reset_TX_clock_in_2		=> gtM_Reset_TX_clock_in(2), 
	gtM_userclk_tx_active_out	=> gtM_userclk_tx_active_out,
	gtM_userclk_tx_usrclk_out	=> gtM_userclk_tx_usrclk_out,
	gtM_userclk_tx_usrclk2_out	=> gtM_userclk_tx_usrclk2_out,
	gtM_userclk_tx_usrclk4_out	=> gtM_userclk_tx_usrclk4_out,
	
    qpll_lock_in         		=> qpll_lock_in,
    qpll_reset_out       		=> qpll_reset_out,
    qpll_clkin           		=> qpll_clkin,
    qpll_ref_clkin       		=> qpll_ref_clkin,
    reset_GTTXRESET_in          => reset_GTTXRESET_in,
    reset_GTTXRESET_out         => reset_GTTXRESET_out,
		
    Snd_gt_rxn_in        		=> SRO_gt_rxn_in ,
    Snd_gt_rxp_in        		=> SRO_gt_rxp_in ,
    Snd_gt_txn_out       		=> SRO_gt_txn_out,
    Snd_gt_txp_out       		=> SRO_gt_txp_out,
    Rst_hrd_sim          		=> Rst_hrd_sim
  );

  
end Behavioral;
