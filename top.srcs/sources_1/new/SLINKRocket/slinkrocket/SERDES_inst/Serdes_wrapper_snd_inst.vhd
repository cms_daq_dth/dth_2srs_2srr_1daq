----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 20.11.2017 10:42:34
-- Design Name: 
-- Module Name: Serdes_wrapper_inst - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 		This file is used to setup the SERDES
-- 		contains the logic to bitslip the word to align it 
-- 		it swaps the 2 64b word (SERDES has a128 b word interface)
--		Has soon as the word is aligned and detects that the other side of the link is on the same behaviour , it send  INPUT DATA (which is idle or pakcet)
--		The link is keep in LINKUP state if at least an Idle word is seen each 512 words ( which is the double of a max packet size
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;
 
entity Serdes_wrapper_snd_inst is
 Generic (  txpolarity_in					        : std_logic := '0';
	        rxpolarity_in					        : std_logic := '0'; 
	        PRBS_check_time                          : integer := 26;
--	        PRBS_check_time                          : integer := 12;--simulation
		    Clock_source							: string := "Master";
				--possible choices are Slave or Master
            throughput								: string := "25.78125";
				--possible choices are  15.66 or 25.78125 
			technology								: string := "GTY" 
				-- possible choices are GTY or GTH 
				); 
  Port (
 		txdiffctrl_in 									: in std_logic_vector(4 downto 0) := "11000";
		txpostcursor_in 								: in std_logic_vector(4 downto 0) := "10100";
		txprecursor_in									: in std_logic_vector(4 downto 0) := "00000";
		Srds_loopback_in								: in std_logic;
		Release_Loopback                                : out std_logic;
        SERDES_status									: out std_logic_vector(63 downto 0);
  -- data bus
		userclk_tx_srcclk_out					        : out std_logic;						-- FRAME to send over SERDES
		tx_header									    : in std_logic_vector(5 downto 0);		-- bit control of the 64/66 encoding
		tx_data										    : in std_logic_vector(127 downto 0);	-- data word
		
		userclk_rx_srcclk_out					        : out std_logic;						-- FRAME received over SERDES
		rx_data_valid								    : out std_logic_vector(1 downto 0);		-- valid data word
		rx_header									    : out std_logic_vector(5 downto 0);		-- header bit (64/66 encoding)
		rx_header_valid							        : out std_logic_vector(1 downto 0);		-- valid header bits
		rx_data										    : out std_logic_vector(127 downto 0);	-- data words (2 x 64 bit)
		rx_SOS										    : out std_logic_vector(1 downto 0);		-- Start Of Sequence

		SERDES_ready									: out std_logic;
  --   Gb serdes interface
  		clk_freerun_in								    : in std_logic;							-- reference clocks QPLL signals
		
  --  Clock source and destination
		--Clock control to/from  SERDES/logic
		-- These signals are from the serdes to be used to generate the master clock
		gtM_Clock_Src_TX_out							: out std_logic; 
		gtx_Reset_TX_clock_out							: out std_logic;  
		gtx_userclk_tx_active_in						: in std_logic; 
		gtx_userclk_tx_usrclk_in						: in std_logic;
		gtx_userclk_tx_usrclk2_in						: in std_logic;
		gtx_userclk_tx_usrclk4_in						: in std_logic; 
		
		--Clock Control to/from MASTER 
		-- these signals are source to generate the master clock of the serdes
		gtM_Clock_Src_TX_in								: in std_logic; 
		gtM_Reset_TX_clock_in							: in std_logic_vector(3 downto 0);--active HIGH  
		gtM_userclk_tx_active_out						: out std_logic; 
		gtM_userclk_rx_active_out						: out std_logic; 
		gtM_userclk_tx_usrclk_out						: out std_logic;
		gtM_userclk_tx_usrclk2_out						: out std_logic;
		gtM_userclk_tx_usrclk4_out						: out std_logic; 
		
  -- QPLL 		 
		qpll_lock_in									: IN STD_LOGIC;
		qpll_reset_out									: OUT STD_LOGIC;
		qpll_clk_in										: IN STD_LOGIC;
		qpll_refclk_in									: IN STD_LOGIC;
		reset_GTTXRESET_in                              : in STD_LOGIC;
		reset_GTTXRESET_out                             : OUT STD_LOGIC;
  -- High speed link	
		gt_rxn_in                          	            : IN  STD_LOGIC_VECTOR(0 DOWNTO 0);		-- SERDES connection 
		gt_rxp_in                          	            : IN  STD_LOGIC_VECTOR(0 DOWNTO 0);	
		gt_txn_out                         	            : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
		gt_txp_out                         	            : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
		 
		Rst_hrd_sim										: in std_logic
  );
end Serdes_wrapper_snd_inst;

--*///////////////////////////////////////////////////////////////////////////////
--*////////////////////////   Behavioral        //////////////////////////////////
--*///////////////////////////////////////////////////////////////////////////////
architecture Behavioral of Serdes_wrapper_snd_inst is

signal counter_slip_done								            : std_logic_vector(15 downto 0) := x"0026";
signal Shift_counter_max											: std_logic_vector(6 downto 0) := "1000000";--64
signal Shift_inv_counter_max										: std_logic_vector(4 downto 0) := "10000";--16
 

signal drpadd                         	         : STD_LOGIC_VECTOR(15 DOWNTO 0)  ;
signal drpdi                           	         : STD_LOGIC_VECTOR(15 DOWNTO 0) :=x"0000";
signal drpen                            	     : STD_LOGIC:='0';
signal drpwe                           	         : STD_LOGIC:='0';
signal drpdo                          	         : STD_LOGIC_VECTOR(15 DOWNTO 0);
signal drprdy                         	         : STD_LOGIC ;

signal enter_loopB                         	         : STD_LOGIC:='0';
signal out_loopB                         	         : STD_LOGIC:='0';
signal reset_GTTXRESET_local               	         : STD_LOGIC:='0';
signal reset_GTTXRESET_lcell_out           	         : STD_LOGIC:='0';
signal reset_GTTXRESET_lcell              	         : STD_LOGIC:='0';
	
signal gtx_Clock_Src_RX								: std_logic;
signal gtx_Reset_RX_clock							: std_logic;
signal gtx_userclk_rx_usrclk						: std_logic;
signal gtx_userclk_rx_usrclk2						: std_logic;
signal gtx_userclk_rx_usrclk4						: std_logic;
signal gtx_userclk_rx_active						: std_logic;

signal Srds_loopback_resync                         : std_logic;
signal Srds_loopback_previous                       : std_logic;

type loopback_state_state is (	Idle,
								readDRP_reg7c, 
								latchDRP_reg7c,
								writeDRP_reg7c,
								write_wait_DRP_done,  
								DRP_done,
								wait_TXreset_done,
								LoopBack_mode_out,
								GT_reset_in_loopback,
								GT_reset_out_loopback
							);
signal loopback_state:loopback_state_state;

signal loopback_in									: std_logic_vector(2 downto 0);
signal rxoutclksel_in								: STD_LOGIC_VECTOR(2 DOWNTO 0);
signal rxcdrhold_in									: STD_LOGIC;
 
signal qpll_reset_cell					: std_logic;
 
type Align_serdes_state is (	ST_Start,
								ST_Reset_counters,
								ST_Check_pattern,
								ST_Check_valid,
								ST_Check_unvalid,
								ST_slip_state,
								ST_wait_slip_done,
								ST_send_idle, 
								ST_wait_Idle_from_other_side,
								ST_link_up 
							);
signal Align_serdes:Align_serdes_state;
-- attribute fsm_encoding : string;
-- attribute fsm_encoding of Align_serdes : signal is "one_hot";
  
signal TX_userclk_cell			 				                    : std_logic;
signal RX_userclk_cell			 				                    : std_logic;
				 
signal gtpowergood_out								                : std_logic;
		   
signal data_path_rx_reset					 	              	    : std_logic := '0';
signal data_path_rx_reset_cell				 	              	    : std_logic := '0'; 
  
signal rxcdrphdone_out 												: std_logic_vector(0 DOWNTO 0);
 
signal gtwiz_reset_tx_done_out              	                    : std_logic := '0'; 
signal gtwiz_reset_rx_cdr_stable_out        	                    : std_logic := '0'; 
			               				  	
signal Shift_counter												: std_logic_vector(6 downto 0);
signal Shift_inv_counter											: std_logic_vector(4 downto 0);
 
signal slip_done													: std_logic; 
signal Init_done													: std_logic;  
signal send_idle                                                    : std_logic;
signal send_idle_sec                                                : std_logic;
signal send_idle_cell                                               : std_logic;

signal send_idle_sync                                               : std_logic;
signal Idle_present        											: std_logic; 
signal Link_locked 													: std_logic; 
signal Link_locked_sync   											: std_logic; 

signal STATE_link													: std_logic; 
constant LINK_UP													: std_logic := '1'; 
constant LINK_DOWN													: std_logic := '0';  
signal retry_init													: std_logic_vector(3 downto 0); 

signal pattern_found												: std_logic; 

signal rxgearboxslip									            : std_logic := '0';
signal rxgearboxslip_sync								            : std_logic;
signal wait_counter 									            : std_logic_vector(15 downto 0) := x"0000";
signal time_wait_counter 								            : std_logic_vector(15 downto 0) := x"0000";
signal flip_words													: std_logic := '0'; 
 
signal status											            : std_logic_vector(63 downto 0) := x"0000000000000000";

-- status RX and TX  reset
signal reset_rx_done								           		: std_logic; 
signal reset_rx_done_cell							           		: std_logic; 
signal reset_rx_done_resync									        : std_logic; 
signal reset_tx_done									            : std_logic; 
signal resetp_drp_counter                                           : std_logic_vector(11 downto 0) := x"000"; 
signal resetp_drp         									        : std_logic;  
signal rx_init_done_reg									            : std_logic; 
   
signal txpolarity_sync												: std_logic;
signal rxpolarity_sync												: std_logic;
   
signal DRP_clock	  											    : std_logic;
   
signal txpippmsel    												: std_logic;
signal txpmaresetdone 												: std_logic;
signal txprgdivresetdone 											: std_logic;
signal rxpmaresetdone 												: std_logic;
signal rxprgdivresetdone 											: std_logic; 
signal Reset_TX_clock_cell 											: std_logic;  
signal Reset_RX_clock_cell 											: std_logic;  

signal tx_clock_ready												: std_logic;
signal rx_clock_ready												: std_logic; 
signal rx_clock_ready_sync  										: std_logic; 
  
--TX data path to SERDES  
signal tx_data_reg													: std_logic_vector(127 downto 0);
signal tx_hd_reg													: std_logic_vector(5 downto 0);
  
signal Reset_sync_logic											    : std_logic;
signal rx_usrclk2_not_readyp										: std_logic := '0';
signal rx_usrclk2_not_readyp_resync									: std_logic := '0'; 
 
signal gtwiz_reset_all_cell 										: std_logic := '0'; 
signal reset_all_logic												: std_logic := '0'; 
  
signal rx_src_data						               				: std_logic_vector(127 downto 0); 
signal rx_scr_data_valid_cell										: std_logic_vector(1 DOWNTO 0);
signal rx_scr_header_cell											: std_logic_vector(5 DOWNTO 0);
signal rx_scr_header_valid_cell										: std_logic_vector(1 DOWNTO 0); 
signal rx_scr_SOS													: std_logic_vector(1 DOWNTO 0);

signal rx_data_valid_rg								                : std_logic_vector(1 downto 0);
signal rx_header_rg									                : std_logic_vector(5 downto 0);
signal rx_header_valid_rg							                : std_logic_vector(1 downto 0);	
signal rx_data_reg										            : std_logic_vector(127 downto 0);		

signal check_link_counter											: std_logic_vector(15 downto 0); 
signal reset_check_link_counter										: std_logic; 
signal check_pattern_counter										: std_logic_vector(1 downto 0); 
signal check_code_counter											: std_logic_vector(15 downto 0); 
signal Incr_check_code_counter										: std_logic;
signal link_lost													: std_logic;
signal Srds_loopback_in_resync_rx_clk   							: std_logic;
signal Srds_loopback_ongoing_resync_rx_clk   						: std_logic;

signal loopback_ongoing                                            : std_logic := '0';

signal rxprbslocked                      							: std_logic;
signal rxprbslocked_sync                  							: std_logic;
signal rxprbssel                      							    : std_logic_vector(3 downto 0);

signal Counter_PRBS_Check                                           : std_logic_vector(PRBS_check_time-1 downto 0) := std_logic_vector(to_unsigned(0,PRBS_check_time));
signal Reset_PRBS_Check                                             : std_logic;
signal Reset_PRBS_Check_resync                                     : std_logic;
signal Skip_first_PRBS_check                                        : std_logic; 
signal Release_Loopback_reg                                         : std_logic;          

signal qpll_lock_resync                                             : std_logic;              
signal txresetdone_out                                              : std_logic;              
signal txresetdone_resync                                           : std_logic;              
  
signal P_TX_TIMER_DURATION_US 			                            : integer;
signal P_RX_TIMER_DURATION_US 			                            : integer;

attribute mark_debug : string;

 attribute mark_debug of Link_locked		     : signal is "true"; 
 attribute mark_debug of rx_data_reg 		 : signal is "true"; 
 attribute mark_debug of tx_data_reg 		 : signal is "true"; 
 attribute mark_debug of Align_serdes		 : signal is "true"; 
 attribute mark_debug of link_lost		     : signal is "true"; 
 attribute mark_debug of Shift_counter		 : signal is "true"; 
 attribute mark_debug of Shift_inv_counter    : signal is "true"; 
 attribute mark_debug of loopback_state    : signal is "true"; 
 attribute mark_debug of drpdi    : signal is "true"; 
 attribute mark_debug of drpwe    : signal is "true"; 
 attribute mark_debug of drpen    : signal is "true"; 
 attribute mark_debug of drpadd    : signal is "true";  
 attribute mark_debug of drprdy    : signal is "true";  
 attribute mark_debug of Reset_sync_logic    : signal is "true"; 
 attribute mark_debug of reset_all_logic    : signal is "true"; 
 attribute mark_debug of rxgearboxslip    : signal is "true"; 
 attribute mark_debug of gtwiz_reset_all_cell    : signal is "true"; 
 attribute mark_debug of loopback_ongoing    : signal is "true"; 
 attribute mark_debug of Counter_PRBS_Check    : signal is "true"; 
 attribute mark_debug of Reset_PRBS_Check    : signal is "true"; 
 attribute mark_debug of rxprbslocked_sync    : signal is "true"; 
 attribute mark_debug of Skip_first_PRBS_check    : signal is "true"; 
 attribute mark_debug of qpll_reset_cell    : signal is "true"; 
 attribute mark_debug of txpmaresetdone    : signal is "true"; 
 attribute mark_debug of txprgdivresetdone    : signal is "true"; 
 attribute mark_debug of rxpmaresetdone    : signal is "true"; 
 attribute mark_debug of rxprgdivresetdone    : signal is "true"; 
   
--#############################################################################
-- Code start here
--#############################################################################
begin
 
-- Control the LoopBack logic
-- should enable TX biffer and disable TXGEARBOX_en  
-- when going to LOOPBACK and reverse when leaving
--
-- Do not change RX_XCLK_SEL , it works without and it is not explicitelly requested (is Loopback signel doesn't it???)
--
 
process(drp_clock)
begin
    if rising_edge(drp_clock) then
        
        if resetp_drp_counter < x"FF0" then
            resetp_drp_counter <= resetp_drp_counter + '1';
            resetp_drp         <= '1';
        else
            resetp_drp         <= '0'; 
        end if;
    end if;
end process;
	
resync_sig_loopback:entity work.resync_sig_gen                                          
    port map(                  
        clocko               => drp_clock      , 
        in_s                => Srds_loopback_in            , 
        out_s               => Srds_loopback_resync         
        );  
	
resync_sig_rxprbslocked:entity work.resync_sig_gen                                          
    port map(                  
        clocko               => drp_clock      , 
        in_s                => rxprbslocked            , 
        out_s               => rxprbslocked_sync         
        );  
	
resync_sig_PLLlocked:entity work.resync_sig_gen                                          
    port map(                  
        clocko               => drp_clock      , 
        in_s                => qpll_lock_in            , 
        out_s               => qpll_lock_resync         
        );  

resync_sig_TXresetDone:entity work.resync_sig_gen                                          
    port map(                  
        clocko               => drp_clock      , 
        in_s                => txresetdone_out            , 
        out_s               => txresetdone_resync         
        );  

 process(resetp_drp,drp_clock)
 begin
  if resetp_drp = '1' then
    loopback_state  <= Idle;
  elsif rising_edge(drp_clock) then
        Srds_loopback_previous  <= Srds_loopback_resync; 
        case loopback_state is 
            when idle =>
                enter_loopB		<= '0'; 
                out_loopB		<= '0'; 
                if	 Srds_loopback_resync = '1' and Srds_loopback_previous = '0' then
					enter_loopB		<= '1';  
                    loopback_state  <= readDRP_reg7c;
				elsif Srds_loopback_resync = '0' and Srds_loopback_previous = '1' then
					out_loopB		<= '1'; 
                    loopback_state  <= readDRP_reg7c;
                end if;
                
           when readDRP_reg7c =>   
			    loopback_state  <= latchDRP_reg7c;
			       
			when latchDRP_reg7c => 
				if drprdy = '1' then
					loopback_state  <= writeDRP_reg7c;
				end if;
			
			when writeDRP_reg7c =>  
				loopback_state  <= write_wait_DRP_done;
			
			when write_wait_DRP_done =>  
				if drprdy = '1' then
					loopback_state  <= DRP_done;
				end if;
				                
			when DRP_done =>  
					enter_loopB		<= '0';
					out_loopB		<= '0';
					if enter_loopB = '1' then
					   loopback_state  <= GT_reset_in_loopback; 
			        else
					   loopback_state  <= GT_reset_out_loopback; 
			       end if;
			       
			when GT_reset_in_loopback => 
			     if qpll_lock_resync = '1' then
			         loopback_state  <= wait_TXreset_done; 
			     end if;
			     
			when GT_reset_out_loopback => 
			     if qpll_lock_resync = '1' then
			         loopback_state  <= wait_TXreset_done; 
			     end if;
			     
		    when wait_TXreset_done => 
		          if  txresetdone_resync = '1' then
		              loopback_state  <= LoopBack_mode_out; 
			     end if;
			     
			when LoopBack_mode_out => 
			     loopback_state 		<= Idle;
			     
			when others => 
				loopback_state 		<= Idle;	
				 
        end case;
  
  end if; 
 end process;
 
 process(resetp_drp,drp_clock)
 begin
    if resetp_drp = '1' then
        loopback_ongoing        <= '0'; 
        Skip_first_PRBS_check   <= '0'; 
	elsif rising_edge(drp_clock) then 
        if     enter_loopB = '1' then
		  loopback_ongoing    <= '1'; 
		elsif  loopback_state = LoopBack_mode_out and Srds_loopback_resync = '0' then
		  loopback_ongoing    <= '0'; 
		end if;
		
		-- Skipt first PRBS lock check
		if     enter_loopB = '1' then
		  Skip_first_PRBS_check   <= '1'; 
		elsif  Reset_PRBS_Check = '1' then
		  Skip_first_PRBS_check   <= '0'; 
		end if;
  end if; 
 end process;
 
 
 process(drp_clock)
 begin
	if rising_edge(drp_clock) then
		if    loopback_state  = latchDRP_reg7c then
			if		 enter_loopB = '1' then
				drpdi		<= drpdo;
				drpdi(7) 	<= '1'; 
				drpdi(13) 	<= '0'; 
			elsif 	out_loopB	= '1' then
				drpdi		<= drpdo;
				drpdi(7) 	<= '0'; 
				drpdi(13) 	<= '1'; 
			end if;
			
		end if;
		
		if    loopback_state = readDRP_reg7c or loopback_state = writeDRP_reg7c then
		
		       drpadd     <= x"007C";
		end if;
		
	    drpen   <= '0'; 
		if      (loopback_state = readDRP_reg7c  
		      or loopback_state = writeDRP_reg7c)  then
		  drpen   <= '1'; 
		end if;
		
		drpwe   <= '0'; 
		if (loopback_state = writeDRP_reg7c )  then
		  drpwe   <= '1';  
		end if;
		
		reset_GTTXRESET_local   <= '0'; 
		if loopback_state = GT_reset_in_loopback then
		  reset_GTTXRESET_local <= '1'; 
		end if;
		
		reset_GTTXRESET_lcell_out   <= '0'; 
		if loopback_state = GT_reset_out_loopback then
		  reset_GTTXRESET_lcell_out <= '1'; 
		end if;
		
		if Srds_loopback_resync = '1' then
		  Counter_PRBS_Check    <= Counter_PRBS_Check + '1'; 
		else
		  Counter_PRBS_Check    <= (others => '0' );
		end if;
		-- Reset the PRBS locked
		Reset_PRBS_Check    <= '0'; 
		if Counter_PRBS_Check(PRBS_check_time-1 downto 0) =  '1'& std_logic_vector(to_unsigned(0,PRBS_check_time-1)) then
		  Reset_PRBS_Check    <= '1'; 
		end if;

		-- release the Loopback if no PRBS locked (no test ongoing!)
		-- This signal will release the Srds_loopback_in signal which will generate the procedure to go out of loopback mode
		Release_Loopback_reg  <= '0'; 
		if Reset_PRBS_Check = '1'  and rxprbslocked_sync = '0' and Skip_first_PRBS_check = '0' then
		  Release_Loopback_reg  <= '1'; 
		end if;
    end if;	
 end process;
 
 reset_GTTXRESET_out    <= reset_GTTXRESET_lcell_out;
 reset_GTTXRESET_lcell  <= reset_GTTXRESET_in or reset_GTTXRESET_local;
 Release_Loopback       <= Release_Loopback_reg;
 --****************************************************************
 
 -- DRP clock use a 100 MHz free runing clock from pll
DRP_clock 			<= clk_freerun_in;
qpll_reset_out		<= qpll_reset_cell;

resync_sig_i14:entity work.resync_sig_gen                                          
    port map(                  
        clocko               => TX_userclk_cell      , 
        in_s                 => Link_locked            , 
        out_s                => Link_locked_sync         
        );  
 
-- send 2 idle to end the sync
-- Because at the reception (when GTH is used) it is received on 2 x 64 bit which can 
-- be seen on 2 following 127 bit word . With 2 Idles of 127 bit we are sure that we receive
-- at least a 127bit idle word at reception
send_idle_cell  <= send_idle or send_idle_sec;
 
resync_pulse_i15:entity work.resync_pulse  
    port map(
        aresetn               => '1',
        clocki                => RX_userclk_cell,  
        in_s                  => send_idle_cell,
        clocko                => TX_userclk_cell,
        out_s                 => Send_Idle_sync
        ); 
        
--/////////////////////////////////////////////////////////////////////////////////////////              
-- Here is a mux used to send a predefined pattern (over teh SERDES) to help the other side of the link to sync
-- When sync is done, this mux will send the data from SR                              		 	    		 
process(TX_userclk_cell)
begin
	if rising_edge(TX_userclk_cell) then
	
		if Link_locked_sync = '0' and Send_Idle_sync = '0' then
			 tx_data_reg 		<= x"07aa55aa55aa55aa07aa55aa55aa55aa";
			 tx_hd_reg			<= "001001";
		else
			tx_data_reg 		<= tx_data;
			tx_hd_reg			<= tx_header;
		end if;
	end if;
end process;   	
  
--/////////////////////////////////////////////////////////////////////////////////
-- Clock managment

Master_clock:if Clock_source = "Master"  generate
	
	Reset_TX_clock_cell					<= '0' when gtM_Reset_TX_clock_in = "0000"  else '1';
          	

    gtwiz_userclk_tx_inst:entity  work.SERDES_GTx_userclk_tx 
        generic map(
            P_CONTENTS                     		=> 0,
            P_FREQ_RATIO_SOURCE_TO_USRCLK  		=> 1,
            P_FREQ_RATIO_USRCLK_TO_USRCLK2 		=> 2
         )
         Port Map( 
            gtwiz_userclk_tx_srcclk_in  		=> gtM_Clock_Src_TX_in,	
            gtwiz_userclk_tx_reset_in   		=> Reset_TX_clock_cell,	
            gtwiz_userclk_tx_usrclk_out 		=> gtM_userclk_tx_usrclk_out,	
            gtwiz_userclk_tx_usrclk2_out		=> gtM_userclk_tx_usrclk2_out,	
            gtwiz_userclk_tx_usrclk4_out		=> gtM_userclk_tx_usrclk4_out,	
            gtwiz_userclk_tx_active_out			=> gtM_userclk_tx_active_out 
     );

end generate;

Reset_RX_clock_cell					<= '0' when gtx_Reset_RX_clock  = '0' else '1';
 
gtwiz_userclk_rx_inst:entity work.SERDES_GTx_userclk_rx  
    generic map(
        P_CONTENTS                     		=> 0,
        P_FREQ_RATIO_SOURCE_TO_USRCLK  		=> 1,
        P_FREQ_RATIO_USRCLK_TO_USRCLK2 		=> 2
     )
    Port Map( 
        gtwiz_userclk_rx_srcclk_in  		=> gtx_Clock_Src_RX,
        gtwiz_userclk_rx_reset_in   		=> Reset_RX_clock_cell,
        gtwiz_userclk_rx_usrclk_out 		=> gtx_userclk_rx_usrclk,
        gtwiz_userclk_rx_usrclk2_out		=> gtx_userclk_rx_usrclk2,
        gtwiz_userclk_rx_usrclk4_out		=> gtx_userclk_rx_usrclk4,
        gtwiz_userclk_rx_active_out			=> gtx_userclk_rx_active 
     );

gtM_userclk_rx_active_out   <= gtx_userclk_rx_active;

--///////////////////////////////////////////////////////////////////////////////// 
--		 SERDES  reset

gtx_Reset_TX_clock_out 					<= '0' when (txpmaresetdone = '1' and txprgdivresetdone = '1') else '1';
gtx_Reset_RX_clock  					<= '0' when rxpmaresetdone = '1' and rxprgdivresetdone = '1' else '1';
 
userclk_tx_srcclk_out					<= TX_userclk_cell;-- these clocks are half(usrclk2_in) for GTH and usrclk2_in for the GTY
userclk_rx_srcclk_out					<= RX_userclk_cell;
	
gtwiz_reset_all_cell    				<= reset_all_logic or  Rst_hrd_sim; 
 
--****************************
-- Settings signals to do a loopback Near-PCS when a SlinkRcoket is not used
--							Normal									loopback Far-end PMA loopback

loopback_in						<=  "000"  when Srds_loopback_in = '0' else "100";
txpippmsel                      <=  '1'    when Srds_loopback_in = '0' else '0';
 
resync_sig1_loopback_rx_clk:entity work.resync_sig_gen                                          
    port map(                  
        clocko              => RX_userclk_cell      , 
        in_s                => Srds_loopback_in            , 
        out_s               => Srds_loopback_in_resync_rx_clk         
        );  	
  
rxprbssel                       <=  "0000" when Srds_loopback_in_resync_rx_clk = '0' else "0101";

--							RXOUTCLKSEL= PROGDIVCLK											
rxoutclksel_in					<=  "101"; 
--																	 
rxcdrhold_in					<=  '0'; 

--/////////////////////////////////////////////////////////////////////////////////
-- resync signals
txpolarity_resync_sig_i1:entity work.resync_sig_gen   
port map( 
	clocko			=> TX_userclk_cell,
	in_s			=> txpolarity_in,
	out_s			=> txpolarity_sync 
	);
	
rxpolarity_resync_sig_i1:entity work.resync_sig_gen   
port map( 
	clocko			=> RX_userclk_cell,
	in_s			=> rxpolarity_in,
	out_s			=> rxpolarity_sync 
	);
	
resync_pulse_reset_prbscnt5:entity work.resync_pulse  
    port map(
        aresetn               => '1',
        clocki               => drp_clock,  
        in_s                => Reset_PRBS_Check,
        clocko               => RX_userclk_cell,
        out_s               => Reset_PRBS_Check_resync
        ); 	 
	
rxgearboxslip_sync	<= rxgearboxslip;
	
--///////////////////////////////////////////////////////////////////////////////// 
--		 SERDES  instantiation

serdes_i1:entity work.Serdes_wrapper_select
Generic map( 	
				throughput								=> throughput, 
				technology								=> technology
		) 
  PORT MAP(
	gtwiz_userclk_tx_active_in(0)			=> gtx_userclk_tx_active_in     ,      
	gtwiz_userclk_rx_active_in(0)			=> gtx_userclk_rx_active     	,      
	rxusrclk_in(0)            				=> gtx_userclk_rx_usrclk     	,   
	rxusrclk2_in(0)           				=> gtx_userclk_rx_usrclk2    	,  
	rxusrclk4_in(0)           				=> gtx_userclk_rx_usrclk4	    ,  
	txusrclk_in(0)            				=> gtx_userclk_tx_usrclk_in     ,      
	txusrclk2_in(0)            				=> gtx_userclk_tx_usrclk2_in    ,     
	txusrclk4_in(0)            				=> gtx_userclk_tx_usrclk4_in    ,     
	rxoutclk_out(0)             			=> gtx_Clock_Src_RX         	,          
	txoutclk_out(0)            				=> gtM_Clock_Src_TX_out         ,          
	TX_userclk_out	                 		=> TX_userclk_cell              ,                       
    RX_userclk_out	                 		=> RX_userclk_cell              ,              
	 
	gtwiz_reset_clk_freerun_in(0)      		=> clk_freerun_in               , 
	gtwiz_reset_all_in(0)               	=> gtwiz_reset_all_cell         , 
	gtwiz_reset_tx_pll_and_datapath_in(0)	=> '0'                          , 
	gtwiz_reset_tx_datapath_in(0)       	=> '0'                          , 
	gtwiz_reset_rx_pll_and_datapath_in(0)	=> '0'                          , 
	gtwiz_reset_rx_datapath_in(0)       	=> data_path_rx_reset           , 
	gtwiz_reset_rx_cdr_stable_out(0)   		=> gtwiz_reset_rx_cdr_stable_out, 
	gtwiz_reset_tx_done_out(0)         		=> reset_tx_done                , 
	gtwiz_reset_rx_done_out(0)         		=> reset_rx_done_cell,--reset_rx_done                , 
	
	gtwiz_userdata_tx_in               		=> tx_data_reg            		, 
	txheader_in                        		=> tx_hd_reg                  	, 
	gtwiz_userdata_rx_out              		=> rx_src_data          		, 
	rxstartofseq_out                   		=> rx_scr_SOS                   , 
	rxdatavalid_out                    		=> rx_scr_data_valid_cell       , 
	rxheader_out                       		=> rx_scr_header_cell           , 
	rxheadervalid_out                  		=> rx_scr_header_valid_cell     , 
	
	drpaddr_in                         		=> drpadd(9 downto 0)           , 
	drpclk_in(0)                       		=> DRP_clock               		, 
	drpdi_in                           		=> drpdi                      	, 
	drpen_in(0)                        		=> drpen                    	, 
	drpwe_in(0)                        		=> drpwe                   		, 
    drpdo_out                          		=> drpdo                    	, 
    drprdy_out(0)                    		=> drprdy                   	, 
	
	gt_rxn_in                          		=> gt_rxn_in                    , 
	gt_rxp_in                          		=> gt_rxp_in                    , 
	gt_txn_out                         		=> gt_txn_out                   , 
	gt_txp_out                         		=> gt_txp_out                   , 
	
	loopback_in                        		=> loopback_in                  , 
    rxoutclksel_in                          => rxoutclksel_in               ,
    rxcdrhold_in(0)                         => rxcdrhold_in                 ,
	gtwiz_reset_qpll0reset_out(0)      		=> qpll_reset_cell              , 
	gtwiz_reset_qpll0lock_in(0)        		=> qpll_lock_in                 , 
	qpll0clk_in(0)                     		=> qpll_clk_in                  , 
	qpll0refclk_in(0)                  		=> qpll_refclk_in               , 
	qpll1clk_in(0)                    		=> '0'	                        , 
	qpll1refclk_in                     		=> "0"	                        , 
	
	rxcdrphdone_out 				   		=> rxcdrphdone_out              , 
	rxgearboxslip_in(0)                		=> rxgearboxslip_sync           , 
	txdiffctrl_in                      		=> txdiffctrl_in                , 
	txpolarity_in(0)                  		=> txpolarity_sync              , 
	rxpolarity_in(0)                  		=> rxpolarity_sync              , 
	txpostcursor_in                    		=> txpostcursor_in              , 
	txprecursor_in                     		=> txprecursor_in               , 
	txsequence_in                      		=> "0000000"                    , 
	gtpowergood_out(0)                 		=> gtpowergood_out              , 
	rxpmaresetdone_out(0)              		=> rxpmaresetdone               , 
	rxprgdivresetdone_out(0)           		=> rxprgdivresetdone            , 
	txpmaresetdone_out(0)              		=> txpmaresetdone               , 
	txprgdivresetdone_out(0)           		=> txprgdivresetdone            , 
    txpippmsel_in(0)                        => txpippmsel                   ,
	txprbsforceerr_in(0)					=> '0'							,
	txprbssel_in                        	=> "0000"						,
	rxprbscntreset_in(0)	               	=> Reset_PRBS_Check_resync   	,
	rxprbssel_in                        	=> rxprbssel					, 
	txpcsreset_in(0)						=> '0'							,
	txoutclksel_in 							=> "101"	                    ,
    gttxreset_in(0)		       			    => reset_GTTXRESET_lcell        ,
    gtrxreset_in(0)		       			    => '0'                          ,
    rxpmareset_in(0)		       		    => '0'                          ,
	-- rxprbserr_out                       	 
	rxprbslocked_out(0)                     => rxprbslocked	               ,
    txresetdone_out(0)		                => txresetdone_out
  );                                                                                          
reset_rx_done   <= '1' when reset_rx_done_cell = '1' or loopback_ongoing = '1' else '0';

tx_clock_ready      <= gtx_userclk_tx_active_in;                                                                                    
  
status(31)		<= '1' when Align_serdes = ST_link_up else '0';
status(30)		<= '1' when Align_serdes = ST_wait_Idle_from_other_side else '0';  
status(29)		<= '1' when Align_serdes = ST_send_idle else '0';  
status(28)		<= '1' ;--when Align_serdes =  else '0';  
status(27)		<= '1' when Align_serdes = ST_wait_slip_done else '0';  
status(26)		<= '1' when Align_serdes = ST_slip_state else '0';  
status(25)		<= '1' when Align_serdes = ST_check_pattern  else '0';  
status(24)		<= '1' when Align_serdes = ST_START else '0';  
 
status(22)		<= Link_locked;
status(21)		<= STATE_link;
status(20)		<= qpll_lock_in;

status(19)		<= tx_clock_ready;
status(18)		<= rx_clock_ready;
status(17)		<= reset_tx_done;
status(16)		<= reset_rx_done;

status(15)		<= gtpowergood_out;
status(14)		<= qpll_reset_cell; 

status(3)		<= txprgdivresetdone;
status(2)		<= txpmaresetdone;
status(1)		<= rxprgdivresetdone; 
status(0)		<= rxpmaresetdone;

--///////////////////////////////////////////////////////////////////////////////// 
-- 
 
reset_resync_i1:entity work.resetn_resync  
port map(
	aresetn				=> reset_rx_done,
	clock				=> RX_userclk_cell,
	Resetn_sync			=> reset_rx_done_resync 
	);
	
--///////////////////////////////////////////////////////////////////////////////// 
-- Initialiae the link
-- logic from Xilinx example design
 	  	     
Reset_sync_logic	<= '1' when reset_all_logic = '1' and  Srds_loopback_resync = '0' else  '0'; 
rx_init_done_reg	<= '1' when reset_rx_done = '1'  else '0';
	
serdes_init_i2:entity work.reset_serdes 
 generic map (
        P_FREERUN_FREQUENCY            =>  100,
        P_TX_TIMER_DURATION_US        =>   30000,
--        P_TX_TIMER_DURATION_US        =>   30,--simulation
        P_RX_TIMER_DURATION_US        =>   130000  
--        P_RX_TIMER_DURATION_US        =>   130  --simulation
 ) 
 Port map( 
        reset_free_run         => Reset_sync_logic,
        clock_free_run         => clk_freerun_in,
        tx_init_done           => reset_tx_done,
        rx_init_done           => rx_init_done_reg,
        rx_data_good           => STATE_link,
        reset_all_out          => reset_all_logic,
        reset_rx_out           => data_path_rx_reset_cell,
        init_done_out          => Init_done,
        retry_cntr             => retry_init
  	);  
  	
 --///////////////////////////////////////////////////////////////////////////////// 
-- data received path
-- data are piped 
 
data_path_rx_reset	<= data_path_rx_reset_cell;
 		 
process(RX_userclk_cell)
begin
	if rising_edge(RX_userclk_cell) then
        if Srds_loopback_in_resync_rx_clk = '0' then
		  rx_data_valid_rg			<= rx_scr_data_valid_cell;
		else
		  rx_data_valid_rg			<= "00";
		end if;
		rx_header_valid_rg			<= rx_scr_header_valid_cell;
		
		rx_data_reg 				<= rx_src_data;
		rx_header_rg				<= rx_scr_header_cell;
	
	end if;
end process;

rx_data_valid						<= rx_data_valid_rg;
rx_header							<= rx_header_rg;
rx_header_valid						<= rx_header_valid_rg;
rx_data								<= rx_data_reg;
SERDES_ready						<= '1' when Align_serdes = ST_link_up else '0';

--///////////////////////////////////////////////////////////////////////////////// 
 -- looking for Idle
-- The maximum block transfer is 4096 bytes  (128b x 256 clocks)
-- This counter (check_link_counter) is free running , if it reachs the max without found a Idle
-- the link is lost and should be reset
-- IF LATER I FOUND A SIGNAL WHICH DETECT A FIBER DISCONNECTION I WILL USE IT

process(reset_rx_done_resync,RX_userclk_cell)
begin
	if reset_rx_done_resync = '0'  then
		check_link_counter				<= (others => '0');
		check_pattern_counter			<= (others => '0');
		check_code_counter				<= (others => '0');
		link_lost						<= '0';
		Incr_check_code_counter			<= '0';
		reset_check_link_counter		<= '0';  
	elsif rising_edge(RX_userclk_cell) then
		--THE COUNTER IS RESET WHEN WE SEE AN IDLE OR A PREDEFINED PATTERN ON THE LINK (SEE BELOW)
		-- OR
		-- IF THE STATE LINK UP IS NOT REACHED
		if reset_check_link_counter = '1' or 
			(Align_serdes /= ST_link_up and	Align_serdes /= ST_wait_Idle_from_other_side) then
			check_link_counter	<= (others => '0');
		
		--THE COUNTER IS ENABLE ONLY IN THOSE TWO STATES (WHERE THE LINK IS SYNCKRONIZED)
		elsif (Align_serdes = ST_link_up or Align_serdes = ST_wait_Idle_from_other_side) then
			check_link_counter <= check_link_counter + '1';
		end if;
		
--		link_lost	<= check_link_counter(15) or check_code_counter(15);
		link_lost	<= check_link_counter(12) or check_code_counter(15); --simulation
		
		--THE COUNTER IS RESET WHEN WE SEE AN IDLE OR A PREDEFINED PATTERN ON THE LINK
		reset_check_link_counter		<= '0';
		if check_pattern_counter = "11" then
			reset_check_link_counter	<= '1';
		end if;
		
		if (rx_data_reg(63 downto 0) = x"1e00000000000000" and rx_data_valid_rg(0) = '1' and Align_serdes = ST_link_up) or
		   (rx_data_reg(63 downto 0) = x"0755aa55aa55aa55" and rx_data_valid_rg(0) = '1' and Align_serdes = ST_wait_Idle_from_other_side) then
			check_pattern_counter	<= check_pattern_counter + '1';
		else 
			check_pattern_counter	<= (others => '0');
		end if;
	
	
		--Check if we see a code not allow

		if Incr_check_code_counter = '1' then
			check_code_counter <= check_code_counter + '1';
		end if;
		 
		
		--THE COUNTER IS RESET WHEN WE SEE AN IDLE OR A PREDEFINED PATTERN ON THE LINK
		Incr_check_code_counter		<= '0';
		if 	(Align_serdes = ST_link_up  and rx_header_rg(4 downto 3) = "10" and rx_data_reg(127 downto 120) /= x"1e" and rx_data_reg(127 downto 120) /= x"78" and rx_data_reg(127 downto 120) /= x"b4") or
			(Align_serdes = ST_link_up  and rx_header_rg(1 downto 0) = "10" and rx_data_reg(63 downto 56)   /= x"1e" and rx_data_reg(63 downto 56)   /= x"78" and rx_data_reg(63 downto 56)   /= x"b4")  then
			Incr_check_code_counter	<= '1';
		end if;
		
	end if;
end process;
 
--///////////////////////////////////////////////////////////////////////////////// 
-- RX aligmenent  control the slip signal
 

rx_usrclk2_not_readyp	<=  reset_all_logic  or not(reset_rx_done ) ; 

rx_clock_ready_sync		<= gtx_userclk_rx_active;
 
reset_resync_i12:entity work.resetp_resync  
port map(
	aresetp				=> rx_usrclk2_not_readyp,
	clock				=> RX_userclk_cell, 
	Resetp_sync			=> rx_usrclk2_not_readyp_resync
	);
	 
resync_sig_iloopback_rx_clk:entity work.resync_sig_gen                                          
    port map(                  
        clocko              => RX_userclk_cell      , 
        in_s                => loopback_ongoing            , 
        out_s               => Srds_loopback_ongoing_resync_rx_clk         
        );  	
 
-- define some bits information/control (like IDLE_present; rxgearboxslip; Send_Idle; ..)
process(rx_usrclk2_not_readyp_resync,Srds_loopback_ongoing_resync_rx_clk,RX_userclk_cell)
begin
	if rx_usrclk2_not_readyp_resync = '1' or Srds_loopback_ongoing_resync_rx_clk = '1' then
 			rxgearboxslip				<= '0';				 
			wait_counter				<= x"0000";	 
			Idle_present	            <= '0';
			send_idle                  	<= '0';
			send_idle_sec              	<= '0';
			pattern_found				<= '0'; 
			shift_counter				<= (others => '0');
			shift_inv_counter			<= (others => '0');
			time_wait_counter			<= (others => '0');
	elsif rising_edge(RX_userclk_cell) then
			--reset counters
			if 		Align_serdes = ST_Reset_counters then
				shift_counter		<= (others => '0');
				shift_inv_counter	<= (others => '0');
			-- increment Shift Counter is pattern valid
			elsif 	Align_serdes = ST_check_pattern and pattern_found = '1' then
				Shift_counter		<= Shift_counter + '1';
			elsif 	Align_serdes = ST_check_pattern and pattern_found = '0'  then
			-- increment Shift Counter &
			-- increment Shift counter invalid is pattern invalid
				Shift_counter		<= Shift_counter + '1';
				Shift_inv_counter	<= Shift_inv_counter + '1';
			end if;
			
			--When in STATE SL_Slip_state
			-- bit shift   if the pattern received is not correct
			-- we execute a bitslip maximum each xFF clock cycles  (wait time in STATE ST_wait_slip_done)
			rxgearboxslip				<= '0';
			if Align_serdes = ST_slip_state  then 
				rxgearboxslip		<= '1';
			end if;
			
			--each 32768 clock we send an Idle when we are sync to informed the other side , and both will be able to switch to Idle and word
            send_idle   <= '0';
            if Align_serdes	= ST_send_idle then
                send_idle   <= '1';
            end if;
            
            send_idle_sec   <= send_idle;
            				
			-- Counter time for bitslip
			if 		Align_serdes = ST_wait_slip_done then  
				wait_counter	<= wait_counter + '1';
			elsif   Align_serdes = ST_slip_state then  
				wait_counter	<= x"0000";	
			end if;
			
			if Align_serdes = ST_Reset_counters then
				slip_done			<= '0'; 
			elsif wait_counter = counter_slip_done then
				slip_done			<= '1';			
			end if;
			
			if  Align_serdes = ST_wait_Idle_from_other_side   then
				time_wait_counter <= time_wait_counter + '1';
			elsif time_wait_counter = x"4000"  then
				time_wait_counter <= (others => '0');
			end if;
			
	--- check pattern found			
		-- At least 16 consecutive pattern
			--- 
			pattern_found			<= '0';
			if (rx_data_reg(127 downto 0) = x"07aa55aa55aa55aa07aa55aa55aa55aa"  and  rx_header_rg(1 downto 0) = "01" and rx_header_rg(4 downto 3) = "01") --  			 
			  or (rx_data_reg(127 downto 0) = x"1e000000000000001e00000000000000" and  rx_header_rg(1 downto 0) = "10" and rx_header_rg(4 downto 3) = "10" ) then --			 
				pattern_found		<='1';
			end if;
	
	-- check when we detect the idle (both side are syncked and we can move to idle word and start to work
			Idle_present   	<= '0';
			if rx_data_reg(127 downto 0) = x"1e000000000000001e00000000000000" and  rx_header_rg(1 downto 0) = "10" and rx_header_rg(4 downto 3) = "10"  then --
                Idle_present    <= '1';
            end if;			
 
	end if;
end process; 
 
--//////////////////////////////////////////////////////////////////////////////////
-- state to control/ reach  LINKUP // LINKDOWN


Sync_SM:process(rx_usrclk2_not_readyp_resync,RX_userclk_cell)
begin 
	if rx_usrclk2_not_readyp_resync = '1' then
	 
		Align_serdes 	<= ST_START;
	elsif rising_edge(RX_userclk_cell) then
		Case Align_serdes is
		
			-- We wait the RX reset done before any check on the data received
			-- 
			when ST_START =>
				if reset_rx_done_resync = '1' and rx_clock_ready_sync = '1' then
					Align_serdes 	<= ST_Reset_counters;
				end if;
		
			when ST_Reset_counters =>
				Align_serdes 	<= ST_check_pattern;
			--  
			--  We check if pattern was found
			-- If NO we have to slip the word in the gearbox
			-- if YES the alignemnt is done
			when ST_check_pattern => 
				 
			-- increment counters valid and check
				if 	    pattern_found = '1' and Shift_counter = Shift_counter_max and Shift_inv_counter = "00000" then 
					Align_serdes 	<= ST_send_idle;
				elsif  pattern_found = '1' and Shift_counter = Shift_counter_max and Shift_inv_counter > "00000" then
					--some pattern(s) are not correct
					Align_serdes 	<= ST_Reset_counters;
 
			-- increment counters unvalid and check
				elsif pattern_found = '0' and (Shift_inv_counter = Shift_inv_counter_max or STATE_link = LINK_DOWN) then 
					-- need a bit shift
					Align_serdes 	<= ST_slip_state;
				elsif pattern_found = '0' and Shift_counter = Shift_counter_max and Shift_inv_counter < Shift_inv_counter_max and STATE_link = LINK_UP  then
					-- lost the link reset counters
					Align_serdes 	<= ST_Reset_counters;
				end if;				
			
			
			-- we do a slip on the gearbox
			when ST_slip_state => 
				 Align_serdes       <= ST_wait_slip_done;
				
			-- wait 48 clock (min32 need after a bitslip) to check is pattern is align
			when ST_wait_slip_done =>
				if slip_done = '1' then
					Align_serdes 	<= ST_Reset_counters;
				end if;
	 
			-- send a Idle word (each 32768 words)to inform the other side of the link that we are ready to lock the link
			when ST_send_idle => 
					Align_serdes	<= ST_wait_Idle_from_other_side;
				
			-- wait for Idle from other side or send an Idle to other side
			when ST_wait_Idle_from_other_side =>
				if 		Idle_present = '1' then
					Align_serdes 	<= ST_link_up;
				elsif 	time_wait_counter(15 downto 0) = x"4000" then
					Align_serdes 	<= ST_send_idle;
				elsif link_lost = '1' then	
					Align_serdes 	<= ST_START;
				end if;
 
			--  Idle found we can lock the link
			-- If a Idle is not found at least each 256 clock , we reset the link 
			when ST_link_up =>
				if link_lost = '1' then
					Align_serdes 	<= ST_START;
				end if;
			when others => 
				Align_serdes 		<= ST_START;
				
		end case;
	end if;
end process;



process(rx_usrclk2_not_readyp_resync,RX_userclk_cell)
begin
    if rx_usrclk2_not_readyp_resync = '1' then  
        STATE_link 			<= LINK_DOWN;
		Link_locked			<= '0';
    elsif rising_edge(RX_userclk_cell) then
		STATE_link 			<= LINK_DOWN;
		-- In this 3 states the link is synckronized  
		if Align_serdes = ST_send_idle or 
			Align_serdes = ST_wait_Idle_from_other_side or 
		 	Align_serdes = ST_link_up  or
		 	Srds_loopback_ongoing_resync_rx_clk  = '1' then
				STATE_link 		<= LINK_UP;
		end if;
		
		--When locked we send only IDLE or data
		--OTHERWISE we send a predefined pattern to help the other side of the link to Synchronize
		Link_locked			<= '0';
		if Align_serdes = ST_link_up or Srds_loopback_ongoing_resync_rx_clk = '1' then  
			Link_locked		<= '1';
		end if;
	end if;
end process;

--///////////////////////////////////////////////////////////////////////////////// 
--Status interface read
SERDES_status	<= status;
 
end Behavioral;
