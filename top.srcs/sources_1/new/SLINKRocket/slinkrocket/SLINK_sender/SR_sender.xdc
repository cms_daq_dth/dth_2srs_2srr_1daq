#SERDES

set _xlnx_shared_i0 [get_pins -filter {REF_PIN_NAME == D} -of [get_cells -hierarchical -filter {NAME =~ */Sender_core_i1/FED_interface/Command_data_rd*[*]}]]
set_false_path -from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME =~ */*serdes_sender*/*Align_serdes*}]] -to $_xlnx_shared_i0
set_false_path -from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME =~ */*serdes_sender*/Link_locked*}]] -to $_xlnx_shared_i0
set_false_path -from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME =~ */*serdes_sender*/STATE_link*}]] -to $_xlnx_shared_i0



#SlinkRocket logic

set_false_path -to [get_pins -filter {REF_PIN_NAME == D} -of [get_cells -hierarchical -filter {NAME =~ */Sender_core_i1/FED_interface/status_data*[*]}]]
set_false_path -from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME =~ */Sender_core_i1/FED_interface/status_data*[*]}]]
set_false_path -to $_xlnx_shared_i0



set_false_path -from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME =~ */Sender_core_i1/FED_interface/sel_test_mode*}]]
set_false_path -from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME =~ */Sender_core_i1/FED_interface/LINKDOWN_cell*}]]
set_false_path -from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME =~ */Sender_core_i1/FED_interface/freq_measure_i1/counter_measure_reg[*]}]]


set_false_path -to [get_pins -filter {REF_PIN_NAME == D} -of [get_cells -hierarchical -filter {NAME=~ */Sender_core_i1/FED_interface/Command_data_rd_reg[*]}]]
set_false_path -to [get_pins -filter {REF_PIN_NAME == D} -of [get_cells -hierarchical -filter {NAME=~ */Sender_core_i1/FED_interface/status_data_reg[*]}]]
#set_false_path -to [get_pins -filter {REF_PIN_NAME == R} -of [get_cells -hierarchical -filter {NAME=~ */Sender_core_i1/FED_interface/status_data_reg[*]}]]


#Event generator

# do you mean the following?:
set_max_delay -datapath_only -from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME=~ */Sender_core_i1/FED_interface/*Event_generator_lite_i1/gen_run*}]] -to [get_pins -filter {REF_PIN_NAME == D} -of [get_cells -hierarchical -filter {NAME=~ */Sender_core_i1/FED_interface/*Event_generator_lite_i1/gen_run_async_reg}]] 4.000

# The wildcard in the almost full signal is to catch also the generated replica of this signal (it has a large fanout)

set_false_path -from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME =~ */FED_interface/internal_FIFO/almost_full_reg_re*}]] -to [get_pins -filter {REF_PIN_NAME == D} -of [get_cells -hierarchical -filter {NAME =~ *Event_generator_lite_i1/data_o_rg_reg[3]}]]

set_false_path -from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME =~ */Sender_core_i1/FED_interface/internal_FIFO/almost_full_reg_re*}]] -to [get_pins -filter {REF_PIN_NAME == D} -of [get_cells -hierarchical -filter {NAME =~ */Sender_core_i1/FED_interface/*Event_generator_lite_i1/data_o_rg_*[3]}]]

set_false_path -from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME =~ */Sender_core_i1/FED_interface/*Event_generator_lite_i1/trigger_counter_reg[*]}]] -to [get_pins -filter {REF_PIN_NAME == D} -of [get_cells -hierarchical -filter {NAME =~ */Sender_core_i1/FED_interface/*Event_generator_lite_i1/data_o_rg_*[*]}]]

set_false_path -from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME =~ */Sender_core_i1/FED_interface/*Event_generator_lite_i1/event_run_cell*}]] -to [get_pins -filter {REF_PIN_NAME == D} -of [get_cells -hierarchical -filter {NAME =~ */Sender_core_i1/FED_interface/*Event_generator_lite_i1/data_o_rg_*[2]}]]

#set_false_path -from [get_pins -hier -filter {NAME=~ */Sender_core_i1/FED_interface/*Event_generator_lite_i1/single_trigger_reg/C}]

set_false_path -from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME =~ */Sender_core_i1/FED_interface/*Event_generator_lite_i1/evt_size_reg[*]}]] -to [get_pins -filter {REF_PIN_NAME == D} -of [get_cells -hierarchical -filter {NAME =~ */Sender_core_i1/FED_interface/*Event_generator_lite_i1/counter_wc*[*]}]]

set_false_path -from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME =~ */Sender_core_i1/FED_interface/*Event_generator_lite_i1/evt_size_reg[*]}]] -to [get_pins -filter {REF_PIN_NAME == D} -of [get_cells -hierarchical -filter {NAME =~ */Sender_core_i1/FED_interface/*Event_generator_lite_i1/Event_size_mem*[*]}]]

set_false_path -from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME =~ */Sender_core_i1/FED_interface/*Event_generator_lite_i1/evt_num_reg[*]}]] -to [get_pins -filter {REF_PIN_NAME == D} -of [get_cells -hierarchical -filter {NAME =~ */Sender_core_i1/FED_interface/*Event_generator_lite_i1/evt_num_cnt*[*]}]]

#set_false_path -from [get_pins -hier -filter {NAME=~ */Sender_core_i1/FED_interface/*Event_generator_lite_i1/load_evt_num_reg/C}]

set _xlnx_shared_i1 [get_pins -filter {REF_PIN_NAME == D} -of [get_cells -hierarchical -filter {NAME =~ */Sender_core_i1/FED_interface/*Event_generator_lite_i1/event_data_word_reg*[*]}]]
set_false_path -from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME =~ */Sender_core_i1/FED_interface/*Event_generator_lite_i1/evt_BX_reg[*]}]] -to $_xlnx_shared_i1

set_false_path -from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME =~ */Sender_core_i1/FED_interface/*Event_generator_lite_i1/evt_Source_reg[*]}]] -to $_xlnx_shared_i1

set_false_path -from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME =~ */Sender_core_i1/FED_interface/*Event_generator_lite_i1/time_wait_reg[*]}]] -to [get_pins -filter {REF_PIN_NAME == D} -of [get_cells -hierarchical -filter {NAME =~ */Sender_core_i1/FED_interface/*Event_generator_lite_i1/counter_timer*[*]}]]


set_false_path -from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME =~ */Sender_core_i1/Packet_receiver/seqnb_reg_reg[*]}]]
set_false_path -from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME =~ */Sender_core_i1/Packet_receiver/data_reg_reg[*]}]]
set_false_path -from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME =~ */Sender_core_i1/Packet_receiver/cmd_reg_reg[*]}]]
#set_false_path -from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME =~ */Sender_core_i1/Packet_receiver/INFO_packt_reg*}]]


set_false_path -from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME =~ */Sender_core_i1/Packet_receiver/pack_counter*[*]}]]
set_false_path -from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME =~ */Sender_core_i1/FED_interface/block_counter*[*]}]]
set_false_path -from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME =~ */Sender_core_i1/FED_interface/event_counter*[*]}]]
set_false_path -from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME =~ */Sender_core_i1/FED_interface/cnt_back_p*[*]}]]
set_false_path -from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME =~ */Sender_core_i1/FED_interface/Retransmit_counter*[*]}]]
set_false_path -from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME =~ */Sender_core_i1/FED_interface/freq_measure*/measure*[*]}]]



