----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 26.09.2019 14:25:39
-- Design Name: 
-- Module Name: SR_transmit - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use work.address_table.all; 
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity SR_transmit_Quadone is
generic(SR0_offset                               : integer := 0; 
        throughput								: string := "15.66";
				--possible choices are 15.66 or 25.78125
		ref_clock								: string := "156.25";
				--possible choices are 156.25  or  322.265625 
		technology								: string := "GTY"
				-- possible choices are GTY or GTH
				);
 Port ( 
	usr_clk           				: IN STD_LOGIC;
    rst_usr_clk_n     				: IN STD_LOGIC;
    usr_func_wr       				: IN STD_LOGIC_VECTOR(16383 DOWNTO 0);
    usr_wen           				: IN STD_LOGIC;
    usr_data_wr       				: IN STD_LOGIC_VECTOR(63 DOWNTO 0);
    usr_func_rd       				: IN STD_LOGIC_VECTOR(16383 DOWNTO 0);
    usr_rden          				: IN STD_LOGIC;
    usr_dto_receiver  				: OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
				
	clk_freerun_in					: in std_logic;
	FED_clock						: in std_logic;
 
	SRO_ref_clkp                    : in std_logic;
	SRO_ref_clkn                    : in std_logic;	
	qpll0_REFCLKOUTMONITOR 			: OUT STD_LOGIC;
	SRO_gt_rxn_in     				: IN STD_LOGIC_VECTOR(0 downto 0);
    SRO_gt_rxp_in     				: IN STD_LOGIC_VECTOR(0 downto 0);
    SRO_gt_txn_out    				: OUT STD_LOGIC_VECTOR(0 downto 0);
    SRO_gt_txp_out    				: OUT STD_LOGIC_VECTOR(0 downto 0);
	
    Rst_hrd_sim         			: in std_logic	
	
 );
end SR_transmit_Quadone;

architecture Behavioral of SR_transmit_Quadone is
  
signal SRO_ref						: std_logic; 
signal qpll_lock_out     			: STD_LOGIC;
signal qpll_reset_out    			: STD_LOGIC;
signal qpll_clkin        			: STD_LOGIC;
signal qpll_ref_clkin    			: STD_LOGIC;
 
signal reset_GTTXRESET_in     			: STD_LOGIC;
signal reset_GTTXRESET_out0    			: STD_LOGIC; 
	
signal usr_dto_receiver0			: std_logic_vector(63 downto 0);
signal usr_dto_receiver1			: std_logic_vector(63 downto 0);
 
--****************************************************************************
--**********    CODE   START     HERE    *************************************
--****************************************************************************
begin
 
   
--
--Clock reference for SlinkRocket
--		
GTE_clock_i1:if technology /= "GTH_KU" generate
    IBUFDS_GTE4_inst :IBUFDS_GTE4 
    port map (
        O     	=> SRO_ref, -- 1-bit output: Refer to Transceiver User Guide
        --ODIV2 	=> ODIV2, -- 1-bit output: Refer to Transceiver User Guide
        CEB   	=> '0', -- 1-bit input: Refer to Transceiver User Guide 
        I     	=> SRO_ref_clkp, -- 1-bit input: Refer to Transceiver User Guide 
        IB 		=> SRO_ref_clkn -- 1-bit input: Refer to Transceiver User Guide
    ); 
end generate;
GTE_clock_i2:if technology = "GTH_KU" generate
   IBUFDS_GTE3_inst : IBUFDS_GTE3
   port map (
       O     	=> SRO_ref, -- 1-bit output: Refer to Transceiver User Guide
       --ODIV2 	=> ODIV2, -- 1-bit output: Refer to Transceiver User Guide
       CEB   	=> '0', -- 1-bit input: Refer to Transceiver User Guide 
       I     	=> SRO_ref_clkp, -- 1-bit input: Refer to Transceiver User Guide 
       IB 		=> SRO_ref_clkn -- 1-bit input: Refer to Transceiver User Guide
   ); 
end generate;
	
	
 QPLLi1:entity work.QPLL_wrapper_select 
  Generic map (throughput			=> throughput,
			   ref_clock			=> ref_clock,
			   technology			=> technology
				 ) 
  Port map( 
	 gtrefclk00_in			=> SRO_ref, 
	 gtrefclk01_in       	=> '0', 
	 qpll0reset_in       	=> qpll_reset_out, 
	 qpll1reset_in       	=> '0', 
							
	qpll0lock_out       	=> qpll_lock_out, 
	qpll0outclk_out     	=> qpll_clkin, 
	qpll0outrefclk_out  	=> qpll_ref_clkin  
    qpll0_REFCLKOUTMONITOR  => qpll0_REFCLKOUTMONITOR,
							 
	-- qpll1lock_out       	=> , 
	-- qpll1outclk_out     	=> , 
	-- qpll1outrefclk_out  	=>  ,
	-- qpll1_REFCLKOUTMONITOR=> 
  );   
                            
 
--
--  CORE SLINKROCKET SENDER
-- 

SR0_sender:entity work.SR_transmit
  generic map( SR_offset		     => SR0_offset, 
			   Clock_source		     => "Master",
			   throughput		     => throughput,
			   ref_clock		     => ref_clock, 
			   technology		     => technology )
  PORT MAP(
	usr_clk           			     => usr_clk         ,
    rst_usr_clk_n     			     => rst_usr_clk_n   ,
    usr_func_wr       			     => usr_func_wr     ,
    usr_wen           			     => usr_wen         ,
    usr_data_wr       			     => usr_data_wr     ,
    usr_func_rd       			     => usr_func_rd     ,
    usr_rden          			     => usr_rden        ,
    usr_dto_receiver  			     => usr_dto_receiver0,
							 
	clk_freerun_in				     => clk_freerun_in, --can be a clock between 3.125 Mhz to 125 Mhz
	FED_clock					     => FED_clock,
	
	gtM_Reset_TX_clock_in(2 downto 0)=> "000",
	
    qpll_lock_in         		     => qpll_lock_out,
    qpll_reset_out       		     => qpll_reset_out,
    qpll_clkin           		     => qpll_clkin,
    qpll_ref_clkin       		     => qpll_ref_clkin,
	reset_GTTXRESET_in          	 => reset_GTTXRESET_in,
	reset_GTTXRESET_out         	 => reset_GTTXRESET_out0,
								           				
    SRO_gt_rxn_in     			     => SRO_gt_rxn_in(0),
    SRO_gt_rxp_in     			     => SRO_gt_rxp_in(0),
    SRO_gt_txn_out    			     => SRO_gt_txn_out(0),
    SRO_gt_txp_out    			     => SRO_gt_txp_out(0),								           				
 				
    Rst_hrd_sim         		     => Rst_hrd_sim
  );  

reset_GTTXRESET_in	<= reset_GTTXRESET_out0;  
usr_dto_receiver	<= usr_dto_receiver0;
  
end Behavioral;
