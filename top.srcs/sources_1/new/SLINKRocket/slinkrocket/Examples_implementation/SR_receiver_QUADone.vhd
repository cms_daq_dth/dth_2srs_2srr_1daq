----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 25.09.2019 15:29:57
-- Design Name: 
-- Module Name: SR_receiver - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use work.address_table.all;  

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
 
library UNISIM;
use UNISIM.VComponents.all;

entity SR_receiver_quadone is
generic(SR0_offset                               : integer := 0;

        throughput								: string := "15.66";
				--possible choices are 15.66 or 25.78125
		ref_clock								: string := "156.25";
				--possible choices are 156.25  or  322.265625 
		technology								: string := "GTY"
				-- possible choices are GTY or GTH
				);
 Port (
    usr_clk           				: IN STD_LOGIC;
    rst_usr_clk_n     				: IN STD_LOGIC;
    usr_func_wr       				: IN STD_LOGIC_VECTOR(16383 DOWNTO 0);
    usr_wen           				: IN STD_LOGIC;
    usr_data_wr       				: IN STD_LOGIC_VECTOR(63 DOWNTO 0);
    usr_func_rd       				: IN STD_LOGIC_VECTOR(16383 DOWNTO 0);
    usr_rden          				: IN STD_LOGIC;
    usr_dto_receiver  				: OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    resetp_counters     			: in std_logic;
				
	clk_freerun_in					: in std_logic;--can be a clock between 3.125 Mhz to 125 Mhz
				
	clock_SR0						: OUT STD_LOGIC;	
	SR0_WEN  						: OUT STD_LOGIC;	
	SR0_UCTRL						: OUT STD_LOGIC;
	SR0_DATA 						: OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
	SR0_LFF  						: IN STD_LOGIC;
			 
    SRI_ref_clkp    				: IN STD_LOGIC;	
    SRI_ref_clkn    				: IN STD_LOGIC;	
	qpll0_REFCLKOUTMONITOR          : OUT STD_LOGIC;
	SRI_gt_rxn_in     				: IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    SRI_gt_rxp_in     				: IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    SRI_gt_txn_out    				: OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    SRI_gt_txp_out    				: OUT STD_LOGIC_VECTOR(0 DOWNTO 0)
);	
end SR_receiver_quadone;

--****************************************************************************
architecture Behavioral of SR_receiver_quadone is
 

signal SRI_ref					: std_logic;
signal qpll_lock_in_cell   		: STD_LOGIC;
signal qpll_reset_cell    		: STD_LOGIC;
signal qpll_reset_int    		: STD_LOGIC;

signal qpll_clkin_cell     		: STD_LOGIC;
signal qpll_ref_clkin_cell 		: STD_LOGIC;
 

signal SR0_control_DTO						: std_logic_vector(63 DOWNTO 0); 
 
signal clock_SR_out                         : std_logic;
signal clock_SR_resetp                      : std_logic;

 
--****************************************************************************
--**********    CODE   START     HERE    *************************************
--****************************************************************************
begin

rst_resync_i0:entity work.resetp_resync
port map(
	aresetp			=> resetp_counters,
	clock			=> clock_SR_out,
					 
	Resetp_sync		=>  clock_SR_resetp
	);
	
--
--Clock reference for SlinkRocket
--		
GTE_clock_i1:if technology /= "GTH_KU" generate
    IBUFDS_GTE4_inst : IBUFDS_GTE4 
    port map (
        O     	=> SRI_ref, -- 1-bit output: Refer to Transceiver User Guide
        --ODIV2 	=> ODIV2, -- 1-bit output: Refer to Transceiver User Guide
        CEB   	=> '0', -- 1-bit input: Refer to Transceiver User Guide 
        I     	=> SRI_ref_clkp, -- 1-bit input: Refer to Transceiver User Guide 
        IB 		=> SRI_ref_clkn -- 1-bit input: Refer to Transceiver User Guide
    ); 
end generate;
GTE_clock_i2:if technology = "GTH_KU" generate
   IBUFDS_GTE3_inst : IBUFDS_GTE3
   port map (
       O     	=> SRI_ref, -- 1-bit output: Refer to Transceiver User Guide
       --ODIV2 	=> ODIV2, -- 1-bit output: Refer to Transceiver User Guide
       CEB   	=> '0', -- 1-bit input: Refer to Transceiver User Guide 
       I     	=> SRI_ref_clkp, -- 1-bit input: Refer to Transceiver User Guide 
       IB 		=> SRI_ref_clkn -- 1-bit input: Refer to Transceiver User Guide
   ); 
end generate;

qpll_reset_cell  <= qpll_reset_int; 	
	
QPLLi1:entity work.QPLL_wrapper_select 
 Generic map (throughput			=> throughput,
			  ref_clock				=> ref_clock,
			  technology			=> technology
				) 
 Port map( 
	gtrefclk00_in			=> SRI_ref, 
	gtrefclk01_in       	=> '0', 
	qpll0reset_in       	=> qpll_reset_cell, 
	qpll1reset_in       	=> '0', 
							 
	qpll0lock_out       	=> qpll_lock_out, 
	qpll0outclk_out     	=> qpll_clkin, 
	qpll0outrefclk_out  	=> qpll_ref_clkin  
    qpll0_REFCLKOUTMONITOR  => qpll0_REFCLKOUTMONITOR,
							 
	-- qpll1lock_out       	=> , 
	-- qpll1outclk_out     	=> , 
	-- qpll1outrefclk_out  	=>  ,
	-- qpll1_REFCLKOUTMONITOR=> 
 );   
        
	
SR1_receiver:entity work.SR_Receiver  
generic map(SR_offset           => SR0_offset,
			Clock_source		=> "Master",
            throughput			=> throughput,
			ref_clock			=> ref_clock,
			technology			=> technology 
				)
 Port map(
    usr_clk           			=> usr_clk       , 
    rst_usr_clk_n     			=> rst_usr_clk_n , 
    usr_func_wr       			=> usr_func_wr   , 
    usr_wen           			=> usr_wen       , 
    usr_data_wr       			=> usr_data_wr   , 
    usr_func_rd       			=> usr_func_rd   , 
    usr_rden          			=> usr_rden      , 
    usr_dto_receiver  			=> SR0_control_DTO, 
    resetp_counters     		=> resetp_counters, 
									 
	clk_freerun_in				=> clk_freerun_in, 
									 
	clock_SR					=> clock_SR0, 
	SR_WEN  					=> SR0_WEN  , 
	SR_UCTRL					=> SR0_UCTRL, 
	SR_DATA 					=> SR0_DATA , 
	SR_LFF  					=> SR0_LFF  , 
 	
    qpll_lock_in                => qpll_lock_in_cell	, 
    qpll_reset_out              => qpll_reset_int		, 
    qpll_clkin                  => qpll_clkin_cell		, 
    qpll_ref_clkin              => qpll_ref_clkin_cell	, 
    
	SRI_gt_rxn_in     			=> SRI_gt_rxn_in(0) , 
    SRI_gt_rxp_in     			=> SRI_gt_rxp_in(0) , 
    SRI_gt_txn_out    			=> SRI_gt_txn_out(0), 
    SRI_gt_txp_out    			=> SRI_gt_txp_out(0)  
);	
   

usr_dto_receiver	<= SR0_control_DTO;   
end Behavioral;
