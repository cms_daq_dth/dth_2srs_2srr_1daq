------------------------------------------------------
-- Receive packet
--
--  Ver 1.00
--
-- Dominique Gigi Feb 2012
------------------------------------------------------
--   
--  
-- 
--  
------------------------------------------------------
LIBRARY ieee;
 
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;
 
entity rcv_pckt is

port (
	resetn			: in std_logic;
	clock			: in std_logic;
	
	--data from SERDES
	PCKT_datai			: in std_logic_vector(127 downto 0); --- data  
	PCKT_Start			: in std_logic;						-- active high valid the start of the packet
	PCKT_End			: in std_logic;						-- active high valid the end of packet
	PCKT_wen			: in std_logic;						-- active high valid a word
	
	free_place	    	: in std_logic;

--   Fragment Data	
	BLK_new		    	: out std_logic;
	BLK_seqnb 		    : out std_logic_vector(31 downto 0);
	BLK_lenght		    : out std_logic_vector(15 downto 0);
	BLK_desc	    	: out std_logic_vector(15 downto 0);

	BLK_datao			: out std_logic_vector(127 downto 0);	-- data from Controller
	BLK_wr_ena		    : out std_logic; 
	BLK_valid_packet    : out std_logic; -- send a ack if packet ended and CRC OK
	BLK_end_blk		    : out std_logic;
	
--   Return data on read command	
	cmd_data_rtn    : out std_logic_vector(63 downto 0);	-- command from Controller	
	
	ack_packet_o	: out std_logic;
	init_packet_o	: out std_logic;
	cnt_pckt		: out std_logic;
	cnt_bad_p	    : out std_logic;
	back_pres	    : out std_logic
 	);
	
end rcv_pckt;

architecture behavioral of rcv_pckt is

component Slink_packet_CRC16_D128b is
  -- polynomial: x^16 + x^15 + x^2 + 1
  -- data width: 128
  -- convention: the first serial bit is Data[127]
 port (
		clock      	: IN  STD_LOGIC; 
		resetp     	: IN  STD_LOGIC; 
		data       	: IN  STD_LOGIC_VECTOR(127 DOWNTO 0); 
		data_valid 	: IN  STD_LOGIC; 
		eoc        	: IN  STD_LOGIC; 
	
		crc        	: OUT STD_LOGIC_VECTOR(15 DOWNTO 0); 
		crc_valid  	: OUT STD_LOGIC 
		);
end component;
 

signal pipe_a				    : std_logic_vector(127 downto 0);
signal data_crc_cmp			    : std_logic_vector(127 downto 0);
signal pipe_a_SPK			    : std_logic;
signal pipe_a_EPK			    : std_logic;
signal pipe_a_wen			    : std_logic; 

signal pipe_b				    : std_logic_vector(127 downto 0);
signal pipe_b_SPK			    : std_logic; 
signal pipe_b_wen			    : std_logic;

signal rst_crc_engine			: std_logic;			
signal EndOfCRC					: std_logic;			
	
signal low_data			        : std_logic_vector(31 downto 0); 
signal data_reg			        : std_logic_vector(127 downto 0); 
signal ena_wr_dt			    : std_logic;
	
signal ena_dt_reg			    : std_logic;  
signal mem_free			        : std_logic;
	
signal data_cmd_returned		: std_logic_vector(63 downto 0); 
signal block_desc_reg	        : std_logic_vector(15 downto 0):= x"0000"; 
signal ena_ack_reg		        : std_logic;	
signal ena_init_reg		        : std_logic;	
	
signal seqnb_reg			    : std_logic_vector(30 downto 0);

signal length_reg			    : std_logic_vector(15 downto 0);
		
signal cmp_crc_val				: std_logic_vector(3 downto 0);
signal cmp_crc				    : std_logic_vector(1 downto 0); 	
signal Val_data_to_CRC			: std_logic;
		
--signal eoc					: std_logic; 	
signal crc_val				    : std_logic_vector(15 downto 0);
signal crc_valid			    : std_logic; 	
signal crc_to_be_check	        : std_logic_vector(15 downto 0);
signal end_check			    : std_logic;
signal data_pckt			    : std_logic;
signal ack_pckt			        : std_logic;
signal init_pckt			    : std_logic;
signal dt_burst			        : std_logic;
	
signal BAD_Pckt			        : std_logic;


attribute mark_debug : string; 
 -- attribute mark_debug of crc_to_be_check			: signal is "true"; 
 -- attribute mark_debug of crc_val 			: signal is "true"; 
 -- attribute mark_debug of crc_valid			: signal is "true"; 
 -- attribute mark_debug of BAD_Pckt			: signal is "true"; 
 -- attribute mark_debug of PCKT_Start			: signal is "true"; 
 -- attribute mark_debug of PCKT_End			: signal is "true"; 
 -- attribute mark_debug of PCKT_wen			: signal is "true"; 
 
--*******************************************************
--**************  BEGIN  ********************************
--*******************************************************
begin
 
process(resetn,clock)   
begin
	if resetn = '0' then
		pipe_a					<= (others => '0');
		pipe_a_SPK				<= '0';
		pipe_a_EPK				<= '0';
		pipe_a_wen				<= '0';
		pipe_b_SPK				<= '0'; 
		pipe_b_wen  			<= '0';
		pipe_b					<= (others => '0');
	elsif rising_edge(clock)  then		
		if PCKT_wen = '1' then
			-- first pipe level of the data and START/STOP signals
			pipe_a					<= PCKT_datai;		 
			pipe_a_SPK				<= PCKT_Start;	--  Begining of the packet
			pipe_a_EPK				<= PCKT_End; 	-- last word is only CRC of the packet
		end if;
		
		-- create a signal "envelop" of the packet (no hole in the packet)
		if PCKT_wen = '1' then		
			pipe_a_wen				<= '1';
		elsif (pipe_a_wen = '1' and pipe_a_EPK = '1') then 	
			pipe_a_wen				<= '0';
		end if;
			
		-- second pipe	
		pipe_b_wen					<= '0';
		if PCKT_wen = '1' or (pipe_a_wen = '1' and pipe_a_EPK = '1' and data_pckt = '0') then 	
			pipe_b(63 downto 0)		<= pipe_a(127 downto 64);
			pipe_b(127 downto 64)	<= PCKT_datai(63 downto 0);
			pipe_b_SPK				<= pipe_a_SPK;
			pipe_b_wen  			<= pipe_a_wen;
		end if;
	end if;
end process;


--////////////////////////////////////////////////////////////////////////
--   CRC calcul
process(clock)  -- pipe data and enable to calcutate the CRC
begin
	if rising_edge(clock)  then
		--delay the enable to calculate the CRC ( this delay is the same as the data
		cmp_crc_val(2 downto 1)			<= cmp_crc_val(1 downto 0); 
		cmp_crc_val(0)					<= PCKT_wen;
		
		data_crc_cmp					<= pipe_a;
		if pipe_a_EPK = '1' then		-- for the last data mask the CRC 
			data_crc_cmp(79 downto 64)	<= (others => '0');
		end if;
	end if;
end process;

process(resetn,clock) -- generate the reset for the CRC engine and an "envelop" (START <---> STOP packet)
begin
	if resetn = '0' then	
		cmp_crc 			<= (others => '0');
		rst_crc_engine		<= '1';
	elsif rising_edge(clock) then
		cmp_crc(1)			<= cmp_crc(0);
		
		rst_crc_engine		<= '0';
		if	 	PCKT_wen = '1' and PCKT_Start = '1' then	
			rst_crc_engine	<= '1';
			cmp_crc(0)		<= '1';
		elsif	pipe_a_wen = '1' and pipe_a_EPK = '1' then
			cmp_crc(0)		<= '0';
		end if;
	end if;
end process;

-- latch the CRC received in the packet
-- specifies the end of the packet
process(clock)
begin
	if rising_edge(clock) then
		EndOfCRC				<= '0';
		if 	pipe_a_EPK = '1' and pipe_a_wen = '1' then
			EndOfCRC			<= '1';
			crc_to_be_check	 	<= pipe_a(79 downto 64);-- CRC from packet
		end if;
	end if;
end process;

 
----
--  instantiation to engine to calculate the CRC
Val_data_to_CRC		<= '1' when   cmp_crc(1) = '1' and cmp_crc_val(1) = '1' else '0';

CRC_generate:Slink_packet_CRC16_D128b 
PORT MAP(           
		clock      	=> clock,
		resetp     	=> rst_crc_engine,
		data       	=> data_crc_cmp,
		data_valid 	=> Val_data_to_CRC,
		eoc        	=> EndOfCRC,
		crc        	=> crc_val,
		crc_valid  	=> crc_valid		
       );

--////////////////////////////////////////////////////////////

-- recover the values from the packet
-- Sequence#,
-- Ack nit,
--Status,....
process(resetn,clock)
begin
	if resetn = '0' then
		data_pckt		<= '0';
		ack_pckt		<= '0';
		init_pckt 		<= '0';
		mem_free		<= '0';
	elsif rising_edge(clock) then
		if pipe_a_SPK = '1' and pipe_a_wen = '1' then
			ack_pckt 			<= pipe_a(63);					-- indicates if the packet is acknowledge (Seq number bit31 = '1')

			if pipe_a(62 downto 32) = "0000000000000000000000000000000" then	-- if first word is 00000 INIT packet
				init_pckt 		<= '1';
			elsif pipe_a(63) = '0' then											-- else a data packet (fROM FED or return data from register/command)
				data_pckt 		<= '1';
			end if;
			
			if free_place = '1' then							-- if freeplace to store the data we create mem_free= '1'
				mem_free		<= '1';
			end if;
		elsif end_check = '1' then							-- at the end of the packet we reset all signals
			data_pckt			<= '0';
			ack_pckt			<= '0';
			init_pckt 			<= '0';
			mem_free			<= '0';
		end if;
	end if;
end process;

-- store values from packet (at the start of the packet
process(clock)
begin
	if rising_edge(clock) then
		if pipe_a_SPK = '1' and pipe_a_wen = '1' then
			seqnb_reg 			<= pipe_a(62 downto 32);
			block_desc_reg		<= pipe_a(31 downto 16);
			length_reg			<= pipe_a(15 downto 00);
			-- use only when return data from command (acknowledge)
			data_cmd_returned	<= pipe_a(127 downto 64);
		end if;
	end if;
end process;

-- Here we control if 
--     there is free place we specify this new packet as a new block of data
--     there is not free place , we generate a backpressure (no acknowledge , the packet will be retransmit after timeout on the sender side of the link")
process(resetn,clock)
begin
	if resetn = '0'  then
		dt_burst 	<= '0';
		BLK_new		<= '0';
		back_Pres 	<= '0';
	elsif rising_edge(clock) then
		BLK_new		<= '0';
		back_Pres 	<= '0';
		if     pipe_a(63) = '0' and pipe_a(62 downto 32) /= "0000000000000000000000000000000" and pipe_a_SPK = '1' and pipe_a_wen = '1' and free_place = '1' then
			BLK_new <= '1';
		elsif  data_pckt = '1' and pipe_b_SPK = '1' and pipe_b_wen = '1' and mem_free = '0' then
			back_Pres <= '1';
		end if;
		
		-- accept the packet only if there is a free place
		-- the validation of the packet CRC correct will be done later (we nevertheless record data, CRC can be checked only at the end of the pakcet)
		if     data_pckt = '1' and mem_free = '1' then 
			dt_burst <= pipe_b_wen;
		end if;
	end if;
end process;

--pipe (delay) the data like the 'wen'  (pipe_b_wen)
process(clock)
begin
	if rising_edge(clock) then
		data_reg	 <= pipe_b;
	end if;
end process;

--  validate or unvalidate the packet according the CRC check
process(resetn,clock)
begin
	if resetn = '0' then
		ena_dt_reg 		<= '0';
		ena_ack_reg 	<= '0';
		ena_init_reg	<= '0';
		end_check 		<= '0';
		BAD_Pckt 		<= '0';
		cnt_pckt		<= '0';
	elsif rising_edge(clock) then
		ena_dt_reg 		<= '0';
		ena_ack_reg 	<= '0';
		end_check 		<= '0';
		ena_init_reg	<= '0';
		BAD_Pckt 		<= '0';
		cnt_pckt		<= '0';
		if crc_valid = '1'  then 
			end_check 				<= '1';
			if crc_val = crc_to_be_check then
				cnt_pckt			<= '1';
				if ack_pckt = '1' then				-- received a ACknowledge packet
					ena_ack_reg 	<= '1';
				elsif init_pckt = '1' then			-- received a INIT  packet
					ena_init_reg	<= '1';
				elsif mem_free = '1' then			-- received a Data packet (if memory has a free place to store it
					ena_dt_reg 		<= '1';
				end if;
			else
				BAD_Pckt 			<= '1';			-- indicates a wrong CRC in the packet
			end if;
		end if;
	end if;
end process;

 
BLK_end_blk					<= end_check;			-- indicates the end of the packet
init_packet_o			    <= ena_init_reg;		-- indicates a INIT packet
ack_packet_o			    <= ena_ack_reg;			-- indicates an ACK packet
BLK_valid_packet 			<= ena_dt_reg;			-- indicates a DATA packet
cmd_data_rtn			    <= data_cmd_returned;	-- data return following a read command
BLK_seqnb(30 downto 0)		<= seqnb_reg;			-- sequence number sent in the packet
BLK_seqnb(31)				<= '0';
	
BLK_lenght					<= length_reg;			-- indicates the length of data in the received packet

 
BLK_datao					<= data_reg;			-- data received
BLK_desc				    <= block_desc_reg;		-- packet description (START/STOP of the fragment)
BLK_wr_ena					<= dt_burst;			-- wen of data
cnt_BAD_P				    <= BAD_Pckt;			-- bad packet for statistic

end behavioral;
 
 