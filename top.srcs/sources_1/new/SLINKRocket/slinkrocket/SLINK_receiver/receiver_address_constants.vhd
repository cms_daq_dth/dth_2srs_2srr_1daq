library IEEE;
library WORK;
--use IEEE.std_logic_1164.all;
--use IEEE.std_logic_arith.all;

--use ieee.numeric_std.all;

--this address should be multiply by 4 to be byte aligned
-- the selection chip are 16#0 ...16#3FFF   64 bit addressing

-- the receiver needs currently 8 addresses

package address_table is

--################################################################################################################################
 

        constant        Slinkrocket_access_command      : integer := 16#0000#; --x2400
                                                                               -- execute the command (the data should be set first)
                                                                               -- send the offset
                                                                               --read b0 access done
                                                                               --read b1 access good
        constant        Slinkrocket_access_data         : integer := 16#0001#; --x2408
                                                                               --write data to write    
                                                                               --read  data received
        constant        Slinkrocket_resync_request      : integer := 16#0002#; --x2410
                
        constant        SLINKRocket_rcv_serdes_control                  : integer := 16#0003#;          
        constant        SLINKRocket_rcv_serdes_DRP_control              : integer := 16#0004#;          
        constant        SLINKRocket_rcv_serdes_DRP_data_returned        : integer := 16#0005#;  
        constant        SLINKRocket_rcv_serdes_status                   : integer := 16#0006#;  
        constant        SLINKRocket_rcv_serdes_analog                   : integer := 16#0007#; 
        constant        SLINKRocket_FEDx_Fragment_rcv                   : integer := 16#0008#; 
        constant        SLINKRocket_FEDx_Fragment_rcv_wth_error         : integer := 16#0009#;    
		constant		SLINKRocket_rcv_serdes_loopback					: integer := 16#000C#;
		constant		SLINKRocket_serdes_PRBS_test					: integer := 16#000D#;
                
end package address_table;
