------------------------------------------------------
-- Receive memory block 
--
--  Ver 1.00
--
-- Dominique Gigi March 2012
------------------------------------------------------
--   
--  
-- 
--  
------------------------------------------------------
LIBRARY ieee;
 
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
 
entity rcv_memory_blocks is
generic (UltraRam_mem       : boolean := TRUE );
port (

	resetn					: in std_logic;
	clock					: in std_logic;
			
	init					: in std_logic;						-- Init reinitialize the SEQ number
	new_blk				    : in std_logic;						-- inform that a new block is coming
	seq_nb				    : in std_logic_vector(31 downto 0); -- only lower 31 bits are used
	blk_desc				: in std_logic_vector(15 downto 0);
	blk_size				: in std_logic_vector(15 downto 0);
	data_in				    : in std_logic_vector(127 downto 0);
	wen					    : in std_logic;
	valid_blk			    : in std_logic; 					-- this pulse is created when the CRC is correct
	end_blk				    : in std_logic;
	free_block			    : out std_logic;
			
	ack_req				    : out std_logic;
	seq_to_be_ack		    : out std_logic_vector(31 downto 0);
	
	rd_data				    : out std_logic;
	Uctrl_out				: out std_logic;
	DATA_out				: out std_logic_vector(127 downto 0);
	LFF					    : in std_logic;
	CUR_seq_num			    : out std_logic_vector(31 downto 0)
	
);
end rcv_memory_blocks;

architecture behavioral of rcv_memory_blocks is

component Memory is
port 	( 
		clock		: in std_logic;
		addr_w		: in std_logic_vector(9 downto 0);
		data_w		: in std_logic_vector(127 downto 0);
		wen			: in std_logic;
		addr_r		: in std_logic_vector(9 downto 0);
		ren			: in std_logic;
		data_r		: out std_logic_vector(127 downto 0)
	);
end component;

component Memory_UR is
port 	( 
		clock		: in std_logic;
		addr_w		: in std_logic_vector(9 downto 0);
		data_w		: in std_logic_vector(127 downto 0);
		wen			: in std_logic;
		addr_r		: in std_logic_vector(9 downto 0);
		ren			: in std_logic;
		data_r		: out std_logic_vector(127 downto 0)
	);
end component;

type  descript is
	record
		seq_num 	:std_logic_vector(31 downto 0);	-- sequence number of the block
		start	 	:std_logic; 					-- specify the start of the block
		stop		:std_logic;						-- specify the end of the block
		length  	:std_logic_vector(15 downto 0); -- wc of valid data in the block
		blk_used	:std_logic;						-- specify if the block is used
		read_run	:std_logic;
	end record;
signal a,b,c,d : descript;	

SIGNAL seq_nm_cnt			: std_logic_vector(31 downto 0);
SIGNAL memory_addr_w		: std_logic_vector(7 downto 0);
SIGNAL memory_addr_r		: std_logic_vector(7 downto 0);
SIGNAL mem_blk_w			: std_logic_vector(1 downto 0);
SIGNAL mem_blk_r			: std_logic_vector(1 downto 0);
SIGNAL size_cnt				: std_logic_vector(15 downto 0);
SIGNAL delay_new_b			: std_logic;

-- The block can be accpeted because the seq number is correct 
--		but the block can be memorized already => give only a acknowledge if the block is correct.
--			accept_seqnb is if the block should be write and ack
--			ack_block if the block has only to be acknowledge
SIGNAL accept_seqnb		    : std_logic;
SIGNAL ack_block			: std_logic;
 
--SIGNAL read_block_run	    : std_logic;
SIGNAL next_seq_nb		    : std_logic;

SIGNAL uctrl_reg			: std_logic_vector(2 downto 0);
SIGNAL data_out_reg		    : std_logic_vector(127 downto 0);
SIGNAL data_mem_cell		: std_logic_vector(127 downto 0);
SIGNAL rd_reg				: std_logic_vector(2 downto 0);
SIGNAL end_rd_blk			: std_logic;

SIGNAL sta_stp				: std_logic_vector(1 downto 0);
SIGNAL ack_req_cell		    : std_logic;
SIGNAL tmp_seq_nb			: std_logic_vector(31 downto 0);
SIGNAL tmp_seq_nb2		    : std_logic_vector(31 downto 0);

--****************************************************************************************************
--**********************************<<     BEGIN     >>***********************************************
--****************************************************************************************************
begin

-- sequence number counter 
-- this value is initialised @ 1  after an init packet received
-- this value is reset @ 0 
-- this value is increase by 1 after each new packet received (correctly  no wrong CRC)
process(resetn,clock)
begin
	if resetn = '0' then
		seq_nm_cnt			<= (others => '0');
	elsif rising_edge(clock) then
		if init = '1' then
			seq_nm_cnt 		<= x"00000001";
		elsif next_seq_nb = '1' then
			if seq_nm_cnt = x"7FFFFFFF" then
				seq_nm_cnt 	<= x"00000001"; 
			else			
				seq_nm_cnt 	<= seq_nm_cnt + "1";
			end if;
		end if;
	end if;
end process;

CUR_seq_num <= seq_nm_cnt;
---***********************************************
--***************<<  Memory blocks  >>************
---***********************************************
-- this memory is divided in 4 blocks . 
-- few registers maintains the content/caracteristics of the block a./ b./ c./ d.
BRAM:if not(UltraRam_mem) generate
    mem_blk:Memory  
    port map	( 
            clock						=> clock,
            addr_w(9 downto 8)			=> mem_blk_w,
            addr_w(7 downto 0)			=> memory_addr_w,
            data_w						=> data_in,
            wen							=> wen,
            addr_r(9 downto 8)			=> mem_blk_r,
            addr_r(7 downto 0)			=> memory_addr_r,
            ren							=> '1',
            data_r						=> data_mem_cell
        );
end generate;

ULTRARAM:if UltraRam_mem generate
    mem_blk:Memory_UR  
    port map	( 
            clock						=> clock,
            addr_w(9 downto 8)			=> mem_blk_w,
            addr_w(7 downto 0)			=> memory_addr_w,
            data_w						=> data_in,
            wen							=> wen,
            addr_r(9 downto 8)			=> mem_blk_r,
            addr_r(7 downto 0)			=> memory_addr_r,
            ren							=> '1',
            data_r						=> data_mem_cell
        );
end generate;


-- select the block to be used to record block of data
process(resetn,clock)
begin
	if resetn = '0' then 
		mem_blk_w 			<= (others => '0');
		free_block 			<= '1';
	elsif rising_edge(clock) then
		if new_blk = '1' then
			if 		a.blk_used = '0' then
				mem_blk_w 	<= "00";
			elsif 	b.blk_used = '0' then
				mem_blk_w 	<= "01";
			elsif 	c.blk_used = '0' then
				mem_blk_w 	<= "10";
			else--if 	d.blk_used = '0' then
				mem_blk_w 	<= "11";
			end if;
		end if;
		
		if a.blk_used = '1' and b.blk_used = '1' and c.blk_used = '1' and d.blk_used = '1' then
			free_block 		<= '0';
		else
			free_block 		<= '1';
		end if;
	end if;
end process;

process(clock)
begin
	if rising_edge(clock) then
		delay_new_b 		<= new_blk;
	end if;
end process;

-- address counter for write access in memory block
 process(resetn,clock)
 begin
	if resetn = '0' then
		memory_addr_w 		<= (others => '0');
	elsif rising_edge(clock) then
		if new_blk = '1' then
			memory_addr_w 	<= (others => '0');
		elsif wen = '1' then
			memory_addr_w 	<= memory_addr_w + "1";
		end if;
	end if;
 end process;

-- latch descriptors
process(clock)
begin
	if rising_edge(clock) then
		if delay_new_b = '1' then
			if 		mem_blk_w = "00" then
				a.seq_num  	<= seq_nb;
				a.start  	<= blk_desc(0);
				a.stop	 	<= blk_desc(1);
				a.length   	<= blk_size;
			elsif 	mem_blk_w = "01" then
				b.seq_num  	<= seq_nb;
				b.start  	<= blk_desc(0);
				b.stop	 	<= blk_desc(1);
				b.length   	<= blk_size;
			elsif 	mem_blk_w = "10" then
				c.seq_num  	<= seq_nb;
				c.start  	<= blk_desc(0);
				c.stop	 	<= blk_desc(1);
				c.length   	<= blk_size;
			else--if 	mem_blk_w = "11" then
				d.seq_num  	<= seq_nb;
				d.start  	<= blk_desc(0);
				d.stop	 	<= blk_desc(1);
				d.length   	<= blk_size;
			end if;
		end if;
	end if;
end process;

-- check if we have to accept and/or acknowledge the packet received  (in function of its sequence number
process(resetn,clock)
begin
	if resetn = '0' then
		accept_seqnb 	<= '0';
		ack_block		<= '0';
	elsif rising_edge(clock) then
		if new_blk = '1' then 
			-- when a new block we should verify few things
			-- is the packet with the same seq number already recorded (in case of retransmit)
			-- the sequence number received should not exceed the current sequence number + 4 
			--						and not lower
			-- then we can accept the pakcet in the Memoeyr block
			 if (  (  seq_nb(30 downto 0) < tmp_seq_nb(30 downto 0)  AND  seq_nb(30 downto 0) >= seq_nm_cnt(30 downto 0)  AND   tmp_seq_nb(31) = '0'   ) 
				 or( (seq_nb(30 downto 0) < tmp_seq_nb(30 downto 0)  OR   seq_nb(30 downto 0) >= seq_nm_cnt(30 downto 0)) AND   tmp_seq_nb(31) = '1' ) ) AND 
				  	not(seq_nb = a.seq_num and a.blk_used = '1') AND 
					not(seq_nb = b.seq_num and b.blk_used = '1') AND 
					not(seq_nb = c.seq_num and c.blk_used = '1') AND 
				    not(seq_nb = d.seq_num and d.blk_used = '1')		then
						accept_seqnb 	<= '1';
			end if;
			
			-- we ack if seq number between current and +4 
			--or if it is a old packet (already recorded in the Memory Block
			 if (  seq_nb(30 downto 0) < tmp_seq_nb(30 downto 0)  AND  seq_nb(30 downto 0) > tmp_seq_nb2(30 downto 0)  AND  tmp_seq_nb(31)  = tmp_seq_nb2(31) )
			 or ( (seq_nb(30 downto 0) < tmp_seq_nb(30 downto 0)  OR   seq_nb(30 downto 0) > tmp_seq_nb2(30 downto 0)) AND  tmp_seq_nb(31) /= tmp_seq_nb2(31) ) then
				ack_block 				<= '1';
			end if;
		--end if;
		
		elsif  end_blk = '1' then
			accept_seqnb 	<= '0';
			ack_block		<= '0';
		end if;
 
	end if;
end process;

-- computer Seq_num + 4   and -100
process(clock)
begin
	if rising_edge(clock) then
		tmp_seq_nb <= seq_nm_cnt + x"00000004";
		tmp_seq_nb2<= seq_nm_cnt - x"00000100";
	end if;
end process;


-- manage the use of the 4 block of memory
-- and the acknowledge request signal
process(resetn,clock)
begin
	if resetn = '0' then
		a.blk_used 	<= '0';
		b.blk_used 	<= '0';
		c.blk_used 	<= '0';
		d.blk_used 	<= '0';
		ack_req_cell<= '0';
	elsif rising_edge(clock) then
		if valid_blk = '1' and accept_seqnb = '1' then
			
			if 		mem_blk_w = "00" then
				a.blk_used <= '1';
			elsif 	mem_blk_w = "01" then
				b.blk_used <= '1';
			elsif 	mem_blk_w = "10" then
				c.blk_used <= '1';
			elsif 	mem_blk_w = "11" then
				d.blk_used <= '1';
			end if;
		end if;
		if end_rd_blk = '1' then
			if 		a.read_run = '1' then
				a.blk_used <= '0';
			elsif 	b.read_run = '1' then
				b.blk_used <= '0';
			elsif 	c.read_run = '1' then
				c.blk_used <= '0';
			elsif 	d.read_run = '1' then
				d.blk_used <= '0';
			end if;
		end if;
		
		ack_req_cell		<= '0';
		if valid_blk = '1' and ack_block = '1' then
			ack_req_cell	<= '1';
		end if;
		
	end if;
end process;

process(clock)
begin
	if rising_edge(clock) then
		if  valid_blk = '1' then
			seq_to_be_ack	<=	seq_nb;
		end if;
	end if;
end process;

ack_req	<= ack_req_cell;


--////////////////////////////////////////////////////////////////////////////////
--*//////////////////////////////////////////////////////////////////////////////
--*********** read out the packet one after the other in the sequence number order

process(resetn,clock)
begin
if resetn = '0' then
	a.read_run		<= '0';
	b.read_run		<= '0';
	c.read_run		<= '0';
	d.read_run		<= '0';
	next_seq_nb	  	<= '0';
elsif rising_edge(clock) then
	next_seq_nb	  	<= '0';
	-- check if there is a block recorded (used by a fragment or a part of a fragment)
	-- if yes we will read it out in the sequence number order
	if a.read_run = '0' and b.read_run = '0' and c.read_run = '0' and d.read_run = '0' then
		if 		a.blk_used = '1' and (a.seq_num = seq_nm_cnt) and a.read_run = '0' then
			next_seq_nb	  	<= '1';
			a.read_run		<= '1';
		elsif 	b.blk_used = '1' and (b.seq_num = seq_nm_cnt) and b.read_run = '0' then
			next_seq_nb	  	<= '1';
			b.read_run		<= '1';
		elsif 	c.blk_used = '1' and (c.seq_num = seq_nm_cnt) and c.read_run = '0' then
			next_seq_nb	  	<= '1';
			c.read_run		<= '1';
		elsif 	d.blk_used = '1' and (d.seq_num = seq_nm_cnt) and d.read_run = '0' then
			next_seq_nb	  	<= '1';
			d.read_run		<= '1';
		end if;
	end if;
	if end_rd_blk = '1' and rd_reg(0) = '1' then
		a.read_run		<= '0';
		b.read_run		<= '0';
		c.read_run		<= '0';
		d.read_run		<= '0';
	end if;
end if;
end process;

-- select the description of the memory block to be read
process(resetn,clock)
begin
	if resetn = '0' then 
		mem_blk_r 	<= (others => '0');
		size_cnt	<= (others => '0');
		sta_stp(0)	<= '0';
		sta_stp(1)	<= '0';
	elsif rising_edge(clock) then
		if next_seq_nb = '1' then
			if 		a.read_run = '1' then
				mem_blk_r 	<= "00";
				size_cnt 	<= a.length;
				sta_stp(0)	<= a.start;
				sta_stp(1)	<= a.stop;
			elsif 	b.read_run = '1' then
				mem_blk_r 	<= "01";
				size_cnt 	<= b.length;
				sta_stp(0)	<= b.start;
				sta_stp(1)	<= b.stop;
			elsif 	c.read_run = '1' then
				mem_blk_r 	<= "10";
				size_cnt 	<= c.length;
				sta_stp(0)	<= c.start;
				sta_stp(1)	<= c.stop;
			else--if 	d.blk_used = '1' then
				mem_blk_r 	<= "11";
				size_cnt 	<= d.length;
				sta_stp(0)	<= d.start;
				sta_stp(1)	<= d.stop;
			end if;
		end if;
		
		if rd_reg(0) = '1' then
			size_cnt <= size_cnt - "1";
		end if;
	end if;
end process;
		

-- address counter for read access Memory Block
 process(resetn,clock)
 begin
	if resetn = '0' then
		memory_addr_r <= (others => '0');
	elsif rising_edge(clock) then
		if next_seq_nb = '1' then
			memory_addr_r <= (others => '0');
		elsif rd_reg(0) = '1' then
			memory_addr_r <= memory_addr_r + "1";
		end if;
	end if;
 end process;
 
 --regenerate the UCTRL signal 
 -- generate the read signal (read the memory block
 process(resetn,clock)
 begin
	 if resetn = '0' then
		uctrl_reg 	<= (others => '0');
		rd_reg		<= (others => '0');
	 elsif rising_edge(clock) then
		uctrl_reg(2 downto 1) 	<= uctrl_reg(1 downto 0);
		
		uctrl_reg(0) <= '0';
		if 		sta_stp(0) = '1' and memory_addr_r = x"0000" and rd_reg(0) = '1' then
			uctrl_reg(0) 	<= '1';
		elsif 	sta_stp(1) = '1' and size_cnt = x"0001" 	 and rd_reg(0) = '1' then
			uctrl_reg(0) 	<= '1';
		end if;
		
		rd_reg(2 downto 1)		<= rd_reg(1 downto 0);
		
		if ( a.read_run = '1' or b.read_run = '1' or c.read_run = '1' or d.read_run = '1' ) and rd_reg(0) = '0' and LFF = '0' then
			rd_reg(0)	<= '1';
		elsif size_cnt = x"0001" then
			rd_reg(0)	<= '0';
			
		end if;
	 end if;
 end process;
 
end_rd_blk	<= '1' when size_cnt = x"0001" and rd_reg(0) = '1' else '0';
 
 process(clock)
 begin
	 if rising_edge(clock) then
		data_out_reg <= data_mem_cell;
	 end if;
 end process;

UCTRL_out		<= uctrl_reg(1); 
DATA_out	    <= data_out_reg;
rd_data 		<= rd_reg(2);
 
end behavioral;



