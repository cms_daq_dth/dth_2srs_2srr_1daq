set_false_path -from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME =~ */*serdes_receiver*/drp_di_reg[*]}]]
set_false_path -from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME =~ */*serdes_receiver*/drp_addr_reg[*]}]]
set_false_path -from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME =~ */*serdes_receiver*/drp_we*[0]}]]
set_false_path -from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME =~ */*serdes_receiver*/drp_rd*[0]}]]

set_false_path -from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME =~ */*serdes_receiver*/drp_data*[*]}]]
set_false_path -from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME =~ */*serdes_receiver*/drp_we_done*}]] -to [get_pins -filter {REF_PIN_NAME == D} -of [get_cells -hierarchical -filter {NAME =~ */*serdes_receiver_i1/control_dto_rg_reg[17]}]]
set_false_path -from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME =~ */*serdes_receiver*/drp_rd_done*}]] -to [get_pins -filter {REF_PIN_NAME == D} -of [get_cells -hierarchical -filter {NAME =~ */*serdes_receiver_i1/control_dto_rg_reg[16]}]]

set_false_path -from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME =~ */*serdes_receiver*/Srds_loopback_in*}]]
set_false_path -from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME =~ */*serdes_receiver*/txdiffctrl_in*}]]
set_false_path -from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME =~ */*serdes_receiver*/txpostcursor_in*}]]
set_false_path -from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME =~ */*serdes_receiver*/txprecursor_in*}]]

set_false_path -from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME =~ */*serdes_receiver*/STATE_link*}]] -to [get_pins -filter {REF_PIN_NAME == D} -of [get_cells -hierarchical -filter {NAME =~ */*serdes_receiver*/resync_sig_i3/reg_async_reg*}]]

set_false_path -to [get_pins -filter {REF_PIN_NAME == D} -of [get_cells -hierarchical -filter {NAME =~ */*serdes_receiver*/control_dto_rg_reg[*]}]]

set_false_path -from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME =~ */Receiver_core*/Manage_command_to_Sender/rd_req_reg}]]
set_false_path -from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME =~ */Receiver_core*/Manage_command_to_Sender/wr_req_reg}]]
set_false_path -from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME =~ */Receiver_core*/Manage_command_to_Sender/req_resync_reg}]]

set_false_path -from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME =~ */Receiver_core*/Manage_command_to_Sender/Cmd_OL_reg[*]}]]
set_false_path -from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME =~ */Receiver_core*/Manage_command_to_Sender/data_ol_reg[*]}]]

set_false_path -from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME =~ */Receiver_core*/Manage_command_to_Sender/seq_num_reg*}]] -to [get_pins -filter {REF_PIN_NAME == D} -of [get_cells -hierarchical -filter {NAME =~ */Receiver_core_i1/Manage_command_to_Sender/seq_num_ck*}]]

set_false_path -from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME =~ */Receiver_core_i1/Manage_command_to_Sender/data_rcv*[*]}]] -to [get_pins -filter {REF_PIN_NAME == D} -of [get_cells -hierarchical -filter {NAME =~ */Receiver_core_i1/Manage_command_to_Sender/USR_dto_rg*[*]}]]

set _xlnx_shared_i0 [get_pins -filter {REF_PIN_NAME == D} -of [get_cells -hierarchical -filter {NAME =~ */usr_dto_receiver_cell_reg[*]}]]
set_false_path -from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME =~ */check_i1/status_cnt_OK_reg[*]}]] -to $_xlnx_shared_i0
set_false_path -from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME =~ */check_i1/status_cnt_KO_reg[*]}]] -to $_xlnx_shared_i0
set_false_path -from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME =~ */Clock_*_SERDES.serdes_receiver_i1/rxprbssel_async_reg[*]}]] -to [get_pins -filter {REF_PIN_NAME == D} -of [get_cells -hierarchical -filter {NAME =~ */Clock_*_SERDES.serdes_receiver_i1/rxprbssel_sync_inter_reg[*]}]]
set_false_path -from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME =~ */Clock_*_SERDES.serdes_receiver_i1/rxprbscntreset_async_reg}]] -to [get_pins -filter {REF_PIN_NAME == D} -of [get_cells -hierarchical -filter {NAME =~ */Clock_*_SERDES.serdes_receiver_i1/rxprbscntreset_sync_inter_reg}]]

set_false_path -from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME =~ */Clock_*_SERDES.serdes_receiver_i1/txprbssel_async_reg[*]}]] -to [get_pins -filter {REF_PIN_NAME == D} -of [get_cells -hierarchical -filter {NAME =~ */Clock_*_SERDES.serdes_receiver_i1/txprbssel_sync_inter_reg[*]}]]

set_false_path -from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME =~ */Clock_*_SERDES.serdes_receiver_i1/rxprbscntreset_async_reg}]] -to [get_pins -filter {REF_PIN_NAME == D} -of [get_cells -hierarchical -filter {NAME =~ */Clock_*_SERDES.serdes_receiver_i1/rxprbscntreset_sync_inter_reg}]]
set_false_path -from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME =~ */Clock_*_SERDES.serdes_receiver_i1/txprbssel_async_reg[1]}]] -to [get_pins -filter {REF_PIN_NAME == D} -of [get_cells -hierarchical -filter {NAME =~ */Clock_*_SERDES.serdes_receiver_i1/STATE_link_reg}]]

set_false_path -from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME =~ */Clock_*_SERDES.serdes_receiver_i1/txprbssel_async_reg[*]}]] -to [get_pins -filter {REF_PIN_NAME == D} -of [get_cells -hierarchical -filter {NAME =~ */Clock_*_SERDES.serdes_receiver_i1/STATE_link_reg}]]

#SERDES
set_false_path -from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME =~ */*serdes_receiver_i1/Rst_hrd_rg*}]]
set_false_path -from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME =~ */*serdes_receiver_i1/Rst_All_rg*}]]


set_max_delay -datapath_only -from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME=~ */*serdes_receiver_i1/txprbssel_async*[*]}]] -to [get_pins -filter {REF_PIN_NAME == D} -of [get_cells -hierarchical -filter {NAME=~ */*serdes_receiver_i1/txprbssel_sync_inter*[*]}]] 3.000

set_max_delay -datapath_only -from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME=~ */*serdes_receiver_i1/txprbssel_async*[*]}]] -to [get_pins -filter {REF_PIN_NAME == D} -of [get_cells -hierarchical -filter {NAME=~ */*serdes_receiver_i1/rxprbssel_sync_rxclk*[*]*}]] 3.000

