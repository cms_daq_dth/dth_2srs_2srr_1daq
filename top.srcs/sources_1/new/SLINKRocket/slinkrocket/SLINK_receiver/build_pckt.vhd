

------------------------------------------------------
-- encapsulate the data/ack/init for Opt Link
--
--  Ver 1.00
--
-- Dominique Gigi March 2012
------------------------------------------------------
--   
--  
-- 
--  
------------------------------------------------------
LIBRARY ieee;
 
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;
 
entity build_pckt is

port (
	resetn_r				: in std_logic;
	resetn_t				: in std_logic;
	clock_t				    : in std_logic;
	clock_r				    : in std_logic;
			
	COMMAND_req				: in std_logic;                         -- indicates that the packet is a data packet
	COMMAND_number			: in std_logic_vector(63 downto 0);	    -- command for command packet
	COMMAND_data			: in std_logic_vector(63 downto 0);		-- datafor command packet
	COMMAND_Seq_num			: in std_logic_vector(31 downto 0);     -- sequence number
	req_ack_pckt			: in std_logic;                         -- indicates that the packet is a acknoldge packet
	status				    : in std_logic_vector(63 downto 0);     -- status data for acknowledge packet
	Descript_Pckt		    : in std_logic_vector(15 downto 0);     -- Descripter packet
	Seq_nb				    : in std_logic_vector(31 downto 0);     -- sequence number
	error_gen			    : in std_logic_vector(1 downto 0);
	send_ack				: out std_logic;
				
	Pckt_datao				: out std_logic_vector(127 downto 0);    -- data  send to SERDES
	Pckt_Start_o			: out std_logic;
	Pckt_End_o			    : out std_logic;
	Pckt_Wen_o			    : out std_logic; 
	ST_START_state			: out std_logic
	);
	
end build_pckt;
architecture behavioral of build_pckt is

 type packet_type is (	ST_START,
						start_of_frame, 
						Data_state,		-- PAD also
						CRC_end_frame,
						wait_crc,
						gap0,
						gap1,
						gap2
						);
signal packet:packet_type;
 
component Slink_packet_CRC16_D128b is
  -- polynomial: x^16 + x^15 + x^2 + 1
  -- data width: 128
  -- convention: the first serial bit is Data[127]
 port (
		clock      	: IN  STD_LOGIC; 
		resetp     	: IN  STD_LOGIC; 
		data       	: IN  STD_LOGIC_VECTOR(127 DOWNTO 0); 
		data_valid 	: IN  STD_LOGIC; 
		eoc        	: IN  STD_LOGIC; 
	
		crc        	: OUT STD_LOGIC_VECTOR(15 DOWNTO 0); 
		crc_valid  	: OUT STD_LOGIC 
		);
end component;
 

component fifo_ack_req
	PORT
	(
		sclrp			: IN STD_LOGIC  := '0';
		data			: IN STD_LOGIC_VECTOR (30 DOWNTO 0);
		rdclk			: IN STD_LOGIC ;
		rdreq			: IN STD_LOGIC ;
		wrclk			: IN STD_LOGIC ;
		wrreq			: IN STD_LOGIC ;
		q				: OUT STD_LOGIC_VECTOR (30 DOWNTO 0);
		rdempty			: OUT STD_LOGIC ;
		wrfull			: OUT STD_LOGIC 
	);
end component;

signal preset				    : std_logic;
signal cmp_crc				    : std_logic;
signal val_crc				    : std_logic_vector(15 downto 0);
signal crc_valid			    : std_logic;
signal end_crc				    : std_logic;
signal sqnb_mem_ack		        : std_logic_vector(31 downto 0);
signal sqnb_mem_cmd		        : std_logic_vector(31 downto 0);
--signal len_mem				: std_logic_vector(15 downto 0);
signal data_0				    : std_logic_vector(31 downto 0);	-- command/data or status
signal data_1				    : std_logic_vector(31 downto 0); 
	
signal del_crc				    : std_logic_vector(2 downto 0);
signal pipe_dta			        : std_logic_vector(127 downto 0);
signal pipe_dtb			        : std_logic_vector(127 downto 0);
signal pipe_dtc			        : std_logic_vector(127 downto 0);
signal pipe_Start_a				: std_logic;
signal pipe_Start_b				: std_logic;
signal pipe_Start_c				: std_logic;
signal pipe_Stop_a				: std_logic;
signal pipe_Stop_b				: std_logic;
signal pipe_Stop_c				: std_logic;
signal pipe_wen_a				: std_logic;
signal pipe_wen_b				: std_logic;
signal pipe_wen_c				: std_logic;
	
signal tmp_dt_crc			    : std_logic_vector(127 downto 0);
	
signal nxt_dt				    : std_logic;
signal wc_val				    : std_logic_vector(15 downto 0);
signal start_framing		    : std_logic;
signal rq_cmd				    : std_logic;
signal rq_ack				    : std_logic;
signal pckt_cmd_ack		        : std_logic;
signal CRC_rst				    : std_logic;
signal mem_error			    : std_logic_vector(1 downto 0);
signal mem_error_gen		    : std_logic_vector(1 downto 0);
signal start_pckt			    : std_logic;
signal ack_pckt_emp 		    :std_logic;
signal read_ack_ff		        : std_logic;
signal cnt_ack_sent		        : std_logic;

attribute KEEP : string;

attribute KEEP of  pipe_Start_a				: signal is "true";
attribute KEEP of  pipe_Stop_a				: signal is "true";
attribute KEEP of  pipe_wen_a				: signal is "true";

--attribute mark_debug : string; 
--attribute mark_debug of packet			: signal is "true";  
--attribute mark_debug of pipe_dtc		: signal is "true";  
--attribute mark_debug of pipe_Start_c	: signal is "true";  
--attribute mark_debug of pipe_Stop_c		: signal is "true";  
--attribute mark_debug of pipe_wen_c		: signal is "true";  
--attribute mark_debug of COMMAND_req		: signal is "true";  
--attribute mark_debug of rq_cmd		: signal is "true";  
--attribute mark_debug of start_framing		: signal is "true";  
 
--*******************************************************
--**************  BEGIN  ********************************
--*******************************************************
begin 

preset <= '1' when resetn_r = '0' else '0';

--*****************  error gen  *********************
process(resetn_t,clock_t)
begin
	if resetn_t = '0' then
		mem_error 			<= (others => '0');
		mem_error_gen 		<= (others => '0');
	elsif rising_edge(clock_t) then
		if error_gen /= "00" then
			mem_error 		<= error_gen;
		elsif  packet = start_of_frame then
			mem_error 		<= "00";
		end if;
		if  packet = start_of_frame then
			mem_error_gen 	<= mem_error;
		elsif del_crc(1) = '1' then
			mem_error_gen 	<= "00";
		end if;
	end if;
end process;
--***************************************************
-- static value for Acknowledge packet
--len_mem					<= (others => '0');
data_0					<= status(31 downto  0); 
data_1					<= status(63 downto 32);
sqnb_mem_ack(31)		<= '1';


-- memorise the ack to be sent to FED to indicate that packet was well received (CRC correct)
i1:fifo_ack_req  
PORT MAP
	(
	sclrp					=> preset,	
	wrclk					=> clock_r,	
	wrreq					=> req_ack_pckt,	
	data					=> Seq_nb(30 downto 0),	
	--wrfull					=> ,	
	rdclk					=> clock_t,	
	rdreq					=> read_ack_ff,	
	q						=> sqnb_mem_ack(30 downto 0),	
	rdempty					=> ack_pckt_emp	
	);
	
-- memorise the request  for ACK and COmmand packet		
process(resetn_t,clock_t)
begin
	if resetn_t = '0' then
		rq_cmd					<= '0';
		rq_ack  				<= '0'; 
		read_ack_ff 			<= '0';
	elsif rising_edge(clock_t) then		
		
		if 	COMMAND_req = '1' then				-- a request to transmit a packet (command) 
			rq_cmd				<= '1';		-- we memorize the request
		elsif packet = wait_crc AND CRC_valid = '1' and pckt_cmd_ack = '1' then  -- we realse the request at the end of its build (only if COmmand packet)
			rq_cmd				<= '0';
		end if;
		
		read_ack_ff <= '0';
		if    ack_pckt_emp = '0' and rq_ack = '0' then -- if not empty and not request ongoing
			rq_ack				<= '1';		-- we memorize the request
			read_ack_ff 		<= '1';		--  read the request form FIFO (buffer)
		elsif packet = wait_crc AND CRC_valid = '1' and pckt_cmd_ack = '0'  then-- we realse the request at the end of its build (only if ACK packet)
			rq_ack				<= '0';
		end if;
	end if;
end process;
 
--manage the begining of a frame
-- choose between ACK and COMMAND packet (command cpaket has the priority if both a requested in the same time) 
process(resetn_t,clock_t)
begin
	if resetn_t = '0' then
		start_framing 			<= '0';
		CRC_rst 				<= '0'; 
		pckt_cmd_ack 			<= '0';
	elsif rising_edge(clock_t) then

		CRC_rst 				<= '0';
		if packet = wait_crc AND CRC_valid = '1' then
			start_framing 		<= '0'; 	-- finihsed the frame (CRC calcul ended)
		
		elsif start_framing = '0' then -- gives the priority to command 
			if 	rq_cmd = '1' then
				pckt_cmd_ack 	<= '1';		-- specify  a COMMAND packet
				start_framing 	<= '1';		-- indicate a START Frame
				CRC_rst 		<= '1';		-- reset the CRC engine at the begining of a packet
			elsif rq_ack = '1' then
				pckt_cmd_ack 	<= '0';		-- specify  an ACK packet
				start_framing 	<= '1';		-- indicate a START Frame
				CRC_rst 		<= '1';		-- reset the CRC engine at the begining of a packet
			end if;
		end if;
	end if;
end process;		

-- Engine to calculate the CRC of the packet to be sent
CRC_engine:Slink_packet_CRC16_D128b   
PORT MAP(          
		clock      			=> clock_t, 
		resetp     			=> CRC_rst, 
		data       			=> tmp_dt_crc, 
		data_valid 			=> cmp_crc, 
		eoc        			=> end_crc, 
		crc        			=> val_crc, 
		crc_valid  			=> crc_valid 
       );	   
 
 -- State machine which structure each word to form the packet
state_step:process(clock_t,resetn_t)
begin 
	if resetn_t = '0' then
		packet <= ST_START;
	elsif rising_edge(clock_t) then
		 Case packet is
			when ST_START =>
				if start_framing = '1' then
					packet	<= start_of_frame;
				end if;
		 
			when start_of_frame =>
				packet 		<= Data_state;
 
			when  Data_state =>
				packet 		<= CRC_end_frame;	
				
			when CRC_end_frame =>
				 packet 		<= wait_crc;
				
			when wait_crc =>
				if crc_valid = '1' then
					packet 	<= gap0;
				end if;
			when gap0 =>
				packet 		<= gap1;
			when gap1 =>
				packet 		<= ST_START;
--			when gap2 =>
--				packet 		<= ST_START;
			when others =>
				packet 		<= ST_START;
		 end case;
	end if;
end process;

process(resetn_t,clock_t)
	variable local_v	: std_logic_vector(127 downto 0);
begin
	if resetn_t = '0' then
		pipe_dta 						<= (others => '0'); 	 
		tmp_dt_crc						<= (others => '0'); 
	elsif rising_edge(clock_t) then
		pipe_dta 						<= (others => '0');  
		
		if packet = start_of_frame  then
		
			if  pckt_cmd_ack = '1' then							-- COMMAND_number packet
				local_v(127 downto 64) 	:= COMMAND_number;
				local_v(63 downto 32)	:= COMMAND_Seq_num;
			else												-- Acknowledge packet
				local_v	(127 downto 64) := (others => '0'); 
				local_v(63 downto 32)	:= sqnb_mem_ack;
			end if;
			
			local_v(31 downto 16) 		:= Descript_Pckt;
			
			if pckt_cmd_ack = '1' then						-- Command packet
				local_v(15 downto  0) 	:= x"0001";				-- for cmd  length = 1
			else											-- Acknowledge packet
				local_v(15 downto  0) 	:= x"0000";				-- for ack  length = 0
			end if;
			
			-- used to compute the CRC
			tmp_dt_crc					<= local_v;
			pipe_dta					<= local_v;
			
		elsif packet = Data_state then
			
			local_v(127 downto 0)		:= (others => '0');
			if pckt_cmd_ack = '1' then							-- Command packet
				local_v(63 downto 0) 	:= COMMAND_data;
			end if;
			
			-- used to compute the CRC
			tmp_dt_crc					<= local_v;
			pipe_dta				 	<= local_v;
			
--		elsif packet = CRC_end_frame then
--			pipe_dta 			 		<= (others => '0');
			
		end if;
	end if;
end process;

process(resetn_t,clock_t) 
begin
	if resetn_t = '0' then
		pipe_Start_a					<= '0';
		pipe_Stop_a						<= '0';
		pipe_wen_a						<= '0';
		cmp_crc 						<= '0';
		end_crc							<= '0';
	elsif rising_edge(clock_t) then
		pipe_Start_a					<= '0';
		pipe_Stop_a						<= '0';
		end_crc							<= '0';
		pipe_wen_a						<= '0';
		
		if packet = start_of_frame  then
			pipe_Start_a				<= '1';				-- indicates the begining of a packet	
			pipe_wen_a					<= '1';
		elsif packet = Data_state then
			pipe_Stop_a					<= '1';			-- indicates the end of a packet			
			pipe_wen_a					<= '1';
			end_crc						<= '1';         -- last word indicate end of CRC calcul
		end if;

		if packet = start_of_frame  then
			cmp_crc 					<= '1';					-- Enable CRC calcul at the begining of a packet
		elsif packet = CRC_end_frame then
			cmp_crc 					<= '0';
		end if;
		
	end if;
end process;

process(clock_t)
begin
	if rising_edge(clock_t) then
		del_crc(2 downto 1) <= del_crc(1 downto 0);
		del_crc(0) <= '0';
		if packet = CRC_end_frame then
			del_crc(0) <= '1';
		end if;
	end if;
end process;


-- Piped data and START/STOP signals during CRC calcul
process(clock_t)
begin
	if rising_edge(clock_t) then
		pipe_Start_c 	<= pipe_Start_b;
		pipe_Start_b 	<= pipe_Start_a;
		
		pipe_Stop_c 	<= pipe_Stop_b;
		pipe_Stop_b 	<= pipe_Stop_a;
		
		pipe_wen_c 		<= pipe_wen_b;
		pipe_wen_b 		<= pipe_wen_a;
		
		pipe_dtc		<= pipe_dtb;
		pipe_dtb		<= pipe_dta;
		-- replace the CRC field with the CRC calculated
		if del_crc(0) = '1' then
			pipe_dtc(79 downto 64)	<= val_crc;
			pipe_dtc(64) 			<= val_crc(0) xor mem_error_gen(0);
		end if;
	end if;
end process;

-- Statistic pulse for counter implementation
process(resetn_t,clock_t)
begin
	if resetn_t = '0' then
		cnt_ack_sent 			<= '0';
	elsif rising_edge(clock_t) then
		cnt_ack_sent 			<= '0';
		if  end_crc = '1' and pckt_cmd_ack = '0' then
			cnt_ack_sent		<= '1';
		end if;
	end if;
end process;

send_ack			<= cnt_ack_sent;

ST_START_state 		<= '1' when packet = ST_START else '0';

-- swapp bytes to be compliante to 10Gb
Pckt_datao			<= pipe_dtc;
Pckt_Start_o		<= pipe_Start_c;
Pckt_End_o			<= pipe_Stop_c;
Pckt_Wen_o			<= pipe_wen_c;
 
end behavioral;


