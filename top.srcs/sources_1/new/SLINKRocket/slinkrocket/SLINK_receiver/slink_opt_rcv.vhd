------------------------------------------------------
--  Optical Link receiver part
--
--  Ver 1.01
--
-- Dominique Gigi March 2012
------------------------------------------------------
--  Change the cmd_ctrl component
--  
-- 
--  
------------------------------------------------------
LIBRARY ieee;
 
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;
 
entity Slink_opt_rcv is
generic (Add_Offset_SLINKR			: integer := 0;
         UltraRam_mem               : boolean := TRUE  ) ;
port (
	resetn_r						: in std_logic;
	clock_r						    : in std_logic;
	resetn_t						: in std_logic;
	clock_t						    : in std_logic;
		
	-- DATA to DAQ system		
	Evt_rd_data						: out std_logic; -- active high
	Evt_UCTRL_out					: out std_logic; -- active High to indicates  a Trailer or a Header  
	Evt_data_out					: out std_logic_vector(127 downto 0);
	Evt_lff							: in std_logic; --backpressure active high
		
	-- DAta from Serdes		
	Init_serdes					    : in std_logic; 					-- initialization of SERDES is done
	DAQ_serdes_RX_hd  	            : in std_logic_vector(5 downto 0);		 
	DAQ_serdes_RX                	: in std_logic_vector(127 downto 0);
 
	ext_cmd						    : in std_logic_vector(15 downto 0);   -- generate errors
	DAQ_serdes_TX_hd                : out std_logic_vector(5 downto 0);	
	DAQ_serdes_TX                   : out std_logic_vector(127 downto 0);
					
	ack_cnt						    : out std_logic; -- Ack sent
	pckt_cnt						: out std_logic; -- good packet received
			
	--Control bus (PCIe,...)
	USR_resetn						: in std_logic;
	USR_clk							: in std_logic;
	USR_rd_func				   	 	: in std_logic_vector(16383 downto 0);--func(0) set the address (bit 31 = 1 write =0 read, func(1) set the data 
	USR_wr_func				    	: in std_logic_vector(16383 downto 0);--func(0) set the address (bit 31 = 1 write =0 read, func(1) set the data 
	USR_dti							: in std_logic_vector(63 downto 0);
	USR_wen							: in std_logic;	
	USR_dto 					    : out std_logic_vector(63 downto 0);
	 
	link_init					    : out std_logic; -- set to '1' after reception of init_link
	prev_acc_end				    : out std_logic;
	CUR_seq_num					    : out std_logic_vector(31 downto 0);
	CMD_seq_num					    : out std_logic_vector(31 downto 0);
	cnt_pckt						: out std_logic; -- total packet received
	cnt_bad_p					    : out std_logic; -- bad packet received
	back_pres					    : out std_logic;
	FED_crc_err					    : out std_logic;
	SLINK_crc_err				    : out std_logic 
);
end Slink_opt_rcv;

--##############################################################################################
--################################       ARCHITECTURE      #####################################
--##############################################################################################

architecture behavioral of Slink_opt_rcv is

  
signal datai						: std_logic_vector(127 downto 0); --- data from SERDES
signal Start_Pkti					: std_logic; 
signal End_Pkti					    : std_logic; 
signal Wen_Pkti					    : std_logic; 
 
signal datao							: std_logic_vector(127 downto 0); --- data and K bit send to SERDES
signal Start_Pkt_o						: std_logic;
signal End_Pkt_o						: std_logic;
signal Wen_Pkt_o						: std_logic;

signal SerDes_READY										: std_logic;


signal free_place 			    : std_logic;
signal new_blk 				    : std_logic;
signal Seq_nb					: std_logic_vector(31 downto 0); 
signal lenght					: std_logic_vector(15 downto 0); 
signal data						: std_logic_vector(127 downto 0); 
signal cmd_data_rtn				: std_logic_vector(63 downto 0);  
signal wr_ena 					: std_logic;
signal seq_to_be_ack			: std_logic_vector(31 downto 0);
--signal valid_para 			: std_logic;
signal valid_packet 			: std_logic;
signal end_blk 				    : std_logic;
signal ack_req					: std_logic;
signal init_rcv					: std_logic;
signal interm_seq_nb			: std_logic_vector(31 downto 0);
signal req_ack_pck			    : std_logic;
signal send_ack				    : std_logic;
signal ack_rcv					: std_logic;
		
signal COMMAND_req				    : std_logic; -- indicates that the packet is a data packet
signal COMMAND_number					: std_logic_vector(63 downto 0);	--COMMAND_number for COMMAND_number packet
signal COMMAND_data					: std_logic_vector(63 downto 0);	--datafor COMMAND_number packet
signal COMMAND_Seq_num				: std_logic_vector(31 downto 0); -- sequence number


-- use to pipe frgament during the CRC check
signal rd_data_cell			    : std_logic;
signal uctrl_cell				: std_logic;
signal data_out_cell			: std_logic_vector(127 downto 0);
signal uctrl_out_cell			: std_logic; 

signal data_ra					: std_logic_vector(127 downto 0);
signal uctrl_ra				    : std_logic; 
signal wen_ra					: std_logic;

signal data_mux				    : std_logic_vector(127 downto 0);

signal data_rb					: std_logic_vector(127 downto 0);
signal uctrl_rb				    : std_logic; 
signal wen_rb					: std_logic;
 
signal Error_Slink_CRC		    : std_logic; 

signal CRC_cmp					: std_logic_vector(15 downto 0);
signal end_evt 				    : std_logic_vector(1 downto 0);
signal CRC_rst					: std_logic;
signal block_desc				: std_logic_vector(15 downto 0);


attribute mark_debug : string; 
-- attribute mark_debug of DAQ_serdes_RX			    : signal is "true"; 
-- attribute mark_debug of DAQ_serdes_RX_hd			: signal is "true"; 


--*************************************************************************************
--***********************************< <  BEGIN   > >**********************************
--*************************************************************************************
begin
--###############################	 
--Receiver part DAQ
	 	 	
T_to_Serdes_recv:entity work.T127_R_serdes_127b_encoding  
    generic map(
            swap_bit	=> false)
	port map	(
		reset_n					=>	resetn_t			, 
		clock					=>	clock_t				, 
		SD_DATA_I				=>	datao				, 
		SD_Start_I				=>	Start_Pkt_o			, 
		SD_Stop_I				=>	End_Pkt_o			, 
		SD_Val_dwrd_I			=>	Wen_Pkt_o			,  
				               
		Serdes_word				=>	DAQ_serdes_TX		, 
		Serdes_Header			=>	DAQ_serdes_TX_hd				  
	 );
	 
Serdes_to_R_recv:entity work.R127b_T_Serdes_127b_decoding  
    generic map(
            swap_bit	=> false)
	port map
	(
		reset_n					=>	resetn_r			,
		clock					=>	clock_r				,
		SerDes_DATA_I			=>	DAQ_serdes_RX		,
		SerDes_Header_I			=>	DAQ_serdes_RX_hd	,
				               
		SD_DATA_O				=>	datai				, --- data from SERDES
		SD_Start_PktO			=>	Start_Pkti			,
		SD_End_PktO				=>	End_Pkti			,
		SD_Wen_PktO				=>	Wen_Pkti					 
	 );
	 
--###############################
-- SLINK receiver logic

Manage_Packet_received:entity work.rcv_pckt  
port map(
	resetn							=> resetn_r,
	clock							=> clock_r,
	PCKT_datai						=> datai				, --- data  
	PCKT_Start						=> Start_Pkti				,-- active high valid the start of the packet
	PCKT_End						=> End_Pkti				,-- active high valid the end of packet
	PCKT_wen						=> Wen_Pkti			,-- active high valid a word 
	free_place					    => free_place, 
	BLK_new						    => new_blk, 
	BLK_seqnb 					    => seq_nb, 
	BLK_lenght					    => lenght, 
	BLK_desc					    => block_desc,
	
	BLK_datao						=> data, 
	BLK_wr_ena					    => wr_ena,  
	BLK_valid_packet			    => valid_packet, 
	BLK_end_blk					    => end_blk,
	
	cmd_data_rtn				    => cmd_data_rtn, 
	
	ack_packet_o				    => ack_rcv, 
	init_packet_o					=> init_rcv ,
	cnt_pckt						=> cnt_pckt,
	cnt_bad_p					    => cnt_bad_p,
	back_pres					    => back_pres
 	);
	
Manage_blocks_received:entity work.rcv_memory_blocks  
generic map (UltraRam_mem       => UltraRam_mem)
port map(
	--IN
	resetn							=> resetn_r                ,
	clock							=> clock_r                ,
	
	init							=> init_rcv               , 			-- Init reinitialize the SEQ number
	new_blk						    => new_blk                , 		    -- inform that a new block is coming
	seq_nb						    => seq_nb                 , 
	blk_desc						=> block_desc(15 downto 0),             -- Start/Stop abd Wrd_Ena
	blk_size						=> lenght                 , 			-- block size in 512 words
	data_in						    => data                   , 
	wen							    => wr_ena                 , 
	valid_blk					    => valid_packet           , 	        -- CRC is correct the blcok is ended and there place to record it (no backpressure)
	end_blk						    => end_blk                , 		    -- CRC is correct the block is ended

	free_block					    => free_place             , 		    -- inform if there is place to record block(s)
	ack_req						    => ack_req                ,			    -- received a packet which request to send an ACK  packet
	seq_to_be_ack				    => seq_to_be_ack          ,             -- inform the SEQ number to be ack
	
	rd_data						    => rd_data_cell           , 
	Uctrl_out						=> uctrl_out_cell		  ,             --Start of fragment
	DATA_out						=> data_out_cell	      ,             -- Data out (512 bit)
	lff							    => Evt_lff                ,				 
	CUR_seq_num					    => CUR_seq_num
);


Manage_packet_build:entity work.build_pckt 
port map(
	resetn_t						=> resetn_t,
	resetn_r						=> resetn_r,
	clock_t						    => clock_t,
	clock_r						    => clock_r,
					
	COMMAND_req						=> COMMAND_req,
	COMMAND_number					=> COMMAND_number,
	COMMAND_data					=> COMMAND_data,
	COMMAND_Seq_num					=> COMMAND_Seq_num,
					
	req_ack_pckt					=> req_ack_pck, 
	status						    => x"0000000000000000", 
	Descript_Pckt				    => X"0000", 
	Seq_nb						    => interm_seq_nb,
	error_gen					    => ext_cmd(1 downto 0),
	send_ack						=> send_ack, 
						
	Pckt_datao						=> datao, 
	Pckt_Start_o			 		=> Start_Pkt_o , 
	Pckt_End_o						=> End_Pkt_o,	
	Pckt_Wen_o						=> Wen_Pkt_o
	--ST_START_state	: out std_logic
);
	
interm_seq_nb 			<= seq_nb	when init_rcv = '1' else seq_to_be_ack ;
req_ack_pck				<= '1' 		when init_rcv = '1' or ack_req = '1' else '0';

pckt_cnt <= valid_packet;
ack_cnt <= send_ack;


Manage_command_to_Sender:entity work.cmd_ctrl 
generic map(address_offset			=> Add_Offset_SLINKR) 
port map (
	USR_resetn 		    			=> USR_resetn,
	USR_clk							=> USR_clk, 
	USR_wr_func		    			=> USR_wr_func,	--func(0) set the address (bit 31 = 1 write =0 read, func(1) set the data 
	USR_rd_func	    				=> USR_rd_func,	--func(0) set the address (bit 31 = 1 write =0 read, func(1) set the data  
	USR_dti							=> USR_dti, 
	USR_wen							=> USR_wen, 	
	USR_dto 		    			=> USR_dto, 
	access_end		    			=> prev_acc_end, -- indicate that previous COMMAND_number access is ended
	Grst_clk_TX		    			=> resetn_t,
	clk_TX			    			=> clock_t, 
	COMMAND_req						=> COMMAND_req, 
	COMMAND_number					=> COMMAND_number, 
	COMMAND_data					=> COMMAND_data, 
	COMMAND_Seq_num					=> COMMAND_Seq_num,
	Grst_clk_RX		    			=> resetn_r,
	clk_RX			    			=> clock_r, 
	init_link		    			=> init_rcv, 		-- reset the seq to 0x0000001
	link_init		    			=> link_init,  	-- set to '1' after reception of init_link
	ACK_rcv		    				=> ack_rcv, 
	ACK_data_i	   					=> cmd_data_rtn,  
	ACK_seq_num_i   				=> seq_nb 
	);
	
CMD_seq_num <= COMMAND_Seq_num;





-- compute the CRC from the fragment (should be all the time correct because the chekc is done when the fragment is send over a packet on the fiber
process(resetn_r,clock_r)
begin
	if resetn_r = '0' then
		End_evt				<= (others => '0');
		CRC_Rst				<= '1';
		data_rb 			<= (others => '0');		
		uctrl_rb			<= '0'; 	
		wen_rb				<= '0';		
		data_ra				<= (others => '0');
		uctrl_ra			<= '0';
		wen_ra				<= '0';		
		
	elsif rising_edge(clock_r) then

		data_rb 			<= data_ra;		
		uctrl_rb			<= uctrl_ra; 	
		wen_rb				<= wen_ra;		
	
		data_ra				<= data_out_cell;
		uctrl_ra			<= uctrl_out_cell;
		wen_ra				<= rd_data_cell;

		End_evt(1)			<= End_evt(0); 
		End_evt(0)			<= '0'; 
		if uctrl_out_cell = '1' and rd_data_cell = '1' and data_out_cell(127 downto 124) = x"A" then
			End_evt(0) 		<= '1';
		end if;
		
--		if uctrl_out_cell = '1' and rd_data_cell = '1' and data_out_cell(127 downto 124) = x"A" then
--			CRC_Rst			<= '1';
		if End_evt(1) = '1' then
			CRC_Rst			<= '1'; -- reset the CRC compute until a header is found
                else
                        CRC_Rst                 <= '0';
		end if;
		

 
	end if;
end process;

	
data_mux(127 downto 120)		<= data_ra(127 downto 120);
data_mux(119 downto 104)		<= data_ra(119 downto 104)	when End_evt(0) = '0' else x"0000";
data_mux(103 downto 32)			<= data_ra(103 downto 32);
data_mux(31 downto 16)			<= data_ra(31 downto 16)	when End_evt(0) = '0' else x"0000";
data_mux(15 downto 0)			<= data_ra(15 downto 0)		when End_evt(0) = '0' else x"0000";

i_crc_check:entity work.FED_fragment_CRC16_D128b  
  -- polynomial: x^16 + x^15 + x^2 + 1
  -- data width: 128
  -- convention: the first serial bit is Data[127]
    Port map( 
		Data 		=>	data_mux	,
		CRC_out 	=>	CRC_cmp		,
		clk 		=>	clock_r		,
		clear_p 	=>	CRC_Rst		,
		enable 		=>	wen_ra			 
		);
 
Error_Slink_CRC	<= '0' when data_rb(119 downto 104) = CRC_cmp  else '1';-- compare with the DAQ_CRC
		
-- SLINK rocket bus output
Evt_rd_data							<= wen_rb;
Evt_UCTRL_out						<= uctrl_rb;
 	
Evt_DATA_out(127 downto 2) 		<= data_rb(127 downto 2);
Evt_DATA_out(1) 					<= Error_Slink_CRC	when End_evt(1) = '1' else data_rb(1);
Evt_DATA_out(0) 			        <= data_rb(0);



-- Count the FED_CRC error bit
FED_crc_err		<= data_ra(0) and End_evt(0);
-- Count the SLINK_CRC error bit
SLINK_crc_err	<= Error_Slink_CRC and End_evt(1);

end behavioral;

 
