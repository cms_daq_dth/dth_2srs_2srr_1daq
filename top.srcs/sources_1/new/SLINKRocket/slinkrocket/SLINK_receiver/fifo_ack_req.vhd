------------------------------------------------------
-- encapsulate the data/ack/init for Opt Link
--
--  Ver 1.00
--
-- Dominique Gigi March 2012
------------------------------------------------------
--   
--  
-- 
--  
------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.all;
 
ENTITY fifo_ack_req IS
	PORT
	(
		sclrp		: IN STD_LOGIC  := '0';
		data		: IN STD_LOGIC_VECTOR (30 DOWNTO 0);
		rdclk		: IN STD_LOGIC ;
		rdreq		: IN STD_LOGIC ;
		wrclk		: IN STD_LOGIC ;
		wrreq		: IN STD_LOGIC ;
		q			: OUT STD_LOGIC_VECTOR (30 DOWNTO 0);
		rdempty		: OUT STD_LOGIC ;
		wrfull		: OUT STD_LOGIC 
	);
END fifo_ack_req;


ARCHITECTURE SYN OF fifo_ack_req IS

	SIGNAL sub_wire0	: STD_LOGIC ;
	SIGNAL sub_wire1	: STD_LOGIC_VECTOR (30 DOWNTO 0);
	SIGNAL sub_wire2	: STD_LOGIC ;

-- FIFO Generator
--		Native
--		Independent Clocks Builtin FIFO
--		Standard FIFO	
--		Width 31
--		Depth 512
-- 		198 MHz

COMPONENT SR_FIFO_Ack_rcv
	  PORT (
	    srst        : IN STD_LOGIC;
	    wr_clk      : IN STD_LOGIC;
	    rd_clk      : IN STD_LOGIC;
	    din         : IN STD_LOGIC_VECTOR(30 DOWNTO 0);
	    wr_en       : IN STD_LOGIC;
	    rd_en       : IN STD_LOGIC;
	    dout        : OUT STD_LOGIC_VECTOR(30 DOWNTO 0);
	    full        : OUT STD_LOGIC;
	    empty       : OUT STD_LOGIC;
	    wr_rst_busy : OUT STD_LOGIC;
	    rd_rst_busy : OUT STD_LOGIC
	  );
END COMPONENT;

BEGIN
 
	dcfifo_component : SR_FIFO_Ack_rcv
	  PORT MAP(
	    srst 			=>	sclrp			, 
	    wr_clk 			=>	wrclk			, 
	    rd_clk 			=>	rdclk			, 
	    din         	=>	data			, 
	    wr_en       	=>	wrreq			, 
	    rd_en       	=>	rdreq			, 
	    dout        	=>	q				, 
	    full        	=>	wrfull			, 
	    empty       	=>	rdempty			  
	    --wr_rst_busy 	=>					, 
	   -- rd_rst_busy 	=>					  
	  );
	  


END SYN;
