------------------------------------------------------
-- encapsulate the data/ack/init for Opt Link
--
--  Ver 1.00
--
-- Dominique Gigi March 2012
------------------------------------------------------
--   
--  
-- 
--  
------------------------------------------------------
LIBRARY ieee;
  
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
  
entity check_slink is
port (
	Gresetn					: in std_logic;
	-- SLINK input	
	clock					: in std_logic;
	wen						: in std_logic;
	uctrl					: in std_logic;
	data					: in std_logic_vector(127 downto 0);
	BACKP					: in std_logic ;	
	SERDES_ready            : in std_logic;		
	-- result			
	pckt_OK					: out std_logic_vector(63 downto 0);
	pckt_KO					: out std_logic_vector(63 downto 0);
	expect_trig_nb			: out std_logic_vector(43 downto 0);	-- indicates the expected trigger number
	bad_trig_num			: out std_logic_vector(43 downto 0) 	-- indicates the received trigger number
);
end check_slink;

architecture behavioral of check_slink is

COMPONENT FED_fragment_CRC16_D128b is
port (
		Data			: in std_logic_vector(127 downto 0);
		CRC_out 		: out std_logic_vector(15 downto 0);
		clk 			: in std_logic;
		clear_p			: in std_logic;
		enable 			: in std_logic
	);
end COMPONENT;

component resetn_resync is
port (
	aresetn				: in std_logic;
	clock				: in std_logic; 

	Resetn_sync		: out std_logic;
	Resetp_sync		: out std_logic
	); 
end component;

signal resetn_clock         : std_logic;
signal reset_before_Serdes_ready : std_logic;

signal start_fm				: std_logic;
signal stop_fm				: std_logic;
signal CRC_to_be_check 		: std_logic_vector(15 downto 0);
--signal evt_frame			: std_logic;
signal pipe_A				: std_logic_vector(129 downto 0);
signal pipe_B				: std_logic_vector(129 downto 0);
signal delay_crc_check		: std_logic_vector(3 downto 0);
--signal delay_reset		: std_logic;
signal CRC_out				: std_logic_vector(15 downto 0);
signal status_cnt_OK		: std_logic_vector(63 downto 0);
signal status_cnt_KO		: std_logic_vector(63 downto 0);

signal reset_val			: std_logic;
signal trig_nb				: std_logic_vector(43 downto 0);
signal trigger_rcv			: std_logic_vector(43 downto 0);
signal trigger_expect		: std_logic_vector(43 downto 0);
signal sync_lost			: std_logic_vector(1 downto 0);
  
signal pcie_dto_rg			: std_logic_vector(31 downto 0);

attribute mark_debug : string;
--attribute mark_debug of CRC_out		: signal is "true";   
--attribute mark_debug of CRC_to_be_check		: signal is "true";   
--attribute mark_debug of delay_crc_check		: signal is "true";   

--**********************************************************************
--***********************<<     BEGIN     >>****************************
--**********************************************************************
begin
 
reset_before_Serdes_ready   <= '0' when Gresetn = '0' or SERDES_ready = '0' else '1'; 
 
reset_resync_i1:resetn_resync 
port map(
	aresetn			=> reset_before_Serdes_ready,
	clock			=> clock,
	Resetn_sync		=> resetn_clock
	);  
 
 
--******************************************************
crc_check:FED_fragment_CRC16_D128b 
port map(
	clear_p					=> start_fm,
	clk 					=> clock,
	Data					=> pipe_B(127 downto 0),
	enable 					=> pipe_B(129),
	CRC_out 				=> CRC_out 
	);
  
process(resetn_clock,clock)
begin
	if resetn_clock = '0' then
		start_fm 			<= '0';
		stop_fm 			<= '0';
--		evt_frame 			<= '0';
		CRC_to_be_check 	<= (others =>'0');
		delay_crc_check		<= (others =>'0');
--		crc_FED_err_reg		<= '0';
	elsif rising_edge(clock) then
		start_fm 				<= '0';
		stop_fm 					<= '0';
		if wen = '1' and uctrl = '1' and data(127 downto 120) = x"55" then
			start_fm		    <= '1';											--*-- Start signal used to reset CRC component
--			evt_frame 			<= '1';											--*-- Event envelop
		end if;
		
--		crc_FED_err_reg		<= '0';
		if wen = '1' and uctrl = '1' and data(127 downto 120) = x"AA" then
			stop_fm				<= '1';
			CRC_to_be_check 	<= data(119 downto 104);						--*-- Latch CRC coming with event
--			evt_frame			<= '0';
--			crc_FED_err_reg		<= data(2);
		end if;
 
		delay_crc_check(3 downto 1) 	<= delay_crc_check(2 downto 0);
		delay_crc_check(0) 				<= stop_fm;
--		delay_reset							<= start_fm;
	end if;
end process;

process(clock)
begin
	if rising_edge(clock) then
		pipe_B(129 downto 0)				<= pipe_A(129 downto 0); 
		
		if stop_fm = '1' then
			pipe_B(119 downto 104)			<= x"0000"; -- remove the DAQ_CRC  
			pipe_B(31 downto 16)			<= x"0000"; -- remove the FED_CRC 
			pipe_B(15 downto 00)			<= x"0000"; -- remove the STATUS
		end if;
		 
		pipe_A(127 downto 0) 				<= data;
		pipe_A(128)							<= uctrl;
		pipe_A(129)							<= wen;
	
	end if;	
end process;


--crc_FED_err	<= crc_FED_err_reg;


process(resetn_clock,clock)
begin
	if resetn_clock = '0' then
		status_cnt_OK 			<= (others =>'0');
		status_cnt_KO			<= (others =>'0');
		sync_lost				<= (others =>'0');
		trigger_rcv				<= (others =>'0');
		trig_nb					<= x"00000000001";
		trigger_expect			<= x"00000000001";
	elsif rising_edge(clock) then
		sync_lost(1)	<= sync_lost(0);
		if start_fm = '1' and sync_lost(0) = '0' then
			trigger_rcv			<= pipe_A(107 downto 64);
			if pipe_A(107 downto 64) /= trig_nb and pipe_A(107 downto 64) /= x"00000001" then
				sync_lost(0)	<= '1';
			end if;
		end if;
		
		if sync_lost(0) = '0' then
			trigger_expect		<= trig_nb;
		end if;
		
		if start_fm = '1' and pipe_A(107 downto 64) = x"00000000001" then
			trig_nb				      <= x"00000000001";
		elsif stop_fm = '1' then
			trig_nb 				  <= trig_nb + "1";
		end if;
		
		if delay_crc_check(1) = '1' then
			if CRC_out /= CRC_to_be_check then
				status_cnt_KO 	<= status_cnt_KO + "1";
			else 	
				status_cnt_OK 	<= status_cnt_OK + "1";
			end if;	
		end if;
	end if;
end process;

expect_trig_nb				<= trigger_expect;
bad_trig_num				<= trigger_rcv;


pckt_OK						<= status_cnt_OK;
pckt_KO						<= status_cnt_KO;

end behavioral;