----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 25.09.2019 15:29:57
-- Design Name: 
-- Module Name: SR_receiver - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use work.address_table.all;  

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
 
library UNISIM;
use UNISIM.VComponents.all;

entity SR_receiver_quad is
generic(SR0_offset                               : integer := 0;
        SR1_offset                               : integer := 0;
        throughput								: string := "15.66";
				--possible choices are 15.66 or 25.78125
		ref_clock								: string := "156.25";
				--possible choices are 156.25  or  322.265625 
		technology								: string := "GTY";
				-- possible choices are GTY or GTH
        UltraRam_mem                            : boolean := TRUE 
				);
 Port (
    usr_clk           	: IN STD_LOGIC;
    rst_usr_clk_n     	: IN STD_LOGIC;
    usr_func_wr       	: IN STD_LOGIC_VECTOR(16383 DOWNTO 0);
    usr_wen           	: IN STD_LOGIC;
    usr_data_wr       	: IN STD_LOGIC_VECTOR(63 DOWNTO 0);
    usr_func_rd       	: IN STD_LOGIC_VECTOR(16383 DOWNTO 0);
    usr_rden          	: IN STD_LOGIC;
    usr_dto_receiver  	: OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    resetp_counters     : in std_logic;
	
	clk_freerun_in		: in std_logic;
	
	clock_SR0			: OUT STD_LOGIC;	
	SR0_WEN  			: OUT STD_LOGIC;	
	SR0_UCTRL			: OUT STD_LOGIC;
	SR0_DATA 			: OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
	SR0_LFF  			: IN STD_LOGIC;

	clock_SR1			: OUT STD_LOGIC;	
	SR1_WEN  			: OUT STD_LOGIC;	
	SR1_UCTRL			: OUT STD_LOGIC;
	SR1_DATA 			: OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
	SR1_LFF  			: IN STD_LOGIC;

	 
    SRI_ref_clkp    	: IN STD_LOGIC;	
    SRI_ref_clkn    	: IN STD_LOGIC;	
	SRI_gt_rxn_in     	: IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    SRI_gt_rxp_in     	: IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    SRI_gt_txn_out    	: OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    SRI_gt_txp_out    	: OUT STD_LOGIC_VECTOR(1 DOWNTO 0)
);	
end SR_receiver_quad;

--****************************************************************************
architecture Behavioral of SR_receiver_quad is
 

signal SRI_ref					: std_logic;
signal qpll_lock_out      		: STD_LOGIC;
signal qpll_reset_out    		: STD_LOGIC;
signal qpll_reset0_out    		: STD_LOGIC;
signal qpll_reset1_out    		: STD_LOGIC;
signal qpll_clkin        		: STD_LOGIC;
signal qpll_ref_clkin    		: STD_LOGIC;
 

signal gt0_Reset_TX_clock_out			: std_logic;                				 
signal gt1_userclk_tx_active_out		: std_logic;                 				 
signal gt1_userclk_tx_usrclk_out		: std_logic;                  				 
signal gt1_userclk_tx_usrclk2_out		: std_logic;                  				 
signal gt1_userclk_tx_usrclk4_out		: std_logic;  
     
signal SR0_control_DTO						: std_logic_vector(63 DOWNTO 0);
signal SR1_control_DTO						: std_logic_vector(63 DOWNTO 0); 
 
signal clock_SR_out                         : std_logic;
signal clock_SR_resetp                      : std_logic;

 
--****************************************************************************
--**********    CODE   START     HERE    *************************************
--****************************************************************************
begin

reset_resync_i0:entity work.resetp_resync
port map(
	aresetp			=> resetp_counters,
	clock			=> clock_SR_out,
					 
	Resetp_sync		=>  clock_SR_resetp
	);
	
--
--Clock reference for SlinkRocket
--		
IBUFDS_GTE4_inst : IBUFDS_GTE4 
port map (
	O     	=> SRI_ref, -- 1-bit output: Refer to Transceiver User Guide
	--ODIV2 	=> ODIV2, -- 1-bit output: Refer to Transceiver User Guide
	CEB   	=> '0', -- 1-bit input: Refer to Transceiver User Guide 
	I     	=> SRI_ref_clkp, -- 1-bit input: Refer to Transceiver User Guide 
	IB 		=> SRI_ref_clkn -- 1-bit input: Refer to Transceiver User Guide
); 

qpll_reset_out  <= qpll_reset0_out and qpll_reset1_out; 	
	
QPLLi1:entity work.QPLL_wrapper_select 
 Generic map (throughput			=> throughput,
			  ref_clock				=> ref_clock,
			  technology			=> technology
				) 
 Port map( 
	gtrefclk00_in			=> SRI_ref, 
	gtrefclk01_in       	=> '0', 
	qpll0reset_in       	=> qpll_reset_out, 
	qpll1reset_in       	=> '0', 
							 
	qpll0lock_out        	=> qpll_lock_out, 
	qpll0outclk_out     	=> qpll_clkin, 
	qpll0outrefclk_out  	=> qpll_ref_clkin  
							 
	-- qpll1lock_in        	=> , 
	-- qpll1outclk_out     	=> , 
	-- qpll1outrefclk_out  	=>   
 );                              
 
SR0_receiver:entity work.SR_Receiver  
generic map(SR_offset           => SR0_offset,
			Clock_source		=> "Slave",
            throughput			=> throughput,
			ref_clock			=> ref_clock,
			technology			=> technology ,
            UltraRam_mem        => UltraRam_mem  
				)
 Port map(
    usr_clk           			=> usr_clk       , 
    rst_usr_clk_n     			=> rst_usr_clk_n , 
    usr_func_wr       			=> usr_func_wr   , 
    usr_wen           			=> usr_wen       , 
    usr_data_wr       			=> usr_data_wr   , 
    usr_func_rd       			=> usr_func_rd   , 
    usr_rden          			=> usr_rden      , 
    usr_dto_receiver  			=> SR0_control_DTO, 
    resetp_counters     		=> resetp_counters, 
									 
	clk_freerun_in				=> clk_freerun_in, 
									 
	clock_SR					=> clock_SR0, 
	SR_WEN  					=> SR0_WEN  , 
	SR_UCTRL					=> SR0_UCTRL, 
	SR_DATA 					=> SR0_DATA , 
	SR_LFF  					=> SR0_LFF  , 
									  
    gtS_Reset_TX_clock_out		=> gt0_Reset_TX_clock_out	, 
    gtS_userclk_tx_active_in	=> gt1_userclk_tx_active_out,  
    gtS_userclk_tx_usrclk_in	=> gt1_userclk_tx_usrclk_out,  
    gtS_userclk_tx_usrclk2_in	=> gt1_userclk_tx_usrclk2_out,  
    gtS_userclk_tx_usrclk4_in	=> gt1_userclk_tx_usrclk4_out,   
									 
    qpll_lock_in                => qpll_lock_out	, 
    qpll_reset_out              => qpll_reset0_out		, 
    qpll_clkin                  => qpll_clkin		, 
    qpll_ref_clkin              => qpll_ref_clkin	, 
	SRI_gt_rxn_in     			=> SRI_gt_rxn_in(0) , 
    SRI_gt_rxp_in     			=> SRI_gt_rxp_in(0) , 
    SRI_gt_txn_out    			=> SRI_gt_txn_out(0), 
    SRI_gt_txp_out    			=> SRI_gt_txp_out(0)  
);	
   
 
SR1_receiver:entity work.SR_Receiver  
generic map(SR_offset           => SR1_offset,
			Clock_source		=> "Master",
            throughput			=> throughput,
			ref_clock			=> ref_clock,
			technology			=> technology ,
            UltraRam_mem        => UltraRam_mem  
				)
 Port map(
    usr_clk           			=> usr_clk       , 
    rst_usr_clk_n     			=> rst_usr_clk_n , 
    usr_func_wr       			=> usr_func_wr   , 
    usr_wen           			=> usr_wen       , 
    usr_data_wr       			=> usr_data_wr   , 
    usr_func_rd       			=> usr_func_rd   , 
    usr_rden          			=> usr_rden      , 
    usr_dto_receiver  			=> SR1_control_DTO, 
    resetp_counters     		=> resetp_counters, 
									 
	clk_freerun_in				=> clk_freerun_in, 
									 
	clock_SR					=> clock_SR1, 
	SR_WEN  					=> SR1_WEN  , 
	SR_UCTRL					=> SR1_UCTRL, 
	SR_DATA 					=> SR1_DATA , 
	SR_LFF  					=> SR1_LFF  , 

    gtM_Reset_TX_clock_in(0) 	=> gt0_Reset_TX_clock_out, 
    gtM_Reset_TX_clock_in(2 downto 1) => "00", 
    gtM_userclk_tx_active_out	=> gt1_userclk_tx_active_out, 
    gtM_userclk_tx_usrclk_out	=> gt1_userclk_tx_usrclk_out, 
    gtM_userclk_tx_usrclk2_out	=> gt1_userclk_tx_usrclk2_out, 
    gtM_userclk_tx_usrclk4_out	=> gt1_userclk_tx_usrclk4_out, 
	
    qpll_lock_in                => qpll_lock_out	, 
    qpll_reset_out              => qpll_reset1_out		, 
    qpll_clkin                  => qpll_clkin		, 
    qpll_ref_clkin              => qpll_ref_clkin	, 
	SRI_gt_rxn_in     			=> SRI_gt_rxn_in(1) , 
    SRI_gt_rxp_in     			=> SRI_gt_rxp_in(1) , 
    SRI_gt_txn_out    			=> SRI_gt_txn_out(1), 
    SRI_gt_txp_out    			=> SRI_gt_txp_out(1)  
);	
   
 

usr_dto_receiver	<= SR0_control_DTO or SR1_control_DTO;   
end Behavioral;
