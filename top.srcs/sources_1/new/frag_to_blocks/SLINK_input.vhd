----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 17.10.2018 15:02:21
-- Design Name: 
-- Module Name: SLINK_input - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use work.address_table.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity SLINK_input is
  generic (	addr_offset_evt_gen 	:integer := 0;
  			addr_offset_Slink 	:integer := 0);
  Port ( 
		usr_clk								: in std_logic;
		usr_rst_n							: in std_logic;
		usr_func_wr							: in std_logic_vector(16383 downto 0); 
		usr_wen								: in std_logic;
		usr_data_wr							: in std_logic_vector(63 downto 0); 
				
		usr_func_rd					      	: in std_logic_vector(16383 downto 0); 
		usr_rden					      	: in std_logic;
		usr_data_rd					      	: out std_logic_vector(63 downto 0);

		clock_fed							: in std_logic;
		
		clock_SR							: in std_logic;
		SR_DATA								: in std_logic_vector(127 downto 0);
		SR_UCTRL							: in std_logic;
		SR_WEN								: in std_logic;
		SR_LFF								: out std_logic; --active high , if not set it accepts a block of SR 4Kbytes
		 
		ext_trigger							: in std_logic;
		ext_veto_out						: out std_logic;
		
		clock_Buffer						: in std_logic;
		SR_read_data_for_packet				: in std_logic;
		SR_ready_data_for_packet			: out std_logic;
		SR_buffered_datao					: out std_logic_vector(127 downto 0);
		SR_buffered_uctrlo					: out std_logic
		
  );
end SLINK_input;

--*********************************************************************
--*******************   ARCHITECTURE          *************************
--*********************************************************************
architecture Behavioral of SLINK_input is
 
 
signal usr_data_rd_reg						: std_logic_vector(63 downto 0);
 
COMPONENT FIFO_128x2048_resync is
  PORT (
    rst         : IN STD_LOGIC;
    wr_clk      : IN STD_LOGIC;
    rd_clk      : IN STD_LOGIC;
    din         : IN STD_LOGIC_VECTOR(128 DOWNTO 0);
    wr_en       : IN STD_LOGIC;
    rd_en       : IN STD_LOGIC;
    dout        : OUT STD_LOGIC_VECTOR(128 DOWNTO 0);
    full        : OUT STD_LOGIC;
    empty       : OUT STD_LOGIC;
    prog_full   : OUT STD_LOGIC;
    wr_rst_busy : OUT STD_LOGIC;
    rd_rst_busy : OUT STD_LOGIC
  );
END COMPONENT;

COMPONENT FIFO_128x256_buffer is
  PORT (
    clk         : IN STD_LOGIC;
    srst        : IN STD_LOGIC;
    din         : IN STD_LOGIC_VECTOR(128 DOWNTO 0);
    wr_en       : IN STD_LOGIC;
    rd_en       : IN STD_LOGIC;
    dout        : OUT STD_LOGIC_VECTOR(128 DOWNTO 0);
    full        : OUT STD_LOGIC;
    empty       : OUT STD_LOGIC;
    prog_full   : OUT STD_LOGIC;
    wr_rst_busy : OUT STD_LOGIC;
    rd_rst_busy : OUT STD_LOGIC
  );
END COMPONENT;


signal usr_data_gen_o									: std_logic_vector(63 downto 0);
signal usr_rst_p					                    : std_logic;

signal GEN_DATA						                    : std_logic_vector(127 downto 0);
signal GEN_UCTRL					                    : std_logic;
signal GEN_WEN						                    : std_logic; 

signal encap_datao					                    : std_logic_vector(127 downto 0);
signal encap_uctrlo					                    : std_logic;
signal encap_enao					                    : std_logic; 
 
signal FED_ID_err					                    : std_logic; 
 
signal backpressure_emu_counter				  	  		: std_logic_vector(31 downto 0);
signal backpressure_cDAQ_counter						: std_logic_vector(31 downto 0);
signal backpressure_emu_counter_Latched				    : std_logic_vector(31 downto 0);
signal backpressure_cDAQ_counter_Latched				: std_logic_vector(31 downto 0);
signal FED_ID				                            : std_logic_vector(31 downto 0);
signal wrong_fed_ID				                        : std_logic_vector(31 downto 0);
signal Max_frag_size				                    : std_logic_vector(31 downto 0);
signal nb_of_cut_frag				                    : std_logic_vector(31 downto 0);
signal rcv_max_size				                        : std_logic_vector(31 downto 0);
signal current_frag_size				                : std_logic_vector(31 downto 0);  
 
signal FIFO_resync_almost_full_n						: std_logic; 						 
signal Latch_for_mon									: std_logic;						 
signal Latch_for_mon_sync								: std_logic;						 
signal Latch_for_mon_emu_sync							: std_logic; 					 
signal clkfed_resync_resetp								: std_logic; 	

signal SEL_emu											: std_logic; 
signal mux_SLINK_DATA			         				: std_logic_vector(127 downto 0);
signal mux_SLINK_UCTRL			         				: std_logic;
signal mux_SLINK_ENA			         				: std_logic;

signal FIFO_GEN_DATA			         				: std_logic_vector(127 downto 0);
signal FIFO_GEN_UCTRL			         				: std_logic;
signal empty_GEN_resync_FIFO			 				: std_logic;
signal FIFO_resync_gen_almost_full		 				: std_logic;
signal read_GEN_resync_FIFO				 				: std_logic;  

signal FIFO_SR_DATA			         					: std_logic_vector(127 downto 0);
signal FIFO_SR_UCTRL			         				: std_logic;
signal empty_SR_resync_FIFO			 					: std_logic;
signal FIFO_resync_SR_almost_full		 				: std_logic;
signal read_SR_resync_FIFO				 				: std_logic; 
signal FIFO_resync_SR_almost_full_n	 					: std_logic;

signal Wait_to_read_sync_FIFO			 				: std_logic;
signal almost_full_buffer				 				: std_logic;

signal resetn_Buffer						 			: std_logic;
signal resetp_mem						 				: std_logic;

signal tmp												: std_logic_vector(1 downto 0);

attribute mark_debug : string;
attribute mark_debug of FIFO_resync_SR_almost_full  			: signal is "true"; 
attribute mark_debug of SEL_emu     			: signal is "true"; 
--attribute mark_debug of FIFO_SR_DATA     		: signal is "true"; 
--attribute mark_debug of read_SR_resync_FIFO     		: signal is "true"; 
--attribute mark_debug of SR_DATA     			: signal is "true"; 
attribute mark_debug of SR_wen     		: signal is "true"; 
--attribute mark_debug of Wait_to_read_sync_FIFO     		: signal is "true";  

--****************************************************************
--*****************    CODE    START     HERE   ******************
--****************************************************************
begin

usr_rst_p	<= not(usr_rst_n);

--*********************************************************************************************************
--		EVENT Generator
--*********************************************************************************************************   
i1_event_gen:entity work.Event_generator 	
	generic map (
		addr_offset 					=> addr_offset_evt_gen
	)
	port map
	(
		usr_clk						=> usr_clk					, 
		usr_rst_n					=> usr_rst_n				, 
		usr_func_wr					=> usr_func_wr				,  
		usr_wen						=> usr_wen					, 
		usr_data_wr					=> usr_data_wr				, 
			                           	                          
		usr_func_rd					=> usr_func_rd				,  
		usr_rden					=> usr_rden					, 
		usr_data_rd					=> usr_data_gen_o			, 
			                        
		dt_clock					=> clock_fed				, 	-- max 190 Mhz for 25 Gb/s
		                           
		event_data_word				=> GEN_DATA					, 
		event_ctrl					=> GEN_UCTRL				, 
		event_data_wen				=> GEN_WEN					, 
		backpressure				=> FIFO_resync_gen_almost_full	,	-- backpressure when '1'
		ext_trigger					=> ext_trigger					,
		ext_veto_out				=> ext_veto_out								 
	 );
	 
--  FIFO for generator --------------------------------------

i2_FIFO:FIFO_128x2048_resync
  PORT MAP(
    rst         		=>	usr_rst_p					    , --reset resync done in the IP
    wr_clk      		=>	clock_fed			            , 
	din(127 downto 0)	=>	GEN_DATA			            , 
	din(128)        	=>	GEN_UCTRL			            , 
    wr_en       		=>	GEN_WEN			                , 
                                                              
    rd_clk      		=>	clock_Buffer			            , 
    rd_en       		=>	read_GEN_resync_FIFO			, 
    dout(127 downto 0)  =>	FIFO_GEN_DATA			        , 
    dout(128)        	=>	FIFO_GEN_UCTRL			        , 
	                                                          
    --full        		=>				                    , 
    empty       		=>	empty_GEN_resync_FIFO			, 
    prog_full   		=>	FIFO_resync_gen_almost_full		 
    -- wr_rst_busy 		=>				, 
    -- rd_rst_busy 		=>				  
  );

process(empty_GEN_resync_FIFO,Wait_to_read_sync_FIFO,almost_full_buffer,clock_Buffer)
begin
	if empty_GEN_resync_FIFO = '1' or Wait_to_read_sync_FIFO = '1' or almost_full_buffer = '1' then  --- do no tread when FIFO is EMPTY or Generate wait is ON or Backpressure
		read_GEN_resync_FIFO		<= '0';
	elsif rising_edge(clock_Buffer) then
		read_GEN_resync_FIFO		<= '1';
	end if;
end process;

 	 

--emu_mode_STTS <= x"4" when EXT_VETO = '1' else x"8";
--*********************************************************************************************************
--		PCI status read
--*********************************************************************************************************
process(usr_clk)
begin
	 if rising_edge(usr_clk) then
		usr_data_rd_reg		<= (others => '0');
		 
		if 		usr_func_rd(SLINKRocket_FEDx_link_setup 				+ addr_offset_Slink) = '1' then	
			usr_data_rd_reg(0)				<= SEL_emu; 
		elsif 	usr_func_rd(SLINKRocket_FEDx_backpressure_emu_counter 	+ addr_offset_Slink) = '1' then	
			usr_data_rd_reg(31 downto 0)	<= backpressure_emu_counter_Latched;
		elsif 	usr_func_rd(SLINKRocket_FEDx_cDAQ_Backpressure 			+ addr_offset_Slink) = '1' then	
			usr_data_rd_reg(31 downto 0)	<= backpressure_cDAQ_counter_Latched;	
		elsif 	usr_func_rd(SLINKRocket_FEDx_FEDID 						+ addr_offset_Slink) = '1' then	
			usr_data_rd_reg(31 downto 0)	<= FED_ID;
			usr_data_rd_reg(63 downto 32)	<= wrong_fed_ID;
		elsif 	usr_func_rd(SLINKRocket_FEDx_Max_frag_size 				+ addr_offset_Slink) = '1' then	
			usr_data_rd_reg(31 downto 0)	<= Max_frag_size;
		elsif 	usr_func_rd(SLINKRocket_FEDx_nb_of_cut_frag 			+ addr_offset_Slink) = '1' then	
			usr_data_rd_reg(31 downto 0)	<= nb_of_cut_frag;
		elsif 	usr_func_rd(SLINKRocket_FEDx_rcv_max_size 				+ addr_offset_Slink) = '1' then	
			usr_data_rd_reg(31 downto 0)	<= rcv_max_size;
		elsif 	usr_func_rd(SLINKRocket_FEDx_current_frag_size 			+ addr_offset_Slink) = '1' then	
			usr_data_rd_reg(31 downto 0)	<= current_frag_size;  
		-- elsif 	usr_func_rd(22) = '1' then	
			-- usr_data_rd_reg					<=  ;
			
		else
			usr_data_rd_reg 				<=  usr_data_gen_o;	
		end if;
	 
	end if;
end process;		

usr_data_rd		<= usr_data_rd_reg;
		
--*******************************************************************************************************		
process(usr_rst_n,usr_clk)
begin
	if usr_rst_n = '0'  then
			Max_frag_size(Max_frag_size_bits)	<= (others => '1'); 
			FED_ID								<= (others => '0'); 
			SEL_emu								<= '0'; 
			Latch_for_mon						<= '0'; 
	elsif rising_edge(usr_clk) then
		Latch_for_mon							<= '0';
		if usr_wen = '1' then

			if 	usr_func_wr(SLINKRocket_FEDx_FEDID 			+ addr_offset_Slink) = '1' then
				FED_ID											<= usr_data_wr(31 downto 0);
			end if;
			
			if usr_func_wr(SLINKRocket_FEDx_Max_frag_size 	+ addr_offset_Slink) = '1' then
				Max_frag_size									<= usr_data_wr(31 downto 0);
			end if;		
			
			if usr_func_wr(SLINKRocket_FEDx_link_setup 		+ addr_offset_Slink) = '1' then
				SEL_emu											<= usr_data_wr(0);
				Latch_for_mon									<= usr_data_wr(1);
			end if;			
			
		end if;
	end if;
end process;
 
--*********************************************************************************************************
--		STATUS counters
--*********************************************************************************************************  

process(clkfed_resync_resetp,clock_fed)
begin
	if clkfed_resync_resetp = '1' then 
		backpressure_emu_counter	<= (others => '0');
		backpressure_cDAQ_counter	<= (others => '0');

	elsif rising_edge(clock_fed) then
		if  FIFO_resync_gen_almost_full = '1' then
			backpressure_emu_counter <= backpressure_emu_counter + '1';
		end if;
						
		if  FIFO_resync_SR_almost_full = '1' then
			backpressure_cDAQ_counter <= backpressure_cDAQ_counter + '1';
		end if;
				
	end if;
end process;
 
 	
latch_resync_pulse_i1:entity work.resync_pulse  
port map(
	aresetn			=> usr_rst_n,
	clocki			=> usr_clk,
	in_s			=> Latch_for_mon,
	clocko			=> clock_fed,
	out_s			=> Latch_for_mon_sync 
	);
	
latch_resync_pulse_i2:entity work.resync_pulse  
port map(
	aresetn			=> usr_rst_n,
	clocki			=> usr_clk,
	in_s			=> Latch_for_mon,
	clocko			=> clock_fed,
	out_s			=> Latch_for_mon_emu_sync 
	);	

reset_resync_i1:entity work.resetp_resync 
port map (
	aresetp			=>	usr_rst_p,
	clock			=>	clock_fed,
	Resetp_sync		=>	clkfed_resync_resetp 
	); 
	
--*************************************************************************************************************	
process(clock_fed)
begin
	if rising_edge(clock_fed) then
		if Latch_for_mon_sync = '1' then
			backpressure_cDAQ_counter_Latched 	<= backpressure_cDAQ_counter;
		end if;	
	end if;
end process;

process(clock_fed)
begin
	if rising_edge(clock_fed) then
		if Latch_for_mon_emu_sync = '1' then
			backpressure_emu_counter_Latched <= backpressure_emu_counter;
		end if;	
	end if;
end process;
 
 
--*********************************************************************************************************
--		FIFO used to change the clock domaine  for Generator (emulator)
--*********************************************************************************************************

--  FIFO for SR data --------------------------------------

i1_FIFO:FIFO_128x2048_resync
  PORT MAP(
    rst         		=>	usr_rst_p					    , --reset resync done in the IP
    wr_clk      		=>	clock_SR			            , 
	din(127 downto 0)	=>	SR_DATA			            	, 
	din(128)        	=>	SR_UCTRL			            , 
    wr_en       		=>	SR_WEN			                , 
                                                              
    rd_clk      		=>	clock_Buffer			        , 
    rd_en       		=>	read_SR_resync_FIFO				, 
    dout(127 downto 0)  =>	FIFO_SR_DATA			        , 
    dout(128)        	=>	FIFO_SR_UCTRL			        , 
	                                                          
    --full        		=>				                    , 
    empty       		=>	empty_SR_resync_FIFO			, 
    prog_full   		=>	FIFO_resync_SR_almost_full		 
    -- wr_rst_busy 		=>				, 
    -- rd_rst_busy 		=>				  
  );
 
SR_LFF	<= FIFO_resync_SR_almost_full;

process(empty_SR_resync_FIFO,Wait_to_read_sync_FIFO,almost_full_buffer,clock_Buffer)
begin
	if empty_SR_resync_FIFO = '1' or Wait_to_read_sync_FIFO = '1' or almost_full_buffer = '1' then  --- do no tread when FIFO is EMPTY or Generate wait is ON or Backpressure
		read_SR_resync_FIFO		<= '0';
	elsif rising_edge(clock_Buffer) then
		read_SR_resync_FIFO		<= '1';
	end if;
end process;



--*********************************************************************************************************
--		switch between SLINKRocket link or emulator
--********************************************************************************************************* 
-- process(clock_Buffer)
  process(SEL_emu,FIFO_SR_DATA,FIFO_SR_UCTRL,read_SR_resync_FIFO,FIFO_GEN_DATA,FIFO_GEN_UCTRL,read_GEN_resync_FIFO)
  begin
	 --if rising_edge(clock_Buffer) then
		 if SEL_emu = '0' then
			mux_SLINK_DATA		<= FIFO_SR_DATA;
			mux_SLINK_UCTRL		<= FIFO_SR_UCTRL;
			mux_SLINK_ENA		<= read_SR_resync_FIFO;
		 else                 
			mux_SLINK_DATA		<= FIFO_GEN_DATA;
		    mux_SLINK_UCTRL		<= FIFO_GEN_UCTRL;
		    mux_SLINK_ENA		<= read_GEN_resync_FIFO;
		 end if;
	 --end if;
  end process;

--*********************************************************************************************************
--		Encapsulation block (check FEDID, CRC and cut fragment if too large)
--*********************************************************************************************************

i1_encap:entity work.Encapsulate_evt  
port map
(	
	reset					            => resetn_Buffer,							
	clock					            => clock_Buffer,							
	data					            => mux_SLINK_DATA,				-- change to 3 following lines if you mux with real SLINKRocket				
	uctrl					            => mux_SLINK_UCTRL,						
	ena						            => mux_SLINK_ENA,			
	
	FED_ID					            => FED_ID,
	
	data_o					            => encap_datao,						
	uctrl_o					            => encap_uctrlo,						
	ena_o					            => encap_enao,							
	Max_frag_size			            => Max_frag_size,	
	nb_of_cut_frag			            => nb_of_cut_frag,	
	rcv_max_size			            => rcv_max_size,		
	current_frag_size(30 downto 0)		=> current_frag_size(30 downto 0),
	current_frag_size(31)				=> tmp(0)				,
	Error_size				            => current_frag_size(31),			
	wait_to_read_dt			            => Wait_to_read_sync_FIFO,	 				
	wrong_FEDID(30 downto 0)            => wrong_fed_ID(30 downto 0) ,
	wrong_FEDID(31)						=> tmp(1),
	FED_ID_error						=> FED_ID_err
);

process(resetn_Buffer,clock_Buffer)	
begin
	if resetn_Buffer = '0' then
		wrong_fed_ID(31)							<= '0';
	elsif rising_edge(clock_Buffer) then		
		if FED_ID_err = '1' then
			wrong_fed_ID(31)						<= '1';
		end if;
	end if;
end process;
	
--*********************************************************************************************************
--		CHECK  Trigger#, CRC
--********************************************************************************************************* 	
	
-- i1_check:check_slink_XGMII  
-- port map(
	-- Greset_PCI				    => reset_PCI, 							
	-- PCIe interface	        => , 										
	-- pcie_clk					=> clock_PCI, 							
	-- pcie_wr					    => PCI_wen, 							
	-- pcie_func				    => usr_func_rd(SLINKRocket_check_event), 						
	-- pcie_cs					    => PCI_cs, 								
	-- pcie_dti					=> PCI_datai, 							
	-- pcie_dto					=> check_PCI_datao, 					
	-- SLINK input	            => , 										
	-- clock						=> clock, 						
	-- wen						    => encap_enao, 						
	-- uctrl						=> encap_uctrlo, 						
	-- data						=>	encap_datao, 
   -- BACKP						=>	not(FIFO_resync_almost_full_n),	
	-- result			        => , 										
	-- reset_gen				    => cnt_reset, 							
	-- crc_FED_err				=> , 									
	-- pckt_OK					    => Fragment_rcv, 						
	-- pckt_KO					    => Fragment_rcv_wth_error, 		
	-- expect_trig_nb			    => expect_trig_nb(23 downto 0), 	
	-- bad_trig_num			    => bad_trig_num(23 downto 0)  	
-- ); 
FIFO_resync_almost_full_n   <= '1';
--*********************************************************************************************************
--		FIFO buffer
--********************************************************************************************************* 
reset_resync_i2:entity work.resetp_resync 
port map(
	aresetp				=>	usr_rst_p,
	clock				=>	clock_Buffer,
	Resetp_sync			=>	resetp_mem 
	); 

resetn_Buffer	<= not(resetp_mem);

i2_FIFO_Buf:FIFO_128x256_buffer
  PORT MAP(
    srst        		=> resetp_mem			, 
    clk         		=> clock_Buffer			, 
    din(127 downto 0)	=> encap_datao			, 
    din(128)			=> encap_uctrlo			, 
	wr_en       		=> encap_enao			, 
                                                  
	rd_en       		=> SR_read_data_for_packet	, 
    dout(127 downto 0)	=> SR_buffered_datao      	,  
    dout(128)			=> SR_buffered_uctrlo     	,  
    -- full        		=>			,             
    empty       		=> SR_ready_data_for_packet, 
    prog_full   		=> almost_full_buffer	  
    -- wr_rst_busy 		=>			, 
    -- rd_rst_busy 		=>			  
  );
 
 
end Behavioral;
