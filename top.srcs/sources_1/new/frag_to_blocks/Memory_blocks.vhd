
------------------------------------------------------
-- MEMORY TO STORE FRAGMENT DIVIDED IN 4KBYTES BLOCKS 
-- WITH A HEADER IN THE FRONT TO SPECIFY THE CONTENT
-- OF THE BLOCK
--  Ver 6.00
--
-- Dominique Gigi JULY 2012
------------------------------------------------------
--   Remove the 2 link , will work with only one link
--   
--  remove the cut fragment
------------------------------------------------------
LIBRARY ieee;

USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

entity memory_blocks is
generic (	swapp : boolean := true;
			addr_offset_100G_eth		: integer := 0);
port (
	reset						: IN STD_LOGIC;
	CLOCK						: IN STD_LOGIC;
	
	-- INPUT DATA FROM SLINK 0-1
	Read_dt_from_SR				: OUT STD_LOGIC; -- used to read the intermediate FIFO
	Data_ready_from_SR			: IN STD_LOGIC;
	Data_i_from_SR				: IN STD_LOGIC_VECTOR(127 DOWNTO 0);
	Uctrl_i_from_SR				: IN STD_LOGIC;		-- active high
	
	-- OUTPUT DATA TO Memory Stream 
	Data_o_to_UR				: OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
	Size_ready_to_UR			: OUT STD_LOGIC_VECTOR(10 DOWNTO 0); -- SIZE READY TO BE TRANSFERED (IF 0x000 AND NOT Data_ready_to_UR (4kBYTES)
	Data_ready_to_UR			: OUT STD_LOGIC; -- DATA IN fifo
	Read_data_for_UR			: IN STD_LOGIC;

	
	-- PARAMETERS READABLE BY PCI SLAVE ACCESS
	TRIGGER						: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
	FED							: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
	BX							: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
	Incr_evt					: OUT STD_LOGIC;
	
	LATCH_WC_BX					: OUT STD_LOGIC;
	ENA_LATCH					: IN STD_LOGIC;
	WC_histo					: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
	BX_histo					: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
	
	status_debug				: OUT STD_LOGIC_VECTOR(31 downto 0);
	internal_LFF				: OUT STD_LOGIC
	);
	
end memory_blocks;

--****************************************************************
--**************************   architecture  *********************
--****************************************************************
architecture behavioral of memory_blocks is


COMPONENT MEMORY_DP
  PORT (
    clka 	: IN STD_LOGIC;
    ena 	: IN STD_LOGIC;
    wea 	: IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    addra 	: IN STD_LOGIC_VECTOR(10 DOWNTO 0);
    dina 	: IN STD_LOGIC_VECTOR(127 DOWNTO 0);
    clkb 	: IN STD_LOGIC;
    enb 	: IN STD_LOGIC;
    addrb 	: IN STD_LOGIC_VECTOR(10 DOWNTO 0);
    doutb 	: OUT STD_LOGIC_VECTOR(127 DOWNTO 0)
  );
END COMPONENT;
 
type EVENT_type is (	Idle,
						wait_dt,
						data,
						switch_st,
						blkHd_a,
						freeHd_a,
						freeHd_b
					);
signal EVENT_s:EVENT_type;

--signal name : STD_LOGIC_VECTOR(xx downto 0);
signal preset 						: STD_LOGIC;
signal Sync_reload				    : STD_LOGIC_VECTOR (1 DOWNTO 0);
				
signal dt_in_blk					: STD_LOGIC_VECTOR (15 DOWNTO 0);
signal rst_blk						: STD_LOGIC;
signal HDa							: STD_LOGIC_VECTOR(127 downto 0);
				
signal wen_ff						: STD_LOGIC;
signal wen_dt						: STD_LOGIC;
signal inc_blk_sz					: STD_LOGIC;
signal add_wr_d					    : STD_LOGIC_VECTOR (11 DOWNTO 0):= "000000000001";
signal add_wr_h					    : STD_LOGIC_VECTOR (11 DOWNTO 0):= (others => '0');
signal add_wr_p					    : STD_LOGIC_VECTOR (11 DOWNTO 0):= (others => '0');
 
signal mux_addw					    : STD_LOGIC_VECTOR (10 DOWNTO 0);
signal mux_dataw					: STD_LOGIC_VECTOR (127 DOWNTO 0);
signal mux_dataw_swapped			: STD_LOGIC_VECTOR (127 DOWNTO 0);
signal temp_dataw					: STD_LOGIC_VECTOR (127 DOWNTO 0);
signal reg_uctrl					: STD_LOGIC;
signal update_ptrw				    : STD_LOGIC; 
signal diff_w_p					    : STD_LOGIC_VECTOR (10 DOWNTO 0);
signal add_wr_rdy					: STD_LOGIC_VECTOR (11 DOWNTO 0);
signal dt_hd						: STD_LOGIC;
signal WEN_hd						: STD_LOGIC;
signal rd_mem						: STD_LOGIC;
				
signal add_rd						: STD_LOGIC_VECTOR (11 DOWNTO 0) := (others => '0');
signal check_ov_r					: STD_LOGIC;
signal diff_r_p					    : STD_LOGIC_VECTOR (10 DOWNTO 0):= (others => '0');
			
signal LFF_r						: STD_LOGIC;
signal packet						: STD_LOGIC_VECTOR(11 downto 0):= (others => '0');
signal rst_pkt						: STD_LOGIC;
signal cnt_pkt						: STD_LOGIC;
signal LFF							: STD_LOGIC;
			
signal pre_empt					    : STD_LOGIC_VECTOR(1 downto 0);
signal WC_histo_reg				    : STD_LOGIC_VECTOR(31 DOWNTO 0);
signal BX_histo_reg				    : STD_LOGIC_VECTOR(31 DOWNTO 0);
signal latch_reg					: std_logic;


signal TRIGGER_reg				    : STD_LOGIC_VECTOR(31 DOWNTO 0);
signal FED_reg						: STD_LOGIC_VECTOR(31 DOWNTO 0);
signal BX_reg						: STD_LOGIC_VECTOR(11 DOWNTO 0);

signal Inc_trig_num				    : STD_LOGIC;


signal status_debug_cell		    : STD_LOGIC_VECTOR(31 downto 0) := x"00000000";

attribute mark_debug : string;
attribute mark_debug of EVENT_s  : signal is "true"; 
attribute mark_debug of Data_ready_from_SR  : signal is "true"; 
attribute mark_debug of LFF     : signal is "true"; 
attribute mark_debug of add_wr_d     : signal is "true"; 
attribute mark_debug of add_wr_h     : signal is "true"; 
attribute mark_debug of add_rd     : signal is "true"; 
attribute mark_debug of diff_w_p     : signal is "true"; 
attribute mark_debug of wen_ff     : signal is "true"; 
attribute mark_debug of mux_addw     : signal is "true"; 
--attribute mark_debug of Data_o_to_UR     : signal is "true"; 

--****************************************************************
--**************************   BEGIN    **************************
--****************************************************************
begin 
-- positive reset signal
preset <= not(reset);

-- resync preset for the Synchronous load 
process(preset,clock)
begin
	if preset = '1' then
		Sync_reload		<= (others => '1');
	elsif rising_edge(clock) then 
		Sync_reload(1) 	<= Sync_reload(0);
		Sync_reload(0)	<= preset;
	end if;
end process;


--****************************************************************************************************************************************
-- Data_ready_to_UR of memory block (not block used , data can be in the block but he FEROL header is not still writtien and then blcok not finished)
process(reset,clock)
begin
	if reset = '0' then
		Data_ready_to_UR <= '0';
	elsif rising_edge(clock)  then
		Data_ready_to_UR <= pre_empt(0);
	end if;
end process;

rst_blk <= '1' when EVENT_s = wait_dt else '0';

block_size_i1:process(clock,Sync_reload(1))
begin
	if Sync_reload(1) = '1' then
		dt_in_blk		<= (others => '0');
	elsif rising_edge(clock) then
		if rst_blk = '1' then
			dt_in_blk	<= (others => '0');
		elsif inc_blk_sz = '1' then
			dt_in_blk 	<= dt_in_blk + '1';
		end if;
	end if;
end process;

----------------------------------------------------------------
-- state machine for the EVENT  COMING in  
----------------------------------------------------------------
 
Event_st:process(RESET,CLOCK)
begin
	if reset = '0' then
		EVENT_s <= Idle;
	elsif rising_edge(CLOCK) then
		Case EVENT_s is
			when Idle =>
				EVENT_s <= wait_dt;
			when wait_dt =>
				-- wait for data ready from SLINK or GENERATOR
				if Data_ready_from_SR = '0'  then
					EVENT_s <= data;
				end if;
				
			when data =>
				-- check the full of the packet or the end of the fragment
				if ((dt_in_blk = x"00FE" ) or (Uctrl_i_from_SR = '1' and Data_i_from_SR(127 downto 120) = x"AA" )) and inc_blk_sz = '1' then
					EVENT_s <= switch_st;
				end if;
				
			when switch_st =>
				EVENT_s <= blkHd_a;
				
				--write the DTH header in front of the payload block
			when blkHd_a =>
				EVENT_s <= freeHd_a;
				 
				 -- update pointer for the next block
			when freeHd_a =>
				EVENT_s <= freeHd_b;
				
			when freeHd_b =>
				EVENT_s <= wait_dt;
				
			when others =>
				EVENT_s <= Idle;
		end case;
	end if;
end process;

status_debug_cell(8)					<= inc_blk_sz;
status_debug_cell(24 downto 9)			<= dt_in_blk;
status_debug_cell(25)					<= Sync_reload(1);

-- Build BLOCK DTH HEADERs (1 x 128 bit words)
--  (header starts 		(127..112) with x475A
--						(106..96) the block; number first is 0
--						(95) if '1' indicates that the block contains the start of the event fragment
--						(94) if '1' indicates that the block contain the end of the event fragment
--						(73..64) indicate the number of word(64-bit) in the block , max. 4Kbytes
-- 						(43..32) FED number
--						(23..0) Event number
-- Others bit are reserved and should be '0'

process(reset,clock)
begin
	if reset = '0' then
		HDa 			<= (others => '0');
		TRIGGER_reg		<= (others => '0');
		FED_reg			<= (others => '0');
		BX_reg 			<= (others => '0');
		Inc_trig_num	<= '0';
		rst_pkt			<= '0';
	elsif rising_edge(clock) then	
	---------
		HDa(015 downto 000) 	<= x"5A47";
		HDa(027 downto 016) 	<= packet(11 downto 0);										--- packet number
		HDa(031 downto 028) 	<= (others => '0');
		
		rst_pkt					<= '0';
		if reg_uctrl = '1' and temp_dataw(127 downto 120) = x"55" and wen_dt = '1'  then
			HDa(47) 			<= '1';														--- indicated if the packet is the first (start the event)
			rst_pkt				<= '1';
		elsif EVENT_s = wait_dt then
			HDa(47) 			<= '0';
		end if;
		Inc_trig_num			<= '0';
		if reg_uctrl = '1' and temp_dataw(127 downto 120) = x"AA" and wen_dt = '1'  then
			HDa(46) 			<= '1';												       -- indicates if the packet is the last (finish the event)
			Inc_trig_num		<= '1';
		elsif EVENT_s = wait_dt then
			HDa(46) 			<= '0';
		end if;	
		
		HDa(45 downto 44) 		<= (others => '0');
		if reg_uctrl = '1' and temp_dataw(127 downto 120) = x"AA" and wen_dt = '1'  then   --indicates the Bunch Crossing number
			HDa(43 downto 32)	<= temp_dataw(75 downto 64);
			BX_reg				<= temp_dataw(75 downto 64);                               -- only 12 bit are valid
		end if;
		
		HDa(63 downto 48) 		<= dt_in_blk(15 downto 0);                                 --  number of 128-bit words in the packet (16 bit)
		
	 	if reg_uctrl = '1' and temp_dataw(127 downto 120) = x"55" and wen_dt = '1'  then
			HDa(95 downto 64) 	<= temp_dataw(95 downto 64);								--- trigger number (32 bit)
			TRIGGER_reg			<= temp_dataw(95 downto 64);
		end if;

		if reg_uctrl = '1' and temp_dataw(127 downto 120) = x"55" and wen_dt = '1'  then
			HDa(127 downto 96) 	<= temp_dataw(31 downto 00);								--- FED ID (32 bit)
			FED_reg				<= temp_dataw(31 downto 00);
		end if;
		
	---------	
	end if;
end process;

TRIGGER					<= TRIGGER_reg; 
BX(11 downto 0) 		<= BX_reg;
FED						<= FED_reg;
Incr_evt				<= Inc_trig_num;
					
					
process(reset,clock)
begin
	if reset = '0' then
		latch_reg				<= '0';
	elsif rising_edge(clock) then
		if (reg_uctrl = '1' and temp_dataw(127 downto 120) = x"AA" and wen_dt = '1' )  then
			latch_reg			<= '1';
			WC_histo_reg(31 downto 20)   <= (others => '0');
			WC_histo_reg(19 downto 0)    <= temp_dataw(95 downto 76);
			BX_histo_reg(31 downto 12)   <= (others => '0');
			BX_histo_reg(11 downto 0)    <= BX_reg;
		elsif ENA_LATCH = '1' then
			latch_reg			<= '0';
		end if;	
	end if;
end process;


WC_histo				<= WC_histo_reg;
BX_histo				<= BX_histo_reg;
LATCH_WC_BX				<= latch_reg and ENA_LATCH;					

--*************************************************************
-- count the number of blocks used by ONE event fragment      *
--*************************************************************
process(reset,clock)
begin
	if reset = '0' then
		cnt_pkt 		<= '0';
	elsif rising_edge(clock) then
	
		cnt_pkt 		<= '0';
		if EVENT_s = freeHd_a then
			cnt_pkt 	<= '1';
		end if;

	end if;
end process;

Packet_i1:process(clock,Sync_reload(1))
begin
	if Sync_reload(1) = '1' then
		packet		<= (others => '0');
	elsif rising_edge(clock) then
		if rst_pkt = '1' then
			packet	<= (others => '0');
		elsif cnt_pkt = '1' then
			packet 	<= packet + '1';
		end if;
	end if;
end process;

-- read next word if : Iin data State (SM to build block(s) of fragment)   if data is present (from the SR or Evt_gen)  and Memory_Block not almost full 
inc_blk_sz		<= '1' when  (EVENT_s = data and Data_ready_from_SR = '0' and LFF = '1') else '0';

Read_dt_from_SR	<= inc_blk_sz;

-- SIGNALS (WEN, ADD,DATA) to access DUAL port memory in Write side
process(clock)
begin
	if rising_edge(clock) then
		if 		EVENT_s = blkHd_a then
			mux_dataw						<= HDa;
		else
			temp_dataw						<= Data_i_from_SR;
			mux_dataw						<= Data_i_from_SR;
		end if;
		reg_uctrl <= Uctrl_i_from_SR;
		
		if EVENT_s = data then
			mux_addw <= add_wr_d(10 downto 0); -- Address to write Event fragment
		else 
			mux_addw <= add_wr_h(10 downto 0); -- Address to write the two headers in the front of the block
		end if;	
		
	end if;
end process;


--**************************************************************************************
--  swapped DTH header and payload
process(mux_dataw)
begin
	if swapp then
		for I in 0 to 15 loop
			mux_dataw_swapped((8*I)+7 downto (8*I))	<= mux_dataw(127-(8*i) downto 120-(8*i));
		end loop;	
	else
			mux_dataw_swapped   					<= mux_dataw;
	end if;
end process;
--**************************************************************************************

-- Write to the memory 
process(reset,clock)
begin
	if reset = '0' then	
		wen_ff 	<= '0';
		wen_dt	<= '0';
	elsif rising_edge(clock) then	
	
		wen_ff 		<= '0';
		if inc_blk_sz = '1' or EVENT_s = blkHd_a then
			wen_ff 	<= '1';
		end if;
		
		wen_dt 		<= '0';
		if inc_blk_sz = '1' then
			wen_dt 	<= '1';
		end if;
		
	end if;
end process;
 
-- address to write payload
-- this counter is used to compute the FULL FIFO
process(Sync_reload(1),CLOCK)
begin	
	if Sync_reload(1) = '1' then
		add_wr_d	<= "000000000001";
	elsif rising_edge(CLOCK) then
	    -- the counter is increased by payload and the header write 
		if inc_blk_sz= '1' or EVENT_s = freeHd_a then
			add_wr_d	<= add_wr_d + '1';
		end if;
	end if;
end process;

update_ptrw <= '1' when EVENT_s = freeHd_a else '0';	
WEN_hd		<= '1' when EVENT_s = blkHd_a  else '0';

-- address to write Header	
-- this counter is used to write block header and updated after to the Data_ready_to_UR place
-- this counter is used to compute the Data_ready_to_UR FIFO
process(Sync_reload(1),CLOCK)
begin	
	if Sync_reload(1) = '1' then
		add_wr_h	<= (others => '0');
	elsif rising_edge(CLOCK) then
	    -- not need because there is only one header word
--		if		WEN_hd = '1' then
--			add_wr_h	<= add_wr_h + '1';
--		els
		-- update the header position with the end of the block payload pointer 
		if update_ptrw = '1' then
			add_wr_h <= add_wr_d;
		end if;
	end if;
end process;	
	 
-- LFF control	1 NOT FULL  0 FULL
process(reset,CLOCK)
begin
	if reset = '0' then
		diff_w_p <= (others => '0');
	elsif rising_edge(CLOCK) then
		diff_w_p <= add_wr_d(10 downto 0) - add_rd(10 downto 0);
	end if;
end process;
  
--********************************************************************
-- backpressure in term of words (in case of memory block almost FULL)
process(reset,clock)
begin
	if reset = '0' then
		LFF <= '1';
	elsif rising_edge(clock) then
		if 	  (diff_w_p > "11111111000" ) then 
			LFF <= '0';
		elsif (diff_w_p < "11111100000" ) then 
			LFF <= '1';
		end if;
	end if;
end process;

internal_LFF	<= LFF; -- almost full on '0' 




--*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$
--*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$
--Dual port memory to record fragment divided in blocks with a DTH header --*$*$*$
memory_i1:MEMORY_DP                                                       --*$*$*$
PORT MAP                                                                  --*$*$*$
	(                                                                     --*$*$*$
    clka 			=> CLOCK				,                             --*$*$*$
	ena 			=> wen_ff				,                             --*$*$*$
	wea(0) 			=> '1'					,                             --*$*$*$
	addra 			=> mux_addw				,                             --*$*$*$
	dina 			=> mux_dataw_swapped	,                             --*$*$*$
																		  --*$*$*$
	clkb 			=> CLOCK				,                             --*$*$*$
	enb 			=> rd_mem				,                             --*$*$*$
	addrb 			=> add_rd(10 downto 0)	,                             --*$*$*$
	doutb 			=> Data_o_to_UR			 	                          --*$*$*$
	);                                                                    --*$*$*$
--*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$
--*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$*$


--*******************************************************************
-- Store the payload pointer to indicate where we can start to read
-- only when the complet block is written in the memory block
process(reset,clock)
begin
	if reset = '0'then

		add_wr_p		<= (others => '0');
	elsif rising_edge(clock) then
		if update_ptrw = '1' then  
			add_wr_p 	<= add_wr_d;
		end if;
	end if;
end process;
 
 
-- Check if there are any memory block ready to be written in TCP buffer 
-- a block = DTH_Header + payload  //  Payload = a x100 word of 128-b  or less at the end of Event_fragment
process(add_wr_p,add_rd)
begin
	diff_r_p <= add_wr_p(10 downto 0) - add_rd(10 downto 0) ;--+ '1';
end process;

process(clock)
--variable tmp_dif 	: std_logic_vector(10 downto 0);
begin
	if rising_edge(clock) then	
	--	tmp_dif := diff_r_p(10 downto 0);
		if diff_r_p(10 downto 8) = "000" then
			Size_ready_to_UR <= diff_r_p(10 downto 0);
		else
			Size_ready_to_UR <= "00100000000";
		end if;
	end if;
end process;

process(diff_r_p) 
begin
	if diff_r_p = "00000000000"  then
		pre_empt(0) <= '0';
	else
		pre_empt(0) <= '1';
	end if;
end process;
 
process(reset,clock)
begin
	if reset = '0' then
		pre_empt(1) 	<= '0';
	elsif rising_edge(clock) then
		pre_empt(1) 	<= pre_empt(0);
	end if;
end process;


rd_mem <= '1' when (Read_data_for_UR = '1' ) else '0'; 
 
-- address to read data
process(Sync_reload(1),CLOCK)
begin	
	if Sync_reload(1) = '1' then
		add_rd	<= (others => '0');
	elsif rising_edge(CLOCK) then
		if		rd_mem = '1' then
			add_rd	<= add_rd + '1';
		end if;
	end if;
end process;
 
status_debug	<= status_debug_cell;
 
end behavioral;
