--****************************************
--
-- Dominique Gigi  CMS/DAQ
--
--****************************************
-- 2015
-- Add the cut of fragment bigger that the value set
-- bug on CRC computation (reset the CRC logic is the problem) (July 2015)
-- use the USB CRC with clear sync
 
--****************************************


LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;


entity Encapsulate_evt is

	port
	(	reset							: in std_logic;
		clock							: in std_logic;
					
		data							: in std_logic_vector(127 downto 0);
		uctrl							: in std_logic;
		ena							    : in std_logic;
		FED_ID						    : in std_logic_vector(31 downto 0);
		data_o						    : out std_logic_vector(127 downto 0);
		uctrl_o						    : out std_logic;
		ena_o							: out std_logic;
					
		Max_frag_size				    : in std_logic_vector(31 downto 0);
					
		nb_of_cut_frag				    : out std_logic_vector(31 downto 0);
		rcv_max_size				    : out std_logic_vector(31 downto 0);
		current_frag_size			    : out std_logic_vector(31 downto 0);
		Error_size					    : out std_logic;
		wait_to_read_dt			        : out std_logic; 
		wrong_FEDID					    : out std_logic_vector(31 downto 0);
		FED_ID_error				    : out std_logic
	);
end Encapsulate_evt;

 
architecture behavioral of Encapsulate_evt is

attribute mark_debug : string;

component crcUSB_128b_16b is
  port (
		clear_p			: in std_logic;
		clk 			: in std_logic;
		enable 			: in std_logic;
		Data			: in std_logic_vector(127 downto 0);
		CRC_out 		: out std_logic_vector(15 downto 0)
	);
end component;

signal envelop_evt_reg				: std_logic;
signal envelop_evt_reg_delay		: std_logic;
signal data_reg						: std_logic_vector(127 downto 0);
signal uctrl_reg					: std_logic;
signal ena_reg						: std_logic;
signal mask							: std_logic;
		
 
signal data_regb					: std_logic_vector(127 downto 0);
signal uctrl_regb					: std_logic;
signal ena_regb						: std_logic;
signal data_reg_crc					: std_logic_vector(127 downto 0);
		
signal cut_fragment					: std_logic_vector(1 downto 0);
signal max_size						: std_logic_vector(19 downto 0);
signal current_size					: std_logic_vector(19 downto 0);
signal current_size_cell			: std_logic_vector(19 downto 0);
signal Current_size_latched			: std_logic_vector(19 downto 0);
signal reset_size_cnt				: std_logic_vector(1 downto 0);
signal rst_crc						: std_logic_vector(2 downto 0);
signal CRC_out						: std_logic_vector(15 downto 0);  
signal ena_CRC						: std_logic;
signal error_frag_size_reg			: std_logic;
signal wr_trailer					: std_logic;
signal nb_of_cut_frag_cnt			: std_logic_vector(31 DOWNTO 0);
signal pause_read					: std_logic;
signal FEDID_err_reg				: std_logic;
signal FEDID_rcv					: std_logic_vector(31 DOWNTO 0);

--attribute mark_debug of CRC_out			: signal is "true";	 
--attribute mark_debug of mask			: signal is "true"; 
--attribute mark_debug of data_reg_crc    : signal is "true";
--attribute mark_debug of ena_crc			: signal is "true";

--*********************************************
--***************  BEGIN   ********************	 
--*********************************************
begin

current_size_cell   <= current_size + '1'; 

process(reset,clock)
begin
	if reset = '0' then
		envelop_evt_reg 					<= '0';
		ena_reg								<= '0';
		mask									<= '0';
		reset_size_cnt						<= (others => '1');
		rst_crc								<= (others => '1');
		FEDID_err_reg						<= '0';
	elsif rising_edge(clock) then

		if cut_fragment(0) = '1' and cut_fragment(1) = '0' then
			data_reg(127 downto 120)		<= x"A1";
			data_reg(095 downto 076)		<= current_size_cell;
			data_reg(075 downto 048)		<= (others => '0');
		else	
			data_reg						<= data;
		end if;
		
		if cut_fragment(0) = '1' and cut_fragment(1) = '0' then
			data_reg_crc(127 downto 120)	<= x"A1";
			data_reg_crc(095 downto 076)	<= current_size_cell;
			data_reg_crc(075 downto 000)	<= (others => '0');
		elsif uctrl = '1' and ena = '1' and data(127 downto 120) = x"AA" then
			data_reg_crc(127 downto 032)	<= data(127 downto 032);
			data_reg_crc(031 downto 016)	<= x"0000";
			data_reg_crc(015 downto 000)	<= x"0000";
		else 	
			data_reg_crc					<= data;
		end if;
		
		
		
		uctrl_reg						<= '1';
		if uctrl = '1' or (cut_fragment(0) = '1' and cut_fragment(1) = '0') then
			uctrl_reg					<= '0';
		end if;
		
		-- enable data all the time until cut fragment  AND add the new trailer if we cut the fragment
		ena_reg							<= '0';
		if (ena ='1' and cut_fragment(0) = '0') then
			ena_reg						<= '1';
		end if;
		
		wr_trailer						<= '0';
		if cut_fragment(0) = '1' and cut_fragment(1) = '0' then	
			wr_trailer					<= '1';
		end if;
				
		mask							<= '0';
		if uctrl_reg = '0' and data_reg(127 downto 120) = x"AA" and ( ena_reg = '1' or wr_trailer = '1') and envelop_evt_reg = '1' then
			mask						<= '1';
		end if;	
		
		rst_crc(2 downto 1)				<= rst_crc(1 downto 0);	
		reset_size_cnt(1)				<= reset_size_cnt(0);
		rst_crc(0)						<= '0';	
		reset_size_cnt(0)				<= '0';
		if 	uctrl     = '1' and data(127 downto 120)     = x"55" and ena     = '1' then
			envelop_evt_reg				<= '1';
			if FED_ID /= data(31 downto 0)  then
				FEDID_err_reg			<= '1';
				FEDID_rcv				<= data(31 downto 0);
			else
				FEDID_err_reg			<= '0';
			end if;
		elsif uctrl_reg = '0' and data_reg(127 downto 120) = x"AA" and ((ena_reg = '1' and  cut_fragment(0) = '0') or wr_trailer = '1') then 
			envelop_evt_reg				<= '0';
			rst_crc(0)					<= '1';
			reset_size_cnt(0)			<= '1';
		end if;
		
		if uctrl_reg = '0' and data_reg(63 downto 60) = x"A" and ena_reg = '1' then
			Current_size_latched		<= data_reg(95 downto 76);
		end if;
		

		envelop_evt_reg_delay			<= envelop_evt_reg;
		
	end if;
end process;


-- compute the CRC to add it in the artificial TRAILER
ena_crc	<= '1' when (envelop_evt_reg = '1' and ((ena_reg = '1' and  cut_fragment(0) = '0') or wr_trailer = '1')) else '0';

i_crc:crcUSB_128b_16b  
port map(
	clear_p		=> rst_crc(1),
	clk		    => clock,
	Data		=> data_reg_crc,
	enable		=> ena_crc,
	crc_out		=> CRC_out
	);  
 
--monitor the fragment size
process(reset,clock)
begin
	if reset = '0' then
		max_size				<= (others => '0');
	elsif rising_edge(clock) then
		-- compute the fragment size
		if reset_size_cnt(0) = '1' then
			current_size	<= (others => '0');
		elsif envelop_evt_reg = '1' and (ena_reg = '1' or wr_trailer = '1') then
			current_size 	<= current_size + '1';
		end if;
		
		if max_size < current_size then
			max_size 		<= current_size;
		end if;
		
	end if;
end process;

rcv_max_size(31 downto 20)      <= (others => '0');
rcv_max_size(19 downto 0)       <= max_size;
current_frag_size(31 downto 20)	<= (others => '0');
current_frag_size(19 downto 0)	<= Current_size_latched;

-- This process will check the fragment size and will cut the fragment if its size is bigger that the one defined
process(reset,clock)
begin
	if reset = '0' then
		cut_fragment		<= (others => '0');
	elsif rising_edge(clock) then
		cut_fragment(1)		<=	cut_fragment(0);
 
		if reset_size_cnt(1) = '1' then
			cut_fragment(0) <= '0';
		elsif ena_reg = '1' and current_size = Max_frag_size(19 downto 0)  and envelop_evt_reg = '1' and not(uctrl_reg = '0' and ena_reg = '1' and data_reg(63 downto 60) = x"A") then -- if max size and not trailer
			cut_fragment(0) <= '1';
		end if;
	end if;
end process;

-- pause the read to finish the compute of CRC    NOT NEED anymore the CRC is not computed HERE   only if the fragment is CUTTED
--process(reset,clock)
--begin
--	 if reset = '0' then
--		 pause_read				<= '0';
--	 elsif rising_edge(clock) then
--		pause_read				<= '0';
--		if uctrl = '1' and data(127 downto 120) = x"AA" and ena = '1' then
--			pause_read			<= '1';
--		end if;
--	 end if;
--end process;

wait_to_read_dt		<= cut_fragment(0);--(pause_read or 

-- count the number of time the cut event were occured.
process(reset,clock)
begin
	if 	reset = '0' then
			nb_of_cut_frag_cnt <= (others => '0');
	elsif rising_edge(clock) then
		if wr_trailer = '1' then
			nb_of_cut_frag_cnt <= nb_of_cut_frag_cnt + '1';
		end if;
	end if;
end process;

nb_of_cut_frag			<= nb_of_cut_frag_cnt;

-- Check if the fragment size correspond to the one in the trailer
process(reset,clock)
begin
	if reset = '0' then
		error_frag_size_reg			<= '0';
	elsif rising_edge(clock) then
		if reset_size_cnt(0) = '1' and cut_fragment(0) = '0' and envelop_evt_reg_delay = '1' then
			if data_regb(95 downto 76) /= current_size then
				error_frag_size_reg	<= '1';
			end if;
		end if;
	end if;
end process;

Error_size	<=  error_frag_size_reg;

process(reset,clock)
begin
	if reset = '0' then
		 ena_regb			<= '0';
	elsif rising_edge(clock) then
		data_regb			<= data_reg;			--65..0
		uctrl_regb			<= uctrl_reg;
		ena_regb			<= '0';
		if (ena_reg = '1' and cut_fragment(0) = '0' and envelop_evt_reg = '1') or wr_trailer = '1' then
			ena_regb		<= '1';
		end if;
	
	end if;
end process;

data_o(127 downto 32)		<= data_regb(127 downto 32);
data_o(31 downto 16)		<= data_regb(31 downto 16) 		when cut_fragment(0) = '0' else CRC_out; -- include the CRC onl if the fragment was cutted!!
data_o(15 downto 3)         <= data_regb(15 downto 3) ;
data_o(2)					<= data_regb(2)				    when mask = '0'            else FEDID_err_reg;
data_o(1 downto 0)			<= data_regb(1 downto 0); 

uctrl_o						<= not(uctrl_regb);
ena_o						<= ena_regb;

FED_ID_error				<= FEDID_err_reg;
wrong_FEDID					<= FEDID_rcv;
 
end behavioral;
