----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09.07.2019 10:04:19
-- Design Name: 
-- Module Name: SLINK_merger - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use work.address_table.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Event_Builder is
  generic (	addr_offset_100G_eth	 	:integer := 0 );
 Port (
		usr_clk								: in std_logic;
		usr_rst_n							: in std_logic;
		usr_func_wr							: in std_logic_vector(16383 downto 0); 
		usr_wen								: in std_logic;
		usr_data_wr							: in std_logic_vector(63 downto 0); 
		
		usr_func_rd					      	: in std_logic_vector(16383 downto 0); 
		usr_rden					      	: in std_logic;
		usr_data_rd					      	: out std_logic_vector(63 downto 0);

		clock_fed							: in std_logic;
		
		clock_SR0							: in std_logic;
		SR0_DATA							: in std_logic_vector(127 downto 0);
		SR0_UCTRL							: in std_logic;
		SR0_WEN								: in std_logic;
		SR0_LFF								: out std_logic;
		 
		clock_SR1							: in std_logic;
		SR1_DATA							: in std_logic_vector(127 downto 0);
		SR1_UCTRL							: in std_logic;
		SR1_WEN								: in std_logic;
		SR1_LFF								: out std_logic;
		 
		ext_trigger							: in std_logic;
		ext_veto_out						: out std_logic;
		
		clock_Buffer						: in std_logic;
		UR0_read_data_for_packet			: in std_logic;
		UR0_ready_data_for_packet			: out std_logic;
		UR0_buffered_datao					: out std_logic_vector(127 downto 0);
		UR0_buffered_uctrlo					: out std_logic;
		UR1_read_data_for_packet			: in std_logic;
		UR1_ready_data_for_packet			: out std_logic;
		UR1_buffered_datao					: out std_logic_vector(127 downto 0);
		UR1_buffered_uctrlo					: out std_logic
 );
end Event_Builder;

--*********************************************************************
--*******************   ARCHITECTURE          *************************
--*********************************************************************
architecture Behavioral of Event_Builder is

component SLINK_input is
  generic (	addr_offset_evt_gen 	:integer := 0;
  			addr_offset_Slink 	:integer := 0);
  Port ( 
		usr_clk								: in std_logic;
		usr_rst_n							: in std_logic;
		usr_func_wr							: in std_logic_vector(16383 downto 0); 
		usr_wen								: in std_logic;
		usr_data_wr							: in std_logic_vector(63 downto 0); 
		
		usr_func_rd					      	: in std_logic_vector(16383 downto 0); 
		usr_rden					      	: in std_logic;
		usr_data_rd					      	: out std_logic_vector(63 downto 0);

		clock_fed							: in std_logic;
		
		clock_SR							: in std_logic;
		SR_DATA								: in std_logic_vector(127 downto 0);
		SR_UCTRL							: in std_logic;
		SR_WEN								: in std_logic;
		SR_LFF								: out std_logic;
		 
		ext_trigger							: in std_logic;
		ext_veto_out						: out std_logic;
		
		clock_Buffer						: in std_logic;
		SR_read_data_for_packet				: in std_logic;
		SR_ready_data_for_packet			: out std_logic;
		SR_buffered_datao					: out std_logic_vector(127 downto 0);
		SR_buffered_uctrlo					: out std_logic
  );
end component;

signal data_rd_SR0					      	: std_logic_vector(63 downto 0);
signal data_rd_SR1					      	: std_logic_vector(63 downto 0);
signal data_rd_SR2					      	: std_logic_vector(63 downto 0);
signal data_rd_SR3					      	: std_logic_vector(63 downto 0);
signal data_rd_SR4					      	: std_logic_vector(63 downto 0);
signal data_rd_SR5					      	: std_logic_vector(63 downto 0);

signal ext_veto_out0						: std_logic;
signal ext_veto_out1						: std_logic;
signal ext_veto_out2						: std_logic;
signal ext_veto_out3						: std_logic;
signal ext_veto_out4						: std_logic;
signal ext_veto_out5						: std_logic;
 
signal SR0_read_data_for_packet				: std_logic;
signal SR0_ready_data_for_packet			: std_logic;
signal SR0_buffered_datao					: std_logic_vector(127 downto 0);
signal SR0_buffered_uctrlo					: std_logic;
		
signal SR1_read_data_for_packet				: std_logic;
signal SR1_ready_data_for_packet			: std_logic;
signal SR1_buffered_datao					: std_logic_vector(127 downto 0);
signal SR1_buffered_uctrlo					: std_logic;
		
--****************************************************************
--*****************    CODE    START     HERE   ******************
--****************************************************************  
begin

SR_i0:SLINK_input  
  generic map(	addr_offset_evt_gen 	=> addr_offset_evt_gen_0 + addr_offset_100G_eth,
  				addr_offset_Slink 		=> addr_offset_slink_0 	 + addr_offset_100G_eth) 
  Port map( 
		usr_clk								=> usr_clk				    ,
		usr_rst_n							=> usr_rst_n			    ,
		usr_func_wr							=> usr_func_wr			    ,
		usr_wen								=> usr_wen				    ,
		usr_data_wr							=> usr_data_wr			    , 
																		 
		usr_func_rd					      	=> usr_func_rd		        ,
		usr_rden					      	=> usr_rden		            ,
		usr_data_rd					      	=> data_rd_SR0		        ,
																		 
		clock_fed							=> clock_fed		        ,
																		 
		clock_SR							=> clock_SR0		        ,
		SR_DATA								=> SR0_DATA			        ,
		SR_UCTRL							=> SR0_UCTRL		        ,
		SR_WEN								=> SR0_WEN			        ,
		SR_LFF								=> SR0_LFF			        ,
																		 
		ext_trigger							=> ext_trigger		        ,
		ext_veto_out						=> ext_veto_out0		    ,
																		 
		clock_Buffer						=> clock_Buffer				,
		SR_read_data_for_packet				=> SR0_read_data_for_packet	,
		SR_ready_data_for_packet			=> SR0_ready_data_for_packet,
		SR_buffered_datao					=> SR0_buffered_datao		,
		SR_buffered_uctrlo					=> SR0_buffered_uctrlo		 
  );
   
SR0_read_data_for_packet			<= UR0_read_data_for_packet;
UR0_ready_data_for_packet			<= SR0_ready_data_for_packet;
UR0_buffered_datao					<= SR0_buffered_datao;
UR0_buffered_uctrlo					<= SR0_buffered_uctrlo;
  
SR_i1:SLINK_input  
 generic map(	addr_offset_evt_gen 	=> addr_offset_evt_gen_1 + addr_offset_100G_eth,
 				addr_offset_Slink 		=> addr_offset_slink_1 	 + addr_offset_100G_eth) 
 Port map( 
		usr_clk								=> usr_clk				    ,
		usr_rst_n							=> usr_rst_n			    ,
		usr_func_wr							=> usr_func_wr			    ,
		usr_wen								=> usr_wen				    ,
		usr_data_wr							=> usr_data_wr			    , 
																		 
		usr_func_rd					      	=> usr_func_rd		        ,
		usr_rden					      	=> usr_rden		            ,
		usr_data_rd					      	=> data_rd_SR1		        ,
																		 
		clock_fed							=> clock_fed		        ,
																		 
		clock_SR							=> clock_SR1		        ,
		SR_DATA								=> SR1_DATA			        ,
		SR_UCTRL							=> SR1_UCTRL		        ,
		SR_WEN								=> SR1_WEN			        ,
		SR_LFF								=> SR1_LFF			        ,
																		 
		ext_trigger							=> ext_trigger		        ,
		ext_veto_out						=> ext_veto_out1		    ,
																		 
		clock_Buffer						=> clock_Buffer				,
		SR_read_data_for_packet				=> SR1_read_data_for_packet	,
		SR_ready_data_for_packet			=> SR1_ready_data_for_packet,
		SR_buffered_datao					=> SR1_buffered_datao		,
		SR_buffered_uctrlo					=> SR1_buffered_uctrlo		 
 );
   
SR1_read_data_for_packet			<= UR1_read_data_for_packet;
UR1_ready_data_for_packet			<= SR1_ready_data_for_packet;
UR1_buffered_datao					<= SR1_buffered_datao;
UR1_buffered_uctrlo					<= SR1_buffered_uctrlo;

usr_data_rd			<= data_rd_SR0 or data_rd_SR1;
ext_veto_out		<= ext_veto_out0 or ext_veto_out1;
  
end Behavioral;
