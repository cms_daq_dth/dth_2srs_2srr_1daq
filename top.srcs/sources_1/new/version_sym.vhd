-- /////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\
--***********   VERSION FILE   ******************
--\\\\\\\\\\\\\\\\\\\\\\/////////////////////////

LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY version_comp IS
	PORT
	(
		VERSION		: OUT STD_LOGIC_VECTOR (31 DOWNTO 0)
	);
END version_comp;

ARCHITECTURE BEHAVIORAL OF version_comp IS
BEGIN
	VERSION <= x"02040004";




END BEHAVIORAL;



-- version .......
--
--*****************************************************************
-- version "00000000";
-- First version
--*****************************************************************  
--02000000->Fri 27 Mar 2020 09:36 First global compilation
--02000001->Mon 06 Apr 2020 14:51 change the device ID to HEX number 0x01B5
--02000002->Tue 28 Apr 2020 10:43 Change the loopback in SR  and the SR version number (x....0202)
--02010000->Tue 16 Jun 2020 08:10 change the Fragment Header and Trailer format
--02010001->Tue 23 Jun 2020 08:49 Error on CRC calcul (miss to mask Status Field in 1 check!)
--02010002->Wed 24 Jun 2020 17:25 Fix bug with the new header trailer for FED SLInk=
--02010003->Mon 13 Jul 2020 13:30 fixe the problem of LINK setup on SlinkRocket (check  is header and data is correct on idle and Start/Stop word)

--02010100->Thu 06 Aug 2020 16:50 Modify theplace of the scramble close to the transceiver (the SR core 5E100300 or above)
--02010200->Fri 18 Sep 2020 16:50 Fixed problem for the 16Gb Slink Rocket (Scramble descrambled has been changed and SERDES for 16G is 64b data user
--02010300->Mon 01 Feb 2021 09:55 Change the txdiffctrl default value to 0x11000 instead of 0x1010
--02010400->Wed 28 Apr 2021 10:15 fix bug hang . error on check sequence number in Reception SlinkRocket  use 30..0 instead of 31..0    bit31 is not part of sequence number it is used for Ack flag
--02010500->Thu 24 Jun 2021 16:12 Eveolve the Generator; Fix problem in SR Sender (stuck the transfer when packet on link < 64 bit
--02010600->Tue 29 Jun 2021 10:18 Add the state Machine synthesys mode (ONE HOT) in serdes sync (rec & snd)
--02020000->Wed 23 Feb 2022 12:02 Update the SLINKHeader format (DAQ_CRC) 
--02020001->Thu 11 Aug 2022 11:43 Now the IP serdes inlcude the PRBS test (SR receiver and loopback (SR_sender)
--02020002->Thu 01 Sep 2022 17:29 Error on CRC : the FED CRC was overwritten by the logic in Encapsulation and gave SlinkCRC error
--02020100->Wed 26 Apr 2023 13:30 Add the Loopback set from receiver and PRBS test + reset stat count when not LinkUP
--02020101->Wed 03 May 2023 10:39 Increase the timeout to remove loopback in sender (25 to 26 bit)
--02020102->Tue 09 May 2023 13:57 include TXresetDone in Loopback state machine (Sender core)
--02030000->Fri 09 Jun 2023 17:50 Implement the Loopback and the PRBS test for the link
--02030001->Thu 15 Jun 2023 11:06 Move to 125Mhx for free running
--02040000->Wed 23 Aug 2023 14:56 Implement the PRBS test (Keep the lite emulator) + Clock_ref is 125Mhz down to 10 Mhz
--02040001->Thu 05 Oct 2023 14:42 Fix a problem of old data send (appears when writting data in file with a lot of backpressure , found by HGCAL)
--                                    Store TCP parameters for all stream (with and without data) because it took parameters of nodata transfer for a data transfer (see arbiter file)
--02040002->Thu 07 Dec 2023 08:57 Fix problem of stop trasnmit data (bug in BIFI ile where ENA should take care of each position line 557
--02040003->Fri 08 Dec 2023 10:38 Implement the FEC in DAQ links (should be enable by software to work)
--02040004->Wed 14 Feb 2024 13:53 Recompile design to be sure that the BIFI modif was takien into account
