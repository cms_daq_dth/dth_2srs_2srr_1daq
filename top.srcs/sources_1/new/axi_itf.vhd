----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07.06.2017 16:15:29
-- Design Name: 
-- Module Name: Usr_Slave - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;



entity AXI_itf is
    Port ( 
			resetn 				: in STD_LOGIC; --
			Usr_clock 			: in STD_LOGIC; -- max 1 MHz
			Usr_add				: in std_logic_vector(31 downto 0);
			Usr_strb			: in std_logic_vector(3 downto 0);
			Usr_dti	 			: in STD_LOGIC_vector(31 downto 0);
			Usr_dto	 			: out STD_LOGIC_vector(31 downto 0);
			Usr_RdREQ			: in std_logic;
			Usr_WrREQ			: in std_logic;
			Usr_AXI_tick		: in std_logic;
			
			Axi_status			: out std_logic_vector(31 downto 0) := x"00000000";
			Axi_done				: out std_logic;

			s_axi_clock         : in std_Logic;
			s_axi_pm_tick 		: OUT STD_LOGIC;
			s_axi_awaddr  		: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
			s_axi_awvalid 		: OUT STD_LOGIC;
			s_axi_awready 		: IN STD_LOGIC;
			s_axi_wdata   		: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
			s_axi_wstrb   		: OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
			s_axi_size          : out STD_LOGIC_VECTOR(2 downto 0);
			s_axi_wvalid  		: OUT STD_LOGIC;
			s_axi_wready  		: IN STD_LOGIC;
			s_axi_bresp   		: IN STD_LOGIC_VECTOR(1 DOWNTO 0);
			s_axi_bvalid  		: IN STD_LOGIC;
			s_axi_bready  		: OUT STD_LOGIC;
			s_axi_araddr  		: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
			s_axi_arvalid 		: OUT STD_LOGIC;
			s_axi_arready 		: IN STD_LOGIC;
			s_axi_rdata   		: IN STD_LOGIC_VECTOR(31 DOWNTO 0);
			s_axi_rresp   		: IN STD_LOGIC_VECTOR(1 DOWNTO 0);
			s_axi_rvalid  		: IN STD_LOGIC;
			s_axi_rready  		: OUT STD_LOGIC
           );
end AXI_itf;

architecture Behavioral of AXI_itf is

--attribute mark_debug : string;

 
type AXI_access_type is (	AXI_idle_state,
							AXI_Write_state,
							AXI_read_State,
							AXI_Ack_State 						
							);
signal AXI_access: AXI_access_type;
--attribute mark_debug of Usr_access: signal is "true";
							
attribute fsm_encoding : string;
---attribute fsm_encoding of Usr_access : signal is "one_hot";						
--attribute fsm_encoding of byte_mgnt : signal is "one_hot";							

--attribute mark_debug of byte_mgnt: signal is "true";
 

signal axi_add_reg					    : std_logic_vector(31 downto 0);
signal axi_dt_wr						: std_logic_vector(31 downto 0);
signal axi_dt_rd						: std_logic_vector(31 downto 0);
signal axi_be_rg						: std_logic_vector(3 downto 0);
signal axi_size_rg						: std_logic_vector(2 downto 0);

signal axi_error_wr					    : std_logic_vector(1 downto 0);
signal axi_error_rd					    : std_logic_vector(1 downto 0);

signal wr_addW_rg						: std_logic;
signal wr_req_reg						: std_logic;
signal wr_done_rg						: std_logic;
signal wr_addR_rg						: std_logic;
signal rd_req_reg						: std_logic;
signal rd_done_rg						: std_logic;

signal wr_dtW_rg						: std_logic;

signal resync_req_axi_read              : std_logic;
signal resync_req_axi_write             : std_logic;
signal reset_axi                        : std_logic;

signal access_done                      : std_logic;
signal async_access_done                : std_logic;
signal sync_access_done                 : std_logic;
--*********************************************************************************************
--***********************      Code start HERE              ***********************************
--*********************************************************************************************
begin

Done_resync_pulse_i1:entity work.resync_pulse
port map(
	aresetn			=> resetn ,
	clocki			=> s_axi_clock,
	in_s			=> async_access_done ,
	clocko			=> Usr_clock  ,
	out_s			=> sync_access_done  
	);

process(resetn,Usr_clock)
begin
	if resetn = '0'  then
		access_done	<= '1';
	elsif rising_edge(Usr_clock) then 
		if Usr_RdREQ = '1' or Usr_WrREQ = '1' then
			access_done	<= '0';
		elsif sync_access_done = '1' then
			access_done	<= '1';
		end if;
	end if;
end process;



-- resync logic;

reset_resync_i1:entity work.resetn_resync 
port map(
	aresetn			=> resetn,
	clock			=> s_axi_clock, 
	Resetn_sync		=> reset_axi
	);

read_resync_pulse_i1:entity work.resync_pulse
port map(
	aresetn			=> resetn ,
	clocki			=> Usr_clock ,
	in_s			=> Usr_RdREQ ,
	clocko			=> s_axi_clock ,
	out_s			=> resync_req_axi_read  
	);
write_resync_pulse_i1:entity work.resync_pulse
port map(
    aresetn        	=> resetn ,
    clocki   		=> Usr_clock ,
    in_s     		=> Usr_WrREQ ,
    clocko     		=> s_axi_clock ,
    out_s         	=> resync_req_axi_write  
    );
-- static values  Address  DATA write, BE
process(s_axi_clock)
begin
	if rising_edge(s_axi_clock) then
		if resync_req_axi_read = '1' or resync_req_axi_write = '1' then
			axi_add_reg		<= Usr_add;
			axi_dt_wr		<= Usr_dti;
			axi_be_rg       <= Usr_strb;
			if Usr_strb = "1111" then 
			     axi_size_rg <= "010" ;
			else 
			     axi_size_rg <= "000" ;			
			end if;
					 
		end if;
	end if;
end process;

s_axi_awaddr	<= axi_add_reg;
s_axi_araddr	<= axi_add_reg;
s_axi_wdata		<= axi_dt_wr;
s_axi_wstrb		<= axi_be_rg;
s_axi_size		<= axi_size_rg;

-- write request
process(reset_axi,s_axi_clock)
begin
	if reset_axi = '0' then
		wr_addW_rg		<= '0';
		wr_dtW_rg		<= '0';
		wr_req_reg		<= '0';
		wr_done_rg		<= '0';
	elsif rising_edge(s_axi_clock) then
		if resync_req_axi_write = '1' then
			wr_addW_rg	<= '1';
		elsif  s_axi_awready = '1' then
			wr_addW_rg	<= '0';
		end if;
		
		if resync_req_axi_write = '1' then
			wr_dtW_rg	<= '1';
		elsif  s_axi_wready = '1' then
			wr_dtW_rg	<= '0';
		end if;
		
		wr_done_rg		    <= '0';
		if resync_req_axi_write = '1' then
			wr_req_reg	    <= '1';
		elsif  AXI_access = AXI_Ack_State and s_axi_bvalid = '1' then
			axi_error_wr	<= s_axi_bresp;
			wr_req_reg	    <= '0';
			wr_done_rg	    <= '1';
		end if; 
	end if;
end process;

s_axi_awvalid		<= wr_addW_rg;
s_axi_wvalid		<= wr_dtW_rg;
s_axi_bready		<= wr_done_rg;


-- read request 
process(reset_axi,s_axi_clock)
begin
	if reset_axi = '0' then
		wr_addR_rg		<= '0';
		rd_req_reg		<= '0';
		rd_done_rg		<= '0';
	elsif rising_edge(s_axi_clock) then
		if resync_req_axi_read = '1' then
			wr_addR_rg	<= '1';
		elsif  s_axi_arready = '1' then
			wr_addR_rg	<= '0';
		end if;
 
		rd_done_rg		    <= '0';
		if resync_req_axi_read = '1' then
			rd_req_reg	    <= '1';
		elsif  AXI_access = AXI_Ack_State and s_axi_rvalid = '1' then
			axi_error_RD	<= s_axi_rresp;
			rd_req_reg	    <= '0';
			axi_dt_rd	    <= s_axi_rdata;
			rd_done_rg	    <= '1';
		end if; 
	end if;
end process;

s_axi_arvalid		<= wr_addR_rg;
s_axi_rready		<= rd_done_rg;
Usr_dto             <= axi_dt_rd;
s_axi_pm_tick		<= Usr_AXI_tick;

-- STATUS
Axi_status(1 downto 0)	     <= axi_error_wr;
Axi_status(5 downto 4)	     <= axi_error_RD;
Axi_status(8)	             <= '1' when AXI_access = AXI_idle_state  else '0';
Axi_status(9)	             <= '1' when AXI_access = AXI_Write_state else '0';
Axi_status(10)	             <= '1' when AXI_access = AXI_read_State else '0';
Axi_status(11)	             <= '1' when AXI_access = AXI_Ack_State else '0';
AXI_done					 <= '1' when access_done = '1'  else '0';
--*********************************************************************************************
-- AXI state machine
AXI_bus_FSM:process(s_axi_clock,reset_axi)
begin
	if reset_axi = '0'  then
		AXI_access 						<= AXI_idle_state;
		async_access_done				<= '0';
	elsif rising_edge(s_axi_clock) then
		async_access_done				<= '0';
        Case AXI_access is
            when AXI_idle_state =>
                if resync_req_axi_read = '1' then
                    AXI_access 	<= AXI_read_State;
                elsif resync_req_axi_write = '1' then
                    AXI_access 	<= AXI_Write_state;
                end if;
                 
            when AXI_Write_state =>
                if wr_dtW_rg = '0' and wr_addW_rg = '0' then
                    AXI_access 	<= AXI_Ack_State;
                end if;
            
            when AXI_read_State =>
                if wr_addR_rg = '0'  then
                    AXI_access 	<= AXI_Ack_State;
                end if;			 
            
            when AXI_Ack_State =>
                if 	s_axi_bvalid = '1' and wr_req_reg = '1' then
                    AXI_access 				<= AXI_idle_state;
					async_access_done		<= '1';
                elsif  s_axi_rvalid = '1' and rd_req_reg = '1' then
                    AXI_access 				<= AXI_idle_state;
					async_access_done		<= '1';
                end if;
                
            when others =>
                AXI_access <= AXI_idle_state;
          end case;
    end if;
end process;

end Behavioral;