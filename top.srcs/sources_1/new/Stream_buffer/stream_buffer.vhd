----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 17.10.2018 11:50:12
-- Design Name: 
-- Module Name: stream_buffer - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;


entity stream_buffer is
  Port ( 
		resetp							: in std_logic;
  		clock							: in std_logic;
  			
  		address_wr						: in std_logic_vector(13 downto 0);
  		data_i							: in std_logic_vector(511 downto 0);
  		write_ena						: in std_logic;
  		word_enable						: in std_logic_vector(3 downto 0);--128-bit access
  
  		address_rd						: in std_logic_vector(13 downto 0);
  		data_o							: out std_logic_vector(511 downto 0); 
  		read_ena						: in std_logic
  	);
end stream_buffer;



architecture Behavioral of stream_buffer is

Component Simple_512bit_memory is
  Port ( 
	resetp							: in std_logic;
  	clock							: in std_logic;
  
  	enable_wr 						: in std_logic;
  	address_wr						: in std_logic_vector(13 downto 0);
  	data_i							: in std_logic_vector(511 downto 0);
  	write_enable					: in std_logic_vector(63 downto 0);
  
  	address_rd						: in std_logic_vector(13 downto 0);
	data_o							: out std_logic_vector(511 downto 0);
  	enable_rd						: in std_logic  
  );
end Component;

signal write_enable_cell			: std_logic_vector(63 downto 0);

--***************************************************************************************
--*********************    CODE    START   HERE                 *************************
--***************************************************************************************

begin

 
write_enable_cell(15 downto  0)	<= x"FFFF" when write_ena = '1' and word_enable(0) = '1' else x"0000"; 
write_enable_cell(31 downto 16)	<= x"FFFF" when write_ena = '1' and word_enable(1) = '1' else x"0000"; 
write_enable_cell(47 downto 32)	<= x"FFFF" when write_ena = '1' and word_enable(2) = '1' else x"0000"; 
write_enable_cell(63 downto 48)	<= x"FFFF" when write_ena = '1' and word_enable(3) = '1' else x"0000"; 
 
mem_inst:Simple_512bit_memory  
  Port map( 
	resetp							=>	resetp						,--: in std_logic;
  	clock							=>	clock						,--: in std_logic;
                                    							
  	enable_wr 						=>	write_ena						,--: in std_logic;
  	address_wr						=>	address_wr						,--: in std_logic_vector(13 downto 0);
  	data_i							=>	data_i						,--: in std_logic_vector(511 downto 0);
  	write_enable					=>	write_enable_cell						,--: in std_logic_vector(63 downto 0);
                                    				 
  	address_rd						=>	address_rd						,--: in std_logic_vector(13 downto 0);
	data_o							=>	data_o						,--: out std_logic_vector(511 downto 0);
  	enable_rd						=>	read_ena						 --: in std_logic  
  );
 

 
end Behavioral;
