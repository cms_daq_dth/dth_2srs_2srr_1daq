----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 21.04.2017 14:23:45
-- Design Name: 
-- Module Name: top - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

use work.interface.all;
use work.address_table.all;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
--use IEEE.NUMERIC_STD.ALL;
use IEEE.std_logic_signed.all;
use IEEE.std_logic_unsigned.all;


-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all; 

entity DAQ_module is
	generic (addr_offset_100G_eth		: integer := 0);
   Port ( 
        clock_low                           : in std_logic; -- low clock ~1MHz for ARP probe
        MAC_lower_part                      : in std_logic_vector(15 downto 0);
	-- PCIe local interface
		usr_clk								: in std_logic;
		rst_usr_clk_n						: in std_logic;
		usr_func_wr							: in std_logic_vector(16383 downto 0); 
		usr_wen								: in std_logic;
		usr_data_wr							: in std_logic_vector(63 downto 0); 
			
		usr_func_rd							: in std_logic_vector(16383 downto 0); 
		usr_rden							: in std_logic;
		usr_dto								: out std_logic_vector(63 downto 0);

		clock_SR0							: in std_logic;
		SR0_DATA							: in std_logic_vector(127 downto 0);
		SR0_UCTRL							: in std_logic;
		SR0_WEN								: in std_logic;
		SR0_LFF								: out std_logic;
		
		clock_SR1							: in std_logic;
		SR1_DATA							: in std_logic_vector(127 downto 0);
		SR1_UCTRL							: in std_logic;
		SR1_WEN								: in std_logic;
		SR1_LFF								: out std_logic;
		
		ext_trigger							: in std_logic;
		ext_veto_out						: out std_logic;		
		
		clock_fed							: in std_logic;
		clock_UR 							: in std_logic; 
		UR_rst_p							: in std_logic;
	-- QSFP connection
		QSFP_RX_P							: in std_logic_vector(3 downto 0);
		QSFP_RX_N							: in std_logic_vector(3 downto 0);
		QSFP_TX_P							: out std_logic_vector(3 downto 0);
		QSFP_TX_N							: out std_logic_vector(3 downto 0);
	
	-- SERDES/MAC
		SERDES_reset_p						: in std_logic;
		init_clock							: in std_logic;     
		GT_ref_clock_p      				: in std_logic;  
		GT_ref_clock_n      				: in std_logic;
		status_out							: out std_logic_vector(31 downto 0) 		
	);		
end DAQ_module;

--*///////////////////////////////////////////////////////////////////////////////
architecture Behavioral of DAQ_module is
  
attribute mark_debug : string;
   
signal SR0_read_data_for_packet				: std_logic;
signal SR0_ready_data_for_packet			: std_logic;
signal SR0_buffered_datao					: std_logic_vector(127 downto 0);
signal SR0_buffered_uctrlo					: std_logic;
 
signal SR1_read_data_for_packet				: std_logic;
signal SR1_ready_data_for_packet			: std_logic;
signal SR1_buffered_datao					: std_logic_vector(127 downto 0);
signal SR1_buffered_uctrlo					: std_logic;
 
signal data_for_UR0_mem						: std_logic_vector(127 downto 0);
signal size_for_UR0_mem						: std_logic_vector(10 downto 0);
signal ready_for_UR0_mem					: std_logic;		
signal read_for_UR0_mem						: std_logic;
		
signal data_for_UR1_mem						: std_logic_vector(127 downto 0);
signal size_for_UR1_mem						: std_logic_vector(10 downto 0);
signal ready_for_UR1_mem					: std_logic;		
signal read_for_UR1_mem						: std_logic;
		 
signal ETH0_MEM_req_rd_blk					: std_logic;	
signal ETH1_MEM_req_rd_blk					: std_logic;	
signal ETH0_MEM_request_done				: std_logic;
signal ETH1_MEM_request_done				: std_logic;
signal ETHx_MEM_request_done				: std_logic;
signal ETHx_MEM_Add_request 				: std_logic_vector(31 downto 0);
signal ETHx_MEM_Size_request 				: std_logic_vector(31 downto 0);

signal TCP_Buf0_SND_NEW						: std_logic_vector(31 downto 0);
signal TCP_Buf0_SND_UNA						: std_logic_vector(31 downto 0);
signal TCP_Buf0_VALID_P						: std_logic;
signal TCP_Buf1_SND_NEW						: std_logic_vector(31 downto 0);
signal TCP_Buf1_SND_UNA						: std_logic_vector(31 downto 0);
signal TCP_Buf1_VALID_P						: std_logic;
 
signal UR0_PL_TX_lbus_A	   					: tx_usr_record;
signal UR0_PL_TX_lbus_B	   					: tx_usr_record;
signal UR0_PL_TX_lbus_C	   					: tx_usr_record;
signal UR0_PL_TX_lbus_D	   					: tx_usr_record; 
signal UR0_blk_len							: std_logic_vector(15 downto 0):= (others => '0');
signal UR0_blk_CS				   			: std_logic_vector(15 downto 0):= (others => '0');
signal UR0_blk_end							: std_logic:= '0';
signal UR0_ack_packet						: std_logic:= '0'; 
signal UR0_rd_blk				   			: std_logic;
	 
signal UR1_PL_TX_lbus_A	   					: tx_usr_record;
signal UR1_PL_TX_lbus_B	   					: tx_usr_record;
signal UR1_PL_TX_lbus_C	   					: tx_usr_record;
signal UR1_PL_TX_lbus_D	   					: tx_usr_record; 
signal UR1_blk_len							: std_logic_vector(15 downto 0):= (others => '0');
signal UR1_blk_CS				   			: std_logic_vector(15 downto 0):= (others => '0');
signal UR1_blk_end							: std_logic:= '0';
signal UR1_ack_packet						: std_logic:= '0'; 
signal UR1_rd_blk				   			: std_logic;
	 
signal URx_PL_TX_lbus_A	   					: tx_usr_record;
signal URx_PL_TX_lbus_B	   					: tx_usr_record;
signal URx_PL_TX_lbus_C	   					: tx_usr_record;
signal URx_PL_TX_lbus_D	   					: tx_usr_record;
signal URx_blk_ready			   			: std_logic;
signal URx_blk_len							: std_logic_vector(15 downto 0):= (others => '0');
signal URx_blk_CS				   			: std_logic_vector(15 downto 0):= (others => '0');
signal URx_blk_end							: std_logic:= '0';
signal URx_ack_packet						: std_logic:= '0'; 
signal URx_rd_blk				   			: std_logic;
		

--attribute mark_debug of URx_PL_TX_lbus_A : signal is "true"; 		
--attribute mark_debug of URx_PL_TX_lbus_B : signal is "true"; 		
--attribute mark_debug of URx_PL_TX_lbus_C : signal is "true"; 		
--attribute mark_debug of URx_PL_TX_lbus_D : signal is "true"; 		
--attribute mark_debug of URx_blk_ready : signal is "true"; 		
--attribute mark_debug of URx_blk_len		 : signal is "true"; 		
--attribute mark_debug of URx_blk_CS			 : signal is "true"; 		
--attribute mark_debug of URx_blk_end		 : signal is "true"; 		
--attribute mark_debug of URx_ack_packet : signal is "true"; 		
--attribute mark_debug of URx_rd_blk			 : signal is "true";  

--attribute mark_debug of UR0_ack_packet			 : signal is "true";  
--attribute mark_debug of UR0_rd_blk			 : signal is "true";  
--attribute mark_debug of UR1_ack_packet			 : signal is "true";  
--attribute mark_debug of UR1_rd_blk			 : signal is "true";  
		
		 
 
signal UR0_MEM_req_rd_blk 				: std_logic;					-- request to read UR 
signal UR0_MEM_Add_request 				: std_logic_vector(31 downto 0);-- Address to start the read
signal UR0_MEM_Size_request 			: std_logic_vector(31 downto 0);-- length to read (in bytes)
signal UR0_MEM_request_done				: std_logic;						-- The Packet is read from UR (ended)
signal UR0_blk_ready					: std_logic;-- informs that the Data for the grant buffer is ready
 
signal UR1_MEM_req_rd_blk 				: std_logic;					-- request to read UR 
signal UR1_MEM_Add_request 				: std_logic_vector(31 downto 0);-- Address to start the read
signal UR1_MEM_Size_request 			: std_logic_vector(31 downto 0);-- length to read (in bytes)
signal UR1_MEM_request_done				: std_logic;						-- The Packet is read from UR (ended)
signal UR1_blk_ready					: std_logic;-- informs that the Data for the grant buffer is ready

signal resetp_counter_out                : std_logic; 
signal reset_FED0                       : std_logic; 
signal reset_FED1                       : std_logic; 
signal UR_Part_selection_0_1			: std_logic;
signal ETH0_MEM_GNT						: std_logic;
signal ETH1_MEM_GNT						: std_logic; 

COMPONENT buffer_to_TX_lbus is
  Port ( 
  reset_in_p						: in std_logic;
  clock_in							: in std_logic;
  
  TX_lbus_in						: in tx_user_ift;
  TX_lbus_wr						: in std_logic;
  TX_pckt_ready						: in std_logic;
  TX_FIFO_Full						: out std_logic; 
  
  clock_out							: in std_logic;
  tx_lbus                     		: out tx_user_ift;
  TX_ready							: in std_logic
  );
end COMPONENT; 
  
 
signal resetp_mem							: std_logic;
signal resetn_mem							: std_logic;

signal rx_lbus								: rx_user_ift;
signal rx_usr_clk							: std_logic;
signal tx_lbus								: tx_user_ift;
signal tx_lbus_ready						: std_logic;
signal tx_usr_clk							: std_logic; 

signal TX_lbus_in							: tx_user_ift;
signal TX_lbus_wr							: std_logic;
signal TX_pack_ready						: std_logic;
signal TX_pckt_ready						: std_logic;
signal TX_FIFO_Full							: std_logic;

signal dto_serdes							: std_logic_vector(63 downto 0);
signal dto_eth								: std_logic_vector(63 downto 0);
signal dto_slink_merger						: std_logic_vector(63 downto 0);
signal dto_data_manager0					: std_logic_vector(63 downto 0); 
signal dto_data_manager1					: std_logic_vector(63 downto 0); 
 
--#############################################################################
-- Code start here
--#############################################################################
begin
 
reset_resync_i2:entity work.resetp_resync 
port map(
	aresetp				=>	UR_rst_p,
	clock				=>	clock_UR,
	Resetp_sync			=>	resetp_mem 
	); 

resetn_mem	<= not(resetp_mem);

--========================================================================================
--  SLINK and event generator (max 6)
Event_builder_i1:entity work.Event_Builder  
  generic map(	addr_offset_100G_eth	 	=> 	addr_offset_100G_eth)
 Port map(
		usr_clk								=> usr_clk			,
		usr_rst_n							=> rst_usr_clk_n	,
		usr_func_wr							=> usr_func_wr		, 
		usr_wen								=> usr_wen			,
		usr_data_wr							=> usr_data_wr		, 
											 
		usr_func_rd					      	=> usr_func_rd		, 
		usr_rden					      	=> usr_rden			,
		usr_data_rd					      	=> dto_slink_merger	,
											 
		clock_fed							=> clock_fed		,
		----------------------------------------------------------------									 
		-- DATA coming from the SlinkRocket								 
		clock_SR0							=> clock_SR0		, -- First SlinkRocket
		SR0_DATA							=> SR0_DATA			, 
		SR0_UCTRL							=> SR0_UCTRL		, 
		SR0_WEN								=> SR0_WEN			, 
		SR0_LFF								=> SR0_LFF			, 
																  
		clock_SR1							=> clock_SR1		, -- Second SlinkRocket
		SR1_DATA							=> SR1_DATA  , 
		SR1_UCTRL							=> SR1_UCTRL 		, 
		SR1_WEN								=> SR1_WEN 			, 
		SR1_LFF								=> SR1_LFF			, 
																  
		ext_trigger							=> ext_trigger		, 
		ext_veto_out						=> ext_veto_out		, 
		----------------------------------------------------------------			
		-- DATA Merged from up to 6 SlinkRocket's or 6 Event generators
		clock_Buffer						=> clock_UR 				,
		UR0_read_data_for_packet			=> SR0_read_data_for_packet	,-- link to UltraRam 0
		UR0_ready_data_for_packet			=> SR0_ready_data_for_packet,
		UR0_buffered_datao					=> SR0_buffered_datao		,
		UR0_buffered_uctrlo					=> SR0_buffered_uctrlo		,
		UR1_read_data_for_packet			=> SR1_read_data_for_packet	,--link to UltraRam 1
		UR1_ready_data_for_packet			=> SR1_ready_data_for_packet,
		UR1_buffered_datao					=> SR1_buffered_datao		,
		UR1_buffered_uctrlo					=> SR1_buffered_uctrlo		 
 );

--======================================================================================
		--**********************************************************************************************
		--Divide the fragment in blocks + DAQ Header  (max 2)
		--//////////////////////////////////////////////////////////////////////////////////////////////
		--  ONE 
		memory_block_i0:entity work.memory_blocks 
			generic map(
			    swapp                   =>  true,	
			    addr_offset_100G_eth 	=> 	addr_offset_100G_eth + addr_off_con0)
			port map(
				reset								=> resetn_mem				,--: IN STD_LOGIC;
				CLOCK								=> clock_UR					,--: IN STD_LOGIC;
				-- INPUT DATA FROM SLINK
				Read_dt_from_SR						=> SR0_read_data_for_packet	,--: OUT STD_LOGIC; -- used to read the intermediate FIFO
				Data_ready_from_SR					=> SR0_ready_data_for_packet,--: IN STD_LOGIC;
				Data_i_from_SR						=> SR0_buffered_datao		,--: IN STD_LOGIC_VECTOR(127 DOWNTO 0);
				Uctrl_i_from_SR						=> SR0_buffered_uctrlo		,--: IN STD_LOGIC;		-- active high
				-- OUTPUT DATA TO UltraRam 
				Data_o_to_UR						=> data_for_UR0_mem			,--: OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
				Size_ready_to_UR					=> size_for_UR0_mem			,--: OUT STD_LOGIC_VECTOR(10 DOWNTO 0); -- SIZE READY TO BE TRANSFERED (IF 0x000 AND NOT EMPTY (4kBYTES)
				Data_ready_to_UR					=> ready_for_UR0_mem		,--: OUT STD_LOGIC; -- DATA IN fifo
				Read_data_for_UR					=> read_for_UR0_mem			,--: IN STD_LOGIC;
				-- PARAMETERS READABLE BY PCI SLAVE ACCESS
				-- TRIGGER								=> ,--: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
				-- FED									=> ,--: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
				-- BX									=> ,--: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
				-- Incr_evt							: OUT STD_LOGIC;
				-- LATCH_WC_BX							: OUT STD_LOGIC;
				ENA_LATCH							=> '0'--: IN STD_LOGIC;
				-- WC_histo							: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
				-- BX_histo							: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
				-- status_debug						: OUT STD_LOGIC_VECTOR(31 downto 0);
				-- internal_LFF						: OUT STD_LOGIC
			);

		--**********************************************************************************************
		--  UltraRam implementation	
		Data_manager_i0:entity work.data_manager_v2 
			generic map(	addr_offset_100G_eth 	=> 	addr_offset_100G_eth + addr_off_con0)  
			Port map( 
				usr_clk								=> usr_clk			,--: in std_logic;
				usr_rst_n							=> rst_usr_clk_n	,--: in std_logic;
				usr_func_wr							=> usr_func_wr		,--: in std_logic_vector(16383 downto 0); 
				usr_wen								=> usr_wen			,--: in std_logic;
				usr_data_wr							=> usr_data_wr		,--: in std_logic_vector(63 downto 0); 
		 
				usr_func_rd					      	=> usr_func_rd		,--: in std_logic_vector(16383 downto 0); 
				usr_rden					      	=> usr_rden			,--: in std_logic;
				usr_data_rd					      	=> dto_data_manager0,--: out std_logic_vector(63 downto 0); 
				----------------------------------------------------------------				
				-- Data From memory block (from SR)
				clock_Buffer						=> clock_UR,--: in std_logic;	
				data_for_UR_mem	    				=> data_for_UR0_mem	,--: in std_logic_vector(127 downto 0);
				size_for_UR_mem	    				=> size_for_UR0_mem	,--: in std_logic_vector(10 downto 0);
				ready_for_UR_mem					=> ready_for_UR0_mem,--: in std_logic;
				read_for_UR_mem	    				=> read_for_UR0_mem	,--: out std_logic;		
		 
				clock_TCP							=> tx_usr_clk		,--: in std_logic;
				----------------------------------------------------------------
				-- request from Ethernet block
				UR_MEM_req_rd_blk					=> UR0_MEM_req_rd_blk,--: in std_logic;
				UR_MEM_Size_request					=> UR0_MEM_Size_request,--: in std_logic_vector(31 downto 0); -- number of bytes
				UR_MEM_Add_request					=> UR0_MEM_Add_request,--: in std_logic_vector(31 downto 0); -- SND_SEQ# (in bytes)
				UR_MEM_request_done					=> UR0_MEM_request_done,--: out std_logic;
													 
				Packet_ready						=> UR0_blk_ready	, -- SND_SEQ# (in bytes)
				eth_data_CS16						=> UR0_blk_CS		, 
				eth_ack_packet						=> UR0_ack_packet	, -- pulse to read CS and release the place
				eth_rd_data							=> UR0_rd_blk		,  
				PL_TX_lbus_A	   					=> UR0_PL_TX_lbus_A	,--  tx_usr_record;
				PL_TX_lbus_B	   					=> UR0_PL_TX_lbus_B	,--  tx_usr_record;
				PL_TX_lbus_C	   					=> UR0_PL_TX_lbus_C	,--  tx_usr_record;
				PL_TX_lbus_D	   					=> UR0_PL_TX_lbus_D	,--  tx_usr_record; -- bus of data packet
				last_data							=> UR0_blk_end		, 
				backpressure						=> TX_FIFO_Full		, 
												 
				SND_NEW_A							=> TCP_Buf0_SND_NEW	,--: out std_logic_vector(31 downto 0); 	-- value of the latest data written in BIFI	WRITE POINTER
				SND_UNA_A							=> TCP_Buf0_SND_UNA	,--: in std_logic_vector(31 downto 0); 	-- latest data acknowledge						READ POINTER
				VALID_P_A							=> TCP_Buf0_VALID_P	,--: in std_logic;
				resetp_FED0							=> reset_FED0       , --: in std_logic; 
													 
				resetp_counter_i                    => resetp_counter_out 
		  ); 
		--//////////////////////////////////////////////////////////////////////////////////////////////
		--  TWO 
		memory_block_i1:entity work.memory_blocks 
			generic map(
			    swapp                   =>  true,	
			    addr_offset_100G_eth 	=> 	addr_offset_100G_eth + addr_off_con1)
			port map(
				reset								=> resetn_mem				,--: IN STD_LOGIC;
				CLOCK								=> clock_UR					,--: IN STD_LOGIC;
				-- INPUT DATA FROM SLINK
				Read_dt_from_SR						=> SR1_read_data_for_packet	,--: OUT STD_LOGIC; -- used to read the intermediate FIFO
				Data_ready_from_SR					=> SR1_ready_data_for_packet,--: IN STD_LOGIC;
				Data_i_from_SR						=> SR1_buffered_datao		,--: IN STD_LOGIC_VECTOR(127 DOWNTO 0);
				Uctrl_i_from_SR						=> SR1_buffered_uctrlo		,--: IN STD_LOGIC;		-- active high
				-- OUTPUT DATA TO UltraRam 
				Data_o_to_UR						=> data_for_UR1_mem			,--: OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
				Size_ready_to_UR					=> size_for_UR1_mem			,--: OUT STD_LOGIC_VECTOR(10 DOWNTO 0); -- SIZE READY TO BE TRANSFERED (IF 0x000 AND NOT EMPTY (4kBYTES)
				Data_ready_to_UR					=> ready_for_UR1_mem		,--: OUT STD_LOGIC; -- DATA IN fifo
				Read_data_for_UR					=> read_for_UR1_mem			,--: IN STD_LOGIC;
				-- PARAMETERS READABLE BY PCI SLAVE ACCESS
				-- TRIGGER								=> ,--: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
				-- FED									=> ,--: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
				-- BX									=> ,--: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
				-- Incr_evt							: OUT STD_LOGIC;
				-- LATCH_WC_BX							: OUT STD_LOGIC;
				ENA_LATCH							=> '0'--: IN STD_LOGIC;
				-- WC_histo							: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
				-- BX_histo							: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
				-- status_debug						: OUT STD_LOGIC_VECTOR(31 downto 0);
				-- internal_LFF						: OUT STD_LOGIC
			);

		--**********************************************************************************************
		--  UltraRam implementation	
		Data_manager_i1:entity work.data_manager_v2 
			generic map(	addr_offset_100G_eth 	=> 	addr_offset_100G_eth + addr_off_con1)  
			Port map( 
				usr_clk								=> usr_clk			,--: in std_logic;
				usr_rst_n							=> rst_usr_clk_n	,--: in std_logic;
				usr_func_wr							=> usr_func_wr		,--: in std_logic_vector(16383 downto 0); 
				usr_wen								=> usr_wen			,--: in std_logic;
				usr_data_wr							=> usr_data_wr		,--: in std_logic_vector(63 downto 0); 
		 
				usr_func_rd					      	=> usr_func_rd		,--: in std_logic_vector(16383 downto 0); 
				usr_rden					      	=> usr_rden			,--: in std_logic;
				usr_data_rd					      	=> dto_data_manager1,--: out std_logic_vector(63 downto 0); 
				----------------------------------------------------------------				
				-- Data From memory block (from SR)
				clock_Buffer						=> clock_UR,--: in std_logic;	
				data_for_UR_mem	    				=> data_for_UR1_mem	,--: in std_logic_vector(127 downto 0);
				size_for_UR_mem	    				=> size_for_UR1_mem	,--: in std_logic_vector(10 downto 0);
				ready_for_UR_mem					=> ready_for_UR1_mem,--: in std_logic;
				read_for_UR_mem	    				=> read_for_UR1_mem	,--: out std_logic;		
		 
				clock_TCP							=> tx_usr_clk		,--: in std_logic;
				----------------------------------------------------------------
				-- request from Ethernet block
				UR_MEM_req_rd_blk					=> UR1_MEM_req_rd_blk,--: in std_logic;
				UR_MEM_Size_request					=> UR1_MEM_Size_request,--: in std_logic_vector(31 downto 0); -- number of bytes
				UR_MEM_Add_request					=> UR1_MEM_Add_request,--: in std_logic_vector(31 downto 0); -- SND_SEQ# (in bytes)
				UR_MEM_request_done					=> UR1_MEM_request_done,--: out std_logic;
													 
				Packet_ready						=> UR1_blk_ready	, -- SND_SEQ# (in bytes)
				eth_data_CS16						=> UR1_blk_CS		, 
				eth_ack_packet						=> UR1_ack_packet	, -- pulse to read CS and release the place
				eth_rd_data							=> UR1_rd_blk		,  
				PL_TX_lbus_A	   					=> UR1_PL_TX_lbus_A	,--  tx_usr_record;
				PL_TX_lbus_B	   					=> UR1_PL_TX_lbus_B	,--  tx_usr_record;
				PL_TX_lbus_C	   					=> UR1_PL_TX_lbus_C	,--  tx_usr_record;
				PL_TX_lbus_D	   					=> UR1_PL_TX_lbus_D	,--  tx_usr_record; -- bus of data packet
				last_data							=> UR1_blk_end		, 
				backpressure						=> TX_FIFO_Full		, 
												 
				SND_NEW_A							=> TCP_Buf1_SND_NEW	,--: out std_logic_vector(31 downto 0); 	-- value of the latest data written in BIFI	WRITE POINTER
				SND_UNA_A							=> TCP_Buf1_SND_UNA	,--: in std_logic_vector(31 downto 0); 	-- latest data acknowledge						READ POINTER
				VALID_P_A							=> TCP_Buf1_VALID_P	,--: in std_logic;
				resetp_FED0							=> reset_FED1       , --: in std_logic; 
													 
				resetp_counter_i                    => resetp_counter_out 
		  ); 		 

ETHx_MEM_request_done		<= ETH0_MEM_request_done;
--==========================================================================
--
 
URx_PL_TX_lbus_A	   	<= 	UR0_PL_TX_lbus_A	when UR_Part_selection_0_1 = '0' else UR1_PL_TX_lbus_A  ; 
URx_PL_TX_lbus_B	   	<= 	UR0_PL_TX_lbus_B	when UR_Part_selection_0_1 = '0' else UR1_PL_TX_lbus_B  ; 
URx_PL_TX_lbus_C	   	<= 	UR0_PL_TX_lbus_C	when UR_Part_selection_0_1 = '0' else UR1_PL_TX_lbus_C  ; 
URx_PL_TX_lbus_D	   	<= 	UR0_PL_TX_lbus_D	when UR_Part_selection_0_1 = '0' else UR1_PL_TX_lbus_D  ; 
URx_blk_ready		   	<=  UR0_blk_ready		when UR_Part_selection_0_1 = '0' else UR1_blk_ready	  ; 
URx_blk_len			   	<=  UR0_blk_len			when UR_Part_selection_0_1 = '0' else UR1_blk_len		  ; 
URx_blk_CS			   	<=  UR0_blk_CS			when UR_Part_selection_0_1 = '0' else UR1_blk_CS		  ; 
URx_blk_end			   	<=  UR0_blk_end			when UR_Part_selection_0_1 = '0' else UR1_blk_end		  ; 
  
UR0_ack_packet			<= '1' when URx_ack_packet = '1' and UR_Part_selection_0_1 = '0' else '0'; 
UR0_rd_blk				<= '1' when URx_rd_blk = '1' and UR_Part_selection_0_1 = '0'  else '0'; 
	 
UR1_ack_packet			<= '1' when URx_ack_packet = '1' and UR_Part_selection_0_1 = '1' else '0'; 
UR1_rd_blk				<= '1' when URx_rd_blk = '1' and UR_Part_selection_0_1 = '1'  else '0'; 
		 		 		 
--==========================================================================
-- Common Ethernet builder	
ethernet_i0:entity work.ethernet_block 
	generic map(addr_offset_100G_eth		=> addr_offset_100G_eth )
	Port map( 
        clock_low                       => clock_low                , -- low clock ~1MHz for ARP probe
        MAC_lower_part                  => MAC_lower_part           ,
		----------------------------------------------------------------
		-- pointers coming from BIFI memory used to manage the TCP logic
		TCP_Buf0_SND_NEW				=> TCP_Buf0_SND_NEW			, 
		TCP_Buf0_SND_UNA				=> TCP_Buf0_SND_UNA			, 
		TCP_Buf0_VALID_P				=> TCP_Buf0_VALID_P			,   
		TCP_Buf1_SND_NEW				=> TCP_Buf1_SND_NEW			, 
		TCP_Buf1_SND_UNA				=> TCP_Buf1_SND_UNA			, 
		TCP_Buf1_VALID_P				=> TCP_Buf1_VALID_P			,
		----------------------------------------------------------------
		-- signals used to ready packet from BIFI memory (UR) 
		UR_PL_TX_lbus_A	   				=> URx_PL_TX_lbus_A	   		, 
		UR_PL_TX_lbus_B	   				=> URx_PL_TX_lbus_B	   		, 
		UR_PL_TX_lbus_C	   				=> URx_PL_TX_lbus_C	   		, 
		UR_PL_TX_lbus_D	   				=> URx_PL_TX_lbus_D	   		, 
		UR_blk_ready			   		=> URx_blk_ready			, 
		UR_blk_len						=> URx_blk_len				, 
		UR_blk_CS				   		=> URx_blk_CS				, 
		UR_blk_end						=> URx_blk_end				, 
		UR_ack_packet					=> URx_ack_packet			,  
		UR_rd_blk				   		=> URx_rd_blk				, 
		UR_Part_selection_0_1			=> UR_Part_selection_0_1	,
		----------------------------------------------------------------
		-- signals used to requets a data packet to be sent over ethernet 
		UR0_MEM_req_rd_blk 				=> UR0_MEM_req_rd_blk		,
		UR0_MEM_Add_request 			=> UR0_MEM_Add_request		,
		UR0_MEM_Size_request 			=> UR0_MEM_Size_request		,
		UR0_MEM_request_done			=> UR0_MEM_request_done		,
		UR0_blk_ready					=> UR0_blk_ready			,
										 
		UR1_MEM_req_rd_blk 				=> UR1_MEM_req_rd_blk		,
		UR1_MEM_Add_request 			=> UR1_MEM_Add_request		,
		UR1_MEM_Size_request 			=> UR1_MEM_Size_request		,
		UR1_MEM_request_done			=> UR1_MEM_request_done		,
		UR1_blk_ready					=> UR1_blk_ready			,
				
		usr_clk							=> usr_clk				  	,
		usr_rst_n						=> rst_usr_clk_n  	 	 	,
		usr_func_wr						=> usr_func_wr			  	, 
		usr_wen							=> usr_wen			    	,
		usr_data_wr						=> usr_data_wr		  		, 
		usr_func_rd						=> usr_func_rd			  	, 
		usr_rden						=> usr_rden			    	,
		usr_data_rd						=> dto_eth			     	,
		--usr_rd_val						=>				        ,
		----------------------------------------------------------------
		-- signals to and from SERDES MAC 100G
		tx_usr_clk						=> tx_usr_clk			  	, 
		TX_lbus_in						=> TX_lbus_in			  	, 
		TX_lbus_wr						=> TX_lbus_wr			  	, 
		TX_pack_ready					=> TX_pckt_ready		  	, 
		TX_FIFO_Full					=> TX_FIFO_Full		  		, 
		rx_lbus 						=> rx_lbus				    ,
		rx_lbus_clk						=> rx_usr_clk               ,
		
		resetp_counter_out               => resetp_counter_out		,
		close_fed0                      => reset_FED0				,	
		close_fed1                      => reset_FED1				 
   );
 
buf_packet_i1:buffer_to_TX_lbus  
	Port map( 
		reset_in_p					=>	UR_rst_p,
		clock_in					=>	tx_usr_clk			  	,
		TX_lbus_in					=>	TX_lbus_in			  	,
		TX_lbus_wr					=>	TX_lbus_wr			  	,
		TX_pckt_ready				=>	TX_pckt_ready		  	,
		TX_FIFO_Full				=>	TX_FIFO_Full		  	,
		clock_out					=>	tx_usr_clk			  	,
		tx_lbus             		=>	tx_lbus			      	,
		TX_ready					=>	tx_lbus_ready				 
  ); 
--=================================================================
-- Common SERDES  for 1 100G ethernet link
serdes_i1:entity work.Hundred_gb_core  
	Generic map(addr_offset_100G_eth	=> addr_off_con0 + addr_offset_100G_eth)
	Port map(
		init_clk                    => init_clock			  	,
		Serdes_Reset_p              => SERDES_reset_p	     	,
		rx_lbus                     => rx_lbus             		,
		mac_rx_clk				    => rx_usr_clk				,
		tx_lbus                     => tx_lbus             		,
		tx_lbus_ready				=> tx_lbus_ready		  	,
		mac_tx_clk                  => tx_usr_clk          		,
		
		usr_clk					    => usr_clk				  	,
		usr_rst_n					=> rst_usr_clk_n		  	,
		usr_func_wr				    => usr_func_wr			  	,
		usr_wen					    => usr_wen				  	,
		usr_data_wr				    => usr_data_wr		  		, 
									   
		usr_func_rd				    => usr_func_rd			  	,
		usr_rden					=> usr_rden				  	,
		usr_data_rd				    => dto_serdes			  	,
		
		QSFP_TX4_N                  => QSFP_TX_N(3)        ,
		QSFP_TX4_P                  => QSFP_TX_P(3)        ,
		QSFP_TX3_N                  => QSFP_TX_N(2)        ,
		QSFP_TX3_P                  => QSFP_TX_P(2)        ,
		QSFP_TX2_N                  => QSFP_TX_N(1)        ,
		QSFP_TX2_P                  => QSFP_TX_P(1)        ,
		QSFP_TX1_N                  => QSFP_TX_N(0)        ,
		QSFP_TX1_P                  => QSFP_TX_P(0)        ,
		QSFP_RX4_P                  => QSFP_RX_P(3)        ,
		QSFP_RX4_N                  => QSFP_RX_N(3)        ,
		QSFP_RX3_P                  => QSFP_RX_P(2)        ,
		QSFP_RX3_N                  => QSFP_RX_N(2)        ,
		QSFP_RX2_P                  => QSFP_RX_P(1)        ,
		QSFP_RX2_N                  => QSFP_RX_N(1)        ,
		QSFP_RX1_P                  => QSFP_RX_P(0)        ,
		QSFP_RX1_N                  => QSFP_RX_N(0)        ,
		GT_ref_clock_p              => GT_ref_clock_p      ,
		GT_ref_clock_n              => GT_ref_clock_n      ,
		status_out				    => status_out           
   );  
   
   
usr_dto	<= dto_serdes  or dto_eth or dto_slink_merger or dto_data_manager0 or dto_data_manager1 ; 

end Behavioral;  