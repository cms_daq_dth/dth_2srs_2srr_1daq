------------------------------------------------------------------------------
--                      Fragment transfert                                 --
------------------------------------------------------------------------------
--  Transfer a data block to SERDES		 
--   
--   Dominique Gigi 2017		 
--								 
--   ver 1.00							 
--  Use ETH packet transmit
-- 
-- 
--
--
--
------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.std_logic_unsigned.all;
use work.interface.all;

 
entity transf_pkt is
 
	port
	(
	CLOCK									: IN STD_LOGIC;
	RESET_p								    : IN STD_LOGIC;
	
	-- Ethernet parameters to send ping or ARP reply
	-- the three following value come from a FIFO written by the IN 10Gb


	-- Ethernet parameters to send data blocks
	PORT_D								    : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
	PORT_S								    : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
	MAC_Gateway							    : IN STD_LOGIC_VECTOR(47 downto 0);
	MAC_D									: IN STD_LOGIC_VECTOR(47 downto 0);
	MAC_S									: IN STD_LOGIC_VECTOR(47 downto 0);
	IP_D									: IN STD_LOGIC_VECTOR(31 downto 0);
	IP_S									: IN STD_LOGIC_VECTOR(31 downto 0);	
	IP_Server							    : IN STD_LOGIC_VECTOR(31 downto 0);
	IP_GATEWAY							    : IN STD_LOGIC_VECTOR(31 downto 0);	
	IP_NETWORK							    : IN STD_LOGIC_VECTOR(31 downto 0);
	NETWORK								    : IN STD_LOGIC_VECTOR(31 downto 0);	
	IP_dst_and_IP_nwk_eq_Net_Dst	        : OUT STD_LOGIC;
	SEQ_N									: IN STD_LOGIC_VECTOR(31 downto 0);
	ACK_N									: IN STD_LOGIC_VECTOR(31 downto 0);
	
	-- Data signals for TCP packet
	DATA_pckt_ready					        : IN STD_LOGIC;							-- a packet is ready to send (fifo Length and CS is filled)
	PL_TX_lbus_A	   						: IN tx_usr_record;
	PL_TX_lbus_B	   						: IN tx_usr_record;
	PL_TX_lbus_C	   						: IN tx_usr_record;
	PL_TX_lbus_D	   						: IN tx_usr_record; -- bus of data packet
	DATA_pckt_len						    : IN STD_LOGIC_VECTOR(15 downto 0); -- length of the packet to send
	DATA_pckt_CS						    : IN STD_LOGIC_VECTOR(15 downto 0); -- CheckSum + carry of the packet to send
	DATA_pckt_end						    : IN STD_LOGIC;							-- last data of the packet 
	RD_DATA_pckt						    : OUT STD_LOGIC;							-- Read of the packet
	RD_CS_LEN							    : OUT STD_LOGIC;
	
	TCP_req								    : IN STD_LOGIC;							-- request a TCP packet --if DATA DATA_pckt_ready should be at '1'
	TCP_wodt								: IN STD_LOGIC; 							-- TCP without data
	TCP_flags							    : IN STD_LOGIC_VECTOR( 7 downto 0);	-- TCP flags	
	TCP_opt								    : IN STD_LOGIC_VECTOR( 2 downto 0);
	TCP_len								    : IN STD_LOGIC_VECTOR(31 downto 0);	-- Number of bytes request to transfert	-- not used anymore
	TCP_max_size 						    : IN STD_LOGIC_VECTOR(15 downto 0);
	TCP_scale							    : IN STD_LOGIC_VECTOR( 7 downto 0);
	TCP_win								    : IN STD_LOGIC_VECTOR(15 downto 0);
	TCP_TS								    : IN STD_LOGIC_VECTOR(31 downto 0);
	TCP_TS_rply							    : IN STD_LOGIC_VECTOR(31 downto 0);
	TCP_request_took					    : OUT STD_LOGIC;
	TCP_request_done					    : OUT STD_LOGIC;

   --request ARP 
	ARP_req								    : IN STD_LOGIC;						-- request a ARP packet
	ARP_rply								: IN STD_LOGIC;      				-- request a ARP reply if at '1' a ARP request if at '0'
	ARP_probe							    : IN STD_LOGIC;						-- probe_ARP looking for a same IP addess
	ARP_Dest								: IN STD_LOGIC;
	ARP_Gateway							    : IN STD_LOGIC;
	MAC_D_ARP_reply					        : IN STD_LOGIC_VECTOR(47 downto 0); -- From the ARP ETH request , we should reply to 
	IP_D_ARP_reply						    : IN STD_LOGIC_VECTOR(31 downto 0);
    Idle_ARP                                : OUT STD_LOGIC;
	-- request PING
	PING_req								: IN STD_LOGIC;
	ICMP_prbl_req						    : IN STD_LOGIC;			-- request q ICMP problem packet (PING with payload not aligned
	PING_HD_emp							    : OUT STD_LOGIC; 			-- inform that the previous PING reply is sent out
	WR_DT_PING							    : IN STD_LOGIC;			-- write & last_dt do a Ping reply request
	Last_Ping_DT						    : IN STD_LOGIC;
	Data_ping							    : IN STD_LOGIC_VECTOR(63 downto 0);		
	Reset_PING_FIFO					        : IN std_logic;
	MAC_D_PING_reply					    : IN STD_LOGIC_VECTOR(47 downto 0); -- for Ping and ARP (same origin)
	IP_D_PING_reply					        : IN STD_LOGIC_VECTOR(31 downto 0);	
	-- request DHCP
	-- DHCP_req								: IN STD_LOGIC;
	-- DHCP_done							: IN STD_LOGIC;
	no_transfer_ongoing						: out std_logic;
		-- results
		
		
	TX_lbus_a							    : OUT tx_usr_record;
	TX_lbus_b							    : OUT tx_usr_record;
	TX_lbus_c							    : OUT tx_usr_record;
	TX_lbus_d							    : OUT tx_usr_record;
	TX_lbus_wr							    : OUT std_logic;
	TX_FIFO_Full						    : IN std_logic;
	TX_pack_ready						    : out std_logic
	--no_request_peding				        : out std_logic
	
	);
end transf_pkt;

---***********************************************************************************
architecture behavioral of transf_pkt is
	
	
component ethernet_HD is
	port (
		-- INPUT
		RESET_n						    : IN STD_LOGIC;
		CLOCK							: IN STD_LOGIC;			
		MAC_D							: IN STD_LOGIC_VECTOR(47 DOWNTO 0);
		MAC_S							: IN STD_LOGIC_VECTOR(47 DOWNTO 0);
		TCP							    : IN STD_LOGIC;
		ARP							    : IN STD_LOGIC;
		PING							: IN STD_LOGIC;
		DHCP							: IN STD_LOGIC;
		Brdcast						    : IN STD_LOGIC;	
		-- OUTPUT		
		PACKET_TYPE					    : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
		ETH_TX_lbus					    : OUT tx_usr_record
	);
end component;	
	
component ARP_HD is
	port (
		-- INPUT
		RESET_n						    : IN STD_LOGIC;
		CLOCK							: IN STD_LOGIC;
		MAC_D							: IN STD_LOGIC_VECTOR(47 DOWNTO 0);
		MAC_S							: IN STD_LOGIC_VECTOR(47 DOWNTO 0);
		IP_D							: IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		IP_S							: IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		Rq_Rply						    : IN STD_LOGIC;-- request reply
		-- OUTPUT		
		TX_lbus_a					    : OUT tx_usr_record;
		TX_lbus_b					    : OUT tx_usr_record;
		TX_lbus_c					    : OUT tx_usr_record;
		TX_lbus_d					    : OUT tx_usr_record 
		);
end component;	

component IP_HD is
  Port (
		-- INPUT
		RESET_n             		    : IN STD_LOGIC;
		CLOCK               		    : IN STD_LOGIC;                                    
		IDENTIF             		    : IN STD_LOGIC_VECTOR(15 DOWNTO 0); -- Identification value
		PL_LENGTH           		    : IN STD_LOGIC_VECTOR(15 DOWNTO 0);    -- Playload  length in bytes
		IP_S                		    : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		IP_D                		    : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		ICMP                		    : IN STD_LOGIC;
		-- DHCP               	        : IN STD_LOGIC;    
		TCP_opt             		    : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
		TCP_wodt            		    : IN STD_LOGIC;
		PREP                		    : IN STD_LOGIC_VECTOR(6 DOWNTO 0);                 
		-- OUTPUT     
		IP_TX_lbus_a				    : OUT tx_usr_record;                 
		IP_TX_lbus_b				    : OUT tx_usr_record;          
		IP_TX_lbus_c				    : OUT tx_usr_record           
    );
end component;
	
component PING_HD is
	port (
		-- INPUT
		RESET_n						        : IN STD_LOGIC;
		CLOCK							    : IN STD_LOGIC;
      PING_request				            : IN STD_LOGIC; -- request to send s PING request
		ICMP_prbl_req				        : IN STD_LOGIC; -- request a ICMP reply error
														-- if none of them PING reply
		Wr_DT_ping			    	        : IN STD_LOGIC;
		DATAi							    : IN STD_LOGIC_VECTOR(63 DOWNTO 0); -- data and reply header is coming form this bus
		Last_dt				    	        : IN STD_LOGIC; 
		PING_FIFO_empty       	            : OUT STD_LOGIC;
		reset_PING_FIFO			            : IN STD_LOGIC;
		PREP							    : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
		PING_Idle					        : out std_logic;
		-- OUTPUT
		ICMP_prbl_pckt		   	            : OUT STD_LOGIC;
		tx_lbus 				    	    : OUT tx_usr_record;
		write_lbus					        : out std_logic;
		sel_lbus						    : out std_logic_vector(3 downto 0);
		PING_len						    : OUT STD_LOGIC_VECTOR(15 DOWNTO 0)   -- used to build the IP header
		 		  
	);
end component;

component TCP_HD is
	port (
		RESET_n			            : IN STD_LOGIC;
		CLOCK				        : IN STD_LOGIC;
		PORT_S			            : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
		PORT_D			            : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
		SEQ_nb			            : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		ACK_nb			            : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		TCP_wodt			        : IN STD_LOGIC;
		FLAGS				        : IN STD_LOGIC_VECTOR(7 DOWNTO 0);	 
		Window			            : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
		IP_S				        : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		IP_D				        : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		length_dm		            : IN STD_LOGIC_VECTOR(15 DOWNTO 0); -- fragment data payload length in 64-bit words
		option			            : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
		MAX_len			            : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
		scale_w			            : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
		Time_stamp		            : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		TS_ACK			            : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		PREP				        : IN STD_LOGIC_VECTOR(14 DOWNTO 0);
		DATA_CS			            : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
 		IP_TX_lbus_c	            : out tx_usr_record;
		IP_TX_lbus_d	            : out tx_usr_record;
		TCP_OPEN_RST	            : OUT STD_LOGIC; -- it is a TCP packet without data (OPEN , RESET,...))
		TCP_OPTION		            : OUT STD_LOGIC_VECTOR(2 DOWNTO 0) 
	);
end component;

component Build_hd is
	port (
	RESET_n					        : IN  STD_LOGIC;
	CLOCK						    : IN  STD_LOGIC;
	ARP_req					        : IN  STD_LOGIC;
	TCP_req					        : IN  STD_LOGIC;
	TCP_opt					        : IN  STD_LOGIC_VECTOR(2 downto 0);
	Data_rdy					    : IN  STD_LOGIC;
	LAST_data					    : IN  STD_LOGIC;
	TCP_wo_DT				        : IN  STD_LOGIC;
	PING_req					    : IN  STD_LOGIC;
	END_PING					    : IN  STD_LOGIC;
	ICMP_prbl_pckt			        : IN  STD_LOGIC;

	ETH_mux					        : OUT STD_LOGIC;
	IP_mux					        : OUT STD_LOGIC;
	IP_PING_mux						: OUT STD_LOGIC;
	IP_prep					        : OUT STD_LOGIC_VECTOR(6 downto 0);
	ARP_mux					        : OUT STD_LOGIC;
	TCP_mux					        : OUT STD_LOGIC;
	IP_TCP_mux						: OUT STD_LOGIC;
	TCP_prep					    : OUT STD_LOGIC_VECTOR(14 downto 0);
	Payload_mux					    : OUT STD_LOGIC;
	read_PayLoad					: OUT STD_LOGIC;
	PING_mux					    : OUT STD_LOGIC;
	PING_prep				        : OUT STD_LOGIC_VECTOR(3 downto 0);
	End_build_hd			        : OUT STD_LOGIC;
	TCP_done				        : OUT STD_LOGIC;
	no_transfer_ongoing				: out std_logic
	);
end component;

signal no_transfer_ongoing_cell													  : std_logic;
signal Payload_end													              : std_logic;
signal read_PayLoad													              : std_logic;

signal ETH_mux														              : std_logic;
signal IP_mux														              : std_logic;
signal IP_PING_mux														          : std_logic;
signal ARP_mux														              : std_logic;
signal TCP_mux														              : std_logic;
signal IP_TCP_mux														          : std_logic;
signal PING_mux													                  : std_logic;
signal Payload_mux												                  : std_logic;
signal IP_PREP														              : std_logic_vector(6 downto 0);
signal PING_PREP												                  : std_logic_vector(3 downto 0);
signal TCP_PREP												                      : std_logic_vector(14 downto 0);
 
signal arbiter														              : std_logic_vector(1 downto 0);
signal reset_n														              : std_logic;
signal ARP_req_mem												                  : std_logic;
signal ARP_rply_mem		 										                  : std_logic; 
signal PING_req_mem		 										                  : std_logic;
signal ICMP_prbl_req_mem 										                  : std_logic;
signal PING_rply_mem		 										              : std_logic;
signal ARP_req_OG 												                  : std_logic; 
signal ARP_rply_OG												                  : std_logic;
signal ARP_req_Gen												                  : std_logic;
signal PING_req_OG												                  : std_logic;
signal PING_rply_OG												                  : std_logic;
signal TCP_req_OG													              : std_logic;
signal TCP_req_Gen												                  : std_logic;

signal TCP_Flags_reg												              : std_logic_vector(7 downto 0);
signal TCP_opt_reg												                  : std_logic_vector(2 downto 0);
signal TCP_wodt_reg												                  : std_logic;
									
signal TCP_length													              : std_logic_vector(15 downto 0);  
signal TCP_request_took_reg									                      : std_logic;
signal TCP_request_done_cell									                      : std_logic;
 		
signal MAC_S_reg													              : std_logic_vector(47 downto 0);
signal MAC_D_reg													              : std_logic_vector(47 downto 0);
signal IP_S_reg													                  : std_logic_vector(31 downto 0);
signal IP_D_reg													                  : std_logic_vector(31 downto 0);
							
signal IP_len														              : std_logic_vector(15 downto 0);
signal PING_len													                  : std_logic_vector(15 downto 0);
 
signal End_Build_PCKT									                          : std_logic;
							
signal Rq_Rply												                      : std_logic;
signal Broadcast													              : std_logic;
signal ICMP_pckt													              : std_logic;


signal ETH_TX_lbus												                  : tx_usr_record;
									
signal ARP_TX_lbus_a												              : tx_usr_record;
signal ARP_TX_lbus_b												              : tx_usr_record;
signal ARP_TX_lbus_c												              : tx_usr_record;
signal ARP_TX_lbus_d                                                              : tx_usr_record;

signal IP_TX_lbus_a								 				                  : tx_usr_record;                 
signal IP_TX_lbus_b                                                               : tx_usr_record;          
signal IP_TX_lbus_c								 				                  : tx_usr_record; 

signal TCP_TX_lbus_c                                                              : tx_usr_record;          
signal TCP_TX_lbus_d								 				              : tx_usr_record;

signal ICMP_req													                  : std_logic;
signal PING_tx_lbus								 				                  : tx_usr_record;      
signal PING_write_lbus											                  : std_logic;
signal PING_Idle					 								              : std_logic;
signal sel_lbus													                  : std_logic_vector(3 downto 0);

signal TX_lbus_a_rg						 						                  : tx_usr_record;
signal TX_lbus_b_rg						 						                  : tx_usr_record;
signal TX_lbus_c_rg						 						                  : tx_usr_record;
signal TX_lbus_d_rg						 						                  : tx_usr_record;
signal TX_lbus_wr_rg												              : std_logic;

signal end_packet_cell															: std_logic;
signal IP_dst_and_IP_nwk_eq_Net_Dst_reg 											: std_logic;

attribute mark_debug : string;
--attribute mark_debug of TX_lbus_a_rg: signal is "true";
--attribute mark_debug of TX_lbus_b_rg: signal is "true";
--attribute mark_debug of TX_lbus_c_rg: signal is "true";
--attribute mark_debug of TX_lbus_d_rg: signal is "true";
--attribute mark_debug of TX_lbus_wr_rg: signal is "true";
--attribute mark_debug of RD_DATA_pckt: signal is "true";
--attribute mark_debug of TCP_req_OG: signal is "true";
--attribute mark_debug of TCP_req_Gen: signal is "true";
--attribute mark_debug of TCP_opt_reg: signal is "true";
--attribute mark_debug of TCP_wodt_reg: signal is "true"; 

--******************************************************************************
--******************************------------------******************************
--***************************<<<     BEGINNING    >>>***************************
--******************************------------------******************************
--******************************************************************************
begin

reset_n <= not(RESET_p);
---******************* ARBITER between ARP/PING reply probe_arp_ ping_request,-- DATA packet
process(RESET_n,clock)
begin
	if RESET_n = '0' then
		arbiter <= (others => '0');
	elsif rising_edge(clock) then
		
		if arbiter(0) = '1' then
			arbiter(1) <= not(arbiter(1));
		end if;
		arbiter(0) <= not(arbiter(0));
	end if;
end process;


process(clock,reset_n)
begin
	if reset_n = '0' then
		ARP_req_mem					<= '0';
		ARP_rply_mem		 		<= '0'; 
		PING_req_mem		 		<= '0';
		PING_rply_mem		 		<= '0';
	elsif rising_edge(clock) then
	
	
		if 	ARP_req = '1' then
			ARP_req_mem	<= '1';
		elsif ARP_req_OG = '1' and End_Build_PCKT = '1' then
			ARP_req_mem	<= '0';
		end if;
		
		if 	ARP_rply = '1' then
			ARP_rply_mem	<= '1';
		elsif ARP_rply_OG = '1' and End_Build_PCKT = '1' then
			ARP_rply_mem	<= '0';
		end if;		
		 
		if 	PING_req = '1' then
			PING_req_mem	<= '1';
		elsif PING_req_OG = '1' and End_Build_PCKT = '1' then
			PING_req_mem	<= '0';
		end if;			
		
		if 	WR_DT_PING = '1' and Last_Ping_DT = '1' then
			PING_rply_mem	<= '1';
		elsif PING_rply_OG = '1' and End_Build_PCKT = '1' then
			PING_rply_mem	<= '0';
		end if;
	end if;			 
end process;

-- this process memory the requets tof access to DAQ 100 Gb opt link (where register is reset by globaL RESET
process(clock,reset_n)
begin
	if reset_n = '0' then
		ICMP_prbl_req_mem 			<= '0';
		ARP_req_OG 					<= '0'; 
		ARP_rply_OG					<= '0';
		PING_req_OG					<= '0';
		PING_rply_OG				<= '0';
		ICMP_req					<= '0';
		ARP_req_Gen					<= '0';	
		TCP_req_OG					<= '0';
		TCP_req_Gen					<= '0';
		TCP_wodt_reg				<= '0';
		TCP_opt_reg					<= "000";
	elsif rising_edge(clock) then	

		--*************************************  ARP request & reply
		if End_Build_PCKT = '1' then
			ARP_req_OG 					<= '0'; 
			ARP_rply_OG					<= '0';
			PING_req_OG					<= '0';
			PING_rply_OG				<= '0';
			TCP_req_OG					<= '0';
			TCP_req_Gen					<= '0';
			ICMP_req					<= '0';
			ARP_req_Gen					<= '0';
			TCP_wodt_reg				<= '0';
			TCP_opt_reg					<= "000";
		------------ ARP request	
		elsif  ARP_req_mem = '1' 	and arbiter = "11" and ARP_req_OG = '0' and ARP_rply_OG = '0' and PING_req_OG = '0'  and PING_rply_OG = '0' and TCP_req_OG = '0' then  
			ARP_req_OG 					<= '1';
			ARP_req_Gen					<= '1';
 
		------------ ARP reply	
		elsif	ARP_rply_mem = '1'	and arbiter = "00" and ARP_req_OG = '0' and ARP_rply_OG = '0' and PING_req_OG = '0'  and PING_rply_OG = '0' and TCP_req_OG = '0' then 
			ARP_rply_OG					<= '1';
			ARP_req_Gen					<= '1';
		-- Ping Reply
		elsif	PING_rply_mem = '1'	 and arbiter = "01" and ARP_req_OG = '0' and ARP_rply_OG = '0' and PING_req_OG = '0'  and PING_rply_OG = '0' and TCP_req_OG = '0' then 
			PING_rply_OG				<= '1';
			ICMP_req					<= '1';
			ICMP_prbl_req_mem			<= ICMP_prbl_req;
		--Ping request
		elsif	PING_req_mem = '1'	 and arbiter = "11" and ARP_req_OG = '0' and ARP_rply_OG = '0' and PING_req_OG = '0'  and PING_rply_OG = '0' and TCP_req_OG = '0' then 
			PING_req_OG					<= '1';
			ICMP_req					<= '1';
		elsif (TCP_req = '1' and (TCP_wodt = '1' or DATA_pckt_ready = '1')) and arbiter = "10" and ARP_req_OG = '0' and ARP_rply_OG = '0' and PING_req_OG = '0'  and PING_rply_OG = '0' and TCP_req_OG = '0' then --and DHCP_done = '1' then
			TCP_req_OG					<= '1';
			TCP_req_Gen					<= '1';
			TCP_opt_reg					<= TCP_opt;
			TCP_wodt_reg				<= TCP_wodt;
		end if;
	end if;
end process;

IDLE_ARP 	<= '1' when ARP_req_OG = '0'   else '0';

-- this process memory the requets tof access to DAQ 100 Gb opt link (where register ARE NOT RESET 
process(clock)
begin
	if rising_edge(clock) then	
	   TCP_request_took_reg            <= '0';
		------------ ARP request	
		if  ARP_req_mem = '1' 	and arbiter = "11" and ARP_req_OG = '0' and ARP_rply_OG = '0' and PING_req_OG = '0'  and PING_rply_OG = '0' and TCP_req_OG = '0' then  
			MAC_S_reg					<= MAC_S;			
			MAC_D_reg					<= (others => '1') ;
			Rq_Rply						<= '0';
			if ARP_dest = '1' then
				IP_S_reg 				<= IP_S;
				IP_D_reg				<= IP_D;
				
			elsif ARP_gateway = '1' then
				IP_S_reg 				<= IP_S;
				IP_D_reg				<= IP_GateWay;
			
			elsif ARP_probe  = '1' then
			    IP_S_reg 				<= (others => '0');
				IP_D_reg				<= IP_S;
			end if;	
		 
		------------ ARP reply	
		elsif	ARP_rply_mem = '1'	and arbiter = "00" and ARP_req_OG = '0' and ARP_rply_OG = '0' and PING_req_OG = '0'  and PING_rply_OG = '0' and TCP_req_OG = '0' then 
			MAC_S_reg					<= MAC_S;
			MAC_D_reg					<= MAC_D_ARP_reply;
			IP_S_reg 					<= IP_S;
			IP_D_reg					<= IP_D_ARP_reply;
			Rq_Rply						<= '1';
		-- Ping Reply
		elsif	PING_rply_mem = '1'	 and arbiter = "01" and ARP_req_OG = '0' and ARP_rply_OG = '0' and PING_req_OG = '0'  and PING_rply_OG = '0' and TCP_req_OG = '0' then 
			MAC_S_reg					<= MAC_S;
			MAC_D_reg					<= MAC_D_PING_reply;
			IP_S_reg 					<= IP_S;
			IP_D_reg					<= IP_D_PING_reply;
		--Ping request
		elsif	PING_req_mem = '1'	 and arbiter = "11" and ARP_req_OG = '0' and ARP_rply_OG = '0' and PING_req_OG = '0'  and PING_rply_OG = '0' and TCP_req_OG = '0' then 
			MAC_S_reg					<= MAC_S;
			MAC_D_reg					<= MAC_D;
			IP_S_reg 					<= IP_S;
			IP_D_reg					<= IP_D;
		elsif (TCP_req = '1' and (TCP_wodt = '1' or DATA_pckt_ready = '1')) and arbiter = "10" and ARP_req_OG = '0' and ARP_rply_OG = '0' and PING_req_OG = '0'  and PING_rply_OG = '0' and TCP_req_OG = '0' then --and DHCP_done = '1' then
			TCP_Flags_reg 				<= TCP_flags;
			TCP_length					<= TCP_len(15 downto 0);--DATA_pckt_len; 
			IP_D_reg					<= IP_D;
		--	if IP_dst_and_IP_nwk_eq_Net_Dst_reg = '1'  then--((IP_D AND IP_NETWORK) = NETWORK) then
				MAC_D_reg				<= MAC_D;
		--	else	
		--		MAC_D_reg				<= MAC_Gateway;
	       TCP_request_took_reg            <= '1';
		--	end if;		
		end if;

		
		 --if ((IP_D AND IP_NETWORK) = NETWORK) then
			 IP_dst_and_IP_nwk_eq_Net_Dst_reg 	<= '1';
		 --else	
		--	 IP_dst_and_IP_nwk_eq_Net_Dst_reg	<= '0';
		 --end if;
		
	end if;
end process;

IP_dst_and_IP_nwk_eq_Net_Dst        <= IP_dst_and_IP_nwk_eq_Net_Dst_reg;
TCP_request_took					<= TCP_request_took_reg;
TCP_request_done					<= TCP_request_done_cell;
Broadcast							<= '1' when ARP_req_OG = '1'  else '0';

ETH_HD0:ethernet_HD  
	port map(
		RESET_n						=> reset_n			,
		CLOCK						=> CLOCK			,
		MAC_D						=> MAC_D_reg		,
		MAC_S						=> MAC_S_reg		,
		TCP							=> TCP_req_Gen		,
		ARP							=> ARP_req_Gen		,
		PING						=> ICMP_req			,
		DHCP						=> '0'				,
		Brdcast						=> Broadcast		,
		-- OUTPUT		         	=>					,
		--PACKET_TYPE				=>					,
		ETH_TX_lbus					=> ETH_TX_lbus		 
	);

ARP_HD0:ARP_HD  
	port map(
		-- INPUT
		RESET_n						    => reset_n					,
		CLOCK							=> CLOCK					,
		MAC_D							=> MAC_D_reg				,
		MAC_S							=> MAC_S_reg				,
		IP_D							=> IP_D_reg					,
		IP_S							=> IP_S_reg					,
		Rq_Rply						    => Rq_Rply					,-- request reply
		-- OUTPUT		=         >						,
		TX_lbus_a					    => ARP_TX_lbus_a			,
		TX_lbus_b					    => ARP_TX_lbus_b			,
		TX_lbus_c					    => ARP_TX_lbus_c			,
		TX_lbus_d					    => ARP_TX_lbus_d			 
		);

IP_len		<= PING_len when PING_req_OG = '1' or PING_rply_OG = '1' else TCP_length;
ICMP_pckt	<= '1'	    when PING_req_OG = '1' or PING_rply_OG = '1' else '0';

IP_HD0:IP_HD 
  Port map(
		-- INPUT
		RESET_n                	      	=> reset_n					,
		CLOCK                 	      	=> CLOCK					,                       
		IDENTIF               	      	=> (others => '0')			, -- Identification value
		PL_LENGTH             	      	=> IP_len					,    -- Playload  length in bytes
		IP_S                  	      	=> IP_S_reg					,
		IP_D                  	      	=> IP_D_reg					,
		ICMP                  	      	=> ICMP_pckt				,
		-- DHCP                	      	=> 							,
		TCP_opt               	      	=> TCP_opt_reg				,
		TCP_wodt              	      	=> TCP_wodt_reg				,
		PREP                  	      	=> IP_PREP					,             
		-- OUTPUT             	      	=> 							,
		IP_TX_lbus_a				  	=> IP_TX_lbus_a				,    
		IP_TX_lbus_b				  	=> IP_TX_lbus_b				,
		IP_TX_lbus_c				  	=> IP_TX_lbus_c			 
    );
	
TCP_HD0:TCP_HD  
	port map(
		RESET_n							=> reset_n						,
		CLOCK							=> CLOCK						,
		PORT_S							=> PORT_S						,
		PORT_D							=> PORT_D						,
		SEQ_nb							=> SEQ_N						,
		ACK_nb							=> ACK_N						,
		TCP_wodt						=> TCP_wodt_reg					,
		FLAGS							=> TCP_flags_reg				, 
		Window							=> TCP_win						,
		IP_S							=> IP_S_reg						,
		IP_D							=> IP_D_reg						,
		length_dm						=> TCP_length					,-- fragment data payload length in 64-bit words
		option							=> TCP_opt_reg					,
		MAX_len							=> TCP_max_size					,
		scale_w							=> TCP_scale					,
		Time_stamp						=> TCP_TS						,
		TS_ACK							=> TCP_TS_rply					,
		PREP							=> TCP_prep						,
		DATA_CS							=> DATA_pckt_CS					,
 		IP_TX_lbus_c					=> TCP_TX_lbus_c				,
		IP_TX_lbus_d					=> TCP_TX_lbus_d				 
		-- TCP_OPEN_RST					=> 						   , -- it is a TCP packet without data (OPEN , RESET,...))
		-- TCP_OPTION					=> 						   , 
	);	
	
PING_HD0:PING_HD  
	port map(
		-- INPUT
		RESET_n							=> reset_n					,
		CLOCK							=> CLOCK					,
      	PING_request				    => PING_req_OG				,-- request to send s PING request
		ICMP_prbl_req					=> ICMP_prbl_req_mem		,-- request a ICMP reply error
																	-- if none of them PING reply
		Wr_DT_ping			    		=> WR_DT_PING				,--: IN STD_LOGIC;
		DATAi							=> Data_ping				, -- data and reply header is coming from this bus
		Last_dt				    		=> Last_Ping_DT				,
		PING_FIFO_empty       	    	=> PING_HD_emp				,
		reset_PING_FIFO			    	=> Reset_PING_FIFO			,
		PREP							=> PING_PREP				,
		PING_Idle						=> PING_Idle				,
		-- OUTPUT                   	=>							,
		--ICMP_prbl_pckt		   		=>							,
		tx_lbus 				    	=> PING_tx_lbus				,
		write_lbus						=> PING_write_lbus			,
		sel_lbus						=> sel_lbus					,
		PING_len						=> PING_len					  -- used to build the IP header	  
	);	
	
end_packet_cell	<= '1' when DATA_pckt_end = '1'  and TX_FIFO_Full = '0' else '0';
	
BUILD_i0:Build_hd  
	port map (
		RESET_n						    => reset_n					,
		CLOCK							=> CLOCK					,
		ARP_req						    => ARP_req_Gen				,
		TCP_req						    => TCP_req_Gen				,
		TCP_opt						    => TCP_opt_reg				,
		TCP_wo_DT					    => TCP_wodt_reg				,
		PING_req						=> ICMP_req					,
		END_PING						=> PING_Idle				,
		ICMP_prbl_pckt				    => ICMP_prbl_req_mem		,
		Data_rdy						=> DATA_pckt_ready			, 		
		LAST_data						=> end_packet_cell			, 	
			
		ETH_mux						    => ETH_mux					,
		IP_mux						    => IP_mux					,
		IP_PING_mux						=> IP_PING_mux				,
		IP_prep						    => IP_prep					,
		ARP_mux						    => ARP_mux					,
		TCP_mux						    => TCP_mux					,
		IP_TCP_mux						=> IP_TCP_mux				,
		TCP_prep						=> TCP_prep					,
		Payload_mux						=> Payload_mux				,
		read_PayLoad					=> read_PayLoad				,
		PING_mux						=> PING_mux					,
		PING_prep					    => PING_prep				,
		End_build_hd				    => End_Build_PCKT			,
		TCP_done				       	=> TCP_request_done_cell		,
		no_transfer_ongoing				=> no_transfer_ongoing_cell
	);	
	
no_transfer_ongoing		<= no_transfer_ongoing_cell;
TX_pack_ready			<= End_Build_PCKT;

---------------------------------------------------------------------------------------------------------
-- 4 x 128 bit bus to be send to the MAC 100 Gb/s
--
process(clock)
begin
	if rising_edge(clock) then
		 ---------------------------------------------------------------------------------------------------------
		--bus lbus A
			if ETH_mux = '1' then
				TX_lbus_a_rg.data_bus(127 downto 16)		<= ETH_TX_lbus.data_bus(127 downto 16);
				if ARP_req_Gen = '1' then
					TX_lbus_a_rg.data_bus(15 downto 0)		<= ARP_TX_lbus_a.data_bus(15 downto 0);	
				else
					TX_lbus_a_rg.data_bus(15 downto 0)		<= IP_TX_lbus_a.data_bus(15 downto 0);
				end if;
				TX_lbus_a_rg.sopckt							<= ETH_TX_lbus.sopckt;
				TX_lbus_a_rg.data_ena						<= ETH_TX_lbus.data_ena;
				TX_lbus_a_rg.eopckt							<= ETH_TX_lbus.eopckt;
				TX_lbus_a_rg.err_pckt						<= ETH_TX_lbus.err_pckt;
				TX_lbus_a_rg.empty_pckt						<= ETH_TX_lbus.empty_pckt;
			elsif TX_FIFO_Full = '0' and PING_mux = '1' and sel_lbus(0) = '1' then
				TX_lbus_a_rg.data_bus 						<= PING_TX_lbus.data_bus ;
				TX_lbus_a_rg.sopckt							<= PING_TX_lbus.sopckt;
				TX_lbus_a_rg.data_ena						<= PING_TX_lbus.data_ena;
				TX_lbus_a_rg.eopckt							<= PING_TX_lbus.eopckt;
				TX_lbus_a_rg.err_pckt						<= PING_TX_lbus.err_pckt;
				TX_lbus_a_rg.empty_pckt						<= PING_TX_lbus.empty_pckt;
			elsif TX_FIFO_Full = '0' and Payload_mux = '1' then
				TX_lbus_a_rg.data_bus 						<= PL_TX_lbus_A.data_bus; 
				TX_lbus_a_rg.sopckt							<= '0';
				TX_lbus_a_rg.data_ena						<= PL_TX_lbus_A.data_ena;
				TX_lbus_a_rg.eopckt							<= PL_TX_lbus_A.eopckt;
				TX_lbus_a_rg.err_pckt						<= '0';
				TX_lbus_a_rg.empty_pckt						<= PL_TX_lbus_A.empty_pckt;
			end if;
		---------------------------------------------------------------------------------------------------------
		--bus lbus B
			if TX_FIFO_Full = '0' and ARP_mux = '1' then
				TX_lbus_b_rg.data_bus						<= ARP_TX_lbus_b.data_bus;
				TX_lbus_b_rg.sopckt							<= ARP_TX_lbus_b.sopckt;
				TX_lbus_b_rg.data_ena						<= ARP_TX_lbus_b.data_ena;
				TX_lbus_b_rg.eopckt							<= ARP_TX_lbus_b.eopckt;
				TX_lbus_b_rg.err_pckt						<= ARP_TX_lbus_b.err_pckt;
				TX_lbus_b_rg.empty_pckt						<= ARP_TX_lbus_b.empty_pckt;
			elsif IP_mux = '1' then
				TX_lbus_b_rg.data_bus 						<= IP_TX_lbus_b.data_bus;
				TX_lbus_b_rg.sopckt							<= IP_TX_lbus_b.sopckt;
				TX_lbus_b_rg.data_ena						<= IP_TX_lbus_b.data_ena;
				TX_lbus_b_rg.eopckt							<= IP_TX_lbus_b.eopckt;
				TX_lbus_b_rg.err_pckt						<= IP_TX_lbus_b.err_pckt;
				TX_lbus_b_rg.empty_pckt						<= IP_TX_lbus_b.empty_pckt;
			elsif TX_FIFO_Full = '0' and PING_mux = '1' and sel_lbus(1) = '1' then
				TX_lbus_b_rg.data_bus 						<= PING_TX_lbus.data_bus ;
				TX_lbus_b_rg.sopckt							<= PING_TX_lbus.sopckt;
				TX_lbus_b_rg.data_ena						<= PING_TX_lbus.data_ena;
				TX_lbus_b_rg.eopckt							<= PING_TX_lbus.eopckt;
				TX_lbus_b_rg.err_pckt						<= PING_TX_lbus.err_pckt;
				TX_lbus_b_rg.empty_pckt						<= PING_TX_lbus.empty_pckt;
			elsif TX_FIFO_Full = '0' and Payload_mux = '1' then
				TX_lbus_b_rg.data_bus 						<= PL_TX_lbus_B.data_bus; 
				TX_lbus_b_rg.sopckt							<= '0';
				TX_lbus_b_rg.data_ena						<= PL_TX_lbus_B.data_ena;
				TX_lbus_b_rg.eopckt							<= PL_TX_lbus_B.eopckt;
				TX_lbus_b_rg.err_pckt						<= '0';
				TX_lbus_b_rg.empty_pckt						<= PL_TX_lbus_B.empty_pckt;
			elsif TX_FIFO_Full = '0' and PING_mux = '1' and sel_lbus(0) = '1' then
				TX_lbus_b_rg.sopckt							<= '0';
				TX_lbus_b_rg.data_ena						<= '0';
				TX_lbus_b_rg.eopckt							<= '0';
			end if;	
		
		---------------------------------------------------------------------------------------------------------
		--bus lbus C
			if TX_FIFO_Full = '0' and ARP_mux = '1' then
				TX_lbus_c_rg.data_bus						<= ARP_TX_lbus_c.data_bus;
				TX_lbus_c_rg.sopckt							<= ARP_TX_lbus_c.sopckt;
				TX_lbus_c_rg.data_ena						<= ARP_TX_lbus_c.data_ena;
				TX_lbus_c_rg.eopckt							<= ARP_TX_lbus_c.eopckt;
				TX_lbus_c_rg.err_pckt						<= ARP_TX_lbus_c.err_pckt;
				TX_lbus_c_rg.empty_pckt						<= ARP_TX_lbus_c.empty_pckt;
			elsif IP_PING_mux = '1'  then
				TX_lbus_c_rg.data_bus(127 downto 112)		<= IP_TX_lbus_c.data_bus(127 downto 112);
				TX_lbus_c_rg.data_bus(111 downto 0)			<= PING_TX_lbus.data_bus(111 downto 0);
				TX_lbus_c_rg.sopckt							<= PING_TX_lbus.sopckt;
				TX_lbus_c_rg.data_ena						<= PING_TX_lbus.data_ena;
				TX_lbus_c_rg.eopckt							<= PING_TX_lbus.eopckt;
				TX_lbus_c_rg.err_pckt						<= PING_TX_lbus.err_pckt;
				TX_lbus_c_rg.empty_pckt						<= PING_TX_lbus.empty_pckt;	
			elsif TX_FIFO_Full = '0' and PING_mux = '1' and sel_lbus(2) = '1' then
				TX_lbus_c_rg.data_bus 						<= PING_TX_lbus.data_bus;
				TX_lbus_c_rg.sopckt							<= PING_TX_lbus.sopckt;
				TX_lbus_c_rg.data_ena						<= PING_TX_lbus.data_ena;
				TX_lbus_c_rg.eopckt							<= PING_TX_lbus.eopckt;
				TX_lbus_c_rg.err_pckt						<= PING_TX_lbus.err_pckt;
				TX_lbus_c_rg.empty_pckt						<= PING_TX_lbus.empty_pckt;
			 elsif IP_TCP_mux = '1' then
				TX_lbus_c_rg.data_bus(127 downto 112)		<= IP_TX_lbus_c.data_bus(127 downto 112);
				TX_lbus_c_rg.data_bus(111 downto 0)			<= TCP_TX_lbus_c.data_bus(111 downto 0);
				TX_lbus_c_rg.sopckt							<= TCP_TX_lbus_c.sopckt;
				TX_lbus_c_rg.data_ena						<= TCP_TX_lbus_c.data_ena;
				TX_lbus_c_rg.eopckt							<= TCP_TX_lbus_c.eopckt;
				TX_lbus_c_rg.err_pckt						<= TCP_TX_lbus_c.err_pckt;
				TX_lbus_c_rg.empty_pckt						<= TCP_TX_lbus_c.empty_pckt;
			elsif TX_FIFO_Full = '0' and Payload_mux = '1' then
				TX_lbus_c_rg.data_bus 						<= PL_TX_lbus_C.data_bus; 
				TX_lbus_c_rg.sopckt							<= '0';
				TX_lbus_c_rg.data_ena						<= PL_TX_lbus_C.data_ena;
				TX_lbus_c_rg.eopckt							<= PL_TX_lbus_C.eopckt;
				TX_lbus_c_rg.err_pckt						<= '0';
				TX_lbus_c_rg.empty_pckt						<= PL_TX_lbus_C.empty_pckt;
			elsif TX_FIFO_Full = '0' and PING_mux = '1' and sel_lbus(0) = '1' then
				TX_lbus_c_rg.sopckt							<= '0';
				TX_lbus_c_rg.data_ena						<= '0';
				TX_lbus_c_rg.eopckt							<= '0';
			end if;		
		---------------------------------------------------------------------------------------------------------
		--bus lbus D
			if TX_FIFO_Full = '0' and ARP_mux = '1' then
				TX_lbus_d_rg.data_bus						<= (others => '0');
				TX_lbus_d_rg.sopckt							<= ARP_TX_lbus_d.sopckt;
				TX_lbus_d_rg.data_ena						<= ARP_TX_lbus_d.data_ena;
				TX_lbus_d_rg.eopckt							<= ARP_TX_lbus_d.eopckt;
				TX_lbus_d_rg.err_pckt						<= ARP_TX_lbus_d.err_pckt;
				TX_lbus_d_rg.empty_pckt						<= ARP_TX_lbus_d.empty_pckt;
			elsif TX_FIFO_Full = '0' and PING_mux = '1' and sel_lbus(3) = '1' then
				TX_lbus_d_rg.data_bus 						<= PING_TX_lbus.data_bus ;
				TX_lbus_d_rg.sopckt							<= PING_TX_lbus.sopckt;
				TX_lbus_d_rg.data_ena						<= PING_TX_lbus.data_ena;
				TX_lbus_d_rg.eopckt							<= PING_TX_lbus.eopckt;
				TX_lbus_d_rg.err_pckt						<= PING_TX_lbus.err_pckt;
				TX_lbus_d_rg.empty_pckt						<= PING_TX_lbus.empty_pckt;
			 elsif TX_FIFO_Full = '0' and TCP_mux = '1' then
				if TCP_wodt_reg = '1' then
					TX_lbus_d_rg.data_bus 					<= TCP_TX_lbus_d.data_bus ;
					TX_lbus_d_rg.sopckt						<= TCP_TX_lbus_d.sopckt;
					TX_lbus_d_rg.data_ena					<= TCP_TX_lbus_d.data_ena;
					TX_lbus_d_rg.eopckt						<= TCP_TX_lbus_d.eopckt;
					TX_lbus_d_rg.err_pckt					<= TCP_TX_lbus_d.err_pckt;
					TX_lbus_d_rg.empty_pckt					<= TCP_TX_lbus_d.empty_pckt;
				else -- payload
					TX_lbus_d_rg.data_bus(127 downto 80)	<= TCP_TX_lbus_d.data_bus(127 downto 80);
					TX_lbus_d_rg.data_bus( 79 downto  0)	<= PL_TX_lbus_D.data_bus( 79 downto  0) ;
					TX_lbus_d_rg.sopckt						<= '0';
					TX_lbus_d_rg.data_ena					<= PL_TX_lbus_D.data_ena;
					TX_lbus_d_rg.eopckt						<= PL_TX_lbus_D.eopckt;
					TX_lbus_d_rg.err_pckt					<= '0';
					TX_lbus_d_rg.empty_pckt					<= PL_TX_lbus_D.empty_pckt;	
				end if;
			elsif TX_FIFO_Full = '0' and Payload_mux = '1' then
				TX_lbus_d_rg.data_bus 						<= PL_TX_lbus_D.data_bus; 
				TX_lbus_d_rg.sopckt							<= '0';
				TX_lbus_d_rg.data_ena						<= PL_TX_lbus_D.data_ena;
				TX_lbus_d_rg.eopckt							<= PL_TX_lbus_D.eopckt;
				TX_lbus_d_rg.err_pckt						<= '0';
				TX_lbus_d_rg.empty_pckt						<= PL_TX_lbus_D.empty_pckt;
			elsif TX_FIFO_Full = '0' and PING_mux = '1' and sel_lbus(0) = '1' then
				TX_lbus_d_rg.sopckt							<= '0';
				TX_lbus_d_rg.data_ena						<= '0';
				TX_lbus_d_rg.eopckt							<= '0';
			end if;		
		 
		TX_lbus_wr_rg									<= '0';
		if TX_FIFO_Full = '0' then
			if 	  ARP_mux = '1' then	
				TX_lbus_wr_rg							<= '1';
			elsif PING_write_lbus = '1' then	
				TX_lbus_wr_rg							<= '1';
			elsif (TCP_mux = '1' or Payload_mux = '1') then					--and TX_FIFO_Full = '0'	
				TX_lbus_wr_rg							<= '1';
			end if;
		end if;
	end if;
end process;	

RD_DATA_pckt				<= '1' when (read_PayLoad = '1') and TX_FIFO_Full = '0' else '0'; 
RD_CS_LEN					<= end_packet_cell and Payload_mux;
	
TX_lbus_a					<= TX_lbus_a_rg;
TX_lbus_b					<= TX_lbus_b_rg;
TX_lbus_c					<= TX_lbus_c_rg;
TX_lbus_d					<= TX_lbus_d_rg;
TX_lbus_wr					<= TX_lbus_wr_rg;
  
end behavioral;