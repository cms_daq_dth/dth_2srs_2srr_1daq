------------------------------------------------------------------------------
--                      PCIe register interface                                 --
------------------------------------------------------------------------------
--   	 
--   
--   Dominique Gigi 2012		 
--								 
--   ver 2.00							 
--  
-- 
-- 
--
--
--
------------------------------------------------------------------------------
LIBRARY ieee;
 

USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;
 
use work.address_table.all;
	
entity PCIe_reg_eth is
 	generic (addr_offset		: integer := 0;
 	         addr_offset1		: integer := 0);
	port
	(CLOCK						: IN STD_LOGIC;
	 RESET						: IN STD_LOGIC;
			
	PCIe_DT						: IN STD_LOGIC_VECTOR(63 downto 0);
	PCIe_func					: IN STD_LOGIC_VECTOR(16383 downto 0); 
	PCIe_wr						: IN STD_LOGIC; 
	PCIe_func_rd				: IN STD_LOGIC_VECTOR(16383 downto 0);  								
	PCIe_out					: OUT STD_LOGIC_VECTOR(63 downto 0);
	
	PORT0_D						: OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
	PORT0_S						: OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
	PORT1_D						: OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
	PORT1_S						: OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
	
	MAC_S						: OUT STD_LOGIC_VECTOR(47 downto 0);
	IP_D						: OUT STD_LOGIC_VECTOR(31 downto 0);
	IP_S						: OUT STD_LOGIC_VECTOR(31 downto 0);
	IP_GATEWAY					: OUT STD_LOGIC_VECTOR(31 downto 0);
	IP_NETWORK					: OUT STD_LOGIC_VECTOR(31 downto 0);
	NETWORK						: OUT STD_LOGIC_VECTOR(31 downto 0);
		
	TCP_max_size				: OUT STD_LOGIC_VECTOR(15 downto 0);
	TCP_scale					: OUT STD_LOGIC_VECTOR(7 downto 0);
	TCP_win						: OUT STD_LOGIC_VECTOR(15 downto 0);
	TCP_TS						: OUT STD_LOGIC_VECTOR(31 downto 0);
	TCP_TS_rply					: OUT STD_LOGIC_VECTOR(31 downto 0);
	snd_cwnd					: OUT STD_LOGIC_VECTOR(31 downto 0); 
	load_new_snd_cwnd			: OUT STD_LOGIC;
	timer_rtt_I					: OUT STD_LOGIC_VECTOR(31 downto 0); 
	timer_rtt_I_sync			: OUT STD_LOGIC_VECTOR(31 downto 0);
	timer_persist_I				: OUT STD_LOGIC_VECTOR(31 downto 0);
	TCP_Rexmt_CWND_sh			: OUT STD_LOGIC_VECTOR(31 downto 0);
	Init_bit					: OUT STD_LOGIC_VECTOR(15 downto 0);
	thresold_retrans			: OUT STD_LOGIC_VECTOR(31 downto 0); 
		
	DHCP_IP						: IN STD_LOGIC_VECTOR(31 downto 0);	
	DHCP_GateWay				: IN STD_LOGIC_VECTOR(31 downto 0);		
	DHCP_Network				: IN STD_LOGIC_VECTOR(31 downto 0);	
	DHCP_received				: IN STD_LOGIC;
	MAC							: IN STD_LOGIC_VECTOR(47 downto 0)
	);
end PCIe_reg_eth;

architecture behavioral of PCIe_reg_eth is

signal	PORT0_D_reg					: STD_LOGIC_VECTOR(15 DOWNTO 0);
signal	PORT0_S_reg					: STD_LOGIC_VECTOR(15 DOWNTO 0);
signal	PORT1_D_reg					: STD_LOGIC_VECTOR(15 DOWNTO 0);
signal	PORT1_S_reg					: STD_LOGIC_VECTOR(15 DOWNTO 0);
signal	MAC_S_reg					: STD_LOGIC_VECTOR(47 downto 0);
signal	IP_D_reg					: STD_LOGIC_VECTOR(31 downto 0);
signal	IP_S_reg					: STD_LOGIC_VECTOR(31 downto 0);



signal	IP_GATEWAY_reg				: STD_LOGIC_VECTOR(31 downto 0);
signal	IP_NETWORK_reg				: STD_LOGIC_VECTOR(31 downto 0);	
signal	NETWORK_reg					: STD_LOGIC_VECTOR(31 downto 0); 

signal	TCP_max_size_reg			: STD_LOGIC_VECTOR(15 downto 0);
signal	TCP_scale_reg				: STD_LOGIC_VECTOR(7 downto 0);
signal	TCP_win_reg					: STD_LOGIC_VECTOR(15 downto 0);
signal	TCP_TS_reg					: STD_LOGIC_VECTOR(31 downto 0);
signal	TCP_TS_rply_reg				: STD_LOGIC_VECTOR(31 downto 0);
signal  Init_reg					: STD_LOGIC_VECTOR(15 downto 0);
signal  snd_cwnd_reg				: STD_LOGIC_VECTOR(31 downto 0); 
signal  timer_rtt_I_reg				: STD_LOGIC_VECTOR(31 downto 0); 
signal  timer_rtt_I_sync_reg		: STD_LOGIC_VECTOR(31 downto 0); 
signal  timer_persist_I_reg			: STD_LOGIC_VECTOR(31 downto 0); 
signal  TCP_Rexmt_CWND_sh_reg		: STD_LOGIC_VECTOR(31 downto 0); 
signal  thresold_retrans_reg		: STD_LOGIC_VECTOR(31 downto 0); 
signal  load_new_snd_cwnd_reg		: std_logic;

signal  PCIe_out_rg					: STD_LOGIC_VECTOR(63 downto 0); 

--****************************************************************************
--*******************************  BEGINNING *********************************
--****************************************************************************
BEGIN

process(clock)
begin
	if rising_edge(clock) then
		if PCIe_wr = '1' then
			if PCIe_func(eth_100Gb_ctrl_IPS_def + addr_offset)  = '1' then
				IP_S_reg(31 downto 0) 		<= PCIe_dt(31 downto 0);
			elsif DHCP_received = '1' then
				IP_S_reg(31 downto 0) 		<= DHCP_IP;
			end if;
		end if;
	end if;
end process;

process(reset,clock)
begin
if reset = '0' then
	PORT0_D_reg							<= (others => '0');
	PORT0_S_reg							<= (others => '0');
	PORT1_D_reg							<= (others => '0');
	PORT1_S_reg							<= (others => '0');
	IP_D_reg							<= (others => '0');
	TCP_max_size_reg					<= x"2300";
	TCP_scale_reg						<= x"01";
	TCP_win_reg							<= x"0400";
	TCP_TS_reg							<= (others => '0');
	TCP_TS_rply_reg						<= (others => '0');
	Init_reg							<= (others => '0');
	TCP_Rexmt_CWND_sh_reg				<= x"00000006";
	snd_cwnd_reg						<= x"00100000";
	timer_rtt_I_reg						<= x"0004c4b4";
	timer_rtt_I_sync_reg				<= x"12A05F20";
	timer_persist_I_reg					<= x"00098968";
	thresold_retrans_reg				<= x"00000003";
	IP_GATEWAY_reg						<= (others => '0');
	IP_NETWORK_reg						<= (others => '0');
 
elsif rising_edge(clock) then
	load_new_snd_cwnd_reg				<= '0';	
	if PCIe_wr = '1' then
		if PCIe_func(eth_100Gb_ctrl_port0_def + addr_offset)  = '1' then
			PORT0_D_reg 					<= PCIe_dt(15 downto 0);
			PORT0_S_reg 					<= PCIe_dt(31 downto 16);	
		end if;
		 
		if PCIe_func(eth_100Gb_ctrl_port1_def + addr_offset)  = '1' then
			PORT1_D_reg 					<= PCIe_dt(15 downto 0);
			PORT1_S_reg 					<= PCIe_dt(31 downto 16);	
		end if;
		 
		if PCIe_func(eth_100Gb_ctrl_IPD_def + addr_offset)  = '1' then
			IP_D_reg(31 downto 0) 			<= PCIe_dt(31 downto 0);
		end if;
  
		if PCIe_func(eth_100Gb_ctrl_Init_def + addr_offset)  = '1' then
			TCP_win_reg						<= PCIe_dt(15 downto 0);
			Init_reg						<= PCIe_dt(31 downto 16);
		end if;
		if PCIe_func(eth_100Gb_ctrl_Max_size_Scale_def + addr_offset)  = '1' then
			TCP_max_size_reg				<= PCIe_dt(15 downto 0);
			TCP_scale_reg					<= PCIe_dt(23 downto 16);
		end if;
		if PCIe_func(eth_100Gb_ctrl_timestamp_def + addr_offset)  = '1' then
			TCP_TS_reg						<= PCIe_dt(31 downto 0);		
		end if;
		if PCIe_func(eth_100Gb_ctrl_timestamp_reply_def + addr_offset)  = '1' then
			TCP_TS_rply_reg	 				<= PCIe_dt(31 downto 0);
		end if;
		
		if PCIe_func(eth_100Gb_ctrl_IP_network_def + addr_offset)  = '1' then
			IP_NETWORK_reg					<= PCIe_dt(31 downto 0);	
		elsif DHCP_received = '1' then
			IP_NETWORK_reg 					<= DHCP_Network;		
		end if;
		
		if PCIe_func(eth_100Gb_ctrl_IP_gateway_def + addr_offset)  = '1' then
			IP_GATEWAY_reg		 			<= PCIe_dt(31 downto 0);
		elsif DHCP_received = '1' then
			IP_GATEWAY_reg 					<= DHCP_GateWay;
		end if;
		
		if PCIe_func(eth_100Gb_ctrl_CongWind_def + addr_offset)  = '1' then
			snd_cwnd_reg(31 downto 3)		<= PCIe_dt(31 downto 3);
			snd_cwnd_reg(2 downto 0)		<= "000";
			load_new_snd_cwnd_reg			<= '1';
		end if;
		load_new_snd_cwnd 					<= load_new_snd_cwnd_reg;
		
		if PCIe_func(eth_100Gb_ctrl_timer_RTT_def + addr_offset)  = '1' then
			timer_rtt_I_reg	 				<= PCIe_dt(31 downto 0);
		end if;
		if PCIe_func(eth_100Gb_ctrl_timer_RTT_sync_def + addr_offset)  = '1' then
			timer_rtt_I_sync_reg			<= PCIe_dt(31 downto 0);
		end if;
		if PCIe_func(eth_100Gb_ctrl_timer_persist_def + addr_offset)  = '1' then
			timer_persist_I_reg				<= PCIe_dt(31 downto 0);
		end if;
		if PCIe_func(eth_100Gb_ctrl_thresold_retrans_def + addr_offset)  = '1' then
			thresold_retrans_reg			<= PCIe_dt(31 downto 0);
		end if;
		if PCIe_func(eth_100Gb_ctrl_Rexmt_CWND_Sh_def + addr_offset)  = '1' then
			TCP_Rexmt_CWND_sh_reg			<= PCIe_dt(31 downto 0);
		end if;
	end if;	
end if;
end process;

process(clock)
begin
	if rising_edge(clock) then	
		PCIe_out_rg <= (others => '0');
		 
		if 	 ( PCIe_func_rd(eth_100Gb_ctrl_port0_def + addr_offset)  = '1' ) or
		     ( PCIe_func_rd(eth_100Gb_ctrl_port0_def + addr_offset1)  = '1' ) then
			PCIe_out_rg(15 downto 0) 					<= PORT0_D_reg;
			PCIe_out_rg(31 downto 16) 					<= PORT0_S_reg;	
		elsif (PCIe_func_rd(eth_100Gb_ctrl_port1_def + addr_offset)  = '1' ) or
		     ( PCIe_func_rd(eth_100Gb_ctrl_port1_def + addr_offset1)  = '1' ) then
			PCIe_out_rg(15 downto 0) 					<= PORT1_D_reg;
			PCIe_out_rg(31 downto 16)			 		<= PORT1_S_reg;	
		elsif (PCIe_func_rd(eth_100Gb_ctrl_MAC_def + addr_offset)  = '1' ) or
		     ( PCIe_func_rd(eth_100Gb_ctrl_MAC_def + addr_offset1)  = '1' ) then
			PCIe_out_rg(47 downto  0) 					<= MAC(47 downto 0);
		elsif (PCIe_func_rd(eth_100Gb_ctrl_IPS_def + addr_offset)  = '1' ) or
		     ( PCIe_func_rd(eth_100Gb_ctrl_IPS_def + addr_offset1)  = '1' ) then
			PCIe_out_rg(31 downto  0)					<= IP_S_reg(31 downto 0);
		elsif (PCIe_func_rd(eth_100Gb_ctrl_IPD_def + addr_offset)  = '1' ) or
		     ( PCIe_func_rd(eth_100Gb_ctrl_IPD_def + addr_offset1)  = '1' ) then
			PCIe_out_rg(31 downto  0) 					<= IP_D_reg(31 downto 0);
		elsif (PCIe_func_rd(eth_100Gb_ctrl_Init_def + addr_offset)  = '1' ) or
		     ( PCIe_func_rd(eth_100Gb_ctrl_Init_def + addr_offset1)  = '1' ) then
			PCIe_out_rg(15 downto  0) 					<= TCP_win_reg;
			PCIe_out_rg(31 downto 16) 					<= Init_reg;
		elsif (PCIe_func_rd(eth_100Gb_ctrl_Max_size_Scale_def + addr_offset)  = '1' ) or
		     ( PCIe_func_rd(eth_100Gb_ctrl_Max_size_Scale_def + addr_offset1)  = '1' ) then
			PCIe_out_rg(15 downto  0) 					<= TCP_max_size_reg;
			PCIe_out_rg(23 downto 16) 					<= TCP_scale_reg;
		elsif (PCIe_func_rd(eth_100Gb_ctrl_timestamp_def + addr_offset)  = '1' ) or
		     ( PCIe_func_rd(eth_100Gb_ctrl_timestamp_def + addr_offset1)  = '1' ) then
			PCIe_out_rg(31 downto  0) 					<= TCP_TS_reg;
		elsif (PCIe_func_rd(eth_100Gb_ctrl_timestamp_reply_def + addr_offset)  = '1' ) or
		     ( PCIe_func_rd(eth_100Gb_ctrl_timestamp_reply_def + addr_offset1)  = '1' ) then
			PCIe_out_rg(31 downto  0) 					<= TCP_TS_rply_reg;
		elsif (PCIe_func_rd(eth_100Gb_ctrl_IP_network_def + addr_offset)  = '1' ) or
		     ( PCIe_func_rd(eth_100Gb_ctrl_IP_network_def + addr_offset1)  = '1' ) then
			PCIe_out_rg(31 downto  0) 					<= IP_NETWORK_reg;	
		elsif (PCIe_func_rd(eth_100Gb_ctrl_IP_gateway_def + addr_offset)  = '1' ) or
		     ( PCIe_func_rd(eth_100Gb_ctrl_IP_gateway_def + addr_offset1)  = '1' ) then
			PCIe_out_rg(31 downto  0) 					<= IP_GATEWAY_reg;		
		elsif (PCIe_func_rd(eth_100Gb_ctrl_CongWind_def + addr_offset)  = '1' ) or
		     ( PCIe_func_rd(eth_100Gb_ctrl_CongWind_def + addr_offset1)  = '1' ) then
			PCIe_out_rg(31 downto  0) 					<= snd_cwnd_reg;	
		elsif( PCIe_func_rd(eth_100Gb_ctrl_timer_RTT_def + addr_offset)  = '1' ) or
		     ( PCIe_func_rd(eth_100Gb_ctrl_timer_RTT_def + addr_offset1)  = '1' ) then
			PCIe_out_rg(31 downto  0) 					<= timer_rtt_I_reg;	
		elsif (PCIe_func_rd(eth_100Gb_ctrl_timer_RTT_sync_def + addr_offset)  = '1' ) or
		     ( PCIe_func_rd(eth_100Gb_ctrl_timer_RTT_sync_def + addr_offset1)  = '1' ) then
			PCIe_out_rg(31 downto  0) 					<= timer_rtt_I_sync_reg;	
		elsif( PCIe_func_rd(eth_100Gb_ctrl_timer_persist_def + addr_offset)  = '1' ) or
		     ( PCIe_func_rd(eth_100Gb_ctrl_timer_persist_def + addr_offset1)  = '1' ) then
			PCIe_out_rg(31 downto  0) 					<= timer_persist_I_reg;		
		elsif ( PCIe_func_rd(eth_100Gb_ctrl_thresold_retrans_def + addr_offset)  = '1' ) or
		      ( PCIe_func_rd(eth_100Gb_ctrl_thresold_retrans_def + addr_offset1)  = '1' ) then
			PCIe_out_rg(31 downto  0) 					<= thresold_retrans_reg;
		elsif ( PCIe_func_rd(eth_100Gb_ctrl_Rexmt_CWND_Sh_def + addr_offset)  = '1' ) or
		      ( PCIe_func_rd(eth_100Gb_ctrl_Rexmt_CWND_Sh_def + addr_offset1)  = '1' ) then
			PCIe_out_rg(31 downto  0) 					<= TCP_Rexmt_CWND_sh_reg;	
--		else	
--			PCIe_out_rg <= (others => '0');
		end if;

	end if;
end process;

PCIe_out		<= PCIe_out_rg;

process(clock )
begin
 if rising_edge(clock) then
	NETWORK_reg	<= IP_S_reg and IP_NETWORK_reg;
end if;
end process;


	PORT0_D						<= PORT0_D_reg;
	PORT0_S						<= PORT0_S_reg;
	PORT1_D						<= PORT1_D_reg;
	PORT1_S						<= PORT1_S_reg;
	MAC_S						<= MAC; 
	IP_D						<= IP_D_reg;
	IP_S						<= IP_S_reg;
	IP_NETWORK					<= IP_NETWORK_reg;
	IP_GATEWAY					<= IP_GATEWAY_reg;
	NETWORK						<= NETWORK_reg;
		
	TCP_max_size				<= TCP_max_size_reg;
	TCP_scale					<= TCP_scale_reg;
	TCP_win						<= TCP_win_reg;
	TCP_TS						<= TCP_TS_reg;
	TCP_TS_rply					<= TCP_TS_rply_reg;
	snd_cwnd					<= snd_cwnd_reg;
	timer_rtt_I					<= timer_rtt_I_reg;
	timer_rtt_I_sync			<= timer_rtt_I_sync_reg;
	timer_persist_I				<= timer_persist_I_reg;
	Init_bit					<= Init_reg;
	thresold_retrans			<= thresold_retrans_reg;
	TCP_Rexmt_CWND_sh			<= TCP_Rexmt_CWND_sh_reg;
		
 

end behavioral;
