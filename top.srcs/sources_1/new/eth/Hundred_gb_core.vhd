----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 18.08.2017 15:39:25
-- Design Name: 
-- Module Name: Hundred_gb_core - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

use work.address_table.all;
use work.interface.all;


-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Hundred_gb_core is
 Generic(addr_offset_100G_eth		: integer := 0);
  Port (
		init_clk                       : in std_logic;
		Serdes_Reset_p         	       : in std_logic;
  
		rx_lbus                        : out rx_user_ift;
		mac_rx_clk					   : out std_logic;
		tx_lbus                        : in tx_user_ift;
		tx_lbus_ready				   : out std_logic;
		mac_tx_clk	                   : out std_logic;
	
		usr_clk						   : in std_logic;
		usr_rst_n					   : in std_logic;
		usr_func_wr					   : in std_logic_vector(16383 downto 0); 
		usr_wen						   : in std_logic;
		usr_data_wr				  	   : in std_logic_vector(63 downto 0); 
		                           
		usr_func_rd					   : in std_logic_vector(16383 downto 0); 
		usr_rden					   : in std_logic;
		usr_data_rd					   : out std_logic_vector(63 downto 0);
		usr_rd_val					   : out std_logic := '1';
		
		QSFP_TX4_N					   : out std_logic;
		QSFP_TX4_P                 	   : out std_logic;
		QSFP_TX3_N                 	   : out std_logic;
		QSFP_TX3_P                 	   : out std_logic;
		QSFP_TX2_N                 	   : out std_logic;
		QSFP_TX2_P                 	   : out std_logic;
		QSFP_TX1_N                 	   : out std_logic;
		QSFP_TX1_P                 	   : out std_logic;
	
		QSFP_RX4_P                 	   : in std_logic;
		QSFP_RX4_N                 	   : in std_logic;
		QSFP_RX3_P                 	   : in std_logic;
		QSFP_RX3_N                 	   : in std_logic;
		QSFP_RX2_P                 	   : in std_logic;
		QSFP_RX2_N                 	   : in std_logic;
		QSFP_RX1_P                 	   : in std_logic;
		QSFP_RX1_N                 	   : in std_logic;
		
		GT_ref_clock_p         		   : in std_logic;  
		GT_ref_clock_n         		   : in std_logic;
		
		status_out						: out std_logic_vector(31 downto 0):= x"00000000" 
  
   );
end Hundred_gb_core;

--*************************************************************************************
architecture Behavioral of Hundred_gb_core is
attribute ASYNC_REG			: string;

signal SERDES_resetn                : std_logic;

COMPONENT  MAC100GBv1_0
  PORT (
    gt_txp_out                          : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    gt_txn_out                          : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    gt_rxp_in                           : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    gt_rxn_in                           : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    gt_txusrclk2                        : OUT STD_LOGIC;
    gt_loopback_in                      : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    gt_eyescanreset                     : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    gt_eyescantrigger                   : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    gt_rxcdrhold                        : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    gt_rxpolarity                       : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    gt_rxrate                           : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    gt_txdiffctrl                       : IN STD_LOGIC_VECTOR(19 DOWNTO 0);
    gt_txpolarity                       : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    gt_txinhibit                        : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    gt_txpippmen                        : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    gt_txpippmsel                       : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    gt_txpostcursor                     : IN STD_LOGIC_VECTOR(19 DOWNTO 0);
    gt_txprbsforceerr                   : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    gt_txprecursor                      : IN STD_LOGIC_VECTOR(19 DOWNTO 0);
    gt_eyescandataerror                 : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    gt_ref_clk_out                      : OUT STD_LOGIC;
    gt_rxrecclkout                      : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    gt_powergoodout                     : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    gt_txbufstatus                      : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    gt_rxdfelpmreset                    : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    gt_rxlpmen                          : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    gt_rxprbscntreset                   : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    gt_rxprbserr                        : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    gt_rxprbssel                        : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    gt_rxresetdone                      : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    gt_txprbssel                        : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    gt_txresetdone                      : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    gt_rxbufstatus                      : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    gtwiz_reset_tx_datapath             : IN STD_LOGIC;
    gtwiz_reset_rx_datapath             : IN STD_LOGIC;
    gt_drpclk                           : IN STD_LOGIC;
    gt0_drpdo                           : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    gt0_drprdy                          : OUT STD_LOGIC;
    gt0_drpen                           : IN STD_LOGIC;
    gt0_drpwe                           : IN STD_LOGIC;
    gt0_drpaddr                         : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
    gt0_drpdi                           : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    gt1_drpdo                           : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    gt1_drprdy                          : OUT STD_LOGIC;
    gt1_drpen                           : IN STD_LOGIC;
    gt1_drpwe                           : IN STD_LOGIC;
    gt1_drpaddr                         : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
    gt1_drpdi                           : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    gt2_drpdo                           : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    gt2_drprdy                          : OUT STD_LOGIC;
    gt2_drpen                           : IN STD_LOGIC;
    gt2_drpwe                           : IN STD_LOGIC;
    gt2_drpaddr                         : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
    gt2_drpdi                           : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    gt3_drpdo                           : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    gt3_drprdy                          : OUT STD_LOGIC;
    gt3_drpen                           : IN STD_LOGIC;
    gt3_drpwe                           : IN STD_LOGIC;
    gt3_drpaddr                         : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
    gt3_drpdi                           : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    s_axi_aclk                          : IN STD_LOGIC;
    s_axi_sreset                        : IN STD_LOGIC;
    pm_tick                             : IN STD_LOGIC;
    s_axi_awaddr                        : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    s_axi_awvalid                       : IN STD_LOGIC;
    s_axi_awready                       : OUT STD_LOGIC;
    s_axi_wdata                         : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    s_axi_wstrb                         : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    s_axi_wvalid                        : IN STD_LOGIC;
    s_axi_wready                        : OUT STD_LOGIC;
    s_axi_bresp                         : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    s_axi_bvalid                        : OUT STD_LOGIC;
    s_axi_bready                        : IN STD_LOGIC;
    s_axi_araddr                        : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    s_axi_arvalid                       : IN STD_LOGIC;
    s_axi_arready                       : OUT STD_LOGIC;
    s_axi_rdata                         : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    s_axi_rresp                         : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    s_axi_rvalid                        : OUT STD_LOGIC;
    s_axi_rready                        : IN STD_LOGIC;
    stat_rx_rsfec_am_lock0              : OUT STD_LOGIC;
    stat_rx_rsfec_am_lock1              : OUT STD_LOGIC;
    stat_rx_rsfec_am_lock2              : OUT STD_LOGIC;
    stat_rx_rsfec_am_lock3              : OUT STD_LOGIC;
    stat_rx_rsfec_corrected_cw_inc      : OUT STD_LOGIC;
    stat_rx_rsfec_cw_inc                : OUT STD_LOGIC;
    stat_rx_rsfec_err_count0_inc        : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    stat_rx_rsfec_err_count1_inc        : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    stat_rx_rsfec_err_count2_inc        : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    stat_rx_rsfec_err_count3_inc        : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    stat_rx_rsfec_hi_ser                : OUT STD_LOGIC;
    stat_rx_rsfec_lane_alignment_status : OUT STD_LOGIC;
    stat_rx_rsfec_lane_fill_0           : OUT STD_LOGIC_VECTOR(13 DOWNTO 0);
    stat_rx_rsfec_lane_fill_1           : OUT STD_LOGIC_VECTOR(13 DOWNTO 0);
    stat_rx_rsfec_lane_fill_2           : OUT STD_LOGIC_VECTOR(13 DOWNTO 0);
    stat_rx_rsfec_lane_fill_3           : OUT STD_LOGIC_VECTOR(13 DOWNTO 0);
    stat_rx_rsfec_lane_mapping          : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    stat_rx_rsfec_uncorrected_cw_inc    : OUT STD_LOGIC;
    user_reg0                           : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    sys_reset                           : IN STD_LOGIC;
    gt_ref_clk_p                        : IN STD_LOGIC;
    gt_ref_clk_n                        : IN STD_LOGIC;
    init_clk                            : IN STD_LOGIC;
    common0_drpaddr                     : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    common0_drpdi                       : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    common0_drpwe                       : IN STD_LOGIC;
    common0_drpen                       : IN STD_LOGIC;
    common0_drprdy                      : OUT STD_LOGIC;
    common0_drpdo                       : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    rx_dataout0                         : OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
    rx_dataout1                         : OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
    rx_dataout2                         : OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
    rx_dataout3                         : OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
    rx_enaout0                          : OUT STD_LOGIC;
    rx_enaout1                          : OUT STD_LOGIC;
    rx_enaout2                          : OUT STD_LOGIC;
    rx_enaout3                          : OUT STD_LOGIC;
    rx_eopout0                          : OUT STD_LOGIC;
    rx_eopout1                          : OUT STD_LOGIC;
    rx_eopout2                          : OUT STD_LOGIC;
    rx_eopout3                          : OUT STD_LOGIC;
    rx_errout0                          : OUT STD_LOGIC;
    rx_errout1                          : OUT STD_LOGIC;
    rx_errout2                          : OUT STD_LOGIC;
    rx_errout3                          : OUT STD_LOGIC;
    rx_mtyout0                          : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    rx_mtyout1                          : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    rx_mtyout2                          : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    rx_mtyout3                          : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    rx_sopout0                          : OUT STD_LOGIC;
    rx_sopout1                          : OUT STD_LOGIC;
    rx_sopout2                          : OUT STD_LOGIC;
    rx_sopout3                          : OUT STD_LOGIC;
    rx_otn_bip8_0                       : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    rx_otn_bip8_1                       : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    rx_otn_bip8_2                       : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    rx_otn_bip8_3                       : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    rx_otn_bip8_4                       : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    rx_otn_data_0                       : OUT STD_LOGIC_VECTOR(65 DOWNTO 0);
    rx_otn_data_1                       : OUT STD_LOGIC_VECTOR(65 DOWNTO 0);
    rx_otn_data_2                       : OUT STD_LOGIC_VECTOR(65 DOWNTO 0);
    rx_otn_data_3                       : OUT STD_LOGIC_VECTOR(65 DOWNTO 0);
    rx_otn_data_4                       : OUT STD_LOGIC_VECTOR(65 DOWNTO 0);
    rx_otn_ena                          : OUT STD_LOGIC;
    rx_otn_lane0                        : OUT STD_LOGIC;
    rx_otn_vlmarker                     : OUT STD_LOGIC;
    rx_preambleout                      : OUT STD_LOGIC_VECTOR(55 DOWNTO 0);
    usr_rx_reset                        : OUT STD_LOGIC;
    gt_rxusrclk2                        : OUT STD_LOGIC;
    stat_rx_aligned                     : OUT STD_LOGIC;
    stat_rx_aligned_err                 : OUT STD_LOGIC;
    stat_rx_bad_code                    : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    stat_rx_bad_fcs                     : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    stat_rx_bad_preamble                : OUT STD_LOGIC;
    stat_rx_bad_sfd                     : OUT STD_LOGIC;
    stat_rx_bip_err_0                   : OUT STD_LOGIC;
    stat_rx_bip_err_1                   : OUT STD_LOGIC;
    stat_rx_bip_err_10                  : OUT STD_LOGIC;
    stat_rx_bip_err_11                  : OUT STD_LOGIC;
    stat_rx_bip_err_12                  : OUT STD_LOGIC;
    stat_rx_bip_err_13                  : OUT STD_LOGIC;
    stat_rx_bip_err_14                  : OUT STD_LOGIC;
    stat_rx_bip_err_15                  : OUT STD_LOGIC;
    stat_rx_bip_err_16                  : OUT STD_LOGIC;
    stat_rx_bip_err_17                  : OUT STD_LOGIC;
    stat_rx_bip_err_18                  : OUT STD_LOGIC;
    stat_rx_bip_err_19                  : OUT STD_LOGIC;
    stat_rx_bip_err_2                   : OUT STD_LOGIC;
    stat_rx_bip_err_3                   : OUT STD_LOGIC;
    stat_rx_bip_err_4                   : OUT STD_LOGIC;
    stat_rx_bip_err_5                   : OUT STD_LOGIC;
    stat_rx_bip_err_6                   : OUT STD_LOGIC;
    stat_rx_bip_err_7                   : OUT STD_LOGIC;
    stat_rx_bip_err_8                   : OUT STD_LOGIC;
    stat_rx_bip_err_9                   : OUT STD_LOGIC;
    stat_rx_block_lock                  : OUT STD_LOGIC_VECTOR(19 DOWNTO 0);
    stat_rx_broadcast                   : OUT STD_LOGIC;
    stat_rx_fragment                    : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    stat_rx_framing_err_0               : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_1               : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_10              : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_11              : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_12              : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_13              : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_14              : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_15              : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_16              : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_17              : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_18              : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_19              : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_2               : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_3               : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_4               : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_5               : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_6               : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_7               : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_8               : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_9               : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_valid_0         : OUT STD_LOGIC;
    stat_rx_framing_err_valid_1         : OUT STD_LOGIC;
    stat_rx_framing_err_valid_10        : OUT STD_LOGIC;
    stat_rx_framing_err_valid_11        : OUT STD_LOGIC;
    stat_rx_framing_err_valid_12        : OUT STD_LOGIC;
    stat_rx_framing_err_valid_13        : OUT STD_LOGIC;
    stat_rx_framing_err_valid_14        : OUT STD_LOGIC;
    stat_rx_framing_err_valid_15        : OUT STD_LOGIC;
    stat_rx_framing_err_valid_16        : OUT STD_LOGIC;
    stat_rx_framing_err_valid_17        : OUT STD_LOGIC;
    stat_rx_framing_err_valid_18        : OUT STD_LOGIC;
    stat_rx_framing_err_valid_19        : OUT STD_LOGIC;
    stat_rx_framing_err_valid_2         : OUT STD_LOGIC;
    stat_rx_framing_err_valid_3         : OUT STD_LOGIC;
    stat_rx_framing_err_valid_4         : OUT STD_LOGIC;
    stat_rx_framing_err_valid_5         : OUT STD_LOGIC;
    stat_rx_framing_err_valid_6         : OUT STD_LOGIC;
    stat_rx_framing_err_valid_7         : OUT STD_LOGIC;
    stat_rx_framing_err_valid_8         : OUT STD_LOGIC;
    stat_rx_framing_err_valid_9         : OUT STD_LOGIC;
    stat_rx_got_signal_os               : OUT STD_LOGIC;
    stat_rx_hi_ber                      : OUT STD_LOGIC;
    stat_rx_inrangeerr                  : OUT STD_LOGIC;
    stat_rx_internal_local_fault        : OUT STD_LOGIC;
    stat_rx_jabber                      : OUT STD_LOGIC;
    stat_rx_local_fault                 : OUT STD_LOGIC;
    stat_rx_mf_err                      : OUT STD_LOGIC_VECTOR(19 DOWNTO 0);
    stat_rx_mf_len_err                  : OUT STD_LOGIC_VECTOR(19 DOWNTO 0);
    stat_rx_mf_repeat_err               : OUT STD_LOGIC_VECTOR(19 DOWNTO 0);
    stat_rx_misaligned                  : OUT STD_LOGIC;
    stat_rx_multicast                   : OUT STD_LOGIC;
    stat_rx_oversize                    : OUT STD_LOGIC;
    stat_rx_packet_1024_1518_bytes      : OUT STD_LOGIC;
    stat_rx_packet_128_255_bytes        : OUT STD_LOGIC;
    stat_rx_packet_1519_1522_bytes      : OUT STD_LOGIC;
    stat_rx_packet_1523_1548_bytes      : OUT STD_LOGIC;
    stat_rx_packet_1549_2047_bytes      : OUT STD_LOGIC;
    stat_rx_packet_2048_4095_bytes      : OUT STD_LOGIC;
    stat_rx_packet_256_511_bytes        : OUT STD_LOGIC;
    stat_rx_packet_4096_8191_bytes      : OUT STD_LOGIC;
    stat_rx_packet_512_1023_bytes       : OUT STD_LOGIC;
    stat_rx_packet_64_bytes             : OUT STD_LOGIC;
    stat_rx_packet_65_127_bytes         : OUT STD_LOGIC;
    stat_rx_packet_8192_9215_bytes      : OUT STD_LOGIC;
    stat_rx_packet_bad_fcs              : OUT STD_LOGIC;
    stat_rx_packet_large                : OUT STD_LOGIC;
    stat_rx_packet_small                : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    stat_rx_pause                       : OUT STD_LOGIC;
    stat_rx_pause_quanta0               : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    stat_rx_pause_quanta1               : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    stat_rx_pause_quanta2               : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    stat_rx_pause_quanta3               : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    stat_rx_pause_quanta4               : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    stat_rx_pause_quanta5               : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    stat_rx_pause_quanta6               : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    stat_rx_pause_quanta7               : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    stat_rx_pause_quanta8               : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    stat_rx_pause_req                   : OUT STD_LOGIC_VECTOR(8 DOWNTO 0);
    stat_rx_pause_valid                 : OUT STD_LOGIC_VECTOR(8 DOWNTO 0);
    stat_rx_user_pause                  : OUT STD_LOGIC;
    core_rx_reset                       : IN STD_LOGIC;
    rx_clk                              : IN STD_LOGIC;
    stat_rx_received_local_fault        : OUT STD_LOGIC;
    stat_rx_remote_fault                : OUT STD_LOGIC;
    stat_rx_status                      : OUT STD_LOGIC;
    stat_rx_stomped_fcs                 : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    stat_rx_synced                      : OUT STD_LOGIC_VECTOR(19 DOWNTO 0);
    stat_rx_synced_err                  : OUT STD_LOGIC_VECTOR(19 DOWNTO 0);
    stat_rx_test_pattern_mismatch       : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    stat_rx_toolong                     : OUT STD_LOGIC;
    stat_rx_total_bytes                 : OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
    stat_rx_total_good_bytes            : OUT STD_LOGIC_VECTOR(13 DOWNTO 0);
    stat_rx_total_good_packets          : OUT STD_LOGIC;
    stat_rx_total_packets               : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    stat_rx_truncated                   : OUT STD_LOGIC;
    stat_rx_undersize                   : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    stat_rx_unicast                     : OUT STD_LOGIC;
    stat_rx_vlan                        : OUT STD_LOGIC;
    stat_rx_pcsl_demuxed                : OUT STD_LOGIC_VECTOR(19 DOWNTO 0);
    stat_rx_pcsl_number_0               : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_1               : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_10              : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_11              : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_12              : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_13              : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_14              : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_15              : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_16              : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_17              : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_18              : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_19              : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_2               : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_3               : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_4               : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_5               : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_6               : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_7               : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_8               : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_9               : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_tx_bad_fcs                     : OUT STD_LOGIC;
    stat_tx_broadcast                   : OUT STD_LOGIC;
    stat_tx_frame_error                 : OUT STD_LOGIC;
    stat_tx_local_fault                 : OUT STD_LOGIC;
    stat_tx_multicast                   : OUT STD_LOGIC;
    stat_tx_packet_1024_1518_bytes      : OUT STD_LOGIC;
    stat_tx_packet_128_255_bytes        : OUT STD_LOGIC;
    stat_tx_packet_1519_1522_bytes      : OUT STD_LOGIC;
    stat_tx_packet_1523_1548_bytes      : OUT STD_LOGIC;
    stat_tx_packet_1549_2047_bytes      : OUT STD_LOGIC;
    stat_tx_packet_2048_4095_bytes      : OUT STD_LOGIC;
    stat_tx_packet_256_511_bytes        : OUT STD_LOGIC;
    stat_tx_packet_4096_8191_bytes      : OUT STD_LOGIC;
    stat_tx_packet_512_1023_bytes       : OUT STD_LOGIC;
    stat_tx_packet_64_bytes             : OUT STD_LOGIC;
    stat_tx_packet_65_127_bytes         : OUT STD_LOGIC;
    stat_tx_packet_8192_9215_bytes      : OUT STD_LOGIC;
    stat_tx_packet_large                : OUT STD_LOGIC;
    stat_tx_packet_small                : OUT STD_LOGIC;
    stat_tx_total_bytes                 : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    stat_tx_total_good_bytes            : OUT STD_LOGIC_VECTOR(13 DOWNTO 0);
    stat_tx_total_good_packets          : OUT STD_LOGIC;
    stat_tx_total_packets               : OUT STD_LOGIC;
    stat_tx_unicast                     : OUT STD_LOGIC;
    stat_tx_vlan                        : OUT STD_LOGIC;
    ctl_tx_send_idle                    : IN STD_LOGIC;
    ctl_tx_send_rfi                     : IN STD_LOGIC;
    ctl_tx_send_lfi                     : IN STD_LOGIC;
    core_tx_reset                       : IN STD_LOGIC;
    stat_tx_pause_valid                 : OUT STD_LOGIC_VECTOR(8 DOWNTO 0);
    stat_tx_pause                       : OUT STD_LOGIC;
    stat_tx_user_pause                  : OUT STD_LOGIC;
    ctl_tx_pause_req                    : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
    ctl_tx_resend_pause                 : IN STD_LOGIC;
    tx_ovfout                           : OUT STD_LOGIC;
    tx_rdyout                           : OUT STD_LOGIC;
    tx_unfout                           : OUT STD_LOGIC;
    tx_datain0                          : IN STD_LOGIC_VECTOR(127 DOWNTO 0);
    tx_datain1                          : IN STD_LOGIC_VECTOR(127 DOWNTO 0);
    tx_datain2                          : IN STD_LOGIC_VECTOR(127 DOWNTO 0);
    tx_datain3                          : IN STD_LOGIC_VECTOR(127 DOWNTO 0);
    tx_enain0                           : IN STD_LOGIC;
    tx_enain1                           : IN STD_LOGIC;
    tx_enain2                           : IN STD_LOGIC;
    tx_enain3                           : IN STD_LOGIC;
    tx_eopin0                           : IN STD_LOGIC;
    tx_eopin1                           : IN STD_LOGIC;
    tx_eopin2                           : IN STD_LOGIC;
    tx_eopin3                           : IN STD_LOGIC;
    tx_errin0                           : IN STD_LOGIC;
    tx_errin1                           : IN STD_LOGIC;
    tx_errin2                           : IN STD_LOGIC;
    tx_errin3                           : IN STD_LOGIC;
    tx_mtyin0                           : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    tx_mtyin1                           : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    tx_mtyin2                           : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    tx_mtyin3                           : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    tx_sopin0                           : IN STD_LOGIC;
    tx_sopin1                           : IN STD_LOGIC;
    tx_sopin2                           : IN STD_LOGIC;
    tx_sopin3                           : IN STD_LOGIC;
    tx_preamblein                       : IN STD_LOGIC_VECTOR(55 DOWNTO 0);
    usr_tx_reset                        : OUT STD_LOGIC;
    core_drp_reset                      : IN STD_LOGIC;
    drp_clk                             : IN STD_LOGIC;
    drp_addr                            : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
    drp_di                              : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    drp_en                              : IN STD_LOGIC;
    drp_do                              : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    drp_rdy                             : OUT STD_LOGIC;
    drp_we                              : IN STD_LOGIC         
  );
END COMPONENT;

signal stat_rx_rsfec_am_lock0                    : STD_LOGIC;
signal stat_rx_rsfec_am_lock1                    : STD_LOGIC;
signal stat_rx_rsfec_am_lock2                    : STD_LOGIC;
signal stat_rx_rsfec_am_lock3                    : STD_LOGIC;
signal stat_rx_rsfec_corrected_cw_inc            : STD_LOGIC;
signal stat_rx_rsfec_cw_inc                      : STD_LOGIC;
signal stat_rx_rsfec_err_count0_inc              : STD_LOGIC_VECTOR(2 DOWNTO 0);
signal stat_rx_rsfec_err_count1_inc              : STD_LOGIC_VECTOR(2 DOWNTO 0);
signal stat_rx_rsfec_err_count2_inc              : STD_LOGIC_VECTOR(2 DOWNTO 0);
signal stat_rx_rsfec_err_count3_inc              : STD_LOGIC_VECTOR(2 DOWNTO 0);
signal stat_rx_rsfec_hi_ser                      : STD_LOGIC;
signal stat_rx_rsfec_lane_alignment_status       : STD_LOGIC;
signal stat_rx_rsfec_lane_fill_0                 : STD_LOGIC_VECTOR(13 DOWNTO 0);
signal stat_rx_rsfec_lane_fill_1                 : STD_LOGIC_VECTOR(13 DOWNTO 0);
signal stat_rx_rsfec_lane_fill_2                 : STD_LOGIC_VECTOR(13 DOWNTO 0);
signal stat_rx_rsfec_lane_fill_3                 : STD_LOGIC_VECTOR(13 DOWNTO 0);
signal stat_rx_rsfec_lane_mapping                : STD_LOGIC_VECTOR(7 DOWNTO 0);
signal stat_rx_rsfec_uncorrected_cw_inc          : STD_LOGIC;

signal counter_reset_resync_gt_rxusrclk2          : STD_LOGIC;
  
signal RX_fec_corrected_counter                 : STD_LOGIC_VECTOR(63 DOWNTO 0);
signal RX_fec_uncorrected_counter                : STD_LOGIC_VECTOR(63 DOWNTO 0);

signal gt_txprbsforceerr_async      							: std_logic;
signal gt_txprbsforceerr_sync       							: std_logic;
attribute ASYNC_REG of  gt_txprbsforceerr_sync  				: signal is "TRUE";
signal gt_txprbsforceerr            							: std_logic_vector(3 downto 0);

signal gt_txprbssel_async                                       : std_logic_vector(3 downto 0);            
signal gt_txprbssel_sync                                        : std_logic_vector(3 downto 0);   
attribute ASYNC_REG of  gt_txprbssel_sync  : signal is "TRUE";        
signal gt_txprbssel                                             : std_logic_vector(15 downto 0);   

signal gt_rxprbserr                                             :  std_logic_vector(3 downto 0);

signal gt_rxprbssel_async                                       : std_logic_vector(3 downto 0);            
signal gt_rxprbssel_sync                                        : std_logic_vector(3 downto 0);   
attribute ASYNC_REG of  gt_rxprbssel_sync  : signal is "TRUE";        
signal gt_rxprbssel                                             : std_logic_vector(15 downto 0);  

signal gt_rxprbscntreset                                        : std_logic_vector(3 downto 0); 
signal gt_rxprbscntreset_async									: std_logic;
signal gt_rxprbscntreset_sync									: std_logic;
		
signal rx_rxlpmen												: std_logic;
signal gt_powergoodout											: std_logic_vector(3 downto 0);
signal powergood												: std_logic;
 
component statistic_15bcounter is
    Port ( 
    	reset : in STD_LOGIC;
		clock : in STD_LOGIC;
  		ena : in STD_LOGIC;
 		values : out STD_LOGIC_VECTOR (15 downto 0)
 		);
end component;

component clk_wrapper is
	port
	(
		gt_ref_CLKi_p		: in std_logic;
		gt_ref_CLKi_n		: in std_logic;
		gt_ref_CLKo			: out std_logic
	 );
end component;
 
-- global signal Reset Clock
signal gt_txusrclk2                                             : std_logic;               
signal gt_rxusrclk2                                             : std_logic;               
                            
signal rx_clk                                                   : std_logic; 
signal counter_reset                                          	: std_logic;
signal counter_reset_resync                                    	: std_logic;

signal sys_reset                                                : std_logic; 
signal usr_rst_tx_clk											: std_logic;
signal rx_lbus_cell                    							: rx_user_ift;

-- statistics signals  
signal stat_rx_local_fault                                      : std_logic;
signal stat_rx_misaligned                                       : std_logic;
signal stat_rx_status                                           : std_logic;
signal stat_rx_synced                                           : std_logic_vector(19 downto 0);
signal stat_rx_synced_err                                       : std_logic_vector(19 downto 0);  

signal counter_reset_sync_txclk                                 : std_logic;
 
type stat_rx_bip_err_type	is array (0 to 3) of std_logic;
signal stat_rx_bip_err                                          : stat_rx_bip_err_type;
type stat_rx_bip_err_cnt_type	is array (0 to 3) of std_logic_vector(63 downto 0);
signal stat_rx_bip_err_cnt                                      : stat_rx_bip_err_cnt_type;

signal stat_rx_total_bytes				                        : STD_LOGIC_vector(6 downto 0);
signal stat_rx_total_bytes_cnt		                            : std_logic_vector(31 downto 0);

signal stat_rx_packet					                        : std_logic_vector(11 downto 0);
signal stat_rx_packet_size				                        : std_logic_vector(11 downto 0);
signal stat_rx_packet_cnt				                        : std_logic_vector(31 downto 12);
signal stat_rx_packet_small			                            : std_logic_vector(2 downto 0);
signal stat_rx_packet_small_cnt		                            : std_logic_vector(31 downto 0);
 
signal stat_rx_aligned 						                    : STD_LOGIC;
signal stat_rx_aligned_err 				                        : STD_LOGIC;
signal stat_rx_bad_code 					                    : STD_LOGIC_VECTOR(2 DOWNTO 0);
signal stat_rx_bad_fcs 						                    : STD_LOGIC_VECTOR(2 DOWNTO 0);
signal stat_rx_bad_preamble 				                    : STD_LOGIC;
signal stat_rx_bad_sfd 						                    : STD_LOGIC;
signal stat_rx_block_lock 					                    : STD_LOGIC_VECTOR(19 DOWNTO 0);
signal stat_rx_fragment 					                    : STD_LOGIC_VECTOR(2 DOWNTO 0);
signal stat_rx_internal_local_fault                             : std_logic;
signal stat_rx_received_local_fault                             : std_logic;
signal inc_tx_total_good_packets			                    : std_logic_vector(31 downto 0);
signal inc_rx_total_good_packets_ena	                        : std_logic;
signal tx_ena_lbus				                                : std_logic;
signal tx_ena_resync				                            : std_logic;
signal tx_ena_lbus_resync		                                : std_logic_vector(3 downto 0);
signal tx_ovfout 			                                    : STD_LOGIC;
signal tx_ovfout_mem		                                    : STD_LOGIC;
signal tx_rdyout 			                                    : STD_LOGIC;
signal tx_unfout 			                                    : STD_LOGIC;
signal tx_unfout_mem		                                    : STD_LOGIC;
signal stat_tx_local_fault                                      : std_logic;
signal stat_tx_bad_fcs                							: STD_LOGIC;
signal stat_tx_broadcast              							: STD_LOGIC;
signal stat_tx_frame_error            							: STD_LOGIC;
signal stat_tx_local_fault_cnt                                  : std_logic_vector(15 downto 0);
signal stat_tx_bad_fcs_cnt                						: STD_LOGIC_vector(15 downto 0);
signal stat_tx_broadcast_cnt              						: STD_LOGIC_vector(15 downto 0);
signal stat_tx_frame_error_cnt            						: STD_LOGIC_vector(15 downto 0);

signal rx_frame_l0_err											: std_logic_vector(1 downto 0);
signal rx_frame_l1_err											: std_logic_vector(1 downto 0);
signal rx_frame_l2_err											: std_logic_vector(1 downto 0);
signal rx_frame_l3_err											: std_logic_vector(1 downto 0);

signal rx_frame_l0_err_val										: std_logic;
signal rx_frame_l1_err_val										: std_logic;
signal rx_frame_l2_err_val										: std_logic;
signal rx_frame_l3_err_val										: std_logic;

signal rx_frame_l0_err_cnt										: std_logic_vector(63 downto 0);
signal rx_frame_l1_err_cnt										: std_logic_vector(63 downto 0);
signal rx_frame_l2_err_cnt										: std_logic_vector(63 downto 0);
signal rx_frame_l3_err_cnt										: std_logic_vector(63 downto 0);

-- control DRP bus accesses
signal DRP_clock											    : std_logic;
signal DRP_rst												    : std_logic;
signal drp_addr 							                    : STD_LOGIC_VECTOR(9 DOWNTO 0) := "0000000000";
signal drp_di 								                    : STD_LOGIC_VECTOR(15 DOWNTO 0) := x"0000";
signal drp_en 								                    : STD_LOGIC_vector(1 downto 0) :="00";
signal drp_en_cell							                    : STD_LOGIC  := '0';
signal drp_do 								                    : STD_LOGIC_VECTOR(15 DOWNTO 0) := x"0000";
signal drp_data							                        : STD_LOGIC_VECTOR(15 DOWNTO 0) := x"0000";
signal drp_rdy 							                        : STD_LOGIC  := '0';
signal drp_we 								                    : STD_LOGIC_vector(1 downto 0);
signal drp_done							                        : std_logic  := '0'; 
signal gt_reset                                                 : std_logic  := '0';
signal data_in_rg                                               : std_logic_vector(63 downto 0);


signal gtx_ch_drp_addr 							                : STD_LOGIC_VECTOR(9 DOWNTO 0):= "0000000000";
signal gtx_ch_drp_di 								            : STD_LOGIC_VECTOR(15 DOWNTO 0) := x"0000";
signal gtx_ch_drp_do 								            : STD_LOGIC_VECTOR(15 DOWNTO 0) := x"0000";
signal gtx_ch_drp_we 								            : STD_LOGIC   := '0';  
signal gtx_ch_drp_we_sync							            : STD_LOGIC   := '0';  
signal gtx_ch_drp_rd								            : STD_LOGIC   := '0';  
signal gtx_ch_drp_rd_sync							            : STD_LOGIC   := '0';  
signal gtx_ch_drp_en								            : STD_LOGIC_vector(3 downto 0) := x"0" ;  
signal gtx_ch_drp_done								            : STD_LOGIC_vector(3 downto 0) := x"0" ;  
 
signal gt0_ch_drp_en 								            : STD_LOGIC   := '0'; 
signal gt0_ch_drp_do  								            : STD_LOGIC_VECTOR(15 DOWNTO 0) := x"0000";  
signal gt0_ch_drp_rdy 							                : STD_LOGIC   := '0';
signal gt0_ch_drp_rdy_sync						                : STD_LOGIC   := '0';
signal gt0_ch_drp_we 								            : STD_LOGIC   := '0';  
 
signal gt1_ch_drp_en 								            : STD_LOGIC   := '0'; 
signal gt1_ch_drp_do  								            : STD_LOGIC_VECTOR(15 DOWNTO 0) := x"0000";  
signal gt1_ch_drp_rdy 							                : STD_LOGIC   := '0';
signal gt1_ch_drp_rdy_sync						                : STD_LOGIC   := '0';
signal gt1_ch_drp_we 								            : STD_LOGIC   := '0';  
 
signal gt2_ch_drp_en 								            : STD_LOGIC  := '0'; 
signal gt2_ch_drp_do  								            : STD_LOGIC_VECTOR(15 DOWNTO 0) := x"0000";  
signal gt2_ch_drp_rdy 							                : STD_LOGIC  := '0';
signal gt2_ch_drp_rdy_sync						                : STD_LOGIC  := '0';
signal gt2_ch_drp_we 								            : STD_LOGIC  := '0';  
 
signal gt3_ch_drp_en 								            : STD_LOGIC  := '0'; 
signal gt3_ch_drp_do  								            : STD_LOGIC_VECTOR(15 DOWNTO 0):= x"0000"; 
signal gt3_ch_drp_rdy 							                : STD_LOGIC  := '0';
signal gt3_ch_drp_rdy_sync						                : STD_LOGIC  := '0';
signal gt3_ch_drp_we 								            : STD_LOGIC  := '0'; 
 
 -- Signals for monitoring   next implemetnations
signal gt_rxrecclkout                                           : std_logic_vector(3 downto 0);
--signal gt_eyescanreset					                        : std_logic_vector(3 downto 0);
--signal gt_eyescantrigger          	                            : std_logic_vector(3 downto 0); 
--signal gt_rxpolarity                                            : std_logic_vector(3 downto 0);
--signal gt_rxrate                                                : std_logic_vector(11 downto 0);
signal gt_txdiffctrl                                            : std_logic_vector(19 downto 0);
signal gt_txpolarity                                            : std_logic_vector(3 downto 0);
--signal gt_txinhibit                                             : std_logic_vector(3 downto 0);
signal gt_txpostcursor                                          : std_logic_vector(19 downto 0);
signal gt_txprecursor                                          : std_logic_vector(19 downto 0);
--signal gt_eyescandataerror                                      :  std_logic_vector(3 downto 0);
signal gt_txbufstatus                                           :  std_logic_vector(7 downto 0);   
signal gt_rxlpmen                                               : std_logic_vector(3 downto 0);              
signal gt_rxresetdone                                           :  std_logic_vector(3 downto 0);  
               
signal gt_txresetdone                                           :  std_logic_vector(3 downto 0);                 
signal gt_rxbufstatus                                           :  std_logic_vector(11 downto 0);   
 
-- sognals for Pause Frame control
signal stat_rx_pause                  							: STD_LOGIC;
signal stat_rx_pause_quanta0          							: STD_LOGIC_VECTOR(15 DOWNTO 0);
signal stat_rx_pause_quanta1          							: STD_LOGIC_VECTOR(15 DOWNTO 0);
signal stat_rx_pause_quanta2          							: STD_LOGIC_VECTOR(15 DOWNTO 0);
signal stat_rx_pause_quanta3          							: STD_LOGIC_VECTOR(15 DOWNTO 0);
signal stat_rx_pause_quanta4          							: STD_LOGIC_VECTOR(15 DOWNTO 0);
signal stat_rx_pause_quanta5          							: STD_LOGIC_VECTOR(15 DOWNTO 0);
signal stat_rx_pause_quanta6          							: STD_LOGIC_VECTOR(15 DOWNTO 0);
signal stat_rx_pause_quanta7          							: STD_LOGIC_VECTOR(15 DOWNTO 0);
signal stat_rx_pause_quanta8          							: STD_LOGIC_VECTOR(15 DOWNTO 0);
signal stat_rx_pause_req              							: STD_LOGIC_VECTOR(8 DOWNTO 0);
signal stat_rx_pause_valid            							: STD_LOGIC_VECTOR(8 DOWNTO 0);
signal stat_rx_user_pause             							: STD_LOGIC;

signal ctl_tx_pause_req               							: STD_LOGIC_VECTOR(8 DOWNTO 0);
signal ctl_tx_pause_req_resync         							: STD_LOGIC_VECTOR(8 DOWNTO 0);
signal ctl_tx_resend_pause            							: STD_LOGIC;
signal ctl_tx_resend_pause_resync      							: STD_LOGIC;

signal stat_rx_pause_counter         							: STD_LOGIC_VECTOR(15 DOWNTO 0);

-- Signals for AXI bus
signal s_axi_pm_tick 		                                    : STD_LOGIC;
signal s_axi_awaddr  		                                    : STD_LOGIC_VECTOR(31 DOWNTO 0);
signal s_axi_awvalid 		                                    : STD_LOGIC;
signal s_axi_awready 		                                    : STD_LOGIC;
signal s_axi_wdata   		                                    : STD_LOGIC_VECTOR(31 DOWNTO 0);
signal s_axi_wstrb   		                                    : STD_LOGIC_VECTOR(3 DOWNTO 0);
signal s_axi_wvalid  		                                    : STD_LOGIC;
signal s_axi_wready  		                                    : STD_LOGIC;
signal s_axi_bresp   		                                    : STD_LOGIC_VECTOR(1 DOWNTO 0);
signal s_axi_bvalid  		                                    : STD_LOGIC;
signal s_axi_bready  		                                    : STD_LOGIC;
signal s_axi_araddr  		                                    : STD_LOGIC_VECTOR(31 DOWNTO 0);
signal s_axi_arvalid 		                                    : STD_LOGIC;
signal s_axi_arready 		                                    : STD_LOGIC;
signal s_axi_rdata   		                                    : STD_LOGIC_VECTOR(31 DOWNTO 0);
signal s_axi_rresp   		                                    : STD_LOGIC_VECTOR(1 DOWNTO 0);
signal s_axi_rvalid  		                                    : STD_LOGIC;
signal s_axi_rready  		                                    : STD_LOGIC;

signal AXI_rq_add                                               : std_logic_vector(31 downto 0);
signal AXI_rq_BE                                                : std_logic_vector(3 downto 0);
signal AXI_rq_dti                                               : std_logic_vector(31 downto 0);
signal AXI_rtn_dto                                              : std_logic_vector(31 downto 0);
signal AXI_rq_rd                                                : std_logic :='0';
signal AXI_rq_wr                                                : std_logic :='0';
signal AXI_status                                               : std_logic_vector(31 downto 0);
signal AXI_done                                                 : std_logic;
signal AXI_tick            	                                    : std_logic;
signal user_reset_n                                             : std_logic;
signal Axi_sys_reset                                            : std_logic;

-- signal to set "analog" parameters
signal tx_diff_ctrl											    : std_logic_vector(4 downto 0):= "11000";
signal txpostcursor											    : std_logic_vector(4 downto 0):= "10100";
signal txprecursor											    : std_logic_vector(4 downto 0):= "00000";
signal tx_polarity											    : std_logic:= '0';
signal tx_polarity_resync									    : std_logic:= '0';
signal txpippmen_reg										    : std_logic_vector(3 downto 0);				
signal txpippmen_reg_resync									    : std_logic_vector(3 downto 0);					
signal txpippmsel_reg										    : std_logic_vector(3 downto 0);
signal gt_loopback_in_rg                                        :  STD_LOGIC_VECTOR(11 DOWNTO 0);  

signal rx_crc_error												: std_logic_vector(15 downto 0); 

attribute mark_debug : string;  
--attribute mark_debug of stat_rx_status              : signal is "true";
 
 
--****************************************************************************************************
--   Code start HERE
--****************************************************************************************************
begin

SERDES_resetn   <= not(Serdes_Reset_p);


reset_resync_i1:entity work.resetn_resync 
port map(
	aresetn				=> SERDES_resetn,
	clock				=> DRP_clock, 
	Resetn_sync			=> DRP_rst 
	);

--clk_wrapper_i1:clk_wrapper  
--	port map
--	(
--		gt_ref_CLKi_p		=>  GT_ref_clock_p,
--		gt_ref_CLKi_n		=>  GT_ref_clock_n,
--		gt_ref_CLKo			=>  gt_ref_clk  
--	 );

-- resync few signals

reset_resync_i2:entity work.resetp_resync  
port map(
	aresetp			=> gt_rxprbscntreset_async,
	clock			=> gt_rxusrclk2, 
	Resetp_sync		=> gt_rxprbscntreset_sync 
	);

reset_resync_i3:entity work.resetn_resync  
port map(
	aresetn			=> SERDES_resetn,
	clock			=> gt_txusrclk2,
	Resetn_sync		=> usr_rst_tx_clk 
	);
	
reset_resync_i4:entity work.resetp_resync  
port map(
	aresetp			=> sys_reset,
	clock			=> init_clk, 
	Resetp_sync		=> Axi_sys_reset 
	);	
				
resync_sig_i4:entity work.resync_sig_gen 
port map( 
	clocko			=> gt_txusrclk2,
	in_s			=> tx_polarity,
	out_s			=> tx_polarity_resync 
	);
	
process(gt_txusrclk2)
begin
	if rising_edge(gt_txusrclk2) then  
		gt_txprbssel_sync		<= gt_txprbssel_async;
	end if;
end process;	

 process(gt_rxusrclk2)
begin
	if rising_edge(gt_rxusrclk2) then 
		gt_rxprbssel_sync	<= gt_rxprbssel_async;
	end if;
end process;	

--  control bit expension
 
gt_txdiffctrl                            	<= tx_diff_ctrl & tx_diff_ctrl & tx_diff_ctrl & tx_diff_ctrl;	--Async signal	
gt_txpostcursor                          	<= txpostcursor & txpostcursor & txpostcursor & txpostcursor;	--Async signal		
gt_txprecursor                          	<= txprecursor & txprecursor & txprecursor & txprecursor;		--Async signal
gt_txpolarity								<= tx_polarity_resync & tx_polarity_resync & tx_polarity_resync & tx_polarity_resync;	

gt_txprbssel								<= gt_txprbssel_sync & gt_txprbssel_sync & gt_txprbssel_sync & gt_txprbssel_sync;
gt_txprbsforceerr							<= gt_txprbsforceerr_sync & gt_txprbsforceerr_sync & gt_txprbsforceerr_sync & gt_txprbsforceerr_sync;
gt_rxprbssel								<= gt_rxprbssel_sync & gt_rxprbssel_sync & gt_rxprbssel_sync &gt_rxprbssel_sync;

gt_rxlpmen                               	<= rx_rxlpmen & rx_rxlpmen & rx_rxlpmen & rx_rxlpmen;	--Async signal
gt_rxprbscntreset                        	<= gt_rxprbscntreset_sync & gt_rxprbscntreset_sync & gt_rxprbscntreset_sync & gt_rxprbscntreset_sync;


-- DRP channel access
DRP_clock		<= init_clk;



process(DRP_clock)
begin
	if rising_edge(DRP_clock) then
		if 		gtx_ch_drp_en(0) = '1' and gt0_ch_drp_rdy = '1' then
			gtx_ch_drp_do	<= gt0_ch_drp_do;
		elsif 	gtx_ch_drp_en(1) = '1' and gt1_ch_drp_rdy = '1' then
			gtx_ch_drp_do	<= gt1_ch_drp_do; 
		elsif 	gtx_ch_drp_en(2) = '1' and gt2_ch_drp_rdy = '1' then
			gtx_ch_drp_do	<= gt2_ch_drp_do; 
		elsif 	gtx_ch_drp_en(3) = '1' and gt3_ch_drp_rdy = '1' then
			gtx_ch_drp_do	<= gt3_ch_drp_do;
		end if;
	end if;
end process;

drp_resync_pulse_i11:entity work.resync_pulse 
port map(
	aresetn	=> '1',
	clocki	=> usr_clk,
	in_s	=> gtx_ch_drp_we,
	clocko	=> DRP_clock,
	out_s	=> gtx_ch_drp_we_sync 
	);


drp_resync_pulse_i12:entity work.resync_pulse 
port map(
	aresetn	=> '1',
	clocki	=> usr_clk,
	in_s	=> gtx_ch_drp_rd,
	clocko	=> DRP_clock,
	out_s	=> gtx_ch_drp_rd_sync 
	);
	

prbs_resync_pulse_i13:entity work.resync_pulse 
port map(
	aresetn	=> '1',
	clocki	=> usr_clk,
	in_s	=> gt_txprbsforceerr_async,
	clocko	=> gt_txusrclk2,
	out_s	=> gt_txprbsforceerr_sync 
	);
	

drp_resync_pulse_i20:entity work.resync_pulse 
port map(
	aresetn	=> '1',
	clocki	=> DRP_clock,
	in_s	=> gt0_ch_drp_rdy,
	clocko	=> usr_clk,
	out_s	=> gt0_ch_drp_rdy_sync 
	);	
	
drp_resync_pulse_i21:entity work.resync_pulse 
port map(
	aresetn	=> '1',
	clocki	=> DRP_clock,
	in_s	=> gt1_ch_drp_rdy,
	clocko	=> usr_clk,
	out_s	=> gt1_ch_drp_rdy_sync 
	);	
	
drp_resync_pulse_i22:entity work.resync_pulse 
port map(
	aresetn	=> '1',
	clocki	=> DRP_clock,
	in_s	=> gt2_ch_drp_rdy,
	clocko	=> usr_clk,
	out_s	=> gt2_ch_drp_rdy_sync 
	);	
	
drp_resync_pulse_i23:entity work.resync_pulse 
port map(
	aresetn	=> '1',
	clocki	=> DRP_clock,
	in_s	=> gt3_ch_drp_rdy,
	clocko	=> usr_clk,
	out_s	=> gt3_ch_drp_rdy_sync 
	);	
		
Pause_resync_pulse_i0:entity work.resync_pulse 
port map(
	aresetn	=> '1',
	clocki	=> usr_clk,
	in_s	=> ctl_tx_resend_pause,
	clocko	=> gt_txusrclk2,
	out_s	=> ctl_tx_resend_pause_resync 
	);		
	
For_i1:for I in 0 to 8 generate	
	Pause_resync_pulse_i0:entity work.resync_pulse 
	port map(
		aresetn	=> '1',
		clocki	=> usr_clk,
		in_s	=> ctl_tx_pause_req(I),
		clocko	=> gt_txusrclk2,
		out_s	=> ctl_tx_pause_req_resync(I) 
		);	
end generate;	

For_i2:for J in 0 to 3 generate	
	TXpipmen_resync_sig_i0:entity work.resync_sig_gen  
	port map( 
		clocko				=> gt_txusrclk2,
		in_s				=> txpippmen_reg(J),
		out_s				=> txpippmen_reg_resync(J)
		);	
end generate;	
		
process(usr_rst_n,usr_clk)
begin
	if usr_rst_n = '0' then
		gtx_ch_drp_done	<= (others => '0');
	elsif rising_edge(usr_clk) then
		if (gtx_ch_drp_we = '1' or gtx_ch_drp_rd = '1') and gtx_ch_drp_en(0) = '1' then
			gtx_ch_drp_done(0)	<= '0';
		elsif gt0_ch_drp_rdy_sync = '1' then
			gtx_ch_drp_done(0)	<= '1';
		end if;
	
		if (gtx_ch_drp_we = '1' or gtx_ch_drp_rd = '1') and gtx_ch_drp_en(1) = '1' then
			gtx_ch_drp_done(1)	<= '0';
		elsif gt0_ch_drp_rdy_sync = '1' then
			gtx_ch_drp_done(1)	<= '1';
		end if;
	
		if (gtx_ch_drp_we = '1' or gtx_ch_drp_rd = '1') and gtx_ch_drp_en(2) = '1' then
			gtx_ch_drp_done(2)	<= '0';
		elsif gt0_ch_drp_rdy_sync = '1' then
			gtx_ch_drp_done(2)	<= '1';
		end if;
	
		if (gtx_ch_drp_we = '1' or gtx_ch_drp_rd = '1') and gtx_ch_drp_en(3) = '1' then
			gtx_ch_drp_done(3)	<= '0';
		elsif gt0_ch_drp_rdy_sync = '1' then
			gtx_ch_drp_done(3)	<= '1';
		end if;
 
	end if;
end process;

gt0_ch_drp_en	<= '1' when (gtx_ch_drp_we_sync = '1' and gtx_ch_drp_en(0) = '1' ) or (gtx_ch_drp_rd_sync = '1' and gtx_ch_drp_en(0) = '1' ) else '0';
gt1_ch_drp_en	<= '1' when (gtx_ch_drp_we_sync = '1' and gtx_ch_drp_en(0) = '1' ) or (gtx_ch_drp_rd_sync = '1' and gtx_ch_drp_en(0) = '1' ) else '0';
gt2_ch_drp_en	<= '1' when (gtx_ch_drp_we_sync = '1' and gtx_ch_drp_en(0) = '1' ) or (gtx_ch_drp_rd_sync = '1' and gtx_ch_drp_en(0) = '1' ) else '0';
gt3_ch_drp_en	<= '1' when (gtx_ch_drp_we_sync = '1' and gtx_ch_drp_en(0) = '1' ) or (gtx_ch_drp_rd_sync = '1' and gtx_ch_drp_en(0) = '1' ) else '0';

gt0_ch_drp_we	<= '1' when (gtx_ch_drp_we_sync = '1' and gtx_ch_drp_en(0) = '1' ) else '0';
gt1_ch_drp_we	<= '1' when (gtx_ch_drp_we_sync = '1' and gtx_ch_drp_en(0) = '1' ) else '0';
gt2_ch_drp_we	<= '1' when (gtx_ch_drp_we_sync = '1' and gtx_ch_drp_en(0) = '1' ) else '0';
gt3_ch_drp_we	<= '1' when (gtx_ch_drp_we_sync = '1' and gtx_ch_drp_en(0) = '1' ) else '0';

-- SERDES  MAC100Gb instantiation
caui4:MAC100GBv1_0
  PORT MAP(
  gt_rxp_in(3)	               		        => QSFP_RX4_P                      ,
  gt_rxp_in(2) 	                            => QSFP_RX3_P                      ,
  gt_rxp_in(1) 	                            => QSFP_RX2_P                      ,
  gt_rxp_in(0) 	                            => QSFP_RX1_P                      ,
  gt_rxn_in(3) 	                            => QSFP_RX4_N                      ,
  gt_rxn_in(2) 	                            => QSFP_RX3_N                      ,
  gt_rxn_in(1) 	                            => QSFP_RX2_N                      ,
  gt_rxn_in(0) 	                            => QSFP_RX1_N                      ,
  gt_txp_out(3)	                            => QSFP_TX4_P                      ,
  gt_txp_out(2) 	                        => QSFP_TX3_P                      ,
  gt_txp_out(1) 	                        => QSFP_TX2_P                      ,
  gt_txp_out(0) 	                        => QSFP_TX1_P                      ,
  gt_txn_out(3) 	                        => QSFP_TX4_N                      ,
  gt_txn_out(2) 	                        => QSFP_TX3_N                      ,
  gt_txn_out(1) 	                        => QSFP_TX2_N                      ,
  gt_txn_out(0) 	                        => QSFP_TX1_N                      ,
  gt_txusrclk2                              => gt_txusrclk2                     ,  
  gt_loopback_in                            => gt_loopback_in_rg                ,  
  gt_eyescanreset                           => "0000"                           ,--eyescanreset_o                                   ,  
  gt_eyescantrigger                         => "0000"                           ,  
  gt_rxcdrhold                              => "0000"                           ,--for SATA protocol only                   
  gt_rxpolarity                             => "0000"                           ,-- can be control (see the routing)        
  gt_rxrate                                 => x"000"                           ,--00 set in WIZARD                         
  gt_txdiffctrl                             => gt_txdiffctrl                     ,--gt_txdiffctrl,                         
  gt_txpolarity                             => gt_txpolarity                    ,                            
  gt_txinhibit                              => "0000"                           ,-- if at '1' block the rtasnmission and set pins to 0 and 1 (n & p)
  gt_txpippmen 							    => txpippmen_reg_resync		        ,--: IN STD_LOGIC_VECTOR(3 DOWNTO 0);
  gt_txpippmsel 						    => txpippmsel_reg                   ,--"1111"		,--: IN STD_LOGIC_VECTOR(3 DOWNTO 0);           
  gt_txpostcursor                           => gt_txpostcursor                  ,--gt_txpostcursor,--   
  gt_txprbsforceerr                         => gt_txprbsforceerr                           , -- to generate errors
  gt_txprecursor                            => gt_txprecursor                   ,--gt_txprecursor, 
  -- gt_eyescandataerror                       => gt_eyescandataerror           , --        OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
  -- gt_ref_clk_out							=>                                  ,
  gt_rxrecclkout                            => gt_rxrecclkout                   , --        OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
  gt_powergoodout						    => gt_powergoodout					,
  gt_txbufstatus                            => gt_txbufstatus                   , --        OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
  gt_rxdfelpmreset                          => "0000"                           , --  IN STD_LOGIC_VECTOR(3 DOWNTO 0);
  gt_rxlpmen                                => gt_rxlpmen                       ,--gt_rxlpmen                               , --  IN STD_LOGIC_VECTOR(3 DOWNTO 0);
  gt_rxprbscntreset                         => gt_rxprbscntreset       		    , --  IN STD_LOGIC_VECTOR(3 DOWNTO 0);
  gt_rxprbserr                              => gt_rxprbserr                     , --        OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
  gt_rxprbssel                              => x"0000"                          , --  IN STD_LOGIC_VECTOR(15 DOWNTO 0);
  gt_rxresetdone                            => gt_rxresetdone                   , --        OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
  gt_txprbssel                              => gt_txprbssel                     , --  IN STD_LOGIC_VECTOR(15 DOWNTO 0);
  gt_txresetdone                            => gt_txresetdone                   , --        OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
  gt_rxbufstatus                            => gt_rxbufstatus                   , --        OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
  gtwiz_reset_tx_datapath                   => '0'                              , --  IN STD_LOGIC;
  gtwiz_reset_rx_datapath                   => '0'                              , --  IN STD_LOGIC;
  gt_drpclk                                 => DRP_clock                        , --  IN STD_LOGIC;
  gt0_drpdo                                 => gt0_ch_drp_do                    , --        OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
  gt0_drprdy                                => gt0_ch_drp_rdy                   , --        OUT STD_LOGIC;
  gt0_drpen                                 => gt0_ch_drp_en                    , --  IN STD_LOGIC;
  gt0_drpwe                                 => gt0_ch_drp_we                    , --  IN STD_LOGIC;
  gt0_drpaddr                               => gtx_ch_drp_addr                  , --  IN STD_LOGIC_VECTOR(9 DOWNTO 0);
  gt0_drpdi                                 => gtx_ch_drp_di                    , --  IN STD_LOGIC_VECTOR(15 DOWNTO 0);
  gt1_drpdo                                 => gt1_ch_drp_do                    , --        OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
  gt1_drprdy                                => gt1_ch_drp_rdy                   , --        OUT STD_LOGIC;
  gt1_drpen                                 => gt1_ch_drp_en                    , --  IN STD_LOGIC;
  gt1_drpwe                                 => gt1_ch_drp_we                    , --  IN STD_LOGIC;
  gt1_drpaddr                               => gtx_ch_drp_addr                  , --  IN STD_LOGIC_VECTOR(9 DOWNTO 0);
  gt1_drpdi                                 => gtx_ch_drp_di                    , --  IN STD_LOGIC_VECTOR(15 DOWNTO 0);
  gt2_drpdo                                 => gt2_ch_drp_do                    , --        OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
  gt2_drprdy                                => gt2_ch_drp_rdy                   , --        OUT STD_LOGIC;
  gt2_drpen                                 => gt2_ch_drp_en                    , --  IN STD_LOGIC;
  gt2_drpwe                                 => gt2_ch_drp_we                    , --  IN STD_LOGIC;
  gt2_drpaddr                               => gtx_ch_drp_addr                  , --  IN STD_LOGIC_VECTOR(9 DOWNTO 0);
  gt2_drpdi                                 => gtx_ch_drp_di                    , --  IN STD_LOGIC_VECTOR(15 DOWNTO 0);
  gt3_drpdo                                 => gt3_ch_drp_do                    , --        OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
  gt3_drprdy                                => gt3_ch_drp_rdy                   , --        OUT STD_LOGIC;
  gt3_drpen                                 => gt3_ch_drp_en                    , --  IN STD_LOGIC;
  gt3_drpwe                                 => gt3_ch_drp_we                    , --  IN STD_LOGIC;
  gt3_drpaddr                               => gtx_ch_drp_addr                  , --  IN STD_LOGIC_VECTOR(9 DOWNTO 0);
  gt3_drpdi                                 => gtx_ch_drp_di                    , --  IN STD_LOGIC_VECTOR(15 DOWNTO 0);
  s_axi_aclk                                => init_clk                         , --  IN STD_LOGIC;
  s_axi_sreset                              => Axi_sys_reset                    , --  IN STD_LOGIC;
  pm_tick		                            => s_axi_pm_tick                    , --  IN STD_LOGIC;
  s_axi_awaddr                              => s_axi_awaddr                     , --  IN STD_LOGIC_VECTOR(31 DOWNTO 0);
  s_axi_awvalid                             => s_axi_awvalid                    , --  IN STD_LOGIC;
  s_axi_awready                             => s_axi_awready                    , --        OUT STD_LOGIC;
  s_axi_wdata                               => s_axi_wdata                      , --  IN STD_LOGIC_VECTOR(31 DOWNTO 0);
  s_axi_wstrb                               => s_axi_wstrb                      , --  IN STD_LOGIC_VECTOR(3 DOWNTO 0);
  s_axi_wvalid                              => s_axi_wvalid                     , --  IN STD_LOGIC;
  s_axi_wready                              => s_axi_wready                     , --        OUT STD_LOGIC;
  s_axi_bresp                               => s_axi_bresp                      , --        OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
  s_axi_bvalid                              => s_axi_bvalid                     , --        OUT STD_LOGIC;
  s_axi_bready                              => s_axi_bready                     , --  IN STD_LOGIC;
  s_axi_araddr                              => s_axi_araddr                     , --  IN STD_LOGIC_VECTOR(31 DOWNTO 0);
  s_axi_arvalid                             => s_axi_arvalid                    , --  IN STD_LOGIC;
  s_axi_arready                             => s_axi_arready                    , --        OUT STD_LOGIC;
  s_axi_rdata                               => s_axi_rdata                      , --        OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
  s_axi_rresp                               => s_axi_rresp                      , --        OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
  s_axi_rvalid                              => s_axi_rvalid                     , --        OUT STD_LOGIC;
  s_axi_rready                              => s_axi_rready                     , --  IN STD_LOGIC;
  --user_reg0									=>								,
  sys_reset                                 => Axi_sys_reset                    , --  IN STD_LOGIC;
  gt_ref_clk_p                   			=> GT_ref_clock_p						            ,--: IN STD_LOGIC;
  gt_ref_clk_n                   			=> GT_ref_clock_n						            ,--: IN STD_LOGIC;
  init_clk                                  => init_clk     	                , --  IN STD_LOGIC;
  common0_drpaddr                			=> x"0000"							,--: IN STD_LOGIC_VECTOR(15 DOWNTO 0);
  common0_drpdi                  			=> x"0000"							,--: IN STD_LOGIC_VECTOR(15 DOWNTO 0);
  common0_drpwe                  			=> '0'								,--: IN STD_LOGIC;
  common0_drpen                  			=> '0' 								,--: IN STD_LOGIC;
  -- common0_drprdy                 			=> 								,--: OUT STD_LOGIC;
  -- common0_drpdo                  			=>								,--: OUT STD_LOGIC_VECTOR(15 DOWNTO 0);  
  rx_dataout0                               => rx_lbus_cell(0).data_bus         , --        OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
  rx_dataout1                               => rx_lbus_cell(1).data_bus         , --        OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
  rx_dataout2                               => rx_lbus_cell(2).data_bus         , --        OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
  rx_dataout3                               => rx_lbus_cell(3).data_bus         , --        OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
  rx_enaout0                                => rx_lbus_cell(0).data_ena         , --        OUT STD_LOGIC;
  rx_enaout1                                => rx_lbus_cell(1).data_ena         , --        OUT STD_LOGIC;
  rx_enaout2                                => rx_lbus_cell(2).data_ena         , --        OUT STD_LOGIC;
  rx_enaout3                                => rx_lbus_cell(3).data_ena         , --        OUT STD_LOGIC;
  rx_eopout0                                => rx_lbus_cell(0).eopckt           , --        OUT STD_LOGIC;
  rx_eopout1                                => rx_lbus_cell(1).eopckt           , --        OUT STD_LOGIC;
  rx_eopout2                                => rx_lbus_cell(2).eopckt           , --        OUT STD_LOGIC;
  rx_eopout3                                => rx_lbus_cell(3).eopckt           , --        OUT STD_LOGIC;
  rx_errout0                                => rx_lbus_cell(0).err_pckt         , --        OUT STD_LOGIC;
  rx_errout1                                => rx_lbus_cell(1).err_pckt         , --        OUT STD_LOGIC;
  rx_errout2                                => rx_lbus_cell(2).err_pckt         , --        OUT STD_LOGIC;
  rx_errout3                                => rx_lbus_cell(3).err_pckt         , --        OUT STD_LOGIC;
  rx_mtyout0                                => rx_lbus_cell(0).empty_pckt       , --        OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
  rx_mtyout1                                => rx_lbus_cell(1).empty_pckt       , --        OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
  rx_mtyout2                                => rx_lbus_cell(2).empty_pckt       , --        OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
  rx_mtyout3                                => rx_lbus_cell(3).empty_pckt       , --        OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
  rx_sopout0                                => rx_lbus_cell(0).sopckt           , --        OUT STD_LOGIC;
  rx_sopout1                                => rx_lbus_cell(1).sopckt           , --        OUT STD_LOGIC;
  rx_sopout2                                => rx_lbus_cell(2).sopckt           , --        OUT STD_LOGIC;
  rx_sopout3                                => rx_lbus_cell(3).sopckt           , --        OUT STD_LOGIC;

  gt_rxusrclk2                              => gt_rxusrclk2                     , --        OUT STD_LOGIC;
  ctl_tx_send_idle                          => '0' ,
  ctl_tx_send_rfi                           => '0' ,
  ctl_tx_send_lfi                           => '0' ,
  stat_rx_aligned                           => stat_rx_aligned                  , --        OUT STD_LOGIC;
  stat_rx_aligned_err                       => stat_rx_aligned_err              , --        OUT STD_LOGIC;
  stat_rx_bad_code                          => stat_rx_bad_code                 , --        OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
  stat_rx_bad_fcs                           => stat_rx_bad_fcs                  , --        OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
  stat_rx_bad_preamble                      => stat_rx_bad_preamble             , --        OUT STD_LOGIC;
  stat_rx_bad_sfd                           => stat_rx_bad_sfd                  , --        OUT STD_LOGIC;
  stat_rx_bip_err_0                         => stat_rx_bip_err(0)               , --        OUT STD_LOGIC;
  stat_rx_bip_err_1                         => stat_rx_bip_err(1)               , --        OUT STD_LOGIC;
  stat_rx_bip_err_2                         => stat_rx_bip_err(2)               , --        OUT STD_LOGIC;
  stat_rx_bip_err_3                         => stat_rx_bip_err(3)               , --        OUT STD_LOGIC;
  stat_rx_block_lock                        => stat_rx_block_lock               , --        OUT STD_LOGIC_VECTOR(19 DOWNTO 0);
  -- stat_rx_broadcast              : OUT STD_LOGIC;
  stat_rx_fragment                          => stat_rx_fragment                 , --        OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
   stat_rx_framing_err_0          			=> rx_frame_l0_err					,--: OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
   stat_rx_framing_err_1          			=> rx_frame_l1_err					,--:  OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
   stat_rx_framing_err_2          			=> rx_frame_l2_err					,--: OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
   stat_rx_framing_err_3          			=> rx_frame_l3_err					,--: OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
   stat_rx_framing_err_valid_0          	=> rx_frame_l0_err_val				,--: OUT STD_LOGIC;
   stat_rx_framing_err_valid_1          	=> rx_frame_l1_err_val				,--: OUT STD_LOGIC;
   stat_rx_framing_err_valid_2          	=> rx_frame_l2_err_val				,--: OUT STD_LOGIC;
   stat_rx_framing_err_valid_3           	=> rx_frame_l3_err_val				,--: OUT STD_LOGIC;
   stat_rx_internal_local_fault              => stat_rx_internal_local_fault    , --        OUT STD_LOGIC;
  -- stat_rx_jabber                 : OUT STD_LOGIC;
   stat_rx_local_fault                       => stat_rx_local_fault             , --        OUT STD_LOGIC;
   -- stat_rx_mf_err                 : OUT STD_LOGIC_VECTOR(19 DOWNTO 0);
  -- stat_rx_mf_len_err             : OUT STD_LOGIC_VECTOR(19 DOWNTO 0);
  -- stat_rx_mf_repeat_err          : OUT STD_LOGIC_VECTOR(19 DOWNTO 0);
  stat_rx_misaligned                        => stat_rx_misaligned               , --        OUT STD_LOGIC;
  -- stat_rx_multicast              : OUT STD_LOGIC;
  -- stat_rx_oversize               : OUT STD_LOGIC;
  stat_rx_packet_1024_1518_bytes            => stat_rx_packet(5)                , --        OUT STD_LOGIC;
  stat_rx_packet_128_255_bytes              => stat_rx_packet(2)                , --        OUT STD_LOGIC;
  stat_rx_packet_1519_1522_bytes            => stat_rx_packet(6)                , --        OUT STD_LOGIC;
  stat_rx_packet_1523_1548_bytes            => stat_rx_packet(7)                , --        OUT STD_LOGIC;
  stat_rx_packet_1549_2047_bytes            => stat_rx_packet(8)                , --        OUT STD_LOGIC;
  stat_rx_packet_2048_4095_bytes            => stat_rx_packet(9)                , --        OUT STD_LOGIC;
  stat_rx_packet_256_511_bytes              => stat_rx_packet(3)                , --        OUT STD_LOGIC;
  stat_rx_packet_4096_8191_bytes            => stat_rx_packet(10)               , --        OUT STD_LOGIC;
  stat_rx_packet_512_1023_bytes             => stat_rx_packet(4)                , --        OUT STD_LOGIC;
  stat_rx_packet_64_bytes                   => stat_rx_packet(0)                , --        OUT STD_LOGIC;
  stat_rx_packet_65_127_bytes               => stat_rx_packet(1)                , --        OUT STD_LOGIC;
  stat_rx_packet_8192_9215_bytes            => stat_rx_packet(11)               , --        OUT STD_LOGIC;
      -- stat_rx_packet_large           : OUT STD_LOGIC;
    stat_rx_packet_small                      => stat_rx_packet_small           , --        OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
  stat_rx_pause        						=> stat_rx_pause        			, --		OUT STD_LOGIC;
  stat_rx_pause_quanta0						=> stat_rx_pause_quanta0			, --		OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
  stat_rx_pause_quanta1						=> stat_rx_pause_quanta1			, --		OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
  stat_rx_pause_quanta2						=> stat_rx_pause_quanta2			, --		OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
  stat_rx_pause_quanta3						=> stat_rx_pause_quanta3			, --		OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
  stat_rx_pause_quanta4						=> stat_rx_pause_quanta4			, --		OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
  stat_rx_pause_quanta5						=> stat_rx_pause_quanta5			, --		OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
  stat_rx_pause_quanta6						=> stat_rx_pause_quanta6			, --		OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
  stat_rx_pause_quanta7						=> stat_rx_pause_quanta7			, --		OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
  stat_rx_pause_quanta8						=> stat_rx_pause_quanta8			, --		OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
  stat_rx_pause_req    						=> stat_rx_pause_req    			, --		OUT STD_LOGIC_VECTOR(8 DOWNTO 0);
  stat_rx_pause_valid  						=> stat_rx_pause_valid  			, --		OUT STD_LOGIC_VECTOR(8 DOWNTO 0);
  stat_rx_user_pause   						=> stat_rx_user_pause   			, --		OUT STD_LOGIC;
  core_rx_reset								=> '0'                              ,
  rx_clk                                    => rx_clk                           , --  		IN STD_LOGIC;
  stat_rx_received_local_fault              => stat_rx_received_local_fault     , --        OUT STD_LOGIC;
  -- stat_rx_remote_fault           			=>								,--: OUT STD_LOGIC;
  stat_rx_status                            => stat_rx_status                   , --        OUT STD_LOGIC;
  -- stat_rx_stomped_fcs            			=>								,--: OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
  stat_rx_synced                            => stat_rx_synced                   , --        OUT STD_LOGIC_VECTOR(19 DOWNTO 0);
  stat_rx_synced_err                        => stat_rx_synced_err               , --        OUT STD_LOGIC_VECTOR(19 DOWNTO 0);
  -- stat_rx_test_pattern_mismatch  			=>								,--: OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
  -- stat_rx_toolong                			=>								,--: OUT STD_LOGIC;
  stat_rx_total_bytes                       => stat_rx_total_bytes              , --        OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
  stat_rx_total_good_packets                => inc_rx_total_good_packets_ena    , --        OUT STD_LOGIC;
  -- stat_rx_total_packets          			=>								,--: OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
  -- stat_rx_truncated              			=>								,--: OUT STD_LOGIC;
  -- stat_rx_undersize              			=>								,--: OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
  -- stat_rx_unicast                			=>								,--: OUT STD_LOGIC;
  -- stat_rx_vlan                   			=>								,--: OUT STD_LOGIC;
  -- stat_rx_pcsl_demuxed           			=>								,--: OUT STD_LOGIC_VECTOR(19 DOWNTO 0);
  -- stat_rx_pcsl_number_0          			=>								,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
  -- stat_rx_pcsl_number_1          			=>								,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
  -- stat_rx_pcsl_number_10         			=>								,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
  -- stat_rx_pcsl_number_11         			=>								,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
  -- stat_rx_pcsl_number_12         			=>								,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
  -- stat_rx_pcsl_number_13         			=>								,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
  -- stat_rx_pcsl_number_14         			=>								,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
  -- stat_rx_pcsl_number_15         			=>								,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
  -- stat_rx_pcsl_number_16         			=>								,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
  -- stat_rx_pcsl_number_17         			=>								,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
  -- stat_rx_pcsl_number_18         			=>								,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
  -- stat_rx_pcsl_number_19         			=>								,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
  -- stat_rx_pcsl_number_2          			=>								,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
  -- stat_rx_pcsl_number_3          			=>								,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
  -- stat_rx_pcsl_number_4          			=>								,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
  -- stat_rx_pcsl_number_5          			=>								,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
  -- stat_rx_pcsl_number_6          			=>								,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
  -- stat_rx_pcsl_number_7          			=>								,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
  -- stat_rx_pcsl_number_8          			=>								,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
  -- stat_rx_pcsl_number_9          			=>								,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
  stat_tx_bad_fcs                		    => stat_tx_bad_fcs    				,--: OUT STD_LOGIC;
  stat_tx_broadcast              			=> stat_tx_broadcast  				,--: OUT STD_LOGIC;
  stat_tx_frame_error            			=> stat_tx_frame_error				,--: OUT STD_LOGIC;
  stat_tx_local_fault                       => stat_tx_local_fault          	, --        OUT STD_LOGIC;
  -- stat_tx_multicast              			=>								,--: OUT STD_LOGIC;
  -- stat_tx_packet_1024_1518_bytes 			=>								,--: OUT STD_LOGIC;
  -- stat_tx_packet_128_255_bytes   			=>								,--: OUT STD_LOGIC;
  -- stat_tx_packet_1519_1522_bytes 			=>								,--: OUT STD_LOGIC;
  -- stat_tx_packet_1523_1548_bytes 			=>								,--: OUT STD_LOGIC;
  -- stat_tx_packet_1549_2047_bytes 			=>								,--: OUT STD_LOGIC;
  -- stat_tx_packet_2048_4095_bytes 			=>								,--: OUT STD_LOGIC;
  -- stat_tx_packet_256_511_bytes   			=>								,--: OUT STD_LOGIC;
  -- stat_tx_packet_4096_8191_bytes 			=>								,--: OUT STD_LOGIC;
  -- stat_tx_packet_512_1023_bytes  			=>								,--: OUT STD_LOGIC;
  -- stat_tx_packet_64_bytes        			=>								,--: OUT STD_LOGIC;
  -- stat_tx_packet_65_127_bytes    			=>								,--: OUT STD_LOGIC;
  -- stat_tx_packet_8192_9215_bytes 			=>								,--: OUT STD_LOGIC;
  -- stat_tx_packet_large           			=>								,--: OUT STD_LOGIC;
  -- stat_tx_packet_small           			=>								,--: OUT STD_LOGIC;
  -- stat_tx_total_bytes            			=>								,--: OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
  -- stat_tx_total_good_bytes       			=>								,--: OUT STD_LOGIC_VECTOR(13 DOWNTO 0);
  -- stat_tx_total_good_packets     			=>								,--: OUT STD_LOGIC;
  -- stat_tx_total_packets          			=>								,--: OUT STD_LOGIC;
  -- stat_tx_unicast                			=>								,--: OUT STD_LOGIC;
  -- stat_tx_vlan                   			=>								,--: OUT STD_LOGIC;
  core_tx_reset                  			=>	'0'						    	,--: IN STD_LOGIC;
  --stat_tx_pause_valid            			=> 	
  --stat_tx_pause                  			=> 	
  --stat_tx_user_pause             			=> 	
  ctl_tx_pause_req               			=> ctl_tx_pause_req_resync			,
  ctl_tx_resend_pause       			    => ctl_tx_resend_pause_resync		,
  tx_ovfout                                 => tx_ovfout                    	, --        OUT STD_LOGIC;
  tx_rdyout                                 => tx_rdyout                    	, --        OUT STD_LOGIC;
  tx_unfout                                 => tx_unfout                    	, --        OUT STD_LOGIC;
  tx_datain0                                => tx_lbus(0).data_bus          	, --  IN STD_LOGIC_VECTOR(127 DOWNTO 0);
  tx_datain1                                => tx_lbus(1).data_bus          	, --  IN STD_LOGIC_VECTOR(127 DOWNTO 0);
  tx_datain2                                => tx_lbus(2).data_bus          	, --  IN STD_LOGIC_VECTOR(127 DOWNTO 0);
  tx_datain3                                => tx_lbus(3).data_bus          	, --  IN STD_LOGIC_VECTOR(127 DOWNTO 0);
  tx_enain0                                 => tx_lbus(0).data_ena          	, --  IN STD_LOGIC;
  tx_enain1                                 => tx_lbus(1).data_ena          	, --  IN STD_LOGIC;
  tx_enain2                                 => tx_lbus(2).data_ena          	, --  IN STD_LOGIC;
  tx_enain3                                 => tx_lbus(3).data_ena          	, --  IN STD_LOGIC;
  tx_eopin0                                 => tx_lbus(0).eopckt            	, --  IN STD_LOGIC;
  tx_eopin1                                 => tx_lbus(1).eopckt            	, --  IN STD_LOGIC;
  tx_eopin2                                 => tx_lbus(2).eopckt            	, --  IN STD_LOGIC;
  tx_eopin3                                 => tx_lbus(3).eopckt            	, --  IN STD_LOGIC;
  tx_errin0                                 => '0'                          	, --  IN STD_LOGIC;
  tx_errin1                                 => '0'                          	, --  IN STD_LOGIC;
  tx_errin2                                 => '0'                          	, --  IN STD_LOGIC;
  tx_errin3                                 => '0'                          	, --  IN STD_LOGIC;
  tx_mtyin0                                 => tx_lbus(0).empty_pckt        	, --  IN STD_LOGIC_VECTOR(3 DOWNTO 0);
  tx_mtyin1                                 => tx_lbus(1).empty_pckt        	, --  IN STD_LOGIC_VECTOR(3 DOWNTO 0);
  tx_mtyin2                                 => tx_lbus(2).empty_pckt        	, --  IN STD_LOGIC_VECTOR(3 DOWNTO 0);
  tx_mtyin3                                 => tx_lbus(3).empty_pckt        	, --  IN STD_LOGIC_VECTOR(3 DOWNTO 0);
  tx_sopin0                                 => tx_lbus(0).sopckt            	, --  IN STD_LOGIC;
  tx_sopin1                                 => tx_lbus(1).sopckt            	, --  IN STD_LOGIC;
  tx_sopin2                                 => tx_lbus(2).sopckt            	, --  IN STD_LOGIC;
  tx_sopin3                                 => tx_lbus(3).sopckt            	, --  IN STD_LOGIC;
  tx_preamblein                             => x"55555555555555"            	,
  -- usr_tx_reset								=> 	
  core_drp_reset							=> '0'                          	,
  drp_clk                                   => DRP_clock                    	, --  IN STD_LOGIC;
  drp_addr                                  => drp_addr                     	, --  IN STD_LOGIC_VECTOR(9 DOWNTO 0);
  drp_di                                    => drp_di                       	, --  IN STD_LOGIC_VECTOR(15 DOWNTO 0);
  drp_en                                    => drp_en_cell                  	, --  IN STD_LOGIC;
  drp_do                                    => drp_do                       	, --        OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
  drp_rdy                                   => drp_rdy                      	, --        OUT STD_LOGIC;
  drp_we                                    => drp_we(1)                    	,   --  IN STD_LOGIC
  
  stat_rx_rsfec_am_lock0                    => stat_rx_rsfec_am_lock0              ,--: OUT STD_LOGIC;
  stat_rx_rsfec_am_lock1                    => stat_rx_rsfec_am_lock1              ,--: OUT STD_LOGIC;
  stat_rx_rsfec_am_lock2                    => stat_rx_rsfec_am_lock2              ,--: OUT STD_LOGIC;
  stat_rx_rsfec_am_lock3                    => stat_rx_rsfec_am_lock3              ,--: OUT STD_LOGIC;
  stat_rx_rsfec_corrected_cw_inc            => stat_rx_rsfec_corrected_cw_inc      ,--: OUT STD_LOGIC;
  stat_rx_rsfec_cw_inc                      => stat_rx_rsfec_cw_inc                ,--: OUT STD_LOGIC;
  stat_rx_rsfec_err_count0_inc              => stat_rx_rsfec_err_count0_inc        ,--: OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
  stat_rx_rsfec_err_count1_inc              => stat_rx_rsfec_err_count1_inc        ,--: OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
  stat_rx_rsfec_err_count2_inc              => stat_rx_rsfec_err_count2_inc        ,--: OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
  stat_rx_rsfec_err_count3_inc              => stat_rx_rsfec_err_count3_inc        ,--: OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
  stat_rx_rsfec_hi_ser                      => stat_rx_rsfec_hi_ser                ,--: OUT STD_LOGIC;
  stat_rx_rsfec_lane_alignment_status       => stat_rx_rsfec_lane_alignment_status ,--: OUT STD_LOGIC;
  stat_rx_rsfec_lane_fill_0                 => stat_rx_rsfec_lane_fill_0           ,--: OUT STD_LOGIC_VECTOR(13 DOWNTO 0);
  stat_rx_rsfec_lane_fill_1                 => stat_rx_rsfec_lane_fill_1           ,--: OUT STD_LOGIC_VECTOR(13 DOWNTO 0);
  stat_rx_rsfec_lane_fill_2                 => stat_rx_rsfec_lane_fill_2           ,--: OUT STD_LOGIC_VECTOR(13 DOWNTO 0);
  stat_rx_rsfec_lane_fill_3                 => stat_rx_rsfec_lane_fill_3           ,--: OUT STD_LOGIC_VECTOR(13 DOWNTO 0);
  stat_rx_rsfec_lane_mapping                => stat_rx_rsfec_lane_mapping          ,--: OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
  stat_rx_rsfec_uncorrected_cw_inc          => stat_rx_rsfec_uncorrected_cw_inc     --: OUT STD_LOGIC;
 );
 
process(counter_reset_resync_gt_rxusrclk2,gt_rxusrclk2)
begin
    if counter_reset_resync_gt_rxusrclk2 = '1' then
        RX_fec_corrected_counter    <= (others => '0');
        RX_fec_uncorrected_counter  <= (others => '0');
    elsif rising_edge(gt_rxusrclk2) then 
        if stat_rx_rsfec_corrected_cw_inc = '1' then
            RX_fec_corrected_counter <= RX_fec_corrected_counter + '1'; 
        end if;
        
        if stat_rx_rsfec_uncorrected_cw_inc = '1' then
            RX_fec_uncorrected_counter <= RX_fec_uncorrected_counter + '1'; 
        end if;
    end if;
end process; 
 
status_out(0)		<= tx_lbus(0).sopckt; 
status_out(1)		<= rx_lbus_cell(0).sopckt; 
 
rx_lbus				<= rx_lbus_cell; 
mac_rx_clk 			<= gt_rxusrclk2;
rx_clk      		<= gt_rxusrclk2;
 
tx_lbus_ready		<= tx_rdyout;  
mac_tx_clk 			<= gt_txusrclk2;


user_reset_n    	<= not(Serdes_Reset_p); 

drp_en_cell			<= '1' when drp_we(1) = '1' or drp_en(1) = '1' else '0';
 
powergood			<= '1' when (gt_powergoodout  = "1111" ) else  '0';
   
 
--***************************************************************************************************
--- AXI bus is used to acces MAC registers (clock domain PCIe)
axi_inst_i1:entity work.AXI_itf  
    Port map( 										   
		resetn               => user_reset_n      	      , 
		Usr_clock            => usr_clk        	          , 
		Usr_add              => AXI_rq_add                , 
		Usr_strb             => AXI_rq_BE        		  , 
		Usr_dti              => AXI_rq_dti                , 
		Usr_dto              => AXI_rtn_dto               , 
		Usr_RdREQ            => AXI_rq_rd                 , 
		Usr_WrREQ            => AXI_rq_wr                 , 
		Usr_AXI_tick         => AXI_tick                  , 
												   
		Axi_status           => AXI_status                , 
		Axi_done             => AXI_done                  , 
												   
		s_axi_clock          => init_clk                  , 
		s_axi_pm_tick        => s_axi_pm_tick             , 
		s_axi_awaddr         => s_axi_awaddr              , 
		s_axi_awvalid        => s_axi_awvalid             , 
		s_axi_awready        => s_axi_awready             , 
		s_axi_wdata          => s_axi_wdata               , 
		s_axi_wstrb          => s_axi_wstrb               , 
--		s_axi_size           =>                           , 
		s_axi_wvalid         => s_axi_wvalid              , 
		s_axi_wready         => s_axi_wready              , 
		s_axi_bresp          => s_axi_bresp               , 
		s_axi_bvalid         => s_axi_bvalid              , 
		s_axi_bready         => s_axi_bready              , 
		s_axi_araddr         => s_axi_araddr              , 
		s_axi_arvalid        => s_axi_arvalid             , 
		s_axi_arready        => s_axi_arready             , 
		s_axi_rdata          => s_axi_rdata               , 
		s_axi_rresp          => s_axi_rresp               , 
		s_axi_rvalid         => s_axi_rvalid              , 
		s_axi_rready     	 => s_axi_rready 		 
           );                                           

 


--**************************************************************************************************
--		PCIe write data

-- register which should be reset 
process(SERDES_resetn,usr_clk)
  begin
      if SERDES_resetn = '0' then
        gt_reset              		<= '1';
        sys_reset             		<= '1';  
     	gt_loopback_in_rg     		<= (others => '0');
     	txpippmen_reg	     		<= (others => '0');
     	txpippmsel_reg	     		<= (others => '1');
      	tx_diff_ctrl				<= "11000";
		txpostcursor				<= "10100";
		txprecursor					<= "00000";
		tx_polarity					<= '0';
		gt_rxprbscntreset_async		<= '0';
		rx_rxlpmen					<= '1';
		gt_txprbssel_async			<= "0000";
		gt_rxprbssel_async			<= "0000";
		gt_txprbsforceerr_async		<= '0';
		ctl_tx_pause_req        	<= (others => '0');
		ctl_tx_resend_pause     	<= '0';
      elsif rising_edge(usr_clk) then
--      	gt_reset              		<= '0';
--      	sys_reset             		<= '0';
      	
        if usr_wen = '1'  then
				if usr_func_wr(MAC100G_SERDES_reset + addr_offset_100G_eth) = '1' then
					gt_reset               	 	<= usr_data_wr(0);
					sys_reset               	<= usr_data_wr(1);  
					counter_reset				<= usr_data_wr(31);
				end if;        

				if usr_func_wr(MAC100G_SERDES_voltage + addr_offset_100G_eth) = '1' then
					tx_diff_ctrl				<= usr_data_wr(4 downto 0);
					txpostcursor				<= usr_data_wr(12 downto 8);
					txprecursor					<= usr_data_wr(20 downto 16);
					tx_polarity					<= usr_data_wr(24);
					rx_rxlpmen					<= usr_data_wr(29);
				end if;
								
				if usr_func_wr(MAC100G_SERDES_loopback + addr_offset_100G_eth) = '1' then
					gt_loopback_in_rg        	<= usr_data_wr(11 downto 0);
					txpippmen_reg				<= usr_data_wr(19 downto 16);
					txpippmsel_reg				<= usr_data_wr(23 downto 20); 
				end if;
				
				if usr_func_wr(MAC100G_SERDES_PRBS_TEST + addr_offset_100G_eth) = '1' then
					gt_txprbssel_async        	<= usr_data_wr(3 downto 0);
					gt_rxprbssel_async			<= usr_data_wr(7 downto 4);
					gt_rxprbscntreset_async		<= usr_data_wr(28);
					gt_txprbsforceerr_async		<= usr_data_wr(29);
					 
				end if;
							
				if usr_func_wr(MAC100G_SERDES_TX_pause_frame_req + addr_offset_100G_eth) = '1' then
					ctl_tx_pause_req        <= usr_data_wr(8 downto 0);
					ctl_tx_resend_pause     <= usr_data_wr(16);
				end if; 	
            
			end if;
      end if;
end process;    
  
-- register which are not reset    
process(usr_clk)
  begin
      if rising_edge(usr_clk) then
          gtx_ch_drp_rd			<= '0';
          gtx_ch_drp_we			<= '0';
		  AXI_rq_wr            	<= '0';
          AXI_rq_rd             <= '0';
          AXI_tick	       		<= '0';   
              
       	 if usr_wen = '1'  then

				if usr_func_wr(MAC100G_SERDES_DRP_cmd			 	+ addr_offset_100G_eth) = '1' then
					drp_en(0)              	<= usr_data_wr(31);
					drp_we(0)              	<= usr_data_wr(30); 
					drp_addr               	<= usr_data_wr(25 downto 16);
					drp_di                 	<= usr_data_wr(15 downto 0);
				end if;    

				if usr_func_wr(MAC100G_SERDES_ChDRP_cmd 			+ addr_offset_100G_eth) = '1' then
					gtx_ch_drp_en      		<= usr_data_wr(31 downto 28);
					gtx_ch_drp_we    		<= usr_data_wr(27);
					gtx_ch_drp_rd    		<= usr_data_wr(26);
					gtx_ch_drp_addr    		<= usr_data_wr(25 downto 16);
					gtx_ch_drp_di      		<= usr_data_wr(15 downto 0);
				end if;    

				if usr_func_wr(MAC100G_SERDES_AXI_Add 				+ addr_offset_100G_eth) = '1' then
					AXI_rq_add              <= usr_data_wr(31 downto 0);
				end if; 

				if usr_func_wr(MAC100G_SERDES_AXI_Dti 				+ addr_offset_100G_eth) = '1' then
					AXI_rq_dti              <= usr_data_wr(31 downto 0);
				end if;      

				if usr_func_wr(MAC100G_SERDES_AXI_ctrl 				+ addr_offset_100G_eth) = '1' then
					AXI_rq_wr               <= usr_data_wr(0);
					AXI_rq_rd               <= usr_data_wr(1);
					AXI_tick 	            <= usr_data_wr(8);
					AXI_rq_BE               <= usr_data_wr(19 downto 16);
				end if;                  
					
			end if;
      end if;
end process;    

--******************************************************************************************************
--		PCIe read data
                    
process(usr_clk)
begin
	if rising_edge(usr_clk) then		
	   data_in_rg                                  	         <= (others => '0');
		 
--			if    usr_func_rd(100) = '1' then
--				data_in_rg(31 downto 0)                      <= inc_tx_total_good_packets;
--			elsif usr_func_rd(101) = '1' then
--				data_in_rg(31 downto 0)                      <= stat_rx_total_bytes_cnt;	
			if usr_func_rd(MAC100G_SERDES_status 		+ addr_offset_100G_eth) = '1' then
				data_in_rg(0)                                <= stat_rx_aligned 	;
				data_in_rg(1)                                <= stat_rx_aligned_err;
				
				data_in_rg(4 downto 2)                       <= stat_rx_bad_code ;
				data_in_rg(8 downto 6)                       <= stat_rx_bad_fcs;
				data_in_rg(9)                              	 <= powergood ;
				data_in_rg(10)                               <= stat_rx_bad_preamble ;
				data_in_rg(11)                               <= stat_rx_bad_sfd 		;
				
				data_in_rg(12)                               <= stat_rx_internal_local_fault;
				data_in_rg(13)                               <= stat_rx_received_local_fault;
				data_in_rg(14)                               <= stat_tx_local_fault; 
				
				data_in_rg(16)			                     <= stat_rx_local_fault   ;
				data_in_rg(17)			                     <= stat_rx_misaligned    ;
				data_in_rg(18)			                     <= stat_rx_status        ; 
	
				data_in_rg(23 downto 20)                     <= gt_rxresetdone;
				data_in_rg(27 downto 24)                     <= gt_txresetdone;
				  
				  
				
			elsif usr_func_rd(MAC100G_SERDES_statusa 	+ addr_offset_100G_eth) = '1' then
				data_in_rg(0)                      			<= powergood ;       
				   
			elsif usr_func_rd(103) = '1' then
				data_in_rg(19 downto 0)                      <= stat_rx_block_lock ;
				data_in_rg(22 downto 20)                     <= stat_rx_fragment 	;	 		
			elsif usr_func_rd(104) = '1' then
				data_in_rg(19 downto 0)                      <= stat_rx_synced ;
			elsif usr_func_rd(105) = '1' then
				data_in_rg(19 downto 0)                      <= stat_rx_synced_err ;		

			elsif usr_func_rd(116) = '1' then
				data_in_rg(11 downto 0)                      <= stat_rx_packet_size;
				data_in_rg(31 downto 12)                     <= stat_rx_packet_cnt ;

			elsif usr_func_rd(117) = '1' then
				data_in_rg(31 downto 0)	                     <= stat_rx_packet_small_cnt;
				
			elsif usr_func_rd(MAC100G_SERDES_reset 		+ addr_offset_100G_eth) = '1' then
				data_in_rg(0) 			                     <= gt_reset; 
				data_in_rg(1)                                <= sys_reset;  
				data_in_rg(31 downto 16)                     <= x"dead";

			elsif usr_func_rd(MAC100G_SERDES_loopback 	+ addr_offset_100G_eth) = '1' then
				data_in_rg(11 downto 0)				         <= gt_loopback_in_rg;	
				data_in_rg(19 downto 16)				     <= txpippmen_reg;	
				data_in_rg(23 downto 20)				     <= txpippmsel_reg;	
				

			elsif usr_func_rd(MAC100G_SERDES_voltage 	+ addr_offset_100G_eth) = '1' then
				data_in_rg(4 downto 0)				         <= tx_diff_ctrl;
				data_in_rg(12 downto 8)				         <= txpostcursor;
				data_in_rg(20 downto 16)			         <= txprecursor;
				data_in_rg(24)							     <= tx_polarity	;
				data_in_rg(29)							     <= rx_rxlpmen;
				
			elsif usr_func_rd(MAC100G_SERDES_PRBS_TEST 	+ addr_offset_100G_eth) = '1' then
				data_in_rg(3 downto 0)				         <= gt_txprbssel_async;
				
				data_in_rg(28)							     <= gt_rxprbscntreset_async;
 
			elsif usr_func_rd(MAC100G_SERDES_DRP_cmd 	+ addr_offset_100G_eth) = '1' then
				data_in_rg(25 downto 16)			         <= drp_addr;
				data_in_rg(15 downto 0)				         <= drp_di;

			elsif usr_func_rd(MAC100G_SERDES_DRP_dt 	+ addr_offset_100G_eth) = '1' then
				data_in_rg(16)							     <= drp_done;
				data_in_rg(15 downto 0)				         <= drp_data;

			elsif usr_func_rd(MAC100G_SERDES_ChDRP_cmd 	+ addr_offset_100G_eth) = '1' then
				data_in_rg(25 downto 16)                     <= gtx_ch_drp_addr;
				data_in_rg(15 downto 0)                      <= gtx_ch_drp_di;

			elsif usr_func_rd(MAC100G_SERDES_ChDRP_dt 	+ addr_offset_100G_eth) = '1' then
				data_in_rg(19 downto 16)                     <= gtx_ch_drp_done;
				data_in_rg(15 downto 0)                      <= gtx_ch_drp_do;

			elsif usr_func_rd(MAC100G_SERDES_AXI_Add 	+ addr_offset_100G_eth) = '1' then
				data_in_rg(31 downto 0)                      <= AXI_rq_add;

			elsif usr_func_rd(MAC100G_SERDES_AXI_Dti 	+ addr_offset_100G_eth) = '1' then
				data_in_rg(31 downto 0)                      <= AXI_rq_dti;

			elsif usr_func_rd(MAC100G_SERDES_AXI_Dto 	+ addr_offset_100G_eth) = '1' then
				data_in_rg(31 downto 0)                      <= AXI_rtn_dto;

			elsif usr_func_rd(MAC100G_SERDES_AXI_status + addr_offset_100G_eth) = '1' then
				data_in_rg(30 downto 0)                      <= AXI_status(30 downto 0);
				data_in_rg(31)                               <= AXI_done;

			elsif usr_func_rd(MAC100G_SERDES_TX_status 	+ addr_offset_100G_eth) = '1' then
				data_in_rg(0)					             <= tx_ovfout_mem ;
				data_in_rg(1)					             <= tx_rdyout;
				data_in_rg(2)					             <= tx_unfout_mem;
				data_in_rg(15 downto 8)					 	 <= gt_txbufstatus;
				
			elsif usr_func_rd(MAC100G_SERDES_RX_error 	+ addr_offset_100G_eth) = '1' then
				data_in_rg(15 downto 0)			             <= rx_crc_error;
				data_in_rg(31 downto 16)			         <= stat_rx_pause_counter;
								
			elsif usr_func_rd(MAC100G_SERDES_TX_error_1 + addr_offset_100G_eth) = '1' then
				data_in_rg(15 downto 0)			             <= stat_tx_local_fault_cnt;
				data_in_rg(31 downto 16)			         <= stat_tx_bad_fcs_cnt;
								
			elsif usr_func_rd(MAC100G_SERDES_TX_error_2 + addr_offset_100G_eth) = '1' then
				data_in_rg(15 downto 0)			             <= stat_tx_broadcast_cnt;
				data_in_rg(31 downto 16)			         <= stat_tx_frame_error_cnt;

			elsif usr_func_rd(MAC100G_rx_frame_l0_error + addr_offset_100G_eth) = '1' then 
				data_in_rg						             <= rx_frame_l0_err_cnt;
			elsif usr_func_rd(MAC100G_rx_frame_l1_error + addr_offset_100G_eth) = '1' then 
				data_in_rg						             <= rx_frame_l1_err_cnt;
			elsif usr_func_rd(MAC100G_rx_frame_l2_error + addr_offset_100G_eth) = '1' then 
				data_in_rg						             <= rx_frame_l2_err_cnt;
			elsif usr_func_rd(MAC100G_rx_frame_l3_error + addr_offset_100G_eth) = '1' then 
				data_in_rg						             <= rx_frame_l3_err_cnt;
				
			elsif usr_func_rd(MAC100G_rx_l0_bip_error 	+ addr_offset_100G_eth) = '1' then
				data_in_rg 				                     <= stat_rx_bip_err_cnt(0); 
			elsif usr_func_rd(MAC100G_rx_l1_bip_error 	+ addr_offset_100G_eth) = '1' then
				data_in_rg									 <= stat_rx_bip_err_cnt(1);
			elsif usr_func_rd(MAC100G_rx_l2_bip_error 	+ addr_offset_100G_eth) = '1' then
				data_in_rg 				                   <= stat_rx_bip_err_cnt(2); 
			elsif usr_func_rd(MAC100G_rx_l3_bip_error 	+ addr_offset_100G_eth) = '1' then
				data_in_rg 				                     <= stat_rx_bip_err_cnt(3);
			elsif usr_func_rd(MAC100G_test 				+ addr_offset_100G_eth) = '1' then
				data_in_rg 				                     <= x"7878787812121212";
                
			elsif usr_func_rd(MAC100G_fec_status    	+ addr_offset_100G_eth) = '1' then
				data_in_rg(0)				                 <= stat_rx_rsfec_am_lock0;
				data_in_rg(1)				                 <= stat_rx_rsfec_am_lock1;
				data_in_rg(2)				                 <= stat_rx_rsfec_am_lock2;
				data_in_rg(3)				                 <= stat_rx_rsfec_am_lock3;
                
				data_in_rg(4)				                 <= stat_rx_rsfec_hi_ser;
                
				data_in_rg(7)				                 <= stat_rx_rsfec_lane_alignment_status;
                
				data_in_rg(23 downto 16)				     <= stat_rx_rsfec_lane_mapping; 
                
			elsif usr_func_rd(MAC100G_fec_corrected_counter + addr_offset_100G_eth) = '1' then
				data_in_rg 				                     <= RX_fec_corrected_counter;
                
			elsif usr_func_rd(MAC100G_fec_uncorrected_counter + addr_offset_100G_eth) = '1' then
				data_in_rg 				                     <= RX_fec_uncorrected_counter;
                
               				
			end if;
 
	end if;
end process; 

usr_data_rd           <=  data_in_rg;     


reset_resync_i125:entity work.resetp_resync 
port map(
	aresetp				=>	counter_reset,
	clock				=>	gt_rxusrclk2,
	Resetp_sync			=>	counter_reset_resync_gt_rxusrclk2 
	); 
 
reset_resync_i5:entity work.resetp_resync 
port map(
	aresetp				=>	counter_reset,
	clock				=>	rx_clk,
	Resetp_sync			=>	counter_reset_resync 
	); 
 
reset_resync_i6:entity work.resetp_resync 
port map(
	aresetp				=>	counter_reset,
	clock				=>	gt_txusrclk2,
	Resetp_sync			=>	counter_reset_sync_txclk 
	); 
 
process(counter_reset_resync,rx_clk) 
begin
	if counter_reset_resync = '1' then
		stat_rx_packet_size	<= (others => '0');
		stat_rx_packet_cnt	<= (others => '0');
	elsif rising_edge(rx_clk) then
		if stat_rx_packet /= x"000" then
			stat_rx_packet_size <= stat_rx_packet;
			stat_rx_packet_cnt <= stat_rx_packet_cnt + '1';
		end if;
	end if;
end process;
		
process(counter_reset_resync,rx_clk) 
begin
	if counter_reset_resync = '1' then
		stat_rx_packet_small_cnt	<= (others => '0'); 
	elsif rising_edge(rx_clk) then
		if stat_rx_packet_small /= "000" then
			stat_rx_packet_small_cnt <= stat_rx_packet_small_cnt + '1';
		end if;		
		
	end if;
end process; 
  		   		   		 
process(counter_reset_resync,rx_clk) 
begin
	if counter_reset_resync = '1' then
		inc_tx_total_good_packets	<= (others => '0');
	elsif rising_edge(rx_clk) then
		if inc_rx_total_good_packets_ena = '1' then
			inc_tx_total_good_packets	<= inc_tx_total_good_packets + '1';
		end if;
	end if;
end process;

process(counter_reset_resync,rx_clk) 
begin
	if counter_reset_resync = '1' then
		stat_rx_total_bytes_cnt	<= (others => '0');
	elsif rising_edge(rx_clk) then		
		if stat_rx_total_bytes /= "0000000" then
			stat_rx_total_bytes_cnt	<= stat_rx_total_bytes_cnt + '1';
		end if;
 
	end if;
end process;
  
  
-- Statistic counter
process(counter_reset_resync,rx_clk)
begin
	if counter_reset_resync = '1' then
		rx_crc_error			<= (others => '0');
		stat_rx_pause_counter	<= (others => '0');
		rx_frame_l0_err_cnt		<= (others => '0');
		rx_frame_l1_err_cnt		<= (others => '0');
		rx_frame_l2_err_cnt		<= (others => '0');
		rx_frame_l3_err_cnt		<= (others => '0');
		
		stat_rx_bip_err_cnt(0)	<= (others => '0');
		stat_rx_bip_err_cnt(1)	<= (others => '0');
		stat_rx_bip_err_cnt(2)	<= (others => '0');
		stat_rx_bip_err_cnt(3)	<= (others => '0');
		
	elsif rising_edge(rx_clk) then
		if stat_rx_bad_fcs  /= "000" then
			rx_crc_error	<= rx_crc_error + '1';
		end if;
		
		if rx_frame_l0_err /= "00" and rx_frame_l0_err_val = '1' then
			rx_frame_l0_err_cnt <= rx_frame_l0_err_cnt + '1';
		end if;
		
		if rx_frame_l1_err /= "00" and rx_frame_l1_err_val = '1' then
			rx_frame_l1_err_cnt <= rx_frame_l1_err_cnt + '1';
		end if;
				
		if rx_frame_l2_err /= "00" and rx_frame_l2_err_val = '1' then
			rx_frame_l2_err_cnt <= rx_frame_l2_err_cnt + '1';
		end if;
				
		if rx_frame_l3_err /= "00" and rx_frame_l3_err_val = '1' then
			rx_frame_l3_err_cnt <= rx_frame_l3_err_cnt + '1';
		end if;
				
		if stat_rx_bip_err(0) = '1' then
			stat_rx_bip_err_cnt(0) <= stat_rx_bip_err_cnt(0) + '1';
		end if;
						
		if stat_rx_bip_err(1) = '1' then
			stat_rx_bip_err_cnt(1) <= stat_rx_bip_err_cnt(1) + '1';
		end if;
						
		if stat_rx_bip_err(2) = '1' then
			stat_rx_bip_err_cnt(2) <= stat_rx_bip_err_cnt(2) + '1';
		end if;
						
		if stat_rx_bip_err(3) = '1' then
			stat_rx_bip_err_cnt(3) <= stat_rx_bip_err_cnt(3) + '1';
		end if;
				
		if stat_rx_pause_req /= "000000000" then
			stat_rx_pause_counter <= stat_rx_pause_counter + '1';
		end if;
	end if;
end process; 

process(counter_reset_sync_txclk,gt_txusrclk2)
begin
	if counter_reset_sync_txclk = '1' then
		stat_tx_local_fault_cnt       <= (others => '0');
		stat_tx_bad_fcs_cnt           <= (others => '0');
		stat_tx_broadcast_cnt         <= (others => '0');
		stat_tx_frame_error_cnt       <= (others => '0');
		tx_ovfout_mem			      <= '0';
		tx_unfout_mem			      <= '0';
	elsif rising_edge(gt_txusrclk2) then
		if stat_tx_local_fault = '1'then
			stat_tx_local_fault_cnt	<= stat_tx_local_fault_cnt + '1';
		end if;
			
		if stat_tx_bad_fcs = '1'then
			stat_tx_bad_fcs_cnt		<= stat_tx_bad_fcs_cnt + '1';
		end if;
		
		if stat_tx_broadcast = '1'then
			stat_tx_broadcast_cnt	<= stat_tx_broadcast_cnt + '1';
		end if;
			
		if stat_tx_frame_error = '1'then
			stat_tx_frame_error_cnt	<= stat_tx_frame_error_cnt + '1';
		end if;
		 	
		if tx_ovfout = '1' then
			tx_ovfout_mem <= '1';
		end if;		
		
		if tx_unfout = '1' then
			tx_unfout_mem <= '1';
		end if;
		
	end if;
end process; 
 
end Behavioral;
