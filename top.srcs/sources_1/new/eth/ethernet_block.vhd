----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 21.04.2017 14:23:45
-- Design Name: 
-- Module Name: top - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

use work.interface.all;
use work.address_table.all;  
 

entity ethernet_block is
	generic ( MAC_card_higher_part			: std_logic_vector(47 downto 16) := x"080030F4"	;
			 addr_offset_100G_eth           : integer := 0 
	);
   Port ( 
        clock_low                           : in std_logic; -- low clock ~1MHz for ARP probe
        MAC_lower_part                      : in std_logic_vector(15 downto 0);
		TCP_Buf0_SND_NEW					: in std_logic_vector(31 downto 0);
		TCP_Buf0_SND_UNA					: out std_logic_vector(31 downto 0);
		TCP_Buf0_VALID_P					: out std_logic; 
		   
		TCP_Buf1_SND_NEW					: in std_logic_vector(31 downto 0);
		TCP_Buf1_SND_UNA					: out std_logic_vector(31 downto 0);
		TCP_Buf1_VALID_P					: out std_logic; 
		   
		UR_PL_TX_lbus_A	   					: in tx_usr_record;
		UR_PL_TX_lbus_B	   					: in tx_usr_record;
		UR_PL_TX_lbus_C	   					: in tx_usr_record;
		UR_PL_TX_lbus_D	   					: in tx_usr_record;
		UR_blk_ready			   			: in std_logic;
		UR_blk_len							: in std_logic_vector(15 downto 0):= (others => '0');
		UR_blk_CS				   			: in std_logic_vector(15 downto 0):= (others => '0');
		UR_blk_end							: in std_logic:= '0';
		UR_ack_packet						: out std_logic:= '0'; 
		UR_rd_blk				   			: out std_logic;
		UR_Part_selection_0_1				: out std_logic; -- select the origin of the payload (UR 0 or 1)
 
		UR0_MEM_req_rd_blk 					: out std_logic;					-- request to read UR 
		UR0_MEM_Add_request 				: out std_logic_vector(31 downto 0);-- Address to start the read
		UR0_MEM_Size_request 				: out std_logic_vector(31 downto 0);-- length to read (in bytes)
		UR0_MEM_request_done				: in std_logic;						-- The Packet is read from UR (ended)
		UR0_blk_ready						: in std_logic;-- informs that the Data for the grant buffer is ready
		
		UR1_MEM_req_rd_blk 					: out std_logic;					-- request to read UR 
		UR1_MEM_Add_request 				: out std_logic_vector(31 downto 0);-- Address to start the read
		UR1_MEM_Size_request 				: out std_logic_vector(31 downto 0);-- length to read (in bytes)
		UR1_MEM_request_done				: in std_logic;						-- The Packet is read from UR (ended)
		UR1_blk_ready						: in std_logic;-- informs that the Data for the grant buffer is ready
		
		usr_clk							    : in std_logic;
		usr_rst_n						    : in std_logic;
		usr_func_wr						    : in std_logic_vector(16383 downto 0); 
		usr_wen							    : in std_logic;
		usr_data_wr					   		: in std_logic_vector(63 downto 0); 
		                              
		usr_func_rd						    : in std_logic_vector(16383 downto 0); 
		usr_rden							: in std_logic;
		usr_data_rd						    : out std_logic_vector(63 downto 0);
		usr_rd_val						    : out std_logic;
		
		tx_usr_clk 						    : in std_logic;
		TX_lbus_in						    : out tx_user_ift;
		TX_lbus_wr						    : out std_logic;
		TX_pack_ready					    : out std_logic;
		TX_FIFO_Full					    : in std_logic;
		
		rx_lbus 							: in rx_user_ift;
		rx_lbus_clk							: in std_logic;
		
		resetp_counter_out                  : out std_logic;
		close_fed0                          : out std_logic;
		close_fed1                          : out std_logic
   );
end ethernet_block;

--*///////////////////////////////////////////////////////////////////////////////
architecture Behavioral of ethernet_block is

signal MAC_Card                                 : std_logic_vector(47 downto 0); 
  
signal UR_Part_selection_0_1_cell						: std_logic;

signal TCP_Px_FLAG_Send_out								: std_logic_vector(7 downto 0);
signal TCP_Px_SEQ_Send_out								: std_logic_vector(31 downto 0);
signal TCP_Px_ACK_Send_out								: std_logic_vector(31 downto 0);
signal TCP_Px_option_Send_out							: std_logic_vector(2 downto 0);
signal TCP_Px_Length_Send_out							: std_logic_vector(31 downto 0);
signal TCP_Px_pckt_wo_dt								: std_logic;

signal UR0_MEM_req_rd_blk_reg							: std_logic; 
signal UR0_MEM_Size_request_reg							: std_logic_vector(15 downto 0);
  
signal no_transfer_ongoing							: std_logic;
signal end_of_eth_transfert							: std_logic;

signal TCP_Px_request_took		   					: std_logic;
signal TCP_Px_request_done 		   					: std_logic;
signal TCP_P0_FLAG_Send_Send_Pkt					: std_logic_vector(7 downto 0);
signal TCP_P0_SEQ_Send_Send_Pkt						: std_logic_vector(31 downto 0);
signal TCP_P0_ACK_Send_Send_Pkt						: std_logic_vector(31 downto 0);
signal TCP_P0_option_Send_Send_Pkt					: std_logic_vector(2 downto 0);
signal TCP_P0_Length_Send_Send_Pkt					: std_logic_vector(31 downto 0);
signal TCP_P0_state_o								: std_logic_vector( 2 downto 0); -- (0) closed  -- (1) sync sent -- (2)  Established
signal TCP_P0_Persist_state							: std_logic;
signal TCP_P0_request_took							: std_logic;
signal TCP_Buf0_resync_new_pointer					: std_logic_vector(31 downto 0);
signal TCP_P0_FLAG_R_Ack_Pkt						: std_logic_vector(7 downto 0);
signal TCP_P0_SEQ_R_Ack_Pkt							: std_logic_vector(31 downto 0);
signal TCP_P0_ACK_R_Ack_Pkt							: std_logic_vector(31 downto 0);
signal TCP_P0_Opt_R_Ack_Pkt							: std_logic_vector(2 downto 0);
signal TCP_P0_wo_DT_R_Ack_Pkt						: std_logic;
signal TCP_P0_wen_rq_Send_pkt						: std_logic;
signal TCP_P0_wen_rq_ack_pkt						: std_logic;
signal TCP_P1_FLAG_Send_Send_Pkt					: std_logic_vector(7 downto 0);
signal TCP_P1_SEQ_Send_Send_Pkt						: std_logic_vector(31 downto 0);
signal TCP_P1_ACK_Send_Send_Pkt						: std_logic_vector(31 downto 0);
signal TCP_P1_option_Send_Send_Pkt					: std_logic_vector(2 downto 0);
signal TCP_P1_Length_Send_Send_Pkt					: std_logic_vector(31 downto 0);
signal TCP_P1_state_o								: std_logic_vector( 2 downto 0); -- (0) closed  -- (1) sync sent -- (2)  Established
signal TCP_P1_Persist_state							: std_logic;
signal TCP_P1_request_took							: std_logic;
signal TCP_Buf1_resync_new_pointer					: std_logic_vector(31 downto 0);
signal TCP_P1_FLAG_R_Ack_Pkt						: std_logic_vector(7 downto 0);
signal TCP_P1_SEQ_R_Ack_Pkt							: std_logic_vector(31 downto 0);
signal TCP_P1_ACK_R_Ack_Pkt							: std_logic_vector(31 downto 0);
signal TCP_P1_Opt_R_Ack_Pkt							: std_logic_vector(2 downto 0);
signal TCP_P1_wo_DT_R_Ack_Pkt						: std_logic;
signal TCP_P1_wen_rq_Send_pkt						: std_logic;
signal TCP_P1_wen_rq_ack_pkt						: std_logic;

signal TCP_P0_pckt_wo_Send_Pkt						: std_logic;
signal TCP_P1_pckt_wo_Send_Pkt						: std_logic;
signal TCP_Px_request_TCP_packet					: std_logic; 
signal TCP_P0_request_TCP_packet					: std_logic; 
signal TCP_P1_request_TCP_packet					: std_logic; 
-- signal TCP_P0_request_TCP_packet_n					: std_logic;   
-- signal TCP_P1_request_TCP_packet_n					: std_logic;   
signal TCP_P0_request_TCP_snd_packet_n				: std_logic;   
signal TCP_P0_request_TCP_ack_packet_n				: std_logic;   
signal TCP_P1_request_TCP_snd_packet_n				: std_logic;   
signal TCP_P1_request_TCP_ack_packet_n				: std_logic; 

-- arbiter to control TCP_BufBuffe 0 or 1
signal TCP_Buf0_REQ			: std_logic;
signal TCP_Buf0_GNT			: std_logic; 
signal TCP_Buf1_REQ			: std_logic;
signal TCP_Buf1_GNT			: std_logic; 
signal toggle_GNT			: std_logic;

signal TCP_P0_statistic_val							: std_logic_vector(63 downto 0);
signal TCP_P1_statistic_val							: std_logic_vector(63 downto 0); 
signal timestamp									: std_logic_vector(63 downto 0);
 
signal req_pause_frame								: std_logic; 
signal wait_on_eth_transfert						: std_logic; 
signal counter_pause_frame							: std_logic_vector(31 downto 0);
signal delay_pause_frame							: std_logic_vector(15 downto 0);
 
COMPONENT tcp_ack_fifo
  PORT (
    clk         : IN STD_LOGIC;
    srst        : IN STD_LOGIC;
    din         : IN STD_LOGIC_VECTOR(127 DOWNTO 0);
    wr_en       : IN STD_LOGIC;
    rd_en       : IN STD_LOGIC;
    dout        : OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
    full        : OUT STD_LOGIC;
    empty       : OUT STD_LOGIC;
    wr_rst_busy : OUT STD_LOGIC;
    rd_rst_busy : OUT STD_LOGIC
  );
END COMPONENT;

signal TCP_Px_ack_num_rcv							: std_logic_vector(31 downto 0);
signal TCP_Px_seq_num_rcv							: std_logic_vector(31 downto 0);
signal TCP_Px_win_num_rcv							: std_logic_vector(31 downto 0);
signal TCP_Px_flag_num_rcv							: std_logic_vector(7 downto 0);
signal TCP_Px_dt_len_rcv  							: std_logic_vector(15 downto 0);
signal TCP_P0_ack_num_rcvo							: std_logic_vector(31 downto 0);
signal TCP_P0_seq_num_rcvo							: std_logic_vector(31 downto 0);
signal TCP_P0_win_num_rcvo							: std_logic_vector(31 downto 0);
signal TCP_P0_flag_num_rcvo							: std_logic_vector(7 downto 0);
signal TCP_P0_dt_len_rcvo							: std_logic_vector(15 downto 0);
signal TCP_P0_no_used								: std_logic_vector(7 downto 0);
signal TCP_P1_ack_num_rcvo							: std_logic_vector(31 downto 0);
signal TCP_P1_seq_num_rcvo							: std_logic_vector(31 downto 0);
signal TCP_P1_win_num_rcvo							: std_logic_vector(31 downto 0);
signal TCP_P1_flag_num_rcvo							: std_logic_vector(7 downto 0);
signal TCP_P1_dt_len_rcvo							: std_logic_vector(15 downto 0);
signal TCP_P1_no_used								: std_logic_vector(7 downto 0);


signal TCP_P0_Ack_present_n						    : std_logic;
signal TCP_P0_Ack_present						    : std_logic;
signal TCP_P1_Ack_present_n						    : std_logic;
signal TCP_P1_Ack_present						    : std_logic;
signal rd_TCP_P0_val_rcvo					        : std_logic;
signal rd_TCP_P1_val_rcvo					        : std_logic;
signal TCP_P0_latch_ack								: std_logic;
signal TCP_P1_latch_ack								: std_logic;
 
signal TCP_P0_Stat_cnt_ena							: std_logic_vector(18 downto 0);
signal TCP_P0_Stat_flags							: std_logic_vector(31 downto 0);
signal TCP_P0_Stat_snd_byte							: std_logic_vector(63 downto 0);
signal TCP_P0_stat_snd_rexmit_byte					: std_logic_vector(63 downto 0);
signal TCP_P0_stat_dupack_max						: std_logic_vector(31 downto 0);
signal TCP_P0_stat_wnd_max							: std_logic_vector(31 downto 0);
signal TCP_P0_stat_wnd_min							: std_logic_vector(31 downto 0);
signal TCP_P0_measure_RTT_max						: std_logic_vector(31 downto 0);
signal TCP_P0_measure_RTT_min						: std_logic_vector(31 downto 0);
signal TCP_P0_Measure_RTT_cnt						: std_logic_vector(31 downto 0);
signal TCP_P0_Measure_RTT_Accu						: std_logic_vector(63 downto 0);
signal TCP_P0_rtt_shifts_MAX						: std_logic_vector(31 downto 0);
signal TCP_P0_rtt_v									: std_logic_vector(31 downto 0);
signal TCP_P0_MSS_rcv								: std_logic_vector(15 downto 0);
signal TCP_P1_Stat_cnt_ena							: std_logic_vector(18 downto 0);
signal TCP_P1_Stat_flags							: std_logic_vector(31 downto 0);
signal TCP_P1_Stat_snd_byte							: std_logic_vector(63 downto 0);
signal TCP_P1_stat_snd_rexmit_byte					: std_logic_vector(63 downto 0);
signal TCP_P1_stat_dupack_max						: std_logic_vector(31 downto 0);
signal TCP_P1_stat_wnd_max							: std_logic_vector(31 downto 0);
signal TCP_P1_stat_wnd_min							: std_logic_vector(31 downto 0);
signal TCP_P1_measure_RTT_max						: std_logic_vector(31 downto 0);
signal TCP_P1_measure_RTT_min						: std_logic_vector(31 downto 0);
signal TCP_P1_Measure_RTT_cnt						: std_logic_vector(31 downto 0);
signal TCP_P1_Measure_RTT_Accu						: std_logic_vector(63 downto 0);
signal TCP_P1_rtt_shifts_MAX						: std_logic_vector(31 downto 0);
signal TCP_P1_rtt_v									: std_logic_vector(31 downto 0); 
signal TCP_P1_MSS_rcv								: std_logic_vector(15 downto 0);
signal latch_for_mon								: std_logic; 
 
signal PORT0_D										: STD_LOGIC_VECTOR(15 DOWNTO 0);
signal PORT0_S										: STD_LOGIC_VECTOR(15 DOWNTO 0);
signal PORT1_D										: STD_LOGIC_VECTOR(15 DOWNTO 0);
signal PORT1_S										: STD_LOGIC_VECTOR(15 DOWNTO 0);
signal PORTx_D										: STD_LOGIC_VECTOR(15 DOWNTO 0);
signal PORTx_S										: STD_LOGIC_VECTOR(15 DOWNTO 0); 

signal MAC_S										: STD_LOGIC_VECTOR(47 downto 0);
signal IP_D											: STD_LOGIC_VECTOR(31 downto 0);
signal IP_S											: STD_LOGIC_VECTOR(31 downto 0);
signal IP_GATEWAY									: STD_LOGIC_VECTOR(31 downto 0);
signal IP_NETWORK									: STD_LOGIC_VECTOR(31 downto 0);
signal NETWORK										: STD_LOGIC_VECTOR(31 downto 0);
signal TCP_max_size								    : STD_LOGIC_VECTOR(15 downto 0);
signal TCP_scale									: STD_LOGIC_VECTOR(7 downto 0);
signal TCP_win										: STD_LOGIC_VECTOR(15 downto 0);
signal TCP_TS										: STD_LOGIC_VECTOR(31 downto 0);
signal TCP_TS_rply								    : STD_LOGIC_VECTOR(31 downto 0);
signal snd_cwnd									    : STD_LOGIC_VECTOR(31 downto 0); 
signal load_new_snd_cwnd						    : STD_LOGIC;
signal load_new_snd_cwnd_sync					    : STD_LOGIC;
signal timer_rtt_I								    : STD_LOGIC_VECTOR(31 downto 0); 
signal timer_rtt_I_sync							    : STD_LOGIC_VECTOR(31 downto 0);
signal timer_persist_I							    : STD_LOGIC_VECTOR(31 downto 0);
signal TCP_Rexmt_CWND_sh						    : STD_LOGIC_VECTOR(31 downto 0);
signal Init_bit									    : STD_LOGIC_VECTOR(15 downto 0);
signal thresold_retrans							    : STD_LOGIC_VECTOR(31 downto 0);	
signal TCP_reg_out								    : STD_LOGIC_VECTOR(63 downto 0);
 
signal reset_low_clock                          : std_logic;
signal request_probe_arp                        : std_logic_vector(1 downto 0);
signal ARP_acc_done 				            : std_logic;
signal request_arp_rg				            : std_logic_vector(3 downto 0);
signal ARP_Req_done					            : std_logic;
signal probe_ARP_DONE				            : std_logic; 
signal IP_dst_and_IP_nwk_eq_Net_Dst	            : std_logic; 
signal Req_acc_ARP              	            : std_logic; 
signal Req_acc_ARP_sync         	            : std_logic; 
signal ARP_probe								: std_logic;
signal ARP_dst  								: std_logic;
signal ARP_gw   								: std_logic;
signal ARP_reply_rcv                            : std_logic;
signal ARP_reply_done                           : std_logic_vector(1 downto 0);

signal ARP_MAC_Dest      						: std_logic_vector(47 downto 0);
signal ARP_MAC_GateWay  						: std_logic_vector(47 downto 0);
signal ARP_check_MAC_src 						: std_logic;
signal ARP_check_IP_src  						: std_logic;
signal ARP_MAC_error    						: std_logic_vector(47 downto 0);
signal ARP_MAC_rcvOK 							: std_logic;
signal ARP_MAC_gwOK  							: std_logic;
signal ARP_IP									: std_logic_vector(31 downto 0); 
signal req_ARP_reply 							: std_logic;
signal ARP_reply     							: std_logic;	
signal valid_ARP_packet							: std_logic;
signal req_icmp_prb								: std_logic;
signal PING_HD_empty							: std_logic;
signal ping_last_dt								: std_logic;
signal wr_PING_data								: std_logic;
signal data_ping								: std_logic_vector(63 downto 0);
signal Reset_PING_FIFO							: std_logic;
signal PING_MAC_dst								: std_logic_vector(47 downto 0);
signal PING_IP_dst								: std_logic_vector(31 downto 0);
				
signal Rst_tx_usr_clk_p							: std_logic;
signal Rst_tx_usr_clk_n							: std_logic;
				 
signal PING_req									: std_logic;  
signal PING_req_resync							: std_logic; 
				
signal Ping_time								: std_logic_vector(31 downto 0);
signal Ping_time_lathced						: std_logic_vector(31 downto 0);
signal Ping_time_ena							: std_logic ;
signal received_Ping_reply						: std_logic ; 
signal destination_MAC							: std_logic_vector(47 downto 0);
signal ping_req_counter							: std_logic_vector(31 downto 0);
signal ping_rec_counter							: std_logic_vector(31 downto 0);
signal counter_rst								: std_logic;
signal counter_rst_resync						: std_logic;  
				
signal request_sync_con0 						: std_logic_vector(1 downto 0);
signal request_sync_con1 						: std_logic_vector(1 downto 0); 

signal request_RESET_con0						: std_logic_vector(1 downto 0);
signal request_RESET_con1						: std_logic_vector(1 downto 0);
				
signal Data_rd_reg								: std_logic_vector(63 downto 0); 
  					 
signal nb_retrans_val							: std_logic_vector(31 downto 0);
signal nb_retrans_cnt							: std_logic_vector(31 downto 0);
signal stop_transfert_TCP						: std_logic;
 
attribute mark_debug : string; 
--attribute mark_debug of TCP_Px_SEQ_Send_out	: signal is "true"; 

--#############################################################################
-- Code start here
--#############################################################################
begin

MAC_Card	<= MAC_card_higher_part & MAC_lower_part;

---*****************************************************************************************************
--Interface Write  and Read command
process(usr_rst_n,usr_clk)
begin
	if usr_rst_n = '0' then
		counter_rst				<= '0'; 
		nb_retrans_val			<= (others => '1');
	elsif rising_edge(usr_clk) then
		counter_rst				<= '0';
	
		if usr_wen = '1'   then
			if ( usr_func_wr(eth_100Gb_counter_reset 			+ addr_off_con0 + addr_offset_100G_eth) = '1') or 
			  ( usr_func_wr(eth_100Gb_counter_reset 			+ addr_off_con1 + addr_offset_100G_eth) = '1') then 
				counter_rst				<= usr_data_wr(28);
			end if;	
			
			if (usr_func_wr(eth_100Gb_ETH_request_status 	+ addr_off_con0 + addr_offset_100G_eth) = '1') or 
			  ( usr_func_wr(eth_100Gb_ETH_request_status    + addr_off_con1 + addr_offset_100G_eth) = '1') then 
			--	local_reset(0)			<= usr_data_wr(31);
			end if;	
			
			if (usr_func_wr(eth_100Gb_nb_retrans_before_stop + addr_off_con0 + addr_offset_100G_eth) = '1') or 
			  ( usr_func_wr(eth_100Gb_nb_retrans_before_stop + addr_off_con1 + addr_offset_100G_eth) = '1') then
				nb_retrans_val			<= usr_data_wr(31 downto 0);
			end if;
		end if;
	end if;
end process;

resetp_counter_out   <= counter_rst;

process(usr_clk)
begin
	if rising_edge(usr_clk) then
 		request_arp_rg(0)				<= '0';
		request_probe_arp(0)			<= '0';
		PING_req						<= '0';
		latch_for_mon					<= '0';
		request_sync_con0(0)			<= '0';
		request_sync_con1(0)			<= '0';
		request_RESET_con0(0)			<= '0';
		request_RESET_con1(0)			<= '0';
		if usr_wen = '1' then
			if (usr_func_wr(eth_100Gb_ETH_request_status 	+ addr_off_con0 + addr_offset_100G_eth) = '1') or 
			  ( usr_func_wr(eth_100Gb_ETH_request_status    + addr_off_con1 + addr_offset_100G_eth) = '1') then
				request_arp_rg(0)   	<= usr_data_wr(0);
				request_probe_arp(0)    <= usr_data_wr(1);
				PING_req				<= usr_data_wr(2);
				
				request_sync_con0(0)	<= usr_data_wr(4);
				request_RESET_con0(0)	<= usr_data_wr(5);
				
				request_sync_con1(0)	<= usr_data_wr(8);
				request_RESET_con1(0)	<= usr_data_wr(9);
				
				latch_for_mon			<= usr_data_wr(30);
			end if;	
		end if;
	end if;
end process;
 
process(usr_clk)
begin
	if rising_edge(usr_clk) then		
		Data_rd_reg 						    <=	TCP_reg_out or TCP_P0_statistic_val  or TCP_P1_statistic_val;
		 
		if (usr_func_rd(eth_100Gb_counter_reset 				+ addr_off_con0 + addr_offset_100G_eth) = '1') or 
		   (usr_func_rd(eth_100Gb_counter_reset 				+ addr_off_con1 + addr_offset_100G_eth) = '1')then 
			Data_rd_reg(28) 			        <= counter_rst;
		elsif (usr_func_rd(eth_100Gb_ping_counter_latched 	+ addr_off_con0 + addr_offset_100G_eth) = '1') or 
		      (usr_func_rd(eth_100Gb_ping_counter_latched 	+ addr_off_con1 + addr_offset_100G_eth) = '1') then
			Data_rd_reg(31 downto 0)		   	<= Ping_time_lathced;
			
		elsif (usr_func_rd(eth_100Gb_ping_req_cnt   + addr_off_con0 + addr_offset_100G_eth) = '1') or 
		      (usr_func_rd(eth_100Gb_ping_req_cnt 	+ addr_off_con1 + addr_offset_100G_eth) = '1') then
			Data_rd_reg(31 downto 0)			<= ping_req_counter;
			
		elsif (usr_func_rd(eth_100Gb_ping_rcv_cnt 	+ addr_off_con0 + addr_offset_100G_eth) = '1') or 
		       (usr_func_rd(eth_100Gb_ping_rcv_cnt 	+ addr_off_con1 + addr_offset_100G_eth) = '1') then
			Data_rd_reg(31 downto 0)			<= ping_rec_counter;
			
		elsif  (usr_func_rd(eth_100Gb_nb_retrans_before_stop + addr_off_con0 + addr_offset_100G_eth) = '1') or 
		       (usr_func_rd(eth_100Gb_nb_retrans_before_stop 	+ addr_off_con1 + addr_offset_100G_eth) = '1') then
			Data_rd_reg(30 downto 0)			<= nb_retrans_val(30 downto 0);	
			-- Data_rd_reg(31)						<= stop_transfert_TCP;
			
		elsif (usr_func_rd(eth_100Gb_pause_frame_rcv 	+ addr_off_con0 + addr_offset_100G_eth) = '1') or 
		       (usr_func_rd(eth_100Gb_pause_frame_rcv 	+ addr_off_con1 + addr_offset_100G_eth) = '1') then
			Data_rd_reg(31 downto 0)			<= counter_pause_frame;	
			Data_rd_reg(47 downto 32)			<= delay_pause_frame; 

        elsif  (usr_func_rd(eth_100Gb_ETH_request_status 	+ addr_off_con0 + addr_offset_100G_eth) = '1') or 
		       (usr_func_rd(eth_100Gb_ETH_request_status 	+ addr_off_con1 + addr_offset_100G_eth) = '1') then
			Data_rd_reg(16)			            <= ARP_reply_rcv; 
			Data_rd_reg(17)			            <= probe_ARP_DONE; 

        elsif  (usr_func_rd(eth_100Gb_Dest_MAC_address 	+ addr_off_con0 + addr_offset_100G_eth) = '1')  or 
		       (usr_func_rd(eth_100Gb_Dest_MAC_address 	+ addr_off_con1 + addr_offset_100G_eth) = '1') then
			Data_rd_reg(47 downto 0)		    <= destination_MAC; 
		end if;
 
	end if;
end process;
 
usr_data_rd  	<= Data_rd_reg;

--****************************************************************************************************** 
-- resync command request ARP PING ....
 	
eth_cmd_resync_pulse_i3:entity work.resync_pulse  
   port map(
   	aresetn				=> usr_rst_n,
   	clocki			    => usr_clk,
   	in_s				=> ping_req,
   	clocko			    => tx_usr_clk,
   	out_s			    => ping_req_resync
   	);   	
		
	
--****************************************************************************************************** 
-- resync reset to MAC 100G tx clock
reset_resync_i4:entity work.resetp_resync 
	port map(
		aresetp				=> counter_rst	, 
		clock				=> tx_usr_clk	,  
		Resetp_sync			=> counter_rst_resync	 
		);  
		
reset_resync_i5:entity work.resetn_resync 
	port map(
		aresetn				=> usr_rst_n	, 
		clock				=> tx_usr_clk	, 
		Resetn_sync			=> Rst_tx_usr_clk_n	 
		); 		
Rst_tx_usr_clk_p		<= not(Rst_tx_usr_clk_n);	

  
--************************************************
-- open con reset con ARP probe	ARP request...
 -- resync open_conn
Open_con_resync_pulse_i0_pci_xgmii:entity work.resync_pulse 
	port map(
		aresetn			=> usr_rst_n					,		 
		clocki			=> usr_clk						,		 
		clocko			=> tx_usr_clk					,		 
		in_s			=> request_sync_con0(0)			,	 
		out_s			=> request_sync_con0(1)		 	 
		);				
 -- resync Reset_conn
Reset_con_resync_pulse_i0_pci_xgmii:entity work.resync_pulse 
	port map(
		aresetn			=> usr_rst_n					,		 
		clocki			=> usr_clk						,		 
		clocko			=> tx_usr_clk					,		 
		in_s			=> request_reset_con0(0)			,	 
		out_s			=> request_reset_con0(1)		  
		);		
 -- resync open_conn		
 Open_con_resync_pulse_i1_pci_xgmii:entity work.resync_pulse 
	port map(
		aresetn			=> usr_rst_n					,		 
		clocki			=> usr_clk						,		 
		clocko			=> tx_usr_clk					,		 
		in_s			=> request_sync_con1(0)			,	 
		out_s			=> request_sync_con1(1)		 	 
		);				
 -- resync Reset_conn
Reset_con_resync_pulse_i1_pci_xgmii:entity work.resync_pulse 
	port map(
		aresetn			=> usr_rst_n					,		 
		clocki			=> usr_clk						,		 
		clocko			=> tx_usr_clk					,		 
		in_s			=> request_reset_con1(0)			,	 
		out_s			=> request_reset_con1(1)		  
		);		
 
--*******************************************************************************************
--   TCP logic for TCP_Buf 0 and 1

process(tx_usr_clk)
begin
	if rising_edge(tx_usr_clk) then
		TCP_Buf0_resync_new_pointer	<= TCP_Buf0_SND_NEW;
	end if;
end process;

TCP_logic_i0:entity work.TCP_manage_0x 
generic map(TCP_THRESHOLD_RTTSHIFT	=>  x"06",
			TCP_MAX_RTTSHIFT		=> x"0C",
			WINDOW					=> x"01"
			-- snd_cwnd			=> x"00020000";
			-- timer_rtt_init	=> x"0002625A"; 
			-- timer_rtt_init_sync => x"12A05F20";
			-- timer_persist_init  => x"0004C4B4"
	 )
port map(
	reset_n								    => Rst_tx_usr_clk_n				   		        , -- Active low
	rst_local							    => counter_rst_resync			                , -- Active high
	clock									=> tx_usr_clk						            ,
	ISS									    => x"FFFFFFFF"						            , -- Initial Segment Number
	f_Push								    => Init_bit(0)						            ,			
	Req_Sync								=> request_sync_con0(1)			                ,			
	Req_Rst									=> request_reset_con0(1)			            ,			
	MSS_srv(15 downto 0)				    => TCP_P0_MSS_rcv						        ,			 --value received from server
	MSS_srv(31 downto 16)			        => x"0000" 							            ,			 --value received from server
	MSS_set								    => TCP_max_size					                ,			 --value set by the controller software 
	-- value receive in a packet   
	rcv_pckt								=> TCP_P0_Ack_present				 	        ,			
	rd_val								    => rd_TCP_P0_val_rcvo					        ,			
	rcv_seg_SEQ							    => TCP_P0_seq_num_rcvo					        ,			
	rcv_seg_ACK							    => TCP_P0_ack_num_rcvo					        ,			
	rcv_WIN								    => TCP_P0_win_num_rcvo					        ,			-- value WINDOW shift by option 2 (if used)
	rcv_FLAG								=> TCP_P0_flag_num_rcvo					        ,			
	rcv_len								    => TCP_P0_dt_len_rcvo					        ,			 -- len in case of we receive data with ack ?? normally not allow
	-- value send to build TCP_IP headers for ACK packet
	Acpt_rq_ack_pkt					        => TCP_P0_request_TCP_ack_packet_n			        ,			-- accept parameters for TCP packet
	wen_rq_ack_pkt						    => TCP_P0_wen_rq_ack_pkt					    ,			-- send a TCP packet parameters
	FLAG_out								=> TCP_P0_FLAG_R_Ack_Pkt					    ,			
	SEQ_out								    => TCP_P0_SEQ_R_Ack_Pkt					        ,			
	ACK_out								    => TCP_P0_ACK_R_Ack_Pkt 					    ,			
	option_out							    => TCP_P0_Opt_R_Ack_Pkt					        ,			
	-- value send to build TCP_IP headers for DATA/SYNC packet
	Acpt_rq_Send_Pkt					    => TCP_P0_request_TCP_snd_packet_n			        ,			-- accept parameters for TCP packet
	wen_rq_Send_pkt					        => TCP_P0_wen_rq_Send_pkt				        ,			-- send a TCP packet parameters
	FLAG_Send_out						    => TCP_P0_FLAG_Send_Send_Pkt			        ,			
	SEQ_Send_out						    => TCP_P0_SEQ_Send_Send_Pkt				        ,			
	ACK_Send_out						    => TCP_P0_ACK_Send_Send_Pkt				        ,			
	option_Send_out					        => TCP_P0_option_Send_Send_Pkt			        ,			
	Length_Send_out					        => TCP_P0_Length_Send_Send_Pkt			        ,			
	pckt_wo_dt							    => TCP_P0_pckt_wo_Send_Pkt				        ,			
	-- signal between SEND and RECEIVE
	Get_TIMESTAMP						    => timestamp(31 downto 0)		                ,			
	-- value from/to the SENDER part
	Pkt_done								=> TCP_P0_request_took				            ,			--: inform that the packet is sent
	snd_new								    => TCP_Buf0_resync_new_pointer				    ,			
	Pointer_read						    => TCP_Buf0_SND_UNA						    	,			
	valid_new_p							    => TCP_Buf0_VALID_P						    	,			
	no_delay								=> Init_bit(2)						            ,			
	-- state_o								=> 			                                    ,				
	statistic							    => TCP_P0_Stat_cnt_ena(18 downto 0)	 	        ,			
	Stat_flag							    => TCP_P0_Stat_flags(31 downto 0)		        ,			
	f_fastretransmit_disabled		        => Init_bit(3)						            ,			
	snd_cwnd								=> snd_cwnd							            ,			
	load_new_snd_cwnd					    => load_new_snd_cwnd_sync		                ,			
	timer_rtt_init						    => timer_rtt_I						            ,			
	timer_rtt_init_sync 				    => timer_rtt_I_sync				                ,			
	timer_persist_init  				    => timer_persist_I				                ,			
	tcprexmtthresh						    => thresold_retrans				                ,			
	timer_stop_disable				        => Init_bit(4)						            ,			
	TCP_REXMT_CWND_SHIFT				    => TCP_Rexmt_CWND_sh				            ,			
	measure_RTT_max					        => TCP_P0_measure_RTT_max				        ,			
	measure_RTT_min					        => TCP_P0_measure_RTT_min				        ,			
	Measure_RTT_cnt					        => TCP_P0_Measure_RTT_cnt				        ,			
	Measure_RTT_Accu					    => TCP_P0_Measure_RTT_Accu				        ,			
	stat_wnd_max						    => TCP_P0_stat_wnd_max					        ,			
	stat_wnd_min						    => TCP_P0_stat_wnd_min					        ,			
	Stat_snd_byte						    => TCP_P0_Stat_snd_byte					        ,		
	stat_snd_rexmit_byte				    => TCP_P0_stat_snd_rexmit_byte			        ,			
	stat_dupack_max					        => TCP_P0_stat_dupack_max				        ,			
	rtt_shifts_MAX						    => TCP_P0_rtt_shifts_MAX				        ,			
	rtt_v									=> TCP_P0_rtt_v							        ,		
	TCP_state_o							    => TCP_P0_state_o						        ,		 -- (0) closed  -- (1) sync sent -- (2)  Established
	Persist_state						    => TCP_P0_Persist_state						 
	);

close_fed0 <= TCP_P0_state_o(0);
 


process(tx_usr_clk)
begin
	if rising_edge(tx_usr_clk) then
		TCP_Buf1_resync_new_pointer	<= TCP_Buf1_SND_NEW;
	end if;
end process;

TCP_logic_i1:entity work.TCP_manage_0x 
generic map(TCP_THRESHOLD_RTTSHIFT	=>  x"06",
			TCP_MAX_RTTSHIFT		=> x"0C",
			WINDOW					=> x"01"
			-- snd_cwnd			=> x"00020000";
			-- timer_rtt_init	=> x"0002625A"; 
			-- timer_rtt_init_sync => x"12A05F20";
			-- timer_persist_init  => x"0004C4B4"
	 )
port map(
	reset_n								    => Rst_tx_usr_clk_n				   		        , -- Active low
	rst_local							    => counter_rst_resync			                , -- Active high
	clock									=> tx_usr_clk						            ,
	ISS									    => x"FFFFFFFF"						            , -- Initial Segment Number
	f_Push								    => Init_bit(0)						            ,			
	Req_Sync								=> request_sync_con1(1)			                ,			
	Req_Rst									=> request_reset_con1(1)			            ,			
	MSS_srv(15 downto 0)				    => TCP_P0_MSS_rcv						        ,			 --value received from server
	MSS_srv(31 downto 16)			        => x"0000" 							            ,			 --value received from server
	MSS_set								    => TCP_max_size					                ,			 --value set by the controller software 
	-- value receive in a packet   
	rcv_pckt								=> TCP_P1_Ack_present				 	        ,			
	rd_val								    => rd_TCP_P1_val_rcvo					        ,			
	rcv_seg_SEQ							    => TCP_P1_seq_num_rcvo					        ,			
	rcv_seg_ACK							    => TCP_P1_ack_num_rcvo					        ,			
	rcv_WIN								    => TCP_P1_win_num_rcvo					        ,			-- value WINDOW shift by option 2 (if used)
	rcv_FLAG								=> TCP_P1_flag_num_rcvo					        ,			
	rcv_len								    => TCP_P1_dt_len_rcvo					        ,			 -- len in case of we receive data with ack ?? normally not allow
	-- value send to build TCP_IP headers for ACK packet
	Acpt_rq_ack_pkt					        => TCP_P1_request_TCP_ack_packet_n			        ,			-- accept parameters for TCP packet
	wen_rq_ack_pkt						    => TCP_P1_wen_rq_ack_pkt					    ,			-- send a TCP packet parameters
	FLAG_out								=> TCP_P1_FLAG_R_Ack_Pkt					    ,			
	SEQ_out								    => TCP_P1_SEQ_R_Ack_Pkt					        ,			
	ACK_out								    => TCP_P1_ACK_R_Ack_Pkt 					    ,			
	option_out							    => TCP_P1_Opt_R_Ack_Pkt					        ,			
	-- value send to build TCP_IP headers for DATA/SYNC packet
	Acpt_rq_Send_Pkt					    => TCP_P1_request_TCP_snd_packet_n			        ,			-- accept parameters for TCP packet
	wen_rq_Send_pkt					        => TCP_P1_wen_rq_Send_pkt				        ,			-- send a TCP packet parameters
	FLAG_Send_out						    => TCP_P1_FLAG_Send_Send_Pkt			        ,			
	SEQ_Send_out						    => TCP_P1_SEQ_Send_Send_Pkt				        ,			
	ACK_Send_out						    => TCP_P1_ACK_Send_Send_Pkt				        ,			
	option_Send_out					        => TCP_P1_option_Send_Send_Pkt			        ,			
	Length_Send_out					        => TCP_P1_Length_Send_Send_Pkt			        ,			
	pckt_wo_dt							    => TCP_P1_pckt_wo_Send_Pkt				        ,			
	-- signal between SEND and RECEIVE
	Get_TIMESTAMP						    => timestamp(31 downto 0)		                ,			
	-- value from/to the SENDER part
	Pkt_done								=> TCP_P1_request_took				            ,			--: inform that the packet is sent
	snd_new								    => TCP_Buf1_resync_new_pointer				    ,			
	Pointer_read						    => TCP_Buf1_SND_UNA						    	,			
	valid_new_p							    => TCP_Buf1_VALID_P						    	,			
	no_delay								=> Init_bit(2)						            ,			
	-- state_o								=> 			                                    ,				
	statistic							    => TCP_P1_Stat_cnt_ena(18 downto 0)	 	        ,			
	Stat_flag							    => TCP_P1_Stat_flags(31 downto 0)		        ,			
	f_fastretransmit_disabled		        => Init_bit(3)						            ,			
	snd_cwnd								=> snd_cwnd							            ,			
	load_new_snd_cwnd					    => load_new_snd_cwnd_sync		                ,			
	timer_rtt_init						    => timer_rtt_I						            ,			
	timer_rtt_init_sync 				    => timer_rtt_I_sync				                ,			
	timer_persist_init  				    => timer_persist_I				                ,			
	tcprexmtthresh						    => thresold_retrans				                ,			
	timer_stop_disable				        => Init_bit(4)						            ,			
	TCP_REXMT_CWND_SHIFT				    => TCP_Rexmt_CWND_sh				            ,			
	measure_RTT_max					        => TCP_P1_measure_RTT_max				        ,			
	measure_RTT_min					        => TCP_P1_measure_RTT_min				        ,			
	Measure_RTT_cnt					        => TCP_P1_Measure_RTT_cnt				        ,			
	Measure_RTT_Accu					    => TCP_P1_Measure_RTT_Accu				        ,			
	stat_wnd_max						    => TCP_P1_stat_wnd_max					        ,			
	stat_wnd_min						    => TCP_P1_stat_wnd_min					        ,			
	Stat_snd_byte						    => TCP_P1_Stat_snd_byte					        ,		
	stat_snd_rexmit_byte				    => TCP_P1_stat_snd_rexmit_byte			        ,			
	stat_dupack_max					        => TCP_P1_stat_dupack_max				        ,			
	rtt_shifts_MAX						    => TCP_P1_rtt_shifts_MAX				        ,			
	rtt_v									=> TCP_P1_rtt_v							        ,		
	TCP_state_o							    => TCP_P1_state_o						        ,		 -- (0) closed  -- (1) sync sent -- (2)  Established
	Persist_state						    => TCP_P1_Persist_state						 
	);

close_fed1 <= TCP_P1_state_o(0);


 
--*******************************************************************************	
-- logic to stop after X retransmit on TCP
-- process(counter_rst_resync,tx_usr_clk)
-- begin
	-- if counter_rst_resync = '1' then
		-- nb_retrans_cnt		<= (others => '0');
		-- stop_transfert_TCP	<= '0';
	-- elsif rising_edge(tx_usr_clk) then
		-- if nb_retrans_cnt > nb_retrans_val then
			-- stop_transfert_TCP	<= '1';
		-- end if;
	
		-- if TCP_P0_Stat_cnt_ena(4) = '1' then
			-- nb_retrans_cnt <= nb_retrans_cnt + '1';
		-- end if;
	-- end if;
-- end process;
 
process(tx_usr_clk)	
begin
	if rising_edge(tx_usr_clk) then
		timestamp	<= timestamp + '1';
	end if;
end process;
	
	
-- *********************************************************************************
-- Arbiter and select the add seq# ack# lenght .... to send a packet
--
--	
Arbiter_i1:entity work.Arbiter_Data_to_ethernet 
 Port Map(
	Rst_tx_usr_clk_n									=> Rst_tx_usr_clk_n,             
	tx_usr_clk											=> tx_usr_clk,                   
	stop_transfert_TCP									=> '0',                          
	wait_on_eth_transfert								=> '0',                          
                                                                                        -- STREAM 0                                             
	TCP_P0_wen_rq_Send_pkt						    	=> TCP_P0_wen_rq_Send_pkt,       						-- request to transfer a TCP (open,Data,..) packet
	TCP_P0_wen_rq_ack_pkt 						    	=> TCP_P0_wen_rq_ack_pkt,        						-- request to transfer a TCP (ack) packet
	PORT0_D												=> PORT0_D,                     -- (15 downto 0);	-- Destination port number
	PORT0_S												=> PORT0_S,                     -- (15 downto 0);	-- Source port number
														 							    -- for a data or open or close
	TCP_P0_FLAG_Send_Send_Pkt					    	=> TCP_P0_FLAG_Send_Send_Pkt,   -- (7 downto 0);	-- flags for TCP packet
	TCP_P0_SEQ_Send_Send_Pkt							=> TCP_P0_SEQ_Send_Send_Pkt,    -- (31 downto 0);	-- Sequence Number for TCP packet
	TCP_P0_ACK_Send_Send_Pkt							=> TCP_P0_ACK_Send_Send_Pkt,    -- (31 downto 0);	-- Acknowledge Number for TCP packet
	TCP_P0_pckt_wo_Send_Pkt						    	=> TCP_P0_pckt_wo_Send_Pkt,     -- 				-- Specify if there are or no data in the packet (Open, close or data packet)
	TCP_P0_option_Send_Send_Pkt					    	=> TCP_P0_option_Send_Send_Pkt, -- (2 downto 0);	-- Specify the option in the OPEN connection
	TCP_P0_Length_Send_Send_Pkt					    	=> TCP_P0_Length_Send_Send_Pkt, -- (31 downto 0);	-- Length of data in the packet (in bytes)
														 								-- 					for an acknowledge packet
	TCP_P0_FLAG_R_Ack_Pkt						    	=> TCP_P0_FLAG_R_Ack_Pkt,       -- (7 downto 0);	-- flags for TCP packet
	TCP_P0_SEQ_R_Ack_Pkt								=> TCP_P0_SEQ_R_Ack_Pkt,        -- (31 downto 0);	-- Sequence Number for TCP packet
	TCP_P0_ACK_R_Ack_Pkt								=> TCP_P0_ACK_R_Ack_Pkt,        -- (31 downto 0);	-- Acknowledge Number for TCP packet
	TCP_P0_Opt_R_Ack_Pkt								=> TCP_P0_Opt_R_Ack_Pkt,        -- (2 downto 0); 	-- Specify the option in the ack
	TCP_P0_request_TCP_snd_packet_n				    	=> TCP_P0_request_TCP_snd_packet_n, -- 				 specify to TCP logic when the request is DONE (UR read and data transfered over Ethernet)
	TCP_P0_request_TCP_ack_packet_n				    	=> TCP_P0_request_TCP_ack_packet_n, -- 				 specify to TCP logic when the request is DONE (UR read and data transfered over Ethernet)
    TCP_P0_request_took									=> TCP_P0_request_took,
	-- STREAM 1	                                             
	TCP_P1_wen_rq_Send_pkt						    	=> TCP_P1_wen_rq_Send_pkt,      -- 				-- request to transfer a TCP (open,Data,..) packet
	TCP_P1_wen_rq_ack_pkt						    	=> TCP_P1_wen_rq_ack_pkt,       -- 				-- request to transfer a TCP (ack) packet
	PORT1_D												=> PORT1_D,                     -- (15 downto 0);
	PORT1_S												=> PORT1_S,                     -- (15 downto 0);
														 							    -- for a data or open or close
	TCP_P1_FLAG_Send_Send_Pkt					    	=> TCP_P1_FLAG_Send_Send_Pkt,   -- (7 downto 0);
	TCP_P1_SEQ_Send_Send_Pkt							=> TCP_P1_SEQ_Send_Send_Pkt,    -- (31 downto 0);
	TCP_P1_ACK_Send_Send_Pkt							=> TCP_P1_ACK_Send_Send_Pkt,    -- (31 downto 0);
	TCP_P1_pckt_wo_Send_Pkt						    	=> TCP_P1_pckt_wo_Send_Pkt,     -- 
	TCP_P1_option_Send_Send_Pkt					    	=> TCP_P1_option_Send_Send_Pkt, -- (2 downto 0);
	TCP_P1_Length_Send_Send_Pkt					    	=> TCP_P1_Length_Send_Send_Pkt, -- (31 downto 0);
														 						        -- for an acknowledge packet
	TCP_P1_FLAG_R_Ack_Pkt						    	=> TCP_P1_FLAG_R_Ack_Pkt,       -- (7 downto 0);
	TCP_P1_SEQ_R_Ack_Pkt								=> TCP_P1_SEQ_R_Ack_Pkt,        -- (31 downto 0);
	TCP_P1_ACK_R_Ack_Pkt								=> TCP_P1_ACK_R_Ack_Pkt,        -- (31 downto 0);
	TCP_P1_Opt_R_Ack_Pkt								=> TCP_P1_Opt_R_Ack_Pkt,        -- (2 downto 0); 
	TCP_P1_request_TCP_snd_packet_n					    => TCP_P1_request_TCP_snd_packet_n, --   														  
	TCP_P1_request_TCP_ack_packet_n					    => TCP_P1_request_TCP_ack_packet_n, --   														  
    TCP_P1_request_took									=> TCP_P1_request_took,
	                                                                                    --///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
                                                                                        -- request on  UR 	                                    
	UR0_MEM_req_rd_blk 									=> UR0_MEM_req_rd_blk 	,       -- 			-- request to read UR 
	UR0_MEM_Add_request 								=> UR0_MEM_Add_request ,        -- (31 downto 0);-- Address to start the read
	UR0_MEM_Size_request 								=> UR0_MEM_Size_request,        -- (31 downto 0);-- length to read (in bytes)
	UR0_MEM_request_done								=> UR0_MEM_request_done,        -- 				-- The Packet is read from UR (ended)
																						   
	UR1_MEM_req_rd_blk 									=> UR1_MEM_req_rd_blk 	,       -- 			-- request to read UR 
	UR1_MEM_Add_request 								=> UR1_MEM_Add_request ,        -- (31 downto 0);-- Address to start the read
	UR1_MEM_Size_request 								=> UR1_MEM_Size_request,        -- (31 downto 0);-- length to read (in bytes)
	UR1_MEM_request_done								=> UR1_MEM_request_done,        -- 				-- The Packet is read from UR (ended)
														 
	UR0_blk_ready										=> UR0_blk_ready,               -- informs that the Data are ready in the BIFI buffer
	UR1_blk_ready										=> UR1_blk_ready,               -- informs that the Data are ready in the BIFI buffer
																						   
	Request_to_send_Eth_packet							=> TCP_Px_request_TCP_packet,   -- 				-- request to send packet over Ethernet link
	UR_Part_selection_0_1								=> UR_Part_selection_0_1_cell,  --   			-- which UR to use
	PORTx_D												=> PORTx_D,                     -- (15 downto 0);
	PORTx_S												=> PORTx_S,                     -- (15 downto 0); 
	TCP_Px_FLAG_Send_out								=> TCP_Px_FLAG_Send_out,        -- (7 downto 0);
	TCP_Px_SEQ_Send_out								    => TCP_Px_SEQ_Send_out,         -- (31 downto 0);
	TCP_Px_ACK_Send_out								    => TCP_Px_ACK_Send_out,         -- (31 downto 0);
	TCP_Px_option_Send_out							    => TCP_Px_option_Send_out,      -- (2 downto 0);
	TCP_Px_Length_Send_out							    => TCP_Px_Length_Send_out,      -- (31 downto 0);
	TCP_Px_pckt_wo_dt									=> TCP_Px_pckt_wo_dt,           -- 
														 
	TCP_Px_request_took		   					    	=> TCP_Px_request_took,					-- The request is taken in account
	TCP_Px_request_done		   					    	=> TCP_Px_request_done 					-- End of packet transfer on the Ethernet side
 );	

UR_Part_selection_0_1					<= UR_Part_selection_0_1_cell;
--*********************************************************** 
-- generate ethernet packet (PING ARP TCP)	 
  		
Gen_eth_packt_i1:entity work.transf_pkt  
	port	map(
	CLOCK									=> tx_usr_clk					    , 
	RESET_p								    => Rst_tx_usr_clk_p			    	, 
	PORT_D								    => PORTx_D						    , 
	PORT_S								    => PORTx_S						    , 
	MAC_Gateway							    => x"000000000000"			        , 
	MAC_D									=> destination_MAC			        , 
	MAC_S									=> MAC_card					 		, 
	IP_D									=> IP_D								, 
	IP_S									=> IP_S 							, 
	IP_Server							    =>	x"00000000"					    , 
	IP_GATEWAY							    =>	IP_GATEWAY					    , 
	IP_NETWORK							    =>	IP_NETWORK   				    , 
	NETWORK								    =>	NETWORK				    , 
	IP_dst_and_IP_nwk_eq_Net_Dst	        =>	IP_dst_and_IP_nwk_eq_Net_Dst    , 
	SEQ_N									=>	TCP_Px_SEQ_Send_out				, 
	ACK_N									=>	TCP_Px_ACK_Send_out 			, 
																				-- Data signals for TCP packet
	DATA_pckt_ready					        => UR_blk_ready				        ,-- a packet is ready to send (fifo Length and CS is filled)
	PL_TX_lbus_A	   						=> UR_PL_TX_lbus_A					,
	PL_TX_lbus_B	   						=> UR_PL_TX_lbus_B					,
	PL_TX_lbus_C	   						=> UR_PL_TX_lbus_C					,
	PL_TX_lbus_D	 						=> UR_PL_TX_lbus_D					,
	DATA_pckt_len						    => UR_blk_len						,-- length of the packet to send
	DATA_pckt_CS						    => UR_blk_CS						,-- CheckSum + carry of the packet to send
	DATA_pckt_end						    => UR_blk_end						,-- last data of the packet	 
	RD_DATA_pckt						    => UR_rd_blk						,-- Read of the packet
	RD_CS_LEN							    => UR_ack_packet					,
	
	TCP_req								    => TCP_Px_request_TCP_packet 		,-- request a TCP packet --if DATA DATA_pckt_ready should be at '1'
 	TCP_wodt								=> TCP_Px_pckt_wo_dt				,-- TCP without data
	TCP_flags							    => TCP_Px_FLAG_Send_out				,-- TCP flags	
	TCP_opt								    => TCP_Px_option_Send_out			,
	TCP_len								    => TCP_Px_Length_Send_out			,-- Number of bytes request to transfert	
	TCP_max_size 						    => TCP_max_size					    ,	 
	TCP_scale							    => TCP_scale						,	 			 
	TCP_win								    => TCP_win							,	 			 
	TCP_TS								    => TCP_TS							,	 			 
	TCP_TS_rply							    => TCP_TS_rply						,	 			 
	TCP_request_took					    => TCP_Px_request_took              ,	
	TCP_request_done					    => TCP_Px_request_done              ,
	
	ARP_req								    =>	Req_acc_ARP				        ,-- request a ARP packet
	ARP_rply								=>	req_ARP_reply				    ,-- request a ARP reply if at '1' a ARP request if at '0'
	ARP_probe							    =>	ARP_probe       		 	    ,-- probe_ARP looking for a same IP addess
	ARP_Dest								=>	ARP_dst						    ,
	ARP_Gateway							    =>	ARP_gw						    ,
	MAC_D_ARP_reply					        =>	destination_MAC					, -- From the ARP ETH request , we should reply to 
	IP_D_ARP_reply						    =>	ARP_IP						    ,
	Idle_ARP                                =>  ARP_acc_done                    ,
	PING_req								=>	PING_req_resync			        ,
	ICMP_prbl_req						    =>	req_icmp_prb				    ,-- request q ICMP problem packet (PING with payload not aligned
	--PING_HD_emp							=>								    ,-- inform that the previous PING reply is sent out
	WR_DT_PING							    =>	wr_PING_data				    ,-- write & last_dt do a Ping reply request
	Last_Ping_DT						    =>	ping_last_dt				    ,
	Data_ping							    => data_ping				 	    , 
	Reset_PING_FIFO					        =>	Reset_PING_FIFO			        ,
	MAC_D_PING_reply					    =>	PING_MAC_dst				    , -- for Ping and ARP (same origin)
	no_transfer_ongoing						=>  no_transfer_ongoing				,
	IP_D_PING_reply					        =>	PING_IP_dst					    , 
	TX_lbus_a							    =>	TX_lbus_in(0)				    ,
	TX_lbus_b							    =>	TX_lbus_in(1)				    ,
	TX_lbus_c							    =>	TX_lbus_in(2)				    ,
	TX_lbus_d							    =>	TX_lbus_in(3)				    ,
	TX_lbus_wr							    =>	TX_lbus_wr					    ,
	TX_FIFO_Full						    =>	TX_FIFO_Full				    ,
	TX_pack_ready						    =>  end_of_eth_transfert
	--no_request_peding				        =>									,--: out std_logic
	);
	
TX_pack_ready	<= end_of_eth_transfert;

 
---**************************************************************
--  PING monitoring 	
process(Rst_tx_usr_clk_p,tx_usr_clk) 
begin
	if Rst_tx_usr_clk_p = '1' then
		Ping_time_ena				<= '0';      
		Ping_time_lathced <= (others => '0');   
	elsif rising_edge(tx_usr_clk) then
		if PING_req_resync = '1' then
			Ping_time_ena			<= '1';
		elsif received_Ping_reply = '1' then
			Ping_time_ena			<= '0';
			Ping_time_lathced 		<= Ping_time;
		end if;
	end if;
end process;	
	
process(tx_usr_clk) 
begin
	if rising_edge(tx_usr_clk) then	
		if received_Ping_reply = '1' or PING_req_resync = '1' then
			Ping_time		<= (others => '0'); 
		elsif Ping_time_ena = '1' then
			Ping_time		<= Ping_time + '1';				
		end if;
	end if;
end process;	


process(counter_rst_resync,tx_usr_clk)
begin
	if counter_rst_resync = '1' then
		ping_req_counter 		<= (others => '0');
		ping_rec_counter 		<= (others => '0');
	elsif rising_edge(tx_usr_clk) then
		if ping_req_resync = '1' then
			ping_req_counter 	<= ping_req_counter + '1';
		end if;
		if received_Ping_reply = '1' then
			ping_rec_counter 	<= ping_rec_counter + '1';
		end if;		
	end if;
end process;
 
--***************************************************************************
--**********************    Receiver decoder    *******************************
eth_decoder_i1:entity work.Eth_rcv_pckt  
port map
	(
	reset_in_p							    =>	Rst_tx_usr_clk_p			, -- sync to tx_usr_clk
	RX_lbus_in						        =>	rx_lbus						,
	rx_lbus_clk								=>  rx_lbus_clk					,
	
	MAC_home								=>	MAC_card					,
	IP_home								    =>	IP_S						,
	IP_Dest								    =>	IP_D						,	-- IP for the ARP request   SERVER IP (used for ARP request  IP Destination)
	PORT0_home							    =>	PORT0_S						,
	PORT1_home							    =>	PORT1_S						,
	PORT0_Host								=>	PORT0_D						,
	PORT1_Host								=>	PORT1_D						,
	IP_GateWay							    =>	x"00000000"					,	-- IP for the ARP request   GATEWAY IP (used for ARP request  IP gateway)
	request_ARP							    =>	Req_acc_ARP					,	-- The request will reset the counter for ARP probe
	ARP_MAC_Dest      				        =>	ARP_MAC_Dest      			,
	ARP_MAC_GateWay  					    =>	ARP_MAC_GateWay  			,
	ARP_check_MAC_src 				        =>	ARP_check_MAC_src 			,
	ARP_check_IP_src  				        =>	ARP_check_IP_src  			,
	ARP_MAC_error    					    =>	ARP_MAC_error    			,
	ARP_MAC_rcvOK 						    =>	ARP_MAC_rcvOK 				,
	ARP_MAC_gwOK  						    =>	ARP_MAC_gwOK  				,
	ARP_IP								    =>	ARP_IP						,
	ARP_MAC								    =>	destination_MAC				,
	req_ARP_reply 						    =>	req_ARP_reply 				,
	ARP_reply     						    =>	ARP_reply     				,
	valid_ARP_packet					    =>	valid_ARP_packet			,
	req_icmp_prb						    =>	req_icmp_prb				,
	PING_HD_empty						    =>	'0'							,
	ping_last_dt						    =>	ping_last_dt				,
	wr_PING_data						    =>	wr_PING_data				,
	data_ping							    =>	data_ping					,
	Reset_PING_FIFO					        =>	Reset_PING_FIFO				,
	PING_MAC_dst						    =>	PING_MAC_dst				,
	PING_IP_dst							    =>	PING_IP_dst					,
	received_Ping_reply				        =>	received_Ping_reply			,
	clock_out							    =>	tx_usr_clk					,
	PORT0_Open_connection					=> 	request_sync_con0(1)		,
	PORT1_Open_connection					=> 	request_sync_con1(1)		,
	PORTx_TCP_MSS_default					=> 	TCP_max_size				,
	-- PORTx_MAC_src							=> 							,
	-- PORTx_PORT_rcv							=> 							,
	PORTx_ACK_number						=> TCP_Px_ack_num_rcv			,
	PORTx_SEQ_number						=> TCP_Px_seq_num_rcv			,
	PORTx_TCP_flag							=> TCP_Px_flag_num_rcv			,
	PORTx_TCP_win							=> TCP_Px_win_num_rcv			,
	PORTx_TCP_len							=> TCP_Px_dt_len_rcv			,
	PORT0_MSS								=> TCP_P0_MSS_rcv				,
	PORT1_MSS								=> TCP_P1_MSS_rcv				,
	PORT0_latch_ack							=> TCP_P0_latch_ack				,
	PORT1_latch_ack							=> TCP_P1_latch_ack				,

	req_pause_frame							=>	req_pause_frame				,
	counter_pause_frame						=> 	counter_pause_frame			,
	delay_pause_frame						=> 	delay_pause_frame			
	 );	
 
--****************************************
-- Pause Frame

pause_i1:entity work.pause_frame  
	port map(
		resetp								=> Rst_tx_usr_clk_p,
		clock								=> tx_usr_clk,
		req_pause_frame						=> req_pause_frame,
		pause_quanta						=> delay_pause_frame,
		no_transfer_ongoing					=> no_transfer_ongoing,
		pause_transmition					=> wait_on_eth_transfert 
	 ); 
--****************************************	
 -- FIFO to record the ACk received
 FIFO_rcv_i0:tcp_ack_FIFO
	PORT map	(
		srst						=> Rst_tx_usr_clk_p		            , 
		clk							=> tx_usr_clk				        , 
		din(31 downto 0)			=> TCP_Px_ack_num_rcv				    , 
		din(63 downto 32)			=> TCP_Px_seq_num_rcv				    , 
		din(95 downto 64)			=> TCP_Px_win_num_rcv				    , 
		din(103 downto 96)			=> TCP_Px_flag_num_rcv			        , 
		din(119 downto 104)			=> TCP_Px_dt_len_rcv  			        , 
		din(127 downto 120) 		=> x"00"                            ,
		wr_en						=> TCP_P0_latch_ack				    , 
		--full						=> 		                            ,				- 
		rd_en						=> rd_TCP_P0_val_rcvo				, 
		empty						=> TCP_P0_Ack_present_n		        , -- '1' = empty
		dout(31 downto 0)			=> TCP_P0_ack_num_rcvo			    , 
		dout(63 downto 32)			=> TCP_P0_seq_num_rcvo			    , 
		dout(95 downto 64)			=> TCP_P0_win_num_rcvo			    ,
		dout(103 downto 96)			=> TCP_P0_flag_num_rcvo			    ,
		dout(119 downto 104)		=> TCP_P0_dt_len_rcvo				,
		dout(127 downto 120)		=> TCP_P0_no_used
	);
	 	
 TCP_P0_Ack_present	<= not(TCP_P0_Ack_present_n);
	    
	    
FIFO_rcv_i1:tcp_ack_FIFO
	PORT map	(
		srst						=> Rst_tx_usr_clk_p		            , 
		clk							=> tx_usr_clk				        , 
		din(31 downto 0)			=> TCP_Px_ack_num_rcv				    , 
		din(63 downto 32)			=> TCP_Px_seq_num_rcv				    , 
		din(95 downto 64)			=> TCP_Px_win_num_rcv				    , 
		din(103 downto 96)			=> TCP_Px_flag_num_rcv			        , 
		din(119 downto 104)			=> TCP_Px_dt_len_rcv  			        , 
		din(127 downto 120) 		=> x"00"                            ,
		wr_en						=> TCP_P1_latch_ack				    , 
		--full						=> 		                            ,				- 
		rd_en						=> rd_TCP_P1_val_rcvo				, 
		empty						=> TCP_P1_Ack_present_n		        , -- '1' = empty
		dout(31 downto 0)			=> TCP_P1_ack_num_rcvo			    , 
		dout(63 downto 32)			=> TCP_P1_seq_num_rcvo			    , 
		dout(95 downto 64)			=> TCP_P1_win_num_rcvo			    ,
		dout(103 downto 96)			=> TCP_P1_flag_num_rcvo			    ,
		dout(119 downto 104)		=> TCP_P1_dt_len_rcvo				,
		dout(127 downto 120)		=> TCP_P1_no_used
	);
	 	
 TCP_P1_Ack_present	<= not(TCP_P1_Ack_present_n);
	    
	  	  
--************************************************
 --settting of parameters
 PCI_setup_register_i1:entity work.PCIe_reg_eth  
 	generic map(addr_offset				=> addr_off_con0 + addr_offset_100G_eth,
 	            addr_offset1			=> addr_off_con1 + addr_offset_100G_eth)
	port map
	(
	CLOCK									=> usr_clk						,				 
	RESET									=> usr_rst_n					,				 
																			
	PCIe_DT								    => usr_data_wr					,				 
	PCIe_func							    => usr_func_wr              	,				  
	PCIe_wr								    => usr_wen						,	 			 
	PCIe_func_rd						    => usr_func_rd					, 
	PCIe_out								=> TCP_reg_out					,		 
											 
	PORT0_D								    => PORT0_D						,		 
	PORT0_S								    => PORT0_S						,		 
	PORT1_D								    => PORT1_D						,		 
	PORT1_S								    => PORT1_S						,		 
	MAC_S									=> MAC_S						,		 
	IP_D									=> IP_D							,		 
	IP_S									=> IP_S							,		 
	IP_GATEWAY							    => IP_GATEWAY					,		 
	IP_NETWORK							    => IP_NETWORK					,		 
	NETWORK								    => NETWORK						,		 
															
	TCP_max_size						    => TCP_max_size					,		 
	TCP_scale							    => TCP_scale					,		 
	TCP_win								    => TCP_win						,		 
	TCP_TS								    => TCP_TS						,		 
	TCP_TS_rply							    => TCP_TS_rply					,		 
	snd_cwnd								=> snd_cwnd						,		 
	load_new_snd_cwnd					    => load_new_snd_cwnd			,		 
	timer_rtt_I							    => timer_rtt_I					,		 
	timer_rtt_I_sync					    => timer_rtt_I_sync				,		 
	timer_persist_I					        => timer_persist_I				,		 
	TCP_Rexmt_CWND_sh					    => TCP_Rexmt_CWND_sh			,		 
	Init_bit								=> Init_bit						,		 
	thresold_retrans					    => thresold_retrans				, 		 
	DHCP_IP								    => x"00000000"					,		 
	DHCP_GateWay						    => x"00000000"					,		 
	DHCP_Network						    => x"00000000"					,		 
	DHCP_received						    => '0'							,		 
	MAC									    => MAC_card					 									 
	);	
	
 -- resync load pulse for load_new_snd_cwnd
 load_new_snd_cwnd_resync_pulse_i2_pci_xgmii:entity work.resync_pulse 
	port map(
		aresetn			=> usr_rst_n				,		 
		clocki			=> usr_clk					,		 
		clocko			=> tx_usr_clk				,		 
		in_s			=> load_new_snd_cwnd		,	 
		out_s			=> load_new_snd_cwnd_sync		 	 
		);	
 
 --************************************************
 -- status registers read from PCI_add
		
stats_i0:entity work.statistic_TCP  
generic map(addr_offset						=> addr_off_con0 + addr_offset_100G_eth)
 port map(
	reset									=> usr_rst_n					,				 
	local_reset							    => counter_rst_resync           ,	--pos 	,					 
	eth_clock							    => tx_usr_clk					,				 
	PCI_clock							    => usr_clk						,				 
	latch_value_to_be_rd				    => latch_for_mon				,				 
	ena_stat								=> TCP_P0_Stat_cnt_ena			,		 
	Stat_reg								=> TCP_P0_Stat_flags			,
	stat_send_byte						    => TCP_P0_Stat_snd_byte			,		  
	stat_snd_rexmit_byte				    => TCP_P0_stat_snd_rexmit_byte	,		  
	stat_dupack_max					        => TCP_P0_stat_dupack_max		,		  
	stat_win_max						    => TCP_P0_stat_wnd_max			,		  
	stat_win_min						    => TCP_P0_stat_wnd_min			,		  
	measure_RTT_max					        => TCP_P0_measure_RTT_max		,		  
	measure_RTT_min					        => TCP_P0_measure_RTT_min		,		  
	measure_RTT_sum					        => TCP_P0_Measure_RTT_Accu		,		  
	Current_WND							    => TCP_P0_win_num_rcvo			,		  
	RTT_Shift_MAX						    => TCP_P0_rtt_shifts_MAX		,
	RTT_Val								    => TCP_P0_rtt_v					,
	MSS_rcv(15 downto 0)				    => TCP_P0_MSS_rcv				,		  
	MSS_rcv(31 downto 16)			        => x"0000"						,	 				 
	ACK_rcv								    => '0'							,
	TCP_state								=> TCP_P0_state_o					,		 
	func									=> usr_func_rd					,				 
	dto_monitoring								    => TCP_P0_statistic_val(63 downto 0)					 	 
	);		

stats_i1:entity work.statistic_TCP  
generic map(addr_offset						=> addr_off_con1 + addr_offset_100G_eth)
 port map(
	reset									=> usr_rst_n					,				 
	local_reset							    => counter_rst_resync           ,	--pos 	,					 
	eth_clock							    => tx_usr_clk					,				 
	PCI_clock							    => usr_clk						,				 
	latch_value_to_be_rd				    => latch_for_mon				,				 
	ena_stat								=> TCP_P1_Stat_cnt_ena			,		 
	Stat_reg								=> TCP_P1_Stat_flags			,
	stat_send_byte						    => TCP_P1_Stat_snd_byte			,		  
	stat_snd_rexmit_byte				    => TCP_P1_stat_snd_rexmit_byte	,		  
	stat_dupack_max					        => TCP_P1_stat_dupack_max		,		  
	stat_win_max						    => TCP_P1_stat_wnd_max			,		  
	stat_win_min						    => TCP_P1_stat_wnd_min			,		  
	measure_RTT_max					        => TCP_P1_measure_RTT_max		,		  
	measure_RTT_min					        => TCP_P1_measure_RTT_min		,		  
	measure_RTT_sum					        => TCP_P1_Measure_RTT_Accu		,		  
	Current_WND							    => TCP_P1_win_num_rcvo			,		  
	RTT_Shift_MAX						    => TCP_P1_rtt_shifts_MAX		,
	RTT_Val								    => TCP_P1_rtt_v					,
	MSS_rcv(15 downto 0)				    => TCP_P1_MSS_rcv				,		  
	MSS_rcv(31 downto 16)			        => x"0000"						,	 				 
	ACK_rcv								    => '0'							,
	TCP_state								=> TCP_P1_state_o					,		 
	func									=> usr_func_rd					,				 
	dto_monitoring						    => TCP_P1_statistic_val(63 downto 0)					 	 
	);		
		 		 
-- ****************************************************
-- ARP probe logic


 -- resync ARP probe
Arp_probe_resync_pulse_i2_pci_clklow:entity work.resync_pulse 
	port map(
		aresetn		=> usr_rst_n					,		 
		clocki		=> usr_clk					,		 
		in_s		=> request_probe_arp(0)		,	 
		clocko		=> clock_low					,		 
		out_s		=> request_probe_arp(1)		 	 
		);			
 -- resync ARP request
Arp_req_resync_pulse_i2_pci_clklow:entity work.resync_pulse 
	port map(
		aresetn		=> usr_rst_n					,		 
		clocki		=> usr_clk					,		 
		in_s		=> request_arp_rg(0)				,	 
		clocko		=> clock_low					,		 
		out_s		=> request_arp_rg(1)		 	 
		);		 
		
 
process(reset_low_clock,clock_low)		
begin
	if reset_low_clock = '0' then
		request_arp_rg(2) 		<= '0';
		request_arp_rg(3) 		<= '0';
		
	elsif rising_edge(clock_low) then
		if ARP_Req_done = '1' then
			request_arp_rg(3) 		<= '0';
		elsif request_arp_rg(2) = '1' and Req_acc_ARP_sync = '0' then
			request_arp_rg(3) 		<= '1';
		end if;	
		
		if request_arp_rg(2) = '1' and Req_acc_ARP_sync = '0' then
			request_arp_rg(2) 		<= '0';
		elsif request_arp_rg(1) = '1' then
			request_arp_rg(2) 		<= '1';
		end if;
	end if;
end process;
	
ARP_probe_i1:entity work.probe_ARP
	port map (
	 Low_clk								   => clock_low, 
	 CLOCK_tx								   => tx_usr_clk,--  MAC 100Gb clock
	 RESET									   => Rst_tx_usr_clk_n,--: in std_LOGIC;				
	 probe_ARP_req							   => request_probe_arp(1),--: in std_logic;
	 req_ARP								   => request_arp_rg(3),--: in std_logic;
	 MAC_S									   => MAC_Card,--: in std_logic_vector(47 downto 0);
	 IP_Gateway								   => IP_GATEWAY ,--: in std_logic_vector(31 downto 0);
	 ARP_acc_done							   => ARP_acc_done,--: in std_logic;  -- ACTIVE LOW
	 IP_dst_and_IP_nwk_eq_Net_Dst		       => IP_dst_and_IP_nwk_eq_Net_Dst,--: in std_logic;
	 Req_acc_ARP							   => Req_acc_ARP,--: out std_logic;
	 ARP_gw									   => ARP_gw,--: out std_logic;
	 ARP_dst								   => ARP_dst,--: out std_logic;
	 ARP_probe								   => ARP_probe,--: out std_logic;
	 probe_DONE								   => probe_ARP_DONE ,--: out std_logic;
	 ARP_req_Done							   => ARP_Req_done,   -- ARP done
	 reset_low_clock						   => reset_low_clock -- resync reset internally
	 );		 
	 
sync_sig_i1:entity work.resync_sig_gen  
port map( 
	clocko				=> clock_low,
	in_s				=> Req_acc_ARP,
	out_s				=> Req_acc_ARP_sync
	);
	
--************************************************
-- control if the ARP request received a reply

ARP_reply_done(0)   <= '1' when ARP_reply = '1' and valid_ARP_packet = '1' else '0';

 -- resync ARP reply on our request
Arp_reply_resync_pulse:entity work.resync_pulse 
	port map(
		aresetn		=> '1'					,		 
		clocki		=> rx_lbus_clk				,		 
		in_s		=> ARP_reply_done(0)	,	 
		clocko		=> usr_clk				,		 
		out_s		=> ARP_reply_done(1)		 	 
		);		 

	
process(usr_rst_n,usr_clk)
begin
    if usr_rst_n = '0' then
        ARP_reply_rcv   <= '1';
    elsif rising_edge(usr_clk) then
        if  request_arp_rg(0) = '1' then
            ARP_reply_rcv <= '0';
        elsif ARP_reply_done(1) = '1' then
            ARP_reply_rcv <= '1';
        end if; 
    end if;
end process;

end Behavioral;

 
