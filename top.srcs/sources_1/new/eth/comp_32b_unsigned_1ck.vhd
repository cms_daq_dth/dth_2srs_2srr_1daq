----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 13.10.2017 07:40:52
-- Design Name: 
-- Module Name: comp_32b_unsigned_1ck - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity comp_32b_unsigned_1ck is
    Port ( 
    	aclr : in STD_LOGIC;
      clock : in STD_LOGIC;
      dataa : in STD_LOGIC_VECTOR (31 downto 0);
      datab : in STD_LOGIC_VECTOR (31 downto 0);
      aeb : out STD_LOGIC;
      agb : out STD_LOGIC
      );
end comp_32b_unsigned_1ck;

architecture Behavioral of comp_32b_unsigned_1ck is

signal aeb_reg			: std_logic;
signal agb_reg			: std_logic;

begin

process(aclr,clock)
begin
	if aclr = '1' then
		aeb_reg 	<= '0';
		agb_reg 	<= '0';
	elsif rising_edge(clock) then
		aeb_reg	 		<= '0';
		if dataa = datab then
			aeb_reg	 	<= '1';
		end if;

		agb_reg	 		<= '0';
		if dataa > datab then
			agb_reg	 	<= '1';
		end if;
	end if;
end process;

aeb	<= aeb_reg;
agb 	<= agb_reg;
end Behavioral;
