----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 01.09.2017 11:36:30
-- Design Name: Dominique Gigi
-- Module Name: buffer_to_TX_lbus - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.std_logic_unsigned.all;
use work.interface.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity buffer_to_TX_lbus is
  Port ( 
  reset_in_p						: in std_logic;
  clock_in							: in std_logic;
  
  TX_lbus_in						: in tx_user_ift;
  TX_lbus_wr						: in std_logic;
  TX_pckt_ready						: in std_logic;
  TX_FIFO_Full						: out std_logic;
  
  clock_out							: in std_logic;
  tx_lbus                    		: out tx_user_ift;
  TX_ready							: in std_logic
  
  );
end buffer_to_TX_lbus;

architecture Behavioral of buffer_to_TX_lbus is
attribute mark_debug : string;

COMPONENT resetp_resync is
port (
	aresetp				: in std_logic;
	clock				: in std_logic; 
	Resetp_sync			: out std_logic
	);
end COMPONENT;

signal rstp_clock_in                      : std_logic;
signal rstp_clock_out                    : std_logic;

--  First Word Fall through
COMPONENT Buffer_TX_serdes
  PORT (
    rst 				: IN STD_LOGIC;
    wr_clk 				: IN STD_LOGIC;
    rd_clk				: IN STD_LOGIC;
    din 				: IN STD_LOGIC_VECTOR(135 DOWNTO 0);
    wr_en 				: IN STD_LOGIC;
    rd_en 				: IN STD_LOGIC;
    dout 				: OUT STD_LOGIC_VECTOR(135 DOWNTO 0);
    full 				: OUT STD_LOGIC;
    empty 				: OUT STD_LOGIC;
    prog_full 			: OUT STD_LOGIC
  );
END COMPONENT;

COMPONENT request_packet
  PORT (
    rst 				: IN STD_LOGIC;
    wr_clk 				: IN STD_LOGIC;
    rd_clk 				: IN STD_LOGIC;
    din 				: IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    wr_en 				: IN STD_LOGIC;
    rd_en 				: IN STD_LOGIC;
    dout 				: OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    full 				: OUT STD_LOGIC;
    empty 				: OUT STD_LOGIC;
    rd_data_count 		: OUT STD_LOGIC_VECTOR(8 DOWNTO 0)
  );
END COMPONENT;

signal Full_buffer			: std_logic;
signal Full_indiv			: std_logic_vector(3 downto 0);

signal tx_lbus_cell			: tx_user_ift;
signal Empty_buffer			: std_logic;
signal Empty_indiv			: std_logic_vector(3 downto 0);
signal read_buffer_reg		: std_logic_vector(3 downto 0);
signal read_buffer_cell		: std_logic_vector(3 downto 0);
signal read_buffer_reg_a	: std_logic;
signal read_buffer_reg_b	: std_logic;
signal packet_ready			: std_logic;
signal ack_packet			: std_logic;
signal nb_pack_ready		: std_logic_vector(8 downto 0);

signal tx_lbus_rg			: tx_user_ift;
 
--attribute mark_debug of tx_lbus_rg				: signal is "true";
--attribute mark_debug of Full_buffer				: signal is "true";
--attribute mark_debug of packet_ready			: signal is "true";
--attribute mark_debug of ack_packet				: signal is "true";
--attribute mark_debug of read_buffer_reg_a		: signal is "true";
--attribute mark_debug of read_buffer_reg_b		: signal is "true";
--attribute mark_debug of read_buffer_reg			: signal is "true";
--attribute mark_debug of nb_pack_ready			: signal is "true";
--attribute mark_debug of Empty_indiv				: signal is "true";
--######################################################################
--                 Core start HERE
--######################################################################
begin

reset_resync_i1:resetp_resync  
port map(
	aresetp				=> reset_in_p,
	clock				=> clock_in, 
	Resetp_sync			=> rstp_clock_in 
	);
 

reset_resync_i2:resetp_resync  
port map(
	aresetp				=> reset_in_p,
	clock				=> clock_out, 
	Resetp_sync			=> rstp_clock_out
	);
 

-- record the data packet
Gen_buffer:FOR J in 0 to 3 generate
	i0:Buffer_TX_serdes--  First Word Fall through
	  PORT map(
		 rst 						=>	rstp_clock_in				,
		 wr_clk 					=>	clock_in				,
		 wr_en 						=>	TX_lbus_wr				,
		 din(127 downto 0) 			=>	TX_lbus_in(J).data_bus	,
		 din(128)               	=>	TX_lbus_in(J).sopckt  	,
		 din(129)               	=>	TX_lbus_in(J).data_ena	,
		 din(130)               	=>	TX_lbus_in(J).eopckt	,
		 din(131)               	=>	TX_lbus_in(J).err_pckt	,
		 din(135 downto 132)    	=>	TX_lbus_in(J).empty_pckt,
		 prog_full					=>	Full_indiv(J)			,
		 
		 rd_clk 					=>	clock_out				,
		 rd_en						=>	read_buffer_cell(J)		,
		 dout(127 downto 0) 		=>	tx_lbus_cell(J).data_bus,
		 dout(128)              	=>	tx_lbus_cell(J).sopckt  ,
		 dout(129)              	=>	tx_lbus_cell(J).data_ena,
		 dout(130)             	 	=>	tx_lbus_cell(J).eopckt	,
		 dout(131)              	=>	tx_lbus_cell(J).err_pckt,
		 dout(135 downto 132)   	=>	tx_lbus_cell(J).empty_pckt,
		 empty 						=>	Empty_indiv(J)			
	);
end generate;
 
 
 
req_I1:request_packet -- accumulate the packet reeady in BUFFER_TX_SERDES FIFO
  PORT map(
    rst 					=>	rstp_clock_in			,
    wr_clk        			=>	clock_in			,
    wr_en         			=>	TX_pckt_ready		,
    din(0)          		=>	'1'					,
    --full          			=>					,
    rd_clk        			=>	clock_out			,
    rd_en         			=>	ack_packet			,
    --dout          			=>					,
    empty         			=>	packet_ready		,
	rd_data_count 	 		=>  nb_pack_ready
  );


  
  -- control the FIFO ready
process(rstp_clock_out,clock_out)
begin
	if rstp_clock_out = '1' then
		read_buffer_reg	<= "0000";
	elsif rising_edge(clock_out) then
		if (tx_lbus_cell(0).eopckt = '1' or tx_lbus_cell(1).eopckt = '1' or tx_lbus_cell(2).eopckt = '1' or tx_lbus_cell(3).eopckt = '1') and read_buffer_reg(1) = '1' and TX_ready = '1' then  -- and (nb_pack_ready = "000000001")
			read_buffer_reg 	<= "0000";
		elsif packet_ready = '0' and Empty_indiv = "0000" then
			read_buffer_reg 	<= "1111";
		end if;
	end if;
end process;

ack_packet	<= '1' when (tx_lbus_cell(0).eopckt = '1' or tx_lbus_cell(1).eopckt = '1' or tx_lbus_cell(2).eopckt = '1' or tx_lbus_cell(3).eopckt = '1') and read_buffer_reg(0) = '1' and TX_ready = '1' else '0';


read_buffer_cell	<= "1111" when read_buffer_reg = "1111" and TX_ready = '1' else "0000";
 
  --transfert the data and control bits to the SERDES/MAC
Gen_i1:FOR I in 0 to 3 generate
	process(tx_lbus_cell,read_buffer_reg,TX_ready)
	begin
				tx_lbus_rg(I).data_bus     <= tx_lbus_cell(I).data_bus;
				tx_lbus_rg(I).err_pckt     <= tx_lbus_cell(I).err_pckt;
				tx_lbus_rg(I).empty_pckt   <= tx_lbus_cell(I).empty_pckt;
				
			if  TX_ready = '1' and read_buffer_reg(2) = '1' then
				tx_lbus_rg(I).sopckt       <= tx_lbus_cell(I).sopckt;
				tx_lbus_rg(I).eopckt       <= tx_lbus_cell(I).eopckt;
				tx_lbus_rg(I).data_ena     <= tx_lbus_cell(I).data_ena;
			else
				tx_lbus_rg(I).sopckt       <= '0';
				tx_lbus_rg(I).eopckt       <= '0';
				tx_lbus_rg(I).data_ena     <= '0';
			end if;
	
	end process;
end generate;

tx_lbus			<= tx_lbus_rg;


process(clock_in)
begin
	if rising_edge(clock_in) then
		Full_buffer	 		<= '0';
		if Full_indiv = "1111" then
			Full_buffer	 	<= '1'; -- all of them should full at the same time
		end if;
	end if;

end process;

TX_FIFO_Full	<= Full_buffer;

end Behavioral;
