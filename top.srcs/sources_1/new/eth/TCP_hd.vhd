------------------------------------------------------
-- this boxe prepare the IP header
--
-- Dominique Gigi
--
-- May 2012 ver 1.00
--
------------------------------------------------------
-- The TCP header is prepared during a block data flow and at the end of the block
-- the length of the TCP clock is computed accordign the options added and the length of the block data.
-- The standard TCP header is 20 bytes + 2 bytes( no standard) used to align the data on 64-bit words
--
-- The block data is composed by a 64 bit  word(reserved bit + 16(bit)Event payload length )+ valid data (X x 64 bit )
-- This block is filled by dummy data to be 8 Kbytes length (???)
--
-- If the block has not data and no option , 4 bytes should be added to have 64 bytes length Ethernet packet length
-------------------------------------------------------
library ieee;
 
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use work.interface.all;
  
entity TCP_HD is
	port (
		-- INPUT
		RESET_n			    : IN STD_LOGIC;
		CLOCK				: IN STD_LOGIC;
			
		PORT_S			    : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
		PORT_D			    : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
		SEQ_nb			    : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		ACK_nb			    : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		TCP_wodt			: IN STD_LOGIC;
		FLAGS				: IN STD_LOGIC_VECTOR(7 DOWNTO 0);	 
		Window			    : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
			-- data for pseudo header
		IP_S				: IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		IP_D				: IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		--protocol          : IN STD_LOGIC_VECTOR(7 DOWNTO 0); - should be 0x06 TCP in IP
		length_dm		    : IN STD_LOGIC_VECTOR(15 DOWNTO 0); -- fragment data payload length in 64-bit words
			-- data for options
		option			    : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
		MAX_len			    : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
		scale_w			    : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
		Time_stamp		    : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		TS_ACK			    : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
			
			
		PREP				: IN STD_LOGIC_VECTOR(14 DOWNTO 0);
		DATA_CS			    : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
 			
		-- OUTPUT	
			
		IP_TX_lbus_c	    : out tx_usr_record;
		IP_TX_lbus_d	    : out tx_usr_record;
		
		TCP_OPEN_RST	    : OUT STD_LOGIC; -- it is a TCP packet without data (OPEN , RESET,...))
		TCP_OPTION		    : OUT STD_LOGIC_VECTOR(2 DOWNTO 0) 
	);
end TCP_HD;

architecture core of TCP_HD is

COMPONENT CS_accumulator_64b
  PORT (
    B 		: IN STD_LOGIC_VECTOR(63 DOWNTO 0);
    CLK 	: IN STD_LOGIC;
    CE 		: IN STD_LOGIC;
    SCLR 	: IN STD_LOGIC;
    Q 		: OUT STD_LOGIC_VECTOR(64 DOWNTO 0)
  );
END COMPONENT;

COMPONENT CS_IP_64b
  PORT (
    A       : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
    B       : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
    CLK     : IN STD_LOGIC;
    C_IN    : IN STD_LOGIC;
    CE      : IN STD_LOGIC;
    SCLR    : IN STD_LOGIC;
    C_OUT   : OUT STD_LOGIC;
    S       : OUT STD_LOGIC_VECTOR(63 DOWNTO 0)
  );
END COMPONENT;

COMPONENT CS_IP_32b
  PORT (
    A       : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    B       : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    CLK     : IN STD_LOGIC;
    C_IN    : IN STD_LOGIC;
    CE      : IN STD_LOGIC;
    SCLR    : IN STD_LOGIC;
    C_OUT   : OUT STD_LOGIC;
    S       : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
  );
END COMPONENT;

COMPONENT CS_IP_16b
  PORT (
    A       : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    B       : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    CLK     : IN STD_LOGIC;
    C_IN    : IN STD_LOGIC;
    CE      : IN STD_LOGIC;
    SCLR    : IN STD_LOGIC;
    C_OUT   : OUT STD_LOGIC;
    S       : OUT STD_LOGIC_VECTOR(15 DOWNTO 0)
  );
END COMPONENT;

COMPONENT ip_length
  PORT (
    A       : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    B       : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    CLK     : IN STD_LOGIC;
    CE      : IN STD_LOGIC;
    S       : OUT STD_LOGIC_VECTOR(15 DOWNTO 0)
  );
END COMPONENT;

signal accu_reset					: std_logic;
signal accu_ena					    : std_logic;
signal mux_checksum				    : std_logic_vector(63 downto 0);
signal CS_acc						: std_logic_vector(64 downto 0);
signal CS_S20               	    : std_logic_vector(31 downto 0);
signal CS_S30               	    : std_logic_vector(15 downto 0);
signal CS_S40               	    : std_logic_vector(15 downto 0);

signal CS_carry             	    : std_logic_vector(31 downto 0);

signal wr_ff						: std_logic;
signal mux							: std_logic_vector(63 downto 0);
signal offset						: std_logic_vector(3 downto 0);
signal TCP_tot_l					: std_logic_vector(15 downto 0);
signal TCP_TOT						: std_logic_vector(15 downto 0);
signal almost_f					    : std_logic_vector( 5 downto 0);
signal TCP_hd_l					    : std_logic_vector(15 downto 0);
 
signal Con_ctrl					    : std_logic; --connection ctrl packet
signal mem_opt						: std_logic_vector(2 downto 0);
 
signal length_data_payload		    : std_logic_vector(15 downto 0);

signal checksum					    : std_logic_vector(15 downto 0);
signal checksum_n 				    : std_logic_vector(15 downto 0);

attribute mark_debug : string;
--attribute mark_debug of TCP_tot_l: signal is "true";

--*****************************************************************************************
--**************************  BEGINNING  **************************************************
--*****************************************************************************************
begin

process(reset_n,clock)
begin
if reset_n = '0'  then
	Con_ctrl 				<= '0';
	mem_opt 				<= "000";
elsif rising_edge(clock) then
	if PREP(0) = '1'  then
		mem_opt 			<= option;
		if TCP_wodt ='1' then
			Con_ctrl 		<= '1';
		else 
			Con_ctrl 		<= '0';
		end if;
	end if;
end if;
end process;

-- option 64-bit word
--  TCP(63..48)		TCP(47..32)		TCP(31..16)		TCP(15..0)
--		TCP				TCP				TCP			Option1
--	Option1			Option2			Option2			Option3
-- 	Option3			Option3			Option3			Option3
--	NOP				NOP				NOP				Start with data
process(TCP_wodt,FLAGS)
begin
	if    TCP_wodt = '1' and FLAGS(1) = '1' then
		TCP_hd_l	<= x"001C"; -- 0x14  +  OPTION '1-2'(2x 32-bit)	 NOP code to replace the missing option (if at leadt one)
		offset		<= x"7";	
	else
		TCP_hd_l	<= x"0014"; -- 0x14   
		offset		<= x"5";
	end if;
end process;

length_data_payload	<= (others => '0') when TCP_wodt = '1'  else length_dm(15 downto 0);

LENGTH_TCP:ip_length
  PORT map(
	A(15 downto 0) 	=> length_data_payload	,
	B       		=> TCP_hd_l         	,
	CLK     		=> CLOCK         	 	,
	CE      		=> '1'					,--PREP(0) 
	S       		=> TCP_tot_l            
  ); 
 
TCP_OPEN_RST	<= Con_ctrl;
TCP_OPTION		<= mem_opt;
 
-----------------------------------------------------			
-- MUX to compute the Checksum : DATA and ENA
-----------------------------------------------------
--!!!!!! mixed value to calculate the CS and have a Carry only and the last addition, if there is!
process(clock)
begin
	if rising_edge(clock) then
		mux_checksum								<= (OTHERS => '0');
		accu_ena									<= '0';
		if 	PREP(0) = '1' then 
			accu_ena								<= '1';
			mux_checksum(63 downto 48)				<= (others => '0');
			mux_checksum(47 downto 32)				<= DATA_CS;
			if TCP_wodt = '1' then		
				mux_checksum(47 downto 32)			<= (others => '0');
			end if;		
			mux_checksum(31 downto  0)				<= IP_S;------------------------------------------------ pseudo TCP HEADER
		elsif PREP(1) = '1' then 		
			accu_ena										<= '1';
			mux_checksum(63 downto 48)				<= x"0006";---------------------- Protocol (Pseudo TCP header)   x"0000"---------------URGENT POINTER
			mux_checksum(47 downto 32)				<= IP_D(15 downto 0);
			mux_checksum(31 downto 16)				<= PORT_S;------------------------PORT S
			mux_checksum(15 downto  0)				<= PORT_D;------------------------PORT D
		elsif PREP(2) = '1' then 			
			accu_ena										<= '1';
			mux_checksum(63 downto 60)				<= offset;------------------------OFFSET-RSVD-FLAGS			
			mux_checksum(59 downto 56)				<= (others => '0');			
			mux_checksum(55 downto 48)				<= FLAGS;			
			mux_checksum(47 downto 32)				<= TCP_tot_l;-------------------- TCP length	 (Pseudo TCP header)
			mux_checksum(31 downto  0)				<= ACK_nb;------------------------ACK NM(31..00)			
		elsif PREP(3) = '1' then		
			accu_ena								<= '1';
			mux_checksum(63 downto 32)				<= SEQ_nb;------------------------SEQ NM 
			mux_checksum(31 downto 16)				<= IP_D(31 downto 16);
			mux_checksum(15 downto  0)				<= Window;------------------------WINDOW
		elsif PREP(4) = '1' and FLAGS(1) = '1' then		-- only on TCP SYNC (open connection)
			accu_ena								<= '1';
			if TCP_wodt = '1' then
				if mem_opt(0) = '1' then-----------------------------OPTION OR PAYLOAD (15..0)
					mux_checksum(63 downto 48)		<= x"0204";
					mux_checksum(47 downto 32)		<= MAX_len;
				else
					mux_checksum(63 downto 32)		<= x"01010101";
				end if;		
				if mem_opt(1) = '1' then
					mux_checksum(031 downto 016)	<= x"0303";
					mux_checksum(015 downto 008)	<= scale_w;
					mux_checksum(007 downto 000)	<= x"00";
				else
					mux_checksum(31 downto  0)		<= x"01010100";
				end if;
			end if;
		end if;
	end if;
end process;

accu_reset 	<= PREP(11) OR NOT(reset_n);

Checksum_i1:CS_accumulator_64b
  PORT MAP(
    B 		=>	mux_checksum		, 
    CLK 	=>	clock				, 
    CE 		=>	accu_ena			, 
    SCLR 	=>	accu_reset			, 
    Q 		=>	CS_acc				  
  );

------------------------------------------------Level III (prep(6))
CS_i30:CS_IP_32b
PORT map(
    A               => CS_acc(31 downto 0), 
    B               => CS_acc(63 downto 32), 
    CLK             => CLOCK               , 
    C_IN            => CS_acc(64)          , 
    CE              => '1'                 , 
    SCLR            => '0'                 , 
    C_OUT           => CS_carry(2)         , 
    S               => CS_S20                
); 
 
--------------------------------------------------Level IV (prep(7))
CS_i40:CS_IP_16b
 PORT map(
   A                => CS_S20(15 downto 0) 	,
   B                => CS_S20(31 downto 16)	,
   CLK              => CLOCK               	,
   C_IN             => CS_carry(2)         	,
   CE               => '1'                	,
   SCLR             => '0'                	,
   C_OUT            => CS_carry(3)        	,
   S                => CS_S30                   
 );
 
---------------------------------------------------Level V (prep(8))
CS_i50:CS_IP_16b
 PORT map(
   A                => CS_S30(15 downto 0) ,
   B                => x"0000"             ,
   CLK              => CLOCK               ,
   C_IN             => CS_carry(3)         ,
   CE               => '1'                 ,
   SCLR             => '0'                 ,
--   C_OUT            => CS_carry(3)       ,
   S                => checksum_n                    
 ); 
 
checksum <= not(checksum_n); 
---------------------------------------------
-- DATA to write to FIFO	: DATA and ENA
---------------------------------------------

IP_TX_lbus_c.data_bus(111 downto 96)	<= PORT_S;
IP_TX_lbus_c.data_bus( 95 downto 80	)	<= PORT_D;
IP_TX_lbus_c.data_bus( 79 downto 48	)	<= SEQ_nb;
IP_TX_lbus_c.data_bus( 47 downto 16	)	<= ACK_nb;
IP_TX_lbus_c.data_bus( 15 downto 12	)	<= offset;
IP_TX_lbus_c.data_bus( 11 downto  8	)	<= (others => '0');
IP_TX_lbus_c.data_bus(  7 downto  0	)	<= FLAGS;
IP_TX_lbus_c.data_ena					<= '1';
IP_TX_lbus_c.sopckt						<= '0';
IP_TX_lbus_c.eopckt						<= '0';
IP_TX_lbus_c.err_pckt					<= '0';
IP_TX_lbus_c.empty_pckt					<= x"0";


IP_TX_lbus_d.data_bus(127 downto 112)	<= Window;
IP_TX_lbus_d.data_bus(111 downto  96)	<= checksum;
IP_TX_lbus_d.data_bus( 95 downto  80)	<= (others => '0'); -- URGENT pointer

process(mem_opt,MAX_len,scale_w,FLAGS(1))
	begin
		if FLAGS(1) = '1' then
			if 	mem_opt(0) = '1' then							-- If yes option 0
				IP_TX_lbus_d.data_bus( 79 downto  64)	<= x"0204";
				IP_TX_lbus_d.data_bus( 63 downto  48)	<= MAX_len;
			else
				IP_TX_lbus_d.data_bus( 79 downto  48)	<= x"01010101";
			end if;
				
			if mem_opt(1) = '1' then							-- If no option 0 and yes option 1
				IP_TX_lbus_d.data_bus( 47 downto  32)	<= x"0303";
				IP_TX_lbus_d.data_bus( 31 downto  24)	<= scale_w;
				IP_TX_lbus_d.data_bus( 23 downto  16)	<= x"00";
			else
				IP_TX_lbus_d.data_bus( 47 downto  16)	<= x"01010100"; -- start data
			end if;
			IP_TX_lbus_d.data_bus( 15 downto  0)		<= (others => '0'); 
		else
			IP_TX_lbus_d.data_bus( 79 downto  0)		<= (others => '0'); 			
		end if;	
end process;

IP_TX_lbus_d.data_ena					<= '1';
IP_TX_lbus_d.sopckt						<= '0';
IP_TX_lbus_d.eopckt						<= '1';
IP_TX_lbus_d.err_pckt					<= '0';

process(TCP_wodt,FLAGS)
begin
	if 	TCP_wodt = '1' and FLAGS(1) = '1' then
		IP_TX_lbus_d.empty_pckt			<= x"2";	
	else
		IP_TX_lbus_d.empty_pckt			<= x"4";	-- PAD packet with 000
	end if;
end process;

end core;