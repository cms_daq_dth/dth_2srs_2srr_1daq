------------------------------------------------------------------------------
--                      Headers construction                               --
------------------------------------------------------------------------------
--  SM flow to send a packet to ethernet		 
--   
--   Dominique Gigi 2010		 
--								 
--   ver 1.00							 
--  
-- 
-- 
--
--
--
------------------------------------------------------------------------------
LIBRARY ieee;
 
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;
 


	
entity Build_hd is
	port 	(
	RESET_n					        : IN  STD_LOGIC;
	CLOCK						    : IN  STD_LOGIC;
	ARP_req					        : IN  STD_LOGIC;
	TCP_req					        : IN  STD_LOGIC;
	TCP_opt					        : IN  STD_LOGIC_VECTOR(2 downto 0);
	Data_rdy					    : IN  STD_LOGIC;
	LAST_data					    : IN  STD_LOGIC;
	TCP_wo_DT				        : IN  STD_LOGIC;
	PING_req					    : IN  STD_LOGIC;
	END_PING					    : IN  STD_LOGIC;
	ICMP_prbl_pckt			        : IN  STD_LOGIC;

	ETH_mux					        : OUT STD_LOGIC;
	IP_mux					        : OUT STD_LOGIC;
	IP_PING_mux						: OUT STD_LOGIC;
	IP_prep					        : OUT STD_LOGIC_VECTOR(6 downto 0);
	ARP_mux					        : OUT STD_LOGIC;
	TCP_mux					        : OUT STD_LOGIC;
	IP_TCP_mux						: OUT STD_LOGIC;
	TCP_prep					    : OUT STD_LOGIC_VECTOR(14 downto 0);
	PING_mux					    : OUT STD_LOGIC;
	PING_prep				        : OUT STD_LOGIC_VECTOR(3 downto 0);
	read_PayLoad					: OUT STD_LOGIC; 
	Payload_mux					    : OUT STD_LOGIC;
	End_build_hd			        : OUT STD_LOGIC;
	TCP_done				        : OUT STD_LOGIC;
	no_transfer_ongoing				: out std_logic
	 
	);
end Build_hd;

architecture behavioral of Build_hd is

 	-- State Machine
type ETH_SM_type is (idle,
					Pckt_prep,-- compute the length as well
					TCP_IP_a,
					TCP_IP_b,
					TCP_IP_c,
					TCP_IP_d,
					TCP_IP_e,
					TCP_IP_f,
					IP_a,
					IP_b,
					IP_c,
					IP_d,
					IP_e,
					IP_f,
					TCP_a,
					TCP_b,
					TCP_c,
					TCP_d,
					TCP_e,
					TCP_f,
					TCP_g,
					TCP_h,
					TCP_i,
					TCP_PL,
					ARP,
					PING,
					end_Build
					);
signal ETH_SM:ETH_SM_type;

signal no_transfer_ongoing_cell	: std_logic; 
  
signal TCP_done_rg			: std_logic;
 	
 attribute mark_debug : string;
 attribute mark_debug of ETH_SM: signal is "true";
 
--***************************************************************************
--************************<<        BEGIN     >>*****************************
--***************************************************************************
begin
 

--////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
--||||||||||| STATE MACHINE TO BUILD ETHERNET HEADER |||||||||||||||
--\\\\\\\\\\\\\\\\\\\\\\\\\\\\//////////////////////////////////////
work_structure:process(clock,RESET_n)
begin
	if RESET_n = '0'  then
		ETH_SM <= idle;
	elsif rising_edge(clock) then
		Case ETH_SM is
			when idle      =>
				-- we start to buid header when TCP  (with or without data    or     ARP      or PING    and not full)
				if (((TCP_req = '1' and (Data_rdy = '1' or TCP_wo_DT = '1')) or ARP_req = '1' or PING_req = '1' )  ) then  
					ETH_SM <= Pckt_prep;
				end if;
				
			when Pckt_prep =>
				if TCP_req = '1' then
					ETH_SM 	<= TCP_IP_a;
				elsif PING_req = '1' then
					ETH_SM 	<= IP_a;
				else --if ARP_req = '1' then
					ETH_SM	<= ARP;
				end if;
				
			when TCP_IP_a  =>
				ETH_SM <= TCP_IP_b;
				
			when TCP_IP_b  =>
				ETH_SM <= TCP_IP_c;
				
			when TCP_IP_c  =>
				ETH_SM <= TCP_IP_d;
				
			when TCP_IP_d  =>
				ETH_SM <= TCP_IP_e;

			when TCP_IP_e  =>
				ETH_SM <= TCP_IP_f;
				
			when TCP_IP_f  =>
				if TCP_req = '1' then
					ETH_SM <= TCP_a;
				else
					ETH_SM <= end_Build;
				end if;
				
			when IP_a      =>
				ETH_SM <= IP_b;
				
			when IP_b      =>
				ETH_SM <= IP_c;
				
			when IP_c      =>
				ETH_SM <= IP_d;
				
			when IP_d      =>
				ETH_SM <= IP_e;
				
			when IP_e      =>
				if 	PING_req = '1' then
					ETH_SM <= PING;
				else
					ETH_SM <= end_Build;
				end if;
				
			when TCP_a     =>
				ETH_SM <= TCP_b;
			when TCP_b     =>
				ETH_SM <= TCP_c;
			when TCP_c     =>
				ETH_SM <= TCP_d;
			when TCP_d     =>
				ETH_SM <= TCP_e;
			when TCP_e     =>
				ETH_SM <= TCP_f;
			when TCP_f     =>
				if   TCP_wo_DT = '1' then
					ETH_SM <= end_Build;
				else
					ETH_SM <= TCP_PL;
				end if;
			when TCP_PL    =>
				if LAST_data = '1' then
					ETH_SM <= end_Build;
				end if;
 

			when PING      =>
				if END_PING = '1' then
					ETH_SM <= end_Build;
				end if;
				
			when ARP       =>
				ETH_SM <= end_Build;
				
			when end_Build =>	
				ETH_SM <= idle;

			when others    =>
				ETH_SM <= idle;
				
		end case;
	end if;
end process;
 
End_build_hd		<= '1' when ETH_SM = end_Build  else '0';
 
PING_prep(0)		<= '1' when ETH_SM = IP_b  else '0'; 
PING_prep(3 downto 1) <= "000"; 

IP_prep(0)			<= '1' when ETH_SM = IP_a  or ETH_SM = TCP_IP_a	      else '0'; 
IP_prep(1)			<= '1' when ETH_SM = IP_b  or ETH_SM = TCP_IP_b	      else '0'; 
IP_prep(2)			<= '1' when ETH_SM = IP_c  or ETH_SM = TCP_IP_c	      else '0'; 
IP_prep(3)			<= '1' when ETH_SM = IP_d  or ETH_SM = TCP_IP_d	      else '0'; 
IP_prep(4)			<= '1' when ETH_SM = IP_e  or ETH_SM = TCP_IP_f	      else '0'; 
IP_prep(5)			<= '1' when  				  ETH_SM = TCP_IP_f		  else '0'; 
IP_prep(6)			<= '1' when ETH_SM = end_Build                        else '0'; 

TCP_prep(0)			<= '1' when ETH_SM = TCP_IP_a                         else '0'; 
TCP_prep(1)			<= '1' when ETH_SM = TCP_IP_b                         else '0'; 
TCP_prep(2)			<= '1' when ETH_SM = TCP_IP_c                         else '0'; 
TCP_prep(3)			<= '1' when ETH_SM = TCP_IP_d                         else '0'; 
TCP_prep(4)			<= '1' when ETH_SM = TCP_IP_e                         else '0';
TCP_prep(5)			<= '1' when ETH_SM = TCP_IP_f                         else '0';
TCP_prep(6)			<= '1' when ETH_SM = TCP_a                            else '0';
TCP_prep(7)			<= '1' when ETH_SM = TCP_b                            else '0';
TCP_prep(8)			<= '1' when ETH_SM = TCP_c                            else '0';
TCP_prep(9)			<= '1' when ETH_SM = TCP_d                            else '0';
TCP_prep(10)		<= '1' when ETH_SM = TCP_e                            else '0';
TCP_prep(11)		<= '1' when ETH_SM = TCP_f                            else '0';
TCP_prep(14 downto 12)		<= (others => '0'); 
 

ETH_mux				<= '1' when ETH_SM = Pckt_prep                  else '0'; 
IP_mux				<= '1' when ETH_SM = IP_e or ETH_SM = TCP_IP_f  else '0'; 
ARP_mux				<= '1' when ETH_SM = ARP                        else '0'; 

IP_TCP_mux			<= '1' when ETH_SM = TCP_IP_f                   else '0';  
TCP_mux				<= '1' when ETH_SM = TCP_f                      else '0';  
Payload_mux			<= '1' when ETH_SM = TCP_PL                     else '0'; 
read_PayLoad		<= '1' when (ETH_SM = TCP_PL or ETH_SM = TCP_f or  ETH_SM = TCP_e)  else '0'; 

PING_mux			<= '1' when ETH_SM = PING                       else '0'; 
IP_PING_mux			<= '1' when ETH_SM = IP_e      				    else '0'; 

process(RESET_n,clock) 
begin
	if RESET_n = '0'then
		TCP_done_rg	<= '0';
	elsif rising_edge(clock) then
		TCP_done_rg	<= '0';
		if (ETH_SM = TCP_PL and LAST_data = '1') or (ETH_SM = TCP_f and  TCP_wo_DT = '1') then
			TCP_done_rg	<= '1';
		end if;
	end if;
end process;

TCP_done					<= TCP_done_rg;

no_transfer_ongoing_cell 	<= '1' when ETH_SM = Idle else '0';

no_transfer_ongoing			<= no_transfer_ongoing_cell; 

end behavioral;
 