----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07.06.2017 16:15:29
-- Design Name: 
-- Module Name: I2C_Slave - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:  The PCIe and DRP interface used the same clock (no need to resync)
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;



entity drp_itf_same_clock is
	generic (
		add_offset			: integer := 0000 ;
		data_offset			: integer := 0001 ;
		ctrl_offset			: integer := 0002 ;
		status_offset		: integer := 0003 
	);
    Port ( 
		usr_clk					: in std_logic;
		usr_rst_n				: in std_logic;
		usr_func_wr				: in std_logic_vector(16383 downto 0); 
		usr_wen					: in std_logic;
		usr_data_wr				: in std_logic_vector(63 downto 0);
		
		usr_func_rd				: in std_logic_vector(16383 downto 0); 
		usr_data_rd				: out std_logic_vector(63 downto 0) := (others => '0');

		di_in               	: out STD_LOGIC_VECTOR(15 DOWNTO 0);
		daddr_in            	: out STD_LOGIC_VECTOR(7 DOWNTO 0);
		den_in              	: out STD_LOGIC;
		dwe_in              	: out STD_LOGIC;
		do_out              	: in STD_LOGIC_VECTOR(15 DOWNTO 0);
		drdy_out            	: in STD_LOGIC

           );
end drp_itf_same_clock;

architecture Behavioral of drp_itf_same_clock is

--attribute mark_debug : string;
 
--attribute mark_debug of byte_mgnt: signal is "true";
 

signal data_wr_reg					    : std_logic_vector(31 downto 0);
signal add_reg						    : std_logic_vector(31 downto 0);
signal data_rd_reg						: std_logic_vector(31 downto 0);
signal data_latched						: std_logic_vector(31 downto 0) := x"00000000";
signal drp_status						: std_logic_vector(31 downto 0) := x"00000000";

signal write_reg						: std_logic;
signal drp_ena_reg						: std_logic;
signal data_ready						: std_logic;


 
--*********************************************************************************************
--***********************      Code start HERE              ***********************************
--*********************************************************************************************
begin

--########################################
--PCIe write access
process(usr_clk)
begin
	if rising_edge(usr_clk) then
		 if usr_wen = '1' then
			if  usr_func_wr(data_offset) = '1' then
				data_wr_reg		<= 	usr_data_wr(31 downto 0);	
			end if;
			
			if  usr_func_wr(add_offset) = '1'  then
				add_reg			<= 	usr_data_wr(31 downto 0);	
			end if;
 		end if;
	end if;
end process;


--#######################################
-- access to DRP bus
process(usr_clk,usr_rst_n)
begin
	if usr_rst_n = '0' then
		write_reg	<= '0';
		drp_ena_reg	<= '0';
		data_ready	<= '0';
	elsif rising_edge(usr_clk) then
		write_reg	<= '0';
		drp_ena_reg	<= '0';
		if  usr_func_wr(ctrl_offset) = '1' and usr_wen = '1' then
			-- only write access
			if usr_data_wr(0) = '1' then  
				write_reg <= '1';
			end if;
			-- drpena on Read and Write access
			if usr_data_wr(1) = '1' or usr_data_wr(0) = '1' then
				drp_ena_reg <= '1';
			end if;
		end if;
		 
		 
		if drdy_out = '1'  then
			data_ready	<= '1';
		elsif  usr_func_wr(ctrl_offset) = '1' and usr_data_wr(0) = '1' and usr_wen = '1' then
			data_ready	<= '0';
		end if;	
	end if;
end process;

process(usr_clk)
begin
	if rising_edge(usr_clk) then
		if drdy_out = '1' then
			data_latched(15 downto 0)	<= do_out;
		end if;
	end if;
end process;

di_in             			<= data_wr_reg(15 downto 0); 	 
daddr_in          			<= add_reg(7 downto 0);	 
den_in            			<= drp_ena_reg;
dwe_in           			<= write_reg;

drp_status(31)				<= data_ready;
drp_status(30 downto 0)		<= (others => '0');
--########################################
--PCIe read access
process(usr_clk)
begin
	if rising_edge(usr_clk) then
		data_rd_reg				<= (others => '0');
 
			if  	usr_func_rd(status_offset) = '1' then
				data_rd_reg		<= 	drp_status;	
			elsif 	usr_func_rd(data_offset) = '1' then
				data_rd_reg 	<= 	data_latched;	
			elsif	usr_func_rd(add_offset) = '1' then
				data_rd_reg 	<= 	add_reg;	
			end if;
 
	end if;
end process;

usr_data_rd(31 downto 0)	<= data_rd_reg;

end Behavioral;