
LIBRARY ieee;

USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use work.address_table.all;
 
--ENTITY-----------------------------------------------------------
entity Sysmon is

	port
	(
		usr_clk					: in std_logic;
		usr_rst_n				: in std_logic;
		usr_func_wr				: in std_logic_vector(16383 downto 0); 
		usr_wen					: in std_logic;
		usr_data_wr				: in std_logic_vector(63 downto 0);
		usr_func_rd				: in std_logic_vector(16383 downto 0); 
		usr_data_rd				: out std_logic_vector(63 downto 0) := (others => '0');
		
		i2c_sda 				: INOUT STD_LOGIC;
		i2c_sclk 				: INOUT STD_LOGIC
	 );
end Sysmon;

--ARCHITECTURE------------------------------------------------------
architecture behavioral of Sysmon is

COMPONENT system_management_wiz_0
  PORT (
    di_in               : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    daddr_in            : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    den_in              : IN STD_LOGIC;
    dwe_in              : IN STD_LOGIC;
    drdy_out            : OUT STD_LOGIC;
    do_out              : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    dclk_in             : IN STD_LOGIC;
    reset_in            : IN STD_LOGIC;
    user_temp_alarm_out : OUT STD_LOGIC;
    vccint_alarm_out    : OUT STD_LOGIC;
    vccaux_alarm_out    : OUT STD_LOGIC;
    ot_out              : OUT STD_LOGIC;
    channel_out         : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    eoc_out             : OUT STD_LOGIC;
    alarm_out           : OUT STD_LOGIC;
    eos_out             : OUT STD_LOGIC;
    busy_out            : OUT STD_LOGIC;
    i2c_sda             : INOUT STD_LOGIC;
    i2c_sclk            : INOUT STD_LOGIC
  );
END COMPONENT;

signal di_in      			: STD_LOGIC_VECTOR(15 DOWNTO 0);
signal daddr_in   			: STD_LOGIC_VECTOR(7 DOWNTO 0);
signal den_in     			: STD_LOGIC;
signal dwe_in     			: STD_LOGIC;
signal drdy_out   			: STD_LOGIC;
signal do_out     			: STD_LOGIC_VECTOR(15 DOWNTO 0);

signal user_temp_alarm_out 	: STD_LOGIC;
signal vccint_alarm_out    	: STD_LOGIC;
signal vccaux_alarm_out    	: STD_LOGIC;
signal ot_out              	: STD_LOGIC;
signal channel_out         	: STD_LOGIC_VECTOR(5 DOWNTO 0); 
signal eoc_out             	: STD_LOGIC;
signal alarm_out           	: STD_LOGIC;
signal eos_out             	: STD_LOGIC;
signal busy_out            	: STD_LOGIC;
 
COMPONENT drp_itf_same_clock is
	generic (
		add_offset			: integer := sys_mon_add ;
		data_offset			: integer := sys_mon_data ;
		ctrl_offset			: integer := sys_mon_ctrl ;
		status_offset		: integer := sys_mon_status 
	);
    Port ( 
		usr_clk					: in std_logic;
		usr_rst_n				: in std_logic;
		usr_func_wr				: in std_logic_vector(16383 downto 0); 
		usr_wen					: in std_logic;
		usr_data_wr				: in std_logic_vector(63 downto 0);
		usr_func_rd				: in std_logic_vector(16383 downto 0); 
		usr_data_rd				: out std_logic_vector(63 downto 0) := (others => '0');
		di_in               	: out STD_LOGIC_VECTOR(15 DOWNTO 0);
		daddr_in            	: out STD_LOGIC_VECTOR(7 DOWNTO 0);
		den_in              	: out STD_LOGIC;
		dwe_in              	: out STD_LOGIC;
		do_out              	: in STD_LOGIC_VECTOR(15 DOWNTO 0);
		drdy_out            	: in STD_LOGIC
           );
end COMPONENT;

signal resetp					: std_logic;
signal drp_data_out				: std_logic_vector(63 downto 0);
signal dsysmon_data_out			: std_logic_vector(63 downto 0);

--***************************************************************** 
--*************<     CODE START HERE    >**************************
--***************************************************************** 
 begin 
 
resetp	<= not(usr_rst_n);
 
SYSMON_i1:system_management_wiz_0
  PORT map(
    di_in               		=>	di_in   		        , 
    daddr_in            		=>	daddr_in		        , 
    den_in              		=>	den_in  		        , 
    dwe_in              		=>	dwe_in  		        , 
    drdy_out            		=>	drdy_out  		        , 
    do_out              		=>	do_out		       		, 
    dclk_in             		=>	usr_clk		            , 
    reset_in            		=>	resetp		            , 
    user_temp_alarm_out 		=>	user_temp_alarm_out		, 
    vccint_alarm_out    		=>	vccint_alarm_out   		, 
    vccaux_alarm_out    		=>	vccaux_alarm_out   		, 
    ot_out              		=>	ot_out             		, 
    channel_out         		=>	channel_out        		, 
    eoc_out             		=>	eoc_out            		, 
    alarm_out           		=>	alarm_out          		, 
    eos_out             		=>	eos_out            		, 
    busy_out            		=>	busy_out           		,
	i2c_sda 					=>	i2c_sda					,
    i2c_sclk 					=>	i2c_sclk				
  );
 
 
 
 
drp_i1:drp_itf_same_clock  
    Port Map( 
		usr_clk					=>	usr_clk			, 
		usr_rst_n				=>	usr_rst_n		, 
		usr_func_wr				=>	usr_func_wr		,  
		usr_wen					=>	usr_wen			, 
		usr_data_wr				=>	usr_data_wr		, 
		usr_func_rd				=>	usr_func_rd		,  
		usr_data_rd				=>	drp_data_out	, 
		di_in               	=>	di_in   		, 
		daddr_in            	=>	daddr_in		, 
		den_in              	=>	den_in  		, 
		dwe_in              	=>	dwe_in  		, 
		do_out              	=>	do_out  		, 
		drdy_out            	=>	drdy_out		  
           );                                         
  
 --########################################
--PCIe read access
process(usr_clk)
begin
	if rising_edge(usr_clk) then
		dsysmon_data_out				<= drp_data_out;
	 
			if  	usr_func_rd(sys_mon_status) = '1' then
				dsysmon_data_out(0)				<= 	user_temp_alarm_out;		
				dsysmon_data_out(1)				<= 	vccint_alarm_out   ;		
				dsysmon_data_out(2)				<= 	vccaux_alarm_out   ;		
				dsysmon_data_out(3)				<= 	ot_out             ;		
				dsysmon_data_out(9 downto 4)	<= 	channel_out        ;		
--				dsysmon_data_out(14 downto 10)	<= 	muxaddr_out        ;		
				dsysmon_data_out(15)			<= 	eoc_out            ;		
				dsysmon_data_out(16)			<= 	alarm_out          ;		
				dsysmon_data_out(17)			<= 	eos_out            ;		
				dsysmon_data_out(18)			<= 	busy_out           ;			
			end if;
 
	end if;
end process;

usr_data_rd	<= dsysmon_data_out;

end behavioral;