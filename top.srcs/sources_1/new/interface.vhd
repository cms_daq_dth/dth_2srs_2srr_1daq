library IEEE;
library WORK;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use ieee.numeric_std.all;

--this address should be multiply by 4 to be byte aligned
-- the selection chip are 16#0 ...16#3FFF   64 bit addressing
package interface is
	
	--################################################################################################################################
     type rx_usr_record	is
        record
            data_bus       	: std_logic_vector(127 downto 0);
            sopckt         	: std_logic;
            data_ena       	: std_logic;
            eopckt         	: std_logic;
            err_pckt       	: std_logic;
            empty_pckt			: std_logic_vector(3 downto 0);
        end record;
    
    type tx_usr_record    is
        record
            data_bus        	: std_logic_vector(127 downto 0);
            sopckt            : std_logic;
            data_ena        	: std_logic;
            eopckt            : std_logic;
            err_pckt        	: std_logic;
            empty_pckt        : std_logic_vector(3 downto 0);
        end record;
        
    type rx_user_ift	is array (0 to 3) of  rx_usr_record;
    type tx_user_ift	is array (0 to 3) of  tx_usr_record;
end package;