library IEEE;
library WORK;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use ieee.numeric_std.all;

--this address should be multiply by 4 to be byte aligned
-- the selection chip are 16#0 ...16#3FFF   64 bit addressing
package address_table is
		constant Compilation_version_off																			: integer := 16#3FFE#;

	--################################################################################################################################
		constant addr_offset_100G_eth0																				: integer := 16#0000#;-----------------------------OFFSET--------------------
		-- constant addr_offset_100G_eth1																				: integer := 16#4000#;-----------------------------OFFSET--------------------
		-- constant addr_offset_100G_eth2																				: integer := 16#8000#;-----------------------------OFFSET--------------------
		-- constant addr_offset_100G_eth3																				: integer := 16#C000#;-----------------------------OFFSET--------------------


	--################################################################################################################################
		-- offset in ONE 100G
		constant addr_offset_evt_gen_0																				: integer := 16#0100#;-----------------------------OFFSET--------------------
		constant addr_offset_evt_gen_1																				: integer := 16#0200#;-----------------------------OFFSET--------------------
		constant addr_offset_evt_gen_2																				: integer := 16#0300#;-----------------------------OFFSET--------------------
		constant addr_offset_evt_gen_3																				: integer := 16#0400#;-----------------------------OFFSET--------------------
		constant addr_offset_evt_gen_4																				: integer := 16#0500#;-----------------------------OFFSET--------------------
		constant addr_offset_evt_gen_5																				: integer := 16#0600#;-----------------------------OFFSET--------------------
		
		
		constant Event_Generator_Control																			: integer := 16#0000#; --0x3000
				-- constant event_gen_loop						                                                        : integer := 0;
				-- constant local_trigger					                                                            : integer := 1;
				-- constant event ongoing						                                                        : integer := 2;
				-- constant Backpressure					                                                            : integer := 3;
				-- constant Select data random  		                                                            	: integer := 4;
				
				-- constant generate a CRC error in the next fragment                                                 	: integer := 16;   
		constant Event_generator_evt_num																			: integer := 16#0001#; --0x3008
		constant Event_generator_BX_SOURCE 									                                        : integer := 16#0002#; --0x3010
				-- subtype BX_value							is natural range 31 downto 00;
				-- subtype source_value						is natural range 63 downto 32;
		constant Event_generator_length										                                        : integer := 16#0003#; --0x3018
		constant Event_generator_threshold_ready								                             		: integer := 16#0004#; --0x3020
		constant Event_generator_threshold_busy								                                        : integer := 16#0005#; --0x3028
		constant Event_Generator_Time_Btw_Frags							                                        	: integer := 16#0006#; --0x3030
		constant Event_generator_seed								                                        		: integer := 16#0007#; --0x3038
		constant Event_Generator_Load_Evt_Descriptors								                            	: integer := 16#0008#; --0x3040
		constant Event_generator_trigger_counter									                            	: integer := 16#0009#; --0x3040
		constant Event_generator_timer_counter															            : integer := 16#000A#; --0x3040
		constant event_test															                            	: integer := 16#0010#; --0x3040

	--################################################################################################################################		
		constant addr_offset_slink_0																				: integer := 16#0800#;-----------------------------OFFSET--------------------
		constant addr_offset_slink_1																				: integer := 16#0900#;-----------------------------OFFSET--------------------
		constant addr_offset_slink_2																				: integer := 16#0A00#;-----------------------------OFFSET--------------------
		constant addr_offset_slink_3																				: integer := 16#0B00#;-----------------------------OFFSET--------------------
		constant addr_offset_slink_4																				: integer := 16#0C00#;-----------------------------OFFSET--------------------
		constant addr_offset_slink_5																				: integer := 16#0D00#;-----------------------------OFFSET--------------------
		
		constant addr_offset_slinkS_0																				: integer := 16#0E00#;-----------------------------OFFSET--------------------
		constant addr_offset_slinkS_1																				: integer := 16#0F00#;-----------------------------OFFSET--------------------
		constant addr_offset_slinkS_2																				: integer := 16#1000#;-----------------------------OFFSET--------------------
		
       -- constant        SSlinkrocket_access_command      : integer := 16#0000#; --x2400
                                                                               -- execute the command (the data should be set first)
                                                                               -- send the offset
                                                                               -- read b0 access done
                                                                               -- read b1 access good
       -- constant        SSlinkrocket_access_data         : integer := 16#0001#; --x2408
                                                                               -- write data to write    
                                                                               -- read  data received
       -- constant        SSlinkrocket_resync_request      : integer := 16#0002#; --x2410 
		
          
        --constant        SSR_RecSERDES_DRP_ctr            : integer := 16#0004#;          
        --constant        SSR_RecSERDES_DRP_rtn            : integer := 16#0005#;  
       -- constant        SSR_RecSERDES_status             : integer := 16#0006#;  
        --constant        SSR_RecSERDES_setup              : integer := 16#0007#;  	
   	
								
													
        constant        Slinkrocket_access_command      															: integer := 16#0000#; --x2400
																																		-- execute the command (the data should be set first)
																																		-- send the offset
																																		--read b0 access done
																																		--read b1 access good
        constant        Slinkrocket_access_data         															: integer := 16#0001#; --x2408
																																		--write data to write    
																																		--read  data received
        constant        Slinkrocket_resync_request      															: integer := 16#0002#; --x2410
	
                
        constant        SLINKRocket_rcv_serdes_control                  											: integer := 16#0003#;          
        constant        SLINKRocket_rcv_serdes_DRP_control              											: integer := 16#0004#;          
        constant        SLINKRocket_rcv_serdes_DRP_data_returned        											: integer := 16#0005#;  
        constant        SLINKRocket_rcv_serdes_status                   											: integer := 16#0006#;  
        constant        SLINKRocket_rcv_serdes_analog                   											: integer := 16#0007#;
        constant        SLINKRocket_FEDx_Fragment_rcv                   											: integer := 16#0008#;
        constant        SLINKRocket_FEDx_Fragment_rcv_wth_error           											: integer := 16#0009#;
        constant        SR_RecSERDES_reset              															: integer := 16#000A#;	
		
		constant		SLINKRocket_FEDx_link_setup																	: integer := 16#000B#;
		constant		SLINKRocket_rcv_serdes_loopback																: integer := 16#000C#;
		constant		SLINKRocket_serdes_PRBS_test																: integer := 16#000D#;
		
													
		constant SLINKRocket_FEDx_OL_status							        										: integer := 16#0010#; --0x0020
													
		constant SLINKRocket_FEDx_FED_crc_counter					        										: integer := 16#0011#; --0x0028
		constant SLINKRocket_FEDx_SLINK_crc_counter				            										: integer := 16#0012#; --0x0030
		constant SLINKRocket_FEDx_backpressure_counter			            										: integer := 16#0013#; --0x0038
		constant SLINKRocket_FEDx_received_PCKT					            										: integer := 16#0014#; --0x0040
		constant SLINKRocket_FEDx_TOT_rcv_PCKT						        										: integer := 16#0015#; --0x0048
		constant SLINKRocket_FEDx_Received_BAD_PCKT				            										: integer := 16#0016#; --0x0050
		constant SLINKRocket_FEDx_CUR_seq_num						        										: integer := 16#0017#; --0x0058
		constant SLINKRocket_FEDx_CMD_seq_num						        										: integer := 16#0018#; --0x0060
		constant SLINKRocket_FEDx_FEDID								        										: integer := 16#0019#; --0x0068
			subtype SLINKRocket_FEDID_set					is natural range  15 downto 0 ;
			subtype SLINKRocket_FEDID_wrong					is natural range  31 downto 16 ;
		constant SLINKRocket_FEDx_Max_frag_size					            										: integer := 16#001A#; --0x0070
			subtype Max_frag_size_bits	 					is natural range 23 downto 0 ;
		constant SLINKRocket_FEDx_nb_of_cut_frag					        										: integer := 16#001B#; --0x0078
		constant SLINKRocket_FEDx_rcv_max_size						        										: integer := 16#001C#; --0x0080
		constant SLINKRocket_FEDx_current_frag_size				            										: integer := 16#001D#; --0x0088
		-- constant SLINKRocket_FEDx_Fragment_rcv						        										: integer := 16#001E#; --0x0090
		--constant SLINKRocket_FEDx_Fragment_rcv_wth_error		            										: integer := 16#001F#; --0x0098
		constant SLINKRocket_FEDx_expect_trig_nb					        										: integer := 16#0020#; --0x00A0
		constant SLINKRocket_FEDx_bad_trig_num						        										: integer := 16#0021#; --0x00A8
		constant SLINKRocket_FEDx_current_trg_number				        										: integer := 16#0022#; --0x00B0
	    constant SLINKRocket_FEDx_current_FED_BX					            									: integer := 16#0023#; --0x00B8
		constant SLINKRocket_FEDx_cDAQ_Backpressure				            										: integer := 16#0024#; --0x00C0
												
		constant SLINKRocket_FEDx_input_status					  	        										: integer := 16#0025#; --0x0100
					
		constant SLINKRocket_FEDx_backpressure_emu_counter			        										: integer := 16#0026#; --0x0200
										
        
		constant	SR_Rec_pckt_counter																				: integer := 16#002D#;	
		constant	SR_Rec_Bad_pckt_counter																			: integer := 16#002E#;	
		constant	SR_Rec_BP_counter																				: integer := 16#002F#;	
		constant	SR_Rec_FED_CRC_error_counter																	: integer := 16#0030#;	
		constant	SR_Rec_SR_CRC_error_counter																		: integer := 16#0031#;	
		constant	SR_DAQ_to_SR_Bp_counter  																		: integer := 16#0032#;	


	--################################################################################################################################	
		
		constant    FED_SR_status_address_off                                                                       : integer := 16#0042#;
		constant    FED_SR_status_off                                                                               : integer := 16#0043#;
		constant    FED_SR_SND_gen_error                                                                            : integer := 16#0044#;
		
		constant    FED_SR_snd_serdes_analog																		: integer := 16#0050#;
		constant    FED_SR_snd_status   																			: integer := 16#0051#;
		
	--################################################################################################################################	
 		-- additionnal offset for status form TCP 
		constant 	addr_off_con0																					: integer := 16#1400#;-----------------------------OFFSET--------------------
		constant 	addr_off_con1																					: integer := 16#1800#;-----------------------------OFFSET--------------------
				
		constant eth_100Gb_ETH_request_status																		: integer := 16#00FA#; --0x2100
			constant eth_100Gb_local_reset		:integer := 0;
			constant eth_100Gb_req_TCP_sync		:integer := 1; -- request 
			constant eth_100Gb_req_TCP_rst		:integer := 2;
			constant eth_100Gb_probe_ARP		:integer := 3;
			constant eth_100Gb_ARP_req			:integer := 4;
			constant eth_100Gb_Persist_state	:integer := 12;
			constant eth_100Gb_MAC_rcv_OK		:integer := 13;
			constant eth_100Gb_MAC_rcv_GW		:integer := 14;
			constant eth_100Gb_ARP_done			:integer := 15;
			--subtype eth_100Gb_TCP_state	is natural range 18 downto 16;--3 bits
			constant eth_100Gb_latch_values	:integer := 31;
		constant eth_100Gb_Dest_MAC_address																			: integer := 16#00FB#; -- 0x2108
		constant eth_100Gb_MAC_Conflict																				: integer := 16#00FC#; -- 0x2110
		constant eth_100Gb_IP_Conflict																				: integer := 16#00FD#; -- 0x2118
		constant eth_100Gb_MAC_WITH_IP_CONFLICT																		: integer := 16#00FE#; -- 0x2120
 
		constant eth_100Gb_ctrl_port0_def																			: integer := 16#00FF#; --0x2800
			subtype eth_100Gb_port0_Dest_def	is natural range 15 downto 0; 
			subtype eth_100Gb_port0_Src_def		is natural range 31 downto 16;
		constant eth_100Gb_ctrl_port1_def																			: integer := 16#0100#; --0x2800
			subtype eth_100Gb_port1_Dest_def	is natural range 15 downto 0; 
			subtype eth_100Gb_port1_Src_def		is natural range 31 downto 16;
		constant eth_100Gb_ctrl_MAC_def																				: integer := 16#0101#; --0x2808 48 b
		constant eth_100Gb_ctrl_IPS_def																				: integer := 16#0102#; --0x2810 32 b
		constant eth_100gb_ctrl_IPD_def																				: integer := 16#0103#; --0x2818 32 b
		constant eth_100gb_ctrl_Init_def																			: integer := 16#0104#; --0x2820 see bit details
		constant eth_100gb_ctrl_Max_size_Scale_def																	: integer := 16#0105#; --0x2828
			subtype eth_100Gb_MaxSize_def		is natural range 15 downto 0;
			subtype eth_100Gb_scale_def			is natural range 23 downto 16;
		constant eth_100Gb_ctrl_timestamp_def																		: integer := 16#0106#; --0x2830 32 b
		constant eth_100Gb_ctrl_timestamp_reply_def																	: integer := 16#0107#; --0x2838 32 b
		constant eth_100Gb_ctrl_IP_network_def																		: integer := 16#0108#; --0x2840 32 b
		constant eth_100Gb_ctrl_IP_gateway_def																		: integer := 16#0109#; --0x2848 32 b
		constant eth_100Gb_ctrl_CongWind_def																		: integer := 16#010A#; --0x2850 32 b
			subtype eth_100Gb_Cong_wind_def		is natural range 31 downto 3; --8 bytes aligned
		constant eth_100Gb_ctrl_timer_RTT_def																		: integer := 16#010B#; --0x2858 32 b
		constant eth_100Gb_ctrl_timer_RTT_sync_def																	: integer := 16#010C#; --0x2860 32 b
		constant eth_100Gb_ctrl_timer_persist_def																	: integer := 16#010D#; --0x2868 32 b
		constant eth_100Gb_ctrl_thresold_retrans_def																: integer := 16#010E#; --0x2870 32 b
		constant eth_100Gb_ctrl_Rexmt_CWND_Sh_def																	: integer := 16#010F#; --0x2878 32 b
		constant eth_100Gb_counter_RTT																				: integer := 16#0110#; --0x2880 32 b
		constant eth_100Gb_counter_conn_attempt																		: integer := 16#0111#; --0x2888 15 b
		constant eth_100Gb_Status_flags_SND_probe																	: integer := 16#0112#; --0x2890 32 b
			subtype eth_100Gb_Status_TCP_flags	is natural range 15 downto 0; --8 bytes aligned
			subtype eth_100Gb_SND_probe				is natural range 31 downto 16; --8 bytes aligned
		constant eth_100Gb_counter_SND_pack																			: integer := 16#0113#; --0x2898 32 b
		constant eth_100Gb_counter_Rexmit_pack																		: integer := 16#0114#; --0x28A0 32 b
		constant eth_100Gb_counter_Rcv_Dupl_pack																	: integer := 16#0115#; --0x28A8 32 b
		constant eth_100Gb_counter_Fast_retransmit																	: integer := 16#0116#; --0x28B0 32 b
		constant eth_100Gb_counter_measure_rtt																		: integer := 16#0117#; --0x28B8 32 b
		constant eth_100Gb_counter_rcv_ack_too_much																	: integer := 16#0118#; --0x28C0 32 b
		constant eth_100Gb_counter_seg_drop_after_ack																: integer := 16#0119#; --0x28C8 32 b
		constant eth_100Gb_counter_seg_dropped																		: integer := 16#011A#; --0x28D0 32 b
		constant eth_100Gb_counter_seg_dropped_wRst																	: integer := 16#011B#; --0x28D8 32 b
		constant eth_100Gb_counter_persist_exit																		: integer := 16#011C#; --0x28E0 32 b
		constant eth_100Gb_counter_persist																			: integer := 16#011D#; --0x28E8 32 b
		constant eth_100Gb_counter_send_bytes																		: integer := 16#011E#; --0x28F0 64 b
		constant eth_100Gb_counter_rexmit_bytes																		: integer := 16#011F#; --0x28F8 64 b
		constant eth_100Gb_counter_persist_closed_win																: integer := 16#0120#; --0x2900 32 b
		constant eth_100Gb_counter_unaligned_to_64b																	: integer := 16#0121#; --0x2908 32 b
		constant eth_100Gb_status_dupack_max																		: integer := 16#0122#; --0x2910 32 b
		constant eth_100Gb_status_win_max																			: integer := 16#0123#; --0x2918 32 b
		constant eth_100Gb_status_win_min																			: integer := 16#0124#; --0x2920 32 b
		constant eth_100Gb_status_RTT_Max																			: integer := 16#0125#; --0x2928 32 b
		constant eth_100Gb_status_RTT_Min																			: integer := 16#0126#; --0x2930 32 b
		constant eth_100Gb_status_RTT_Sum																			: integer := 16#0127#; --0x2938 64 b
		constant eth_100Gb_status_Current_wind							    										: integer := 16#0128#; --0x2940 32 b
		constant eth_100Gb_counter_persist_zero_wind					    										: integer := 16#0129#; --0x2948 32 b
		constant eth_100Gb_status_RTT_shift_max							    										: integer := 16#012A#; --0x2950 32 b
		constant eth_100Gb_status_RTT_val																			: integer := 16#012B#; --0x2958 32 b
		constant eth_100Gb_status_Max_T_between_ack					        										: integer := 16#012C#; --0x2960 32 b
		constant eth_100Gb_status_Ack_current_time						    										: integer := 16#012D#; --0x2968 32 b
		constant eth_100Gb_counter_nb_ack																			: integer := 16#012E#; --0x2970 32 b
		constant eth_100Gb_Accu_ack_timer																			: integer := 16#012F#; --0x2978 64 b
		constant eth_100Gb_status_MSS_receive							    										: integer := 16#0130#; --0x2980 32 b
		constant eth_100Gb_counter_rcv_good_pckt						    										: integer := 16#0131#; --0x2988 64 b
		constant eth_100Gb_counter_rcv_bad_pckt							    										: integer := 16#0132#; --0x2990 64 b
		constant eth_100Gb_counter_sent_pckt																		: integer := 16#0133#; --0x2998 64 b
		constant eth_100Gb_counter_rcv_ack								    										: integer := 16#0134#; --0x29A0 64 b
		constant eth_100Gb_counter_rcv_ARP								    										: integer := 16#0135#; --0x29A8 64 b		
		constant eth_100Gb_pause_frame_rcv								    										: integer := 16#0136#; --0x29B0 64 b
		constant eth_100Gb_pfc_frame_rcv																			: integer := 16#0137#; --0x29B8 64 b	
		constant eth_100Gb_tx_error_pckt																			: integer := 16#0138#; --0x29C0 32 b									                                            : integer := 16#538#; --0x29C0 32 b	
		constant eth_100Gb_tcp_state																				: integer := 16#0139#; --0x29C8 32 b
		constant eth_100Gb_ping_counter_latched																		: integer := 16#0140#; --0x29D0 32 b
		constant eth_100Gb_counter_reset																			: integer := 16#0140#; --0x29D8 32 b
		constant eth_100Gb_nb_retrans_before_stop																	: integer := 16#0141#; --0x29E0 32 b
		constant eth_100Gb_ping_req_cnt																				: integer := 16#0142#; --0x29E0 32 b
		constant eth_100Gb_ping_rcv_cnt																				: integer := 16#0143#; --0x29E0 32 b
		constant eth_100Gb_test																						: integer := 16#0146#; --0x29E0 32 b
												
															
		constant BIFI_used_off																						: integer := 16#0150#; --0x3800 64 b 
		constant BIFI_Max_used																						: integer := 16#0151#; --0x3808 64 b
		constant BIFI_BackPressure																					: integer := 16#0152#; --0x3810 64 b
		constant Window_trg_stop																					: integer := 16#0153#; --0x3818 64 b
		constant nb_frag_before_BP																					: integer := 16#0154#; --0x3820 64 b
		constant gap_between_eth_pckt										                                        : integer := 16#0155#; --0x3828 64 b
		constant BIFI_size            										                                        : integer := 16#0156#; --0x3830 64 b
		constant BIFI_wr_pointer_item  										                                        : integer := 16#0157#; --0x3838 64 b
		constant BIFI_new_SND_item  										                                        : integer := 16#0158#; --0x3838 64 b
			
		
		constant eth_10gb_qsfp_acc																					: integer := 16#0180#; --0x2028
			--   register send to access the SFP
			--   bit 31-24  = "A0" to access the internal register; "A2" to access the real time registers
			--			bit 24   '1'  read access ; '0' write access
			--   bit 23-16 --
			-- 	 bit 15-8  register address
			--   bit 7-0   data to be written
			--   
			--  returned data
			--  bit 17 '1' indicate that previous access had a error (reset by a access request
			--  bit 16 '1' previous access is finished ; data value for read access; a write spend 7 ms;
			--  bit 7-0  data read
			--
			-- Ex:  A2002356 : execute a write on address 16#A2  register 16#23 data 16#56
			--		  A10034xx : execute a read request on address 16#A0  register 16#34
			--							a read on DTO will return the data read
		constant eth_100gb_qsfp_setup																				: integer := 16#0181#; --0x2030
			constant QSPF_present					: integer := 0;
			constant QSFP_int						: integer := 1;
			constant QSFP_low_power_r_bit			: integer := 2;
			constant QSFP_reset_r_bit				: integer := 3;
			constant QSFP_modsel_r_bit				: integer := 4;
		 

		
	--##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  
	
		constant  MAC100G_SERDES_reset																						: integer := 16#0000#;--0x4000
		constant  MAC100G_SERDES_loopback																					: integer := 16#0001#;--0x4008
		constant  MAC100G_SERDES_voltage																					: integer := 16#0002#;--0x4010
		constant  MAC100G_SERDES_DRP_cmd                         															: integer := 16#0003#;--0x4018  
		constant  MAC100G_SERDES_DRP_dt                         															: integer := 16#0004#;--0x4020  
		constant  MAC100G_SERDES_ChDRP_cmd                         															: integer := 16#0005#;--0x4028  
		constant  MAC100G_SERDES_ChDRP_dt                        															: integer := 16#0006#;--0x4030  
		
		constant  MAC100G_SERDES_AXI_Add                         															: integer := 16#0007#;--0x4038  
		constant  MAC100G_SERDES_AXI_Dti                         															: integer := 16#0008#;--0x4040  
		constant  MAC100G_SERDES_AXI_ctrl                        															: integer := 16#0009#;--0x4048  
		constant  MAC100G_SERDES_AXI_Dto                         															: integer := 16#000A#;--0x4050  
		constant  MAC100G_SERDES_AXI_status                      															: integer := 16#000B#;--0x4058  
		
		constant  MAC100G_SERDES_TX_status                      															: integer := 16#000C#;--0x4060  
		constant  MAC100G_SERDES_RX_error	                      															: integer := 16#000D#;--0x4068  
		constant  MAC100G_SERDES_TX_error_1	                      															: integer := 16#000E#;--0x4070  
		constant  MAC100G_SERDES_TX_error_2	                      															: integer := 16#000F#;--0x4078  

		constant  MAC100G_SERDES_TX_pause_frame_req 	             														: integer := 16#0010#;--0x4080  
		constant  MAC100G_SERDES_PRBS_TEST																					: integer := 16#0011#;--0x4088  
		
		constant  MAC100G_SERDES_status																						: integer := 16#0020#;--0x4330
		constant  MAC100G_SERDES_statusa																					: integer := 16#0021#;--0x4338
		constant  MAC100G_rx_frame_l0_error																					: integer := 16#0022#;--0x4340
		constant  MAC100G_rx_frame_l1_error																					: integer := 16#0023#;--0x4348
		constant  MAC100G_rx_frame_l2_error																					: integer := 16#0024#;--0x4350
		constant  MAC100G_rx_frame_l3_error																					: integer := 16#0025#;--0x4358 
		constant  MAC100G_rx_l0_bip_error																					: integer := 16#0026#;--0x4360
		constant  MAC100G_rx_l1_bip_error																					: integer := 16#0027#;--0x4368
		constant  MAC100G_rx_l2_bip_error																					: integer := 16#0028#;--0x4370
		constant  MAC100G_rx_l3_bip_error																					: integer := 16#0029#;--0x4378 
		constant  MAC100G_test																								: integer := 16#002A#;--0x4378 
        constant MAC100G_fec_status   																	                    : integer := 16#002B#; --0x29E0 32 b
		constant MAC100G_fec_corrected_counter																		        : integer := 16#002C#; --0x29E0 32 b
		constant MAC100G_fec_uncorrected_counter   																	        : integer := 16#002D#; --0x29E0 32 b
		
	--################################################################################################################################
		
		constant	Generic_reset										    												: integer := 16#3FFF#;-- x
		
		constant	QSFP_i2c_access_a									    												: integer := 16#0000#;-- x
		constant	QSFP_i2c_access_b									    												: integer := 16#0001#;-- x
														
		constant	FireFly_i2c_access_a																					: integer := 16#0002#;-- x
		constant	FireFly_i2c_access_b																					: integer := 16#0003#;-- x
														
		constant	Power_i2c_access_a									    												: integer := 16#0004#;-- x
		constant	Power_i2c_access_b									    												: integer := 16#0005#;-- x
														
		constant	SN_i2c_access_a									        												: integer := 16#0006#;-- x
		constant	SN_i2c_access_b									        												: integer := 16#0007#;-- x
														
		constant	Clock_i2c_access_a									    												: integer := 16#0008#;-- x
		constant	Clock_i2c_access_b									    												: integer := 16#0009#;-- x
														
														
		constant 	sys_mon_add																								: integer := 16#0020#;-- x
		constant 	sys_mon_data																							: integer := 16#0021#;-- x
		constant 	sys_mon_ctrl																							: integer := 16#0022#;-- x
		constant 	sys_mon_status																							: integer := 16#0023#;-- x
												
		constant	LED_control																								: integer := 16#0040#;
		constant	reset_hw_comp																							: integer := 16#0041#;
		constant	Slink_speed_off																							: integer := 16#0042#;
		constant 	QSFP_monitoring																							: integer := 16#0043#;
		constant 	Firefly_monitoring																						: integer := 16#0044#;
		constant 	CLOCK_gen_status																						: integer := 16#0045#;
		
        constant    clock_FED_measure                                                                                       : integer := 16#0046#;
        constant    clock_UR_measure                                                                                        : integer := 16#0047#;
        constant    clock_SR0_measure                                                                                       : integer := 16#0048#;
        constant    clock_SR1_measure                                                                                       : integer := 16#0049#;
        
		constant  	Prog_flash_AXI_Add                                           											: integer := 16#00000200#; -- Load the address of the flash to start to write firmware
		constant  	Prog_flash_AXI_Dti                                           											: integer := 16#00000201#; -- Load the data to write to the Flash memory
		constant  	Prog_flash_AXI_Dto                                           											: integer := 16#00000202#; -- Data from AXI FPGA Flash 
		constant  	Prog_flash_AXI_ctrl                                          											: integer := 16#00000203#; -- Control the access to the flash
		constant  	Prog_flash_AXI_status                                        											: integer := 16#00000204#; -- AXI FPGA_FLASH status
	    constant  	Prog_ICAPE_dti                                               											: integer := 16#00000205#; -- Write 32-bit data to ICAPE IP
		constant  	Prog_ICAPE_dto                                               											: integer := 16#00000206#; -- read 32-bti data from ICAPE IP
		constant  	Prog_ICAPE_ctrl                                              											: integer := 16#00000207#; -- ICAPE status
		    
		
end package address_table;